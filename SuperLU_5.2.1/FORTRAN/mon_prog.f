*
* Copyright (c) 2003, The Regents of the University of California, through
* Lawrence Berkeley National Laboratory (subject to receipt of any required 
* approvals from U.S. Dept. of Energy) 
* 
* All rights reserved. 
* 
* The source code is distributed under BSD license, see the file License.txt
* at the top-level directory.
*
      program f77_main
      integer maxn, maxnz
      parameter ( maxn = 10000, maxnz = 100000 )
      integer rowind(maxnz), colptr(maxn)
      real*8  values(maxnz), b(maxn)
      integer n, nnz, nrhs, ldb, info, iopt
      integer*8 factors
*
c      call hbcode1(n, n, nnz, values, rowind, colptr)
            open(34,file='ma_matrice')
            read(34,*)n,nnz
            read(34,*)(colptr(i),i=1,n+1)
            read(34,*)(rowind(i),i=1,nnz)
            read(34,*)(values(i),i=1,nnz)
            read(34,*)(b(i),i=1,n)   
            close(34)   
*
      nrhs = 1
      ldb = n
c      do i = 1, n
c         b(i) = 1
c      enddo
*
* First, factorize the matrix. The factors are stored in *factors* handle.
      iopt = 1
      call c_fortran_dgssv( iopt, n, nnz, nrhs, values, rowind, colptr, 
     $                      b, ldb, factors, info )
*
      if (info .eq. 0) then
         write (*,*) 'Factorization succeeded'
      else
         write(*,*) 'INFO from factorization = ', info
      endif
*
* Second, solve the system using the existing factors.
      iopt = 2
      call c_fortran_dgssv( iopt, n, nnz, nrhs, values, rowind, colptr, 
     $                      b, ldb, factors, info )
*
      if (info .eq. 0) then
         write (*,*) 'Solve succeeded'
         do i = 1, n
         write (*,*)i, b(i)
         enddo
      else
         write(*,*) 'INFO from triangular solve = ', info
      endif

* Last, free the storage allocated inside SuperLU
      iopt = 3
      call c_fortran_dgssv( iopt, n, nnz, nrhs, values, rowind, colptr, 
     $                      b, ldb, factors, info )
*
      stop
      end


