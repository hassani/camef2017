c
c---- Programme qui lit un fichier de maillage GMSH et qui
c
c     - ecrit une entete donnant le nbr d'element, de noeuds, etc
c     - renumerote les noeuds (optionnel)
c
c     Usage : optinum_gmh -i=file_in [-o=file_out] [-n]
c
c     Seule l'option -i est obligatoire. 
c     Les options sont dans un ordre quelconque.
c
c     avec 
c        -i : (obligatoire) 
c            donner le nom du fichier gmsh (sans l'extension .msh)
c
c        -o :  (optionnel)
c            donner le nom du fichier resultant (sans l'extension .msh)
c            Si l'option -o n'est pas utilisee, file_out = file_in (ecrasement)
c
c        -n : (optionnel)
c            pas de renumeroation des noeuds
c            Si l'option -n n'est pas utilisee, renumerotation
c
c
c     Exemples :
c
c           * optinum_gmh -i=cub1 -o=cub1_optim
c
c           lira le fichier cub1.msh, optimisera sa numerotation et ecrira
c           sur le fichier cub1_optim.msh une entete et le maillage renumerote
c
c           * optinum_gmh -i=cub1 -n
c
c           lira le fichier cub1.msh, le réécrira avec une entete
c
c           * optinum_gmh -i=cub1 -n -o=cub2
c
c           lira le fichier cub1.msh, le réécrira avec une entete dans le fichier cub2.msh
c
c
c
c
c
c
c                                          R.H., Chambery 2007 
c                                          modifie, Nice 2016
c
      implicit integer (a-z)    
c
      parameter (wmax0 = 152 000 000, irati0 = 2)  
c      
      common w(wmax0)
c
c---- common de gestion memoire
c
      common / numero / inum
      common / wsize  / wmax, iratio
c      
      inum = 0
      wmax = wmax0
      iratio = irati0
c      
      ncolm = 50
      maxcn = 50
      ndofn = 1
c      
      call readgmsh0 (npoin,nobje,ndime,nnode,nelem,
     .                nmats,nedge,nface,ndfac,ndedg,
     .                npoif,ioptim)
c
c---- tablaux necessaires a la sauvegarde du maillage
c   
      coord = ialloc ('coord',    3*npoin,2) ! table de coordonnees
      lobje = ialloc ('lobje',ncolm*nobje,1) ! table des objets elementaires
c
c---- tableau necessaires a l'optimisation de la numerotation
c
      lnods = ialloc ('lnods',nnode*nelem,1) ! table de connectivite
      coorn = ialloc ('coorn',    3*npoin,2) ! nouvelle table de coordonnees
      lobjn = ialloc ('lobjn',ncolm*nobje,1) ! nouvelle table des objets elementaires
      
      ndfro = ialloc ('ndfro',nelem      ,1)
      jnt   = ialloc ('jnt  ',npoin      ,1)
      joint = ialloc ('joint',npoin      ,1)
      jmem  = ialloc ('jmem ',npoin      ,1)
      memjt = ialloc ('memjt',maxcn*npoin,1)
      newjt = ialloc ('newjt',npoin      ,1)
      je    = ialloc ('je   ',nelem      ,1)      

      lorph = ialloc ('lorph',npoin      ,1) ! pour marquer les points "orphelins"      
      
c      call infos(6)
c
c---- lecture du maillage
c
      call readgmsh1 (w(coord),w(lobje),w(lnods),w(joint),
     .                ndime   ,npoin   ,nobje   ,nnode   ,
     .                nelem   ,ncolm   )
     
      if (ioptim .eq. 1) then
c
c----    optimisation
c         
         ifron = 0
         ndofn = 3
         call optinum (w(lnods),w(jnt  ),w(joint),w(jmem ),
     .                 w(memjt),w(newjt),w(ndfro),w(je   ),
     .                 w(lorph),
     .                 ifron   ,nnode   ,nelem   ,npoin   ,
     .                 ndime   ,ndofn   ,maxcn            )
c 
c----    renumerotation
c     
         call newnum (w(lobje),w(lobjn),w(coord),w(coorn),
     .                w(jmem ),w(jnt  ),
     .                nobje   ,npoin   ,ncolm   ) 
c
c----    ecriture
c
         call writegmsh (w(coorn),w(lobjn),npoin,nobje,
     .                   ncolm   ,ndime   ,nnode,nelem,
     .                   nmats,nedge,nface,ndfac,ndedg,
     .                   npoif)
      else
c
c----    ecriture (pas d'optimisation : numerotation conservee)
c
         call writegmsh (w(coord),w(lobje),npoin,nobje,
     .                   ncolm   ,ndime   ,nnode,nelem,
     .                   nmats,nedge,nface,ndfac,ndedg,
     .                   npoif)    
      endif  
      
      write(*,1)
1     format(//,' T E R M I N A I S O N   N O R M A L E',//)           
      end
c
c
c*********************************************************************
c
      subroutine writegmsh (coord,lobje,npoin,nobje,
     .                      ncolm,ndime,nnode,nelem,
     .                      nmats,nedge,nface,ndfac,
     .                      ndedg,npoif)
c
      include 'common/precision.f'
c
      dimension coord(3    ,npoin), lobje(ncolm,nobje)
c
      character msh_name_in*80, msh_name_out*80
      common / namefiles / msh_name_in, msh_name_out      
         
      imsh = 1
      open(imsh,file=msh_name_out)
c        
      write(imsh,'(a4)')'$NOD'
      write(imsh,1)0,nelem,npoin,ndime,nnode,
     .               nedge,nface,nmats,ndfac,ndedg,npoif,' (info)'
      write(imsh,'(a7)')'$ENDNOD'
c           
      write(imsh,'(a4)')'$NOD'
      write(imsh,*)npoin
      do ip = 1, npoin
         write(imsh,2)ip,(coord(id,ip),id=1,3)
      enddo
      write(imsh,'(a7)')'$ENDNOD'
      write(imsh,'(a4)')'$ELM'
      write(imsh,*)nobje
      do io = 1, nobje
         nnodo = lobje(4,io)
         write(imsh,3)io,(lobje(i,io),i=1,4),(lobje(i+4,io),i=1,nnodo)
      enddo
      write(imsh,'(a7)')'$ENDELM'      
c
      close(imsh)
      return
1     format(i1,1x,10i8,1x,a7)
2     format(i8,1x,3e13.5)
3     format(30i8)      
      end
c
c*********************************************************************
c
      subroutine readgmsh0 (npoin,nobje,ndime,nnode,nelem,
     .                      nmats,nedge,nface,ndfac,ndedg,
     .                      npoif,ioptim)
c
c---- on lit une premiere fois le fichier de maillage
c     pour determiner le nbr de noeuds, d'elements, de 
c     dimensions, etc.
c
      character msh_name_in*80, msh_name_out*80,buf*80
c
      character arg*80, options(3)*200, opt*2

      common / namefiles / msh_name_in, msh_name_out
c
      ioptim = 1
      msh_name_in  = ''
      msh_name_out = ''
      iopt_i = 0
      iopt_o = 0
      iopt_v = 0
c
      noptions = iargc()
c
      if (noptions .eq. 0) then
         write(*,2)
         stop
      endif
c               
      options = ""
      do i = 1, noptions
         call getarg(i,options(i))
      end do

      do i = 1, noptions
         opt = options(i)(1:2)
         select case(trim(opt))
          case("-i")
             arg = options(i)(4:)
             read(arg,*)msh_name_in
             iopt_i = 1
          case("-o")
             arg = options(i)(4:)      
             read(arg,*)msh_name_out
             iopt_o = 1
          case("-n")
             ioptim = 0
             iopt_n = 1
          case("-h")
              write(*,2)
              stop   
          case default
             print*,'option ',trim(opt),' non reconnue'
             stop
         end select
      END DO
c
      if (iopt_i .eq. 0) then
         print*
         print*,'Erreur : nom de fichier initial manquant (option -i)'
         print*,'pour avoir de l''aide, tapez : optinum_gmsh -h'
         print*,'stop'
         print*
         stop
      endif         
c
      
c      write(*,*)
c      write(*,'(a,$)')
c     .' nom du fichier gmsh initial (sans l''extension .msh): '
c      read(*,'(a80)')msh_name_in
      nlong = istrlen(msh_name_in)
      msh_name_in(nlong+1:nlong+4) = '.msh'
      print*,'fichier input:',msh_name_in

c
c      write(*,'(a,$)')
c     .' nom du fichier gmsh final   (sans l''extension .msh): '
c      read(*,'(a80)')msh_name_out
      nlong = istrlen(msh_name_out)
      if (iopt_o .eq. 0) then 
         msh_name_out = msh_name_in
      else   
         msh_name_out(nlong+1:nlong+4) = '.msh' 
      endif        
      print*,'fichier output:',msh_name_out

      print*,'ioptim:',ioptim
      if (ioptim .eq. 0) then
         print*
         print*,'Pas de renumerotation demandee'
         print*,'(numerotation initiale conservee)'
         print*
      endif   
c              
      imsh = 1
      open(imsh,file=msh_name_in,status='old')
c      
      nedge = 0
      nface = 0
      ndfac = 0
      ndedg = 0
      nrefv = 0
      nreff = 0
      nrefe = 0
      npoif = 0
10    read(imsh,8,end=20)buf
      if (buf(1:1) .eq. '$') then
         if (buf(2:8) .eq. 'NOD') then
            read(imsh,*)npoin
            do ip = 1, npoin
              read(imsh,*)ipp
            enddo 
         endif        
         if (buf(2:8) .eq. 'ELM') then
            read(imsh,*)nobje
            ie2d = 0
            ie3d = 0
            do io = 1, nobje
               read(imsh,*)i,itypo,iregp,irego,nnoeud
               
               if (itypo .eq. 1 .or. itypo .eq. 8) then
                  nedge = nedge + 1
                  ndedg = nnoeud
                  nrefe = max(nrefe,irego)
               endif
               if (itypo .eq. 2 .or. itypo .eq. 3  .or.
     .             itypo .eq. 9 .or. itypo .eq. 10 .or.
     .             itypo .eq. 16) then
                  nface = nface + 1
                  ndfac = nnoeud
                  nreff = max(nreff,irego)
               endif
               if (itypo .eq. 4  .or. itypo .eq. 5  .or.
     .             itypo .eq. 11 .or. itypo .eq. 12 .or.
     .             itypo .eq. 17) then
                  nrefv = max(nrefv,irego)
               endif      
                  
               if (itypo .eq. 2 .or. itypo .eq. 3 .or.
     .             itypo .eq. 9 .or. itypo .eq. 10 .or.
     .             itypo .eq. 16) then
                  ie2d = ie2d + 1
                  nnode2d = nnoeud
               endif
c         
               if (itypo .eq. 4 .or. itypo .eq. 5 .or.
     .             itypo .eq. 6 .or. itypo .eq. 7 .or.
     .             itypo .eq.11 .or. itypo .eq.12 .or.
     .             itypo .eq.13 .or. itypo .eq.14 .or.
     .             itypo .eq.17) then
                  ie3d = ie3d + 1
                  nnode3d = nnoeud
               endif
c               
               if (itypo .eq. 15) then
                  npoif = npoif + 1
               endif   
            enddo
c
            if (ie3d .eq. 0) then
               ndime = 2
               nnode = nnode2d
               nelem = ie2d
            else
               ndime = 3
               nnode = nnode3d
               nelem = ie3d
            endif                
         endif
      endif
c         
      goto 10
20    close(imsh) 
c      
      nmats = nrefv
      if (ndime .eq. 2) nmats = nreff
      write(*,9) npoin,nobje,nnode,nelem,ndime
c      
      return
c
1     format(a1)
2     format(//,'Programme optinum',//,
     . 'Lit un fichier de maillage GMSH, optimise la numerotation',
     ./,'et ecrit une entete (utile pour camef)',//,
     . 'Usage :   optinum -i=file_in [-o=file_out] [-n]  ',/,
     ./,'  Seule -i est obligatoire et les arguments sont dans un',  
     .' ordre quelconque',/,   
     ./,'  avec',
     ./,'  -i : (obligatoire) permet de donner le nom du fichier gmsh',
     ./,'        initial (sans l''extension .msh)',
     ./,'  -o : (optionnel) permet de donner le nom du fichier sortie',
     ./,'        (sans l''extension .msh)',
     ./,'        Defaut : file_out=file_in (ecrasement)   ',
     ./,'  -n : (optionnel) pas de modification de la numerotation)',
     ./,'        Defaut : optimisation',/,
     ./,'Exemples :',/,
     ./,'         * optinum_gmh -i=cub1 -o=cub1_optim',/,
     ./,'lira le fichier cub1.msh, optimisera sa numerotation et',
     ./,'ecrira sur le fichier cub1_optim.msh une entete et le maillage',
     ./,'renumerote.',/,
     ./,'         * optinum_gmh -i=cub1 -n',/,
     ./,'lira le fichier cub1.msh, le réécrira avec une entete.',/,
     ./,'         * optinum_gmh -i=cub1 -n -o=cub2',/,
     ./,'lira le fichier cub1.msh, le réécrira avec une entete dans',
     ./,'le fichier cub2.msh.',//)
8     format(a80)
9     format(//,'  - Nbr de noeuds              :',i9,/,
     .          '  - Nbr d''objets elementaires  :',i9,/,
     .          '  - Nbr de noeuds par elements :',i9,/,
     .          '  - Nbr d''elements             :',i9,/,
     .          '  - Nbr de dimensions          :',i9)
c      
      end 
c
c*********************************************************************
c
      subroutine readgmsh1 (coord,lobje,lnods,lwork,
     .                      ndime,npoin,nobje,nnode,
     .                      nelem,ncolm)
c
c---- lecture effective du fichier de maillage
c     
c      
      include 'common/precision.f'
c
      dimension coord(3    ,npoin), lobje(ncolm,nobje),
     .          lnods(nnode,nelem), lwork(      npoin)
c
      dimension lnod(40)     
c
      character msh_name_in*80, msh_name_out*80,buf*80
      common / namefiles / msh_name_in, msh_name_out
c       
      imsh = 1
      open(imsh,file=msh_name_in,status = 'old')
c      
10    read(imsh,8,end=20)buf
      if (buf(1:1) .eq. '$') then
c         
         if (buf(2:8) .eq. 'NOD') then      
c
c----       lecture des coordonnees des noeuds
c
            read(imsh,*)npoin
            do ip = 1, npoin
               read(imsh,*)ipp,(coord(id,ip),id=1,ndime)
               lwork(ipp) = ip
            enddo
         endif
c         
         if (buf(2:8) .eq. 'ELM') then               
c
c----       lecture des objets elementaires
c            
            read(imsh,*)nobje
            ie = 0
            do io = 1, nobje
               read(imsh,*)i,itypo,iregp,irego,nnoeud,
     .                    (lnod(j),j=1,nnoeud)
c         
               lobje(1,io) = itypo
               lobje(2,io) = iregp
               lobje(3,io) = irego
               lobje(4,io) = nnoeud
         
               do in = 1, nnoeud
                  ipold = lnod(in)
                  lobje(4+in,io) = lwork(ipold)
               enddo
c         
               if (ndime .eq. 2) then
                  if (itypo .eq. 2 .or. itypo .eq. 3  .or.
     .                itypo .eq. 9 .or. itypo .eq. 10 .or.
     .                itypo .eq. 16) then
                     ie = ie + 1
                     do in = 1, nnoeud
                        lnods(in,ie) = lobje(4+in,io)
                     enddo
                  endif
               endif
c         
               if (ndime .eq. 3) then
                  if (itypo .eq. 4 .or. itypo .eq. 5 .or.
     .                itypo .eq. 6 .or. itypo .eq. 7 .or.
     .                itypo .eq.11 .or. itypo .eq.12 .or.
     .                itypo .eq.13 .or. itypo .eq.14     ) then
                     ie = ie + 1
                     do in = 1, nnoeud
                        lnods(in,ie) = lobje(4+in,io)
                     enddo
                  endif
               endif 
            enddo      
         endif
      endif
c     
      goto 10
20    close(imsh)
c      
      return
c      
1     format(a1)
8     format(a80)
c      
      end
c
c***********************************************************************
c
      subroutine optinum (lnods,jnt  ,joint,jmem ,
     .                    memjt,newjt,ndfro,je   ,
     .                    lorph,
     .                    ifron,nnode,nelem,npoin,
     .                    ndime,ndofn,maxcn      )
c
      include 'common/precision.f'      
c
c---- bandwidth and frontwidth optimization program
c
c note :
c -----
c      this program determines how many nodes are directly connected
c      to a given node and the node numbers of the nodes that are
c      connected to a given node.
c      it is assume that any given node will not be  connected
c      to more than maxcn.
c
c external subroutines :
c ----------------------
c                    .optnum
c
c input/output devices :
c ---------------------
c                    .ier : line printer output device
c
c
c     Source : extrait de Geodyn (J.-P. Vilotte)
c
c----
c
      dimension lnods(nnode,nelem), memjt(maxcn*npoin),
     .          ndfro(nelem      ), jnt  (npoin      ),
     .          joint(npoin      ), jmem (npoin      ),
     .          newjt(npoin      ), je   (nelem      )
     
      dimension lorph(npoin      )
c
      ier = 6
c      
c---- i n i t i a l i z a t i o n
c
      idiff = 0
      call iclear (jmem,npoin)
      call iclear (memjt,maxcn*npoin)
      call iclear (ndfro,nelem)
c
c---- d e t e r m i n e   c o n n e c t i v i t y
c
      do 1500 jelem = 1,nelem
         do 1400 inode = 1,nnode
            jnti = lnods(inode,jelem)
            if(jnti .eq. 0)                                  go to 1500
               jsub = (jnti-1) * maxcn
               do 1300 iinode = 1,nnode
                  if(iinode .eq. inode)                      go to 1300
                     jjt = lnods(iinode,jelem)
                     if(jjt .eq. 0)                          go to 1400
                        mem1 = jmem(jnti)
                        if(mem1 .eq. 0)                      go to 1200
                           do 1100 iii=1,mem1
                              if(memjt(jsub+iii) .eq. jjt)   go to 1300
1100                       continue
1200                    jmem(jnti)             = jmem(jnti) + 1
                        memjt(jsub+jmem(jnti)) = jjt
                        if(iabs(jnti-jjt).gt.idiff) idiff=iabs(jnti-jjt)
1300           continue
1400        continue
1500  continue
c      
c---- reperer les points "orphelins" (c-a-d, n'intervenant pas dans
c     la connectivite)
c
      norph = 0    
      do ipoin = 1, npoin
         if (jmem(ipoin) .eq. 0) then
            norph = norph + 1
            lorph(ipoin) = 1
         else
            lorph(ipoin) = 0
         endif
      enddo    
      write(*,901)norph
c
c---- c o m p u t e   o r i g i n a l   f r o n d w i d t h
c
      kfron = 0
      if(ifron .eq. 0)                                       go to 2700
         do 2500 ipoin = 1,npoin
            kstar = 0
            do 2300 ielem = 1,nelem
               do 2200 inode = 1,nnode
                  if(lnods(inode,ielem) .ne. ipoin)          go to 2200
                     if(kstar .ne. 0)                        go to 2100
                        kstar = ielem
                        ndfro(ielem) = ndfro(ielem) + ndofn
2100                 continue
                     klast = ielem
                     nlast = inode
2200           continue
2300        continue
            if(kstar .eq. 0)                                 go to 2400
               if(klast .lt. nelem)
     .                           ndfro(klast+1) = ndfro(klast+1) - ndofn
                                                             go to 2500
2400        write(*,900) ipoin
2500     nfron = 0
         do 2600 ielem = 1,nelem
            nfron = nfron + ndfro(ielem)
            if(nfron .gt. kfron)                   kfron = nfron
2600     continue
c
c---- print original structure
c
2700  write(*,905) idiff
      if (ifron .ne. 0) write(*,906) kfron
c
c---- c a l l   o p t i m i z a t i o n   s u b r o u t i n e
c
      call optnum (joint,newjt,jmem ,memjt,
     .             jnt  ,ndfro,je   ,lnods,
     .             lorph,
     .             kfron,idiff,ifron,nnode,
     .             nelem,npoin,ndime,ndofn,
     .             maxcn,norph            )
c
      return
c
c---- format
c
900   format(/10x,
     .'e r r o r : a  n o d e  n e v e r  a p p e a r s',//20x,
     .'node :',i5,10x,'never appears')

901   format('  - Nbr de noeuds ''''orphelins'''':',i9)

905   format(////,8x,
     .'O P T I M I S A T I O N  DE   LA  N U M E R O T A T I O N',//,
     .10x,'numerotation initiale (diff. nodale maxi.) :',i10)

906   format(10x,'largeur de bande        :',i10,/)

c
      end
c
c***********************************************************************
c
      subroutine optnum (joint,newjt,jmem ,memjt,
     .                   jnt  ,ndfro,je   ,lnods,
     .                   lorph,
     .                   kfron,idiff,ifron,nnode,
     .                   nelem,npoin,ndime,ndofn,
     .                   maxcn,norph            )
c
      include 'common/precision.f'
c
c
c
c---- node numbering optimisation and element numbering postprocessing
c
c     note :
c     -----
c       .this subroutine trys n renumbering schemes where n is the
c         number of nodes in the model.
c       .an element renumbering option is possible as a postprocessing
c         for frondwidth optimization (ifron .eq. 1)
c
c     external subroutines :
c     --------------------
c                 . clear
c
c
c     input/output devices :
c     ---------------------
c                       .ier : line printer output device
c
c     Ref.: Lawrence L. Durocher, Andrew Gasper, 1979, A versatile two-dimensional mesh generator 
c           with automatic bandwidth reduction, Computers & Structures.
c
c     Source : extrait de Geodyn (J.-P. Vilotte)
c
c----
c
      dimension lnods(nnode,nelem), memjt(maxcn*npoin),
     .          ndfro(nelem      ), jnt  (npoin      ),
     .          joint(npoin      ), jmem (npoin      ),
     .          newjt(npoin      ), je   (nelem      )
 
      dimension lorph(npoin)
c
c---- i n i t i a l i z a t i o n
c
      jrn    = 1
      minmax = idiff
      npreel = npoin - norph  ! nbr de noeuds "effectifs" 
                              ! (c-a-d sans les "orphelins")
c
c---- t r y   n p o i n    r e n u m b e r i n g    s c h e m e s
c
      ntry = min(npoin,max(10,npoin/100))
      
      do 1700 ipoin = 1, ntry
               
         if (lorph(ipoin) .eq. 1) goto 1700
               
         maxd         = 0
         i            = 1
         k            = 1
c
c----    initialize arrays
c
         call iclear(joint,npoin)
         call iclear(newjt,npoin)
         newjt(1)     = ipoin
         joint(ipoin) = 1
c
1000     k4           = jmem(newjt(i))            
         if(k4 .eq. 0)                                     go to 1200
            jsub = (newjt(i)-1) * maxcn
            do 1100 jj = 1,k4
               k5 = memjt(jsub+jj)
               if(joint(k5) .gt. 0)                        go to 1100
                  k         = k+1
                  newjt(k)  = k5
                  joint(k5) = k
                  ndiff     = iabs(i-k)
                  if(ndiff .ge. minmax)                    go to 1700
                     if(ndiff .gt. maxd) maxd = ndiff
1100        continue
            if(k .eq. npreel)                         go to 1300
1200     i = i + 1
                                                           go to 1000
1300     minmax = maxd
         
         ni = 0
         do 1400 jpoin = 1,npoin
            if (lorph(jpoin) .eq. 0) then
               jnt(jpoin) = joint(jpoin)
            else
               ni = ni + 1
               jnt(jpoin) = npreel + ni  ! mettre les pts isoles a la fin
            endif
1400     continue
1700  continue
c
      if(minmax .lt. idiff)                                go to 1900
c
c----    n e w   n u m b e r i n g   k e p t
c
         jrn = 0
         do 1800 jpoin = 1,npoin
            jnt(jpoin) = jpoin
            jmem(jnt(jpoin)) = jpoin
1800     continue
         write(*,100)
                                                           go to 2200
1900  idiff = minmax
c
c---- w r i t e   n e w   n u m b e r i n g
c
      do 2000 ipoin = 1,npoin
2000     jmem(jnt(ipoin)) = ipoin
c      write(*,110)
c      write(*,120) (i,jnt(i),i=1,npoin)
c
c---- i n v e r s e   r e n u m b e r i n g
c
      do 2100 jpoin = 1,npoin
         joint(jpoin)     = npoin-jnt(jpoin) + 1
         jnt(jpoin)       = joint(jpoin)
         jmem(jnt(jpoin)) = jpoin
2100  continue
c      write(*,130)
c      write(*,120) (i,jnt(i),i=1,npoin)
c
c---- r e o r d e r   e l e m e n t   n u m b e r s
c
2200  kfron = 0
      if(ifron .ne. 0)                                    go to 2250
         do 2210  ielem = 1,nelem
            je(ielem) = ielem
2210     continue
                                                          go to 3150
2250  max2 = 0
      do 2400 ielem = 1,nelem
         max1 = 5000
         do 2300 jelem = 1,nelem
            ipoin = jnt(lnods(nnode,jelem))
            if(ipoin .le. max2)                            go to 2300
               if(ipoin .gt. max1)                         go to 2300
                  max1 = ipoin
                  mx1  = jelem
2300     continue
         max2 = max1
         je(ielem) = mx1
2400  continue
c
c---- c o m p u t e   n e w   f r o n t w i d t h
c
      do 3000 ipoin = 1,npoin
         kstar = 0
         do 2900 ielem = 1,nelem
            do 2800 inode = 1,nnode
               if(jnt(lnods(inode,je(ielem))) .ne. ipoin)  go to 2900
                  if(kstar .ne. 0)                         go to 2700
                     kstar        = ielem
                     ndfro(ielem) = ndfro(ielem)+ndofn
2700              continue
                  klast = ielem
                  nlast = inode
2800        continue
2900     continue
         if(klast .lt. nelem)   ndfro(klast+1) = ndfro(klast+1) - ndofn
3000  continue
      nfron = 0
      do 3100 ielem = 1,nelem
         nfron = nfron + ndfro(ielem)
         if(nfron .gt. kfron)      kfron = nfron
3100  continue
c
3150  if(jrn .ne. 0)                                       go to 3200
c         write(*,140) idiff,kfron
         return
3200  write(*,150) idiff
      if (ifron .ne. 0) write(*,160) kfron

      return
c
c---- format
c
c
100   format(/10x,
     .'n u m e r o t a t i o n  i n i t i a l e  r e t e n u e')
110   format(/10x,
     .'n o d a l   r e n u m b e r i n g   a c t i v a t e d',//20x,
     .5(11h  old   new,5x))
120   format(5(2i5,5x))
130   format(/10x,
     .'i n v e r s e   n u m b e r i n g   a c t i v a t e d',//20x,
     .5(11h  old   new,5x))
140   format(//20x,'node difference no renumbering =',i5,/20x,
     .'max frontwidth no renumbering =',i5)
150   format(10x,'nouvelle numerotation (diff. nodale maxi.) :',i10)
160   format(10x,'largeur de bande        :',i10,/)     
c
c----
c
      end
c
c***********************************************************************
c
      subroutine newnum (lobje,lobjn,coord,coorn,
     .                   jmem ,jnt  ,
     .                   nobje,npoin,ncolm      )  
c
      include 'common/precision.f'
c
c---- nouvelles topologie et coordonnees
c
      dimension lobje(ncolm,nobje), coord(3    ,npoin),
     .          lobjn(ncolm,nobje), coorn(3    ,npoin),
     .          jmem (npoin      ), jnt  (      npoin)
c          
      do ip = 1, npoin
c         jp = jmem(ip)
         ipnew = jnt(ip)
         do id = 1, 3
            coorn(id,ipnew) = coord(id,ip)
         enddo
      enddo
c
      do io = 1, nobje
         do i = 1, 4
            lobjn(i,io) = lobje(i,io)
         enddo
         nnod = lobje(4,io)
         do in = 1, nnod
            lobjn(4+in,io) = jnt(lobje(4+in,io))
         enddo
      enddo
c
      return
      end
c
c***********************************************************************
c
      subroutine newmsh (lnods ,lnodn,coord ,coorn ,
     .                   jnt   ,je   ,jmem  ,
     .                   nnode ,nelem,npoin ,ndime ,
     .                   ndofn ,maxcn)
c
      include 'common/precision.f'
c
c---- nouvelles topologie et coordonnees
c
      dimension lnods(nnode,nelem), coord(ndime,npoin),
     .          lnodn(nnode,nelem), coorn(ndime,npoin),
     .          jnt  (npoin      ), jmem (npoin      ),
     .          je   (nelem      )
c
      do 2000 ie = 1, nelem
         iel = je(ie)
         do 1000 in = 1, nnode
            lnodn(in,ie) = jnt(lnods(in,iel))
1000      continue
2000  continue
c
      do 3000 ip = 1, npoin
         jp = jmem(ip)
         coorn(1,ip) = coord(1,jp)
         coorn(2,ip) = coord(2,jp)
3000  continue
c
      return
      end
c
c********************************************************************
c
      function ialloc(nom5,long,ir)
c
c---- alloue l'espace necessaire au stockage du tableau de nom "nom5",
c     de taille "long" et de type "ir" (ir = 1 pour les entiers,
c     ir = 2 pour les reels)
c     Si "iratio" (parametre defini dans le programme principal) a une
c     valeur de 2, les reels sont en double precision. Ils occupent alors
c     deux fois plus de place que les entiers. S'il vaut 1, entiers et
c     reels occupent la meme place (1 mot par reel).
c
      integer pnom,wmax
c
      parameter (maxnum = 100)    ! 100 tableaux peuvent stockes
c
      character*1 type
      character*5 nom,nom5
c
      common / noms     / nom (maxnum)  ! noms des tableaux
      common / pointeur / pnom(maxnum)  ! pointeurs des debuts de tableaux
      common / types    / type(maxnum)  ! types des tableaux ("I" ou "R")
c
      common / numero   / inum          ! nbr actuel de tableaux
      common / wsize    / wmax,iratio
c   
c---- attention : pas d'allocation si long=0
c
      if (long .eq. 0) return
c
      inum = inum + 1                  ! Attribution d'un numero au tableau
      if (inum .eq. 1) pnom(inum) = 1  ! Si c'est le 1er, il pointe en debut de table
c
      nom(inum) = nom5                 ! sauvegarde de son nom
c
      if (ir .eq. 1) then
c
c-----   c'est un tableau d'entiers
c
         icoef = 1
         type (inum) = 'i'
      else
c
c----    c'est un tableau de reels
c
         icoef = iratio
         type (inum) = 'r'
      endif
c
      if (inum+1 .gt. maxnum) then
         write(*,*)
     .   ' in ialloc : nombre de tableaux alloues trop grand : stop'
         stop
      endif
c
      longt = long * icoef   ! longueur effective du tableau dans la table "w"
c
      if (mod(pnom(inum),2) .eq. 0) pnom(inum) = pnom(inum) + 1
c
      pnom(inum+1) = pnom(inum) + longt  ! adresse du debut du prochain tableau
c
      if (pnom(inum+1) .gt. wmax) then
         write(*,*)
     .   ' IN IALLOC : CAPACITE DU VECTEUR CENTRAL W DEPASSEE : STOP'
         write(*,*)'liste des tableaux alloues et leur longueur:'
         do i = 1, inum
            write(*,'(a5,1x,i9)')nom(i),longt
         enddo
         write(*,*)'wmax=',wmax
         stop
      endif
c
      ialloc = pnom(inum) 
c
      return
      end
c
c********************************************************************
c
      subroutine infos (ier)
c
c---- impression de l'etat de la memoire
c
c     ecrit sur le fichier d'unite logique "ier" la liste des tableaux
c     stockes dans la table "w" avec leur types et leur taille et
c     affiche la place restante, disponible dans "w".
c 
      implicit integer(a-z)
      parameter (maxnum=100)
      character*1 type
      character*5 nom
      real*4 percent
c
      common / noms     / nom (maxnum)
      common / pointeur / pnom(maxnum)
      common / types    / type(maxnum)
c
      common / numero   / inum
      common / wsize    / wmax,iratio
c
      write(ier,100)
      write(ier,300)
      nsizt = 0
      do 1000 i=1,inum
         nsize = pnom(i+1)-pnom(i)
         nsizt = nsizt + nsize
         if (type(i) .eq. 'r') then
            nsizr = nsize/iratio
            write(ier,200) i,nom(i),type(i),iratio,nsizr,pnom(i)
         else
            write(ier,200) i,nom(i),type(i),1,nsize,pnom(i)
         endif
1000  continue
      percent = nsizt*100. / wmax
      write(ier,300)
      write(ier,350) 'espace total declare                   ',wmax
      write(ier,300)
      write(ier,351) 'espace total necessite                 ',nsizt,
     .                 '  soit ',percent,' %'
      write(ier,300)
c
      return
100   format (//,35('@')/,'@ FROM INFOS : ETAT DE LA MEMOIRE @'/,35('@')
     .       //,70('=')/,'|NUMERO |',6X,'NOM',6X,'|',3X,'TYPE',3X,'|'
     .       ,5X,'TAILLE',4X,'|',4X,'POINTEUR',5X,'|')
200   format('|',I3,4X,'|',5X,A6,4X,'|',5X,A1,4X,'|',1X,
     .       I1,1X,'X',I8,3X,'|',5X,I8,4X,'|'/,69('-'))
300   format(69('='))

350   format('*',A40,I10,17X,'*')
351   format('*',A40,I10,A7,F5.1,A2,'   *')
c
      end
c
c
c***********************************************************
c
      subroutine iclear (intg,n)
c
      dimension intg(n)
c
      do i = 1, n
         intg(i) = 0.0
      enddo
c
      return
      end
c
c***********************************************************
c
      subroutine clearx (realx,n)
c
      include 'common/precision.f'
c
      dimension realx(n)
c
      do i = 1, n
         realx(i) = 0.0
      enddo
c
      return
      end
c
C**********************************************************************
c
      subroutine icopy (la,lb,n)
c
      dimension la(n), lb(n)
c
      do i = 1, n
         lb(i) = la(i)
      enddo
c
      return
      end
c
C**********************************************************************
c
      subroutine xcopy (xa,xb,n)
c
      include 'common/precision.f'
c
      dimension xa(n), xb(n)
c
      do i = 1, n
         xb(i) = xa(i)
      enddo
c
      return
      end
c
C********************************************************************
c      
      integer function istrlen(st)
      integer           i
      character         st*(*)
      i = len(st)
      do while (st(i:i) .eq. ' ')
        i = i - 1
      enddo
      istrlen = i
      return
      end
