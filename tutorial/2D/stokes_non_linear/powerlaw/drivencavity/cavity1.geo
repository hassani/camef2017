Mesh.MshFileVersion = 1;
c = 1e0;
size   = 0.02*c; // element size



////////    definition of points, lines and face at y = 0  ///////////

                                       //   4        5          3
Point(1) = { 0*c, 0*c,  0*c, size};    //   x---4----x-----3----x     y
Point(2) = { 1*c, 0*c,  0*c, size};    //   |                   |     ^
Point(3) = { 1*c, 1*c,  0*c, size};    //   5   (plan z=0)      2     |
Point(4) = { 0*c, 1*c,  0*c, size};    //   |                   |     |
                                       //   x---------1---------x     |-----> x
                                       //   1                   2

Point(5)={0.5*c,1*c,0*c,size};

Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,5}; Line(4) = {5,4}; Line(5)={4,1}; 

Line Loop(1) = {1,2,3,4,5} ; Plane Surface(1) = {1} ;

