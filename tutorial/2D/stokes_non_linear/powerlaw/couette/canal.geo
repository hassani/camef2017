Mesh.MshFileVersion = 1;  // Toujours utiliser ce format

c = 1e0;
size   = 0.025*c; // element size



////////    definition of points, lines and face at y = 0  ///////////

                                       //   4                   2
Point(1) = { 0*c, -0.5*c,  0*c, size}; //   x--------3----------x     y
Point(2) = { 2*c, -0.5*c,  0*c, size}; //   |                   |     ^
Point(3) = { 2*c,  0.5*c,  0*c, size}; //   4   (plan z=0)      2     |
Point(4) = { 0*c,  0.5*c,  0*c, size}; //   |                   |     |
                                       //   x---------1---------x     |-----> x
                                       //   1                   2

Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; Line(4) = {4,1}; 

Line Loop(1) = {1,2,3,4} ; Plane Surface(1) = {1} ;
Transfinite Surface {1}; Recombine Surface {1};

