clear,close,clc
M=read('couette.1.vnod',-1,5);
E=read('couette.1.velm',-1,9);
x=M(:,1); y=M(:,2); ux=M(:,3); p=M(:,5);

xm=min(x); xM=max(x); l = xM-xm;
ym=min(y); yM=max(y); h = yM-ym;

dp = 2; beta = 1; n=3; C=sqrt(2)/((sqrt(2)*beta)^n)

function [vx] = velo (y)
    vx = C*((dp/l)^n)*((h/2)^(n+1)-y.^(n+1))/(n+1);
endfunction
function [eta] = visco(y)
    eta = ( ( (dp/l)*y )^(1-n) ) / C;
endfunction

yy = linspace(ym,yM,100);



plot(y,ux,'ob')
plot(yy,velo(yy),'-r','linewidth',3)

legend('ux (camef)','ux (exact)')

figure
y=E(:,2); eta=E(:,9);
plot(y,log10(eta),'o',yy,log10(visco(yy)))



