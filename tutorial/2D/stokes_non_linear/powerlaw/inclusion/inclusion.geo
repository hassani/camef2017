Mesh.MshFileVersion = 1;  // Toujours utiliser ce format pour adeli

c=1e3; 

s=5*c; sr=s/10;
a=50*c ; b=50*c; r=2.5*c;
e=r;

p1=1; Point(p1) = {-a, -a, 0,  s};
p2=2; Point(p2) = { a, -a, 0,  s};
p3=3; Point(p3) = {-a,  a, 0,  s};
p4=4; Point(p4) = { a,  a, 0,  s};

p5=5; Point(p5) = {-r,  0, 0,  sr};
p6=6; Point(p6) = { 0, -r, 0,  sr};
p7=7; Point(p7) = { 0,  r, 0,  sr};
p8=8; Point(p8) = { r,  0, 0,  sr};

p9=9; Point(p9) = {0, 0, 0, s};

l1=1; Line(l1) = {p1,p2};
l2=2; Line(l2) = {p2,p4};
l3=3; Line(l3) = {p4,p3};
l4=4; Line(l4) = {p3,p1};

l5=5; Circle(l5) = {p5,p9,p6};
l6=6; Circle(l6) = {p6,p9,p8};
l7=7; Circle(l7) = {p8,p9,p7};
l8=8; Circle(l8) = {p7,p9,p5};

Line Loop(1) = {1,2,3,4,-5,-6,-7,-8}; Plane Surface(1) = {1};
Line Loop(2) = {5,6,7,8}; Plane Surface(2) = {2};



