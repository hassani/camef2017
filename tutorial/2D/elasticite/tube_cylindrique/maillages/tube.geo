Mesh.MshFileVersion = 1;  // Toujours utiliser ce format pour adeli

size   = 0.1; // element size

r1 = 1.0;
r2 = 2.0;

Point(1) = { r1,  0,  0, size/5};
Point(2) = { r2,  0,  0, size};
Point(3) = {  0, r1,  0, size/5};
Point(4) = {  0, r2,  0, size};

Point(5) = {  0,  0,  0, size};

Line(1) = {1,2}; Circle(2) = {2,5,4}; 
Line(3) = {4,3}; Circle(4) = {3,5,1}; 

Line Loop(1) = {1,2,3,4} ; Plane Surface(1) = {1} ;
