clear,close,clc
M=read('tubeP1.1.velml',-1,10);
[n,m] = size(M); 

// x,y,err,ett,ezz,erz,srr,stt,szz,srz
// 1 2 3   4   5   6   7   8   9   10
k=0;
for i=1:n
    if M(i,2) == 0 then
        k = k + 1;
        r(k) = M(i,1);
        sxx(k) = M(i,7);        
        syy(k) = M(i,8);
        szz(k) = M(i,9);
        sxy(k) = M(i,10);        
    end
end

a=min(r);b=max(r);rr=linspace(a,b,100);
p = 1;
function [srr,stt] = theo (r,a,b,p)
    pb = (p*a^2)/(b^2 - a^2);
    srr = pb*(1-(b./r)^2);
    stt = pb*(1+(b./r)^2);
endfunction

[srr,stt] = theo(rr,a,b,p);

//plot(r,sxx,'ob',rr,srr,'-b',r,syy,'or',rr,stt,'-r')
plot(r,sxx,'ob',r,syy,'or',r,szz,'og',r,sxy,'oy')
//legend('srr (camef)','srr (exact)','stt (camef)','stt (exact)')



