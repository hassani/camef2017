Mesh.MshFileVersion = 1;  // Toujours utiliser ce format

c = 1e0;
size   = .1*c; // element size
n=20;



////////    definition of points, lines and face at y = 0  ///////////

Point(1) = { 0*c, 0*c,  0*c, size};    //   4-------------------3     z
Point(2) = { 2*c, 0*c,  0*c, size};    //   |                   |     ^
Point(3) = { 2*c, 1*c,  0*c, size};    //   |   (plan y=0)      |     |
Point(4) = { 0*c, 1*c,  0*c, size};    //   1-------------------2     |-----> x

Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; Line(4) = {4,1}; 

Line Loop(1) = {1,2,3,4} ; Plane Surface(1) = {1} ;



