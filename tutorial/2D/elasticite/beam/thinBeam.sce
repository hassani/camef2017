clear,close,clc

m=read('thinBeam.1.vnod',-1,4);
x=m(:,1); w=m(:,4);

function y = theo(x)
    q=-1e6; E=1e10; v=0.25; h=0.05; L=1; D=(E*h^3)/(12*(1-v^2)); 
    y = ((q*x.^2)/D).*((x.^2)/24 - L*x/6 + (L^2)/4)
endfunction


err = norm(w-theo(x));

xx=linspace(0,1,100); 
plot(x,w,'o',xx,theo(xx),'r','linewidth',10)
legend('Camef','Exact')
title('Uniformly loaded plate embeded at one end (||w-theo|| = '+string(err)+')','fontsize',3)
xlabel('x (m)','fontsize',3); ylabel('deflection (m)','fontsize',3)
xgrid()
