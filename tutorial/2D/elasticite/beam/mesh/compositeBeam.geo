
 Mesh.MshFileVersion = 1;  // Always use this format for camef

 sz = 0.02; // element size
 L  = 4.00; // beam width
 H  = 0.33; // layer thickness 

 ////////  definition of points, lines and face at x3 = 0  ///////////

 Point(1) = { 0, 0  , 0, sz};  
 Point(2) = { L, 0  , 0, sz};  
 Point(3) = { L, H  , 0, sz};  
 Point(4) = { 0, H  , 0, sz};  
 Point(5) = { 0, 2*H, 0, sz};  
 Point(6) = { L, 2*H, 0, sz};  
 Point(7) = { 0, 3*H, 0, sz};  
 Point(8) = { L, 3*H, 0, sz};  
                             

 Line(1) = {1,2}; Line(2) = {2,3}; Line( 3) = {3,4}; Line(4) = {4,1}; 
 Line(5) = {3,6}; Line(6) = {6,5}; Line( 7) = {5,4}; 
 Line(8) = {6,8}; Line(9) = {8,7}; Line(10) = {7,5}; 

 Line Loop(1) = { 1,2,3, 4}; Plane Surface(1) = {1} ;
 Line Loop(2) = {-3,5,6, 7}; Plane Surface(2) = {2} ;
 Line Loop(3) = {-6,8,9,10}; Plane Surface(3) = {3} ;
Transfinite Surface {1}; Recombine Surface {1};
Transfinite Surface {2}; Recombine Surface {2};
Transfinite Surface {3}; Recombine Surface {3};

 Mesh.ElementOrder = 2; // 9-noded quad

