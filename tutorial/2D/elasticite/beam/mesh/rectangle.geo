
 Mesh.MshFileVersion = 1;  // Always use this format for camef

 sz = .1; // element size

 ////////  definition of points, lines and face at x3 = 0  ///////////

 Point(1) = { 0, 0, 0, sz};  // Point4-----Line3-----Point3  x2
 Point(2) = { 4, 0, 0, sz};  //   |                    |     ^
 Point(3) = { 4, 1, 0, sz};  // Line4  (plane x3=0)  Line2   |
 Point(4) = { 0, 1, 0, sz};  //   |                    |     |
                             // Point1-----Line1-----Point2  |-----> x1

 Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; Line(4) = {4,1}; 

 Line Loop(1) = {1,2,3,4}; Plane Surface(1) = {1} ;

 Mesh.ElementOrder = 2; // 6-noded triangles

