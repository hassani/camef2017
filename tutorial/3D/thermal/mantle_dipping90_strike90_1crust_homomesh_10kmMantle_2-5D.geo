Mesh.MshFileVersion = 1;  // Toujours utiliser ce format

c = 1e3;
size   = 10*c; // element size
E = 60 ;


//BLOCK 1  <------------------ $$ UPPER MANTLE CPO1

////////    definition of points, lines and face at y = 0  ///////////

Point(1) = {0*c, 0*c, -80*c, size};    //   4-------------------3     z
Point(2) = {E*c, 0*c, -80*c, size};    // |                   |     ^
Point(3) = {E*c, 0*c, -0*c, size};  // |   (plan y=0)      |     |
Point(4) = {0*c, 0*c, -0*c, size};    // 1-------------------2     |-----> x

Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; Line(4) = {4,1}; 

Line Loop(1) = {1,2,3,4} ; Plane Surface(1) = {1} ;


////////    definition of points, lines and face at y = 10*c  ///////////

L1=140;
//L1=540;
Y1=L1; 
Point(5) = {0*c, Y1*c, -80*c, size};    //   8-------------------7     z
Point(6) = {E*c, Y1*c, -80*c, size};   //  |                   |     ^
Point(7) = {E*c, Y1*c, -0*c, size};  // |   (plan y=300*c)  |     |
Point(8) = {0*c, Y1*c, -0*c, size};    // 5-------------------6     |-----> x

Line(5) = {5,6}; Line(6) = {6,7}; Line(7) = {7,8}; Line(8) = {8,5}; 

Line Loop(2) = {-8,-7,-6,-5} ; Plane Surface(2) = {2} ;


///////   transverse lines and faces   /////////

Line(9) = {1,5}; Line(10) = {2,6}; Line(11) = {3,7}; Line(12) = {4,8};

Line Loop(3) = { 12,  8,  -9, -4} ; Plane Surface(3) = {3} ;  // face x=0??
Line Loop(4) = {  9,  5, -10, -1} ; Plane Surface(4) = {4} ;  // face z=-10*c??
Line Loop(5) = { 10,  6, -11, -2} ; Plane Surface(5) = {5} ;  // face x=40*c????
Line Loop(6) = { 11,  7, -12, -3} ; Plane Surface(6) = {6} ;  // face z=0???????


///////   Definition of the entire volume BLOCK 1  //////////

Surface Loop(1) = {1,2,3,4,5,6}; Volume(1) = {1};




////%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//BLOCK 2   <------------------ $$ UPPER MANTLE CPO2

////////    definition of points, lines and face at y = 400*c  ///////////

L2=20; Y2=Y1+L2;
Point( 9) = {0*c, Y2*c, -80*c, size};    //   12------------------11     z
Point(10) = {E*c, Y2*c, -80*c, size};  //   |                   |     ^
Point(11) = {E*c, Y2*c, -0*c, size};  // |  (plan y=400*c)   |     |
Point(12) = {0*c, Y2*c, -0*c, size};   //  9-------------------10    |-----> x

Line(13) = {9,10}; Line(14) = {10,11}; Line(15) = {11,12}; Line(16) = {12,9}; 

Line Loop(7) = {-16,-15,-14,-13} ; Plane Surface(7) = {7} ;


///////   transverse lines and faces   /////////

Line(17) = {5,9}; Line(18) = {6,10}; Line(19) = {7,11}; Line(20) = {8,12};

Line Loop( 8) = { 20,  16, -17, -8} ; Plane Surface(8) = {8} ;  // face x=0??
Line Loop( 9) = { 17,  13, -18, -5} ; Plane Surface(9) = {9} ;  // face z=-10*c??
Line Loop(10) = { 18,  14, -19, -6} ; Plane Surface(10) = {10} ;  // face x=40*c????
Line Loop(11) = { 19,  15, -20, -7} ; Plane Surface(11) = {11} ;  // face z=0???????


///////   Definition of the entire volume BLOCK 2 //////////

Surface Loop(2) = {-2,7,8,9,10,11}; Volume(2) = {2};




////%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//BLOCK 3  <------------------ $$


////////    definition of points, lines and face at y = 700*c  ///////////

//L3=540;
L3=140; 
Y3=Y2+L3;
Point(13) = {0*c, Y3*c, -80*c, size};    //   16-------------------15     z
Point(14) = {E*c, Y3*c, -80*c, size};    //  |                   |      ^
Point(15) = {E*c, Y3*c, -0*c, size};  //  |   (plan y=700*c)  |      |
Point(16) = {0*c, Y3*c, -0*c, size};    // 13-------------------14     |-----> x

Line(21) = {13,14}; Line(22) = {14,15}; Line(23) = {15,16}; Line(24) = {16,13}; 

Line Loop(12) = {-24,-23,-22,-21} ; Plane Surface(12) = {12} ;


///////   transverse lines and faces   /////////

Line(25) = {9,13}; Line(26) = {10,14}; Line(27) = {11,15}; Line(28) = {12,16};

Line Loop(13) = { 28,  24, -25, -16} ; Plane Surface(13) = {13} ;  // face x=0??
Line Loop(14) = { 25,  21, -26, -13} ; Plane Surface(14) = {14} ;  // face z=-10*c??
Line Loop(15) = { 26,  22, -27, -14} ; Plane Surface(15) = {15} ;  // face x=40*c????
Line Loop(16) = { 27,  23, -28, -15} ; Plane Surface(16) = {16} ;  // face z=0???????


///////   Definition of the entire volume BLOCK 3 //////////

Surface Loop(3) = {-7,12,13,14,15,16}; Volume(3) = {3};

//BLOCK 4   <------------------ $$ LOWER CRUST (NO CPO)


