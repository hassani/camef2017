Mesh.MshFileVersion = 1;  // Toujours utiliser ce format 

c = 1;
size   = .1*c; // element size



////////    definition of points, lines and face at y = 0  ///////////

Point(1) = { 0*c, 0*c,  -1*c, size};    //   4-------------------3     z
Point(2) = { 5*c, 0*c,  -1*c, size};    //   |                   |     ^
Point(3) = { 5*c, 0*c,   0*c, size};    //   |   (plan y=0)      |     |
Point(4) = { 0*c, 0*c,   0*c, size};    //   1-------------------2     |-----> x

Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; Line(4) = {4,1}; 

Line Loop(1) = {1,2,3,4} ; Plane Surface(11) = {1} ;


////////    definition of points, lines and face at y = 1*c  ///////////

Point(5) = { 0*c, 1*c,  -1*c, size};    //   8-------------------7     z
Point(6) = { 5*c, 1*c,  -1*c, size};    //   |                   |     ^
Point(7) = { 5*c, 1*c,   0*c, size};    //   |   (plan y=1*c)    |     |
Point(8) = { 0*c, 1*c,   0*c, size};    //   5-------------------6     |-----> x

Line(5) = {5,6}; Line(6) = {6,7}; Line(7) = {7,8}; Line(8) = {8,5}; 

Line Loop(2) = {-8,-7,-6,-5} ; Plane Surface(12) = {2} ;



///////   transverse lines and faces   /////////


Line(9) = {1,5}; Line(10) = {2,6}; Line(11) = {3,7}; Line(12) = {4,8};

Line Loop(3) = { 12,  8,  -9, -4} ; Plane Surface(13) = {3} ;  // face x=0
Line Loop(4) = {  9,  5, -10, -1} ; Plane Surface(14) = {4} ;  // face z=-10*c
Line Loop(5) = { 10,  6, -11, -2} ; Plane Surface(15) = {5} ;  // face x=40*c
Line Loop(6) = { 11,  7, -12, -3} ; Plane Surface(16) = {6} ;  // face z=0


///////   Definition of the entire volume  //////////

Surface Loop(1) = {11,12,13,14,15,16}; Volume(1) = {1};
