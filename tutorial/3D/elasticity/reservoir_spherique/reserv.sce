clear,close,clc
M=read('fort.2',-1,7);
[n,m] = size(M); 

r=M(:,1); sxx=M(:,2); syy=M(:,3); szz=M(:,4);

a=min(r);b=max(r);rr=linspace(a,b,100);
p = 1;
function [srr,stt] = theo (r,a,b,p)
    pb = (p*a^3)/(b^3 - a^3);
    srr = pb*(1-(b./r)^3);
    stt = pb*(1+0.5*(b./r)^3);
endfunction

[srr,stt] = theo(rr,a,b,p);

err = norm(sxx-theo(r,a,b,p))

plot(r,sxx,'ob',rr,srr,'-b',  r,syy,'or',rr,stt,'-r',  'linewidth',3)

legend('srr (camef)','srr (exact)','stt (camef)','stt (exact)')



