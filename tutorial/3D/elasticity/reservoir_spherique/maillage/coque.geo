Mesh.MshFileVersion = 1;  

r1 = 1.0; r2 = 2.0; size = 0.05; // Radius and element size
Point(1) = { r1,  0,  0, size/5}; Point(2) = { r2,  0,  0, size};
Point(3) = {  0,  0, r1, size/5}; Point(4) = {  0,  0, r2, size};
Point(5) = {  0, r1,  0, size/5}; Point(6) = {  0, r2,  0, size};

Point(7) = {  0,  0,  0, size};

Line(1) = {1,2}; Circle(2) = {2,7,4}; 
Line(3) = {4,3}; Circle(4) = {3,7,1}; 
Line(5) = {5,6}; Circle(6) = {6,7,4};

Circle(7) = {3,7,5};
Circle(8) = {1,7,5}; 
Circle(9) = {2,7,6};

Line Loop(1) = { 1, 2, 3, 4}; Plane Surface(1) = {1} ;
Line Loop(2) = {-5,-6,-3,-7}; Plane Surface(2) = {2} ;
Line Loop(3) = { 8, 5,-9,-1}; Plane Surface(3) = {3};

Line Loop(4) = { 7,-8,-4}; Ruled Surface(4) = {4};
Line Loop(5) = { 6,-2, 9}; Ruled Surface(5) = {5};

Surface Loop(1) = {1,2,3,4,5}; Volume(1) = {1};
