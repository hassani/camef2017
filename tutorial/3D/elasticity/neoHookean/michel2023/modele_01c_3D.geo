
Mesh.MshFileVersion = 1;  
//Mesh.ElementOrder=2;

// maillage dependant de la distance aux surfaces critiques
// sizemin = taille à proximité de ces surfaces
// sizemax = taille en champ lointain
// evolution lineaire de la taille entre distmin et distmax

h = 0.8; //0.4

sizemin = h;
sizemax = h*5.0;
distmin = 1;
distmax = 1;

pi = 3.14159265;

withcollim=0;
If ( withcollim > 0 ) 
  SetFactory('OpenCASCADE');
EndIf

cascade=0;
If ( cascade > 0 ) 
  SetFactory('OpenCASCADE');
EndIf

///////////////////////////////////
// parametres geometriques
//
// TT: épaisseur des languettes (L3=L4 de Martin)
// AA: angle (pas d'info explicite de Martin)
// Ti: épaisseur de la jonction entre bras latéral et bouton poussoir (L1 de Martin)
// Tb: épaisseur de la jonction entre la base et le bras latéral (L2 de Martin)
// zthick: profondeur de l'instrument [z]
// L4: position(y) du poincon par rapport à la base (H6 de Martin)
// LL: longueur de jonction des bras  (y compris les rayons des extremites) (I6=I4 de Martin)
// ath2: epaisseur des bras (H3 de Martin)
// Ri et Rb: rayons pour la jonction a l'input (Ri) et a la base (Rb) (rayon std)
// L1: longueur(x) de la base (I8+0.5*I9 de Martin)
// L2: epaisseur(y) de la base (H2 de Martin)
// L3: epaisseur(x) de la branche laterale 
//     pour le moment je la prends uniforme de haut en bas et égale à I3
//     mais ça varie de I2 à I3 puis I10?
// L4: position(y) du poincon par rapport à la base
// L5: longueur(x) du poincon (I1 de Martin)
// L6: hauteur(y) du poincon (H7 de Martin)
// L7: hauteur(y) de la branche laterale (rayon + H4 + H5 + ?? de Martin)
// L8: longueur de la partie centrale épaissie de la base (0.5*I9 de Martin)
// L9: epaisseur de la partie centrale épaissie de la base (H11 de Martin)
// aL2: epaisseur(x) de la masse centrale (0.5*I7 de Martin)
// aL3: hauteur(y) de la masse centrale (H8 + H9 + L3 ? de Martin)

size=h;

// rayon de fraisage (1/16 inch)
rayon=1.59;

// prms de Martin

MI1=14.95;
MI2=7.0;
MI3=9.1;
MI4=9.95;
MI5=25.7;
MI6=MI4;
MI7=7.1;
MI8=39.9;
MI9=18.05;
//MI10=??;

MH1=6.5;
MH2=9.25;
MH3=6.2;
MH4=1.55;
MH5=21.90;
MH6=8.3;
MH7=5.1;
MH8=2.9;
MH9=4.5;
MH10=15.6;
MH11=13.15;

ML1=0.9;
ML2=1.35;
ML3=0.45;
ML4=0.45;

// d'où ce qui suit

TT=ML3;
AA=1.0;
Ti=ML1;
Tb=ML2;

zthick=5;

ybase=0;
L4=MH6;
LL=MI4;
ath2=MH3;
Ri=rayon;
Rb=rayon;

// dc: decalage pour arc de cercle
dc=rayon;

L1 = MI8+0.5*MI9+2*rayon+MI2;    
L2 = MH2;    
L3 = MI3;
L5 = MI1;
L6 = MH7;

// decalage du bras de la partie haute de la barre latérale ?????????
decy = 5.0;
L7 = rayon + MH4 + MH5 + decy;
L8 = 0.5*MI9;
L9 = MH11;

// Masse centrale
aL2 = 0.5*MI7;
aL3 = MH8 + MH9 + ML3;

// dL7: hauteur au dessus du bloc lateral de la partie haute de la masse centrale
dL7 = 1.0;

// ad1: décalage du centre du 1er bras par rapport au bord sup
// marge: marge a respecter avec le bord sup
ad1=1.0;
marge=0.5;

  
///////////////////////////////////////////////////////
Macro FindCenterCircle

  xm = 0.5*(xc1a+xc1b);
  ym = 0.5*(yc1a+yc1b);

  a = (xc1b-xc1a)^2+(yc1b-yc1a)^2;
  b = xc1a*(yc1b-yc1a)^2;
  b = b + xm*(xc1b-xc1a)^2;
  b = b + (ym-yc1a)*(xc1b-xc1a)*(yc1b-yc1a);
  b = (-2.0)*b;
  c = (xc1a)^2*(yc1b-yc1a)^2;
  c = c + (ym-yc1a)^2*(yc1b-yc1a)^2;
  c = c + 2*(ym-yc1a)*(yc1b-yc1a)*(xc1b-xc1a)*xm;
  c = c + xm^2*(xc1b-xc1a)^2;
  c = c - rayon^2*(yc1b-yc1a)^2;

  delta = b*b-4.0*a*c;
  xx = ((-1.0)*b + signe * Sqrt(delta))/(2.0*a);
  yy = ym + (xm - xx)*(xc1b-xc1a)/(yc1b-yc1a);

Return
///////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// a partir d'ici pas de valeur numerique explicite a fournir
//
// je convertis les valeurs de distance
// (definies precedemment en mm) en m !
////////////////////////////////////////////////////////////////

fac = 0.001 ;

TT = TT*fac;
LL = LL*fac;
Ti = Ti*fac;
Tb = Tb*fac;
Ri = Ri*fac;
Rb = Rb*fac;
dc = dc*fac;
rayon = rayon*fac;
L1 = L1*fac;    
L2 = L2*fac;    
L3 = L3*fac;
L4 = L4*fac;
L5 = L5*fac;
L6 = L6*fac;
L7 = L7*fac;
L8 = L8*fac;
L9 = L9*fac;
ad1 = ad1*fac;
marge = marge*fac;
aL2 = aL2*fac;
aL3 = aL3*fac;
dL7 = dL7*fac;
ath2 = ath2*fac;
zthick = zthick*fac;

// z pour faces avant et arrière
zfront=zthick;  
zback=0.0;  


////////////////////////////////////////////////////////////////
// variables secondaires
////////////////////////////////////////////////////////////////

// arm junction thickness and length 
ath1 = TT;
aL4 = LL;

// arms tilt
angd = AA;
angr = angd*Pi/180.;

// epaisseur des encoches
// d2: entre base et partie laterale
// d3: entre partie laterale et poincon
d2 = 2.0*Rb;
d3 = 2.0*Ri;

d1 = Tb;
d4 = Ti;

// arm length
// ath2: arm thickness 
// d: gap between arms (perpendicular to arms)
aL1 = (L1 - L3 - aL2)/Cos(angr) - 2*LL;

// la base en y=0 
ybase=0.0;

////////////////////////////////////////////////////////////////

For k In {1:2:1}

  If ( k == 1 ) 
    ///////////////////////////////////////////////////////////////
    ////                       FACE AVANT
    ///////////////////////////////////////////////////////////////

    z=zfront;  
    numfac=100;
    numcircles=800;
    Printf("Face avant");

    // on affiche les coordonnees des noeuds de la face avant
    printcoord=1;
  ElseIf ( k == 2 )
    ///////////////////////////////////////////////////////////////
    ////                       FACE ARRIERE
    ///////////////////////////////////////////////////////////////

    z=zback;  
    numfac=200; 
    numcircles=900; 
    Printf("Face arriere");
    
    // on n'affiche pas les coordonnees des noeuds de la face arriere    
    printcoord=0;    
  Else
    Exit
  EndIf


  // Base
  num=numfac+1;
  coord[0]=0.0;
  coord[1]=ybase;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf
  Point(num) = { coord[0],coord[1], z};
  
  num=numfac+2;  
  coord[0]=L1-L3-d2;
  coord[1]=ybase;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};
  
  num=numfac+3;  
  coord[0]=L1-L3-Rb;
  coord[1]=ybase+Rb;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};  
  
  num=numfac+4;
  coord[0]=L1-L3;
  coord[1]=ybase;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};  

  ////////////////////////////////////
  // arc de cercle entre 2 et 3
  xc1a = L1-L3-d2;
  yc1a = ybase;
  xc1b = L1-L3-Rb;
  yc1b = ybase+Rb;

  rr=Rb;
  signe = (1.0);
  Call FindCenterCircle;
  
  num=numcircles+1;  
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  ////////////////////////////////////
  // arc de cercle entre 3 et 4
  xc1a = L1-L3-Rb;
  yc1a = ybase+Rb;
  xc1b = L1-L3;
  yc1b = ybase;

  rr=Rb;
  signe = (-1.0);
  Call FindCenterCircle;

  num=numcircles+2;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   
  ////////////////////////////////////
  
  num=numfac+5;
  coord[0]=L1;
  coord[1]=ybase;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    

  
  // Poincon
  num=numfac+6;
  coord[0]=L1;
  coord[1]=ybase+L4-0.5*d4-Ri;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};  
  
  num=numfac+7; 
  coord[0]=L1+Ri;
  coord[1]=ybase+L4-0.5*d4;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    

  num=numfac+8; 
  coord[0]=L1+d3;
  coord[1]=ybase+L4-0.5*d4-Ri;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  ////////////////////////////////////
  // arc de cercle entre 6 et 7
  xc1a = L1;
  yc1a = ybase+L4-0.5*d4-Ri;
  xc1b = L1+Ri;
  yc1b = ybase+L4-0.5*d4;

  rr=Ri;
  signe = (1.0);
  Call FindCenterCircle;
  num=numcircles+3;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  // arc de cercle entre 7 et 8
  xc1a = L1+Ri;
  yc1a = ybase+L4-0.5*d4;
  xc1b = L1+d3;
  yc1b = ybase+L4-0.5*d4-Ri;

  rr=Ri;
  signe = (-1.0);
  Call FindCenterCircle;
  num=numcircles+4;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  ////////////////////////////////////
  
  num=numfac+9;
  coord[0]=L1+d3;
  coord[1]=ybase+L4-0.5*L6;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};      

  num=numfac+10;
  coord[0]=L1+d3+L5;
  coord[1]=ybase+L4-0.5*L6;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  num=numfac+11;
  coord[0]=L1+d3+L5;
  coord[1]=ybase+L4+0.5*L6;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    
  
  num=numfac+12;
  coord[0]=L1+d3;
  coord[1]=ybase+L4+0.5*L6;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    
  
  num=numfac+13;
  coord[0]=L1+d3;
  coord[1]=ybase+L4+0.5*d4+Ri;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    
  
  num=numfac+14;
  coord[0]=L1+Ri;
  coord[1]=ybase+L4+0.5*d4;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    
  
  num=numfac+15;
  coord[0]=L1;
  coord[1]=ybase+L4+0.5*d4+Ri;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    

  ////////////////////////////////////
  // arc de cercle entre 13 et 14
  xc1a = L1+d3;
  yc1a = ybase+L4+0.5*d4+Ri;
  xc1b = L1+Ri;
  yc1b = ybase+L4+0.5*d4;

  rr=Ri;
  signe = (-1.0);
  Call FindCenterCircle;
  num=numcircles+5;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  // arc de cercle entre 14 et 15
  xc1a = L1+Ri;
  yc1a = ybase+L4+0.5*d4;
  xc1b = L1;
  yc1b = ybase+L4+0.5*d4+Ri;

  rr=Ri;
  signe = (1.0);
  Call FindCenterCircle;
  num=numcircles+6;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  num=numfac+16;  
  coord[0]=L1;
  coord[1]=ybase+L7;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     
  
  num=numfac+17;  
  coord[0]=L1-L3;
  coord[1]=ybase+L7;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  // test du decalage du haut
  // test: distance entre la partie haute de l'instrument et la 
  // position du point de construction du cercle le plus haut
  // Si on a moins que la marge definie en entete alors on ajuste
  // le decalage ad1
  test = ad1-0.5*ath1/Cos(angr)-dc;
  If ( test <= marge )
    ad1 = 0.5*ath1/Cos(angr)+dc+marge;
  EndIf

  // Bras sup. Partie haute

  // l'intersection du centre de la jonction du haut avec la barre 
  // laterale se trouve en ybase+L7-ad1
  // Ici c'est la meme chose mais pour la partie haute de la 
  // jonction
  xatt1a = L1-L3;
  yatt1a = ybase+L7-ad1+0.5*ath1/Cos(angr);

  ////////////////////////////////////
  // arc de cercle 1
  // On se decale de dc ans les directions des 2 parties
  //    verticalement pour la barre laterale
  //    obliquement pour la jonction
  xc1a = xatt1a;
  yc1a = yatt1a+dc;
  xc1b = xatt1a - dc*Cos(angr);
  yc1b = yatt1a - dc*Sin(angr);

  num=numfac+18;
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    

  num=numfac+19; 
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  signe = (-1.0);
  Call FindCenterCircle;

  num=numcircles+7;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   
  
  ////////////////////////////////////

  // intersection de la partie haute de la jonction avec la partie
  // epaisse du bras
  xatt1b = xatt1a - aL4*Cos(angr);
  yatt1b = yatt1a - aL4*Sin(angr);

  ////////////////////////////////////
  // arc de cercle 2
  xc1a = xatt1b + dc*Cos(angr);
  yc1a = yatt1b + dc*Sin(angr);
  xc1b = xatt1b - dc*Sin(angr);
  yc1b = yatt1b + dc*Cos(angr);

  num=numfac+20;
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};      

  num=numfac+21;
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};      

  signe = (1.0);
  Call FindCenterCircle;

  num=numcircles+8;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  ////////////////////////////////////

  // suite des points sans arc de cercle
  x2 = xatt1b - 0.5*(ath2-ath1) * Sin(angr) ;
  y2 = yatt1b + 0.5*(ath2-ath1) * Cos(angr) ;
  x3 = x2 - aL1*Cos(angr);
  y3 = y2 - aL1*Sin(angr);

  num=numfac+22;
  coord[0]=x2;
  coord[1]=y2;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   
  
  num=numfac+23;    
  coord[0]=x3;
  coord[1]=y3;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  // nouvelle attache
  xatt1c = x3 + 0.5*(ath2-ath1) * Sin(angr) ;
  yatt1c = y3 - 0.5*(ath2-ath1) * Cos(angr) ;
 
  ////////////////////////////////////
  // arc de cercle 3
  xc1a = xatt1c - dc*Sin(angr);
  yc1a = yatt1c + dc*Cos(angr);
  xc1b = xatt1c - dc*Cos(angr);
  yc1b = yatt1c - dc*Sin(angr);

  num=numfac+24;  
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  num=numfac+25;
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  signe = (-1.0);
  Call FindCenterCircle;

  num=numcircles+9;
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   
  
  ////////////////////////////////////

  // nouvelle attache
  xatt1d = aL2;
  yatt1d = yatt1c - (xatt1c - xatt1d) * Tan(angr) ;
 
  ////////////////////////////////////
  // arc de cercle 4
  xc1a = xatt1d + dc*Cos(angr);
  yc1a = yatt1d + dc*Sin(angr);
  xc1b = xatt1d;
  yc1b = yatt1d + dc;

  num=numfac+26; 
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};       

  num=numfac+27;  
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};       

  signe = (1.0);
  Call FindCenterCircle;

  num=numcircles+10;  
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};  
  
  ////////////////////////////////////
  // les attaches et les points pour la partie basse du 1er bras

  xatt2a = xatt1a;
  yatt2a = yatt1a - ath1/Cos(angr);

  xatt2b = xatt2a - aL4*Cos(angr);
  yatt2b = yatt2a - aL4*Sin(angr);

  x13 = xatt2b + 0.5*(ath2-ath1) * Sin(angr) ;
  y13 = yatt2b - 0.5*(ath2-ath1) * Cos(angr) ;
  x12 = x13 - aL1*Cos(angr);
  y12 = y13 - aL1*Sin(angr);

  xatt2c = x12 - 0.5*(ath2-ath1) * Sin(angr) ;
  yatt2c = y12 + 0.5*(ath2-ath1) * Cos(angr) ;
  xatt2d = aL2;
  yatt2d = yatt2c - (xatt2c - xatt2d) * Tan(angr) ;

  // fin de determintion des points utiles sur les bords 
  // des bras
  // on reprend la ou on en etait reste

//  ycur = ybase + L7 + dL7;
  ycur = yc1b + MH9*fac - rayon;
  xcur = aL2;
  num=numfac+28;  
  coord[0]=xcur;
  coord[1]=ycur;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  xcur = 0.0;
  num=numfac+29; 
  coord[0]=xcur;
  coord[1]=ycur;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  ycur = ybase + L7 - aL3;
  num=numfac+30;  
  coord[0]=xcur;
  coord[1]=ycur;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  xcur = aL2;
  num=numfac+31;  
  coord[0]=xcur;
  coord[1]=ycur;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////

  // arc de cercle 5-hole
  // coin gauche de la partie basse du bras sup
  xc1a = xatt2d ;
  yc1a = yatt2d - dc;
  xc1b = xatt2d + dc*Cos(angr);
  yc1b = yatt2d + dc*Sin(angr);

  num=numfac+32;
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  num=numfac+33;  
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  signe = (1.0);
  Call FindCenterCircle;

  num=numcircles+11;  
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////

  ////////////////////////////////////
  // arc de cercle 6-hole
  // on poursuit vers la droite la partie basse du bras sup
  xc1a = xatt2c - dc*Cos(angr);
  yc1a = yatt2c - dc*Sin(angr);
  xc1b = xatt2c + dc*Sin(angr);
  yc1b = yatt2c - dc*Cos(angr);

  num=numfac+34; 
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    

  num=numfac+35;  
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    

  signe = (-1.0);
  Call FindCenterCircle;

  num=numcircles+12;  
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  // suite

  num=numfac+36; 
  coord[0]=x12;
  coord[1]=y12;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  num=numfac+37;    
  coord[0]=x13;
  coord[1]=y13;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  ////////////////////////////////////
  // arc de cercle 7-hole
  xc1a = xatt2b + dc*Sin(angr);
  yc1a = yatt2b - dc*Cos(angr);
  xc1b = xatt2b + dc*Cos(angr);
  yc1b = yatt2b + dc*Sin(angr);

  num=numfac+38;    
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  num=numfac+39;   
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};     

  signe = (1.0);
  Call FindCenterCircle;

  num=numcircles+13;    
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////

  ////////////////////////////////////
  // arc de cercle 8-hole
  // coin droit de la partie basse du bras sup

  xc1a = xatt2a - dc*Cos(angr);
  yc1a = yatt2a - dc*Sin(angr);
  xc1b = xatt2a;
  yc1b = yatt2a - dc;

  num=numfac+40;    
  coord[0]=xc1a;
  coord[1]=yc1a;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};
  
  num=numfac+41;    
  coord[0]=xc1b;
  coord[1]=yc1b;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  signe = (-1.0);
  Call FindCenterCircle;

  num=numcircles+14;    
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  // on rejoint la barre laterale puis la base

  num=numfac+42;  
  coord[0]=L1-L3;
  coord[1]=ybase+Tb+2*Rb;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  num=numfac+43;    
  coord[0]=L1-L3-Rb;
  coord[1]=ybase+Tb+Rb;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   
  
  num=numfac+44;    
  coord[0]=L1-L3-d2;
  coord[1]=ybase+Tb+2*Rb;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   

  ////////////////////////////////////
  // arc de cercle entre 42 et 43
  xc1a = L1-L3;
  yc1a = ybase+Tb+2*Rb;
  xc1b = L1-L3-Rb;
  yc1b = ybase+Tb+Rb;

  rr=Rb;
  signe = (-1.0);
  Call FindCenterCircle;

  num=numcircles+15;    
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  // arc de cercle entre 43 et 44
  xc1a = L1-L3-Rb;
  yc1a = ybase+Tb+Rb;
  xc1b = L1-L3-d2;
  yc1b = ybase+Tb+2*Rb;

  rr=Rb;
  signe = (1.0);
  Call FindCenterCircle;

  num=numcircles+16;    
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////

  num=numfac+45;  
  coord[0]=L1-L3-d2;
  coord[1]=ybase+L2;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};  

  num=numfac+46;    
  coord[0]=L8;
  coord[1]=ybase+L2;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};  
  
  num=numfac+47;  
  coord[0]=L8-rayon;
  coord[1]=ybase+L2+rayon;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};   
  
  num=numfac+48;  
  coord[0]=L8;
  coord[1]=ybase+L2+2*rayon;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  // arc de cercle entre 46 et 47
  xc1a = L8;
  yc1a = ybase+L2;
  xc1b = L8-rayon;
  yc1b = ybase+L2+rayon;

  rr=rayon;
  signe = (1.0);
  Call FindCenterCircle;

  num=numcircles+17;    
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ////////////////////////////////////
  // arc de cercle entre 47 et 48
  xc1a = L8-rayon;
  yc1a = ybase+L2+rayon;
  xc1b = L8;
  yc1b = ybase+L2+2*rayon;

  rr=rayon;
  signe = (1.0);
  Call FindCenterCircle;

  num=numcircles+18;    
  coord[0]=xx;
  coord[1]=yy;  
  If ( printcoord == 1 ) 
    Printf("Centre cercle %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z}; 
  
  ///
  num=numfac+49; 
  coord[0]=L8;
  coord[1]=ybase+L9;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};
  
  num=numfac+50;    
  coord[0]=0.0;
  coord[1]=ybase+L9;  
  If ( printcoord == 1 ) 
    Printf("Noeud %4.0f : %f /// %f",num,coord[0],coord[1]);
  EndIf  
  Point(num) = { coord[0],coord[1], z};    

EndFor  
///////////////////////////////////////////////////////

// Arcs and Lines for front
Printf("Arcs et lignes pour Face avant");
// anciennement Line et Circle numerotes en 100+k
Line (1) = {101,102};
Circle(2) = {102,801,103}; 
Circle(3) = {103,802,104}; 
Line (4) = {104,105};
Line (5) = {105,106};
Circle (6) = {106,803,107};
Circle (7) = {107,804,108};
Line (8) = {108,109};
Line (9) = {109,110};
Line (10) = {110,111};
Line (11) = {111,112};
Line (12) = {112,113};
Circle (13) = {113,805,114};
Circle (14) = {114,806,115};
Line (15) = {115,116};
Line (16) = {116,117};
Line (17) = {117,118};
Circle (18) = {118,807,119};
Line (19) = {119,120};
Circle (20) = {120,808,121};
Line (21) = {121,122};
Line (22) = {122,123};
Line (23) = {123,124};
Circle (24) = {124,809,125};
Line (25) = {125,126};
Circle (26) = {126,810,127};
Line (27) = {127,128};
Line (28) = {128,129};
Line (29) = {129,130};
Line (30) = {130,131};
Line (31) = {131,132};
Circle (32) = {132,811,133};
Line (33) = {133,134};
Circle (34) = {134,812,135};
Line (35) = {135,136};
Line (36) = {136,137};
Line (37) = {137,138};
Circle (38) = {138,813,139};
Line (39) = {139,140};
Circle (40) = {140,814,141};
Line (41) = {141,142};
Circle (42) = {142,815,143};
Circle (43) = {143,816,144};
Line (44) = {144,145};
Line (45) = {145,146};
Circle (46) = {146,817,147};
Circle (47) = {147,818,148};
Line (48) = {148,149};
Line (49) = {149,150};
Line (50) = {150,101};


// Arcs and Lines for back
Printf("Arcs et lignes pour Face arriere");

Line (201) = {201,202};
Circle(202) = {202,901,203}; 
Circle(203) = {203,902,204}; 
Line (204) = {204,205};
Line (205) = {205,206};
Circle (206) = {206,903,207};
Circle (207) = {207,904,208};
Line (208) = {208,209};
Line (209) = {209,210};
Line (210) = {210,211};
Line (211) = {211,212};
Line (212) = {212,213};
Circle (213) = {213,905,214};
Circle (214) = {214,906,215};
Line (215) = {215,216};
Line (216) = {216,217};
Line (217) = {217,218};
Circle (218) = {218,907,219};
Line (219) = {219,220};
Circle (220) = {220,908,221};
Line (221) = {221,222};
Line (222) = {222,223};
Line (223) = {223,224};
Circle (224) = {224,909,225};
Line (225) = {225,226};
Circle (226) = {226,910,227};
Line (227) = {227,228};
Line (228) = {228,229};
Line (229) = {229,230};
Line (230) = {230,231};
Line (231) = {231,232};
Circle (232) = {232,911,233};
Line (233) = {233,234};
Circle (234) = {234,912,235};
Line (235) = {235,236};
Line (236) = {236,237};
Line (237) = {237,238};
Circle (238) = {238,913,239};
Line (239) = {239,240};
Circle (240) = {240,914,241};
Line (241) = {241,242};
Circle (242) = {242,915,243};
Circle (243) = {243,916,244};
Line (244) = {244,245};
Line (245) = {245,246};
Circle (246) = {246,917,247};
Circle (247) = {247,918,248};
Line (248) = {248,249};
Line (249) = {249,250};
Line (250) = {250,201};

// Traverse lines
Printf("Lignes transverses");

For k In {1:50:1}

  num1=100+k;
  num2=200+k;
  num=900+k;
  
  Line (num) = {num1,num2};
    
EndFor

// Contours for front
Printf("Surface Face avant");

Line Loop(101) = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50} ; 

Plane Surface(101) = {101} ;
Surface Loop(101) = {101};

// Contours for back
Printf("Surface Face arriere");

Line Loop(102) = {201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250} ; 

Plane Surface(102) = {102} ;
Surface Loop(102) = {102};

// Contours for thickness
Printf("Line loops transverses");
For k In {1:49:1}

  num=k;
  num1=k;
  num2=900+k+1;
  num3=-200-k;
  num4=-900-k;
  
  Line Loop (num) = {num1,num2,num3,num4};
  Printf("Line Loop... %4.0f : %4.0f %4.0f %4.0f %4.0f",k,num1,num2,num3,num4);
    
EndFor
Line Loop (50) = {50,901,-250,-950};

Printf("Surface transverses");
For k In {1:22:1}

  Printf("... %4.0f",k);
  num=k;

  // Surface Loop(num) = {num};
  Surface(num) = {num} ;
    
EndFor

k=23;
Printf("... %4.0f",k);
Plane Surface(23) = {23} ;

For k In {24:47:1}

  Printf("... %4.0f",k);
  num=k;

  // Surface Loop(num) = {num};
  Surface(num) = {num} ;
    
EndFor

k=48;
Printf("... %4.0f",k);
Plane Surface(48) = {48} ;

For k In {49:50:1}

  Printf("... %4.0f",k);
  num=k;

  // Surface Loop(num) = {num};
  Surface(num) = {num} ;
    
EndFor

Surface Loop(1) = {101,102,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50};


Volume (1) = {1};

If ( withcollim > 0 ) 


  //// cylindre tangent à la base
  //// xc, yc, zc: point du centre du cylindre
  //// xv, yv, zv: vecteur orientation du cylindre
  //// R: rayon
  //// alpha: [option] ouverture d'angle du cylindre
  //
  //// inclinaison du cylindre par rapport à la verticale
  
  angle = 22*pi/180;

  length=12*0.001;
  //length=20;

  R=5.5*0.001;
  xc=0;
  yc=R*Sin(angle);
  zc=zthick+R*Cos(angle);
  xv=0;
  yv=length*Cos(angle);
  zv=(-1.0)*length*Sin(angle);

  Cylinder(2) = {xc, yc, zc, xv, yv, zv, R};

  BooleanDifference{ Volume{1}; Delete;}{ Volume{2}; Delete;}
EndIf


Field[1] = Distance;
Field[1].SurfacesList = {2,3,6,7,13,14,18,19,20,24,25,26,32,33,34,38,39,40,42,43,46,47,48};
Field[1].Sampling = 1000;

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = sizemin*fac;
Field[2].LcMax = sizemax*fac;
Field[2].DistMin = distmin*fac;
Field[2].DistMax = distmax*fac;
Background Field = 2;
