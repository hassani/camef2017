Mesh.MshFileVersion = 1;  // Toujours utiliser ce format pour adeli

c = 1;
lc = 350*c;

L = 0.5*c;

h = L/10;

// example of a purely hexahedral mesh using only transfinite
// mesh constraints

Point(1) = {-L,0,-L, lc};
Point(2) = { L,0,-L, lc};
Point(3) = { L,h,-L, lc};
Point(4) = {-L,h,-L, lc};
Point(5) = {-L,0, L, lc};
Point(6) = { L,0, L, lc};
Point(7) = { L,h, L, lc};
Point(8) = {-L,h, L, lc};
Line(1) = {4,3};
Line(2) = {3,2};
Line(3) = {2,1};
Line(4) = {1,4};
Line(5) = {5,6};

Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,5};
Line(9) = {1,5};
Line(10) = {4,8};
Line(11) = {2,6};
Line(12) = {3,7};
Line Loop(1) = {3,4,1,2}; 
Line Loop(2) = {5,6,7,8}; 
Line Loop(3) = {9,-8,-10,-4};
Line Loop(4) = {-7,10,-1,-12};
Line Loop(5) = {-11,-6,12,-2};
Line Loop(6) = {-5,11,-3,-9};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};

Surface Loop(1) = {2,6,5,4,3,1};
Volume(1) = {1};

//Transfinite Line{1,3,5,7,9:12} = n;
//Transfinite Line{2,4,6,8} = 2;

//Transfinite Surface {1} = {1,2,3,4};
//Transfinite Surface {2} = {5,6,7,8};
//Transfinite Surface {3} = {1,5,8,4};
//Transfinite Surface {4} = {8,7,3,4};
//Transfinite Surface {5} = {6,2,3,7};
//Transfinite Surface {6} = {5,1,2,6};
//Transfinite Volume{1} = {1,2,3,4,5,6};
//Recombine Surface{15:25:2};
