#include "error.fpp"

MODULE input_m
!--------------------------------------------------------------------------------------------- 
! Module for reading all necessary data
!
! The data entry can be done through a data file (.par extension) or directly on the standard 
! input (in line)
!
! The data are organized according to a set of keywords. The general syntax of a keyword is  
! the following: 
!
!           keyword(arg0 = val0) : {arg1 = val1, arg2 = val2, ... }
!
! where "keyword" is the name of the keyword, "arg?" are the names of its arguments and "val?" 
! the values assigned to these arguments. These values can be either a number, a string or an
! array of numbers or of strings. For string values, although it is not always necessary, it 
! is recommended to use lowercases and to enclose them between quotes or double quotes (quotes 
! are particularly important for file name or for components of strings array).
!
! Example:
!
!      mesh : { file = "../meshmodel/mymesh.msh", nip = 3, elt = "Tetr04" }
!
!      Dirichlet(face=10) : { dof(1) = 5.23, dof(2) = 4.1 }
!
!      linear_solver : {lib = "sparskit", meth="bcgstab", opt=[120, 1e-8, 1e-5] }
! 
!         
! You can add your own keyword by:
!
! - giving a name to this keyword (say for example "mykwd")
! - add it to the select case instance of the driver input_param (see below)
! - implement a subroutine input_mykwd (see one of the existing routines below)) that will  
!   read the data associated to your keyword. As far as possible, use in it a call to the 
!   "input_getval" routine to read the assigned values of each arguments.
!-----------------------------------------------------------------------------------R.H. 12/16  

   use kindParameters_m
   
   use constants_m
   use defTypes_m
   use param_m
   use signalHandler_m

   use models_m , only: models_driver   
   use element_m, only: element_driver
   use util_m   , only: util_removesp, util_string_low, util_get_unit, util_alloc, &
                        util_timestamp, util_wtime, util_executeCommandLine, &
                        i2a => util_intTochar, util_FindSubstring2, util_IsBalanced2

   implicit none
      
   type var_t                                  ! For a user-defined variable
      character(20   ) :: name                 ! Name of the variable (max. 20 char.) 
      character(MDSTR), allocatable :: valC(:) ! String-values (max. 80 char.)
   end type var_t     
      
!
!- Shared variables:
!
   character(len=:), allocatable :: ifile ! Name of the input file

   integer  (Ikind) :: ui,  &             ! Logical unit of the input file
                       numl               ! The # of the current line
                          
   character(LGSTR) :: rec                ! Current record 
   
   integer  (Ikind) :: narg               ! the current record is parsed into 
   character(20   ) :: args(0:40), keywd  ! a keyword (keywd) narg arguments (args)
   character(MDSTR) :: vals(0:40)         ! and narg string-values
   
   logical           :: is_eof            ! .true. when the End-of-File is reached

   type     (var_t) :: myvar(1000)        ! Set of user-defined variables
   integer  (Ikind) :: nvar = 0           ! Counter of variables
   
   integer  (Ikind) :: warn = 0           ! Counter of warnings       
   
!
!- The following will serve to cross-reference the data that come from the mesh, the
!  physical problem, the boundary conditions,.. in order to find possible inconsistencies.
!   
   integer  (Ikind) :: ndofD,  &  ! number of d.o.f invoked by the Dirichlet bc data
                       ncmpM,  &  ! number of d.o.f invoked by the Neumann bc data
                       ndimm,  &  ! space dimension deduced from the mesh data
                       nmatm,  &  ! number of sub-domains deduced from the mesh 
                       npros      ! number of material properties found in the data

   private
   public :: input_param

   
CONTAINS

!=============================================================================================
   SUBROUTINE input_param ( mydata, mymesh, stat )
!=============================================================================================
   type(udata_t), intent(in out) :: mydata
   type(mesh_t ), intent(in out) :: mymesh
   type(err_t  ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Reads each record on the input file and parses it in term of keyword and corresponding
!  attached data
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------  
   character(len=*    ), parameter :: Here = 'input_param'    
   integer  (Ikind    )            :: iostat, phase, nrec
   character(len=LGSTR)            :: iomsg
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 

   call util_timestamp ( STDOUT )
!
!- initialize the set of parameters to their default values:
!       
   phase = 0 ; call input_default ( phase, stat )
!
!- prompt the user to give an input file name:
!
   write(*,'(a)',advance='no')' name of the input file (without the .par extension) : '
   read(*,'(a)') name_input_file    
   ifile = trim(adjustl(name_input_file))//'.par'

   timer%tstart = util_wtime()   
!
!- open the file "ifile":
!    
   call util_get_unit ( ui )
   open (ui, file = ifile, status = 'old', action = 'read', iostat = iostat, iomsg  = iomsg)
   
   if ( iostat /= 0 ) then
      stat = err_t ( stat = UERROR, where = Here, msg = 'Unable to open the file "' // &
                     ifile //'"'// NLT // 'IO MSG: '// trim(iomsg) )
      return                  
   end if   
   
   print*
   print*,'.... read of the file: ',trim(ifile)
   print*
    
    
   numl = 0 ; nrec = 0 ; is_eof = .false. 

!
!- read each record of the input file:
!
   do
!
!-    get a new record:
!   
      call input_newrec ( stat )  ;  error_TraceNreturn(stat>IZERO, Here, stat) 
!
!-    exit if the end of file is reached or if the end of data is specified:
!     
      if ( is_eof ) exit
!
!-    parse the record (extract the keyword, fetch its arguments and their values):
!       
      call input_parse ( stat ) ;  error_TraceNreturn(stat>IZERO, Here, stat)
!      
!-    read now the assigned values according to the keyword name:
!   
      select case ( trim(keywd)  )
   
         case ( 'mesh' )
            call input_mesh ( mymesh, stat )
                  
         case ( 'matprop'  )
            call input_matprop ( mydata, stat )
      
         case ( 'dirichlet' )
            call input_dirichlet ( mydata, stat )
      
         case ( 'neumann' )
            call input_neumann ( mydata, stat )
         
         case ( 'physical_problem' )
            call input_physicalpb ( stat ) 
      
         case ( 'linear_solver' )
            call input_linearsolver ( stat )
      
         case ( 'nlinear_solver' )
            call input_nlinearsolver ( stat )

         case ( 'output_format' )
            call input_outputformat ( stat )
                 
         case ( 'verbosity' )
            call input_verbosity ( stat )
            
         case ( 'var' )
            call input_var ( stat )    
         
         case default
            call input_error ( stat, 'Unknown keyword "'// trim(keywd)//'"', &
                                      UERROR, Here, numl, ifile, trim(rec)   )               
            return
         
      end select   
      
      error_TraceNreturn(stat>IZERO, Here, stat)
         
      nrec = nrec + 1
       
   end do
   
   if ( nrec == 0  ) then
      stat = err_t ( stat = UERROR, where = Here, msg = 'End-of-file of ' // ifile // &
                     ' is reached with no valid record encountered' )
      return
   end if
                 
   if ( verbosity(1) >= 1 ) print*,'number of valid records encountered:',nrec     
!
!- check the data:
!
   call input_check ( mydata, mymesh, stat ) ; error_TraceNreturn(stat>IZERO, Here, stat)
!
!- if necessary complete the uninitialized optional values:
!
   phase = 1 ; call input_default ( phase, stat ) ; error_TraceNreturn(stat>IZERO, Here, stat)
   
   if ( verbosity(1) >= 1 ) call input_summary ( mydata, mymesh, stat )
   
   timer%times(1) = util_wtime(timer%tstart) ; 
   timer%names(1) = 'Reading and checking user data'
   
   END SUBROUTINE input_param
   
   
!=============================================================================================
   SUBROUTINE input_newrec ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Read on the file "ifile" (logical unit ui) a new record. At this step, a record is just a 
!  string ending by the symbol "}". 
!
!  - Blank lines are ignored even if inserted between the begining and the end of the record. 
!  - Line of comments (begining by one of the symbols : "//", "!" or "%") are ignored. 
!  - Part of a record located at the right of "//", "!" or "%" will be removed from the record.
!  - Case is insensitve.
!  - Spaces and tabs will be removed from the record.
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------   
   character(len=*  ), parameter :: Here = 'input_newrec'  
   !character(len=200)            :: line
   character(len=LGSTR)            :: line
   character(len=1  )            :: last
   integer  (Ikind  )            :: err, pos
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 
   
   rec = '' ; last = '&'
    
   do while ( last /= '}' )  ! read until the next record is complete (i.e. terminated by "}")
! 
!-    Go to the next non-blank or non-commented line :
!        
      line = ''
      do while ( len(trim(line))==0 .or. index(line, '//')==1 .or.  & 
                 index(line,'!')==1 .or. index(line, '%' )==1       )
                
         numl = numl + 1
         read(ui,'(a)',iostat=err) line
         
         if ( err > 0 ) then
            stat = err_t ( stat = UERROR, where = Here, msg = 'Read error at line ' // &
                           i2a(numl) // ' of file '//ifile )
            return
         end if
!
!-       return with is_eof = .true. if the end of file is reached:
!       
         if ( err < 0 ) then
            is_eof = .true.
            return
         end if          
!
!-       Remove spaces and tabs from the resulting string:
!       
         line  = util_removesp ( adjustl(line) )
!
!-       return with is_eof = .true. if the end of data is imposed  (keyword "eod")
!        (ignoring everything that comes after):
!         
         if ( util_string_low(line(1:3)) == 'eod' ) then
            is_eof = .true.
            return
         end if   
!
!-       abort if required (keyword "stop"):
!
         if ( util_string_low(line(1:4)) == 'stop' ) then
            write(STDOUT,'(a,/)')'--> ABORT (stop required by the user)'
            stop
         end if
!
!-       Remove possible ending comments:
!         
         pos = index(line,'//') ; if (pos > 1) line = line(1:pos-1)
         pos = index(line,'!')  ; if (pos > 1) line = line(1:pos-1)   
          
      end do   
         
      rec = trim(rec)//trim(line)
       
      pos = len(trim(rec)) ; last = rec(pos:pos)

   end do    
       
   END SUBROUTINE input_newrec


!=============================================================================================
   SUBROUTINE input_parse ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Parse the record string rec to find the keyword, its associated arguments and their 
!  assigned values
!
!  A valid record is of the form
!
!     keywd : { arg1 = val1, arg2 = [val2(1), val2(2),...], arg3 = 'hello', ... }
!
!  or
!
!     keywd(Id) : {arg1 = val1, arg2 = [val2(1), val2(2),...], arg3 = 'hello', ... }
!
!  where the string "Id" (identifier) is also of the form: arg0 = val0.
!
!  This procedure extracts the sub-string "keywd", the number "narg" of arguments, their names
!  as sub-strings 'arg0", "arg1", "arg2",..., and their assigned values as sub-strings "val0", 
!  "val1", "val2",... 
!
!  The argument names are stored into the string array "args" and the assigned values into the
!  string array "vals"
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables --------------------------------------------------------------------------- 
   character(len=*  ), parameter :: Here = 'input_parse'  
   integer  (Ikind  ), parameter :: neqmax = 40
   integer  (Ikind  )            :: eq(neqmax)   
   integer  (Ikind  )            :: i, bl, br, pl, pr, al, ar, sl, sr, dlim, dlim0, err
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 
   
   err = 0
!
!- remove space characters from rec (normally this is already done):
!
   rec  = util_removesp ( adjustl(rec) )
!
!- Find the locations of '{' and '}' (first check the consistency):
!   
   if  ( .not. util_IsBalanced2 (trim(rec), '{}', stat) ) then
      call input_error ( stat, 'Unbalanced braces ("{" "}")', UERROR, Here, numl, ifile, rec )  
      return
   end if        

   bl = index(rec,'{') ; br = index(rec,'}')  ! locations of the braces        
!
!- Find the location of the symbol ":"
!
   if ( rec(bl-1:bl-1) /= ':' ) then   ! ":" has to be in location bl-1
      keywd = rec(1:bl-1)
      call input_error ( stat, 'Symbol ":" is missing after keyword "'//trim(keywd)//'"', &
                         UERROR, Here, numl, ifile, rec )               
      return
   end if   
!
!- See if "(" and ")" are present in this record and check for consistency. 
!  Then extract the name of the keyword
!   
   pl = index(rec(1:bl-2),'(') ; pr = index(rec(1:bl-2),')') ! locations of the parentheses
   
   if ( pl == 0 .and. pr == 0 ) then
!
!-    case without parentheses --> the keyword is between 1 and bl-2:
!    
      keywd = util_string_low ( rec(1:bl-2) ) ; args(0) = '' ; vals(0) = ''
      
   else if ( pl > pr .or. pl*pr == 0 ) then
!
!-    misplaced parentheses:
!   
      call input_error ( stat, 'Unbalanced parentheses', UERROR, Here, numl, ifile, rec )               
      return
   else   
!
!-    case with parentheses --> the keyword is between 1 and pl-1:
!             
      keywd = util_string_low ( rec(1:pl-1) )
!
!-    the identifier argument args(0) and its "string-values" vals(0):
!      
      i = index(rec(pl+1:pr-1),'=')
      if ( i == 0 .and. pl+1 /= pr ) then
         call input_error ( stat, 'Missing "=" for the identifier of "'//trim(keywd)//'"', &
                            UERROR, Here, numl, ifile, rec )                     
         return
      end if   
      i = i + pl 
      args(0) = rec(pl+1:i-1) ; vals(0) = rec(i+1:pr-1)     
   end if
!
!- arguments and values are located between al = bl+1 and ar = br-1:
!
   if ( br == bl+1 ) then
      narg = 0  ! empty braces (print a warning)
      call input_error ( stat, 'Empty braces for keyword "'//trim(keywd)//'"', &
                         WARNING, Here, numl, ifile, rec )                           
      return
   end if   

   al = bl + 1; ar = br - 1      
!
!- occurences of the symbol "=" in rec(al:ar) (= number of arguments) and their locations
!
   call input_findachar ( eq, narg, rec, al, ar, neqmax, '=', err )
   
   if ( err == 1 ) then
      call input_error ( stat, 'Too many "="s', UERROR, Here, numl, ifile, rec )                           
      return
   end if      
!
!- extract the sub-strings corresponding to the argument names and the sub-strings 
!  corresponding to their assigned values:
!
   dlim0 = al-1
   do i = 1, narg-1
!
!-    find the location of the delimeter symbol "," just before the next argument
!   
      dlim = index(rec(eq(i):eq(i+1)),',',back=.true.) 
      
      if ( dlim == 0 ) then ; err = 1 ; exit ; endif
            
      dlim = dlim + eq(i) - 1
!
!-    check if this comma is really a delimiter i.e. not between quotes or double 
!     quotes (numeric values as well as string values may include a comma) or square 
!     brackets (array type uses also commas as delimiters between components):
!
      sl = eq(i) - 1 + index(rec(eq(i):eq(i+1)),'"')
      sr = eq(i) - 1 + index(rec(eq(i):eq(i+1)),'"',back=.true.)
      if (sl <= dlim .and. dlim <= sr) then ; err = 1 ; exit ; endif
      
      sl = eq(i) - 1 + index(rec(eq(i):eq(i+1)),"'")
      sr = eq(i) - 1 + index(rec(eq(i):eq(i+1)),"'",back=.true.)
      if (sl <= dlim .and. dlim <= sr) then ; err = 1 ; exit ; endif
        
      sl = eq(i) - 1 + index(rec(eq(i):eq(i+1)),"[")
      sr = eq(i) - 1 + index(rec(eq(i):eq(i+1)),"]")
      if (sl <= dlim .and. dlim <= sr) then ; err = 1 ; exit ; endif
!
!-    vals(i) is then between eq(i)+1 and dlim-1 and args(i) between dlim0+1 and eq(i)-1
!  
      vals(i) = rec(eq(i)+1:dlim-1); args(i) = rec(dlim0+1:eq(i)-1)
      dlim0 = dlim
   end do
   
   if ( err == 1 ) then
      call input_error ( stat, 'A comma is missing', UERROR, Here, numl, ifile, rec )                           
      return
   end if
!
!- for the last one:
!   
   vals(narg) = rec(eq(narg)+1:ar) ; args(narg) = rec(dlim0+1:eq(narg)-1)
!
!  At this step, the record is splited into keywd, args(0:narg) and vals(0:narg) such that 
!
!  rec =   "keywd(args(0)=vals(0)): {args(1)=vals(1),args(2)=vals(2),...}"
!
!  and we are ready to read the data contained in vals(:) according to "keywd" by calling,
!  in input_param, the corresponding subroutine

   END SUBROUTINE input_parse
   

!=============================================================================================
   SUBROUTINE input_getval ( str, nmax, stat, nval, seekv, valI, valR, valC )
!=============================================================================================
   character(len=*), intent(in    )           :: str
   integer  (Ikind), intent(in    )           :: nmax
   type     (err_t), intent(in out)           :: stat
   integer  (Ikind), intent(   out)           :: nval
   logical         , intent(in    ), optional :: seekv  
   integer  (Ikind), intent(   out), optional :: valI(nmax)
   real     (Rkind), intent(   out), optional :: valR(nmax)
   character(len=*), intent(   out), optional :: valC(nmax)
!--------------------------------------------------------------------------------------------- 
!  Converts the string "str" into  
!
!               - an array of integers if valI is present, 
!               - an array of reals    if valR is present,
!               - an array of strings  if valC is present, 
!
!  The maximum size allocated by the calling program for this array is nmax.
!
!  Inputs
!
!    str  : the string to convert
!
!    nmax : the maximum number of values expected. nmax must be >= than nval, otherwise
!           an error message is displayed and the program is stoped
!
!    seekv: unless seekv = .false., the procedure first seeks among the user-defined variables  
!           if a variable of name str exists and if it is, it gets its value(s).
!
!  Outputs
!
!    valI: if presents, valI contains the integer values read from str
!
!    valR: if presents, valR contains the real values read from str
!
!    valC: if presents, valC contains the string values read from str
!
!    nval: the corresponding number of values found 
!
!
!  Notice - Different cases may occur:
!
!          1) str corresponds to a single string value, e.g. str = hello 
!             and valC is present --> the outputs are: nval = 1, valC(1) = hello
!                 valR or valI present --> error
!           
!          2) str corresponds to a single real value, e.g., str = 123.5
!             and valR is present --> the outputs are: nval = 1, valR(1) = 123.5
!                 valI present --> error

!          3) str corresponds to an array of strings, e.g., str = ['one', 'two']
!             and valC is present --> the outputs are: nval = 2, valC(1) = one, valC(2) = two
!                 valR or valI present --> error
!
!          4) str corresponds to an array of reals, e.g., str = [1, 2]
!             and valR is present --> the outputs are: nval = 2, valR(1) = 1, valR(2) = 2
!                 valI present --> error

!          5) str corresponds to a single user-defined variable, e.g., str = var1 with "var1"
!             the name of a variable stored in myvar structure. The outputs depend on the
!             nature of this variable (cases 1, 2, 3 or 4)
!
!          6) str corresponds to an array containing one or several user-defined variable,
!             e.g., str = [1, two, 3] and "two" is a user-defined variable
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables --------------------------------------------------------------------------- 
   character(len=*), parameter   :: Here = 'input_getval' 
   integer  (Ikind), parameter   :: nsubmax = 50
   character(MDSTR)              :: substr(nsubmax), buf(nsubmax)
   integer  (Ikind)              :: i, j, nv, err, nval0
   character(len=:), allocatable :: msg
   logical                       :: seekvar
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 
         
   nval = 0

   if ( present(seekv) ) then
      seekvar = seekv
   else
      seekvar =.true.
   end if            
!
!- if appropriate, split "str" in nval0 sub-strings ("substr")
!                        
   call input_arrayofstrings ( str=str, resC=substr, n=nsubmax, nval=nval0, stat=stat )
   error_TraceNreturn(stat>IZERO, Here, stat)
!
!- loop over the nval0 substrings:  
   
   do i = 1, nval0
!
!-    see among the user-defined variable if "substr" is a name of such variable. If yes put
!     its nv string-values in the array "buf". Else, "buf" is simply reduced to "substr": 
!     
      nv = 0; if ( seekvar ) call input_findvar ( substr(i), buf, nv )
            
      if ( nv == 0 ) then
         nv = 1; buf(1) = substr(i)
      end if
!
!-    if nmax values is exceeded, print an error message and stop:
!      
      if ( nval+nv > nmax ) then
         stat = err_t ( stat = UERROR, where = Here, msg = &
                        'The number of values exceeds the assigned size ('//i2a(nmax)//')' )
         return
      end if            
!
!-    read and store in valC or valR or valI the nv values contained in buf:
!
      do j = 1, nv
         nval = nval + 1
         
         err = 0
         
         if ( present(valC) ) then 
            if ( .not. util_IsBalanced2 (buf(j),"''"//'""',stat) ) then
               msg = '(Missing a quote? string must be enclosed in quotation marks)'
               err = 1
            else
               read(buf(j),*,iostat=err) valC(nval)
               msg = '(wrong character string or uninitialized variable)'
            end if
         endif            

         if ( err == 0 .and. present(valI) ) then
            read(buf(j),*,iostat=err) valI(nval)
            msg = '(wrong integer number or uninitialized variable)'
         end if
                  
         if ( err == 0 .and. present(valR) ) then
            read(buf(j),*,iostat=err) valR(nval)
            msg = '(wrong real number or uninitialized variable)'
         end if

         if ( err /= 0 ) then
            msg = 'Unable to get the value of "'//trim(buf(j))//'" '// NLT // trim(msg)       
            stat = err_t ( stat = UERROR, where = Here, msg = trim(msg))
            return
         end if     
                               
      end do
                     
   end do  
                           
   END SUBROUTINE input_getval
   
   
!=============================================================================================
   SUBROUTINE input_arrayofstrings ( str, n, stat, resC, nval )
!=============================================================================================   
   character(len=*), intent(in    ) :: str
   integer  (Ikind), intent(in    ) :: n
   type     (err_t), intent(in out) :: stat
   character(len=*), intent(   out) :: resC(n)
   integer  (Ikind), intent(   out) :: nval
!--------------------------------------------------------------------------------------------- 
!  Splits the string "str" in a set of sub-strings, if this string is enclosed between brackets 
!  and contains commas.  Stores then these sub-strings into array resC. 
!
!  If no brackets are present, resC = str and nval = 1.
!
!  All spaces and tabs are assumed to be removed from "str" (call removsp before)
!
!  Inputs
!      str : the string that begins by a "[" and ends by a "]”
!      n   : the size of the array resC (n >= nval)
!
!  Ouput
!      resC: the substrings found
!      nval: the number of substrings found (nval <= n)
!
!  Examples: 
!      * str is the string "[one,two,three]" 
!        then nval = 3 and resC(1) = 'one', resC(2) = 'two', resC(3)= 'three'
!
!      * str is the string "hello" or "[hello]"
!        then nval = 1 and resC(1) = 'hello'
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables --------------------------------------------------------------------------- 
   character(len=*), parameter  :: Here = 'input_arrayofstrings'  
   integer  (Ikind)             :: lb, rb, p1, p2, j, ndelim, err, mloc
!   integer  (Ikind)             :: delim(n+1)
   integer  (Ikind), allocatable :: delim(:)
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return    
!
!- read the strings (between "[" and "]") 
!       
   lb = index(str,'[') ; rb = index(str,']',back=.true.)
            
   if ( lb == 0 .and. rb == 0 ) then
      nval = 1
      resC(1) = str
      return
   else
      if ( lb > rb ) then
         stat = err_t( stat = UERROR, where = Here, msg = 'unbalanced brackets' )
         return
      end if  
      if ( rb == lb+1 ) then
         nval = 0  ! empty bracket
         return
      end if     
   end if
 
!
!- determine the number of strings to be read = number of commas (",") + 1
!  and save the positions in str of these delimiters
!
!   delim(1) = lb   
!   nval = size(delim)-2     
!
!   call input_findachar ( delim(2:), ndelim, str, lb+1, rb-1, nval, ',', err )
!
!    if ( err == 1 ) then
!       stat = err_t( stat = UERROR, where = Here, msg = 'Unable to parse the record' )
!       return
!    end if   
!
!   nval = ndelim + 1
!   delim(nval+1) = rb
!
!
!- store in resC
!            
!    do j = 1, nval
!       p1 = delim(j)+1 ; p2 = delim(j+1)-1  ! string located between p1 and p2 in str
!       resC(j) = adjustl(str(p1:p2))
!    end do

   call util_FindSubstring2 ( delim, nval, str(lb+1:rb-1), IONE, rb-lb+IONE, ",", &
                              '()[]""'//"''", stat )
   error_TraceNreturn(stat>IZERO, HERE, stat) 
     
   delim = delim + lb
   
   p1 = lb+1
   do j = 1, nval
      p2 = delim(j) - 1  
      resC(j) = adjustl(str(p1:p2))
      p1 = p2 + 2
   end do
   
   nval = nval + 1
   resC(nval) = adjustl(str(p1:rb-1))

   END SUBROUTINE input_arrayofstrings
   
   
!=============================================================================================
   SUBROUTINE input_findachar ( loc, n, str, lind, rind, nmax, ch, err )
!=============================================================================================  
   integer  (Ikind), intent(in    ) :: lind, rind, nmax
   character(len=*), intent(in    ) :: str, ch
   integer  (Ikind), intent(   out) :: n, loc(*), err
!--------------------------------------------------------------------------------------------- 
!  Finds in the string "str" and between indices lind and rind, the occurences (n) of the 
!  character "ch". Stores its different locations in loc. 
!  Returns err = 1 if n > nmax
!  Returns err = 2 if lind > rind
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------
   integer(Ikind) :: j
!--------------------------------------------------------------------------------------------- 
   
   if ( lind > rind ) then
      err = 2
      return
   else 
      err = 1   
   end if

   n = 0
   do j = lind, rind
      if ( str(j:j) == ch ) then
         n = n + 1
         if ( n > nmax ) return  
         loc(n) = j
      end if   
   end do  
   
   err = 0
   
   END SUBROUTINE input_findachar
   
      
!=============================================================================================
   SUBROUTINE input_findvar ( str, valC, nval )
!=============================================================================================   
   character(len=*), intent(in    ) :: str
   character(len=*), intent(   out) :: valC(*)
   integer  (Ikind), intent(   out) :: nval
!---------------------------------------------------------------------------------------------  
!  Finds among the list of user-defined variables, the variable of name str and if presents
!  gets its corresponding (string-)values.
!  Return nval = 0 is str is not a user-defined variables.   
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------
   integer(Ikind) :: i
!---------------------------------------------------------------------------------------------  
   
   nval = 0
!
!- if str begins by quote or double quote, it cannot be a name of variable (str is a pur string)
!
   if ( index(str, '"') /= 0 .or. index(str,"'") /= 0 ) return   
!
!- loop over the stored variables:
!
   do i = 1, nvar
      if ( trim(str) == trim(myvar(i)%name) ) then
         nval = size(myvar(i)%valC)
         valC(1:nval) = myvar(i)%valC(:)
         return
      end if
   end do
                    
   END SUBROUTINE input_findvar


!=============================================================================================
   SUBROUTINE input_var ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Reads the data for the keyword "var". OPTIONAL KEYWORD.
!
!  This keyword is used to set user-defined variables which can be used in other keywords
!  (including the keyword "var" itself)
!
!  Warning: if a variable name is already used, its value will be redefined.
!
!
!  Examples:
!
!        var : { a = 4.0,   b = [1, 2, 3],   c = "../Truc" }
!
!  and previously defined variables may be reused in new one:
!
!        var : { B = [0, b, a],   C = [c,"Machin"],  A = B , c = "new"}
!
!  Notes: - for this keyword, argument names (and their values) are CASE-SENSITIVE, e.g. in
!           the above example A contains [0, 1, 2, 3, 4.0] and not the value of b ([1, 2, 3]).
!         - the value contained in a variable may be redefined, as "c" in the last example
!           (and the user will be warned).
!
!
!  Inputs
!
!     narg: number of arguments in the current record
!     args: arguments names (Here, the names of the user-defined variables)
!     vals: assigned values (as strings)
!
!  Outputs
!
!     myvar: up-dated structure containing the list of variables
!     myvar%name : the names of the variables (20 char. max.)
!     myvar%valC : the associated string-values (80 char. max.)
!
!  Note: at this step, the string-values are not yet evaluated (just stored).
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: Here = 'input_var'
   integer  (Ikind), parameter :: nstrmax0 = 50, nstrmax = 100
   character(MDSTR)            :: str0(nstrmax0), str(nstrmax)   
   integer  (Ikind)            :: i, j, k, nval0, nval, ivar, jvar, n
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
   
   do i = 1, narg
!
!-    Search in the list of stored variables (myvar) if the name args(i) is already
!     used. If yes, its string-value (valC) will be replaced by the new one (vals(i))
!      
      ivar = 0
      do j = 1, nvar
         if ( trim(myvar(j)%name) == trim(args(i)) ) then
            ivar = j  ! this name already exists, print a warning
            call input_error ( stat, 'Redefining the user-variable "'//trim(args(i))//'"', &
                               WARNING, Here, numl, ifile, rec )                           
            exit
         end if   
      end do    

!
!-    If ivar == 0, it's a new variable. Store its name:
!
      if ( ivar == 0 ) then
         nvar = nvar + 1 ; ivar = nvar
         myvar(ivar)%name = args(i)
      end if

!
!-    Distribute the components of vals(i) in "nval0" sub-strings:  
!
      call input_arrayofstrings ( str=vals(i), resC=str0, n=nstrmax0, nval=nval0, stat=stat )  
      
      if ( stat > IZERO ) then
         call input_error ( stat, '', UERROR, Here, numl, ifile, rec )
         return
      end if                           
                                   
!
!-    For each of these components, check if it is a user-defined variable: 
!
      nval = 0
      do j = 1, nval0
!      
!-       If the jth string-value, str0(j), of this variable is not of string type 
!        (i.e., not starting with a quote or a double quote), see if this value is 
!        itself a user-defined variable. If yes, get its values and store them into str
!
         jvar = 0
         if ( index(str0(j),"'") == 0 .and. index(str0(j),'"') == 0 ) then
!
!-          scroll through the previous stored variable names:  
!   
            do k = 1, nvar 
               if ( trim(myvar(k)%name) == trim(str0(j)) ) then
!
!-                str0(j) is the user-defined variable myvar(k), save its # and exit the loop:
!               
                  jvar = k
                  exit
               end if
            end do  
         end if   
            
         if ( jvar /= 0 ) then    
!
!-          the value str0(j) is itself a user-defined variable: add its value(s) into str
!
            n = size(myvar(jvar)%valC)
            str(nval+1:nval+n) = myvar(k)%valC(1:n)
            nval = nval + n
         
         else   
!
!-          str0(j) is not a user-defined variable:
!         
            nval = nval + 1
            str(nval) = str0(j)
         end if
         
      end do     
!
!-    allocate (in mode "erase") the strings array myvar(ivar)%valC of size nval and length of 80
!
      call util_alloc ( 'e', myvar(ivar)%valC, nval, MDSTR, stat )
      
      if ( stat > IZERO ) then
         call input_error ( stat, 'Problem during allocation of "myvar(ivar)%valC"', &
                            IERROR, Here, numl, ifile, rec )
         return
      end if                           
!
!-    Store (or replace) the strings-values of this variable in myvar(ivar)%valC
!
      myvar(ivar)%valC(1:nval) = str(1:nval)
                                                       
   end do
                   
   END SUBROUTINE input_var
   
         
!=============================================================================================
   SUBROUTINE input_physicalpb ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Reads the data for the keyword "physical_problem". REQUIRED KEYWORD.
!
!  Four parameters are associated to this keyword : "class",  "model", "Time", "Ntime"
!  and their corresponding values are of type     :  string,   string,  real , integer
!
!  "class" and "model" are required.
!
!  "class" define the class of the problem: "steady" or "transient"
!  "model" is the name of the selected model
!
!  "Time" and "Ntime" are required if class = transient
!
!  "Time" is the time duration
!  "Ntime" is the number of time steps 
!
!
!  Examples:
!           
!     physical_problem: {class='steady'   , model='elasticity(plane_strain)'}  
!
!     physical_problem: {class='transient', model='diffusion(3d)', time=20.5, ntime=1000}  
!
!  (case insensitive) tHere is no prescribed order, blanks have no effect, line breaks may be 
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!     Physical_Problem: { 
!                        Class = 'transient',      // a time-dependent problem
!
!                        Model = 'diffusion(3d)',  // 3D heat equation
!
!                        time  = 20.5              // 
!                        Ntime = 1000              // nbr of time steps
!                        }
!
!  Inputs:
!      narg       : number of arguments found for this keyword
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!
!  Outputs:
!      name_class_pb : name class of the problem (saved in module param) 
!      name_model_pb : name of the model  (saved in module param)
!      Ntime         : number time steps (saved in module param)
!      Ttime         : total time (saved in module param)
!-----------------------------------------------------------------------------------R.H. 12/16  
   
!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'physicalpb'
   integer  (Ikind)              :: valI(2), i, iexit, nval, okclass, okmodel, okTtime, okNtime
   real     (Rkind)              :: valR(2)
   character(len=:), allocatable :: msg
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
!
!- put the narg argument names in lowercase
!   
   do i = 0, narg
      args(i) = util_string_low ( args(i) )
   end do   
      
   okclass = 0; okmodel = 0; okTtime = 0; okNtime = 0
!
!- if the problem is already defined, print an error message and stop
!
   if ( len_trim(name_model_pb) /= 0 ) then
      call input_error ( stat, '"Physical_problem" was previously used (More than one '// &
                         'problem cannot be considered)', UERROR, Here, numl, ifile, rec )   
      return   
   end if
   
!
!- if args(0) /= '', print a warning message and continue
!  (in a future implementation, args(0) may serve to define the problem's # in case of
!   multiphysics, e.g., physical_problem(#=1): {class=...} )
!
   if ( len_trim(args(0)) /= 0 ) then
      call input_error ( stat, 'Identifier "'//trim(args(0))//'" ignored for the '// &
                         'keyword Physical_problem', WARNING, Here, numl, ifile, rec )   
   end if   
   
   do i = 1, narg
      
      iexit = i
      
      select case ( args(i) )
      
         case ( 'class' )
            okclass = 1
            call input_getval ( str=vals(i), valC=name_class_pb, nmax=IONE, nval=nval, &
                                stat=stat )            
            if ( stat > IZERO ) exit 
          
            name_class_pb = util_string_low ( name_class_pb )                   
         
         case ( 'model' )
            okmodel = 1
            call input_getval ( str=vals(i), valC=name_model_pb, nmax=IONE, nval=nval, &
                                stat=stat ) 
            if ( stat > IZERO ) exit 

            name_model_pb = util_string_low ( name_model_pb )                   
 
         case ( 'time' )
            okTtime = 1
            call input_getval ( str=vals(i), valR=valR , nmax=IONE, nval=nval, stat=stat ) 
            if ( stat > IZERO ) exit 
            
            Ttime = valR(1)                      

         case ( 'ntime' )
            okNtime = 1
            call input_getval ( str=vals(i), valI=valI, nmax=IONE, nval=nval, stat=stat )
            if ( stat > IZERO ) exit 
            
            ntime = valI(1)
                
         case default
            msg = 'Unknown argument "'//trim(args(i))//'" for keyword Physical_problem'
            call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
            return   
               
      end select       
             
   end do   
   
   if ( stat > IZERO ) then
      msg = 'Read error for "'//trim(args(iexit))//'"'
      call input_error ( stat, msg, UERROR, Here, numl, ifile, rec, append = 'before' )
      return   
   end if   
   
   msg = ''
   if ( okclass == 0 ) then
      msg = 'The required argument "class" is missing in keyword "physical_problem"'
   else
      if ( trim(name_class_pb) /= 'transient' .and. trim(name_class_pb) /= 'steady' ) &
         msg ='A valid class name is "transient" or "steady"'
      if (trim(name_class_pb) == 'transient') then
         if (okTtime == 0) msg = 'Argument "Time" is needed for this class of problem'
         if (okntime == 0) msg = 'Argument "Ntime" is needed for this class of problem'
      end if  
   end if
   
   if ( okmodel == 0 ) &
      msg = 'The required argument "model" is missing in keyword "physical_problem"'
   
   if ( len(msg) /= 0 ) then
      call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
      return
   end if
      
   
   END SUBROUTINE input_physicalpb


!=============================================================================================
   SUBROUTINE input_verbosity ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Reads the data for the keyword "verbosity". OPTIONAL KEYWORD.
!
!  Five parameters are associated to this keyword:
! 
!                     "all",  "io", "fem", "linear_solver", "nlinear_solver"
!  of type integer.
!
!  All of them are optional.
!
!  Examples:
!           
!     verbosity: {all = 0}    means no-verbosity
!
!     verbosity: {io=1, fem=1, nlinear_solver=1, linear_solver=1} same as {all=1}   
!
!  (case insensitive) tHere is no prescribed order, blanks have no effect, line breaks may be 
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!     verbosity: { 
!                  io = 0,           // no verbosity during data read or write
!                  fem = 0,          // no information displayed during F.E. construction
!
!                  linear_solver =0  // no verbosity from the linear solver 
!                  nlinear_solver=1  // print residual during non-linear iterations
!                }
!
!  The verbosity level of the linear solver assigned with the argument "verb" of the keyword 
!  "linear_solver" prevails on the level assigned with "verbosity". The same rule applies for
!  the level of verbosity for the non-linear solver.
!
!  The level of "all" (if given) prevails over the others.
! 
!
!  Inputs:
!      narg       : number of arguments found for this keyword
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!
!  Outputs:
!      verbosity  : integer array (saved in module param) with
!                   verbosity( 1) = level for io
!                   verbosity( 2) = level for fem
!                   verbosity( 3) = level for linear_solver
!                   verbosity( 4) = level for nlinear_solver
!
! Note that:
!
!    * If the keyword "verbosity" is not used then verbosity(:) = 1 
!
!    * If the level of the argument "all" is given then verbosity(:) = level of "all"
!
!    * If only one of the arguments is given (and not "all"), then verbosity = 0 for
!      the others 
!
!    * If verbosity(3) or verbosity(4) are already set (with the argument "verb" of the
!      keywords "linear_solver" or "nlinear_solver", their values are preserved (unless
!      "all" is used)
!-----------------------------------------------------------------------------------R.H. 12/16  
   
!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'verbosity'
   integer  (Ikind)              :: i, iexit, all, nval
   integer  (Ikind)              :: val(2)
   character(len=:), allocatable :: msg
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 

   all = -999
!
!- put the narg arguments names in lowercase
!   
   do i = 0, narg
      args(i) = util_string_low ( args(i) )
   end do   
            
   do i = 1, narg
            
      iexit = i
      
      select case ( args(i) )
      
         case ( 'all' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat )
            if ( stat > IZERO ) exit 
            all = val(1)                   
                  
         case ( 'io' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat )
            if ( stat > IZERO ) exit 
            verbosity(1) = val(1)           
       
         case ( 'fem' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat )
            if ( stat > IZERO ) exit 
            verbosity(2) = val(1)                   

         case ( 'linear_solver' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat )
            if ( stat > IZERO ) exit 
            if ( verbosity(3) < 0 ) verbosity(3) = val(1)                   

         case ( 'nlinear_solver' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat ) 
            if ( stat > IZERO ) exit 
            if ( verbosity(4) < 0 ) verbosity(4) = val(1)    
                
         case default
            msg = 'Unknown argument "'//trim(args(i))//'" for keyword verbosity'
            call input_error ( stat, msg, UERROR, Here, numl, ifile, rec)   
            return            
      
      end select       
      
      if ( all /= -999 ) then 
         verbosity(:) = all
         return
      end if   
      
   end do 

   if ( stat > IZERO ) then
      call input_error ( stat, 'Failed to read the value of "'//trim(args(iexit))//'"', &
                         UERROR, Here, numl, ifile, rec, append='before' )   
      return   
   end if   
   
   END SUBROUTINE input_verbosity
  
   
!=============================================================================================
   SUBROUTINE input_outputformat ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Reads the data for the keyword "output_format". OPTIONAL KEYWORD.
!
!  3 parameters are associated to this keyword:  "type"     , "fmt"      , "freq"
!  and their corresponding values are of type :   char array,  char array,  real array
!
!  Only the first one ("type") is required.
!
!  * "type" gives the list of the output type files.  
!
!  * "fmt" gives the corresponding formats ('ascii' or 'bin').
!
!  * "freq" gives the frequencies of each output. If freq is not given, the results are
!     printed only at the last time-step.
!
!   These three arrays may have different sizes (see examples)
!
!  Examples:
!           
!    1) output_format: { type = ['vtk', 'vnod', 'velm'] , fmt = ['ascii','ascii', 'bin'] } 
!
!  (case insensitive) tHere is no prescribed order, blanks have no effect, line breaks may be 
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!    2) Output_Format : { type = ['vnod','vtk'], // nodal values + vtk output
!
!                       fmt = ['bin']          // vnod in binary, vtk in default format (ascii)    
!                     }
!
!    3) Output_fomat:{type = ['vtk','vnod'], freq = [10]} ! print results every 10% of the
!                                                           total time in vtk type and at
!                                                           100% in and vnod type
!
!    4) Output_fomat:{type = ['vtk','vnod'], freq = [10, 20]} ! every 10% in vtk type
!                                                             ! every 20% in vnod type
!
!  Inputs:
!      narg       : number of arguments found for this keyword
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!
!  Outputs:
!      name_outputtype : output file types 
!      name_outputfmt  : output file formats
!-----------------------------------------------------------------------------------R.H. 12/16  
   
!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: Here = 'input_outputformat'
   integer  (Ikind)            :: i, i0, iexit, n, oktype, nval=0, nop=size(name_outputtype)
   integer  (Ikind)            :: p, l
   logical                     :: first = .true.
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
   
   if ( .not. first ) then
      call input_error ( stat, 'Only one instance of "Output_format" can be used', &
                         UERROR, Here, numl, ifile, rec )   
      return      
   end if   
   
   first = .false.
   
   oktype = 0 
!
!- put the narg arguments names in lowercase
!   
   do i = 0, narg
      args(i) = util_string_low ( args(i) )
   end do   
   
   i0 = nop
   do i = 1, nop
      if ( len_trim(name_outputtype(i)) == 0 ) then
         i0 = i
         exit
      end if 
   end do
   n = nop - i0 + 1
      
   do i = 1, narg
   
      iexit = i
      
      select case ( args(i) )
                  
         case ( 'type' )
            oktype = 1    
            
            call input_getval ( str=vals(i), valC=name_outputtype(i0:nop), nmax=n, &
                                nval=nval, stat=stat )  
            
         case ( 'fmt' )
            call input_getval ( str=vals(i), valC=name_outputfmt(i0:nop) , nmax=n, &
                                nval=nval, stat=stat )  

         case ( 'freq' )
            call input_getval ( str=vals(i), valI=freq_output(i0:nop), nmax=n, &
                                nval=nval, stat=stat )  

         case ( 'dir' )
            call input_getval ( str=vals(i), valC=name_outputdir, nmax=IONE, &
                                nval=nval, stat=stat ) 
                                 
         case default
            call input_error ( stat, 'Unknown argument "' // trim(args(i)) // &
                               '" for keyword output_format', UERROR, Here, numl, ifile, rec )   
            return      
      end select       
      
      if ( stat > IZERO ) exit 
  
   end do   
   
   if ( stat > IZERO ) then
      call input_error ( stat, 'Failed to read the value of "'//trim(args(iexit))//'"', &
                         UERROR, Here, numl, ifile, rec, append='before' )      
   end if
         
   if ( oktype == 0 .and. narg > 0 ) then
      call input_error ( stat, 'The required argument "type" is missing in kewyword "' // &
                         'output_format"', UERROR, Here, numl, ifile, rec           )      
      return 
   end if

   do i = i0, nop
      name_outputtype(i) = util_string_low (name_outputtype(i))
      name_outputfmt (i) = util_string_low (name_outputfmt (i))
!
!-    Particular case for output type 'vnod([n1,n2,...])' or 'velm([n1,n2,...])'
!     (read and store the list of nodes or elements in nod_output, elms_output) or pnt_output
!                     
      if ( index(name_outputtype(i),'vnod(') == 1 ) then
         call input_outputNodeOrElem ( trim(adjustl(name_outputtype(i)(5:))), &
                                       nod_output, nNod_output, stat )
         error_TraceNreturn(stat>IZERO, HERE, stat)
         name_outputtype(i) = 'vnodSubSet'
      end if
      
      if (  index(name_outputtype(i),'velm(') == 1 ) then
         call input_outputNodeOrElem ( trim(adjustl(name_outputtype(i)(5:))), &
                                       elm_output, nElm_output, stat )
         error_TraceNreturn(stat>IZERO, HERE, stat)
         name_outputtype(i) = 'velmSubSet'
      end if

      if (  index(name_outputtype(i),'vpnt(') == 1 ) then
         call input_outputNodeOrElem ( trim(adjustl(name_outputtype(i)(5:))), &
                                       pnt_output(:,1), nPnt_output, stat )
         error_TraceNreturn(stat>IZERO, HERE, stat)
         name_outputtype(i) = 'vpntSubSet'
      end if      
   end do   
!
!- no output if 'none' is used (at least once) or if no argument is given:
!   
   if ( any(name_outputtype == 'none') .or. narg == 0 ) then
      do i = 1, size(name_outputtype)
         name_outputtype(i) = ''
      end do
      name_outputtype(1) = 'none'
      return
   end if   
      
   END SUBROUTINE input_outputformat


!=============================================================================================
   SUBROUTINE input_outputNodeOrElem ( str, list, nval, stat, point )
!=============================================================================================
   character(len=*), intent(in    ) :: str
   integer  (Ikind), intent(in out) :: list(:), nval
   type     (err_t), intent(in out) :: stat
   logical         , intent(in    ), optional :: point
!--------------------------------------------------------------------------------------------- 
!
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'input_outputNodeOrElem'
   integer  (Ikind)              :: lb, rb, p1, p2, i, i0, nlist, nmax, col, n(2), sign
   character(len=:), allocatable :: stmp
!--------------------------------------------------------------------------------------------- 

   sign = 1
   if ( present(point) ) then
      if ( point ) sign =-1
   end if
   
   p1 = 1 ; p2 = len_trim(str)
   
   lb = index(str,'(') ; rb = index(str,')')
   
   if ( (lb + rb /= 0 .and. lb*rb == 0) .or. lb > rb ) then
      call input_error ( stat, 'Unbalanced parentheses', UERROR, Here, numl, ifile, rec )
      return
   end if
   
   if ( lb /= 0 ) then
      if ( lb /= p1 .or. rb /=  p2 ) then
         call input_error ( stat, 'Syntax error', UERROR, Here, numl, ifile, rec )
         return
      end if
      p1 = lb+1 ; p2 = rb-1
   else
      call input_error ( stat, 'Missing parentheses', UERROR, Here, numl, ifile, rec )
      return
   end if 

   lb = index(str,'[') ; rb = index(str,']')
      
   if ( (lb + rb /= 0 .and. lb*rb == 0) .or. lb > rb ) then
      call input_error ( stat, 'Unbalanced brackets', UERROR, Here, numl, ifile, rec )
      return
   end if
         
   nlist = size(list)
   do i = 1, nlist
      if ( list(i) == 0 ) then
         i0 = i ; exit
      end if
   end do
   nmax = nlist - i0 + 1
   
   if ( nmax == 0 ) then
      call input_error ( stat, 'The maximum of '//i2a(nlist)//' nodes is reached', &
                         UERROR, Here, numl, ifile, rec )
      return
   end if

   col = index(str,':')
   
   if ( col /= 0 ) then  
!
!-    ":" is present. A set of #s of the form n1:n2 (which means n1, n1+1, ..., n2)
!     has to be stored in dom. First, remove "[" and "]" from vals(0) if necessary:
!
      if ( lb /= 0 ) then
         stmp = adjustl(str(lb+1:rb-1)) ; col = index(stmp, ':')
      end if   
!
!-    now read n1 and n2:
!
      call input_getval ( str=stmp(1:col-1), valI=n(1), nmax=IONE, nval=nval, stat=stat )
      
      if ( stat > IZERO ) then
         stmp = 'Read error of the lower bound (# of the form n1:n2)'
         call input_error ( stat, stmp, UERROR, Here, numl, ifile, rec)
         return
      end if
!
!-    do the same things for n2:
!
      call input_getval ( str=stmp(col+1:), valI=n(2), nmax=IONE, nval=nval, stat=stat )
      
      if ( stat > IZERO ) then
         stmp = 'Read error of the upper bound (# of the form n1:n2)'
         call input_error ( stat, stmp, UERROR, Here, numl, ifile, rec)
         return
      end if
         
      if ( n(2) < n(1) ) then
         nval = n(1) ; n(1) = n(2) ; n(2) = nval
      end if   
      
      nval = n(2) - n(1) + 1
      
      list(i0:i0+nval-1) = [(i,i=n(1),n(2))]
                                                                
   else       
      if ( lb == 0 ) stmp = '['//str(p1:p2)//']'
      call input_getval ( str=stmp, valI=list(i0:nlist), nmax=nmax, nval=nval, stat=stat )
      error_TraceNreturn(stat>IZERO, Here, stat)
   end if
   
   nval = i0 + nval - 1
   
   END SUBROUTINE input_outputNodeOrElem


!=============================================================================================
   SUBROUTINE input_linearsolver ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Reads the data for the keyword "linear_solver". OPTIONAL KEYWORD.
!
!  5 parameters are associated to this keyword: "lib", "meth", "storage", "verb", "opt"
!  and their corresponding values are of type :  char,  char ,  char    ,  int  ,  real array
!
!  Only the first one ("lib") is required, the 4 others are optional.
!
!  * "lib" is the name of the solver library.
!
!  * "meth" is the method name (if more than one method is present in the library). 
!
!  * "storage" is the name of the sparse format storage: csr, csrsym or skyline (if  
!     incompatible with the selected solver the default value for that solver will be used). 
!
!  * "verb" is the level of verbosity for the solver 
!     (can also be selected with the keyword "verbosity")
!
!  * "opt" is a real array containing optional parameters for iteratives solvers. They have 
!     to be given in this order:
!      
!        #1: defines the max. number of iteration
!        #2: relative tolerance
!        #3: absolute tolerance
!        #4: subspace dimension for GMRES-like methods
!        #5: fill-in parameter (ILUT)
!
!  * "reord" is the name of the reordering method 
!     (optimization of the dof numbering for bandwidth and profile reduction)
!
!  Examples:
!           
!     linear_solver: {lib='mkl', meth='fgmres', opt=[100, 1e-8], verb = 1, storage = 'csr'} 
!
!     linear_solver: {lib='superLU', verb = 2, reord = 'DG'} 
!
!
!  (case insensitive) tHere is no prescribed order, blanks have no effect, line breaks may be 
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!     linear_solver : { meth = 'FGMRES'  ,     // use fgmres method
!                       lib  = 'SPARSKIT',     // from sparskit
!                       verb = 0         ,     // keep quiet
!
!                       opt = [
!                               150    ,     // max. nbr of iterations: max(150,nbr of unknowns)
!                               1.0e-05,     // relative tolerance
!                               1.0e-10,     //  absolute tolerance
!                               50     ,     // subspace dimension for GMRES-like methods
!                               15           // fill-in parameter
!                              ]                  
!                       }
!
!  For this example, the corresponding data will be stored in name_linsolver, name_storage and 
!  optlinsol as follows:
!
!      name_linsolver = 'sparskit(fgmres)',  name_storage = 'csr' (default)
!
!      optlinsol = [150, 1e-5, 1e-10, 50, 15]
!
!
!  Inputs:
!      narg       : number of arguments found for this keyword
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!
!  Outputs:
!      name_linsolver  : the name of the solver  (saved in module param) in the form lib(meth)
!      name_storage    : the name of the storage (saved in module param)
!      name_reorderMeth: the name of the reordering method (saved in module param)
!      optlinsol       : the optional parameters (saved in module param)
!      verbosity(3)    : the level of verbosity  (saved in module param)
!-----------------------------------------------------------------------------------R.H. 12/16  
   
!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: Here = 'input_linearsolver'
   integer  (Ikind)            :: i, iexit, oksolv, okmeth, nval, nop=size(optlinsol)
   integer  (Ikind)            :: val(2)
   character(MDSTR)            :: meth
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
!
!- put the narg arguments names in lowercase
!   
   do i = 0, narg
      args(i) = util_string_low ( args(i) )
   end do    
   
   oksolv = 0; okmeth = 0
!
!- if already defined, print a warning message and
!
   if ( len_trim(name_linsolver) /= 0 ) then
      
      call input_error ( stat, &
      'Keyword "linear_solver" already used (with solver='//trim(name_linsolver)//')'//NLT// &
      'The new data will be used and all original values for options will be erased' //NLT// &
      'by new ones (if given) or replaced by their default values (if not given)',           &
       WARNING, Here, numl, ifile, rec ) 
      optlinsol(:) = -1.0_Rkind
   end if
   
   do i = 1, narg
      
      iexit = i
      
      select case ( args(i) )
      
         case ( 'lib' )
            oksolv = 1
            call input_getval ( str=vals(i), valC=name_linsolver, nmax=IONE, nval=nval, &
                                stat=stat )
            if ( stat > IZERO ) exit 
            
            name_linsolver = util_string_low ( name_linsolver )                   
         
         case ( 'meth' )
            okmeth = 1
            call input_getval ( str=vals(i), valC=meth, nmax=IONE, nval=nval, stat=stat )
            if ( stat > IZERO ) exit 
          
            meth = util_string_low ( meth )                   
       
         case ( 'storage' )
            call input_getval ( str=vals(i), valC=name_storage, nmax=IONE, nval=nval, &
                                stat=stat )  
            if ( stat > IZERO ) exit 
          
            name_storage = util_string_low ( name_storage )                   

         case ( 'verb' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat )
            if ( stat > IZERO ) exit 

            verbosity(3) = val(1)                  
            
         case( 'opt' )
            call input_getval ( str=vals(i), valR=optlinsol, nmax=nop, nval=nval, stat=stat )                       
            if ( stat > IZERO ) exit 
         
         case ( 'reord' )
            call input_getval ( str=vals(i), valC=name_reorderMeth, nmax=IONE, nval=nval, &
                                stat=stat ) 
            if ( stat > IZERO ) exit 
          
            name_reorderMeth = util_string_low ( name_reorderMeth )                   
                                                         
       case default

            call input_error ( stat, 'Unknown argument "'//trim(args(i))//'" for keyword '// &
                               'linear_solver', UERROR, Here, numl, ifile, rec )   
            return      

      end select       
             
   end do   

   if ( stat > IZERO ) then
      call input_error ( stat, 'Failed to read the value of "'//trim(args(iexit))//'"', &
                         UERROR, Here, numl, ifile, rec, append='before'                )      
      return
   end if
            
   if ( oksolv == 0 ) then
      call input_error ( stat, 'The required argument "lib" is missing in kewyword "' // &
                         'linear_solver"', UERROR, Here, numl, ifile, rec                )         
      return 
   else
      if ( okmeth /= 0 ) name_linsolver = trim(name_linsolver)//'('//trim(meth)//')'  
   end if

   END SUBROUTINE input_linearsolver
   
   
!=============================================================================================
   SUBROUTINE input_nlinearsolver ( stat )
!=============================================================================================
   type(err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Reads the data for the keyword "nlinear_solver". OPTIONAL KEYWORD.
!
!  3 parameters are associated to this keyword: "meth", "verb", "opt"
!  and their corresponding values are of type :  char,   int  ,  real array
!
!  Only the first one ("meth") is required, the 2 others are optional.
!
!  * "meth" is the name of the method.
!
!  * "verb" is the level of verbosity for the solver.
!
!  * "opt" is a real array containing optional parameters for iteratives solvers. They have 
!     to be given in this order:
!      
!        #1: defines the max. number of iteration
!        #2: relative tolerance
!        #3: absolute tolerance
!
!  Examples:
!           
!     nlinear_solver: {meth='newton', opt=[100, 1e-8], verb = 1} 
!
!
!  (case insensitive) tHere is no prescribed order, blanks have no effect, line breaks may be 
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!     nlinear_solver : { meth = 'NEWTON',     // use Newton's method
!                        verb = 1       ,     // print ||dU|| and ||U|| during iterations
!
!                       opt = [
!                               50     ,     // max. nbr of iterations
!                               1.0e-08,     // convergence criteria: ||dU|| < reltol*||U||
!                               1.0e-10      // or if ||dU|| < abstol
!                              ]                  
!                       }
!
!  Inputs
!
!     narg       : number of arguments found for this keyword
!     args       : strings corresponding to the argument names
!     vals       : strings corresponding to their values 
!
!  Outputs
!
!     name_nlinsolver: the name of the solver (saved in module param) 
!     optnlinsol     : the optional parameters (saved in module param) with optnlinsol(1)=verb
!-----------------------------------------------------------------------------------R.H. 12/16  
   
!- local variables ---------------------------------------------------------------------------
   character(len=*  ), parameter :: Here = 'input_nlinearsolver'
   integer  (Ikind  )            :: i, oksolv, nval, iexit, nop=size(optnlinsol)
   integer  (Ikind  )            :: val(2)
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
!
!- put the narg argument names in lowercase
!   
   do i = 0, narg
      args(i) = util_string_low ( args(i) )
   end do      
   
   oksolv = 0
!
!- if already defined, print a warning message and
!
   if ( len_trim(name_nlinsolver) /= 0 ) then
      call input_error ( stat, & 
      'Keyword "nlinear_solver" already used (with solver='//trim(name_nlinsolver)//')'//    &
       NLT//'The new data will be used and all original values for options will be erased'// &
       NLT//'by new ones (if given) or replaced by their default values (if not given)',     &
       WARNING, Here, numl, ifile, rec )      
      optnlinsol(:) = -1.0_Rkind
   end if
   
   do i = 1, narg
      
      iexit = i
      
      select case( args(i) )
               
         case ( 'meth' )
            oksolv = 1
            call input_getval ( str=vals(i), valC=name_nlinsolver, nmax=IONE, nval=nval, &
                               stat=stat )  
            if ( stat > IZERO ) exit 

            name_nlinsolver = util_string_low ( name_nlinsolver )
          
         case ( 'verb' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat ) 
            if ( stat > IZERO ) exit 

            verbosity(4) = val(1)                      
            
         case ( 'opt' )
            call input_getval ( str=vals(i), valR=optnlinsol, nmax=nop, nval=nval, stat=stat )     
            if ( stat > IZERO ) exit 
                            
         case default
            call input_error ( stat, 'Unknown argument "'//trim(args(i))//'" for keyword '// &
                               'nlinear_solver', UERROR, Here, numl, ifile, rec              )   
            return               
      end select       
             
   end do   

   if ( stat > IZERO ) then
      call input_error ( stat, 'Failed to read the value of "'//trim(args(iexit))//'"', &
                         UERROR, Here, numl, ifile, rec, append='before'                )      
      return
   end if
   
   if ( oksolv == 0 ) then
      call input_error ( stat, 'The required argument "meth" is missing in kewyword "' // &
                         'linear_solver"', UERROR, Here, numl, ifile, rec                 )            
      return    
   end if
                     
   END SUBROUTINE input_nlinearsolver
   
       
!=============================================================================================
   SUBROUTINE input_mesh ( mymesh, stat )
!=============================================================================================
   type(mesh_t), intent(in out) :: mymesh
   type(err_t ), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Reads the data for the keyword "mesh". REQUIRED KEYWORD.
!
!  Three parameters are associated to this keyword : "file",  "elt"   and   "nip"
!  and their corresponding values are of type      : string,  string  and  integer
!
!  The parameter "file" (name of the file containing the mesh) is required while "elt" (the
!  finite element name) and "nip" (the number of integration points) are optional. 
!
!  If "elt" is not given by the user, a default value will be deduced from the mesh
!  If "nip" is not given by the user, a default value will be deduced form "elt" 
!
!  Example:
!            mesh: { elt = 'tetr10' , file='earth.msh' , nip = 7 }  
!
!  (case insensitive) tHere is no prescribed order, blanks have no effect, line breaks may be 
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!            mesh: { 
!                     elt = 'Tetr10',     // 10-noded tetra
!
!                     FILE = 'earth.msh', 
!
!                     nip  = 7            // number of I.P.
!                  }
!
!  Inputs:
!      narg       : number of arguments found for this keyword
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!
!  Outputs:
!      name_mesh_file : the name of the mesh file (saved in module param) 
!      name_element   : the name of the finite element (saved in module param)
!      nipts          : the number of I.P. (saved in module param)
!      mymesh         : the mesh structure
!-----------------------------------------------------------------------------------R.H. 12/16  
   
!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'input_mesh'
   integer  (Ikind)              :: i, iexit, nval, val(2)
   character(len=:), allocatable :: msg
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
!
!- put the narg argument names in lowercase:
!   
   do i = 0, narg
      args(i) = util_string_low ( args(i) )
   end do      
!
!- if the mesh is already defined, print an error message and stop
!
   if ( len_trim(name_mesh_file) /= 0 ) then
      msg = '"Mesh" was previously used (More than one mesh cannot be considered)'
      call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )   
      return      
   end if
   
!
!- if args(0) /= '', print a warning message and continue
!  (in a future implementation, args(0) may serve to define the # of mesh in case of
!   multi meshes, e.g., mesh(#=5): {file=...} )
!
   if ( len_trim(args(0)) /= 0 ) then
      msg =  'Identifier "'//trim(args(0))//'" ignored for the keyword "Mesh"'
      call input_error ( stat, msg, WARNING, Here, numl, ifile, rec )   
   end if   
   
   do i = 1, narg
      
      iexit = i
      
      select case ( args(i) )
      
         case ( 'file' )
            call input_getval ( str=vals(i), valC=name_mesh_file, nmax=IONE, nval=nval, &
                                stat=stat )  
            if ( stat > IZERO ) exit 
       
         case ( 'elt' )
            call input_getval ( str=vals(i), valC=name_element, nmax=IONE, nval=nval, &
                                stat=stat ) 
            if ( stat > IZERO ) exit 
                             
            name_element = util_string_low ( name_element )                   
       
         case ( 'nip' )
            call input_getval ( str=vals(i), valI=val, nmax=IONE, nval=nval, stat=stat ) 
            if ( stat > IZERO ) exit 

            nipts = val(1)                           
            if ( nipts <= 0 ) then  
               msg = 'The number of integration (nip) points must be a positive integer'
               call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
               return                      
            end if          
       
         case default
            msg = 'Unknown argument "' // trim(args(i)) // '" for keyword mesh'
            call input_error ( stat, msg , UERROR, Here, numl, ifile, rec )   
            return      
      end select       
             
   end do   

   if ( stat > IZERO ) then
      msg = 'Failed to read the value of "'//trim(args(iexit))//'"'
      call input_error ( stat, msg, UERROR, Here, numl, ifile, rec, append='before' )      
      return
   end if
   
   if ( len_trim(name_mesh_file) == 0 ) then
      msg = 'The required argument "file" is missing in kewyword "Mesh"' // NLT // &
            'or the value argument of this is not enclosed between quotes'
      call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )                                     
      return       
   end if      
    
!
!- Read the mesh from the file "name_mesh_file":
!   
   call input_readmesh ( mymesh, stat ) ; error_TraceNreturn(stat>IZERO, Here, stat)
          
   END SUBROUTINE input_mesh


!=============================================================================================
   SUBROUTINE input_matprop ( mydata, stat )
!=============================================================================================
   type(udata_t), intent(in out) :: mydata
   type(err_t  ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Reads the data for the keyword "matprop". REQUIRED KEYWORD.
!
!  Two parameters are associated to this keyword : "domain", "val"
!  and their corresponding values are of type    :  integer,  real array
!
!  Example:
!            matprop(domain=6): {val = [1e10, 0.25, 2000] }  
!
!  (case insenstive) tHere is no prescribed order, blanks have no effect, line breaks may be
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!            Matprop (domain = 6): { 
!
!                                   Val = [ 1e10,   // Young's modulus
!                                           0.25,   // Poisson's coefficient
!                                           3000 ]  // Density
!                                  }
!
!  Alternatively, the assignment may concerns a set of sub-domains (use of ":"). For example
!
!            matprop ( domain = 3:6 ) :   {val = [1e10, 0.25, 2000] }  
!
!  the sub-domains #3, #4, #5, #6 will share the same material properties.
!
!  Inputs:
!      narg       : number of arguments found for this keyword (normally narg=1 at present)
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!      subkeywd   : the part of the record corresponding to the string between "(" and ")"
!                   giving the domain #
!
!  Outputs:
!      nmats           : the number of sub-domains (saved in module param) 
!      mydata%lmatprop : the # of these sub-domaines
!      mydata%vmatprop : the values of the material properties
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'input_matprop'
   integer  (Ikind), parameter   :: ncmpmax = 100, ndommax = 100
   integer  (Ikind)              :: valI(ncmpmax), dom(ndommax)
   real     (Rkind)              :: valR(ncmpmax)
   integer  (Ikind)              :: i, j, k, col, n(2), lb, rb, nval, nmat0, okval, ndom, iexit
   character(len=:), allocatable :: msg
!---------------------------------------------------------------------------------------------
 
    if ( stat > IZERO ) return    
!
!- put the narg argument names in lowercase:
!   
   do i = 0, narg
      args(i) = util_string_low( args(i) )
   end do   
   
!
!  -----------------------------------------------
!- 1) Read the corresponding #(s) of sub-domain(s)
!  -----------------------------------------------

   if ( index(args(0),'domain') == 0 ) then
      call input_error ( stat, 'Missing identifier "domain" for the keyword matprop', &
                         UERROR, Here, numl, ifile, rec )
      return
   end if

!
!- check the presence of ":" (it can appear in expression like "domain = 2:5")
!   
   col = index(vals(0),':')
   
   if ( col == 0 ) then
   
      call input_getval ( str=vals(0), valI=valI, nmax=ncmpmax, nval=ndom, stat=stat )  
      
      if ( stat > IZERO ) then
         call input_error ( stat, 'Failed to read the domain ID of "matprop"', &
                            UERROR, Here, numl, ifile, rec, append='before' )
         return
      end if
             
      dom(1:ndom) = valI(1:ndom)                   
         
   else
!
!-    ":" is present. A set of #s of the form n1:n2 (which means n1, n1+1, ..., n2)
!     has to be stored in dom. First, remove "[" and "]" from vals(0) if necessary:
!
      lb = index(vals(0),'[') ; rb = index(vals(0),']')
      
      if ( (lb + rb /= 0 .and. lb*rb == 0) .or. lb > rb ) then
         call input_error ( stat, 'Unbalanced brackets', UERROR, Here, numl, ifile, rec )
         return
      end if
         
      if ( lb /= 0 ) then
         vals(0) = adjustl(vals(0)(lb+1:rb-1)) ; col = index(vals(0), ':')
      end if   
!
!-    now read n1 and n2:
!
      call input_getval ( str=vals(0)(1:col-1), valI=n(1), nmax=IONE, nval=ndom, stat=stat )
      
      if ( stat > IZERO ) then
         msg = 'Read error of the lower bound (# of domains in the form n1:n2)'
         call input_error ( stat, msg, UERROR, Here, numl, ifile, rec)
         return
      end if
!
!-    do the same things for n2:
!
      call input_getval ( str=vals(0)(col+1:), valI=n(2), nmax=IONE, nval=ndom, stat=stat )
      
      if ( stat > IZERO ) then
         msg = 'Read error of the upper bound (# of domains in the form n1:n2)'
         call input_error ( stat, msg, UERROR, Here, numl, ifile, rec)
         return
      end if
         
      if ( n(2) < n(1) ) then
         ndom = n(1) ; n(1) = n(2) ; n(2) = ndom
      end if   
      
      ndom = n(2) - n(1) + 1 ; dom(1:ndom) = [(i,i=n(1),n(2))]  ! ou [n1:n2] mais ne marche 
                                                                ! pas avec gfortran !
   end if     
       
   nmat0 = nmats ; nmats = nmat0 + ndom   
!
!- add the new ndom # of sub-domains in mydata % lmatprop (allocate or re-allocate it):
!      

  !-----------------------------------------------------------------------------!
  !                             A L L O C A T I O N                             !
  !-----------------------------------------------------------------------------! 
  ! allocate mydata%lmatprop:                                                   !
  !                                                                             !  
   call util_alloc ( 's',mydata%lmatprop, nmats, stat )                           !
   error_TraceNreturn(stat>IZERO, Here, stat)
  !-----------------------------------------------------------------------------!
  
   do i = 1, ndom
      j = dom(i)
!
!-    check if the sub-domain #j is not already used:
!
      do k = 1, nmat0
         if ( mydata%lmatprop(k) == j ) then
            call input_error ( stat, 'The domain #'//i2a(j)//' was previously used', &
                               UERROR, Here, numl, ifile, rec )
            return         
         end if        
      end do
!
!-    store this # in lmatprop:
!                
      mydata%lmatprop(nmat0+i) = j
   end do   
                      
!
!  ---------------------------------------------
!- 2) Read the values assigned to each arguments
!  ---------------------------------------------
!
   okval = 0
   do i = 1, narg
      
      iexit = i
      
      select case ( args(i) )
      
         case ( 'val' )
            okval = 1
    
            call input_getval ( str=vals(i), valR=valR, nmax=ncmpmax, nval=nval, stat=stat )
            if ( stat > IZERO ) exit

            npros = nval            
!
!-          read and store these values in mydata % vmatprop (allocate or re-allocate it)
!
           !--------------------------------------------------------------------------------!
           !                                A L L O C A T I O N                             !
           !--------------------------------------------------------------------------------! 
           ! allocate mydata%vmatprop:                                                      !
           !                                                                                !  
            call util_alloc ( 's',mydata%vmatprop,npros,nmats,stat )                          !
            error_TraceNreturn(stat>IZERO, Here, stat)
           !--------------------------------------------------------------------------------!

            do k = 1, ndom
               mydata%vmatprop(1:nval,nmat0+k) = valR(1:nval)
            end do
                     
         case default 
            msg = 'Unknown parameter "' // trim(args(i)) // '" for keyword matprop'
            call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )   
            return
                  
      end select
             
   end do     

   if ( stat > IZERO ) then
      call input_error ( stat, 'Failed to read the value of "'//trim(args(iexit))//'"', &
                         UERROR, Here, numl, ifile, rec, append='before'          )      
      return   
   end if   
      
   if ( okval == 0 ) then
      call input_error ( stat, 'Required argument "val" missing in the keyword "matprop"', &
                          UERROR, Here, numl, ifile, rec)
      return   
   end if   
   
   END SUBROUTINE input_matprop


!=============================================================================================
   SUBROUTINE input_dirichlet ( mydata, stat )
!=============================================================================================
   type(udata_t), intent(in out) :: mydata
   type(err_t  ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Reads the data for the keyword "dirichlet". OPTIONAL KEYWORD.
!
!  Four parameters are associated to this keyword : "face",   "line",  "point", "dof"
!  and their corresponding values are of type     :  integer,  integer, integer, real
!
!  The parameter "dof" has its own argument of type integer or character*1:
!
!    dof(i) with "i" an integer (1, 2,...) indictating wich d.o.f # in the global frame is
!    prescribed
!
!  or
!
!    dof(c) with "c" a character*1 (= n or t) meaning that the normal (n) or the tagential (t)
!    d.o.f in the local frame of the object (point, line face) is prescribed (only for 
!    particular physical problem).
!
!         !!! POUR L'INSTANT QU'EN 2D. A VOIR COMMENT FAIRE ÇA EN 3D !!!
!         !!! EN 3D PAS DE SOUCIS POUR LA COMPOSANTE NORMALE         !!!
!         !!! REFLECHIR POUR LES COMPOSANTES TANGENTIELLES           !!!        
!
!  Examples:
!            - dirichlet(face=2): {dof(1) = 3.17e-10, dof(2) = 0.0 } 
! 
!            - dirichlet(point=10): {dof(3) =-5e-11}  
!
!            - dirichlet(face=2): {dof(n) = 3.17e-10, dof(t) = 0.0 } 
!
!  (case insenstive) tHere is no prescribed order, blanks have no effect, line breaks may be
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!            Dirichlet (face = 2): { 
!                                                       // on the face #2:
!                                   dof(1) = 3.17e-10   // vx is prescribed to 1 cm/y
!                                   dof(2) = 0.0        // vy is prescribed to 0 cm/y 
!                                                       // vz is free
!                                  }
!
!  Alternatively, the assignment may concerns a set of entities:
!
!  1) by use of ":", e.g.
!
!            dirichlet ( face = 2:5 ) : {dof(1) = 3.17e-10, dof(2) = 0.0 }   
!
!  then the faces #2, #3, #4 and #5 will have the same bc
!
!  2) by use of "[" ”]", e.g.
!
!            dirichlet ( face = [1,3,8] ) : {dof(1) = 3.17e-10, dof(2) = 0.0 }   
!
!  then the faces #1, #3 and #8 will have the same bc
!
!  3) by use of a user-defined variable
!
!            dirichlet ( face = fbcdir ) : {dof(1) = 3.17e-10, dof(2) = 0.0 }   
!
!  where fbcdir is a name of a variable previously defined by a "var" assignement.  
!  The value of this variable can be a single integer, an array of integers (using "[]” or
!  ":") or the name of another existing variable:
!
!           var : { fbcdir = 2 }  
!      or   var : { fbcdir = 2:5 } 
!      or   var : { fbcdir = [1,3,8] }
!      or   var : { fbcdir = f1 }  
! 
!
!  Inputs:
!      narg       : number of arguments found for this keyword
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!
!  Outputs:
!      ndiri            : the number of dirichlet bc on geometric objects (saved in module param) 
!      mydata%ldirchlet : the # of these objects
!      mydata%odirchlet : the corresponding objects type ('F': face, 'L': line, 'P': point) 
!      mydata%vdirchlet : the precribed values 
!      mydata%codediri  : codes of imposition 
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'input_dirichlet'
   integer  (Ikind), parameter   :: ncmpmax = 100, one = 1
   integer  (Ikind)              :: valI(ncmpmax), bcnum(ncmpmax)
   real     (Rkind)              :: valR(ncmpmax)
   integer  (Ikind)              :: i, j, k, col, n(2), newm, lp, rp, nval, ndir0, err, &
                                    idof, icode, face, line, point, iexit
   character(len=6)              :: cdof, obj
   character(len=:), allocatable :: msg
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return     
!
!- put the narg argument names in lowercase:
!   
   do i = 0, narg
      args(i) = util_string_low( args(i) )
   end do   
   
!
!- 1) Read the corresponding #(s) of faces(s), line(s) or point(s)
!
   obj = ''
   face  = index(args(0),'face' ) ; if (face  /= 0) obj = 'face '
   line  = index(args(0),'line' ) ; if (line  /= 0) obj = 'line '
   point = index(args(0),'point') ; if (point /= 0) obj = 'point'
   
   if ( len_trim(obj) == 0 ) then
      msg = 'Missing  argument "face", "line" or "point" for the keyword "Dirichlet"'
      call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
      return
   end if
      
   col = index(vals(0),':')
   
   if ( col == 0 ) then
   
      call input_getval ( str=vals(0), valI=valI, nmax=ncmpmax, nval=nval, stat=stat )
      
      if ( stat > IZERO ) then
         call input_error ( stat, 'Read error for the '//trim(obj)//' Id', &
                            UERROR, Here, numl, ifile, rec, append = 'before' )
         return
      end if

      bcnum(1:nval) = valI(1:nval)                 
         
   else
!
!-    ":" is present. A set of #s of the form n1:n2 (which means n1, n1+1, ..., n2)
!     has to be stored in dom. First, remove "[" and "]" from vals(0) if necessary:
!
      lp = index(vals(0),'[') ; rp = index(vals(0),']')
      
      if ( (lp + rp /= 0 .and. lp*rp == 0) .or. lp > rp ) then
         call input_error ( stat, 'Unbalanced brackets', UERROR, Here, numl, ifile, rec )
         return
      end if
         
      if ( lp /= 0 ) then
         vals(0) = adjustl(vals(0)(lp+1:rp-1)) ; col = index(vals(0), ':')
      end if   
!
!-    now read n1 and n2:
!
      call input_getval ( str=vals(0)(1:col-1), valI=n(1), nmax=one, nval=nval, stat=stat )
      
      if ( stat > IZERO ) then
         msg = 'Read error of the lower bound (# of bc objects in the form n1:n2)'
         call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
         return
      end if      
!
!-    do the same things for n2:
!
      call input_getval ( str=vals(0)(col+1:) , valI=n(2), nmax=one, nval=nval, stat=stat )

      if ( stat > IZERO ) then
         msg = 'Read error of the upper bound (# of bc objects in the form n1:n2)'
         call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
         return
      end if      
         
      if ( n(2) < n(1) ) then
         nval = n(1) ; n(1) = n(2) ; n(2) = nval
      end if   
      
      nval = n(2) - n(1) + 1 ; bcnum(1:nval) = [(i,i=n(1),n(2))]  ! ou [n1:n2] mais ne marche 
                                                                  ! pas avec gfortran !         
   end if         
   
   newm = nval ; ndir0 = ndiri ; ndiri = ndir0 + newm   
!
!- add the newm # of objects in mydata % ldirichl (allocate or re-allocate it):
!      

  !-----------------------------------------------------------------------------------!
  !                               A L L O C A T I O N S                               !
  !-----------------------------------------------------------------------------------! 
  ! allocate mydata%ldirichl:                                                         !
  !                                                                                   !  
   call util_alloc ('s',mydata%ldirichl, ndiri, stat)                                 !
   error_TraceNreturn(stat>IZERO, Here, stat)
   call util_alloc ('s',mydata%odirichl, ndiri, one,stat)                             !
   error_TraceNreturn(stat>IZERO, Here, stat)
  !-----------------------------------------------------------------------------------!
  
   do i = 1, newm
      j = bcnum(i)
!
!-    check if the face #j was not already used:
!
!         !!! PEUT-ETRE PAS VRAIMENT DANGEREUX SI DECLARE           !!!
!         !!! PLUS D'UNE FOIS MAIS DE FACON COMPLEMENTAIRE.. A VOIR !!!
!
      do k = 1, ndir0
         if ( mydata%ldirichl(k) == j .and. mydata%odirichl(k) == obj(1:1) ) then
            msg =  'The '//trim(obj)//' #'//i2a(j)//' was previously used'
            call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
            return
         end if        
      end do
!
!-    store this # in ldirichl and reference type object in odirichl:
!                
      mydata%ldirichl(ndir0+i) = j ; mydata%odirichl(ndir0+i) = obj(1:1)
   end do   
                      
!
!- 2) Read the values assigned to each arguments (at the moment, only one: dof)
!
   do i = 1, narg
   
      iexit = i
      
      select case ( args(i)(1:3) )
      
         case ( 'dof' )
!
!-          read the dof # located between "(" and ")" after the arg "dof"
!           At the moment, a valid dof # is one of : n, t, 1, 2, 3, ...
!
            lp = index(args(i),'(') ; rp = index(args(i),')')         
            
            if ( lp == 0 .or. rp == 0 .or. lp > rp ) then
               call input_error ( stat, 'Unbalanced parentheses for argument "dof"', &
                                  UERROR, Here, numl, ifile, rec )
               return
            end if
            
            call input_getval ( str=args(i)(lp+1:rp-1), valC=cdof, nmax=one, nval=nval, &
                                stat=stat )
            if ( stat > IZERO ) exit
            
            cdof = util_string_low (cdof)                   
                               
            select case ( cdof )
               case ( '1':'9' )
                  icode = 1
                  read(cdof,*,iostat=err) idof
                  if (err /= 0) then
                     msg = 'Read error for the dof identifier'
                     call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
                     return
                  end if   
               case ( 'n','t' )   
                  icode = 2
                  if (cdof == 'n') idof = 1
                  if (cdof == 't') idof = 2
               case default
                  icode = 0
                  msg = '"'//trim(cdof)//'" is not a valid dof identifier'
                  call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
                  return
            end select      
            
            ndofD = max(ndofD,idof)
!
!-          read the prescribed value:
!
            call input_getval ( str=vals(i), valR=valR, nmax=one, nval=nval, stat=stat )
            if ( stat > IZERO ) exit
!
!-          store the value in mydata % vdirichl (allocate or re-allocate it)
!           and imposition code in mydata % codediri (allocate or re-allocate it)
!
           !---------------------------------------------------------------------------------!
           !                                A L L O C A T I O N S                            !
           !---------------------------------------------------------------------------------! 
           ! allocate mydata%vdirichl and mydata%codediri                                    !
           !                                                                                 !  
            call util_alloc ('s',mydata%vdirichl,ndofD,ndiri,stat)                           !
            error_TraceNreturn(stat>IZERO, Here, stat)
            call util_alloc ('s',mydata%codediri,ndofD,ndiri,stat)                           !
            error_TraceNreturn(stat>IZERO, Here, stat)
           !---------------------------------------------------------------------------------!

            if ( mydata%codediri(idof,ndir0+1) /= 0 ) then
!
!-             this dof was already used: print an error message and stop
!            
               msg = 'The dof #'//trim(cdof)//' used more than once'
               call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
               return
            end if   
                                    
            mydata%vdirichl(idof,ndir0+1:ndir0+newm) = valR(1)
            mydata%codediri(idof,ndir0+1:ndir0+newm) = icode
                 
         case default 
            msg = 'Unknown parameter "'//trim(args(i))//'" for keyword dirichlet'
            call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
            return
      
      end select
             
   end do     

   if ( stat > IZERO ) then
      call input_error ( stat, 'Failed to read the value of "'//trim(args(iexit))//'"', &
                         UERROR, Here, numl, ifile, rec, append='before' )      
      return   
   end if      
   
   END SUBROUTINE input_dirichlet


!=============================================================================================
   SUBROUTINE input_neumann ( mydata, stat )
!=============================================================================================
   type(udata_t), intent(in out) :: mydata
   type(err_t  ), intent(in out) :: stat      
!---------------------------------------------------------------------------------------------
!  Reads the data for the keyword "neumann". OPTIONAL KEYWORD.
!
!  Four parameters are associated to this keyword : "face",   "line",  "point", "dof"
!  and their corresponding values are of type     :  integer,  integer, integer, real
!
!  The parameter "dof" has its own argument of type integer or character*1:
!
!    dof(i) with "i" an integer (1, 2,...) indictating wich d.o.f # in the global frame is
!    prescribed
!
!  or
!
!    dof(c) with "c" a character*1 (= n or t) meaning that the normal (n) or the tagential (t)
!    d.o.f in the local frame of the face is prescribed (only for particular physical problem).
!
!         !!! POUR L'INSTANT QU'EN 2D. A VOIR COMMENT FAIRE ÇA EN 3D !!!
!         !!! EN 3D PAS DE SOUCIS POUR LA COMPOSANTE NORMALE         !!!
!         !!! REFLECHIR POUR LES COMPOSANTES TANGENTIELLES           !!!        
!
!  Examples:
!            - neumann(line=2): {dof(1) = 5e8, dof(2) =-1e8 } 
! 
!            - neumann(point=10): {dof(3) =-2e10}  
!
!            - neumann(face=2): {dof(n) =-5e8} 
!
!  (case insenstive) tHere is no prescribed order, blanks have no effect, line breaks may be
!  used, blank lines may be inserted and comments (symb "//" or "!") may be added (at the eol):
!
!            Neumann (face = 2): { 
!                                                       // the face #2 submitted to 
!                                   dof(1) = 5e8        //  500 MPa on the x direction
!                                   dof(2) =-1e8        // -100 MPa on the y direction 
!                                                       //  0   MPa on the z direction
!                                  }
!
!  Alternatively, the assignment may concerns a set of entities:
!
!  1) by use of ":", e.g.
!
!            Neumann ( face = 2:5 ) : {dof(1) = 3e8, dof(2) = 0.0 }   
!
!  then the faces #2, #3, #4 and #5 will have the same bc
!
!  2) by use of "[" ”]", e.g.
!
!            Neumann ( face = [1,3,8] ) : {dof(1) = 3e8, dof(2) = 0.0 }   
!
!  then the faces #1, #3 and #8 will have the same bc
!
!  3) by use of a user-defined variable
!
!            Neumann ( face = fbcneum ) : {dof(1) = 3e8, dof(2) = 0.0 }   
!
!  where fbcneum is a name of a variable previously defined by a "var" assignement.  
!  The value of this variable can be a single integer, an array of integers (using "[]” or
!  ":") or the name of another existing variable:
!
!           var : { fbcneum = 2 }  
!      or   var : { fbcneum = 2:5 } 
!      or   var : { fbcneum = [1,3,8] }
!      or   var : { fbcneum = f1 }  
!
!
!  Inputs:
!      narg       : number of arguments found for this keyword
!      args       : strings corresponding to the argument names
!      vals       : strings corresponding to their values 
!
!  Outputs:
!      nneum           : the number of neumann bc on geometric objects (saved in module param) 
!      mydata%lneumann : the # of these objects
!      mydata%oneumann : the corresponding objects type ('F': face, 'L': line, 'P': point) 
!      mydata%vneumann : the precribed values 
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'input_neumann'
   integer  (Ikind), parameter   :: ncmpmax = 100, one = 1
   integer  (Ikind)              :: valI(ncmpmax), bcnum(ncmpmax)
   real     (Rkind)              :: valR(ncmpmax)
   integer  (Ikind)              :: i, j, k, col, n(2), newm, lp, rp, nval, nneu0, err, &
                                    idof, jdof, icode, jcode, face, line, point, iexit
   character(len=6)              :: cdof, obj
   character(len=:), allocatable :: msg
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return     
!
!- put the narg argument names in lowercase:
!   
   do i = 0, narg
      args(i) = util_string_low( args(i) )
   end do   
!
!- 1) Read the corresponding #(s) of faces(s), line(s) or point(s)
!
   obj = ''
   face  = index(args(0),'face') ; if (face  /= 0) obj = 'face'
   line  = index(args(0),'line') ; if (line  /= 0) obj = 'line'
   point = index(args(0),'point'); if (point /= 0) obj = 'point'
   
   if ( len_trim(obj) == 0 ) then
      msg = 'Missing  argument "face", "line" or "point" for the keyword "Neumann"'
      call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
      return
   end if
      
   col = index(vals(0),':')
   
   if ( col == 0 ) then
   
      call input_getval ( str=vals(0), valI=valI, nmax=ncmpmax, nval=nval, stat=stat )

      if ( stat > IZERO ) then
         call input_error ( stat, 'Read error for the '//trim(obj)//' Id', &
                            UERROR, Here, numl, ifile, rec )
         return
      end if

      bcnum(1:nval) = valI(1:nval)               
         
   else
!
!-    ":" is present. A set of #s of the form n1:n2 (which means n1, n1+1, ..., n2)
!     has to be stored in dom. First, remove "[" and "]" from vals(0) if necessary:
!
      lp = index(vals(0),'[') ; rp = index(vals(0),']')
      
      if ( (lp + rp /= 0 .and. lp*rp == 0) .or. lp > rp ) then
         call input_error ( stat, 'Unbalanced brackets', UERROR, Here, numl, ifile, rec )
         return
      end if
      
      if ( lp /= 0 ) then
         vals(0) = adjustl(vals(0)(lp+1:rp-1)) ; col = index(vals(0), ':')
      end if   
!
!-    now read n1 and n2:
!
      call input_getval ( str=vals(0)(1:col-1), valI=n(1), nmax=one, nval=nval, stat=stat )
      
      if ( stat > IZERO ) then
         msg = 'Read error for the lower bound (Id of bc objects in the form n1:n2)'
         call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
         return
      end if     
!
!-    do the same things for n2:
!
      call input_getval ( str=vals(0)(col+1:) , valI=n(2), nmax=one, nval=nval, stat=stat )

      if ( stat > IZERO ) then
         msg = 'Read error for the upper bound (Id of bc objects in the form n1:n2)'
         call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
         return
      end if     
         
      if ( n(2) < n(1) ) then
         nval = n(1) ; n(1) = n(2) ; n(2) = nval
      end if   
      
      nval = n(2) - n(1) + 1 ; bcnum(1:nval) = [(i,i=n(1),n(2))]  ! ou [n1:n2] mais ne marche 
                                                                  ! pas avec gfortran !         
   end if         
   
   newm = nval ; nneu0 = nneum ; nneum = nneu0 + newm   
!
!- add the newm # of objects in mydata % lneumann (allocate or re-allocate it):
!      

  !----------------------------------------------------------------------------------!
  !                              A L L O C A T I O N S                               !
  !----------------------------------------------------------------------------------!
  ! allocate mydata%lneumann and mydata%oneumann:                                    !
  !                                                                                  !  
   call util_alloc ('s',mydata%lneumann, nneum, stat)                                !
   error_TraceNreturn(stat>IZERO, Here, stat)
   call util_alloc ('s',mydata%oneumann, nneum, one, stat)                           !
   error_TraceNreturn(stat>IZERO, Here, stat)
  !----------------------------------------------------------------------------------!
  
   do i = 1, newm
      j = bcnum(i)
!
!-    check if the face #j is not already used:
!
!         !!! PEUT-ETRE PAS VRAIMENT DANGEREUX SI DECLARE           !!!
!         !!! PLUS D'UNE FOIS MAIS DE FACON COMPLEMENTAIRE.. A VOIR !!!
!
      do k = 1, nneu0
         if ( mydata%lneumann(k) == j .and. mydata%oneumann(k) == obj(1:1) ) then    
            msg = 'The '//trim(obj)//' #'//i2a(j)//' was previously used'
            call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
            return
         end if        
      end do
!
!-    store this # lneumann and reference type object in oneumann:
!                
      mydata%lneumann(nneu0+i) = j ; mydata%oneumann(nneu0+i) = obj(1:1)
   end do   
                      
!
!- 2) Read the values assigned to each arguments
!
   do i = 1, narg
   
      iexit = i
      
      select case ( args(i)(1:4) )
      
         case ( 'fval' )
!
!-          read the dof # located between "(" and ")" after the arg "fval"
!
            lp = index(args(i),'(') ; rp = index(args(i),')')         
            
            if ( lp == 0 .or. rp == 0 .or. lp > rp ) then
               msg = 'Unbalanced parentheses for argument "fval"'
               call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
               return
            end if

            call input_getval ( str=args(i)(lp+1:rp-1), valC=cdof, nmax=one, nval=nval, &
                                stat=stat )
            if ( stat > IZERO ) exit
                               
            cdof = util_string_low (cdof)
                               
            select case ( cdof )
               case ( '1':'9' )
                  icode = 1
                  read(cdof,*,iostat=err) idof
                  if ( err /= 0 ) then
                     call input_error ( stat, 'Read error of the dof identifier', &
                                        UERROR, Here, numl, ifile, rec )
                     return             
                  end if      
               case ( 'n','t' )   
                  icode = 2
                  if ( cdof == 'n' ) idof = 1
                  if ( cdof == 't' ) idof = 2
               case default
                  icode = 0
                  msg = '"'//trim(cdof)//'" is not a valid fval identifier'
                  call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
                  return             
            end select      
            
            ncmpM = max(ncmpM,idof)
!
!-          read the prescribed value:
!
            call input_getval ( str=vals(i), valR=valR, nmax=one, nval=nval, stat=stat )
            
            if ( stat > IZERO ) exit
!
!-          store the value in mydata % vneumann (allocate or re-allocate it)
!           and imposition code in mydata % codeneum (allocate or re-allocate it)
!
           !--------------------------------------------------------------------------------!
           !                               A L L O C A T I O N S                            !
           !--------------------------------------------------------------------------------! 
           ! allocate mydata%vdirichl and mydata%codeneum                                   !
           !                                                                                !  
            call util_alloc ('s',mydata%vneumann,ncmpM,nneum,stat)                          !
            error_TraceNreturn(stat>IZERO, Here, stat)
            call util_alloc ('s',mydata%codeneum,ncmpM,nneum,stat)                          !
            error_TraceNreturn(stat>IZERO, Here, stat)
           !--------------------------------------------------------------------------------!
            
            if ( mydata%codeneum(idof,nneu0+1) /= 0 ) then
!
!-             this dof was already used: print an error message and return
!            
               call input_error ( stat, 'The fval #'//trim(cdof)//' used more than once', &
                                  UERROR, Here, numl, ifile, rec )
               return
            end if   
            
            do jdof = 1, ncmpM
               if ( jdof /= idof ) then
                  jcode = mydata%codeneum(jdof,nneu0+1)
                  if ( jcode /= 0 .and. jcode /= icode ) then
                     msg = 'The Neumann components must be expressed in the same frame'
                     msg = msg // NLT // '(you cannot mixte "n","t" with "1","2",..)'
                     call input_error ( stat, msg, UERROR, Here, numl, ifile, rec )
                     return
                  end if
               end if
            end do         
            
            mydata%vneumann(idof,nneu0+1:nneu0+newm) = valR(1)
            mydata%codeneum(idof,nneu0+1:nneu0+newm) = icode
                 
         case default 
            msg = 'Unknown parameter "'//trim(args(i))//'" for keyword neumann'
            call input_error ( stat, msg, UERROR, Here, numl, ifile, rec)
            return      
      end select
             
   end do   
   
   if ( stat > IZERO ) then
      call input_error ( stat, 'Failed to read the value of "'//trim(args(iexit))//'"', &
                         UERROR, Here, numl, ifile, rec, append='before'          )      
      return
   end if        
   
   END SUBROUTINE input_neumann


!=============================================================================================
   SUBROUTINE input_check ( mydata, mymesh, stat )
!=============================================================================================
   type(udata_t), intent(in out) :: mydata
   type(mesh_t ), intent(in out) :: mymesh
   type(err_t  ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   character(len=*   ), parameter   :: Here = 'input_check'
   character(len=6   )              :: obj
   character(MDSTR   )              :: msg(20), valC(10)
   character(len=:   ), allocatable :: errmsg
   integer  (Ikind   )              :: i, j, iref, jref, iok, err, nv, frame
   type     (linsys_t)              :: dum
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return       
!
!- get informations about the selected physical problem (space dimension, nbr of mat. prop.,..)
!
   msg = ''
   
   call models_driver ( mymesh, mydata, dum, stat, msg )
   error_TraceNreturn(stat>IZERO,Here,stat)
!
!- check if consistent with the read data
!
   if ( npros /= nprop ) then  
!
!-    the number of material properties required by the model (nprop) differs from that given
!     by the user (via the keyword matprop):
!     
      errmsg = 'You gave '//i2a(npros)//' mat. properties while the model "'// &
                trim(name_model_pb)//'" needs '//i2a(nprop)//'.'
      do i = 1, size(msg)
         if ( len_trim(msg(i)) /= 0 ) errmsg = trim(errmsg) // NLT // trim(msg(i))
      end do
      stat = err_t( stat = UERROR, where = Here, msg = errmsg )
      return
   end if

   if ( ndime /= ndimm ) then
!
!-    the space dimension for this model (ndime) differs from that deduced from the mesh file
!  
      errmsg = 'The expected spatial dimension for the model "' // trim(name_model_pb) // &
               '" is '//i2a(ndime)//NLT//'while the spatial dimension of the mesh is ' // &
               i2a(ndimm)
      stat = err_t( stat = UERROR, where = Here, msg = errmsg )
      return
   end if
   
   if ( ndofn < ndofD ) then
!
!-    Dirichlet user-data invoke too many d.o.f
!
      errmsg = 'Only '//i2a(ndofn)//' degrees of freedom is needed for the model "' // &
               trim(name_model_pb)//'"' // NLT // 'while your Dirichlet data invokes '// &
               'up to '//i2a(ndofD)//' d.o.f.'
      stat = err_t( stat = UERROR, where = Here, msg = errmsg )
      return
   end if

   if ( ndofn > ndofD )then
!
!-    Resize arrays codediri and vdirichl (because user-data concern only a subset
!     of the set of all dof)
!
      call util_alloc ('s',mydata%vdirichl,ndofn,ndiri,stat) 
      error_TraceNreturn(stat>IZERO, Here, stat)
      call util_alloc ('s',mydata%codediri,ndofn,ndiri,stat)
      error_TraceNreturn(stat>IZERO, Here, stat)
   end if

   if ( ncmpN < ncmpM ) then
!
!-    Neumann user-data invoke too many components
!
      errmsg = 'Only '//i2a(ncmpN)//' components for the traction vector is needed '// &
               'for the model "' // trim(name_model_pb)//'"' // NLT // 'while your '// &
               'Neumann data invokes up to '// i2a(ncmpM) // ' components'
      stat = err_t( stat = UERROR, where = Here, msg = errmsg )
      return
   end if
   
   if ( ncmpN > ncmpM ) then
!
!-    Resize arrays codeneum and vneumann (because user-data concern only a subset
!     of the set of all components of the traction vector)
!
      call util_alloc ('s',mydata%vneumann,ncmpN,nneum,stat) 
      error_TraceNreturn(stat>IZERO, Here, stat)
      call util_alloc ('s',mydata%codeneum,ncmpN,nneum,stat)
      error_TraceNreturn(stat>IZERO, Here, stat)
   end if
!
!- If a Dirichlet's bc is given in a local frame, check if it makes sens
!   
   do i = 1, ndiri
      obj  = mydata % odirichl(i); frame = maxval(mydata % codediri(:,i))
      iref = mydata % ldirichl(i)      
      if ( obj(1:1) == 'p' .and. frame == 2 ) then
         errmsg = 'A local frame cannot be defined for a point:' // NLT // &
                  'The Dirichlet bc at the point #' // i2a(iref) // NLT // &
                  'must be given in the global frame.'
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )
         return
      end if
      if ( obj(1:1) == 'l' .and. frame == 2 .and. ndime == 3 ) then
         errmsg = 'A local frame cannot be defined for a 3D line:' // NLT // &
                  'The Dirichlet bc on the line #' // i2a(iref)    // NLT // &
                  'must be given in the global frame.'
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )
         return
      end if
   end do
   
!
!- If a Neumann's bc is given in a local frame, check if it makes sens
!   
   do i = 1, nneum
      obj  = mydata % oneumann(i); frame = maxval(mydata % codeneum(:,i))
      iref = mydata % lneumann(i)      
      if ( obj(1:1) == 'p' .and. frame == 2 ) then
         errmsg = 'A concentrated load cannot be given in a local frame:' // NLT // &
                  'The Neumann bc at the point #' // i2a(iref)            // NLT // &
                  'must be given in the global frame.'
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )
         return
      end if
      if ( obj(1:1) == 'l' .and. frame == 2 .and. ndime == 3 ) then
         errmsg = 'A 3D line load cannot be given in a local frame:' // NLT // &
                  'The Neumann bc on the line #' // i2a(iref)        // NLT // &
                  'must be given in the global frame.'
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )
         return
      end if
   end do
!
!- Check the coherency between the chosen element and those present in the mesh:
!
   if ( len_trim(name_element) /= 0 ) then
      call element_driver ( trim(name_element), ndime, nnode, stat )
      error_TraceNreturn(stat>IZERO, Here, stat)
   end if
   
   if ( nmats /= nmatm ) then
!
!-    The number of sub-domains found in the mesh differs from that deduced from user's data
!     (via keyword matprop)
!
      if ( nmatm > nmats ) errmsg = 'your data uses only '  //i2a(nmats)//' sub-domain(s).'
      if ( nmatm < nmats ) errmsg = 'your data uses up to ' //i2a(nmats)//' sub-domain(s).'

      errmsg = 'The number of sub-domains found in the mesh is '//i2a(nmatm)//' while' // &
               NLT // trim(errmsg)
         
      stat = err_t( stat = UERROR, where = Here, msg = errmsg )  
      return
   end if

!
!- # of sub-domains given via matprop keyword vs # of sub-domains read in the mesh
!  
   do i = 1, nmats
      iref = mydata % lmatprop(i); iok = 0
      do j = 1, nelem
         jref = mymesh % numdom(j)
         if ( iref == jref ) then
            iok = 1
            mymesh % numdom(j) = i
            exit
         end if
      end do
      
      if ( iok == 0 ) then
         errmsg = 'Inconsitency between matprop data and mesh data:' // NLT // &
                  'the sub-domain #'//i2a(iref)//' does not exist in the mesh.'
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )  
         return
      end if
      
   end do

!
!- # of faces, lines and points via Dirichlet vs # of faces, lines and points in the mesh
!
   do i = 1, ndiri
      iref = mydata % ldirichl(i)    
      obj  = mydata % odirichl(i) 
              
      iok = 0
       
      select case ( obj(1:1) )
          
         case ( 'f' )
            do j = 1, nface       
               jref = mymesh % lface(1,j)
               if ( iref == jref ) then
                  iok = 1
                  exit
               end if
            end do
          
         case ( 'l' )   
            do j = 1, nedge
               jref = mymesh % ledge(1,j)
               if ( iref == jref ) then
                  iok = 1
                  exit
               end if
            end do
             
         case ( 'p' )             
            do j = 1, npoif
               jref = mymesh % lpoin(1,j)
               if ( iref == jref ) then
                  iok = 1
                  exit
               end if
            end do     
             
         case default
            errmsg = 'Indetermined object "'//trim(obj)//'" found for the Dirichlet #'//i2a(i)
            stat = err_t( stat = UERROR, where = Here, msg = errmsg )  
            return
             
      end select                
                 
      if ( iok == 0 ) then
         if ( obj(1:1)=='f' ) errmsg = 'the face of reference #' // i2a(iref)
         if ( obj(1:1)=='l' ) errmsg = 'the line of reference #' // i2a(iref)
         if ( obj(1:1)=='p' ) errmsg = 'the point of reference #'// i2a(iref)
         errmsg = 'Inconsitency between Dirichlet data and mesh data:' // NLT // &
                  trim(errmsg) //' does not exist in the mesh.'
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )  
         return
      end if
   end do         

!
!- # of faces, lines and points via Neumann vs # of faces, lines and points in the mesh
!    
   do i = 1, nneum
      iref = mydata % lneumann(i)    
      obj  = mydata % oneumann(i) 
              
      iok = 0
       
      select case ( obj(1:1) )
          
         case ( 'f' )
            do j = 1, nface       
               jref = mymesh % lface(1,j)
               if ( iref == jref ) then
                  iok = 1
                  exit
               end if
            end do
          
         case ( 'l' )   
            do j = 1, nedge
               jref = mymesh % ledge(1,j)
               if ( iref == jref ) then
                  iok = 1
                  exit
               end if
            end do
             
         case ( 'p' )             
            do j = 1, npoif
               jref = mymesh % lpoin(1,j)
               if ( iref == jref ) then
                  iok = 1
                  exit
               end if
            end do     
             
         case default
            errmsg = 'Indetermined object "'//trim(obj)//'" found for the Neumann #'//i2a(i)
            stat = err_t( stat = UERROR, where = Here, msg = errmsg )  
            return
             
      end select                
                 
      if ( iok == 0 ) then
         if ( obj(1:1)=='f' ) errmsg = 'the face of reference #' // i2a(iref)
         if ( obj(1:1)=='l' ) errmsg = 'the line of reference #' // i2a(iref)
         if ( obj(1:1)=='p' ) errmsg = 'the point of reference #'// i2a(iref)
         errmsg = 'Inconsitency between Neumann data and mesh data:' // NLT // &
                  trim(errmsg) //' does not exist in the mesh.'
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )  
         return      
      end if
   end do   
   
!
!- check the storage format vs the solver, if needed make the necessary change and print 
!  a warning message
!
   if ( index(name_linsolver,'sparskit') /= 0 .and. trim(name_storage) /= 'csr' ) then
      if ( len_trim(name_storage) /= 0 )  stat = err_t( stat = WARNING, where = Here, &
         msg = 'storage format changed to csr (required by sparskit)' )  
      name_storage = 'csr' 
   end if
   
   if ( index(name_linsolver,'geodynldu') /= 0 .and. trim(name_storage) /= 'skyline' ) then
      if ( len_trim(name_storage) /= 0 ) stat = err_t( stat = WARNING, where = Here, &
         msg = 'storage format changed to skyline (required by geodynldu)' )
      name_storage = 'skyline'
   end if
   
   if ( index(name_linsolver,'superlu') /= 0 .and. trim(name_storage) /= 'csr' ) then      
      if ( len_trim(name_storage) /= 0 ) stat = err_t( stat = WARNING, where = Here, &
         msg = 'storage format changed to csr (required by superlu)')
      name_storage = 'csr' 
   end if
   
    if ( index(name_linsolver,'pardiso') /= 0 .and. trim(name_storage) == 'skyline' ) then    
      if (len_trim(name_storage) /= 0 ) stat = err_t( stat = WARNING, where = Here, &      
         msg = 'storage format changed to csr (as skyline is not used by pardiso)')
      name_storage = 'csr'       
   end if     
   
   if ( index(name_linsolver,'mkl(fgmres)') /= 0 .and. trim(name_storage) /= 'csr' ) then      
      if ( len_trim(name_storage) /= 0 ) stat = err_t( stat = WARNING, where = Here, &
         msg = 'storage format changed to csr (required by mkl(fgmres))')
      name_storage = 'csr' 
   end if   

#ifdef noMKL
   if  (index(name_linsolver,'mkl') /= 0 ) then
      stat = err_t( stat = UERROR, where = Here, msg = &
                    "Unable to use MKL (you compiled this code with mkl='no')")
      return
   end if
#endif   

!
!- check the output file types and formats:
!
   do i = 1, size(name_outputtype)
!
!-    if an output type is not valid: print an error message and stop
!   
      if ( len_trim(name_outputtype(i)) == 0 ) cycle
      
      if ( index(name_outputtype(i),'vtk'   ) == 0 .and.    &
           index(name_outputtype(i),'vtkl'  ) == 0 .and.    &
           index(name_outputtype(i),'vnod'  ) == 0 .and.    &
           index(name_outputtype(i),'velm'  ) == 0 .and.    &
           index(name_outputtype(i),'velml' ) == 0 .and.    &
           index(name_outputtype(i),'sparse') == 0 .and.    &
           index(name_outputtype(i),'p'     ) == 0 .and.    &
           index(name_outputtype(i),'none'  ) == 0          )    then
         stat = err_t( stat = UERROR, where = Here, msg ='Unknown file type: "'// &      
                       trim(name_outputtype(i))//'"' )
         return
      end if
!
!-    if an output format is not valid: print a warning message and change it to "bin"
!      
      if ( trim(name_outputfmt(i)) /= 'ascii' .and. trim(name_outputfmt(i)) /= 'bin' )  then
         stat = err_t( stat = WARNING, where = Here, msg = 'Unknown file format: "' // &
                       trim(name_outputfmt(i))//'". Changed to "bin"')
         name_outputfmt(i) = 'bin'
      end if       
      
      if ( index(name_outputtype(i),'vtk') == 1 .and. trim(name_outputfmt(i)) == 'bin' ) then
         stat = err_t( stat = WARNING, where = Here, msg = 'binary vtk not yet ' // &
                       'implemented. Changed to ascii' )
         name_outputfmt(i) = 'ascii'
      end if   
         
   end do
!
!- Check the output nodes:
!
   do i = 1, nNod_output
      j = nod_output(i)
      if ( j < 0 .or. j > npoin ) then
         nod_output(i) = 0
         errmsg = 'Invalid node #' // i2a(j) // ' given in output_format.' // NLT // &
                  'It will be ignored.'
         stat = err_t( stat = WARNING, where = Here, msg = errmsg )
      end if
   end do
!
!- Check the output elements:
!
   do i = 1, nElm_output
      j = elm_output(i)
      if ( j < 0 .or. j > nelem ) then
         elm_output(i) = 0
         errmsg = 'Invalid element #' // i2a(j) // ' given in output_format.' // NLT // &
                  'It will be ignored.'
         stat = err_t( stat = WARNING, where = Here, msg = errmsg )
      end if
   end do   
!
!- Check the output points and find their associated nodes:
!   
   do i = 1, nPnt_output
      iref = pnt_output(i,1) ; pnt_output(i,2) = 0
      do j = 1, npoif
         jref = mymesh%lpoin(1,j)
         if ( iref == jref ) then
            pnt_output(i,2) = mymesh%lpoin(2,j)
            exit
         end if
      end do   
      if ( pnt_output(i,2) == 0 ) then
         errmsg = 'Reference point #' // i2a(iref) // ' given in output_format ' // &
                  'is not associated' // NLT // 'with any mesh node. It will be ignored.'
         stat = err_t( stat = WARNING, where = Here, msg = errmsg )
      end if
   end do
!
!- if a user-defined variable is called "optmod" save its (real) values in array optmod
!
   call input_findvar ( "optmod", valC, nv )
   
   do i = 1, nv
      read(valC(i),*,iostat=err) optmod(i)
      if ( err /= 0 ) then
         errmsg  = 'Wrong real number or uninitialized variable' // NLT //  &
                   'for the entry #'//i2a(i)//' of the variable "optmod":'//NLT//trim(valC(i))
         stat = err_t( stat = UERROR, where = Here, msg = errmsg )
         return
      end if   
   end do             
 
   END SUBROUTINE input_check


!=============================================================================================
   SUBROUTINE input_default ( phase, stat )
!=============================================================================================
   integer(Ikind), intent(in    ) :: phase
   type   (err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Initialization of the set of parameters to their default values
!
!  If phase == 0: initialize to zero and to empty strings all parameters
!                 (phase 0 has to be called at the beginning) 
!
!  If phase == 1: some non-initialized parameters are set to their default value 
!                 (phase 1 has to be called at the end of the reading)
!-----------------------------------------------------------------------------------R.H. 12/16  

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'input_default'
   character(len=:), allocatable :: errmsg
   integer  (Ikind)              :: i
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 

   if ( phase == 1 ) then
   
      if ( len_trim(name_element) == 0 ) then
!
!-       Set the element name to the one corresponding to the pair (ndime,nnode) where nnode
!        is the number of nnode per element found in the mesh:
!               
         errmsg = 'No element found corresponding to ndime = ' // i2a(ndime) // &
                  ' and nnode = '//i2a(nnode) 
      
         select case ( ndime )
            case ( 1 ) 
               select case ( nnode  )
                  case ( 2 )
                     name_element = 'segm02'
                  case ( 3 )   
                     name_element = 'segm03'
                     
                  case default
                     stat = err_t( stat = UERROR, where = Here, msg = errmsg ) ; return
               end select 
               
            case ( 2 )
               select case ( nnode )
                  case ( 3 )
                     name_element = 'tria03'
                  case ( 4 )   
                     name_element = 'quad04'
                  case ( 6 )
                     name_element = 'tria06'
                  case ( 7 )
                     name_element = 'tria07'
                  case ( 8 )
                     name_element = 'quad08'
                  case ( 9 )
                     name_element = 'quad09'
                     
                  case default
                     stat = err_t( stat = UERROR, where = Here, msg = errmsg ) ; return
               end select 
            
            case ( 3 )    
               select case ( nnode )
                  case ( 4 )
                     name_element = 'tetr04'
                  case ( 8 )   
                     name_element = 'hexa08'
                  case ( 10 )
                     name_element = 'tetr10'
                  case default
                     stat = err_t( stat = UERROR, where = Here, msg = errmsg ) ; return
               end select 
               
            case default
               stat = err_t( stat = UERROR, where = Here, msg = errmsg ) ; return
         
         end select  

         stat = err_t( stat = WARNING, where = Here, msg = &
             "As you didn't select an element type it has been set to: "//trim(name_element))
      else
         name_element = adjustl(name_element)
         if ( trim(name_element)/='segm02' .and. trim(name_element)/= 'segm03' .and.  &
              trim(name_element)/='tria03' .and. trim(name_element)/= 'tria04' .and.  &
              trim(name_element)/='tria06' .and. trim(name_element)/= 'tria07' .and.  &
              trim(name_element)/='quad04' .and. trim(name_element)/= 'quad08' .and.  &
              trim(name_element)/='quad09' .and. trim(name_element)/= 'tetr04' .and.  &
              trim(name_element)/='tetr10' .and. trim(name_element)/= 'hexa08'        ) then
            stat = err_t( stat = UERROR, where = Here, msg = 'Unknown element: "'// &
                                                             trim(name_element)//'"')
            return
         end if
      end if

      if ( nipts == 0 ) then
!
!-       Set the number of I.P.:
!      
         i = index(name_class_pb,'steady')
         select case ( name_element )
            case ( 'segm02' )
               nipts = 1; if ( i == 0 ) nipts = 2
            case ( 'segm03' ) 
               nipts = 2; if ( i == 0 ) nipts = 3
            case ( 'tria03' )
               nipts = 1; if ( i == 0 ) nipts = 3
               if ( i == 0 .and. index(name_model_pb,'axisym') /= 0 ) nipts = 4
            case ( 'tria06', 'tria07' )
               nipts = 3; if ( i == 0 ) nipts = 7
               if ( index(name_model_pb,'axisym') /= 0 .and. i /= 0 ) nipts = 4
            case ( 'quad04' )     
               nipts = 4; if ( i == 0 ) nipts = 9
            case( 'quad08', 'quad09' )
               nipts = 9; if ( i == 0 ) nipts = 9
            case ( 'tetr04' )   
               nipts = 1; if ( i == 0 ) nipts = 4
            case ( 'tetr10' )
               nipts = 4; if ( i == 0 ) nipts = 5
            case ( 'hexa08' )   
               nipts = 8; if ( i == 0 ) nipts = 27
         end select   

         stat = err_t( stat = WARNING, where = Here, msg = "As you didn't set the " //   &
                       "number of integration points it has been set to: " // i2a(nipts) )
      end if
!
!-    Set the element geometry:
!      
      if      ( index(name_element,'segm') /= 0 ) then
         name_geometry = 'segment'
      else if ( index(name_element,'tria') /= 0 ) then
         name_geometry = 'triangle'
      else if ( index(name_element,'quad') /= 0 ) then
         name_geometry = 'carre'
      else if ( index(name_element,'tetr') /= 0 ) then 
         name_geometry = 'tetraedre'
      else if ( index(name_element,'hexa') /= 0 ) then
         name_geometry = 'cube'
      else
         stat = err_t( stat = UERROR, where = Here, msg = 'Unable to determine '// &
                      'the geometry of the element: "' // trim(name_element) // '"')
         return
      end if   

!
!-    Set the number of dof per element:
!
      nevab = ndofn * nnode
!
!-    Set the total number of I.P.:  
!
      ntotg = nipts * nelem          
!
!-    if the linear solver is not set, set it to sparskit(fom) for example:
!      
      if ( len_trim(name_linsolver) == 0 ) then
         name_linsolver = 'superlu'
         stat = err_t( stat = WARNING, where = Here, msg = "As the linear solver is not "// &
                       "set, the default ("//trim(name_linsolver)//") will be used")
      end if   
!
!-    if the storage format is not choosen, set it to 'csr' 
!     (or 'skyline' if geodynldu solver is selected):
!           
      if ( len_trim(name_storage) == 0 ) then
         name_storage = 'csr'
         if ( index(name_linsolver,'geodynldu') /= 0 ) name_storage = 'skyline'
      end if   
!
!-    if the no reordering scheme is choosen, set it to 'GPSK' 
!           
      if ( len_trim(name_reorderMeth) == 0 ) then
         name_reorderMeth = 'GPSK'
         stat = err_t( stat = WARNING, where = Here, msg = "As you didn't set the " //      &
               "reordering scheme, the default ("//trim(name_reorderMeth)//") will be used" )
      end if   
      
!
!-    if the options for iterative solvers are not specified by the user, set them
!     to their default values:
!
      if ( optlinsol(1) < 0 ) optlinsol(1) = 150           ! max. nbr of iterations: max(150,n)
      if ( optlinsol(2) < 0 ) optlinsol(2) = 1.0e-05_Rkind ! relative tolerance
      if ( optlinsol(3) < 0 ) optlinsol(3) = 1.0e-10_Rkind ! absolute tolerance
      if ( optlinsol(4) < 0 ) optlinsol(4) = 50            ! subspace dim. for GMRES-like meth.
      if ( optlinsol(5) < 0 ) optlinsol(5) = 15            ! fill-in parameter 
      if ( optlinsol(6) < 0 ) optlinsol(6) = 1.0e-08_Rkind ! tolerance for ILUT 

!
!-    if the non-linear solver is not specified, set it to 'newton'
!      
      if ( len_trim(name_nlinsolver) == 0 ) name_nlinsolver = 'newton'
!
!-    if the options for iterative non-linear solvers are not specified by the user, 
!     set them to their default values:
!         
      if ( optnlinsol(1) < 0 ) optnlinsol(1) = 100           ! max. nbr of iterations   
      if ( optnlinsol(2) < 0 ) optnlinsol(2) = 1.0e-06_Rkind ! relative tolerance
      if ( optnlinsol(3) < 0 ) optnlinsol(3) = 1.0e-10_Rkind ! absolute tolerance    
!
!-    if the output file type are not specified set only vtk (and in ascii)
!      
      if ( all(len_trim(name_outputtype) == 0) ) then
         name_outputtype(1) = 'vtk' 
         name_outputfmt (1) = 'ascii' 
      end if
      
      if ( len_trim(name_outputDir) > 0 ) then
         call util_executeCommandLine ( 'mkdir -p '//trim(name_outputDir), stat )
         error_TraceNreturn(stat>IZERO, HERE, stat)
         name_outputDir = trim(adjustl(name_outputDir)) // '/'
      else
         name_outputDir ='./'
      end if
!
!-    if the verbosity levels are not set, set them to 1
!                  
      if ( all(verbosity < 0) ) verbosity(:) = 1
!
!-    set any non-initialized verbosity level to 0
!      
      do i = 1, size(verbosity)
         if ( verbosity(i) < 0 ) verbosity(i) = 0
      end do   
                                  
   end if        
   
   if ( phase == 0 ) then

      call signalHandler_SignalCatch &
       ( STDOUT, '--> Camef2017 Info:', 'thank you for using Camef2017. See you soon. Bye!' )   
!                 
!-    parameters from the mesh
!
      name_mesh_file  = '' ! name of the mesh file 

      ndimM = 0 ! space dimension (1, 2 or 3) that will be deduced from the Mesh
      nnode = 0 ! nbr of nodes per element
      nsomt = 0 ! nbr of vertices per element (= nnode for (bi-,tri-) linear elements)
   
      nelem = 0 ! nbr of elements in the mesh
      npoin = 0 ! nbr of nodes in the mesh
      nmatm = 0 ! nbr of sub-domains in the mesh
    
      nedge = 0 ! nbr of edges on the mesh boundary
      nface = 0 ! nbr of facets on the mesh boundary (=nedge in 2d) 
      npoif = 0 ! nbr of geometrical points
    
      ndedg = 0 ! nbr of nodes by edge
      ndfac = 0 ! nbr of nodes by facet (=ndegd in 2d)
    
      maxne = 0 ! max. nbr of elements connected to a node
      maxnn = 0 ! max. nbr of nodes connected to a node
   
!
!-    parameters from the physics
!    
      name_model_pb = ''! model problem name
      name_class_pb = ''! class problem name

      ndime   = 0 ! space dimension (1, 2 or 3) deduced from the physical problem
      
      ndofn   = 0 ! nbr of degrees of freedom deduced from the physical problem
      ndofD   = 0 ! nbr of degrees of freedom deduced from the Dirichlet bc
      
      ncmpN   = 0 ! nbr of components in the traction vector deduced from the the physical problem      
      ncmpM   = 0 ! nbr of components in the traction vector deduced from the neuMann bc
      
      nmats   = 0 ! nbr of materials
      
      nprop   = 0 ! nbr of properties for each mat. deduced from the physical problem
      npros   = 0 ! nbr of properties for each mat. deduced from the data
      
      nstri   = 0 ! nbr of independent components of variable derivatives ("stresses")

      ndiri   = 0 ! nbr of faces where Dirichlet bc are imposed
      nneum   = 0 ! nbr of faces whre Neumann bc are imposed
      
      ntime   = 0 ! nbr of time-steps
      Ttime   = 0 ! total time
   
!
!-    parameters related to the finite element
! 
      name_element  = '' ! name of the selected finite element 
      name_geometry = '' ! name of its geometry (segment, triangle, carre, cube, tetraedre)

      nipts = 0 ! nbr of integration points per element
      ntotg = 0 ! total nbr of integration points (= nipts*nelem)
      nevab = 0 ! nbr of nodal d.o.f per element (= nnode*ndofn)       
                                         
!
!-    parameters related to the linear system  
!                       
      nequa = 0 ! total nbr of equations (=nbr of unknowns, =size of the matrix)
      nzero = 0 ! size of the array needed to store the matrix
    
!
!-    parameters related to the linear solver
!
      name_linsolver   = ''      ! name of the solver
      name_storage     = ''      ! name of the storage format
      name_reorderMeth = ''      ! name of the reordering scheme    
      optlinsol(:)   =-1.0_Rkind ! optional parameters for the solver (-1 = not initialized)

!
!-    parameters related to the non-linear solver
!
      name_nlinsolver = ''       ! name of the solver  
      optnlinsol(:)   =-1.0_Rkind ! optional parameters for the solver (-1 = not initialized)

!
!-    input/ouput file names
!
      name_input_file = ''          ! input file name (without the .par extension)
      name_outputtype(:) = ''       ! extensions of the output files
      name_outputfmt (:) = 'bin'    ! formats of the output files
      name_outputDir     = ''       ! output directory
      freq_output    (:) = 1        ! output frequencies (a write every freq_output steps) 

!
!-    verbosity levels
!
      verbosity(:) =-1  ! "-1" means that verbosities are not initialized  

!
!-    free options
!
      optmod(:) = 0.0_Rkind      
   end if
   
   END SUBROUTINE input_default


!=============================================================================================
   SUBROUTINE input_readmesh ( mymesh, stat )
!=============================================================================================
   type(mesh_t), intent(in out) :: mymesh
   type(err_t ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Reads a mesh given in GMSH format
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------    
   character(len=* ), parameter :: Here = 'input_readmesh'  	
   integer  (Ikind )            :: nrefe, nreff, nrefv, noeuf  
   integer  (Ikind )            :: um, i, j, in, io, id, ip, ipp, ie, ie2d, ie3d,          &
                                   nnoeud, nnode2d, nnode3d, imil, nobje, itypo,           &
                                   irego, iregp, ned, err, nfc, nel, nep,                  &
                                   maxval, num, nve, nvp, ideja, kp, jp, kvp  
   integer  (Ikind )            :: lnod(30)
   character(len=LGSTR)         :: msg, buf
!---------------------------------------------------------------------------------------------   

   if ( stat > IZERO ) return 

   if ( verbosity(1) >= 1 ) &
      print '(/,a)', '.... reading mesh on the file: '//trim(name_mesh_file)

   call util_get_unit (um)                  
   open (um, file = name_mesh_file, status = 'old', action = 'read', iostat=err, iomsg = msg)
   
   if ( err /= 0 ) then
      call input_error ( stat, 'Unable to open the file "'//trim(name_mesh_file)//'"'//NLT// &
                         'IO-MSG: '// trim(msg), UERROR, Here, numl, ifile, rec )
      return
   end if
    
   nedge = 0; nface = 0; ndfac = 0; ndedg = 0; nrefv = 0; nreff = 0; nrefe = 0; npoif = 0
   maxval = 0; nnode2d = 0; nnode3d = 0
                      
   num = 0 ; err = 0
   do
      if ( err > 0 ) exit
      
      num = num + 1
      read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit 
                                                         
      if ( buf(1:1) == '$' ) then
         if ( buf(2:4) == 'NOD' ) then
            num = num + 1
            !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit 
            read(um,*,iostat=err)npoin   ; if ( err /= 0 ) exit
            do ip = 1, npoin
              num = num + 1
              !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit
              read(um,*,iostat=err)ipp     ; if ( err /= 0 ) exit             
            end do 
            
         else if ( buf(2:4) == 'ELM' ) then
            num = num + 1
            !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit 
            read(um,*,iostat=err)nobje   ; if ( err /= 0 ) exit
            ie2d = 0; ie3d = 0
            do io = 1, nobje
               num = num + 1
               !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit
               read(um,*,iostat=err)i,itypo,iregp,irego,nnoeud
               if ( err /= 0 ) exit                               
               if ( itypo == 1 .or. itypo == 8 ) then
                  nedge = nedge + 1
                  ndedg = nnoeud
                  nrefe = max(nrefe,irego)
               end if
               if ( itypo == 2  .or. itypo == 3  .or. itypo == 9 .or. &
                    itypo == 10 .or. itypo == 16                      ) then
                  nface = nface + 1
                  ndfac = nnoeud
                  nreff = max(nreff,irego)
               end if
               if ( itypo == 4  .or. itypo == 5  .or. itypo == 11 .or. &
                    itypo == 12 .or. itypo == 17                       ) then
                  nrefv = max(nrefv,irego)
               end if                       
               if ( itypo == 2  .or. itypo == 3 .or. itypo == 9 .or. &
                    itypo == 10 .or. itypo == 16                     ) then
                  ie2d = ie2d + 1
                  nnode2d = nnoeud
               end if         
               if ( itypo == 4  .or. itypo ==  5 .or. itypo ==  6 .or. &
                    itypo == 7  .or. itypo == 11 .or. itypo == 12 .or. &
                    itypo == 13 .or. itypo == 14 .or. itypo == 17      ) then
                  ie3d = ie3d + 1
                  nnode3d = nnoeud
               end if              
               if ( itypo == 15 ) then
                  npoif = npoif + 1
               end if   
            end do
            
            if ( ie3d == 0 ) then
               ndimm = 2
               nnode = nnode2d
               nelem = ie2d
            else
               ndimm = 3
               nnode = nnode3d
               nelem = ie3d
            end if                
         else if ( buf(2:4) /= 'END' ) then
            err = 1 ; exit
         end if   
     else
         err = 1 ; exit 
     end if
        
   end do    
   
   if ( err > 0 ) then
      call input_error ( stat, 'Failed to read the mesh',  &
                         UERROR, Here, num, trim(name_mesh_file), trim(buf) )
      return
   end if
!
!- number of sub-domains (nmatm) found in the mesh:
!    
   nmatm = nrefv
   if ( ndimm == 2 ) nmatm = nreff
!
!- number of nodes per face (noeuf) et number of vertices in the simplex (nsomt):
!    
   noeuf = 0; nsomt = 0
   if ( ndimm == 2 ) then
      ndfac = ndedg
      nface = nedge
    
      imil = 0
      if ( nnode==6 .or. nnode==7 .or. nnode==8 .or. nnode==9 ) imil = 1
!
!-    number of nodes per edge
!
      if ( imil == 0 ) then
         noeuf = 2   ! for linear or bi-linear element
      else
         noeuf = 3   ! for quadratic element
      end if
      if ( nnode==3 .or. nnode==6 .or. nnode==7 ) nsomt = 3
      if ( nnode==4 .or. nnode==8 .or. nnode==9 ) nsomt = 4
   end if
    
   if ( ndimm == 3 ) then
      if ( nnode==4 .or. nnode==10 ) then
!
!-       4- or 10-noded tetra
!
         nsomt = 4
         if ( nnode ==  4 ) noeuf = 3 ! for linear element
         if ( nnode == 10 ) noeuf = 6 ! for quadratic element
      end if   
      if ( nnode.eq.8 .or. nnode.eq.20 .or. nnode.eq.27 ) then
!
!-       8-, 20- our 27-nodes hexa
!
         nsomt = 8
         if ( nnode ==  8 ) noeuf = 4 ! for tri-linear element
         if ( nnode == 20 ) noeuf = 8 ! for quadratic element
         if ( nnode == 27 ) noeuf = 9 ! for quadratic element
      end if   
   end if  

   if ( noeuf == 0 .or. nsomt == 0 ) then
      stat = err_t( stat = UERROR, where = Here, &
                    msg = 'Unexpected element found in the mesh:' // NLT // &
                          '- Number of nodes / element = '//i2a(nnode) // NLT // &
                          '- Number of dimension = '//i2a(ndimm) )
      return
   end if
 
!
!- effective read of the mesh
!    
   rewind(um)

  !--------------------------------------------------------------------------!
  !                         A L L O C A T I O N S                            !
  !--------------------------------------------------------------------------!   
  ! allocate lnods, coord, ledge, lface, numdom, lvale:                       !
  !                                                                          ! 
   allocate(mymesh % lnods (nnode  ,nelem), mymesh % coord(ndimm  ,npoin),  & !
            mymesh % ledge (ndedg+1,nedge), mymesh % lface(ndfac+1,nface),  & !
            mymesh % numdom(        nelem), mymesh % lvale(        npoin),  & !
            mymesh % lpoin (2      ,npoif), mymesh % coor0(ndimm  ,npoin),  &
            stat=err)                         !
  !--------------------------------------------------------------------------!

   if ( err /= 0 ) then
      stat = err_t(stat=IERROR, where=Here,msg='Allocation failure (for mesh arrays)')
      return
   end if
    
   mymesh % lvale(:) = 0
    
   num = 0 ; err = 0
    
   do
      if ( err > 0 ) exit
      
      num = num + 1
      read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit
               
      if ( buf(1:1) == '$' ) then
         if ( buf(2:4) == 'NOD' ) then
            num = num + 1
            !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit
            read(um,*,iostat=err)npoin   ; if ( err /= 0 ) exit
            do ip = 1, npoin
               num = num + 1
               !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit
               read(um,*,iostat=err)ipp,(mymesh%coord(id,ip),id=1,ndimm)
               if ( err /= 0 ) exit
            end do
         else if ( buf(2:4) .eq. 'ELM' ) then               
!
!-          elementary objects:
!            
            num = num + 1
            !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit
            read(um,*,iostat=err)nobje   ; if ( err /= 0 ) exit
            if ( err /= 0 ) exit
            
            nep = 0; nel = 0; ned = 0; nfc = 0; maxval = 0
            do io = 1, nobje
               num = num + 1
               !read(um,'(a)',iostat=err) buf ; if ( err /= 0 ) exit
               read(um,*,iostat=err)i,itypo,iregp,irego,nnoeud,(lnod(j),j=1,nnoeud)
               if ( err /= 0 ) exit
               
               if ( itypo == 15 ) then
!
!-                the element object is a point
!
                  nep = nep + 1
                  mymesh%lpoin(1,nep) = irego; mymesh%lpoin(2,nep) = lnod(1)
               end if
                  
               if ( itypo == 1 .or. itypo == 8 ) then
!
!-                the element object is a line:
!
                  ned = ned + 1
                  mymesh%ledge(1,ned) = irego; mymesh%ledge(2:nnoeud+1,ned) = lnod(1:nnoeud)
                   
                  if ( ndimm == 2 ) then
                     nfc = nfc + 1
                     if ( nnoeud /= ndfac ) then
                        stat = err_t( stat=UERROR, where=Here, msg = 'ndfac /= nnoeud' // &
                                   NLT//'ndfac = '//i2a(ndfac)//' nnoeud = '//i2a(nnoeud) )
                     end if  
                     mymesh%lface(1,nfc) = irego; mymesh%lface(2:nnoeud+1,nfc) = lnod(1:nnoeud)
                  end if   
               end if
                          
               if ( itypo == 2  .or. itypo == 3 .or. itypo == 9 .or. &
                    itypo == 10 .or. itypo == 16                     ) then
!
!-                the element object is a triangle or a quadrangle:
!              
                  if ( ndimm == 2 ) then
                     nel = nel + 1
                     do in = 1, nnode
                        ip = lnod(in)
                        mymesh%lnods(in,nel) = ip
                        mymesh%lvale(ip) = mymesh%lvale(ip) + 1
                        maxval = max(maxval,mymesh%lvale(ip))
                     end do
                     mymesh%numdom(nel) = irego
                  end if
            
                  if ( ndimm == 3 ) then
                     nfc = nfc + 1
                     if ( nnoeud /= ndfac ) then
                        stat = err_t( stat=UERROR, where=Here, msg = 'ndfac /= nnoeud' // &
                                   NLT//'ndfac = '//i2a(ndfac)//' nnoeud = '//i2a(nnoeud) )
                        return
                     end if  
                     mymesh%lface(1,nfc) = irego; mymesh%lface(2:nnoeud+1,nfc) = lnod(1:nnoeud)
                  end if    
               end if
                
               if ( ndimm == 3 ) then
                  if (itypo ==  4 .or. itypo ==  5 .or. itypo ==  6 .or. itypo == 7 .or.   &
                      itypo == 11 .or. itypo == 12 .or. itypo == 13 .or. itypo == 14 ) then
!                       
!-                   the element object is a tetrahedron or a hexahedron:
!                       
                     nel = nel + 1
                     mymesh%numdom(nel) = irego; mymesh%lnods(1:nnoeud,nel) = lnod(1:nnoeud)
                     do in = 1, nnoeud
                        ip = lnod(in)
                        mymesh%lvale(ip) = mymesh%lvale(ip) + 1
                        maxval = max(maxval,mymesh%lvale(ip))               
                     end do
                    
                     if ( itypo == 11 ) then                         !! GMSH 10-noded tetra:
                        imil = mymesh%lnods(9,nel) ;                 !! the 2 last nodes are
                        mymesh%lnods( 9,nel) = mymesh%lnods(10,nel)  !! permutted to conform  
                        mymesh%lnods(10,nel) = imil                  !! with the ordering of
                      end if                                         !! the element lib. 
                                        
                  end if
               end if 
                
            end do    ! end loop over the objects  
            
         else if ( buf(2:4) /= 'END' ) then
            err = 1
         end if   
      
      else
         err = 1     
      end if
         
   end do
   
   if ( err > 0 ) then
      call input_error ( stat, 'Failed to read the mesh',  &
                         UERROR, Here, num, trim(name_mesh_file), trim(buf) )
      return
   end if
    
   close(um)

   if ( verbosity(1) >= 1 ) print '(a,/)','.... mesh reading completed'
    
!
!- build neighborhood tables:
!
!     lvale(  ip) : number of elements connected to node #ip
!     lelem(:,ip) : list of elements attached to node #ip
!     lvois(:,ip) : list of nodes connected to node #ip
!
   maxne = noeuf*maxval   ! max number of node/elements connexions 
   maxnn = nnode*maxval+1 ! max number of node/nodes connexions
    

  !--------------------------------------------------------------------------!
  !                         A L L O C A T I O N S                            !
  !--------------------------------------------------------------------------!   
  ! allocate lelem, lvois:                                                   !
  !                                                                          ! 
   allocate(mymesh%lelem(maxne,npoin), mymesh%lvois(maxnn,npoin), stat=err)  !
  !--------------------------------------------------------------------------!

   if ( err /= 0 ) then
      stat = err_t(stat=IERROR,where=Here,msg='Allocation failure (for "lelem" and "lvois")')
      return
   end if

   mymesh % lelem(:,:) = 0; mymesh % lvale(:) = 0; mymesh % lvois(:,:) = 0
!
!- build lelem:
!    
   do ie = 1, nelem
!
!-    store ie into the list of elements attached to the nodes # lnods(:,ie)      
!
      do in = 1, nnode
         ip  = mymesh % lnods(in,ie)
         nve = mymesh % lvale(ip) + 1
         mymesh % lvale(    ip) = nve   
         mymesh % lelem(nve,ip) = ie   
      end do
   end do
	
!
!- build lvois:
!	
   do ip = 1, npoin
!
!-    scroll through the elements attached to ip and find the nodes connected to ip
!
      nvp = 1
      mymesh%lvois(nvp,ip) = ip
       
      nve = mymesh%lvale(ip)
      do i = 1, nve 
         ie = mymesh%lelem(i,ip)
         do in = 1, nnode
            jp = mymesh%lnods(in,ie)
            ideja = 0  
            do kvp = 1, nvp
               kp = mymesh%lvois(kvp,ip)
               if ( kp .eq. jp ) then
                  ideja = 1 ! already stored
                  exit
               end if   
            end do
            if ( ideja == 1 ) cycle ! already stored: next one
            nvp = nvp + 1
            if ( nvp .gt. maxnn ) then
               stat = err_t( stat=IERROR, where=Here, msg='nvp > maxnn' )
               return
            end if
            mymesh%lvois(nvp,ip) = jp
         end do
      end do
      mymesh%lvois(maxnn,ip) = nvp ! total number of nodes attached to ip
   end do
             
   END SUBROUTINE input_readmesh


!=============================================================================================
   SUBROUTINE input_summary ( mydata, mymesh, stat )
!=============================================================================================
   type(udata_t), intent(in out) :: mydata
   type(mesh_t ), intent(in out) :: mymesh  
   type(err_t  ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------    
   
!- local variables ---------------------------------------------------------------------------      
   integer  (Ikind   ) :: i, j, n, m, lenN, lenV, lenVi, totlen
   character(MDSTR   ) :: s1, s2, fmt, fmt0, info(30)
   type     (linsys_t) :: dum
!---------------------------------------------------------------------------------------------    

   if ( stat > IZERO ) return 

   write(*,'(/,a)')" :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
   write(*,'(a  )')" ::-- I N P U T   D A T A   S U M M A R Y   R E P O R T --::"
   write(*,'(a,/)')" :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
   
   write(*,'(  3x,a)')"+====================================+" 
   write(*,'(  3x,a)')"|      User's defined variables      |"
   write(*,'(3x,a,/)')"+====================================+" 
   if ( nvar == 0 ) then
      write(*,'(3x,a)')'None'
   else      
      lenN = 0 ; lenV = 0
      do i = 1, nvar
         if ( len_trim(myvar(i)%name) > lenN ) lenN = len_trim(myvar(i)%name)
         n = size(myvar(i)%valC)
         lenVi = 0
         do j = 1, n
            lenvi = lenVi + len_trim(myvar(i)%valC(j))
         end do
         lenVi = lenVi + n - 1   
         lenV = max(lenV, lenVi)
      end do   
      totlen = lenN+lenV+3
      write(fmt,'(a,i0,a)')'(3x,',totlen,'("-"))'                       ; write(*,fmt)
      write(fmt,'(a,i0,a)')'(3x,"Name ",',max(1,lenN-4),'x, "| Value")' ; write(*,fmt)
      write(fmt,'(a,i0,a)')'(3x,',totlen,'("-"))'                       ; write(*,fmt)
   
      write(fmt0,'(a,i0)')'a',lenN
      do i = 1, nvar
         n = size(myvar(i)%valC)
         write(fmt,'(a,i0,a)')'(3x,'//trim(fmt0)//',1x,"| ",',n,'(a,1x))'
         write(*,fmt)trim(myvar(i)%name),(trim( myvar(i)%valC(j) ),j=1,n)            
      end do      
      write(fmt,'(a,i0,a)')'(3x,',totlen,'("-"))'             ; write(*,fmt)
   end if

   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'( 3x,a)' )"|             Mesh data              |"
   write(*,'(3x,a,/)')"+====================================+" 
   write(*,'(3x,a,a)' )"  - Mesh file name              : ",trim(name_mesh_file)
   write(*,'(3x,a,a)' )"  - Element name                : ",trim(name_element)
   write(*,'(3x,a,a)' )"  - Element geometry            : ",trim(name_geometry)
   write(*,'(3x,a,i0)')"  - Space dimension             : ",ndimm
   write(*,'(3x,a,i0)')"  - Number of I.P               : ",nipts
   write(*,'(3x,a,i0)')"  - Number of nodes             : ",npoin
   write(*,'(3x,a,i0)')"  - Number of elements          : ",nelem
   write(*,'(3x,a,i0)')"  - Number of subdomains        : ",nmatm
   write(*,'(3x,a,i0)')"  - Number of nodes per element : ",nnode
   write(*,'(3x,a,i0)')"  - Number of external faces    : ",nface
   write(*,'(3x,a,i0)')"  - Number of external edges    : ",nedge
   write(*,'(3x,a,i0)')"  - Number of nodes per face    : ",ndfac
   write(*,'(3x,a,i0)')"  - Number of nodes per edge    : ",ndedg
    
   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|          Physical problem          |"
   write(*,'(3x,a,/)')"+====================================+" 
   write(*,'(3x,a,a)')"  - Class problem        : ",trim(name_class_pb)
   write(*,'(3x,a,a)')"  - Model problem        : ",trim(name_model_pb)
   if ( trim(name_class_pb) /= 'steady' ) then
      write(*,'(3x,a,i0)')"  - Number of time steps : ",ntime
      write(fmt,'(e13.5)')Ttime
      write(*,'(3x,a,a)')"  - Total time           : ",trim(adjustl(fmt))
   end if
   info = ''; call models_driver ( mymesh, mydata, dum, stat, info )
   error_TraceNreturn(stat>IZERO, 'input_summary', stat)
   if ( len_trim(info(2)) /= 0 ) write(*,'(5x,"- ",a)' )trim(adjustl(info(2)))
   do i = 3, size(info)
      if ( len_trim(info(i)) /= 0 ) write(*,'(7x,a)' )trim(adjustl(info(i)))
   end do   

   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|         Material properties        |"
   write(*,'(3x,a,/)')"+====================================+" 
   write(*,'(3x,a,i0)')"  - Number of subdomains : ",nmats
   write(*,'(3x,a,i0)')"  - Number of properties : ",npros
   write(*,'(3x,a   )')"  - Property values for each material: "
   
   n = 0
   do i = 1, nmats
      n = max( n, len(i2a(mydata%lmatprop(i))) )
   end do
   fmt0 = '(7x,"Mat. #",a'//i2a(n)//'": ",30e13.5)'
   do i = 1, nmats
      write(*,fmt0)i2a(mydata%lmatprop(i)),mydata%vmatprop(:,i)
   end do     
   
   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|    Dirichlet boundary conditions   |"
   write(*,'(3x,a,/)')"+====================================+"   
   write(*,'(3x,a,i0)')"  Number of Dirichlet bc : ",ndiri

   write(*,*)
   n = 0
   do i = 1, ndiri
      n = max( n, len(i2a(mydata%ldirichl(i))) )    
   end do  
   fmt = '(5x,a7,a' // i2a(n) // '": code(s): ",a '// i2a(ndofn)//'"  value(s): ",20e13.5)'   
   do i = 1, ndiri
      do j = 1, ndofn
         write(s2(j:j),'(i1)')mydata%codediri(j,i)
      end do  
      if ( mydata%odirichl(i) == 'p' ) then
         s1 = 'point #'
      else if ( mydata%odirichl(i) == 'l' ) then
         s1 = 'line  #'
      else if ( mydata%odirichl(i) == 'f' ) then
         s1 = 'face  #'
      end if
      write(*,fmt)s1,i2a(mydata%ldirichl(i)),s2,mydata%vdirichl(:,i)
   end do            
   
   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|     Neumann boundary conditions    |"
   write(*,'(3x,a,/)')"+====================================+"   
   write(*,'(3x,a,i0)')"  Number of Neumann bc : ",nneum
   
   lenN = 0; lenV = 0
   do i = 1, nneum
      write(s1,'("#",i0)')mydata%lneumann(i); s1 = adjustl(s1)
      if ( len_trim(s1) > lenN ) lenN = len_trim(s1)
   end do
   write(fmt,'(i0)')lenN+6
   write(fmt0,'(i0)')ndofn
   fmt = '(5x,a'//trim(fmt)//',2x,"code(s):",1x,a'//trim(fmt0)//',2x,"value(s):",20e13.5)'
   do i = 1, nneum   
      write(s1,'("#",i0)')mydata%lneumann(i); s1 = adjustl(s1)   
      do j = 1, ncmpN
         write(s2(j:j),'(i1)')mydata%codeneum(j,i)
      end do
      if ( mydata%oneumann(i) == 'l' ) s1 = 'line :'//trim(s1)
      if ( mydata%oneumann(i) == 'p' ) s1 = 'point:'//trim(s1)
      if ( mydata%oneumann(i) == 'f' ) s1 = 'face :'//trim(s1)
      write(*,fmt)trim(s1), trim(s2), mydata%vneumann(:,i)  
   end do    
   
   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|           Linear solver            |"
   write(*,'(3x,a,/)')"+====================================+"   
   write(*,'(3x,a,a)' )"  name of the solver    : ",trim(name_linsolver)
   write(*,'(3x,a,i0)')"  level of verbosity    : ",verbosity(3)
   write(*,'(3x,a,a )')"  sparse storage format : ",trim(name_storage)
   write(*,'(3x,a,a )')"  reordering scheme name: ",trim(name_reorderMeth)
   write(*,'(3x,a,a)' )"  Options (for iterative solver only): "
   write(*,'(3x,a,i0)' )"  - Maximum number of iterations : ",nint(optlinsol(1))
   write(s1,'(e13.5)')optlinsol(2)
   write(*,'(3x,a,a)' )"  - Relative tolerance           : ",trim(adjustl(s1))
   write(s1,'(e13.5)')optlinsol(3)
   write(*,'(3x,a,a)' )"  - Absolute tolerance           : ",trim(adjustl(s1))
   write(*,'(3x,a,i0)')"  - krylov subspace dimension    : ",nint(optlinsol(4))
   write(*,'(3x,a,i0)')"  - Fill-in parameter (for ILUT) : ",nint(optlinsol(5))
   
   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|         Non-linear solver          |"
   write(*,'(3x,a,/)')"+====================================+"   
   write(*,'(3x,a,a)' )"  name of the solver: ",trim(name_nlinsolver)
   write(*,'(3x,a,i0)')"  level of verbosity: ",verbosity(4)   
   write(*,'(3x,a,a)' )"  Options: "
   write(*,'(3x,a,i0)')"  - Maximum number of iterations       : ",nint(optnlinsol(1))
   write(s1,'(e13.5)')optnlinsol(2)
   write(*,'(3x,a,a)' )"  - Relative tolerance                 : ",trim(adjustl(s1))
   write(s1,'(e13.5)')optnlinsol(3)
   write(*,'(3x,a,a)' )"  - Absolute tolerance                 : ",trim(adjustl(s1))
   write(*,'(3x,a,i0)')"  - Number of initial Picard iterations: ",nint(optnlinsol(4))
   write(*,'(3x,a,i0)')"  - Number of increments:                ",nint(optnlinsol(5))

   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|         Output file format         |"
   write(*,'(3x,a,/)')"+====================================+" 
   if ( any(name_outputtype =='none') ) then
      write(*,'(3x,a)')'None'
   else   
      n = 0 ; m = 0
      do i = 1, size(name_outputtype)
         if ( len_trim(name_outputtype(i)) /= 0 ) then
            n = max( n, len_trim(name_outputtype(i)) ) 
            m = max( m, len_trim(name_outputfmt(i))  )
         end if
      end do
      fmt = '(5x,"type: ",a'//i2a(n)//'",  format: ",a'//i2a(m)//',", frequency: ",i0)'
      do i = 1, 10
         if ( len_trim(name_outputtype(i)) /= 0 ) &
            write(*,fmt)trim(name_outputtype(i)), trim(name_outputfmt(i)),freq_output(i)
      end do
      write(*,'(/,5x,a)')'Output directory: '//trim(name_outputDir)
   end if      

   write(*,'(/,3x,a)')"+====================================+" 
   write(*,'(3x,a  )')"|          Verbosity levels          |"
   write(*,'(3x,a,/)')"+====================================+" 
   write(*,'(3x,a,i0)')" - for input/output      : ",verbosity(1)
   write(*,'(3x,a,i0)')" - for F.E.M             : ",verbosity(2)
   write(*,'(3x,a,i0)')" - for linear solver     : ",verbosity(3)
   write(*,'(3x,a,i0)')" - for non-linear solver : ",verbosity(4)

   write(fmt,'(3x,a,i0,a)')"  Number of warnings: ",err_ncall(2)
   if ( err_ncall(2) > 0 ) fmt = trim(fmt)//" (see messages above)"
   totlen = len_trim(fmt)
   write(s1,'(a,i0,a)')'(3x,',totlen,'("-"))'
   write(*,'(/)'); write(*,s1); write(*,'(a)')trim(fmt); write(*,s1)

    
   write(*,'(/,a)')" :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
   write(*,'(a  )')" ::--        E N D   O F   D A T A   R E P O R T        --::"
   write(*,'(a,/)')" :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"   
   
   END SUBROUTINE input_summary


!=============================================================================================
   SUBROUTINE input_error ( stat, msg, err, where, numl, file, rec, append )
!=============================================================================================
   type     (err_t),           intent(in out) :: stat
   character(len=*),           intent(in    ) :: msg, where, file, rec
   integer  (Ikind),           intent(in    ) :: err, numl
   character(len=*), optional, intent(in    ) :: append
!---------------------------------------------------------------------------------------------    
!  Completes error message with the line number, the file name and the last record read
!---------------------------------------------------------------------------------------------    

!- local variables: -------------------------------------------------------------------------- 
   logical :: before   
!---------------------------------------------------------------------------------------------  

   if ( stat > IZERO ) then
      if ( present(append) ) before = (append == 'before')
      call stat%AddMsg ( msg=trim(msg), before=before, newline=.true. )
      call stat%AddTrace ( where )
   else
      stat = err_t ( stat = err, where = where, msg = msg )
   end if

   call stat%AddMsg ( '. Current record: ' // trim(rec) // NLT //            &
                      '. Line ' // i2a(numl) // ' of the file ' // trim(file), &
                      newline = .true.                                         )
   
   END SUBROUTINE input_error


!=============================================================================================
   SUBROUTINE input_errorOld (uerr, num, file, ligne, msg, nmsg, where, warning)
!=============================================================================================
   integer  (Ikind), intent(in), optional :: num, nmsg, uerr
   integer         , intent(in), optional :: warning
   character(len=*), intent(in), optional :: file
   character(len=*), intent(in), optional :: ligne
   character(len=*), intent(in), optional :: msg(*)
   character(len=*), intent(in), optional :: where
!---------------------------------------------------------------------------------------------    
!  Affichage d'un message d'erreur ou d'avertissement. Arret du programme dans le 1er cas
!
!  Si uerr n'est pas present, l'affichage se fait sur la sortie standard, sinon uerr doit
!  correspondre a l'unite logique d'un fichier prealablement ouvert.
!----------------------------------------------------------------------------------- R.H 11/16  

!- local variables: --------------------------------------------------------------------------    
   integer  (Ikind ) :: i, ue
!---------------------------------------------------------------------------------------------            

   if (present(uerr)) then 
     ue = uerr
   else 
     ue = 6
   end if    
   
   write(ue,*)
   if (present(warning)) then 
      if (warning > 0) then
         warn = warn + 1
         write(ue,'(a)')'Warning:'   
      end if   
   else   
      write(ue,'(a)')'Error:'
   end if   
    
   if (present(where)) then
      write(ue,'(a)')'--> in input_'//trim(adjustl(where))
   end if
       
   if (present(file)) then
      write(ue,'(a)')'--> while reading the input file "'//trim(adjustl(file))//'"'
   end if   
    
   if (present(num)) then
      if (present(warning)) then
         write(ue,'(a,i0)')'--> Warning for the line #',num
      else   
         write(ue,'(a,i0)')'--> Error at line #',num
      end if
   end if
    
   if (present(ligne)) write(ue,'(a)')'--> The last read line was: '//trim(adjustl(ligne))   
    
    
   if (present(msg)) then
      if (present(nmsg)) then
         do i = 1, nmsg
            if (len_trim(msg(i)) > 0) write(ue,'(a)')'--> '//trim(msg(i))
         end do
      else   
         write(ue,'(a)')'--> '//trim(msg(1))
      end if   
   end if      
    
   if (.not. present(warning)) then
      write(ue,'(a,/)')'--> STOP'
      stop
   else
      if (warning < 0) then
         write(ue,'(a,/)')'--> ABORT (stop required by the user)'
         stop
      end if
   end if       
      
   write(ue,*)

   END SUBROUTINE input_errorOld 
   
   
   
   subroutine input_forFlagShyp ( mymesh, mydata, stat )
   type(mesh_t ), intent(in    ) :: mymesh
   type(udata_t), intent(in    ) :: mydata
   type(err_t  ), intent(in out) :: stat
   ! Prepare an input file for Flagshyp
   character(len=*), parameter :: Here = 'input_forFlagShyp'
   integer(Ikind) :: u, idir, iref, jref, err, ifac, in, ip, id, ie, im, l(3) = [1,2,4]
   integer(Ikind) :: matyp, nplds, nprs, nbpel, nincr, miter
   real   (Rkind) :: cnorm, searc, arcln, Young, Poiss, rho
   character(len=1) :: obj
   character(len=:), allocatable :: eltyp
   integer(Ikind), allocatable :: code(:), codep(:,:), codel(:,:), codef(:,:)
   real   (Rkind), allocatable :: presc(:,:), prescp(:,:), prescl(:,:), prescf(:,:), &
                                  props(:,:), gravt(:), g(:)

   if ( stat > IZERO ) return

      allocate(props(3,nmats),gravt(ndime),g(ndime))

   if ( name_model_pb == 'neohookean(3d)' ) then
      matyp = 1
      do im = 1, nmats
         Young = mydata%vmatprop(1,im) ; Poiss = mydata%vmatprop(2,im)
         rho = sqrt( dot_product(mydata%vmatprop(3:,im),mydata%vmatprop(3:,im)) )
         if ( rho /= RZERO ) then
            g = mydata%vmatprop(3:,im) / rho
            if ( im > 1 ) then
               if ( dot_product(gravt-g, gravt-g) > 1e-6 ) then
                  print*,'=> warning: input file for flagshyp not wrote'
                  print*,'Please check your body forces'
                  print*,'(must have the same direction for all materials)'
                  return
               end if
            end if
         else
            g = RZERO
            rho = RONE
         end if
         gravt = g ! I considere here a unit vector and that rho = || body force ||
         props(1,im) = rho
         props(2,im) = 0.5  *Young / (1.0+Poiss)
         props(3,im) = Poiss*Young / (1.0+Poiss) / (1.0-2.0*Poiss)
      end do
   else
      print*,'=> warning: input file for flagshyp not wrote'
      print*,'no correspondance between name_model_pb and matyp'
      print*,'name_model_pb: '//name_model_pb
      return   
   end if
      
   if ( nneum /= 0 ) then
      print*,'=> warning: input file for flagshyp not wrote'
      print*,' Neumann boundary condition not yet considered'
   end if

1   allocate(code(npoin),codep(ndofn,npoin),codel(ndofn,npoin),codef(ndofn,npoin), &
            source = IZERO)
   allocate(presc(ndofn,npoin),prescp(ndofn,npoin),prescl(ndofn,npoin),prescf(ndofn,npoin), &
            source = RZERO)
   nprs = 0 ; nplds = 0 ; nbpel = 0
!
!  Prescribed codes in flagshyp:
!               (0,0,0)  0: free
!               (1,0,0)  1: x fixed
!               (0,1,0)  2: y fixed
!               (1,1,0)  3: x+y fixed 
!               (0,0,1)  4: z fixed
!               (1,0,1)  5: x+z fixed
!               (0,1,1)  6: y+z fixed
!               (1,1,1)  7: x+y+z fixed          
!            => (i,j,k)  n: i + 2*j + 4*k
        
   do idir = 1, ndiri
      iref = mydata%ldirichl(idir); obj = mydata%odirichl(idir)
      if ( obj == 'f' ) then 
         err = 1
         do ifac = 1, nface
            jref = mymesh%lface(1,ifac)
         
            if ( iref == jref ) then
               err = 0
               do in = 2, ndfac+1
                  ip = mymesh%lface(in,ifac)
                  codef(:,ip) = mydata%codediri(:,idir)
                  prescf(:,ip) =  mydata%vdirichl(:,idir)
               end do
            end if
         end do
          
      else if ( obj == 'l' ) then ! bc on a line
         err = 1
         do ifac = 1, nedge
            jref = mymesh%ledge(1,ifac)
         
            if ( iref == jref ) then
               err = 0
               do in = 2, ndedg+1
                  ip = mymesh%ledge(in,ifac)
                  codel(:,ip) = mydata%codediri(:,idir)
                  prescl(:,ip) =  mydata%vdirichl(:,idir)                  
               end do
            end if
         end do

      else if ( obj == 'p' ) then  ! bc at a point
         err = 1
         do ifac = 1, npoif
            jref = mymesh%lpoin(1,ifac)
         
            if ( iref == jref ) then
               err = 0
               ip = mymesh%lpoin(2,ifac)
               codep(:,ip) = mydata%codediri(:,idir)
               prescp(:,ip) =  mydata%vdirichl(:,idir)
            end if
         end do
      end if
           
   end do     
   
   nprs = 0
   do ip = 1, npoin
      code(ip) = IZERO
      do id = 1, ndofn
         codep(id,ip) = codep(id,ip) + codel(id,ip) + codef(id,ip)
         presc(id,ip) = prescp(id,ip) + prescl(id,ip) + prescf(id,ip)
         if ( codep(id,ip) > 1 ) codep(id,ip) = 1
         code(ip) = code(ip) + codep(id,ip)*l(id)
      end do
      if ( code(ip) == 0 ) cycle
                  
      do id = 1, ndofn
         if ( presc(id,ip) /= RZERO ) nprs = nprs + 1
      end do
   end do
   
   if ( trim(name_element) == 'tria03' ) then
      eltyp = 'tria3'
   else if ( trim(name_element) == 'tria06' ) then
      eltyp = 'tria6'
   else if ( trim(name_element) == 'quad04' ) then
      eltyp = 'quad4'
   else if ( trim(name_element) == 'tetr04' ) then
      eltyp = 'tetr4'      
   else if ( trim(name_element) == 'tetr10' ) then
      eltyp = 'tetr10'      
   else if ( trim(name_element) == 'hexa08' ) then
      eltyp = 'hexa8'  
   else
      print*,'=> warning: input file for flagshyp not wrote'
      print*,'no correspondance between name_element and eltyp'
      print*,'name_element: '//trim(name_element)
      return
   end if

   open(newunit=u, file = trim(name_input_file)//'.flagshyp', &
        action = 'write', status = 'replace')
   write(u,'(a)')'Translation from '//trim(ifile)//' to Flagshyp08v30 input file'
   write(u,'(a)') eltyp
   write(u,'(i0)') npoin
   do ip = 1, npoin
      write(u,'(i0,1x,i0,2x,3(1x,g0))') ip,code(ip),mymesh%coord(:,ip)
   end do
   write(u,'(i0)') nelem
   do ie = 1, nelem
      write(u,'(i0,1x,i0,*(1x,i0))')ie,mymesh%numdom(ie),mymesh%lnods(:,ie)
   end do
   write(u,'(i0)') nmats
   do im = 1, nmats
      write(u,'(i0,1x,i0)') im, matyp
      write(u,'(*(g0,1x))') props(:,im)
   end do
   write(u,'(i0,1x,i0,1x,i0,1x,3e13.5)') nplds,nprs,nbpel,gravt(:)
   do ip = 1, npoin
      if ( code(ip) == 0 ) cycle
      do id = 1, ndofn
         if ( presc(id,ip) /= RZERO ) write(u,'(i0,1x,i0,1x,g0)')ip,id,presc(id,ip)
      end do
   end do  
   miter = nint(optnlinsol(1)) 
   nincr = nint(optnlinsol(5))
   cnorm = optnlinsol(2)
   searc = RZERO
   arcln = RZERO
   write(u,'(*(g0,1x))')nincr,RONE,RONE/nincr,miter,cnorm,searc,arcln,IZERO,IZERO,IZERO,IZERO
   close(u)
       
   end subroutine input_forFlagShyp
   
END MODULE input_m
