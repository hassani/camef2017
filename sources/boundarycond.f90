MODULE boundarycond_m

   use kindParameters_m
   use param_m   
   use defTypes_m
   
   use constants_m, only: UERROR
   use util_m     , only: util_trisurf
    
   implicit none
    
CONTAINS
    
!=============================================================================================
   SUBROUTINE boundarycond_Dirichlet1 ( eldata )
!=============================================================================================
   type(eldata_t), intent(in out) :: eldata
!---------------------------------------------------------------------------------------------
!  Attention: This routine must be called before assembling the element load vector Fe into 
!             the global rhs (==> call it in the loop over the elements).
!             It computes, when appropriate, the response to a Dirichlet BC and puts it into
!             element rhs.
!             An other way is to introduce Dirichlet BC after the assembling by modifying 
!             the assembled matrix and rhs (e.g. by penalisation)
!
!  The element load vector (Fe) is modified by taking into account the prescribed boundary 
!  condition as follows:
!
!            Fe <-- Fe - Ke * Ue
!
!  where Ke is the element stiffness matrix and where the components of Ue are either 0  
!  (for free dofs) or are the prescribed value (for constrained dof. Note that if Ue is
!  an increment (non-linear problems) prescribed values must be 0).
!
!  Remark: this routine should be used only if the prescribed dofs have been previsously 
!  eliminated from the global system (i.e. in accordance with the fact that the prescribed
!  dofs are not numbered). 
!
!  Inputs
!
!        eldata%dof : elemental dof #. (negative # corresponds to a constrained dof)
!        eldata%K   : elemental stiffness matrix
!        eldata%F   : elemental load vector
!        eldata%U   : contains the prescribed values where dof < 0
!
!  Output
!
!        eldata%F   : the modified elemental load vector 
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i
   real   (Rkind) :: presc(size(eldata%dof))
!---------------------------------------------------------------------------------------------            
    
   presc(:) = 0.0_Rkind

   do i = 1, size(eldata%dof)
      if ( eldata%dof(i) < 0 ) presc(i) = eldata%U(i)
   end do
       
   eldata%F = eldata%F - matmul(eldata%K,presc)   
          
   END SUBROUTINE boundarycond_Dirichlet1


!=============================================================================================
   SUBROUTINE boundarycond_Dirichlet ( Ke, Fe, ldof, vdiri )
!=============================================================================================
   integer(Ikind), intent(in    ) :: ldof(:,:)
   real   (Rkind), intent(in    ) :: Ke(:,:)
   real   (Rkind), intent(in    ) :: vdiri(:,:)
   real   (Rkind), intent(in out) :: Fe(:)
!---------------------------------------------------------------------------------------------
!  Attention: This routine must be called before assembling the element load vector Fe into 
!             the global rhs (==> call it in the loop over the elements).
!             It computes, when appropriate, the response to a Dirichlet BC and put it into
!             element rhs.
!             An other way is to introduce Dirichlet BC after the assembling by modifying 
!             the assembled matrix and rhs (e.g. by penalisation)
!
!  The element load vector (Fe) is modified by taking into account the prescribed boundary 
!  condition as follows:
!
!            Fe <-- Fe - Ke * Ue
!
!  where Ke is the element stiffness matrix and where the components of Ue is either 0  
!  (for free dofs) or the prescribed value (for constrained dof)
!
!  Remark: this routine should be used only if the prescribed dofs have been previsously 
!  eliminated from the global system (i.e. in accordance with the fact that the prescribed
!  dofs are not numbered). 
!
!  Inputs
!
!        ldof : elemental dof #. A negative # corresponds to a constrained dof.
!        Ke   : elemental stiffness matrix
!        Fe   : elemental load vector
!        vdiri: prescribed values
!
!  Output
!
!        Fe   : the modified load vector 
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: ndofn, nnode, idof, in, id, idir
   real   (Rkind) :: presc(size(Fe))
!---------------------------------------------------------------------------------------------            

   ndofn = size(ldof,1) ! number of d.o.f. per node
   nnode = size(ldof,2) ! number of nnode per element
    
   presc(:) = 0.0_Rkind

   idof = 0
   do in = 1, nnode
      do id = 1, ndofn
         idof = idof + 1
         idir =-ldof(id,in)                          ! Ref. of the Dirichlet bc
         if (idir > 0) presc(idof) = vdiri(id,idir)  ! The prescribed value
      end do
   end do
    
   Fe = Fe - matmul(Ke,presc)
          
   END SUBROUTINE boundarycond_Dirichlet


!=============================================================================================
   SUBROUTINE boundarycond_Neumann3d ( mymesh, mydata, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(udata_t ), intent(in    ) :: mydata 
   type(linsys_t), intent(in out) :: femsys 
   type(err_t   ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Computes the equivalent nodal load for Neumann's boundary conditions for 3D case
!  (imposed load on faces, edges or points)
!
!  Assumptions:
!       - the load is constant on each face or egde (P0)
!       - the edges are 2-noded (linear) or 3-noded (quadratic)
!       - the edges are rectilinear
!       - the faces are 3-noded (linear), 4-noded (bi-linear) or 6-noded (quadratic)
!       - the faces are planar
!       - for quadratic elements, the non-vertices nodes are at the middle locations  
!
!  If one of these assumptions is not fulfilled use the NEUMANN3DQ subroutine (version
!  with quadrature schemes).
! 
!  With these assumptions, the contribution of a given edge to the global load can be computed
!  by hand:
!
!  - linear case:
!
!        (1)                                 
!         |
!         |  <--- F      F_i =  F * L / 2,   i = 1, 2
!         |                    
!        (2)
!  
!  - quadratic case:
!
!        (1)                                 
!         |
!        (3)  <--- F      F_i = F * L / 6,       i = 1, 2
!         |                   = F * 4 * L / 6,   i = 3
!        (2)
!                        
!
!  And for a given face:
!
!  - linear case or bi-linear cases:                   - quadratic cases:
!
!         (1)               (1)-------(4)                         (1)  
!        /   \               |         |                         /   \
!       /     \              |         |                       (4)   (6)
!      /       \             |         |                       /       \
!    (2)-------(3)          (2)-------(3)                    (2)--(5)--(3)
!
!   F_i = F * S / 3,       F_i = F * S / 4               F_j = 0,   j = 1, 2, 3 
!                       
!     i = 1, 2, 3           i = 1, 2, 3, 4               F_i = F * S / 3,   i = 4, 5, 6
!
!    
!  Inputs:
!
!       nneum : number of Neumann's bc
!       nedge : number of referenced external edges
!       npoif : number of referenced external points
!       ndofn : number of dof

!       mymesh%ledge   : the edges references and their nodes
!       mymesh%lpoin   : the points references and their attached nodes
!       mydata%lneumann: # of points or edges where a bc applies
!       mydata%oneumann: geometry type (point or edge)
!       mydata%vneumann: imposed values
!       mydata%codeneum: code that indicates if these values are given in
!                        local or global frame (for vectorial problem) 
!
!  Ouputs:
!
!       femsys%Rhs: the assembled right hand side       
!---------------------------------------------------------------------------------------------

!--local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: here = 'boundarycond_Neumann3d'
   real     (Rkind), parameter :: z = 0.0_Rkind, d = 0.5_Rkind, u = 1.0_Rkind,      & 
                                  t = 3.0_Rkind, q = 4.0_Rkind, s = 6.0_Rkind
                                  
   integer  (Ikind) :: nsomt_e, nsomt_f, nnod_e, nnod_f, case_e, case_f
   integer  (Ikind) :: iref, jref, in, id, ip, i, j, idof, frame
   
   real     (Rkind) :: area, stri, coef, ut, us, uq, qs
   
   real     (Rkind) :: cface(6,2), cedge(3,2), solic(ncmpN), val(ncmpN), &
                       xyz(3,4), xyzt(3,3), unitnormal(3), v(3)
                        
   character(len=1 ) :: obj
!---------------------------------------------------------------------------------------------
   
   if (nneum == 0) return
   
   if (name_geometry == 'tetraedre') then
      nsomt_f = 3  ! number of vertices on face
      ut = u / t
      cface(:,1) = [ut, ut, ut, z , z , z ]   ! coef. for 3-noded triangle face
      cface(:,2) = [z , z , z , ut, ut, ut]   ! coef. for 6-noded triangle face
   else if (name_geometry == 'hexaedre') then
      nsomt_f = 4 ! number of vertices on face
      uq = u / q   
      cface(:,1) = [uq, uq, uq, uq, z, z] ! coef. for 4-noded quad face
   else
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg ='Unknown element geometry: '//trim(name_geometry) ) 
      return  
   end if
   
   nsomt_e = 2 ! number of vertices on edge   
   us = u / s; qs = q / s
   cedge(:,1) = [d , d , z ] ! coef. for 2-noded edge
   cedge(:,2) = [us, us, qs] ! coef. for 3-noded edge
      
    
   nnod_f = size(mymesh%lface,1) - 1 ! number of nodes on face

   if (nnod_f /= 3 .and. nnod_f /= 6 .and. nnod_f /= 4) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'this subroutine is only applicable for 3-, 4-, 6-noded faces' ) 
      return  
   end if
   
   nnod_e = size(mymesh%ledge,1) - 1 ! number of nodes on edge

   if (nnod_e /= 2 .and. nnod_e /= 3) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'this subroutine is only applicable 2-noded or 3-noded edges' ) 
      return  
   end if
   
   case_f = 1; if (nnod_f /= nsomt_f) case_f = 2
   case_e = 1; if (nnod_e /= nsomt_e) case_e = 2
    
   do i = 1, nneum
   
      iref = mydata % lneumann(i)  ! ref # of the ith Neumann bc
      
      obj  = mydata % oneumann(i)  ! object identifier ('p' for point or 'l' for line)
      
      val  = mydata % vneumann(:,i) ! the given values
            
      if (obj == 'p') then
!
!-       Concentrated load. Find in the mesh the nodes of reference "iref":
!      
         do j = 1, npoif 
            jref = mymesh % lpoin(1,j) ! ref. # of the jth boundary node
            if (jref == iref) then
               ip = mymesh % lpoin(2,j)! global # of this node 
               do id = 1, ncmpN
                  idof = femsys % Dof(id,ip)
                  femsys % Rhs(idof) = femsys % Rhs(idof) + val(id)
               end do
            end if
         end do
         
      else if (obj == 'l') then
!
!-       Distributed load on an edge. Find in the mesh the edges of references "iref":                
!                           
         do j = 1, nedge   
            jref = mymesh % ledge(1,j) ! ref. # of the jth edge
            if (jref == iref) then  
               xyz(:,1:nsomt_e) = mymesh % coord(:, mymesh % ledge(2:nsomt_e+1,j)) ! vertices coordinates
               v = xyz(:,2) - xyz(:,1) ! tangential vector
               
               solic = val*sqrt(dot_product(v,v))
                     
               do in = 1, nnod_e
                  ip = mymesh % ledge(1+in,j)
                  coef = cedge(in,case_e)
                  do id = 1, ncmpN
                     idof = femsys%Dof(id,ip)
                     if (idof > 0) femsys%Rhs(idof) = femsys%Rhs(idof) + coef*solic(id)
                  end do
               end do
            end if
         end do
         
      else if (obj == 'f') then
!
!-       Distributed load on a face. Find in the mesh the faces of references "iref":                
!        
         frame = maxval(mydata % codeneum(:,i))  ! =1 for global frame, =2 for local frame

         do j = 1, nface
       
            jref = mymesh % lface(1,j)
       
            if (jref == iref) then
               xyz(:,1:nsomt_f) = mymesh%coord(:,mymesh % lface(2:nsomt_f+1,j)) ! vertices coordinates
               if (nsomt_f == 3) then
!
!-                triangle area and unit normal:
!               
                  call util_trisurf (xyz, area, unitnormal)
               else
!
!-                quadrangle area (as assumed planar, split it in 2 triangles):
!
                  xyzt = xyz(:,1:3)    ; call util_trisurf (xyzt, area, unitnormal)
                  xyzt = xyz(:,[3,4,1]); call util_trisurf (xyzt, stri, unitnormal)
                  area = area + stri
               end if     

               solic = z
               
               if (frame == 1) then
!
!-                the load components are given in the global frame:
!
                  solic = val * area     
               else if (frame == 2) then
!
!-                the load components are given in the local frame
!                 (in fact, only the normal component at present)
!                 Compute its components in the global frame:
!                          
                  if (ncmpN == 1) then
                     solic = val(1) * area
                  else
                     solic = val(1) * area * unitnormal
                  end if
               end if   
               
               do in = 1, nnod_f
                  ip = mymesh % lface(1+in,j)
                  coef = cface(in,case_f)
                  do id = 1, ncmpN
                     idof = femsys % Dof(id,ip)
                     if (idof > 0) femsys % Rhs(idof) = femsys % Rhs(idof) + coef*solic(id)
                  end do
               end do
            end if
         end do
      end if   
   end do                 
                       
   END SUBROUTINE boundarycond_Neumann3d


!=============================================================================================
   SUBROUTINE boundarycond_Neumann2d ( mymesh, mydata, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(udata_t ), intent(in    ) :: mydata 
   type(linsys_t), intent(in out) :: femsys 
   type(err_t   ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Computes the equivalent nodal load for Neumann's boundary conditions for 2D case
!  (imposed load on edges or points)
!
!  Assumptions:
!       - the load is constant on each edge (P0)
!       - the edges are 2-noded (linear) or 3-noded (quadratic)
!       - the edges are rectilinear
!       - for 3-noded edge, the third node is located at the middle  
!
!  If one of these assumptions is not fulfilled use the NEUMANN2DQ subroutine (version with
!  quadrature schemes).
! 
!  With these assumptions, the contribution of a given edge to the global load can be computed
!  by hand:
!
!  - linear case:
!
!        (1)                                 
!         |
!         |  <--- F      F_i =  F * L / 2,   i = 1, 2
!         |                    
!        (2)
!  
!  - quadratic case:
!
!        (1)                                 
!         |
!        (3)  <--- F      F_i  = F * L / 6,       i = 1, 2
!         |                    = F * 4 * L / 6,   i = 3
!        (2)
!                        
!
!  Inputs:
!
!       nneum : number of Neumann's bc
!       nedge : number of referenced external edges
!       npoif : number of referenced external points
!       ndofn : number of dof

!       mymesh%ledge   : the edges references and their nodes
!       mymesh%lpoin   : the points references and their attached nodes
!       mydata%lneumann: # of points or edges where a bc applies
!       mydata%oneumann: geometry type (point or edge)
!       mydata%vneumann: imposed values
!       mydata%codeneum: code that indicates if these values are given in
!                        local or global frame (for vectorial problem) 
!
!  Ouputs:
!
!       femsys%Rhs: the assembled right hand side       
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: here = 'boundarycond_Neumann2d'
   real     (Rkind), parameter :: z = 0.0_Rkind, d = 0.5_Rkind, u = 1.0_Rkind,   &
                                  q = 4.0_Rkind, s = 6.0_Rkind

   integer  (Ikind) :: nsomt, noeud, iref, jref, cas, in, id, ip, i, j, idof, frame
   real     (Rkind) :: xy(2,2), n(2), t(2), solic(ncmpN), val(ncmpN), c(3,2)
   real     (Rkind) :: us, qs, coef, dL
   character(len=1) :: obj
!---------------------------------------------------------------------------------------------

   if (nneum == 0) return
    
   nsomt = 2                        ! number of vertices
   noeud = size(mymesh%ledge,1) - 1 ! number of nodes

   if (noeud /= 2 .and. noeud /= 3) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg='this subroutine is only applicable for 2-noded or 3-noded edges' )
      return
   end if
              
   us = u / s ; qs = q * us
              
   c(:,1) = [d , d , z ] ! coefficients for a 2-noded edge
   c(:,2) = [us, us, qs] ! coefficients for a 3-noded edge
    
   cas = 1; if (noeud == 3) cas = 2
        
   do i = 1, nneum
      
      iref = mydata % lneumann(i)  ! ref # of the ith Neumann bc
      
      obj  = mydata % oneumann(i)  ! object identifier ('p' for point or 'l' for line)
      
      val  = mydata % vneumann(:,i) ! the given values
      
      if (obj == 'p') then
!
!-       Concentrated load. Find in the mesh the nodes of reference "iref":
!      
         do j = 1, npoif 
            jref = mymesh % lpoin(1,j) ! ref. # of the jth boundary node
            if (jref == iref) then
               ip = mymesh % lpoin(2,j)! global # of this node 
               do id = 1, ncmpN
                  idof = femsys % Dof(id,ip)
                  femsys % Rhs(idof) = femsys % Rhs(idof) + val(id)
               end do
            end if
         end do
         
      else if (obj == 'l') then
!
!-       Distributed load. Find in the mesh the edges of references "iref":                
!                  
         frame = maxval(mydata % codeneum(:,i))  ! =1 for global frame, =2 for local frame
         
         do j = 1, nedge   
            jref = mymesh % ledge(1,j) ! ref. # of the jth edge
            if (jref == iref) then  
            
               xy(:,1:nsomt) = mymesh % coord(:, mymesh % ledge(2:nsomt+1,j) ) ! vertices coordinates
               t = xy(:,2) - xy(:,1)  ! tangential vector
               
               solic = z
               if (frame == 1) then
!
!-                The load is given in the global frame
!               
                  dL = sqrt(dot_product(t,t))
                  solic = val*dL
               else if (frame == 2) then      
!
!-                The load is given in the local frame. 
!                 Compute its components in the global one:
!               
                  n(1) = t(2) ; n(2)=-t(1)    ! outward normal vector    
                  solic = val(1)*n + val(2)*t !                    
               end if   
                     
               do in = 1, noeud
                  ip = mymesh%ledge(1+in,j)
                  coef = c(in,cas)
                  do id = 1, ncmpN
                     idof = femsys%Dof(id,ip)
                     if (idof > 0) femsys%Rhs(idof) = femsys%Rhs(idof) + coef*solic(id)
                  end do
               end do
            end if
         end do
      end if   
   end do      
                         
   END SUBROUTINE boundarycond_Neumann2d


!=============================================================================================
   SUBROUTINE boundarycond_Neumann3dQ ( mymesh, mydata, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(udata_t ), intent(in    ) :: mydata 
   type(linsys_t), intent(in out) :: femsys 
   type(err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------------------    
!  Version par integration numerique
!  a re-implementer
!---------------------------------------------------------------------------------------------  
   
   stat = err_t ( stat = UERROR, where = 'boundarycond_Neumann3dQ', &
                   msg = 'subroutine not yet implemented')
   
   END SUBROUTINE boundarycond_Neumann3dQ


!=============================================================================================
   SUBROUTINE boundarycond_Neumann2dQ ( mymesh, mydata, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(udata_t ), intent(in    ) :: mydata 
   type(linsys_t), intent(in out) :: femsys 
   type(err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------------------    
!  Version par integration numerique
!  a re-implementer
!---------------------------------------------------------------------------------------------  
   
   stat = err_t ( stat = UERROR, where = 'boundarycond_Neumann2dQ', &
                   msg = 'subroutine not yet implemented' )
   
   END SUBROUTINE boundarycond_Neumann2dQ
    
            
END MODULE boundarycond_m
