#include "error.fpp"

MODULE quadrature_m

    use kindParameters_m
    use err_m
    
    use constants_m, only: IZERO, UERROR
    use util_m     , only: i2a => util_intToChar

    use util_m, only : util_string_low

    implicit none
    
!---------------------------------------------------------------------------------------------
!
!     code CAMEF (Code d'Apprentissage a la Methode des Elements Finis)
!                 
!     MASTER 2 Ingenierie Mathematique   
!
!     INITIATION A LA METHODE DES ELEMENTS FINIS
!
!     voir le polycopie du cours "MISE EN OEUVRE DE LA METHODE DES ELEMENTS FINIS"
!     
!                               
!     Riad HASSANI, Univ. Savoie, Univ. Nice-Sophia Antipolis
!
!     version initiale (f77)  : 09/2000
!     version revisitee (f90) : 04/2016
!
!---------------------------------------------------------------------------------------------
!
!     Fichier quadrature.f90
!
!     petite librairie de routines qui regroupe des formules de quadrature sur des simplexes
!     de reference ( [-1,1]^ndime, triangle et tetraedre standards )
!     
!     Adaptation faite a partir des sources suivantes :
!      - Dhatt & Touzot (une presentation de la methode des elements finis)
!      - Hinton & Owen (Finite Element in Plasticity)
!      - Cheung (A Pract. Introd. to F.E.-Analysis)
!      - code Geodyn (J.P. Vilotte)
!
!
!     Chaque routine (quadrature_XXXXXX) est relative a un simplexe de reference.
!
!     Une routine pilote (quadrature_driver) permet l'appel a la routine desiree.
!     Elle necessite le nom du simplexe (segement, carre, cube, triangle ou tetraedre)
!
!---------------------------------------------------------------------------------------------
    
CONTAINS
    
!=============================================================================================
    SUBROUTINE quadrature_driver ( coorIP, weigIP, nbrIP, ndime, geom, stat )
!=============================================================================================
    integer  (Ikind), intent(in    ) :: nbrIP, ndime
    character(len=*), intent(in    ) :: geom 
    real     (Rkind), intent(   out) :: coorIP(:,:), weigIP(:)
    type     (err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!   Routine pilote qui en fonction de "geom" selectionne l'appel a la routine demandee
!
!   - Entrees :
!          geom  (character) : peut etre "segment", "carre", "cube", "triangle" ou "tetraedre"
!          nbrIP (integer  ) : nbr de points d'integration demande
!          ndime (integer  ) : dimension de l'espace (utilisee uniquement pour verifier
!                              la coherence)
!
!   - Sorties :
!         coorIP (real     ) : table qui contient les coordonnees des nbrPI P.I.
!         weigIP (real     ) : table qui contient les poids des nbrPI P.I.  
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    character(len=*), parameter   :: Here = 'quadrature_driver' 
    character(len=:), allocatable :: geomlow
    integer  (Ikind)              :: err
!---------------------------------------------------------------------------------------------

    if ( stat > IZERO ) return 
    
    geomlow = util_string_low ( adjustl(geom) )
    
    err = 1
    
    select case ( geomlow )
    
       case ( "segment" )
         if ( ndime == 1 ) then
            call quadrature_segment   ( coorIP, weigIP, nbrIP, stat )  ; err = 0
         end if   
    
       case ( "carre" )
         if ( ndime == 2 ) then       
            call quadrature_carre     ( coorIP, weigIP, nbrIP, stat )  ; err = 0
         end if   

       case ( "cube" )
         if ( ndime == 3 ) then       
            call quadrature_cube      ( coorIP, weigIP, nbrIP, stat )  ; err = 0 
         end if   

       case ( "triangle" )
         if ( ndime == 2 ) then       
            call quadrature_triangle  ( coorIP, weigIP, nbrIP, stat )  ; err = 0         
         end if   

       case ( "tetraedre" )
         if ( ndime == 3 ) then       
            call quadrature_tetraedre ( coorIP, weigIP, nbrIP, stat )  ; err = 0    
         end if   

       case default
         stat = err_t(stat=UERROR, where=Here, msg='Unexpected geometry "' // geomlow // '"')
         return
                  
    end select
    
    error_TraceNreturn(stat>IZERO, Here, stat)
    
    if (err /= 0) then
       stat = err_t( stat = UERROR, where = Here, msg= 'Inconsistency between geometry ('// &
                            geomlow // ') and spatial dimensions (' // i2a(ndime) // ')'    )
       return
    end if
        
    END SUBROUTINE quadrature_driver     
                          
                          
!=============================================================================================
    SUBROUTINE quadrature_segment ( gpt, wgpt, ng, stat )
!=============================================================================================
    integer(Ikind), intent(in    ) :: ng
    real   (Rkind), intent(   out) :: gpt(ng), wgpt(ng)
    type   (err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!   Quadrature de gauss sur l'interval [-1,1] a 1, 2, 3 ou 4 points
!
!     - en entree :
!         ng : nbr de points de gauss
!
!     - en sortie :
!         gpt  : abscisses des ng points de gauss
!         wgpt : poids associes
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    character(len=*), parameter :: Here = 'quadrature_segment' 
    real     (Rkind)            :: a1, a2, w1, w2
!---------------------------------------------------------------------------------------------

    if ( stat > IZERO ) return 
    
    select case (ng)
    
       case(1) ! formule a 1 point :

         gpt (1) = 0.0_Rkind ; wgpt(1) = 2.0_Rkind
         
       case(2) ! formule a 2 points :

         a1 = 0.577350269189626_Rkind ; w1 = 1.000000000000000_Rkind;
         
         gpt = [ -a1, a1 ] ; wgpt = [ w1, w1 ]
         
       case(3) ! formule a 3 points :
         
         a1 = 0.774596669241483_Rkind ; a2 = 0.000000000000000_Rkind
         w1 = 0.555555555555556_Rkind ; w2 = 0.888888888888889_Rkind
         
         gpt = [ -a1, a2, a1 ] ; wgpt = [ w1, w2, w1 ]
                  
       case(4) ! formule a 4 points :
         
         a1 = 0.861136311594053_Rkind ; a2 = 0.339981043584856_Rkind
         w1 = 0.347854845137454_Rkind ; w2 = 0.652145154862546_Rkind
         
         gpt = [ -a1, -a2,  a2, a1 ] ; wgpt = [ w1, w2, w2, w1 ]
         
       case default
         
         stat = err_t( stat = UERROR, where = Here, msg = i2a(ng) // &
                      '-points quadrature method not available for line element')
         return
                  
    end select

    END SUBROUTINE quadrature_segment
    
    
!=============================================================================================
    SUBROUTINE quadrature_carre ( gpt, wgpt, ng, stat )
!=============================================================================================
    integer(Ikind), intent(in    ) :: ng
    real   (Rkind), intent(   out) :: gpt(2,ng), wgpt(ng)
    type   (err_t), intent(in out) :: stat    
!--------------------------------------------------------------------------------------------- 
!   Quadrature de gauss sur le carre [-1,1]^2
!
!     - en entree :
!         ng : nbr de points de gauss (doit etre le carre d'un entier)
!
!     - en sortie :
!         gpt  : abscisses des ng points de gauss
!         wgpt : poids associes
!  
!     routine appelee : quadrature_segment
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    character(len=*), parameter :: Here = 'quadrature_carre' 
    integer  (Ikind)            :: ngaus, k, ig, jg
    real     (Rkind)            :: gpt1d(ng), wgpt1d(ng)
!---------------------------------------------------------------------------------------------  

    if ( stat > IZERO ) return 
    
    if (ng == 1) then
       ngaus = 1_Ikind
    else if (ng > 1) then
       ngaus = nint( exp ( log(real(ng,kind=Rkind)) / 2.0_Rkind ) )
    else 
       stat = err_t ( stat = UERROR, where = Here, &
                       msg = 'The number of integration points must be a positive integer' )
    end if

    if (ng /= ngaus**2) then
       stat = err_t ( stat = UERROR, where = Here, &
                       msg = 'The number of integration should be the square of an integer' )
       return
    end if
!
!-- abscisses des points de quadrature :
!
    call quadrature_segment ( gpt1d, wgpt1d, ngaus, stat )

    error_TraceNreturn(stat>IZERO, Here, stat)    
!
!-- coordonnees et poids des points d'integration
!
    k = 0
    do ig = 1, ngaus
       do jg = 1, ngaus
          k = k + 1
          gpt (1,k) = gpt1d (ig)
          gpt (2,k) = gpt1d (jg)
          wgpt(  k) = wgpt1d(ig) * wgpt1d(jg)
       end do
    end do

    END SUBROUTINE quadrature_carre
    
    
!=============================================================================================
    SUBROUTINE quadrature_cube ( gpt, wgpt, ng, stat )
!=============================================================================================
    integer(Ikind), intent(in    ) :: ng
    real   (Rkind), intent(   out) :: gpt(3,ng), wgpt(ng)
    type   (err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------  
!   Quadrature de gauss sur le cube [-1,1]^3 
!
!     - en entree :
!         ng : nbr de points de gauss (doit etre le cube d'un entier)
!
!     - en sortie :
!         gpt  : abscisses des ng points de gauss
!         wgpt : poids associes
!
!     routine appelee : quadrature_segment
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    character(len=*), parameter :: Here = 'quadrature_cube' 
    integer  (Ikind)            :: ngaus, k, ig, jg, kg
    real     (Rkind)            :: gpt1d(ng), wgpt1d(ng)
!---------------------------------------------------------------------------------------------  

    if ( stat > IZERO ) return 
    
    if (ng == 1) then
       ngaus = 1
    else if (ng > 1) then
       ngaus = nint( exp ( log(real(ng,kind=Rkind)) / 3.0_Rkind ) )
    else 
       stat = err_t ( stat = UERROR, where = Here, &
                       msg = 'The number of integration points must be a positive integer' )
       return
    end if

    if (ng /= ngaus**3) then
       stat = err_t ( stat = UERROR, where = Here, &
                       msg = 'The number of integration should be the cube of an integer' )
       return
    end if     
!
!-- abscisses des points de quadrature :
!
    call quadrature_segment ( gpt1d, wgpt1d, ngaus, stat )
    
    error_TraceNreturn(stat>IZERO, Here, stat)
!
!-- coordonnees et poids des points d'integration
!
    k = 0
    do ig = 1, ngaus
       do jg = 1, ngaus
          do kg = 1, ngaus
             k = k + 1
             gpt (1,k) = gpt1d (ig)
             gpt (2,k) = gpt1d (jg)
             gpt (3,k) = gpt1d (kg)
             wgpt(  k) = wgpt1d(ig) * wgpt1d(jg) * wgpt1d(kg)
          end do
       end do
    end do

    END SUBROUTINE quadrature_cube
    
    
!=============================================================================================
    SUBROUTINE quadrature_triangle ( gpt, wgpt, ng, stat )
!=============================================================================================
    integer(Ikind), intent(in    ) :: ng
    real   (Rkind), intent(   out) :: gpt(2,ng), wgpt(ng)
    type   (err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Quadrature de Radau et de Gauss-Radau sur le domaine triangulaire
!                     0 <= x <= 1,  0 <= y <= 1-x
!
!     - en entree :
!         ng : nbr de points d'integration
!
!     - en sortie :
!         gpt  : abscisses des ng points de gauss
!         wgpt : poids associes 
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------   
    character(len=*), parameter :: Here = 'quadrature_triangle' 
    real     (Rkind)            :: a01, w01, a31, a32, w31, a41, a42, a43, w41, w42, a71, &
                                   a72, a73, a74, a75, w71, w72, w73, a91, a92, a93, a94, &
                                   a95, a96, a97, a98, a99, w91, w92, w93, w94, w95, w96                      
!---------------------------------------------------------------------------------------------  

    if ( stat > IZERO ) return 

    select case (ng)
    
        case ( 1 ) ! formule a 1 point (ordre 1) :
        
           a01 = 0.333333333333333_Rkind
           
           w01 = 0.5_Rkind
           
           gpt(1:2,1) = [ a01, a01 ];  wgpt(1) = w01
           
        case ( 3 ) ! formule a 3 points (ordre 2) :   
        
           a31 = 0.5_Rkind; a32 = 0.0_Rkind 
           
           w31 = 0.166666666666666_Rkind
           
           gpt(1:2,1) = [ a31, a32 ];  wgpt(1) = w31
           gpt(1:2,2) = [ a31, a31 ];  wgpt(2) = w31
           gpt(1:2,3) = [ a32 ,a31 ];  wgpt(3) = w31

        case ( 4 ) ! formule a 4 points (ordre 3) :
           
           a41 = 0.333333333333333_Rkind; a42 = 0.2_Rkind; a43 = 0.6_Rkind
           
           w41 =-0.281250000000000_Rkind; w42 = 0.260416666666667_Rkind
           
           gpt(1:2,1) = [ a41, a41 ]; wgpt(1) = w41
           gpt(1:2,2) = [ a42, a42 ]; wgpt(2) = w42
           gpt(1:2,3) = [ a43, a42 ]; wgpt(3) = w42
           gpt(1:2,4) = [ a42, a43 ]; wgpt(4) = w42
           
        case ( 7 ) ! formule a 7 points :
        
           a71 = 0.05971587_Rkind; a72 = 0.10128651_Rkind
           a73 = 0.47014206_Rkind; a74 = 0.79742699_Rkind
           a75 = 0.333333333333333_Rkind
                      
           w71 = 0.06296959_Rkind; w72 = 0.06619708_Rkind
           w73 = 0.11250000_Rkind           
           
           gpt(1:2,1) = [ a72, a72 ]; wgpt(1) = w71
           gpt(1:2,2) = [ a73, a71 ]; wgpt(2) = w72
           gpt(1:2,3) = [ a74, a72 ]; wgpt(3) = w71
           gpt(1:2,4) = [ a73, a73 ]; wgpt(4) = w72
           gpt(1:2,5) = [ a72, a74 ]; wgpt(5) = w71
           gpt(1:2,6) = [ a71, a73 ]; wgpt(6) = w72
           gpt(1:2,7) = [ a75, a75 ]; wgpt(7) = w73

        case ( 9 ) ! formule a 9 points :

           a91 = 0.023931132290_Rkind; a92 = 0.06655406786_Rkind
           a93 = 0.102717654830_Rkind; a94 = 0.10617026910_Rkind
           a95 = 0.188409405913_Rkind; a96 = 0.29526656780_Rkind 
           a97 = 0.455706020250_Rkind; a98 = 0.52397906774_Rkind
           a99 = 0.808694385670_Rkind
                      
           w91 = 0.019396383304_Rkind; w92 = 0.031034213285_Rkind                 
           w93 = 0.055814420490_Rkind; w94 = 0.063678085097_Rkind                
           w95 = 0.089303072783_Rkind; w96 = 0.101884936154_Rkind                  
              
           gpt(1:2,1) = [ a95, a91 ]; wgpt(1) = w91
           gpt(1:2,2) = [ a98, a92 ]; wgpt(2) = w94
           gpt(1:2,3) = [ a99, a93 ]; wgpt(3) = w93
           gpt(1:2,4) = [ a94, a94 ]; wgpt(4) = w92
           gpt(1:2,5) = [ a96, a96 ]; wgpt(5) = w96
           gpt(1:2,6) = [ a97, a97 ]; wgpt(6) = w95
           gpt(1:2,7) = [ a91, a95 ]; wgpt(7) = w91
           gpt(1:2,8) = [ a92, a98 ]; wgpt(8) = w94
           gpt(1:2,9) = [ a93, a99 ]; wgpt(9) = w93

        case default

         stat = err_t( stat = UERROR, where = Here, msg = i2a(ng) // &
                      '-points quadrature method not available for triangle element')
         return
 
    end select   
    
    END SUBROUTINE quadrature_triangle   
                            
                                                   
!=============================================================================================
    SUBROUTINE quadrature_tetraedre ( gpt, wgpt, ng, stat )
!=============================================================================================
    integer(Ikind), intent(in    ) :: ng
    real   (Rkind), intent(   out) :: gpt(3,ng), wgpt(ng)
    type   (err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------  
!   Quadrature a 1,4 ou 5 points sur le domaine tetraedrique 
!                   0 <= x <= 1,  0 <= y <= 1-x,  0 <= z <= 1 - x - y
!
!     - en entree :
!         ng : nbr de points d'integration
!
!     - en sortie :
!         gpt  : abscisses des ng points de gauss
!         wgpt : poids associes
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    character(len=*), parameter :: Here = 'quadrature_tetraedre' 
    real      (Rkind)           :: a01, w01, a41, a42, w41, a51, a52, a53, W51, w52                      
!---------------------------------------------------------------------------------------------  

    if ( stat > IZERO ) return 

    select case (ng)
    
        case ( 1 ) ! formule d'ordre 1 (1 point) :
        
           a01 = 0.2500000000000000_Rkind
           
           w01 = 0.1666666666666667_Rkind
           
           gpt(1:3,1) = [ a01, a01, a01 ];  wgpt(1) = w01
           
        case ( 4 ) ! formule d'ordre 2 (4 points) :   

           a41 = 0.1381966011250105_Rkind; a42 = 0.5854101966249685_Rkind
           
           w41 = 0.041666666666666664_Rkind

           gpt(1:3,1) = [ a41, a41, a41 ];  wgpt(1) = w41
           gpt(1:3,2) = [ a41, a41, a42 ];  wgpt(2) = w41
           gpt(1:3,3) = [ a41, a42, a41 ];  wgpt(3) = w41
           gpt(1:3,4) = [ a42, a41, a41 ];  wgpt(4) = w41
           
        case ( 5 ) ! formule d'ordre 3 (5 points) :   

           a51 = 0.2500000000000000_Rkind; a52 = 0.1666666666666667_Rkind; a53 = 0.5_Rkind
           
           w51 = 0.1333333333333333_Rkind; w52 = 0.075_Rkind
           
           gpt(1:3,1) = [ a51, a51, a51 ];  wgpt(1) =-w51
           gpt(1:3,2) = [ a52, a52, a52 ];  wgpt(2) = w52
           gpt(1:3,3) = [ a52, a52, a53 ];  wgpt(3) = w52
           gpt(1:3,4) = [ a52, a53, a52 ];  wgpt(4) = w52
           gpt(1:3,5) = [ a53, a52, a52 ];  wgpt(5) = w52

        case default

           stat = err_t( stat = UERROR, where = Here, msg = i2a(ng) // &
                        '-points quadrature method not available for tetrahedron')
           return        

    end select   

    END SUBROUTINE quadrature_tetraedre


!=============================================================================================
    SUBROUTINE quadrature_error (uerr, num, msg, nmsg, where)
!=============================================================================================
    integer  (Ikind), intent(in), optional :: num, uerr, nmsg
    character(len=*), intent(in), optional :: msg(*), where
!---------------------------------------------------------------------------------------------    
!   Affichage d'un message d'erreur et arret du programme
!--------------------------------------------------------------------------------------------- 

!-- local variables: ------------------------------------------------------------------------- 
    integer  (Ikind ) :: ue, i      
!---------------------------------------------------------------------------------------------            

    if (present(uerr)) then 
      ue = uerr
    else 
      ue = 6
    end if    
       
    write(ue,'(/,a)')'Error (quadrature_error):'

    if ( present(where) ) write(ue,'(a)'   )'--> in quadrature_'//trim(adjustl(where))
        
    if ( present(num)   ) write(ue,'(a,i0)')'--> error  #',num
        
    if (present(msg)) then
       if (present(nmsg)) then
          do i = 1, nmsg
             if (len_trim(msg(i)) > 0) write(ue,'(a)')'--> '//trim(msg(i))
          end do
       else   
          write(ue,'(a)')'--> '//trim(msg(1))
       end if   
    end if    
    
    write(ue,'(a,/)')'--> STOP'
            
    stop

    END SUBROUTINE quadrature_error
    
END MODULE quadrature_m
