#include "error.fpp"

MODULE solverdriver_m
!
! The drivers for the few linear solvers used by camef
!
!-----------------------------------------------------------------------------------R.H. 12/16  

    use kindParameters_m
    use err_m
    use defTypes_m
    
    use constants_m , only: IERROR, UERROR, IZERO, NLT
    use util_m      , only: util_string_low
    
    implicit none
    
    interface solverdriver_mklPardisoGeneric
       module procedure solverdriver_mklPardisoI32
       module procedure solverdriver_mklPardisoI64
    end interface
    	
CONTAINS
	
!=============================================================================================
   SUBROUTINE solverdriver_resol ( femsys, stat, factorize, solve, clean )
!=============================================================================================
    use param_m
    type     (linsys_t), intent(in out)           :: femsys 
    type     (err_t   ), intent(in out)           :: stat
    character(len=*   ), intent(in    ), optional :: factorize, solve, clean
!---------------------------------------------------------------------------------------------	
!   Solves the linear system Mat * X = Rhs (where Mat, X, Rhs are femsys fields) by
!   calling the solver selected by the user
!
!   if factorize == 'yes' ==> Initialization & Factorization 
!   if solve     == 'yes' ==> Solve
!   if clean     == 'yes' ==> Release memory
!
!   if the 3 options are not present ==> Initialization & Factorization & Solve & Release mem.
!---------------------------------------------------------------------------------------------	
	
!--locals:------------------------------------------------------------------------------------
    character(len=*), parameter   :: Here = 'solverdriver_resol'
    character(len=:), allocatable :: solverlib
    logical                       :: fac, sol, del
!---------------------------------------------------------------------------------------------

    if ( stat > IZERO ) return 

    solverlib = util_string_low ( adjustl(name_linsolver) ) ! name of the solver

    if ( solverlib(1:8) == 'sparskit' ) solverlib = 'sparskit'
    
    if ( .not. present(factorize) .and. .not. present(solve) .and. .not. present(clean) ) then
       fac = .true. ; sol = .true. ; del = .true.
    else
       fac = .false.; sol = .false.; del = .false.
       if ( present(factorize) ) then
          if ( trim(adjustl(factorize) ) == 'yes') fac = .true.
       end if
       if ( present(solve) ) then
          if ( trim(adjustl(solve)) == 'yes' ) sol = .true.
       end if
       if ( present(clean) ) then
          if ( trim(adjustl(clean)) == 'yes' ) del = .true.
       end if
    end if
             
    select case ( trim(solverlib) )
    
       case ( "pardiso" )

          !call solverdriver_pardiso ( femsys, mydata )

          stat = err_t ( stat = UERROR, where = Here, msg = 'sorry, pardiso is not yet ' // &
                                                     'implemented (please use mkl(pardiso)' ) 
          return          
       
       case ( "mkl(pardiso)" )
       
          call solverdriver_mklpardiso ( femsys, fac, sol, del, stat )
       
       case ( "mkl(fgmres)" )

          call solverdriver_mklfgmres  ( femsys, fac, sol, del, stat )
       
       case ( "sparskit" )
   
          call solverdriver_sparskit   ( femsys, fac, sol, del, stat )
          
       case ( "superlu" )
   
          call solverdriver_superlu    ( femsys, fac, sol, del, stat )
   
       case ( "geodynldu" )

          call solverdriver_geodynldu  ( femsys, fac, sol, stat )   
   
       case default

          stat = err_t ( stat = UERROR, where = Here, msg = 'Unknown solver "' // &
                                                             trim(name_linsolver) ) 
          return          
               
    end select   
    
    error_TraceNreturn(stat>IZERO, Here, stat)

    END SUBROUTINE solverdriver_resol
             
    
!=============================================================================================
    SUBROUTINE solverdriver_superlu ( femsys, fac, sol, del, stat )
!=============================================================================================
    use param_m   , only : verbosity
    use sparskit_m, only : amux
    type   (linsys_t), intent(in out) :: femsys 
    logical          , intent(in    ) :: fac, sol, del
    type   (err_t   ), intent(in out) :: stat        
!---------------------------------------------------------------------------------------------		
!   Driver for calling the direct solver SuperLU (with CSR storage format)
!
!   fac = .true. ==> Initialization & Factorization
!   sol = .true. ==> Solve
!   del = .true. ==> Release memory
!---------------------------------------------------------------------------------------------	
	
!--locals:------------------------------------------------------------------------------------
    character(len=*), parameter   :: Here = 'solverdriver_superlu'
    integer  (Ikind)              :: nrhs, info, verb, iopt
    real     (Rkind), allocatable :: resid(:)
    real     (Rkind)              :: normres, normrhs
!--local initialized at the first call (fac) saved for other phases (sol or del): ------------
    integer  ( i64 ), save        :: factors
    logical         , save        :: is_cleaned = .true.
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 

    verb = verbosity(3)
    
    if ( Rkind /= rDP )  then
       stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'SuperLu works only with double precision' )
       return
    end if
    
    !if (Ikind /= i32)  &
    !call solverdriver_error (where = here, msg='Superlu works with int32')     
       
    info = 0; nrhs = 1
             
    if ( fac ) then
!
!--    Factorize the matrix. The factors are stored in *factors* handle. 
!   
       if ( verb >= 1 ) then
          print*
          print*,'SuperLU: LU factorization (be patient!)'
       end if  
       factors = 0 
       iopt = 1
       call c_fortran_dgssv ( iopt      , femsys%n  , femsys%nzero, nrhs      ,  & 
                              femsys%Mat, femsys%Col, femsys%Row  , femsys%X  ,  &
                              femsys%n  , factors   , info        , verb      )

       if ( info == 0 ) then
          if ( verb >= 1 ) print*, '> SuperLU: Factorization succeeded'
       else
          stat = err_t ( stat = UERROR, where = Here, msg = &
                         'Problem with SuperLU during factorization phase' )
          return
       end if
       is_cleaned = .false.
    end if
  
    if ( sol ) then
!
!--    Solve the system using the existing factors.
!  
       femsys%X = femsys%Rhs
    
       if ( verb >= 1 ) print*,'SuperLU: Resolution'      
       iopt = 2
       call c_fortran_dgssv ( iopt      , femsys%n  , femsys%nzero, nrhs      ,  & 
                              femsys%Mat, femsys%Col, femsys%Row  , femsys%X  ,  &
                              femsys%n  , factors   , info        , verb      )
       if (info == 0) then
          if ( verb >= 1 ) print*, '> SuperLU: Solve succeeded'
          if ( verb >= 1 ) then
!
!--          print the residual norm
!
             allocate(resid(femsys%n), stat = info)
             
             if ( info /= 0 ) then
                stat = err_t ( stat = IERROR, where = Here, &
                               msg = 'Allocation failure for "resid"' )   
                return
             end if     
             
             call amux(femsys%n,femsys%X,resid,femsys%Mat,femsys%Col,femsys%Row)
    
             resid = femsys%Rhs - resid
    
             normres = sqrt(dot_product(resid,resid)) 
             normrhs = sqrt(dot_product(femsys%rhs,femsys%rhs)) 
             print*
             print*,'- residual norm      = ||A*x - b||       =',normres
             print*,'- relative res. norm = ||A*x - b||/||b|| =',normres / normrhs 
             print*
          
             print*,'min max :',minval(femsys%x), maxval(femsys%x)
          end if
       else
          stat = err_t ( stat = UERROR, where = Here, &
                          msg = 'Problem with SuperLU during during resolution phase' )
          return      
       end if

       is_cleaned = .false.       
    end if
    
    if ( del .and. .not. is_cleaned ) then
!
!--    Free the storage allocated inside SuperLU
!
       if ( verb >= 1 ) print*,'SuperLU: Release internal memory '   
       iopt = 3       
       call c_fortran_dgssv ( iopt      , femsys%n  , femsys%nzero, nrhs      ,  & 
                              femsys%Mat, femsys%Col, femsys%Row  , femsys%X  ,  &
                              femsys%n  , factors   , info        , verb      )
    
       if ( info == 0 ) then
          if ( verb >= 1 ) then
             print*, '> SuperLU: Terminated normaly'
             print*
          end if
       else
          stat = err_t ( stat = UERROR, where = Here, &
                          msg = 'Problem with SuperLU during during dealloction phase' )
          return        
       end if
       
       is_cleaned = .true.
    end if

    END SUBROUTINE solverdriver_superlu           
      
      
!=============================================================================================
    SUBROUTINE solverdriver_sparskit ( femsys, fac, sol, del, stat )
!=============================================================================================
    use param_m   , only : verbosity, name_linsolver, optlinsol    
    use sparskit_m, only : cg, cgnr, bcg, dbcg, tfqmr, fom, gmres, fgmres, bcgstab, dqgmres, &
                           amux, sparskit_runrc, sparskit_ilut
    type   (linsys_t), intent(in out) :: femsys 
    logical          , intent(in    ) :: fac, sol, del
    type   (err_t   ), intent(in out) :: stat            
!---------------------------------------------------------------------------------------------		
!   Driver for calling iterative methods from the sparkit toolkit (with CSR storage format)
!
!   fac = .true. ==> Initialization & Factorization
!   sol = .true. ==> Solve
!   del = .true. ==> Release memory
!
!   Parameters are set in optlinsol (user-defined or default values) as follows (see input):
!
!    - optlinsol(1):  max. nbr of iterations computed as max(optlinsol(1),n) (default: 150)
!    - optlinsol(2):  relative tolerance (default: 1e-5)
!    - optlinsol(3):  absolute tolerance (default: 1e-10)
!    - optlinsol(4):  subspace dimension for GMRES-like methods (default: 50)
!    - optlinsol(5):  fill-in parameter (default: 15)
!    - optlinsol(6):  tolerance for ILUT (default: 1e-8)
!---------------------------------------------------------------------------------------------	
	
!-- locals: ----------------------------------------------------------------------------------
    character(len=* ), parameter   :: Here = 'solverdriver_sparskit'
    integer  (Ikind )              :: pos1, pos2, err, n, nnz, nwi, nwu, nws, lfil, kryl, &
                                      maxit, verb
    real     (Rkind )              :: tol, normres, normrhs
    character(len=: ), allocatable :: msg
!--locals initialized at the first call (fac) saved for other phases (sol or del): -----------
    character(len=80), save              :: meth
    integer  (Ikind ), save              :: ipar(128)
    real     (Rkind ), save              :: fpar(128)
    integer  (Ikind ), save, allocatable :: jwrku(:), jwrkv(:), iwork(:)        
    real     (Rkind ), save, allocatable :: awrku(:), works(:)
    logical          , save              :: is_cleaned = .true.
!---------------------------------------------------------------------------------------------

    if ( stat > IZERO ) return 
      
    verb = verbosity(3)
        
    if ( fac ) then
!
!--    Initialization & ILUT factorization phase:    
    
       pos1 = index(name_linsolver,"(")+1 ; pos2 = index(name_linsolver,")")-1
!    
!--    the method choosen by the user: 
!
       meth =''; meth(1:pos2-pos1+1) = util_string_low ( name_linsolver(pos1:pos2) ) 
    
       lfil = nint(optlinsol(5)) ! fill-in parameter
       kryl = nint(optlinsol(4)) ! size of the Krylov subspace (pour gmres et variantes)

       n = femsys % n; nnz = femsys % nzero
!
!--    sizes of the arrays 
!    
       nwi = 3*n; nwu = nnz + 2*lfil*n         
         
       if      ( trim(meth)=='cg'.or. trim(meth)=='cgnr'    ) then   
          nws = 5*n 
       else if ( trim(meth)=='bcg'                          ) then
          nws = 7*n
       else if ( trim(meth)=='dbcg'.or. trim(meth)=='tfqmr' ) then
          nws = 11*n
       else if ( trim(meth)=='fom'.or. trim(meth)=='gmres'  ) then
          nws = (n+3)*(kryl+2) + (kryl+1)*kryl / 2
       else if ( trim(meth)=='fgmres'                       ) then
          nws = 2*n*(kryl+1) + (kryl+1)*kryl/2 + 3*kryl + 2     
       else if ( trim(meth)=='bcgstab'                      ) then
          nws = 8*n
       else if ( trim(meth)=='dqgmres'                      ) then
          nws = n + (kryl+1)*(2*n+4)
       else
          stat = err_t ( stat = UERROR, where = Here, &
                          msg = 'Unknown method "'//trim(meth)//'"') 
          return
       end if      
     
       nws = max(nws,2*n)

       allocate (awrku(nwu), jwrku(nwu), iwork(nwi), works(nws), jwrkv(n), stat=err)

       if ( err /= 0 ) then 
          stat = err_t ( stat = UERROR, where = Here, &
                          msg = 'Allocation failure for work arrays')
          return
       end if
                      
!
!--    set-up the preconditioner ILUT(15, 1E-4) ! new definition of lfil
!
      ! tol = 1.0e-4_Rkind ! this is too high for ilut for saylr1
      ! tol = 1.0e-8_Rkind
       tol = optlinsol(6)
      
       if ( verb >= 1 ) print*,'sparskit: ILUT factorization)'
        
       call sparskit_ilut (femsys%n, femsys%Mat, femsys%Col, femsys%Row, &
                           lfil    , tol       , awrku     , jwrku     , &
                           jwrkv   , nwu       , works     , iwork     , &
                           err     , verb                              )       
     
       if ( err /= 0 ) then
          if ( err >   0 ) msg = 'Zero pivot encountered'
          if ( err == -1 ) msg = 'Input matrix wrong'
          if ( err == -2 ) msg = 'Matrix L overflows array al'
          if ( err == -3 ) msg = 'Matrix U overflows array alu'
          if ( err == -4 ) msg =' Illegal value for lfil'
          if ( err == -5 ) msg = 'Zero row encountered'
          stat = err_t ( stat=UERROR, where=Here, msg='ILUT Factorization failed'// NLT // &
                         'Message returned by sparskit: '//msg )
          deallocate (awrku, jwrku, iwork, works, jwrkv)
          is_cleaned = .true.
          return
       end if
!
!--    set the parameters for the iterative solvers
!
       maxit = nint(optlinsol(1))
       
       ipar(:) = 0; fpar(:) = 0.0_Rkind

       ipar(1) = 0             ! reverse communication return code
       ipar(2) = 2             ! 0 = no preconditioning; 1 = left; 2 = right.
       ipar(3) = 1             ! 1 = residual-norm based convergence tests.
       ipar(4) = nws           ! size of workspace
       ipar(5) = kryl          ! subspace dimension for GMRES-like methods
       !!ipar(6) = max(maxit,n)  ! maximum number of iterations
       ipar(6) = maxit  ! maximum number of iterations
       fpar(1) = optlinsol(2)  ! relative tolerance
       fpar(2) = optlinsol(3)  ! absolute tolerance
    
      is_cleaned = .false.
    end if ! end initialization & ILUT factorization phase
    
    if ( sol ) then
    
!       femsys%X = 0.0_Rkind   ! initial guess
!
!--    Call the choosen iterative method:
! 
       if ( trim(meth) == 'fgmres' ) then
          if ( verb > 0 ) write(*,'(/,"*** FGMRES ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                fgmres    , verb      , stat                             )
                   
       else if ( trim(meth) == 'bcg' ) then
          if ( verb > 0 ) write(*,'(/,"*** BCG ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                bcg       , verb      , stat                             )

       else if ( trim(meth) == 'dbcg' ) then
          if ( verb > 0 ) write(*,'(/,"*** DBCG ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                dbcg      , verb      , stat                             )

       else if ( trim(meth) == 'cgnr' ) then
          if ( verb > 0 ) write(*,'(/,"*** CGNR ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                cgnr      , verb      , stat                             )

       else if ( trim(meth) == 'bcgstab' ) then
          if ( verb > 0)  write(*,'(/,"*** BCGSTAB ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                bcgstab   , verb      , stat                             )      
                   
       else if (trim(meth) == 'tfqmr') then
          if (verb > 0) write(*,'(/,"*** TFQMR ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                tfqmr     , verb      , stat                             )                            

       else if ( trim(meth) == 'fom' ) then
          if ( verb > 0 ) write(*,'(/,"*** FOM ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                fom       , verb      , stat                             )   
                   
       else if ( trim(meth) == 'gmres' ) then
          if ( verb > 0 ) write(*,'(/,"*** GMRES ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                gmres     , verb      , stat                             )   

       else if ( trim(meth) == 'dqgmres' ) then
          if (verb > 0) write(*,'(/,"*** DQGMRES ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                dqgmres   , verb      , stat                             )   

       else if ( trim(meth) == 'cg' ) then
          if ( verb > 0 ) write(*,'(/,"*** CG ***")')
          call sparskit_runrc ( femsys%n  , femsys%Rhs, femsys%X  , ipar , fpar , works, &
                                femsys%Mat, femsys%Col, femsys%Row, awrku, jwrku, jwrkv, &
                                cg        , verb      , stat                             )   
                 
       else
          stat = err_t ( stat=UERROR, where=Here, msg='Unknown method "'//trim(meth)//'"' )
          return
       end if  
       
       error_TraceNreturn(stat>IZERO, Here, stat)
       
       if ( verb > 0 ) then
          print*,'min max :',minval(femsys%x), maxval(femsys%x)      
!
!--       check and print the error
!
          call amux(femsys%n,femsys%X,works,femsys%Mat,femsys%Col,femsys%Row)
    
          works(1:femsys%n) = femsys%Rhs(1:femsys%n) - works(1:femsys%n)
    
          normres = sqrt(dot_product(works(1:femsys%n),works(1:femsys%n))) 
          normrhs = sqrt(dot_product(femsys%rhs,femsys%rhs)) 
          print*
          print*,'- residual norm      = ||A*x - b||       =',normres
          print*,'- relative res. norm = ||A*x - b||/||b|| =',normres / normrhs 
          print*
       end if
       is_cleaned = .false.
       
    end if  ! end solve phase
    
    if ( del .and. .not. is_cleaned ) then
!
!--    deallocate local arrays
!
       if ( verb >= 1 ) print*,'sparskit: Release internal memory'
       
       deallocate(awrku, jwrku, iwork, works, jwrkv)
       is_cleaned = .true.
    
    end if
    
    END SUBROUTINE solverdriver_sparskit


!=============================================================================================
    SUBROUTINE solverdriver_geodynldu ( femsys, fac, sol, stat )
!=============================================================================================
    use geodynldu_m
    use param_m, only : verbosity
    type   (linsys_t), intent(in out) :: femsys
    logical          , intent(in    ) :: fac, sol
    type   (err_t   ), intent(in out) :: stat    
!---------------------------------------------------------------------------------------------	
!   Driver for calling the direct solver geodyn (with skyline storage format)
!
!   fac = .true. ==> Initialization & Factorization
!   sol = .true. ==> Solve
!---------------------------------------------------------------------------------------------

!--locals:------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------    
    
    if ( stat > IZERO ) return 
!
!-- LDU factorization:
!	   
    if ( fac ) then
    
       if ( verbosity(3) >= 1 ) print*,'geodynLDU: LDU factorization'

       call geodynldu_facLDU ( femsys%Mat, femsys%Row, femsys%nzero, &
                               femsys%n  , verbosity(3)              )
    end if
!
!-- Forward and backsubstitution pass:
!    
    if ( sol ) then
  
       if ( verbosity(3) >= 1 ) print*,'geodynLDU: Resolution'

       call geodynldu_solLDU ( femsys%Mat, femsys%Rhs, femsys%Row,  &
                               femsys%X  , femsys%n  , femsys%nzero )
       
       if ( verbosity(3) >= 1 ) print*,'min max :',minval(femsys%x), maxval(femsys%x)
       
    end if
    
    END SUBROUTINE solverdriver_geodynldu


!=============================================================================================
    SUBROUTINE solverdriver_mklpardiso ( femsys, fac, sol, del, stat )
!=============================================================================================
    use param_m   , only : verbosity, name_storage
    use sparskit_m, only : amux
    use util_m    , only : util_prodCsrSymMv
    type   (linsys_t), intent(in out) :: femsys  
    logical          , intent(in    ) :: fac, sol, del
    type   (err_t   ), intent(in out) :: stat        
!---------------------------------------------------------------------------------------------	
!   Driver for calling the direct MKL solver pardiso (with CSR or CSR-sym storage format)	
!
!   fac = .true. ==> Initialization & Factorization
!   sol = .true. ==> Solve
!   del = .true. ==> Release memory
!---------------------------------------------------------------------------------------------	
	
!--locals:------------------------------------------------------------------------------------
    character(len=*), parameter   :: Here = 'solverdriver_mklpardiso'
    integer  (Ikind)              :: mtype, nrhs, msglvl, phs, idum(1), err, verb, n
    real     (Rkind)              :: normres, normrhs, xdum(1)
!--locals initialized at the first call (fac) saved for other phases (sol or del): -----------
    integer  ( i64 ), save              :: pt(64)
    integer  (Ikind), save              :: iparm(64), maxfct, mnum
    integer  (Ikind), save, allocatable :: iperm(:)  
    real     (Rkind), save, allocatable :: resid(:)     
    logical         , save              :: is_cleaned = .true.    
!--------------------------------------------------------------------------------------------- 
    
    if ( stat > IZERO ) return 
    
#ifdef noMKL
    stat = err_t ( stat = UERROR, where = Here, &
                   msg = "unable to use MKL (you compiled this code with mkl='no')" )
    return
#else
    
    n = femsys%n ; verb = verbosity(3) 
!
!-- Defines the matrix type, which influences the pivoting method:
!      
    mtype = 11
    select case ( trim(name_storage) )
       case ( 'csrsym' )
          mtype = -2  ! = -2 : real and symmetric indefinite (CSR-sym format)
       case ( 'csr' )
          mtype = 11  ! = 11 : real and nonsymmetric (CSR format)
       case default
          stat = err_t ( stat = UERROR, where = Here, &
                   msg = 'Unknown storage format:'//trim(name_storage) )
          return
    end select
           
    msglvl = min(verb,1_Ikind) ! Message level information. 
                               ! = 0 : then pardiso generates no output,
                               ! = 1 : the solver prints statistical information to the screen.
    
    nrhs = 1 ! Number of right-hand sides that need to be solved for.        
              
    if ( fac ) then
!
!--    Initialization and Factorization phase:
!    
       call pardisoinit ( pt, mtype, iparm ) ! (only necessary for the first call of pardiso)
!
!--    use mkl_dcsradd routine to sort the elements of a CSR matrix in increasing order of their
!      column indices within each row (alternativly we can also use the sparskit csort routine)
!    
!       trans   = 'n'; request = 0; sort = 1; beta    = 0.0e0_Rkind

!       call mkl_dcsradd (trans     , request   , sort      , femsys%n    , femsys%n,      &
!                         femsys%Mat, femsys%Col, femsys%Row, beta        ,                &
!                         femsys%Mat, femsys%Col, femsys%Row,                              &  
!                         femsys%Mat, femsys%Col, femsys%Row, femsys%nzero, err     )
     
!       if (err /= 0) call solverdriver_error &
!       (msg='in solverdriver_mklpardiso (Pb while excuting mkl_dcsradd)',num=err)
    
!
!--    Default control parameters (iparm) may be modified (see list at the end of the module)
!
       iparm(4) = 60  ! Preconditioned CGS/CG. This parameter controls preconditioned CGS for
                      ! nonsymmetric or structurally symmetric matrices and CG for sym. matrices.
                      ! iparm(4) has the form iparm(4)= 10*L+K.
                      ! K = 0 : The factorization is always computed as required by "iphase".
                      ! K = 1 : CGS iteration replaces the computation of LU.
                      ! K = 2 : CGS iteration for sym.pos.def. matrices replaces the computation of LLT. 
                      ! The value L controls the stopping criterion of the Krylow-Subspace iteration 
	                 
       iparm(5) = 2   ! User permutation. This parameter controls whether user supplied fill-in
                      ! reducing permutation is used instead of the integrated multiple-minimum degree 
                      ! or nested dissection algorithms. Another use of this parameter is to control
                      ! obtaining the fill-in reducing permutation vector calculated during the reordering
                      ! stage of Intel MKL PARDISO.
                      ! = 0 : (default) User permutation in the perm array is ignored.
                      ! = 1 : Intel MKL PARDISO uses the user supplied fill-in reducing permutation
                      !       from the iperm array. iparm(2) is ignored.
                      ! = 2 : Intel MKL PARDISO returns the permutation vector computed at phase 1
                      !       in the iperm array.	
                                   
       iparm(8)  = 2  ! Iterative refinement step.
                      ! = 0 : The solver automatically performs two steps of iterative 
                      !       refinement when perturbed pivots are obtained during the 
                      !       numerical factorization.
                      ! > 0 : Maximum numbers of iterative refinement steps
                   
       iparm(27) = 1  ! Matrix checker.
                      ! = 0 : do not check the sparse matrix representation for errors.
                      ! = 1 : check integer arrays ia and ja. In particular, 
                      !       Intel MKL PARDISO checks whether column indices are sorted
                      !       in increasing order within each row.
    
       iparm(28) = 0  ! Single or double precision Intel MKL PARDISO.
                      ! = 0 : Input arrays (a, x and b) and all internal arrays must be presented
                      !       in double precision.
                      ! = 1 : input arrays (a, x and b) must be presented in single precision.
       if (Rkind == 4) iparm(28) = 1               

       maxfct = 1     ! Maximum number of factors with identical sparsity structure that must
                      ! be kept in memory at the same time. In most applications this value is
                      ! equal to 1. It is possible to store several different factorizations
                      ! with the same nonzero structure at the same time in the internal data
                      ! structure management of the solver.
                
       mnum = 1       ! Indicates the actual matrix for the solution phase. With this scalar
	                   ! you can define which matrix to factorize. The value must be: 
                      ! 1 ≤ mnum ≤ maxfct. In most applications this value is 1.
    

      !allocate local arrays: 
       err = 0  
       if ( allocated(iperm) ) then
         if ( size(iperm) /= n ) then
            deallocate(iperm) ; allocate (iperm(n),stat=err)  
         end if
       else
          allocate (iperm(n),stat=err)
       end if
       
       if ( err /= 0 ) then
          stat = err_t ( stat = UERROR, where = Here,msg= "Allocation failure for 'iperm'" )
          return
       end if    
       
       if ( allocated(resid) ) then
          if ( size(resid) /= n ) then
             deallocate(resid) ; allocate (resid(n),stat=err)  
          end if
       else
          allocate (resid(n),stat=err)
       end if

       if ( err /= 0 ) then
          stat = err_t ( stat = UERROR, where = Here, msg= "Allocation failure for 'resid'" )
          return
       end if    
                       
       if ( verb >= 1 ) print*,'pardiso: Factorization phase'

       phs = 12  ! = 12 : Analysis & numerical factorization 
       
       call solverdriver_mklPardisoGeneric                                    &
                    ( pt        , maxfct    , mnum      , mtype, phs , n    , &
                      femsys%Mat, femsys%Row, femsys%Col, iperm, nrhs, iparm, & 
                      msglvl    , femsys%Rhs, femsys%X  , err                 )

       if ( err /= 0 )  then
          stat = err_t ( stat = UERROR, where = Here, msg = 'Factorization phase failed' )
          return
       end if
	
	    is_cleaned = .false.
    end if  ! end factorization phase
		
    if ( sol ) then
!
!--    Solve phase:
!	
       if (verb >= 1) print*,'pardiso: Resolution phase'
       
       phs = 33 ! = 33 : Solve & iterative refinement
       
       femsys%X = 0.0e0_Rkind   

       call solverdriver_mklPardisoGeneric                                    &
                    ( pt        , maxfct    , mnum      , mtype, phs , n    , &
                      femsys%Mat, femsys%Row, femsys%Col, iperm, nrhs, iparm, & 
                      msglvl    , femsys%Rhs, femsys%X  , err                 )

       if ( err /= 0 )  then
          stat = err_t ( stat = UERROR, where = Here, msg = 'Solve phase failed' )
          return
       end if
       
       if ( verb >= 1 ) then
!
!--       compute and print the residual
!          
          print*,'min max :',minval(femsys%x), maxval(femsys%x)    

          !CALL MKL_DCSRsymV ('U',n,femsys%Mat,femsys%Row,femsys%Col,femsys%X,resid)
          if ( mtype == 11 ) then
             call amux (n, femsys%X, resid, femsys%Mat,femsys%Col,femsys%Row)
          else
             call util_prodCsrSymMv (n, femsys%Mat, femsys%Row, femsys%Col, femsys%X, resid)
          end if
          
          resid = femsys%Rhs - resid
    
          normres = sqrt(dot_product(resid,resid)) 
          normrhs = sqrt(dot_product(femsys%rhs,femsys%rhs)) 
          print*
          print*,'- residual norm      = ||A*x - b||       =',normres
          print*,'- relative res. norm = ||A*x - b||/||b|| =',normres / normrhs 
          print*
          print*
          print*,'Number of iterative refinement steps performed :',iparm(7)   
          print*       

       end if  
	    is_cleaned = .false.
    end if     ! end solve phase

    if ( del .and. .not. is_cleaned ) then
!
!--    Release of memory phase:
!
       phs =-1
       if ( verb >=1 ) print*,'MKL: Release internal memory'
       if ( allocated(resid) ) deallocate(resid)
       call solverdriver_mklPardisoGeneric                                    &       
                    ( pt        , maxfct    , mnum      , mtype, phs , n    , &
                      xdum      , idum      , idum      , iperm, nrhs, iparm, & 
                      msglvl    , xdum      , xdum      , err                 )
                      
       if ( err /= 0 )  then
          stat = err_t ( stat = UERROR, where = Here, msg = 'Cleaning phase failed' )
          return
       end if
       
       is_cleaned = .true.
    end if

#endif
    
    END SUBROUTINE solverdriver_mklpardiso
                  
                  			      
!=============================================================================================
    SUBROUTINE solverdriver_mklfgmres ( femsys, fac, sol, del, stat )
!=============================================================================================
    use param_m, only : verbosity, optlinsol    
    type   (linsys_t), intent(in out) :: femsys
    logical          , intent(in    ) :: fac, sol, del
    type   (err_t   ), intent(in out) :: stat        
!---------------------------------------------------------------------------------------------	
!   Driver for calling the MKL fgmres solver (source: intel mkl/examples/solverf/source)
!
!   fac = .true. ==> Initialization & Factorization
!   sol = .true. ==> Solve
!   del = .true. ==> Release memory
!---------------------------------------------------------------------------------------------	
	
!--locals:------------------------------------------------------------------------------------ 
    character(len=* ), parameter :: Here = 'solverdriver_mklfgmres'
    integer  (Ikind )            :: lfil, nrest, maxits, itercount, err, verb, request
    integer  (Ikind )            :: n, nwu, nwi, nws
    real     (Rkind )            :: tol, dvar, b    
    character(len=80)            :: buf, buf1, buf2
!--locals initialized at the first call (fac) saved for other phases (sol or del): -----------
    integer  (Ikind ), save              :: ipar(128)
    real     (Rkind ), save              :: fpar(128)
    real     (Rkind ), save, allocatable :: awrku(:), works(:), trvec(:), rhs0(:), resid(:)
    integer  (Ikind ), save, allocatable :: jwrku(:), iwork(:)
    logical          , save              :: is_cleaned = .true.
!---------------------------------------------------------------------------------------------

    if ( stat > IZERO ) return 

#ifdef noMKL
    stat = err_t ( stat = UERROR, where = Here, &
                   msg = "unable to use MKL (you compiled this code with mkl='no')" )
    return
#else
        
    verb = verbosity(3); err = 0
    
    if ( fac ) then
!
!--    Initialisation & ILUT factorization phase:
!    
       lfil = nint(optlinsol(5)) ! fill-in parameter
       maxits = min( nint(optlinsol(1)), femsys%n ) 
       tol = optlinsol(2) 
       
       !!lfil = 15;  maxits = min(150_Ikind, femsys%n)
       !!tol = 1.0e-8_Rkind
       
       nrest = 150 
!
!--    use mkl_dcsradd routine to sort the elements of a CSR matrix in increasing order of their
!      column indices within each row (alternativly we can also use the sparskit csort routine)
!
!       trans   = 'n'; request = 0; sort = 1; beta = 0.0e0_Rkind
!    
!       call mkl_dcsradd (trans     , request   , sort      , femsys%n    , femsys%n,      &
!                         femsys%Mat, femsys%Col, femsys%Row, beta        ,                &
!                         femsys%Mat, femsys%Col, femsys%Row,                              &  
!                         femsys%Mat, femsys%Col, femsys%Row, femsys%nzero, err     )
!     
!       if (err /= 0) call solverdriver_error &
!       (msg='in solverdriver_mklfgmres (Pb while excuting mkl_dcsradd)',num=err)
    

       n = femsys%n
       nws = n * (2*nrest+1) + nrest*(nrest+9) / 2 + 1
       nwu = n * (2*lfil +1) - lfil *(lfil +1)     + 1
       nwi = n + 1

       err = 0
       if (err==0 .and. allocated(works)) deallocate(works) ; allocate(works(nws),stat=err)
       if (err==0 .and. allocated(jwrku)) deallocate(jwrku) ; allocate(jwrku(nwu),stat=err) 
       if (err==0 .and. allocated(awrku)) deallocate(awrku) ; allocate(awrku(nwu),stat=err) 
       if (err==0 .and. allocated(rhs0 )) deallocate(rhs0 ) ; allocate(rhs0 (n  ),stat=err)
       if (err==0 .and. allocated(trvec)) deallocate(resid) ; allocate(trvec(n  ),stat=err) 
       if (err==0 .and. allocated(resid)) deallocate(resid) ; allocate(resid(n  ),stat=err) 
       if (err==0 .and. allocated(iwork)) deallocate(iwork) ; allocate(iwork(nwi),stat=err) 

       if ( err /= 0 ) then
          stat = err_t ( stat=UERROR, where=Here, msg='Allocation failure for work arrays' )
          return
       end if                  
!
!--    Save the right-hand side in vector B for future use:
!
       rhs0 = femsys%rhs
!
!--    Initial guess:
!
       !femsys%X(:) = 0.0_Rkind
!
!--    Initialize the solver
!
       call dfgmres_init ( femsys%n, femsys%X, femsys%rhs, request, ipar, fpar, works ) 
    
       if (request /= 0) then 
          deallocate (works,jwrku,awrku,iwork,rhs0,trvec,resid); call mkl_free_buffers
          stat = err_t ( stat=UERROR, where=Here, msg='Initialization phase failed' )
          is_cleaned = .true.
          return
       end if    

!
!--    calculate ilut preconditioner.
!                         !attention!
!      dcsrilut routine uses some ipar, fpar set by dfgmres_init routine.
!      important for dcsrilut default entries set by dfgmres_init are
!        ipar(2) = 6 - output of error messages to the screen,
!        ipar(6) = 1 - allow output of error messages,
!        ipar(31)= 0 - abort dcsrilut calculations if routine meets zero diagonal element.
!        ipar(7) = 1 - output warn messages if any and continue
!
!      if ilut is going to be used out of mkl fgmres context, than the values
!      of ipar(2), ipar(6), ipar(31), and fpar(31), should be user
!      provided before the dcsrilut routine call.
!
!      in this example, specific for dcsrilut entries are set in turn:
!        ipar(31)= 1 - change small diagonal value to that given by fpar(31),
!        fpar(31)= 1.d-5  instead of the default value set by dfgmres_init.
!                         it is the target value of the diagonal value if it is
!                         small as compared to given tolerance multiplied
!                         by the matrix row norm and the routine should
!                         change it rather than abort dcsrilut calculations.
       ipar(7) = 1
       ipar(31) = 1
       fpar(31) = 1.0e-5_Rkind

       if ( verb >= 1 ) print*,'MKL FGMRES: ILUT preconditioner'
       call dcsrilut ( femsys%n, femsys%mat, femsys%row, femsys%col,  & 
                       awrku   , iwork     , jwrku     , tol       ,  &
                       lfil    , ipar      , fpar      , err          )
       if ( err /= 0 ) then
          deallocate (works,jwrku,awrku,iwork,rhs0,trvec,resid); call mkl_free_buffers
          stat = err_t ( stat=UERROR, where=Here, msg='ILUT preconditioner failed' )
          is_cleaned = .true.
          return 
       end if
       if ( verb >= 1 ) print*,'MKL FGMRES: done'
       

!--    set the desired parameters:
!      do the restart after 2 iterations
!      logical parameters:
!      do not do the stopping test for the maximal number of iterations
!      do the preconditioned iterations of fgmres method
!      set parameter ipar(11) for preconditioner call.
!      for this example, it reduces the number of iterations.
!      double precision parameters
!      set the relative tolerance to 1.0d-3 instead of default value 1.0d-6
!      note. preconditioner may increase the number of iterations for an
!      arbitrary case of the system and initial guess and even ruin the
!      convergence. it is user's responsibility to use a suitable preconditioner
!      and to apply it skillfully.

       ipar(15) = nrest
       ipar( 5) = maxits
       ipar( 9) = 0
       ipar(11) = 1
       fpar( 1) = tol
!    
!--    check the correctness and consistency of the newly set parameters:
!
       call dfgmres_check (n, femsys%X, femsys%rhs, request, ipar, fpar, works)
       
       if ( request /= 0 ) then
          deallocate (works,jwrku,awrku,iwork,rhs0,trvec,resid); call mkl_free_buffers
          stat = err_t ( stat=UERROR, where=Here, msg='dfgmres_check returned an error' )
          is_cleaned = .true.
          return
       end if       
    
!
!--    print the info about the rci fgmres method:
!
       if ( verb >= 2 ) then
          write( *,'(a)') ' '
          write( *,'(a)') 'some info about the current run of rci fgmres method:'
          write( *,'(a)') ' '
          if ( ipar(8) /= 0 ) then
             write(*,'(a,i1,a,a)') 'as ipar(8)=',ipar(8),', the automatic',     &
             ' test for the maximal number of iterations will be performed'
          else
             write(*,'(a,i1,a,a)') 'as ipar(8)=',ipar(8),', the automatic',     &
             ' test for the maximal number of iterations will be skipped'
          end if
          write( *,'(a)') '+++'
          if ( ipar(9) /= 0 ) then
             write(*,'(a,i1,a,a)') 'as ipar(9)=',ipar(9),', the automatic',     &
             '  residual test will be performed'
          else
             write(*,'(a,i1,a,a)') 'as ipar(9)=',ipar(9),', the automatic',     &
             ' residual test will be skipped'
          end if
          write( *,'(a)') '+++'
          if ( ipar(10) /= 0 ) then
              write(*,'(a,i1,a,a)') 'as ipar(10)=',ipar(10),', the user-defined ',   &
              ' stopping test will be requested via rci_request=2'
          else
              write(*,'(a,i1,a,a)') 'as ipar(10)=',ipar(10),', the user-defined ',   &
            ' stopping test will not be requested, thus, rci_request will not take the value 2'      
          end if
          write( *,'(a)') '+++'
          if ( ipar(11) /= 0 ) then
             write(*,'(a,i1,a,a)') 'as ipar(11)=',ipar(11),', the preconditioned',   &
             '  fgmres iterations will be performed, thus,'
             write(*,'(a)') 'the preconditioner action will be requested via rci_request=3'
          else
             write(*,'(a,i1,a,a)') 'as ipar(11)=',ipar(11),', the preconditioned',   &
             ' fgmres iterations will not be performed,'
             write( *,'(a)') 'thus, rci_request will not take the value 3'
          end if
          write( *,'(a)') '+++'
          if ( ipar(12) /= 0 ) then
             write(*,'(a,i1,a,a)')'as ipar(12)=',ipar(12),', the automatic test',   &
             ' for the norm of the next generated vector is not'
             write( *,'(a,a)') ' equal to zero up to rounding and computational',   &
             '  errors will be performed,'
             write( *,'(a)') 'thus, rci_request will not take the value 4'
          else
             write(*,'(a,i1,a,a)')'as ipar(12)=',ipar(12),', the automatic test ',  &
             ' for the norm of the next generated vector is'
             write(*,'(a,a)') 'not equal to zero up to rounding and computational',  &
             '  errors will be skipped,'
             write(*,'(a,a)') 'thus, the user-defined test will be requested via rci_request=4'
          end if
          write( *,'(a)') '+++'
       end if
       is_cleaned = .false.
    end if   

    if ( sol ) then

       ipar(3) = 1 ; ipar(4) = 0 ; ipar(14) = 0
!   
!--    compute the solution by rci (p)fgmres solver with preconditioning
!      reverse communication starts here
!
       n = femsys%n
       
1      call dfgmres ( n, femsys%X, femsys%rhs, request, ipar, fpar, works )
!
!--    if rci_request=0, then the solution was found with the required precision
!
       if ( request == 0 ) go to 3
!
!--    if rci_request=1, then compute the vector a*works(ipar(22))
!       and put the result in vector works(ipar(23))
!
       if ( request == 1 ) then
          call mkl_dcsrgemv ( 'n',n, femsys%mat, femsys%row, femsys%col, &
                              works(ipar(22)),                           &
                              works(ipar(23))                            )
          go to 1
       end if
!
!--    if rci_request=2, then do the user-defined stopping test
!      the residual stopping test for the computed solution is performed here
!
!      note: from this point vector rhs0(n) is no longer containing the right-hand
!      side of the problem! it contains the current fgmres approximation to the
!      solution. if you need to keep the right-hand side, save it in some other
!      vector before the call to dfgmres routine. here we saved it in vector
!      rhs(n). the vector rhs0 is used instead of rhs to preserve the original
!      right-hand side of the problem and guarantee the proper restart of fgmres
!      method. vector rhs0 will be altered when computing the residual stopping
!      criterion!

       if ( request == 2 ) then
    
!         request to the dfgmres_get routine to put the solution into b(n) via ipar(13):

          ipar(13) = 1

!         get the current fgmres solution in the vector rhs0(n):

          call dfgmres_get ( n, femsys%X, rhs0, request, ipar, fpar, works, itercount ) 

!         compute the current true residual via mkl (sparse) blas routines

          call mkl_dcsrgemv ( 'n', n, femsys%mat, femsys%row, femsys%col, rhs0, resid )

          call daxpy( n, -1.0e0_Rkind, femsys%rhs, 1, resid, 1 )

          dvar = sqrt(dot_product(resid,resid))

          if ( verb >= 1 ) then
             write(buf1,'(i6)')itercount
             write(buf2,'(e13.5)')dvar
             buf = ' iteration # : '//trim(adjustl(buf1))//', redisual : '//trim(adjustl(buf2))
             write (6,'(a1,a)',advance='no')char(13),trim(buf)
          end if  

          if ( dvar < fpar(1) ) then
             go to 3
          else
             go to 1
          end if
       end if
!
!--    if rci_request=3, then apply the preconditioner on the vector
!      works(ipar(22)) and put the result in vector works(ipar(23))
!      here is the recommended usage of the result produced by ilut routine
!      via standard mkl sparse blas solver routine mkl_dcsrtrsv.

       if ( request == 3 ) then
          call mkl_dcsrtrsv ( 'l','n','u',n,awrku,iwork,jwrku,works(ipar(22)),trvec )
          call mkl_dcsrtrsv ( 'u','n','n',n,awrku,iwork,jwrku,trvec,works(ipar(23)) )
          go to 1
       end if
!
!--    if rci_request=4, then check if the norm of the next generated vector is
!      not zero up to rounding and computational errors. the norm is contained
!      in fpar(7) parameter

       if ( request == 4 ) then
          if ( fpar(7) < 1.0e-12_Rkind ) then
             go to 3
          else
             go to 1
          end if
!
!--       if rci_request=anything else, then dfgmres subroutine failed
!         to compute the solution vector: computed_solution(n)
       else
          deallocate (works,jwrku,awrku,iwork,rhs0,trvec,resid) ; call mkl_free_buffers
          stat = err_t (stat=UERROR, where=Here, msg='dfgmres failed to compute the solution')
          is_cleaned = .true.
          return
       end if
!
!--    reverse communication ends here
!      get the current iteration number and the fgmres solution. (do not forget to
!      call dfgmres_get routine as computed_solution is still containing
!      the initial guess!). request to dfgmres_get to put the solution into
!      vector computed_solution(n) via ipar(13)
!
3      ipar(13) = 0
       call dfgmres_get ( n,femsys%x,femsys%rhs,request,ipar,fpar,works,itercount )

       if ( verb >= 1 ) then

          print*
          print*,'min, max :',minval(femsys%x), maxval(femsys%x)           
!
!--       print the number of iterations: itercount
! 
          write( *,'(a)') ' '
          write( *,'(a)') 'the system has been solved'
          write( *,'(a)') ' '
          write( *,'(a)') ' '
          write( *,'(a,i5)') 'number of iterations: ',itercount
          write( *,'(a)') ' '
!
!--       print the residual norm
!
          b = sqrt( dot_product( femsys%rhs, femsys%rhs ) )
          print*
          print*,'residual norm         = ||a*x - b||         =',dvar  
          print*,'relative resid. norm  = ||a*x - b|| / ||b|| =',dvar/b 
       end if
       is_cleaned = .false.
    end if ! end solve phase      
    
    if ( del .and. .not. is_cleaned ) then    
!
!--    release internal mkl memory that might be used for computations
!      note: it is important to call the routine below to avoid memory leaks
!      unless you disable mkl memory manager

       if (verb >= 1) print*,'MKL: Release internal memory'
       deallocate (works,jwrku,awrku,iwork,rhs0,trvec,resid) ; call mkl_free_buffers
       is_cleaned = .true.
    end if
    
#endif 
   
    END SUBROUTINE solverdriver_mklfgmres


!=============================================================================================
    SUBROUTINE solverdriver_mklpardisoI32 (pt    , maxfct, mnum, mtype, phase, n    , &
                                           Mat   , Row   , Col , iperm, nrhs , iparm, & 
                                           msglvl, Rhs   , X   , err                  )
!=============================================================================================                                          
    integer( i64 ), intent(in out) :: pt(64)
    integer( i32 ), intent(in    ) :: maxfct, mnum, mtype, phase, n, nrhs, msglvl
    integer( i32 ), intent(in    ) :: Row(:), Col(:)
    integer( i32 ), intent(in out) :: iperm(:), iparm(:)
    integer( i32 ), intent(in out) :: err
    real   (Rkind), intent(in    ) :: Mat(:)
    real   (Rkind), intent(in out) :: Rhs(:)
    real   (Rkind), intent(in out) :: X(:)
!---------------------------------------------------------------------------------------------	
!   calls pardiso_32
!---------------------------------------------------------------------------------------------	

#ifdef MKL

    call pardiso ( pt    , maxfct, mnum, mtype, phase, n    , &
                   Mat   , Row   , Col , iperm, nrhs , iparm, & 
                   msglvl, Rhs   , X   , err                  )
                  
#endif
   
    END SUBROUTINE solverdriver_mklpardisoI32


!=============================================================================================
    SUBROUTINE solverdriver_mklpardisoI64 ( pt    , maxfct, mnum, mtype, phase, n    , &
                                            Mat   , Row   , Col , iperm, nrhs , iparm, & 
                                            msglvl, Rhs   , X   , err                  )
!=============================================================================================                                          
    integer( i64 ), intent(in out) :: pt(64)
    integer( i64 ), intent(in    ) :: maxfct, mnum, mtype, phase, n, nrhs, msglvl
    integer( i64 ), intent(in    ) :: Row(:), Col(:)
    integer( i64 ), intent(in out) :: iperm(:), iparm(:)
    integer( i64 ), intent(in out) :: err
    real   (Rkind), intent(in    ) :: Mat(:)
    real   (Rkind), intent(in out) :: Rhs(:)
    real   (Rkind), intent(in out) :: X(:)
!---------------------------------------------------------------------------------------------	
!   calls pardiso_64
!---------------------------------------------------------------------------------------------	

#ifdef MKL
    
    call pardiso_64 (pt    , maxfct, mnum, mtype, phase, n    ,  &
                     Mat   , Row   , Col , iperm, nrhs , iparm,  & 
                     msglvl, Rhs   , X   , err                )

#endif
   
    END SUBROUTINE solverdriver_mklpardisoI64


!=============================================================================================
    SUBROUTINE solverdriver_error (uerr, where, num, msg)
!=============================================================================================
    integer  (Ikind), intent(in), optional :: num, uerr
    character(len=*), intent(in), optional :: msg, where
!---------------------------------------------------------------------------------------------	
!   Prints an error message and stops 
!---------------------------------------------------------------------------------------------	
    
!--locals:------------------------------------------------------------------------------------ 
    integer  (Ikind ) :: ue   
!---------------------------------------------------------------------------------------------            

    if (present(uerr)) then 
      ue = uerr
    else 
      ue = 6
    end if    
       
    write(ue,'(/,a)')'Error (solverdriver_error):'

    if (present(where)) write(ue,'(a)')'--> in solverdriver_'//trim(adjustl(where))
        
    if (present(num)) write(ue,'(a,i0)')'--> error  #',num
        
    if (present(msg)) write(ue,'(a)')'--> '//trim(msg)
       
    write(ue,'(a,/)')'--> STOP'
        
    stop

    END SUBROUTINE solverdriver_error
        
END MODULE solverdriver_m  

!---------------------------------------------------------------------------------------------            
!
!-- memo: list of control parameters for MKL Pardiso
!
!	  iparm(1)       !  = 0 : iparm(2) - iparm(64) are filled with default values.
!	                 ! <> 0 : You must supply all values in components iparm(2) - iparm(64).!
!	                 
!	  
!     iparm(2)       ! Fill-in reducing ordering for the input matrix.
!                    ! = 2 : The nested dissection algorithm from the METIS package
!	  
!	  iparm(3) = 0   ! Reserved. Set to zero.
!	  
!	  iparm(4)       ! Preconditioned CGS/CG. This parameter controls preconditioned CGS for
!	                 ! nonsymmetric or structurally symmetric matrices and CG for sym. matrices.
!	                 ! iparm(4) has the form iparm(4)= 10*L+K.
!	                 ! K = 0 : The factorization is always computed as required by "iphase".
!	                 ! K = 1 : CGS iteration replaces the computation of LU.
!	                 ! K = 2 : CGS iteration for sym.pos.def. matrices replaces the computation of LLT. 
!	                 ! The value L controls the stopping criterion of the Krylow-Subspace iteration 
!	                 
!	  iparm(5)       ! User permutation. This parameter controls whether user supplied fill-in
!	                 ! reducing permutation is used instead of the integrated multiple-minimum degree 
!	                 ! or nested dissection algorithms. Another use of this parameter is to control
!	                 ! obtaining the fill-in reducing permutation vector calculated during the reordering
!	                 ! stage of Intel MKL PARDISO.
!	                 ! = 0 : (default) User permutation in the perm array is ignored.
!	                 ! = 1 : Intel MKL PARDISO uses the user supplied fill-in reducing permutation
!	                 !       from the iperm array. iparm(2) is ignored.
!	                 ! = 2 : Intel MKL PARDISO returns the permutation vector computed at phase 1
!	                 !       in the iperm array.
!	  
!	  iparm(6)       ! = 0 : (default) The array x contains the solution; 
!	                 !       right-hand side vector b is kept unchanged
!	                 ! = 1 : The solver stores the solution on the right-hand side b.
!	                 
!	  iparm(7)       ! (Output) Number of iterative refinement steps performed.
!	                 ! Reports the number of iterative refinement steps that were
!	                 ! actually performed during the solve step.
!	                 
!     iparm(8)       ! Iterative refinement step.
!                    ! = 0 : The solver automatically performs two steps of iterative 
!                    !       refinement when perturbed pivots are obtained during the 
!                    !       numerical factorization.
!                    ! > 0 : Maximum numbers of iterative refinement steps
!                     
!	  iparm(9)       ! Reserved
!
!	  iparm(10)      ! Pivoting perturbation.
!	                 ! = 8 : The default value for symmetric indefinite matrices
!	                 ! (mtype =-2, mtype=-4, mtype=6), eps = 10-8.
!	                 ! = 13 : The default value for nonsymmetric matrices(
!	                 ! mtype =11, mtype=13), eps = 10-13.
!	                 
!	  iparm(11)      ! Scaling vectors.
!	                 ! = 0 : Disable scaling. Default for symmetric indefinite matrices.
!	                 ! = 1 : Enable scaling. Default for nonsymmetric matrices.
!	                 
!	  iparm(12)      ! Solve with transposed or conjugate transposed matrix A.
!	                 ! = 0 : (default) Solve a linear system AX = B.
!	                 ! = 1 : Solve a conjugate transposed system AHX = B based on the 
!	                 !       factorization of the matrix A.        
!	                 ! = 2 : Solve a transposed system ATX = B based on the 
!	                 !       factorization of the matrix A.      
!	                 
!	  iparm(13)      ! Improved accuracy using (non-) symmetric weighted matching.
!	                 ! = 0 : Disable matching. Default for symmetric indefinite matrices.
!	                 ! = 1 : Enable matching. Default for nonsymmetric matrices.
!	                 
!     iparm(14)      ! (output) Number of perturbed pivots.
!
!     iparm(15)      ! (output) Peak memory on symbolic factorization.
!
!     iparm(16)      ! (output) Permanent memory on symbolic factorization.
!
!     iparm(17)      ! (output) Size of factors/Peak memory on numerical factorization and solution.
!
!     iparm(18)      ! (input/output) Report the number of non-zero elements in the factors.
!                    !  < 0 : Enable reporting if iparm(18) < 0 on entry. The default value is -1.
!                    ! >= 0 : Disable reporting.
!
!     iparm(19)      ! (input/output) Report number of floating point operations 
!                    ! (in 1e6 floating point operations) that are necessary to factor the matrix A.
!
!     iparm(20)      ! (output) Report CG/CGS diagnostics.
!                    ! > 0 : CGS succeeded, reports the number of completed iterations.
!                    ! < 0 : CG/CGS failed (error=-4 after the solution phase).
!
!     iparm(21)      ! Pivoting for symmetric indefinite matrices.
!                    ! = 0 : Apply 1x1 diagonal pivoting during the factorization process.
!                    ! = 1 : (default) Apply 1x1 and 2x2 Bunch-Kaufman pivoting during the  
!                    !       factorization process. Bunch-Kaufman pivoting is available for 
!                    !       matrices of mtype=-2, mtype=-4, or mtype=6.
!
!     iparm(22)      ! (output) Inertia: number of positive eigenvalues.	
!  
!     iparm(23)      ! (output) Inertia: number of negative eigenvalues.
!	  
!     iparm(24)      ! Parallel factorization control.
!                    ! = 0 : Intel MKL PARDISO uses the classic algorithm for factorization.
!                    ! = 1 : Intel MKL PARDISO uses the sequential forward and backward solve.
!
!     iparm(25)      ! Parallel forward/backward solve control.
!                    ! = 0 : Intel MKL PARDISO uses a parallel algorithm for the solve step.
!                    ! = 1 : Intel MKL PARDISO uses the sequential forward and backward solve.
!
!     iparm(26)      ! Reserved. Set to zero.
!
!     iparm(27)      ! Matrix checker.
!                    ! = 0 : do not check the sparse matrix representation for errors.
!                    ! = 1 : check integer arrays ia and ja. In particular, 
!                    !       Intel MKL PARDISO checks whether column indices are sorted
!                    !       in increasing order within each row.
!                     
!     iparm(28)      ! Single or double precision Intel MKL PARDISO.
!                    ! = 0 : Input arrays (a, x and b) and all internal arrays must be presented
!                    !       in double precision.
!                    ! = 1 : niput arrays (a, x and b) must be presented in single precision.
!
!     iparm(29)      ! Reserved. Set to zero.
!
!     iparm(30)      ! (output) Number of zero or negative pivots.
!
!     iparm(31)      ! Partial solve and computing selected components of the solution vectors.
!                    ! = 0 : (default) Disables this option.
!                     
!     iparm(32)      ! Reserved. Set to zero.
!     iparm(33)      ! Reserved. Set to zero.
!                     
!     iparm(34)      ! Optimal number of OpenMP threads for conditional numerical
!                    ! reproducibility (CNR) mode.
!                    ! = 0 : (default) Intel MKL PARDISO determines the optimal number of OpenMP
!                    !       threads automatically, and produces numerically reproducible results
!                    !       regardless of the number of threads.
!                     
!     iparm(35)      ! One- or zero-based indexing of columns and rows.
!                    ! = 0 : (default) One-based indexing
!                    ! = 1 : Zero-based indexing
!                     
!     iparm(36)      ! Schur complement matrix computation control. 
!                    ! = 0 : (default) Do not compute Schur complement.
!                    ! = 1 : Compute Schur complement matrix as part of Intel MKL PARDISO
!                    !       factorization step and return it in the solution vector.
!
!     iparm(37)      ! Format for matrix storage.
!                    ! = 0 : (default) Use CSR format (see Three Array Variation of CSR Format)
!                    !       for matrix storage. 
!
!     iparm(38)      ! 
!     ...            ! Reserved. Set to zero.
!     iparm(55)      !                   
!
!     iparm(56)      ! Diagonal and pivoting control.
!                    ! = 0 : (default) Internal function used to work with pivot and
!                    !        calculation of diagonal arrays turned off.
!                    ! = 1 : You can use the mkl_pardiso_pivot callback routine to control
!                    !       pivot elements which appear during numerical factorization.
!
!     iparm(57)      ! 
!     ...            ! Reserved. Set to zero.
!     iparm(59)      !                   
!
!     iparm(60)      ! Intel MKL PARDISO mode.
!                    ! iparm(60) switches between in-core (IC) and out-of-core (OOC) Intel
!                    ! MKL PARDISO. OOC can solve very large problems by holding the matrix
!                    ! factors in files on the disk, which requires a reduced amount of main
!                    ! memory compared to IC.
!                    ! = 0 : (default) IC mode.
!
!     iparm(61)      ! 
!     ...            ! Reserved. Set to zero.
!     iparm(62)      !      
!
!     iparm(63)      ! (Output) Size of the minimum OOC memory for numerical factorization and solution.
!
!     iparm(64)      ! Reserved. Set to zero.
!    
!--------------------------------------------------------------------------------------------------------    
    