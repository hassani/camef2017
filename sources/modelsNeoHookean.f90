#include "error.fpp"

MODULE modelsNeoHookean_m
!--------------------------------------------------------------------------------------------- 
!-----------------------------------------------------------------------------------R.H. 12/16  

   use kindParameters_m

   use constants_m
   use defTypes_m
   use param_m     
   
   use modelsDiffusion_m, only : modelsDiffusion_error
   use element_m        , only : element_jacob, element_driver
   use femsystem_m      , only : femsystem_statio, femsystem_locelem
   use solverdriver_m   , only : solverdriver_resol   
   use output_m         , only : output_vnod, output_velm, output_velml, output_vnodSubSet, &
                                 output_vpntSubSet, output_velmSubSet
   use boundarycond_m   , only : boundarycond_Neumann2d, boundarycond_Neumann3d
   use util_m           , only : util_detAndInverse, util_triSurf, i2a=>util_intToChar
   use quadrature_m     , only : quadrature_driver

   implicit none
   
   private
   public :: modelsNeoHookean_main

   integer(Ikind) :: nincrem, increm
   real   (Rkind) :: loadfact, theta
          
CONTAINS


!=============================================================================================    
   SUBROUTINE modelsNeoHookean_main ( mymesh, mydata, femsys, stat, info )
!=============================================================================================    
   type     (mesh_t  ), intent(in out)           :: mymesh
   type     (udata_t ), intent(in out)           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in out), optional :: info(*)
!---------------------------------------------------------------------------------------------
!  Procedure that solves a large strain hyperelasticity (compressible Neo-Hookean) problem
!
!  We follow Here the method described in J. Bonet & R. D. Wood "Nonlinear continuum mechanics
!  for finite element analysis" and implemented in their code (FLagSHyP).
!--------------------------------------------------------------------------------------------- 
!
!  Inputs
!
!       mymesh: mesh structure
!
!       mydata: data (material properties & boundary conditions) structure
!
!       femsys: matrix structures
!
!       init  : logical. 
!               If init==.true.  --> initialization phase (see below)
!               If init==.false. --> solution phase       
!
!  Outputs
!
!       ndime, ndofn, nprop, nstri (in the initialization phase)
!
!       femsys%Sol (in the solution phase)
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'modelsNeoHookean_main'
   real     (Rkind), parameter   :: Half = 0.5e0_Rkind, Two = 2.0e0_Rkind
   character(len=:), allocatable :: geom
   integer  (Ikind)              :: i, p, d, lp, rp, dof, iter, jter, nitermax, niter0
   real     (Rkind)              :: tolrel, tolabs, dloadfact, Young, Poiss
   real     (Rkind)              :: s, ds, res, res1, step
   logical                       :: request(4)
   real     (Rkind), allocatable :: force0(:)
!---------------------------------------------------------------------------------------------    

   if ( stat > IZERO ) return 
   
   lp = index(name_model_pb,'(') ; rp = index(name_model_pb,')')
    
   geom = trim(adjustl(name_model_pb(lp+1:rp-1)))
    
   if ( trim(name_class_pb) == 'transient' ) then
      stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'sorry, transient elasticity problem not yet implemented' ) 
      return
   end if
    
   if ( present(info) ) then
!
!-    Information/initialization phase. According to the desired model define:
!
!      - ndime: space dimensions
!      - ndofn: nbr of d.o.f.
!      - nprop: nbr of material properties
!      - nstri: nbr of independent stress components 
!  
      if ( geom == 'plane_strain' .or. geom == 'plane_stress' ) then
         ndime = 2          ! 2D
         ndofn = ndime      ! 2 dof (x, y)
         ncmpN = ndime      ! 2 components of traction vector (for Neumann bc)
         nstri = 3          ! 3 indep. stresses : sxx, syy, sxy         
         nprop = 2 + ndime  ! 4 prop.: lambda, mu, rho*gx, rho*gy
         
         noutp = 2*nstri+2  ! 8 post-proceded variables: 
                            ! . 3 strains and 4 stresses in plane-strain
                            ! . 4 strains and 3 stresses in plane-stress
                            ! . det(F)

      else if ( geom == 'axisymmetry' ) then
         ndime = 2          ! 2D
         ndofn = ndime      ! 2 dof (r, z)
         ncmpN = ndime      ! 2 components of stress vector (for Neumann's bc)    
         nstri = 4          ! 4 indep. stresses : srr, szz, stt, srz                       
         nprop = 2 + ndime  ! 4 prop.: lambda, mu, rho*gr, rho*gz
         
         noutp = 2*nstri+1  ! post-proceded variables: 4 stresses, 4 strains, det(F)
       
      else if ( geom == '3d' ) then
         ndime = 3           ! 3D
         ndofn = ndime       ! 3 dof (x, y, z)
         ncmpN = ndime       ! 3 components of stress vector (for Neumann's bc)     
         nstri = 6           ! 6 indep. stresses : sxx, syy, szz, sxy, syz, szx                      
         nprop = 2 + ndime   ! 5 prop.: lambda, mu, rho*gx, rho*gy, rho*gz

         noutp = 2*nstri + 1 ! post-proceded variables: 6 stresses, 6 strains, det(F)
          
      else     
         stat = err_t ( stat = UERROR, where = Here, &
                         msg = trim(name_model_pb)//': not a valid model' ) 
         return
      end if
!
!-    messages output
!       
      info( 1) = "Informations from subroutine modelsNeoHookean:"
      info( 2) = '   list of parameters for the model "'//trim(name_model_pb)//'":'
      write(info(3),'("  * Space dimension                       : ",i0)')ndime
      write(info(4),'("  * Number of degrees of freedom          : ",i0)')ndofn
      write(info(5),'("  * Number of components for stress vector: ",i0)')ncmpN
      write(info(6),'("  * Number of material properties         : ",i0)')nprop
      write(info(7),'("   and these ",i0," material properties are:")')nprop
      info( 8) = "       (1:1) Young modulus"
      info( 9) = "       (2:2) Poisson ratio"
      if ( geom == 'plane_stress' .or. geom == 'plane_strain' ) &
         info(10) = "       (3:4) body forces: rho*gx, rho*gy"    
      if ( geom == 'axisymmetry' ) & 
         info(10) = "       (3:4) body forces: rho*gr, rho*gz"   
      if ( geom == '3d' ) &         
         info(10) = "       (3:5) body forces: rho*gx, rho*gy, rho*gz"    
      info(11) = "   and must be given in this order"
       
      return
      
   end if   
!
!- Resolution phase:
! 
!              Mat.     Rhs      Str.      Prod.
   request = [.true. , .true. , .true. , .false.] ! reform matrix and rhs at each iteration
    
   nitermax = nint(optnlinsol(1)) ! max. nbr of Newton's iterations 
   tolrel   =      optnlinsol(2)  ! relative tolerance
   tolabs   =      optnlinsol(3)  ! absolute tolerance
   niter0   = nint(optnlinsol(4)) ! 
   nincrem  = nint(optnlinsol(5)) ! number of increments
   step     = optnlinsol(6)
      
   if ( nincrem <= 0 ) nincrem = 1
   if ( niter0  <= 0 ) niter0  = 1
   if ( step    <= 0 ) step    = RONE
!
!- Compute Lamé parameters from Young modulus and Poisson ratio:
!
   do i = 1, nmats
      Young = mydata%vmatprop(1,i)    
      Poiss = mydata%vmatprop(2,i)   
      mydata%vmatprop(1,i) = Young*Poiss / (RONE + Poiss) / (RONE - Two*Poiss)
      mydata%vmatprop(2,i) = Young / ( Two * (RONE + Poiss) ) 
   end do
!
!- Save the initial nodal coordinates in %coor0
!
   mymesh%coor0 = mymesh%coord
!
!- Compute constant loads (not deformation-dependent): load due to gravity, point-forces and
!  deformation-independent surface traction. Store them in force0:
!
   allocate(force0(nequa))
   
   if ( geom == '3d' ) then   
      call modelsNeoHookean_ConstantLoad3d ( mymesh, mydata, femsys, force0, stat )
   else
      stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'sorry, 2D Neo-Hookean hyperlasticity noy yet implemented' ) 
      return
   end if   
!
!- Load increment loop:
!
   dloadfact = RONE / real(nincrem) ; loadfact = RZERO
   
   do increm = 1, nincrem
  
      loadfact = loadfact + dloadfact
!     
!-    Prescribed displacements:
!
      femsys%bCond = loadfact * mydata%vdirichl
      
      do p = 1, npoin
         do d = 1, ndime
            dof = femsys%Dof(d,p)
            if ( dof < 0 ) then
               mymesh%coord(d,p) = mymesh%coor0(d,p) + femsys%bCond(d,-dof)
            end if
         end do
      end do
!
!-    Newton iterations:   
!    
      if ( verbosity(4) >=1 )  &
         write(*,'(/,1x,91("-"),/,                                                        &
  &       18x,"I n c r e m e n t  # ",i0,"/",i0,"  (l o a d   f a c t o r = ",g0.3,")",/, &
  &       20x,"s t a r t i n g    N e w t o n    i t e r a t i o n s",/,               &
  &       1x,91("-"),/," Iter #",4x,"||dU||",8x,"||U||",4x, "||dU||/||U||",               &
  &       5x,"||R||",4x,"||R||/||R0||",2x,"Step length")') increm, nincrem, loadfact

      do iter = 1, nitermax    
!
!-       Form the tangent matrix and the rhs (residual):
!        
         if ( geom == '3d' ) then
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem  = modelsNeoHookean_KFelem3d    )
                                    
            if (stat==NEGJAC) print*,"à voir quoi faire (pour l'instant : sortir en erreur)"
            
            error_TraceNreturn(stat>IZERO, Here, stat)
         end if
!
!-       Add the constant loads to the residual:
!      
         femsys%Rhs = femsys%Rhs + loadfact * force0   
!
!-       Solve the linear system: compute the solution increment (in femsys%X):
!
         call solverdriver_resol ( femsys, stat, factorize='yes', solve='yes', clean='yes' ) 

         error_TraceNreturn(stat>IZERO, Here, stat)
!
!-       Update the solution (the coordinates) and compute the residual norm:
!      
         res = RZERO ; ds = RZERO ; s = RZERO 
         
         do dof = 1, nequa
            d = femsys%Dof2node(1,dof)
            p = femsys%Dof2node(2,dof)
            mymesh%coord(d,p) = mymesh%coord(d,p) + step*femsys%X(dof)
            
            s   = s   + mymesh%coord(d,p) * mymesh%coord(d,p)
            res = res + femsys%Rhs  (dof) * femsys%Rhs  (dof)
            ds  = ds  + femsys%X    (dof) * femsys%X    (dof)
         end do   

         s = sqrt(s) ; ds  = sqrt(ds) ; res = sqrt(res)     
         
         if ( iter ==  1 ) res1 = res    
!
!-       Stopping criteria (ds/s < tolrel):
! 
                           
         if ( verbosity(4) >=1 ) write(*,'(3x,i4,6e13.5)') &
                                 iter, ds, s, ds/s, res, res/res1, step

         if ( ds <= tolrel*s ) exit   
         
      end do    
   
      if ( verbosity(4) >=1 ) write(*,'(1x,91("-"))') 
      if ( ds <= tolrel*s ) then 
         if ( verbosity(4) >=1 ) &
            write(*,'(16x,                                                         &
            &     "C o n v e r g e n c e    t e s t    i s    s a t i s f i e d",  &
            &     /,1x,91("-"),/)')
      else    
         if ( verbosity(4) >=1 ) &
            write(*,'(12x,                                                                 &
            &     "c o n v e r g e n c e    t e s t    i s    N O T    s a t i s f i e d", &
            &     /,1x,91("-"),/)')
         stat = err_t( stat = WARNING, where = Here, &
                       msg = 'Convergence test is not satified for increment #'//i2a(increm) )
      end if                                         
!
!-    write the solution:
!
      femsys%Sol = mymesh%coord - mymesh%coor0 ! the total displacement
   
      call modelsNeoHookean_output ( mydata, mymesh, femsys, stat )     
       
   end do
   
   END SUBROUTINE modelsNeoHookean_main


!=============================================================================================
   SUBROUTINE modelsNeoHookean_KFelem3d ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element tangent stiffness matrix eldata%K, load vector eldata%F 
!  and/or stresses and strains eldata%S for a given element (# eldata%el)
!
!  Ref.: J. Bonet & R. D. Wood "Nonlinear continuum mechanics for finite element analysis"
!
!                     +-----------------------------------------------+  
!                     |       3D LARGE STRAIN NEO-HOOKEAN CASE        |
!                     +-----------------------------------------------+
!
!  The rheological model is the compressible neo-Hookean law:
!
!                  stress = (mu/J) * (b - I) + (lambda/J) * log(J) * I
!
!  where
!   . stress    : the Cauchy stress tensor
!   . b         : the left Cauchy-Green tensor (b = F*F^T)
!   . F         : the tranformation gradient (F = dx/dX = I + du/dX)
!   . I         : the rank-2 identity tensor
!   . J         : the volumetric dilatation (J = det(F))
!   . mu, lambda: the Lamé's modulus
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!           . props(1:1) = lambda,
!           . props(2:2) = mu,
!           . props(3:5) = body force components!    
!    - eldata%x    : current nodal coordinates
!    - eldata%x    : initial nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the tangent stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!  Notice
!
!   * For this model, note that
!       . ndime: = 3      , space dimensions
!       . nprop: = 5      , nbr of properties 
!       . ndofn: = 3      , nbr of dof per node (= ndime coordinates)
!       . nstri: = 6      , nbr independent  stresses
!       . nevab: = 3*nnode, nbr of dof per element
!
!   * The components of the Cauchy stress tensor and the Almansi strain tensor are stored as:
!
!      . stress = [ sigma_xx  sigma_yy  sigma_zz    sigma_xy    sigma_yz    sigma_zx ]
!      . strain = [ epsil_xx  epsil_yy  epsil_zz  2*epsil_xy  2*epsil_yz  2*epsil_zx ]
!
!     and the element nodal variables as 
!
!      . U = [ x1 y1 z1 | x2 y2 z2 | ...  | xn yn zn ] 
!
!     (n = nnode = nbr of nodes).
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = 'modelsNeoHookean_KFelem3d'
   real     (Rkind), parameter :: Half = 0.5_Rkind, Two = 2.0_Rkind
   real     (Rkind)            :: Be(nstri,nevab), De(nstri,nstri), &
                                  elcar(ndime,nnode), strai(nstri), volsrc(ndofn), &
                                  F(ndime,ndime), b(ndime,ndime), sigma(ndime,ndime), &
                                  s(nnode,nnode), binv(ndime,ndime), sgradN(ndime,nnode)
   real     (Rkind)            :: dv, eljac, mup, mu_J, lamb_J, detF
   integer  (Ikind)            :: ip, in, jn, iv, jv, id
!---------------------------------------------------------------------------------------------          

   if ( rqst(1) ) then
      Be(:,:) = RZERO ;  De(:,:) = RZERO ; eldata%K(:,:) = RZERO 
   end if 
    
   if ( rqst(2) ) eldata%F(:) = RZERO

!
!- loop over the I.P. (integration phase):
!          
   do ip = 1, nipts      
!
!-    compute the cartesian derivatives (elcar) w.r.t current coordinates at the ip-th I.P. 
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//i2a(eldata%el)//')' )
         return
      end if
            
      dv = eljac * eldata%wIP(ip)
!
!-    given the current cartesian derivatives (elcar) of the shape functions and the 
!     initial nodal coordinates (eldata%x0) compute the left Cauchy-Green tensor (b), 
!     the transformation gradient (F) and its determinant (detF)
!
      call modelsNeoHookean_LeftCauchyGreen ( elcar, eldata%x0, eldata%el, &
                                              b, F, detF, binv, stat )
      error_TraceNreturn(stat>IZERO, Here, stat)
!
!-    compute the Cauchy stress:
!                     
      lamb_J = eldata%props(1) / detF    !< lambda / J
      mu_J   = eldata%props(2) / detF    !< mu / J
      mup    = mu_J - lamb_J * log(detF) !< mu / J - (lambda / J) * log(J)
      
      sigma = mu_J * b 
      do id = 1, ndime
         sigma(id,id) = sigma(id,id) - mup
      enddo
      
      sgradN = matmul ( sigma, elcar )
      
      if ( rqst(1) ) then
!
!-       D matrix (elasticity tensor):  
!         
         De(1:3,1:3) = lamb_J
         De(1,1) = De(1,1) + Two*mup
         De(2,2) = De(2,2) + Two*mup
         De(3,3) = De(3,3) + Two*mup         
         De(4,4) = mup ; De(5,5) = mup ; De(6,6) = mup
!    
!-       B matrix:
!
         Be(1, 1:nevab:3) = elcar(1,:)
         Be(2, 2:nevab:3) = elcar(2,:)
         Be(3, 3:nevab:3) = elcar(3,:) 
         Be(4, 1:nevab:3) = elcar(2,:) ;  Be(4, 2:nevab:3) = elcar(1,:)
         Be(5, 2:nevab:3) = elcar(3,:) ;  Be(5, 3:nevab:3) = elcar(2,:)
         Be(6, 1:nevab:3) = elcar(3,:) ;  Be(6, 3:nevab:3) = elcar(1,:)    
!
!-       Tangent stiffness matrix: 1) constitutive part (material non-linearities)
!         
         eldata%K = eldata%K + dv * matmul ( matmul ( transpose(Be), De ), Be )      
!
!-       Tangent stiffness matrix: 2) initial stress part (geometric non-linearities)
!         
         s = dv * matmul ( transpose(elcar), sgradN )
            
         do jn = 1, nnode
            do in = 1, nnode
               do id = 1, ndime
                  iv = (in-1)*ndime + id
                  jv = (jn-1)*ndime + id
                  eldata%K(iv,jv) = eldata%K(iv,jv) + s(in,jn)
               end do
            end do
         end do
                         
      end if
             
      if (rqst(2)) then   
!
!-       Elemental residual (without the Neumann bc nor the body forces. These loads are
!        already computed and assembled):
!
         iv = 0
         do in = 1, nnode
            jv = in*ndime ; iv = jv - ndime + 1
            eldata%F(iv:jv) = eldata%F(iv:jv) - sgradN(:,in) * dv
         end do
         
      end if
      
      if ( rqst(3) ) then
!
!-       Store the Cauchy stress tensor, the Euler-Almansi strain tensor and det(F):
!        
         eldata%S(1:nstri,ip) = Half*[RONE-binv(1,1), RONE-binv(2,2), RONE-binv(3,3), &
                                          -binv(1,2),     -binv(2,3),     -binv(3,1) ]
                                          
         eldata%S(nstri+1:2*nstri,ip) = [ sigma(1,1),sigma(2,2),sigma(3,3), &
                                          sigma(1,2),sigma(2,3),sigma(3,1) ]
                                          
         eldata%S(2*nstri+1,ip) = detF                                         
      end if       
            
   end do   

   END SUBROUTINE modelsNeoHookean_KFelem3d


!=============================================================================================
   SUBROUTINE modelsNeoHookean_LeftCauchyGreen ( dNdx, x0, el, b, F, detF, binv, stat )
!=============================================================================================
   real   (Rkind), intent(in    ) :: dNdx(:,:), x0(:,:)
   integer(Ikind), intent(in    ) :: el
   real   (Rkind), intent(   out) :: b(:,:), F(:,:), detF, binv(:,:)
   type   (err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Computes the transformation gradient (F), the left Cauchy-Green tensor (b) and the volumic
!  ratio (detF) given:
!  . the cartesian derivatives (w.r.t current coordinates), dNdx, of the shape functions 
!  . the initial nodal coordinates x0
!
!  The gradient of the inverse transformation is first computed: Finv = sum {x0_i .o. dNdx_i}
!  (.o. the tensor product) then inverted.
!
!  Source: copied and modified from flagshyp08v30, Bonnet & Wood
!--------------------------------------------------------------------------------------------- 

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = 'modelsNeoHookean_LeftCauchyGreen'
   integer  (Ikind)            :: i, j, n
   real     (Rkind)            :: Finv(ndime,ndime), detFinv
!--------------------------------------------------------------------------------------------- 

!
!- determine F^(-1):
!
   Finv = RZERO
   
   do n = 1, nnode
      do j = 1, ndime
         do i = 1, ndime
            Finv(i,j) = Finv(i,j) + x0(i,n) * dNdx(j,n)
         end do
      end do
   end do   
!
!- inverse F^(-1):
!   
   call util_detAndInverse ( A = Finv, n = ndime,      & !< in
                             Ainv = F, detA = detFinv, & !> out
                             stat = stat               ) 
                             
   if ( detFinv <= 0 ) then
      stat = err_t ( stat = NEGJAC, where = Here, &
                      msg = 'Negative jacobian (element #'//i2a(el)//')' )
      return
   end if
   
   detF = RONE / detFinv
!                                     
!- Finally obtains the tensor b = F*F^T and its inverse
!  
   b    = matmul ( F              , transpose(F) )
   binv = matmul ( transpose(Finv), Finv         )
      
   END SUBROUTINE modelsNeoHookean_LeftCauchyGreen


!=============================================================================================
   SUBROUTINE modelsNeoHookean_constantLoad3d ( mymesh, mydata, femsys, force0, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(udata_t ), intent(in    ) :: mydata 
   type(linsys_t), intent(in    ) :: femsys 
   real(Rkind   ), intent(   out) :: force0(:)
   type(err_t   ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Computes the equivalent nodal load for:
!
!  . Gravity forces,
!  . Neumann's boundary conditions for 3D case (imposed load on faces, edges or points).
!
!  Assumptions:
!       - body forces are constant on each element
!       - the prescribed boundary loads are constant on each face or egde (P0)
!       - the edges are 2-noded (linear) or 3-noded (quadratic)
!       - the edges are rectilinear
!       - the faces are 3-noded (linear), 4-noded (bi-linear) or 6-noded (quadratic)
!       - the faces are planar
!       - for quadratic elements, the non-vertices nodes are at the middle locations  
!
!  If one of these assumptions is not fulfilled use the NEUMANN3DQ subroutine (version
!  with quadrature schemes).
! 
!  With these assumptions, the contribution of a given edge to the global load can be computed
!  by hand:
!
!  - linear case:
!
!        (1)                                 
!         |
!         |  <--- F      F_i =  F * L / 2,   i = 1, 2
!         |                    
!        (2)
!  
!  - quadratic case:
!
!        (1)                                 
!         |
!        (3)  <--- F      F_i = F * L / 6,       i = 1, 2
!         |                   = F * 4 * L / 6,   i = 3
!        (2)
!                        
!
!  And for a given face:
!
!  - linear case or bi-linear cases:                   - quadratic cases:
!
!         (1)               (1)-------(4)                         (1)  
!        /   \               |         |                         /   \
!       /     \              |         |                       (4)   (6)
!      /       \             |         |                       /       \
!    (2)-------(3)          (2)-------(3)                    (2)--(5)--(3)
!
!   F_i = F * S / 3,       F_i = F * S / 4               F_j = 0,   j = 1, 2, 3 
!                       
!     i = 1, 2, 3           i = 1, 2, 3, 4               F_i = F * S / 3,   i = 4, 5, 6
!
!    
!  Inputs:
!
!       nneum : number of Neumann's bc
!       nedge : number of referenced external edges
!       npoif : number of referenced external points
!       ndofn : number of dof

!       mymesh%ledge   : the edges references and their nodes
!       mymesh%lpoin   : the points references and their attached nodes
!       mydata%lneumann: # of points or edges where a bc applies
!       mydata%oneumann: geometry type (point or edge)
!       mydata%vneumann: imposed values
!       mydata%codeneum: code that indicates if these values are given in
!                        local or global frame (for vectorial problem) 
!
!  Ouputs:
!
!       femsys%Rhs: the assembled right hand side       
!---------------------------------------------------------------------------------------------

!--local variables: --------------------------------------------------------------------------
   character(len=*   ), parameter :: Here = 'modelsNeoHookean_constantLoad3d'
   real     (Rkind   ), parameter :: z = 0.0_Rkind, d = 0.5_Rkind, u = 1.0_Rkind,      & 
                                     t = 3.0_Rkind, q = 4.0_Rkind, s = 6.0_Rkind
                                  
   integer  (Ikind   ) :: nsomt_e, nsomt_f, nnod_e, nnod_f, case_e, case_f
   integer  (Ikind   ) :: iref, jref, in, id, ip, i, j, idof, frame, el
   
   real     (Rkind   ) :: area, stri, coef, ut, us, uq, qs
   
   real     (Rkind   ) :: cface(6,2), cedge(3,2), solic(ncmpN), val(ncmpN), &
                          xyz(3,4), xyzt(3,3), unitnormal(3), v(3)
   
   type     (eldata_t) :: eldata                    
   real     (Rkind   ) :: elcar(ndime,nnode), rhog(ndime), eljac, dv
                      
   character(len=1 ) :: obj
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 

   force0 = z
!
!- 1) Compute the body forces:
! 
   if ( .not. eldata%allocated ) call eldata%alloc ( ndime, nnode, ndofn, noutp, &
                                                     nipts, nevab, nprop )

   call quadrature_driver ( eldata%xIP, eldata%wIP, nipts, ndime, name_geometry, stat )
   error_TraceNreturn(stat>IZERO, Here, stat) 
         
   do ip = 1, nipts
      call element_driver ( name_element, ndime, nnode, stat, pnt=eldata%xIP(:,ip), &
                            shp=eldata%shp(:,ip), drv=eldata%der(:,:,ip) )
      error_TraceNreturn(stat>IZERO, Here, stat)
   end do       
     
   do el = 1, nelem 
      eldata%el = el
      
      call femsystem_locelem ( mymesh, mydata, femsys, eldata )
   
      rhog = eldata%props(3:5)   
             
      do ip = 1, nipts
         call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode )

         if ( eljac <= 0 ) then
            stat = err_t ( stat = NEGJAC, where = Here, &
                           msg = 'Negative jacobian (element #'//i2a(eldata%el)//')' )
            return
         end if

         dv = eljac * eldata%wIP(ip)  
         
         i = 0
         do in = 1, nnode
            do id = 1, ndime
               i = i + 1
               idof = eldata%Dof(i)
               if ( idof > 0 ) force0(idof) = force0(idof) + dv * rhog(id)*eldata%shp(in,ip)
            end do
         end do
      end do
   end do
      
   if (nneum == 0) return
!
!- 2) Compute the prescribed boundary load:
!     
   if ( name_geometry == 'tetraedre' ) then
      nsomt_f = 3  ! number of vertices on face
      ut = u / t
      cface(:,1) = [ut, ut, ut, z , z , z ]   ! coef. for 3-noded triangle face
      cface(:,2) = [z , z , z , ut, ut, ut]   ! coef. for 6-noded triangle face
   else if ( name_geometry == 'hexaedre' ) then
      nsomt_f = 4 ! number of vertices on face
      uq = u / q   
      cface(:,1) = [uq, uq, uq, uq, z, z] ! coef. for 4-noded quad face
   else
      stat = err_t ( stat=UERROR, where=Here, msg= 'Unknown element geometry: ' // &
                     trim(name_geometry) )
      return
   end if
   
   nsomt_e = 2 ! number of vertices on edge   
   us = u / s; qs = q / s
   cedge(:,1) = [d , d , z ] ! coef. for 2-noded edge
   cedge(:,2) = [us, us, qs] ! coef. for 3-noded edge
      
   nnod_f = size(mymesh%lface,1) - 1 ! number of nodes on face

   if ( nnod_f /= 3 .and. nnod_f /= 6 .and. nnod_f /= 4 ) then
      stat = err_t ( stat = UERROR, where = Here, &
                     msg = 'this subroutine is only applicable for 3-, 4-, 6-noded faces' )
      return
   end if
   
   nnod_e = size(mymesh%ledge,1) - 1 ! number of nodes on edge

   if ( nnod_e /= 2 .and. nnod_e /= 3 ) then
      stat = err_t ( stat = UERROR, where = Here, &
                     msg = 'this subroutine is only applicable for 2-noded or 3-noded edges' )
      return
   end if
       
   case_f = 1; if ( nnod_f /= nsomt_f ) case_f = 2
   case_e = 1; if ( nnod_e /= nsomt_e ) case_e = 2
    
   do i = 1, nneum
   
      iref = mydata % lneumann(i)  ! ref # of the ith Neumann bc
      
      obj  = mydata % oneumann(i)  ! object identifier ('p' for point or 'l' for line)
      
      val  = mydata % vneumann(:,i) ! the given values
            
      if ( obj == 'p' ) then
!
!-       Concentrated load. Find in the mesh the nodes of reference "iref":
!      
         do j = 1, npoif 
            jref = mymesh % lpoin(1,j) ! ref. # of the jth boundary node
            if ( jref == iref ) then
               ip = mymesh % lpoin(2,j)! global # of this node 
               do id = 1, ncmpN
                  idof = femsys % Dof(id,ip)
                  force0(idof) = force0(idof) + val(id)
               end do
            end if
         end do
         
      else if ( obj == 'l' ) then
!
!-       Distributed load on an edge. Find in the mesh the edges of references "iref":                
!                           
         do j = 1, nedge   
            jref = mymesh % ledge(1,j) ! ref. # of the jth edge
            if ( jref == iref ) then  
               xyz(:,1:nsomt_e) = mymesh % coord(:, mymesh % ledge(2:nsomt_e+1,j)) ! vertices 
                                                                                   ! coord.
               v = xyz(:,2) - xyz(:,1) ! tangential vector
               
               solic = val*sqrt(dot_product(v,v))
                     
               do in = 1, nnod_e
                  ip = mymesh % ledge(1+in,j)
                  coef = cedge(in,case_e)
                  do id = 1, ncmpN
                     idof = femsys%Dof(id,ip)
                     if ( idof > 0 ) force0(idof) = force0(idof) + coef*solic(id)
                  end do
               end do
            end if
         end do
         
      else if ( obj == 'f' ) then
!
!-       Distributed load on a face. Find in the mesh the faces of references "iref":                
!        
         frame = maxval(mydata % codeneum(:,i))  ! =1 for global frame, =2 for local frame

         do j = 1, nface
       
            jref = mymesh % lface(1,j)
       
            if ( jref == iref ) then
               xyz(:,1:nsomt_f) = mymesh%coord(:,mymesh % lface(2:nsomt_f+1,j)) ! vertices
                                                                                ! coordinates
               if ( nsomt_f == 3 ) then
!
!-                triangle area and unit normal:
!               
                  call util_trisurf ( xyz, area, unitnormal )
               else
!
!-                quadrangular area (as assumed planar, split it in 2 triangles):
!
                  xyzt = xyz(:,1:3)    ; call util_trisurf (xyzt, area, unitnormal)
                  xyzt = xyz(:,[3,4,1]); call util_trisurf (xyzt, stri, unitnormal)
                  area = area + stri
               end if     

               solic = z
               
               if ( frame == 1 ) then
!
!-                the load components are given in the global frame:
!
                  solic = val * area     
               else if ( frame == 2 ) then
               
                  stat = err_t( stat=UERROR, where=Here, msg= &
                  'Sorry, not yet ready for dependent-deformation loads in large strain' )
                  return
!
!-                the load components are given in the local frame
!                 (in fact, only the normal component at present)
!                 Compute its components in the global frame:
!                          
                  if ( ncmpN == 1 ) then
                     solic = val(1) * area
                  else
                     solic = val(1) * area * unitnormal
                  end if
                  

               end if   
               
               do in = 1, nnod_f
                  ip = mymesh % lface(1+in,j)
                  coef = cface(in,case_f)
                  do id = 1, ncmpN
                     idof = femsys % Dof(id,ip)
                     if ( idof > 0 ) force0(idof) = force0(idof) + coef*solic(id)
                  end do
               end do
            end if
         end do
      end if   
   end do                 
                       
   END SUBROUTINE modelsNeoHookean_ConstantLoad3d
   
   
!=============================================================================================
   SUBROUTINE modelsNeoHookean_output ( mydata, mymesh, femsys, stat )
!============================================================================================= 
   type(udata_t ), intent(in) :: mydata
   type(mesh_t  ), intent(in) :: mymesh        
   type(linsys_t), intent(in) :: femsys  
   type(err_t   ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!
!---------------------------------------------------------------------------------------------

!--local variables: --------------------------------------------------------------------------
   character(len=* ), parameter   :: Here = 'modelsNeoHookean_output' 
   integer  (Ikind )              :: iout, freq
   character(len=: ), allocatable :: title, form
   character(len=80)              :: buf
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 

   if (name_outputtype(1) == 'none' ) return
   
   write(buf,'(e13.5)') loadfact
   title = 'Increment: '//i2a(increm)//'/'//i2a(nincrem)//'   load factor: '//trim(buf)
   
   do iout = 1, size(name_outputtype)
   
      form = name_outputfmt(iout)
      freq = freq_output   (iout)
      
      select case ( name_outputtype(iout) )

         case('vnod')
!
!-          for printing nodal values (primary unknowns):
!         
            call output_vnod ( form, freq, femsys, mymesh, stat, title )
            
         case('vnodSubSet')
!
!-          for printing nodal values of given nodes:
!         
            call output_vnodSubSet ( form, freq, femsys, mymesh, stat )
            
         case('vpntSubSet')
!
!-          for printing nodal values of given points:
!         
            call output_vpntSubSet ( form, freq, femsys, mymesh, stat )
            
         case('velm')
!
!-          for printing elemental values at I.P. locations:
!         
            call output_velm ( form, freq, femsys, stat, title )
            
         case('velmSubSet')
!
!-          for printing elemental values at I.P. of given elements:
!         
            call output_velmSubSet ( form, freq, femsys, stat )            

         case('velml')
!
!-          for printing elemental values averaged on element level and smoothed at nodes
!         
            call output_velml ( form, freq, femsys, mymesh, stat, title )            
                   
         case('p')
!
!-          for printing results in vtk format (secondary unknowns smoothed at nodes)
!         
            call modelsNeoHookean_writePfile ( form, freq, femsys, mymesh, mydata, stat )

         case('')
                  
         case default
            stat = err_t( stat = WARNING, where = Here, msg = 'Unknown output format "'//  &
                                                        trim(name_outputtype(iout)) //'"'  )
            
      end select
   end do
   
   END SUBROUTINE modelsNeoHookean_output 


!=============================================================================================   
   SUBROUTINE modelsNeoHookean_writePfile ( form, freq, femsys, mymesh, mydata, stat )
!=============================================================================================
   use util_m, only: util_fromElementNameToVtkCode, util_write
   character(len=*   ), intent(in    ) :: form
   integer  (Ikind   ), intent(in    ) :: freq        
   type     (udata_t ), intent(in    ) :: mydata
   type     (mesh_t  ), intent(in    ) :: mymesh        
   type     (linsys_t), intent(in    ) :: femsys  
   type     (err_t   ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!
!---------------------------------------------------------------------------------------------

!--local variables: --------------------------------------------------------------------------
   character(len=*), parameter         :: Here = 'modelsNeoHookean_writePfile', &
                                          fmt='(1000(g0,1x))'
   integer  (Ikind), save              :: n = 0, uo = 0
   integer  (Ikind)                    :: codevtk, i, iE, iS, iJ, ip, i1, i2, el
   logical         , save              :: first = .true., desc = .true.
   character(len=:), allocatable       :: pfile
   character(len=:), save, allocatable :: cnelem, cnpoin, szlnods, cndime, cnstri
   real     (Rkind)                    :: r, xipts
   real     (Rkind)                    :: mE(nstri,nelem), mS(nstri,nelem), mJ(nelem)
!---------------------------------------------------------------------------------------------   

   if ( stat > IZERO ) return 

   n = n + 1
   
   if ( first ) then
      cnelem = i2a(nelem)
      cnpoin = i2a(npoin)
      cndime = i2a(ndime)
      cnstri = i2a(nstri)
      szlnods = i2a(nelem*nnode)       
!
!-    If first access, open the p-file and write the header
!
      pfile = trim(name_outputDir) // 'p'//trim(name_input_file)
      
      if ( form == 'bin' ) then
         open ( newunit = uo, file = pfile, action = 'write', form = 'unformatted' )
         write(uo)'$Binary, sizeOfInt=' //i2a(kind(n))// &
                  ', sizeOfReal='//i2a(kind(r))
      else
         open ( newunit = uo, file =  pfile, action = 'write' )        
         write(uo,'(a)')'$Ascii, sizeOfInt=' //i2a(kind(n))// &
                        ', sizeOfReal='//i2a(kind(r))         
      end if
!
!-    Write the mesh connectivities and the initial coordinates:
!
      call util_write ( uo, form, '$Begin_step, num=0' )
      call util_write ( uo, form, '# This record (num=0) shows only the initial mesh' )

      call util_write ( uo, form, '$Mesh:Connectivity, geomEntities=cells, ' // &
                                 'nGeomEntities=' // cnelem // ', size=' // szlnods )
      call util_write ( uo, form, mymesh%lnods, fmt=fmt )
            
      call util_write ( uo, form, '$Mesh:VtkTypeId, geomEntities=cells, ' // &
                                 'nGeomEntities=' // cnelem // ', size=1' )
      codevtk = util_fromElementNameToVtkCode ( name_element, stat )
      
      error_TraceNreturn(stat>IZERO, Here, stat)
      
      call util_write ( uo, form, codevtk ) ! vtk code (assuming only one type of element) 

      call util_write ( uo, form, '$Mesh:DomainId, geomEntities=cells, ' // &
                                 'nGeomEntities=' // cnelem // ', size=' // cnelem ) 
      call util_write ( uo, form, mymesh%numdom, fmt=fmt  )

      call util_write ( uo, form, '$Mesh:Coordinates, nDime = '//i2a(ndime) //  &
                       ',nNode ='//i2a(npoin)//', unit = m' )
      call util_write ( uo, form, mymesh%coor0, fmt=fmt  )
      
      call util_write ( uo, form, '$End_step, num=0' )
      
      first = .false.      
   end if


   if ( n == freq ) then
      if (verbosity(1) >= 1) write(*,'(1x,a)') 'Writing into the output file "'// &
         trim(name_outputDir) // 'p'//trim(name_input_file)//'" in '//trim(form)//' format'   
   
!
!-    Open a new record for the increment #increm
!   
      call util_write ( uo, form, '$Begin_step, num='//i2a(increm) )
!
!-    Current coordinates:
!                 
      call util_write ( uo, form, '$Mesh:Coordinates, nDime = '//i2a(ndime) //  &
                     ',nNode ='//i2a(npoin)//', unit = m' )
      call util_write ( uo, form, mymesh%coord, fmt=fmt )
!
!-    Mesh-independent variables:
!
      if ( desc ) then
         call util_write ( uo, form, '$Fields, nField = 2, geomEntities = -' )
         call util_write ( uo, form, 'name=increment, physicalQuantity=-, unit=-, nComp=1'   )
         call util_write ( uo, form, 'name=load_factor, physicalQuantity=-, unit=-, nComp=1' )
      else
         call util_write ( uo, form, '$Fields, nField = 2, geomEntities = -, desc=no' )
      end if            
      call util_write ( uo, form, [real(increm,kind=Rkind),loadfact] )
!
!-    Nodal variables:
!      
      if ( desc ) then
         call util_write ( uo, form, '$Fields, nField = 1, geomEntities = nodes, '// &
                           'nGeomEntities = '//cnpoin )
         call util_write ( uo, form, 'name = U, physicalQuantity = displacement, '// &
                           'unit = m, nComp = '//cndime )
      else
         call util_write ( uo, form, '$Fields, nField = 1, geomEntities = nodes, '// &
                           'nGeomEntities = '//cnpoin//', desc = no' )
      end if            
      call util_write ( uo, form, femsys%Sol, fmt=fmt )      
!
!-    Elemental variables:
!
      if ( desc ) then
         call util_write ( uo, form, '$Fields, nField = 3, geomEntities = cells, '// &
                           'nGeomEntities = '//cnelem )
         call util_write ( uo, form, 'name = Almansi_Strain, physicalQuantity = -, '// &
                           'unit = -, nComp = '//cnstri )     
         call util_write ( uo, form, 'name = Cauchy_Stress, physicalQuantity = stress, '// &
                           'unit = Pa, nComp = '//cnstri )
         call util_write ( uo, form, 'name = Volume_ratio, physicalQuantity = -, '// &
                           'unit = -, nComp = 1')      
      else
         call util_write ( uo, form, '$Fields, nField = 3, geomEntities = cells, '// &
                           'nGeomEntities = '//cnelem//', desc = no' )
      end if            
      
      ! compute the mean stress and mean strain in each element by averaging the I.P. values:
      
      xipts = RONE / real(nipts,kind=Rkind)
      iE = ndime ; iS = ndime + nstri ; iJ = ndime + 2*nstri
      do el = 1, nelem
         mS(:,el) = RZERO
         mE(:,el) = RZERO
         mJ(el) = RZERO
         i1 = (el - 1) * nipts + 1 ; i2 = i1 + nipts - 1 ! first and last I.P. of el
         do ip = i1, i2
            do i = 1, nstri 
               mE(i,el) = mE(i,el) + femsys%Str(iE+i,ip)
               mS(i,el) = mS(i,el) + femsys%Str(iS+i,ip)
            end do
            mJ(el) = mJ(el) + femsys%Str(iJ+1,ip)
         end do
         mE(:,el) = mE(:,el) * xipts ; mS(:,el) = mS(:,el) * xipts ; mJ(el) = mJ(el)*xipts
      end do
      if ( form == 'bin' ) then
         write(uo) mE,mS,mJ
      else
         write(uo,fmt) mE,mS,mJ
      end if
       
!
!-    Close the record:
!
      call util_write ( uo, form, '$End_step, num='//i2a(increm) )

      n = 0
      desc = .false.    
        
   end if
   
   if ( increm == nincrem ) then
      call util_write ( uo, form, '$End' )
      close(uo)
   end if   
   
   END SUBROUTINE modelsNeoHookean_writePfile   


END MODULE modelsNeoHookean_m
