MODULE param_m

   use kindParameters_m

   use defTypes_m
   use err_m
   use constants_m, only: MDSTR
    
   save
!
!- parameters from the mesh
!
   character(len=MDSTR) :: name_mesh_file  ! name of the mesh file
    
   integer  (Ikind ) :: nnode,    &     ! nbr of nodes per element
                        nsomt,    &     ! nbr of vertices per element (= nnode for (bi-,tri-)
                                         ! linear elements)
    
                        nelem,    &     ! nbr of elements in the mesh
                        npoin,    &     ! nbr of nodes in the mesh
    
                        nedge,    &     ! nbr of edges on the mesh boundary
                        nface,    &     ! nbr of facets on the mesh boundary (=nedge in 2d) 
                        npoif,    &     ! nbr of geometrical points
    
                        ndedg,    &     ! nbr of nodes by edge
                        ndfac,    &     ! nbr of nodes by facet (=ndegd in 2d)
    
                        maxne,    &     ! max. nbr of elements connected to a node
                        maxnn           ! max. nbr of nodes connected to a node
!
!- parameters from the physics
!    
   character(len=80) :: name_model_pb, & ! Model problem name
                        name_class_pb    ! Class problem name ("steady" or "transient')

   integer  (Ikind ) :: ndime,    &     ! Space dimension (1, 2 or 3, deduced from the physical
                                        ! problem)
                        ndofn,    &     ! Nbr of degrees of freedom (deduced from the physical
                                        ! problem)
                        ncmpN,    &     ! Nbr of components for generalized forces (traction
                                        ! vector, normal flux,...) used to define Neumann values
                        nmats,    &     ! Nbr of materials (deduced from the data)
                        nprop,    &     ! Nbr of properties for each mat. (deduced from the
                                        ! physical problem)
                        nstri,    &     ! Nbr of indep. components of secondary unknowns 
                                        ! (stresses,...) (deduced from the physical problem)
                        noutp           ! Nbr of post-proceded variables (stresses, strain, etc)

   integer  (Ikind ) :: ndiri,    &     ! nbr of Dirichlet bc
                        nneum           ! nbr of Neumann bc
                         
   real     (Rkind ) :: Ttime           ! total time in case of transient problem  
   integer  (Ikind ) :: Ntime           ! nbr of time-steps
   
!
!- parameters related to the finite element
! 
   character(len=80) :: name_element, & ! name of the selected finite element 
                        name_geometry   ! name of its geometry

   integer  (Ikind)  :: nipts,        & ! nbr of integration points per element
                        ntotg,        & ! total nbr of integration points (= nipts*nelem)
                        nevab           ! nbr of nodal d.o.f per element (= nnode*ndofn)   
                                                                      
!
!- parameters related to the linear system  
!                       
   integer  (Ikind)  :: nequa,        & ! total nbr of equations (=nbr of unknowns, =size of
                                         ! the matrix)
                        nzero           ! size of the array needed to store the matrix
    
!
!- parameters related to the linear solver
!
   character(len=80) :: name_linSolver, & ! name of the solver
                        name_storage  , & ! name of the storage format
                        name_reorderMeth  ! name of the reordering method
                         
   real     (Rkind ) :: optlinsol(21)     ! optional parameters for the solver                     
                         
!
!- parameters related to the non-linear solver
!
   character(len=80) :: name_nlinsolver   ! name of the solver
    
   real     (Rkind ) :: optnlinsol(21)    ! optional parameters for the solver                     
                         
!
!- input file name, ouput file names and output frequencies 
!
   character(len=MDSTR) :: name_input_file    , &  ! input file name (without the .par extension)
                           name_outputtype(10), &  ! extensions of the output files
                           name_outputfmt (10), &  ! corresponding formats (ascii or bin)
                           name_outputDir
                         
   integer  (Ikind) :: freq_output(10)  ! output frequencies
   
   integer  (Ikind) :: nod_output(100  ), nNod_output = 0, &
                       elm_output(100  ), nElm_output = 0, &
                       pnt_output(100,2), nPnt_output = 0
!
!- verbosity levels
!    
   integer(Ikind) :: verbosity(10)

!
!- Free options
!
   real(Rkind) :: optmod(10)  
!
!- Error handler
!
   !!type(err_t) :: stat
!
!- Timer
!   
   type(cpu_t) :: timer  
                 
END MODULE param_m
