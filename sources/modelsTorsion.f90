#include "error.fpp"

MODULE modelsTorsion_m
!--------------------------------------------------------------------------------------------- 
!-----------------------------------------------------------------------------------R.H. 12/16  
   use kindParameters_m

   use defTypes_m
   use err_m
   use param_m       

   use constants_m      , only: IZERO, RZERO, RONE, NEGJAC       
   use element_m        , only: element_jacob
   use femsystem_m      , only: femsystem_statio, femsystem_storeNodalValues
   use solverdriver_m   , only: solverdriver_resol   
   use util_m           , only: util_intToChar
   use output_m         , only: output_driver

   implicit none
   
   private
   public :: modelsTorsion_main
    
CONTAINS


!=============================================================================================    
   SUBROUTINE modelsTorsion_main ( mymesh, mydata, femsys, stat, info )
!=============================================================================================    
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (udata_t ), intent(in out)           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat   
   character(len=*   ), intent(in out), optional :: info(*)
!---------------------------------------------------------------------------------------------            
!  An example showing how to solve a simple stationnary non-linear problem.
!
!   The problem concerns the torsion of an elastoplastic shaft. According to the cylindrical
!   symmetry the problem reduces to a 2d problem where only a scalar function u is sought 
!   (u is the so called stress potential) defined on the cross section w (in R^2) of the shaft.  
!   The resulting optimization problem is of the form:
!
!                 find u in U   st   J(u) < J(v)  for all v in U
!
!   where               J(v) := int_{w} ( |grad(u)|^2 - f )
!
!   and                  U = { v in V, F(v) := |grad(v)| - Y <= 0 },   V = H_0^1(w)
!
!   that is an optimization problem with inequality constraints. 
!   In this expression, f is the angle (by unit length) of torsion, and Y is the plastic yield.
!
!   A penalization method may be used to relax the constraints: J is replaced by
!
!                    J_e(v) := J(v) + (1/e) * int_{w} < F(v) >^n
!
!   where e is a small enough parameter, <.> stands for the positive part and n is an even
!   integer (to preserve the convexity). An alternative, is to use the Uzawa's method.
!   
!   The Euler's condition gives the variationnal problem:  find u in V  st 
!
!          DJ_e(u)(v) = 0  <=>  int_{w} ( a(u) grad(u).grad(v) - fv ) = 0  for all v in V.  (*)
!
!   where a(u) = 1 + (2n/e) < F(u) >^{n-1}.
!
!   The problem we have to solve is then a generalized laplacian problem (note that for Y 
!   large enough, F(u) < 0 and thus a(u) = 1: the problem is a classical laplacian one).  
!   
!   An iteration of the Newton's method applied to (*) reads: find du in V st   
!
!   int{w} grad(v).A(u^k).grad(du) = int{w} ( fv - a(u^k) grad(v).grad(u^k) ) for all v in V
!
!   u^{k+1} = u^k + du
!
!   where A(u) = a(u)I + b(u) grad(u) o grad(u)   and  b(u) = (4n(n-1)/e) < F(u) >^{n-2}
!
!   and "o" stands for the tensorial product.
!
!   We can then use the generic procedure femsystem_statio by passing it in argument the name
!   of a KFelem subroutine (called here modelsTorsion_KFelem) that computes the element   
!   stiffness matrix and load vector.
!--------------------------------------------------------------------------------------------- 
!
!  Inputs
!
!       mymesh: mesh structure
!
!       mydata: data (material properties & boundary conditions) structure
!
!       femsys: matrix structures
!
!       init  : logical. 
!               If init==.true.  --> initialization phase (see below)
!               If init==.false. --> solution phase       
!
!  Outputs
!
!       ndime, ndofn, nprop, nstri (in the initialization phase)
!
!       femsys%Sol (in the solution phase)
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: Here = 'modelsTorsion_main'
   integer  (Ikind)            :: iter, nitermax
   real     (Rkind)            :: s, ds, res, res0, tolrel, tolabs
   logical                     :: request(4), is_conv
!---------------------------------------------------------------------------------------------    
    
   if ( present(info) ) then
!
!-    Initialisation phase (to be called before reading data). Define:
!
!      - ndime: space dimensions
!      - ndofn: nbr of d.o.f. 
!      - nprop: nbr of material properties to be read and saved in mydata%vmatprop
!      - nstri: nbr of independent stress components)  
!
      ndime = 2 ! 2D only
      ndofn = 1 ! the unkonwn is the scalar stress potential
      nstri = 2 ! its two gradient components
      nprop = 3 ! 3 properties: yield, angle of torsion, penalty parameter     
      ncmpN = 1 ! 1 component of the normal flux (for Neumann's bc)     
      
      noutp = nstri            
!
!-    output messages
!       
      info( 1) = "Informations from subroutine models_torsion:"
      info( 2) = '   list of parameters for the model "'//trim(name_model_pb)//'":'
      write(info(3),'("  * Space dimension                     : ",i0)')ndime
      write(info(4),'("  * Number of degrees of freedom        : ",i0)')ndofn
      write(info(5),'("  * Number of components for normal flux: ",i0)')ncmpN
      write(info(6),'("  * Number of material properties       : ",i0)')nprop
      write(info(7),'("   and these ",i0," material properties are:")')nprop
      info( 8) = "       (1:1) Plastic yield"
      info( 9) = "       (2:2) Angle of torsion"
      info(10) = "       (3:3) Eps (Penalty parameter for the penalized formulation)"
                     
      return 
    
   end if   
!
!- Resolution phase: Penalized formulation solved with Newton's method:
! 
!              Mat.     Rhs      Str.      Prod.
   request = [.true. , .true. , .false. , .false.] ! reform matrix and rhs at each iteration
    
   nitermax = nint(optnlinsol(1)) ! max. nbr of Newton's iterations 
   tolrel   = optnlinsol(2)       ! relative tolerance
   tolabs   = optnlinsol(3)       ! absolute tolerance
    
   femsys%Sol = RZERO ; femsys%X = RZERO ! initialize solution and increment solution
    
   if ( verbosity(4) >=1 )                                                  &
   write(*,'(/,1x,71("-"),/,                                                &
  &       9x,"s t a r t i n g    N e w t o n ‘ s    i t e r a t i o n s",/, &
  &       1x,71("-"),/," Iter #",4x,"||dU||",8x,"||U||",4x, "||dU||/||U||", &
  &       5x,"||R||",4x,"||R||/||R0||")')
      
       
   do iter = 1, nitermax
!
!-    Form the matrix and the rhs:
!
      call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                              KFelem  = modelsTorsion_KFelem         )
                              
      error_TraceNreturn(stat>IZERO, Here, stat)                        
!
!-    Solve the linear system: compute the solution increment (in femsys%X):
!       
      femsys%X = RZERO
      call solverdriver_resol ( femsys, stat, factorize='yes', solve='yes', clean='yes' )  
      error_TraceNreturn(stat>IZERO, Here, stat) 
!
!-    Update the solution:
!
      call femsystem_storeNodalValues ( femsys, is_incr = .true. )
!
!-    Stopping criteria (ds/s < tolrel & ds < tolabs):
! 
      call modelsTorsion_CheckConv ( femsys, tolrel, tolabs, s, ds, res, is_conv )
      
      if ( iter == 1 ) res0 = res
       
      if ( verbosity(4) >=1 ) write(*,'(3x,i4,5e13.5)')iter, ds, s, ds/s, res, res/res0

      if ( is_conv ) exit
      
      res0 = res

   end do    
   
   write(*,'(1x,71("-"))') 
   if ( ds < tolrel*s .and. ds < tolabs ) then
      write(*,'(7x,                                                         &
     &     "c o n v e r g e n c e    t e s t    i s    s a t i s f i e d",  &
     &     /,1x,71("-"),/)')
   else    
      write(*,'(2x,                                                                 &
     &     "c o n v e r g e n c e    t e s t    i s    n o t    s a t i s f i e d", &
     &     /,1x,71("-"),/)')
   end if   

!
!- Postprocessing: compute the gradient vector at each I.P.
!
!                Mat.     Rhs       Str.     Prod.
   request = [ .false. , .false. , .true. , .false. ] ! compute only the derived quantities

   call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                           KFelem  = modelsTorsion_KFelem         )
                           
   error_TraceNreturn(stat>IZERO, Here, stat)                                                                
!
!- write the solution:
!
   call output_driver ( mydata, mymesh, femsys, stat )
   
   END SUBROUTINE modelsTorsion_main

                
!=============================================================================================
   SUBROUTINE modelsTorsion_KFelem ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata   
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F and/or 
!  stresses and strains eldata%S for a given element (# eldata%el)
!
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!                    props(1) = Yield, props(2) = angle of torsion
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!   Notice
!
!   * For this model, note that
!
!       - ndime: = 2          , space dimensions
!       - nprop: = 2          , nbr of properties 
!       - ndofn: = 1          , nbr of dof per node
!       - nstri: = ndime      , nbr gradient components
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!   * The gradient and the flux on each I.P. are
!
!       grad = [ dxu, dyu ]
!
!     and the nodal dof (n = nnode = nbr of nodes)
!
!       U = [ u1, u2, ..., u_n ] 
!
!     With this convention   
!     
!       grad = Be * U    (thus  \int (K = Be' * De(u) * Be))
!
!     where De(u) is 2x2 matrix and Be contains the shape function derivatives at I.P.
!
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = "modelsTorsion_KFelem"
   real     (Rkind)            :: De(2,2), seui2, penal, alpha, gradu(2), ngradu2, a, b, diff, &
                                  dv, eljac, elcar(ndime,nnode)
   integer  (Ikind)            :: n, ip
!---------------------------------------------------------------------------------------------          
    
   eldata%K(:,:) = RZERO ; eldata%F(:) = RZERO

   n = 4
   seui2 = eldata%props(1)**2; alpha = 2.0*eldata%props(2)
   penal = RONE / eldata%props(3)

   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if                           
!
!-    compute the gradient solution of the previous iteration:
!
      gradu = matmul (elcar, eldata%U) ; ngradu2 = dot_product(gradu,gradu)
!       
!-    integration phase:
!
      dv = eljac * eldata%wIP(ip)
         
      diff = ngradu2 - seui2

      if ( diff > 0 ) then
         a = RONE + 2.0_Rkind * n         * penal * diff**(n-1)
         b =        4.0_Rkind * n * (n-1) * penal * diff**(n-2)
      else
         a = RONE; b = RZERO
      end if   
        
      De(1,:) = [ a + b*gradu(1)*gradu(1),     b*gradu(1)*gradu(2) ]
      De(2,:) = [     b*gradu(1)*gradu(2), a + b*gradu(2)*gradu(2) ]
    
      eldata%K = eldata%K + dv * matmul ( transpose(elcar), matmul(De, elcar) )
      
      eldata%F = eldata%F + &
               dv * (alpha * eldata%shp(:,ip) - a * matmul ( transpose(elcar),gradu ))         
!
!-    save the gradient if requested
!       
      if ( rqst(3) ) eldata%S(:,ip) = gradu(:)
       
   end do   
    
   END SUBROUTINE modelsTorsion_KFelem


!=============================================================================================
   SUBROUTINE modelsTorsion_Checkconv ( femsys, tolrel, tolabs, s, ds, res, is_conv )
!=============================================================================================
   type   (linsys_t), intent(in    ) :: femsys 
   real   (Rkind   ), intent(in    ) :: tolrel, tolabs
   real   (Rkind   ), intent(   out) :: s, ds, res
   logical          , intent(   out) :: is_conv
!--------------------------------------------------------------------------------------------- 
!  
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   integer(Ikind) :: id, ip, ieq
!--------------------------------------------------------------------------------------------- 
   
   is_conv = .false. ; s = RZERO ; ds = RZERO ; res = RZERO
   
   do ip = 1, npoin
      do id = 1, ndofn
         ieq = femsys%Dof(id,ip)
         if ( ieq > 0 ) then
            s   = s   + femsys%Sol(id,ip) * femsys%Sol(id,ip)
            ds  = ds  + femsys%X(ieq)     * femsys%X(ieq)
            res = res + femsys%Rhs(ieq)   * femsys%Rhs(ieq)
         end if
      end do
   end do
   
   s   = sqrt(s) ! norm of the solution vector
   ds  = sqrt(ds) ! norm of the increment vector
   res = sqrt(res) ! norm of the residual vector 
   
   is_conv = ( ds < tolrel*s .and. ds < tolabs )  
  
   END SUBROUTINE modelsTorsion_Checkconv
   

!=============================================================================================
   SUBROUTINE modelsTorsion_KFelemByPerturbation ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata   
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Experimental
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = "modelsTorsion_KFelem"
   real     (Rkind), parameter :: eps = sqrt(epsilon(RONE))
   real     (Rkind)            :: seui2, penal, alpha, gradu(2), ngradu2, a, b, diff, kij, nu, &
                                  dv, eljac, h, elcar(ndime,nnode), F0(nevab), F(nevab), u(nevab)
   integer  (Ikind)            :: n, ip, iv, jv
!---------------------------------------------------------------------------------------------          
    
   eldata%F(:) = RZERO ; eldata%K(:,:) = RZERO

   n = 4
   seui2 = eldata%props(1)**2; alpha = 2.0*eldata%props(2)
   penal = RONE / eldata%props(3)

   nu = dot_product(eldata%U,eldata%U)
   
   if ( nu /= 0 ) then
      h = max(eps,eps/sqrt(nu))
   else
      h = eps
   end if


   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode  )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if                           
      
      dv = eljac * eldata%wIP(ip)

      u = eldata%U
      do iv = 0, nevab
         if (iv > 0) then
           ! if (u(iv) /= 0) then
           !    h = max(eps, eps / abs(U(iv)))
           ! else
           !    h = eps
           ! end if
            u(iv) = u(iv) + h
         end if 
         
         gradu = matmul (elcar, u) ; ngradu2  = dot_product(gradu,gradu)
         diff = ngradu2 - seui2

         if ( diff > 0 ) then
            a = RONE + 2.0_Rkind * n * penal * diff**(n-1)
         else
            a = RONE
         end if   
         
         F = dv * (alpha * eldata%shp(:,ip) - a * matmul ( transpose(elcar),gradu ))
         
         if ( iv == 0 ) then 
            F0 = F
            eldata%F = eldata%F + F0
         else
            eldata%K(:,iv) = eldata%K(:,iv) -(F-F0)/h
         end if
            
         if ( iv > 0 ) u(iv) = eldata%U(iv)
      end do  
!
!-    save the gradient if requested
!       
      if ( rqst(3) ) eldata%S(:,ip) = gradu(:)
       
   end do   
   
   do jv = 1, nevab
      do iv = jv+1, nevab
         kij = 0.5_Rkind*(eldata%K(iv,jv) + eldata%K(jv,iv))
         eldata%K(iv,jv) = kij ; eldata%K(jv,iv) = kij
      end do
   end do

    
   END SUBROUTINE modelsTorsion_KFelemByPerturbation   
            
   
END MODULE modelsTorsion_m
