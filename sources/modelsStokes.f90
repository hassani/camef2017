#include "error.fpp"

MODULE modelsStokes_m
!--------------------------------------------------------------------------------------------- 
!-----------------------------------------------------------------------------------R.H. 12/16  
   use kindParameters_m

   use defTypes_m
   use err_m
   use param_m       

   use constants_m      , only: IZERO, RZERO, RONE, UERROR, NEGJAC, WARNING       
   use element_m        , only: element_jacob
   use femsystem_m      , only: femsystem_statio, femsystem_storeNodalValues
   use solverdriver_m   , only: solverdriver_resol   
   use util_m           , only: util_intToChar   
   use output_m         , only: output_driver
   use boundarycond_m   , only: boundarycond_Neumann2d, boundarycond_Neumann3d, &
                                boundarycond_Dirichlet1

   implicit none
   
   private
   public :: modelsStokes_main, modelsStokes_mainNL, modelsStokes_mainNL2

   real   (Rkind) :: theta
   integer(Ikind) :: maxiter
       
   real   (Rkind), allocatable :: peier(:)    
   
CONTAINS


!=============================================================================================
   SUBROUTINE modelsStokes_main ( mymesh, mydata, femsys, stat, info )
!=============================================================================================
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (udata_t ), intent(in    )           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in out), optional :: info(*)
!---------------------------------------------------------------------------------------------            
!  Procedure that solves a linear Stokes problem
!--------------------------------------------------------------------------------------------- 
!
!  Inputs
!
!      name_model_pb: string giving the name of the model (in lowercase and without spaces)
!
!      name_class_pb: string giving the class of the problem (steady or transient)
!                    
!      mymesh       : mesh structure
!
!      mydata       : data (material properties & boundary conditions) structure
!
!      femsys       : matrix structures
!
!      info         : array of strings, optional
!                     Through this option the model can be interogated to give some informations
!                     to the calling procedure about the required properties, number of d.o.f,
!                     space dimensions,... (useful for the input data phase)
!
!                     If present     --> informations are required
!
!                     If not present --> solution phase          
!
!  Outputs
!
!      information phase: 
!                      ndime: space dimensions
!                      ndofn: number of dof (= ndime + 1)
!                      ncmpN: number of components of the traction vector (= ndime)
!                      nprop: number of material properties
!                      nstri: number of indep. deviatoric stress components
!                      info : messages that can be displayed to the user
!
!      solution phase: femsys%Sol
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'modelsStokes_main' 
   character(len=:), allocatable :: geom
   integer  (Ikind)              :: lp, rp
   logical                       :: request(4)
   external                      :: KFelem, Neumann
!---------------------------------------------------------------------------------------------    

   if ( stat > IZERO ) return 

   lp = index(name_model_pb,'(') ; rp = index(name_model_pb,')')
    
   geom = trim(adjustl(name_model_pb(lp+1:rp-1)))
    
   if ( trim(name_class_pb) == 'transient' ) then
      stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'sorry, transient Stokes problem not yet implemented' ) 
      return 
   end if 
      
   if ( present(info) ) then
!
!-    Information/initialization phase. According to the desired model define:
!
!      - ndime: space dimensions
!      - ndofn: nbr of d.o.f.
!      - nprop: nbr of material properties
!      - nstri: nbr of independent stress components 
!  
      if ( geom == '2d' ) then
         ndime = 2           ! 2D
         ndofn = 3           ! 3 dof (ux, uy, p)
         ncmpN = ndime       ! 2 components (Fx, Fy) of traction vector (for Neumann's bc)     
         nstri = 3           ! 3 indep. dev. stresses : sxx, syy, sxy             
         nprop = 3           ! 3 prop.: viscosity, rho*gx, rho*gy
         
         noutp = 2*nstri + 1 ! post-proceded variables: 3 stresses, 3 strain rates, viscosity 
       
      else if ( geom == '3d' ) then
         ndime = 3           ! 3D
         ndofn = 4           ! 4 dof (ux, uy, uz, p)
         ncmpN = ndime       ! 3 components (Fx, Fy, Fz) of traction vector (for Neumann's bc)         
         nstri = 6           ! 6 indep. stresses : sxx, syy, szz, sxy, syz, szx      
         nprop = 4           ! 4 prop.: viscosity, rho*gx, rho*gy, rho*gz

         noutp = 2*nstri + 1 ! post-proceded variables: 6 stresses, 6 strain rates, viscosity 
          
      else     
         stat = err_t ( stat = UERROR, where = Here, &
                         msg = trim(name_model_pb)//': not a valid model' ) 
         return       
      end if
!
!-    messages output
!       
      info( 1) = "Informations from subroutine models_Stokes:"
      info( 2) = '   list of parameters for the model "'//trim(name_model_pb)//'":'
      write(info(3),'("  * Space dimension                       : ",i0)')ndime
      write(info(4),'("  * Number of degrees of freedom          : ",i0)')ndofn
      write(info(5),'("  * Number of components for stress vector: ",i0)')ncmpN
      write(info(6),'("  * Number of material properties         : ",i0)')nprop
      write(info(7),'("   and these ",i0," material properties are:")')nprop
      info( 8) = "       (1:1) viscosity"
      if ( geom == '2d' ) &
      info( 9) = "       (2:3) body forces: rho*gx, rho*gy"    
      if ( geom == '3d' ) &         
      info( 9) = "       (2:4) body forces: rho*gx, rho*gy, rho*gz"    
      info(10) = "   and must be given in this order"
       
      return
      
   end if   
!
!- Resolution phase:
!
   femsys%X(:) = RZERO
    
   select case (name_class_pb)

      case ('steady')
!
!-       Form the matrix and the rhs:
!
         if ( verbosity(2) >= 1 ) print*,'Computing the rigidity matrix and the load vector'

!                     Mat      Rhs      Stress    Prod.
         request = [ .true. , .true. , .false. , .false. ]
       
         if ( geom == '3d' ) then
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsStokes_KFelem   ,    &
                                    Neumann   = boundarycond_Neumann3d,    &
                                    Dirichlet = boundarycond_Dirichlet1    )
       
         else if ( geom == '2d' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsStokes_KFelem   ,    &
                                    Neumann   = boundarycond_Neumann2d,    &
                                    Dirichlet = boundarycond_Dirichlet1    )
                                  
         end if    
         
         error_TraceNreturn(stat>IZERO, Here, stat)
!
!-       Solve the linear system:
!       
         if ( verbosity(2) >= 1 ) print*,'Solving the linear system'

         call solverdriver_resol ( femsys, stat )  
         error_TraceNreturn(stat>IZERO, Here, stat)
      
         call femsystem_storeNodalValues ( femsys )
!      
!-       Compute the stresses (in femsys%Str):
!
         if ( verbosity(2) >=1 ) print*,'Computing the derived quantities (stress)'
          
!                     Mat       Rhs       Stress    Prod.
         request = [ .false. , .false. , .true.  , .false. ]
              
         if ( geom == '3d' ) then
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem  = modelsStokes_KFelem          )       
         else if ( geom == '2d' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem  = modelsStokes_KFelem          )                           
         end if       

         error_TraceNreturn(stat>IZERO, Here, stat)
!
!-       write the solution on the output file:
!
         if ( verbosity(2) >= 1 ) print*,'Printing the results'

         call output_driver ( mydata, mymesh, femsys, stat )
   

      case ('transient')
!
!-       cas transitoire : pas encore fait
!    
!        conditions initiales, ...
!
!        do itime = 1, ntime
!           ...
!           ... matrice rigidite + masse + rhs(time)
!           ... resolution 
!           ... impression:
!           call output_driver (mydata, mymesh, femsys)

!        end do

      case default
         stat = err_t ( stat = UERROR, where = Here, msg = trim(name_class_pb) // &
                       ': not a valid class problem' )
         return
             
   end select   
                     
   END SUBROUTINE modelsStokes_main


!=============================================================================================    
   SUBROUTINE modelsStokes_mainNL ( mymesh, mydata, femsys, stat, info )
!=============================================================================================    
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (udata_t ), intent(in out)           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in out), optional :: info(*)
!---------------------------------------------------------------------------------------------
!  Procedure that solves a non-linear Stokes problem
!--------------------------------------------------------------------------------------------- 
!
!  Inputs
!
!       mymesh: mesh structure
!
!       mydata: data (material properties & boundary conditions) structure
!
!       femsys: matrix structures
!
!       init  : logical. 
!               If init==.true.  --> initialization phase (see below)
!               If init==.false. --> solution phase       
!
!  Outputs
!
!       ndime, ndofn, nprop, nstri (in the initialization phase)
!
!       femsys%Sol (in the solution phase)
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'modelsStokes_mainNL'
   character(len=:), allocatable :: geom
   integer  (Ikind)              :: i, p, d, iter, nitermax, lp, rp, niterPicard
   real     (Rkind)              :: tolrel, tolabs
   real     (Rkind)              :: su, dsu, resu, resu1
   real     (Rkind)              :: sp, dsp, resp, resp1
   logical                       :: request(4)
!---------------------------------------------------------------------------------------------    

   if ( stat > IZERO ) return 
   
   lp = index(name_model_pb,'(') ; rp = index(name_model_pb,')')
    
   geom = trim(adjustl(name_model_pb(lp+1:rp-1)))
    
   if ( trim(name_class_pb) == 'transient' ) then
      stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'sorry, transient Stokes problem not yet implemented' ) 
      return 
   end if 
    
   if ( present(info) ) then
!
!-    Information/initialization phase. According to the desired model define:
!
!      - ndime: space dimensions
!      - ndofn: nbr of d.o.f.
!      - nprop: nbr of material properties
!      - nstri: nbr of independent stress components 
!  
      if ( geom == '2d' ) then
         ndime = 2           ! 2D
         ndofn = 3           ! 3 dof (ux, uy, p)
         ncmpN = ndime       ! 2 components (Fx, Fy) of traction vector (for Neumann's bc)     
         nstri = 3           ! 3 indep. dev. stresses : sxx, syy, sxy             
         nprop = 5           ! 4 prop.: beta, n, regul, rho*gx, rho*gy
         
         noutp = 2*nstri + 1 ! post-proceded variables: 3 stresses, 3 strain rates, viscosity 
       
      else if ( geom == '3d' ) then
         ndime = 3          ! 3D
         ndofn = 4          ! 4 dof (ux, uy, uz, p)
         ncmpN = ndime      ! 3 components (Fx, Fy, Fz) of traction vector (for Neumann's bc)         
         nstri = 6          ! 6 indep. stresses : sxx, syy, szz, sxy, syz, szx      
         nprop = 6          ! 5 prop.: beta, n, regul, rho*gx, rho*gy, rho*gz

         noutp = 2*nstri + 1 ! post-proceded variables: 6 stresses, 6 strain rates, viscosity 
          
      else     
         stat = err_t ( stat = UERROR, where = Here, &
                         msg = trim(name_model_pb)//': not a valid model' ) 
         return       
       
      end if
!
!-    messages output
!       
      info( 1) = "Informations from subroutine models_StokesNL:"
      info( 2) = '   list of parameters for the model "'//trim(name_model_pb)//'":'
      write(info(3),'("  * Space dimension                       : ",i0)')ndime
      write(info(4),'("  * Number of degrees of freedom          : ",i0)')ndofn
      write(info(5),'("  * Number of components for stress vector: ",i0)')ncmpN
      write(info(6),'("  * Number of material properties         : ",i0)')nprop
      write(info(7),'("   and these ",i0," material properties are:")')nprop
      info( 8) = "       (1:1) viscosity coefficient: beta"
      info( 9) = "       (2:2) exponent: n"
      info(10) = "       (3:3) regularization parameter: eps"
      if ( geom == '2d' ) then
         info(11) = "       (4:5) body forces: rho*gx, rho*gy"    
      end if
      if ( geom == '3d' ) then       
         info(11) = "       (4:6) body forces: rho*gx, rho*gy, rho*gz"    
      end if 
      info(12) = "   and must be given in this order"
       
      return
      
   end if   

!
!- Resolution phase (Newton's method):
! 
!              Mat.     Rhs      Str.      Prod.
   request = [.true. , .true. , .false. , .false.] ! reform matrix and rhs at each iteration
    
   nitermax    = nint(optnlinsol(1)) ! max. nbr of Newton's iterations 
   tolrel      = optnlinsol(2)       ! relative tolerance
   tolabs      = optnlinsol(3)       ! absolute tolerance
   niterPicard = nint(optnlinsol(4)) ! number of initial Picard's iterations
   
   if ( niterPicard < 0 ) niterPicard = 5
       
   femsys%Sol = RZERO ; femsys%X = RZERO ! initialize solution and increment solution
    
   if ( verbosity(4) >=1 )                                                  &
   write(*,'(/,1x,91("-"),/,                                                &
  &      18x,"s t a r t i n g    N e w t o n'' s   i t e r a t i o n s",/,  &
  &       1x,71("-"),/," Iter #",4x,"||dU||",8x,"||U||",4x, "||dU||/||U||", &
  &       5x,"||R||",4x,"||R||/||R0||",5x,"step")')
         
   theta = RZERO 
   
   do iter = 1, nitermax      
!
!-    Form the matrix and the rhs:
!
      femsys%X = RZERO

      if ( geom == '3d' ) then
         call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                 KFelem  = modelsStokes_KFelemNL,       &
                                 Neumann = boundarycond_Neumann3d       )
      else if ( geom == '2d' ) then
         call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                 KFelem  = modelsStokes_KFelemNL,       &
                                 Neumann = boundarycond_Neumann2d       )
      end if
      
      error_TraceNreturn(stat>IZERO, Here, stat)
!
!-    Solve the linear system: compute the solution increment (in femsys%X):
!
      call solverdriver_resol ( femsys, stat, factorize='yes', solve='yes', clean='yes' ) 
      error_TraceNreturn(stat>IZERO, Here, stat)  
!
!-    Update the solution:
!       
      call femsystem_storeNodalValues ( femsys, is_incr = .true. )
!
!-    Stopping criteria (ds/s < tolrel & ds < tolabs):
! 
      sp = RZERO; dsp = RZERO; resp = RZERO; su = RZERO; dsu = RZERO; resu = RZERO
      do p = 1, npoin
         do d = 1, ndofn
            i = femsys%dof(d,p)
            if ( i > 0 ) then
               if ( d == ndofn ) then
                  sp   = sp   + femsys%Sol(d,p)**2
                  dsp  = dsp  + femsys%X(i)    **2
                  resp = resp + femsys%Rhs(i)  **2
               else
                  su   = su   + femsys%Sol(d,p)**2
                  dsu  = dsu  + femsys%X(i)    **2
                  resu = resu + femsys%Rhs(i)  **2
               end if
            end if
         end do
      end do
      sp = sqrt(sp) ; dsp = sqrt(dsp) ; resp = sqrt(resp)
      su = sqrt(su) ; dsu = sqrt(dsu) ; resu = sqrt(resu)
         
      !s   = sqrt(dot_product(femsys%Sol,femsys%Sol)) ! norm of the solution vector
      !ds  = sqrt(dot_product(femsys%X  ,femsys%X  )) ! norm of the increment vector
      !res = sqrt(dot_product(femsys%Rhs,femsys%Rhs)) ! norm of the residual vector
            
      if ( iter == 1 ) then
         resu1 = resu ; resp1 = resp
      end if
      
      if ( niterPicard > 0 ) then
         if ( iter == niterPicard ) then
            resu1 = resu ; resp1 = resp
         else if ( iter > niterPicard ) then
            theta = max(0.0_Rkind, 1.0_Rkind-sqrt(resu/resu1))
         end if  
      else if ( iter > 1 ) then
         theta = max(RZERO, RONE-sqrt(resu/resu1))
      end if
                           
      if ( verbosity(4) >=1 ) write(*,'(3x,i4,7e13.5)') &
                           iter, dsu, su, dsu/su, resu, resu/resu1, theta

      if ( dsu < tolrel*su ) exit ! .and. dsp < tolrel*sp) exit  
      
   end do    

   if ( verbosity(4) >=1 ) write(*,'(1x,91("-"))') 
   if ( dsu < tolrel*su ) then !.and. dsu < tolabs ) then
      if ( verbosity(4) >=1 ) &
         write(*,'(16x,                                                         &
         &     "C o n v e r g e n c e    t e s t    i s    s a t i s f i e d",  &
         &     /,1x,91("-"),/)')
   else    
      if ( verbosity(4) >=1 ) &
         write(*,'(12x,                                                                 &
         &     "c o n v e r g e n c e    t e s t    i s    N O T    s a t i s f i e d", &
         &     /,1x,91("-"),/)')
      stat = err_t( stat = WARNING, where = Here, &
                    msg = 'Convergence test is not satified' )
   end if         
!
!- Postprocessing: compute the gradient vector at each I.P.
!
!                Mat.     Rhs       Str.     Prod.
   request = [ .false. , .false. , .true. , .false. ] ! compute only the derived quantities

   call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                           KFelem  = modelsStokes_KFelemNL        )
   error_TraceNreturn(stat>IZERO, Here, stat)                                                                
!
!- write the solution:
!
   call output_driver ( mydata, mymesh, femsys, stat )
   
   END SUBROUTINE modelsStokes_mainNL
   

!=============================================================================================    
   SUBROUTINE modelsStokes_mainNL2 ( mymesh, mydata, femsys, stat, info )
!=============================================================================================    
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (udata_t ), intent(in out)           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in out), optional :: info(*)
!---------------------------------------------------------------------------------------------
!  Procedure that solves a non-linear Stokes problem
!--------------------------------------------------------------------------------------------- 
!
!  Inputs
!
!       mymesh: mesh structure
!
!       mydata: data (material properties & boundary conditions) structure
!
!       femsys: matrix structures
!
!       init  : logical. 
!               If init==.true.  --> initialization phase (see below)
!               If init==.false. --> solution phase       
!
!  Outputs
!
!       ndime, ndofn, nprop, nstri (in the initialization phase)
!
!       femsys%Sol (in the solution phase)
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'modelsStokes_mainNL2'
   character(len=:), allocatable :: geom
   integer  (Ikind)              :: p, d, i, iter, nitermax, lp, rp, niterPicard
   real     (Rkind)              :: tolrel, tolabs
   real     (Rkind)              :: su, dsu, resu, resu1
   real     (Rkind)              :: sp, dsp, resp, resp1   
   logical                       :: request(4)
!---------------------------------------------------------------------------------------------    

   if ( stat > IZERO ) return 
   
   lp = index(name_model_pb,'(') ; rp = index(name_model_pb,')')
    
   geom = trim(adjustl(name_model_pb(lp+1:rp-1)))
    
   if ( trim(name_class_pb) == 'transient' ) then
      stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'sorry, transient Stokes problem not yet implemented' ) 
      return 
   end if 
    
   if ( present(info) ) then
!
!-    Information/initialization phase. According to the desired model define:
!
!      - ndime: space dimensions
!      - ndofn: nbr of d.o.f.
!      - nprop: nbr of material properties
!      - nstri: nbr of independent stress components 
!  
      if ( geom == '2d' ) then
         ndime = 2          ! 2D
         ndofn = 3          ! 3 dof (ux, uy, p)
         ncmpN = ndime      ! 2 components (Fx, Fy) of traction vector (for Neumann's bc)     
         nstri = 3          ! 3 indep. dev. stresses : sxx, syy, sxy             
         nprop = 10         !10 prop.: see below

         noutp = 2*nstri + 1 ! post-proceded variables: 3 stresses, 3 strain rates, viscosity 
       
      else if ( geom == '3d' ) then
         ndime = 3          ! 3D
         ndofn = 4          ! 4 dof (ux, uy, uz, p)
         ncmpN = ndime      ! 3 components (Fx, Fy, Fz) of traction vector (for Neumann's bc)         
         nstri = 6          ! 6 indep. stresses : sxx, syy, szz, sxy, syz, szx      
         nprop = 11         ! 11 prop.: see below

         noutp = 2*nstri + 1 ! post-proceded variables: 6 stresses, 6 strain rates, viscosity 
          
      else     
         stat = err_t ( stat = UERROR, where = Here, &
                         msg = trim(name_model_pb)//': not a valid model' ) 
         return       
       
      end if
!
!-    messages output
!       
      info( 1) = "Informations from subroutine models_StokesNL2:"
      info( 2) = '   list of parameters for the model "'//trim(name_model_pb)//'":'
      write(info(3),'("  * Space dimension                       : ",i0)')ndime
      write(info(4),'("  * Number of degrees of freedom          : ",i0)')ndofn
      write(info(5),'("  * Number of components for stress vector: ",i0)')ncmpN
      write(info(6),'("  * Number of material properties         : ",i0)')nprop
      write(info(7),'("   and these ",i0," material properties are:")')nprop
      info( 8) = "       (1:1) pre-exponential term: gamma0"
      info( 9) = "       (2:2) exponent: n"
      info(10) = "       (3:3) activation energy: Q"
      info(11) = "       (4:4) Peierls stress: sbar"
      info(12) = "       (5:5) exponent: r"
      info(13) = "       (6:6) exponent: q"
      info(14) = "       (7:7) Cut-off temperature: T"
      info(15) = "       (8:8) regularization parameter: eps"
      if ( geom == '2d' ) then
         info(16) = "      (9:10) body forces: rho*gx, rho*gy"    
      end if
      if ( geom == '3d' ) then       
         info(16) = "      (9:11) body forces: rho*gx, rho*gy, rho*gz"    
      end if 
      info(17) = "   and must be given in this order"
       
      return
      
   end if   

   call setPeierls6
!
!- Resolution phase (Newton's method):
! 
!              Mat.     Rhs      Str.      Prod.
   request = [.true. , .true. , .true. , .false.] ! reform matrix and rhs at each iteration
    
   nitermax = nint(optnlinsol(1))    ! max. nbr of Newton's iterations 
   niterPicard = nint(optnlinsol(4)) ! number of initial Picard's iterations   
   tolrel   = optnlinsol(2)          ! relative tolerance
   tolabs   = optnlinsol(3)          ! absolute tolerance
   
   if ( niterPicard < 0 ) niterPicard = 5   
       
   femsys%Sol = RZERO ; femsys%X = RZERO ; femsys%Str = RZERO ! initialize solution and increment solution
 
   if ( verbosity(4) >=1 )                                                 &
   write(*,'(/,1x,91("-"),/,                                               &
  &      18x,"s t a r t i n g    N e w t o n'' s   i t e r a t i o n s",/, &
  &       1x,91("-"),/," Iter #",4x,"s/iter",6x,"theta",6x,"||dU||/||U||", &
  &       2x,"||Ru||/||Ru0||",2x,"||dP||/||P||",2x,"||Rp||/||Rp0||")')
   
   theta = RZERO 
      
   do iter = 1, nitermax   
   
      maxiter = 0
!
!-    Form the matrix and the rhs:
!
      femsys%X = RZERO

      call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                              KFelem  = modelsStokes_KFelemNL2       )   
                              
      error_TraceNreturn(stat>IZERO, Here, stat)                                 
!
!-    Solve the linear system: compute the solution increment (in femsys%X):
!
      call solverdriver_resol ( femsys, stat, factorize='yes', solve='yes', clean='yes' )
      error_TraceNreturn(stat>IZERO, Here, stat)
!
!-    Update the solution:
!       
      call femsystem_storeNodalValues ( femsys, is_incr = .true. )
!
!-    Stopping criteria (ds/s < tolrel & ds < tolabs):
! 
      sp = RZERO; dsp = RZERO; resp = RZERO; su = RZERO; dsu = RZERO; resu = RZERO
      do p = 1, npoin
         do d = 1, ndofn
            i = femsys%dof(d,p)
            if ( i > 0 ) then
               if ( d == ndofn ) then
                  sp   = sp   + femsys%Sol(d,p)**2
                  dsp  = dsp  + femsys%X(i)    **2
                  resp = resp + femsys%Rhs(i)  **2
               else
                  su   = su   + femsys%Sol(d,p)**2
                  dsu  = dsu  + femsys%X(i)    **2
                  resu = resu + femsys%Rhs(i)  **2
               end if
            end if
         end do
      end do
      sp = sqrt(sp) ; dsp = sqrt(dsp) ; resp = sqrt(resp)
      su = sqrt(su) ; dsu = sqrt(dsu) ; resu = sqrt(resu)

      if ( iter == 1 ) then
         resu1 = resu ; resp1 = resp
      end if
      
      if ( niterPicard > 0 ) then
         if ( iter == niterPicard ) then
            resu1 = resu ; resp1 = resp
         else if ( iter > niterPicard ) then
            theta = max(RZERO, RONE-sqrt(resu/resu1))
         end if  
      else if ( iter > 1 ) then
         theta = max(RZERO, RONE-sqrt(resu/resu1))
      end if

      if ( verbosity(4) >=1 ) write(*,'(3x,i4,6x,i4,1x,5(e13.5,2x))') &
               iter, maxiter, theta, dsu/su, resu/resu1, dsp/sp, resp/resp1

      !if (dsu < tolrel*su .and. dsp < tolrel*sp) exit  
      if ( dsu < tolrel*su .and. dsu < tolabs ) exit
   end do    


   if ( verbosity(4) >=1 ) write(*,'(1x,91("-"))') 
   if ( dsu < tolrel*su .and. dsu < tolabs ) then
      if ( verbosity(4) >=1 ) &
         write(*,'(16x,                                                         &
         &     "C o n v e r g e n c e    t e s t    i s    s a t i s f i e d",  &
         &     /,1x,91("-"),/)')
   else    
      if ( verbosity(4) >=1 ) &
         write(*,'(12x,                                                                 &
         &     "c o n v e r g e n c e    t e s t    i s    N O T    s a t i s f i e d", &
         &     /,1x,91("-"),/)')
      stat = err_t( stat = WARNING, where = Here, &
                    msg = 'Convergence test is not satified' )
   end if              
!
!- Postprocessing: compute the gradient vector at each I.P.
!
!                Mat.     Rhs       Str.     Prod.
   request = [ .false. , .false. , .true. , .false. ] ! compute only the derived quantities

   call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                           KFelem  = modelsStokes_KFelemNL2       )
   
   error_TraceNreturn(stat>IZERO, Here, stat)                                                                
!
!- write the solution:
!
   call output_driver ( mydata, mymesh, femsys, stat, peier )

   CONTAINS
   
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      SUBROUTINE setPeierls 
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

      character(len=200)              :: buf
      integer                         :: i, j, p, e, ndom, idom, n, m, ntot
      real     (Rkind  )              :: frac, r, dpeier, peier_mean
      character(len= : ), allocatable :: cdoms
      integer           , allocatable :: doms(:), nedom(:), eldom(:)      
      
      print*,"Saisir les numeros des domaines de la zone heterogene"
      print*," (separes par des espaces) :"
      read(*,'(a)')buf
1     print*,"Pourcentage (f) d'elements a perturber (<=50) :"
      print*,"(il y aura f % d'elements durs, f % d'elements mous)"
      read(*,*) frac 
      if (frac > 50) then
         print*,'Erreur : f doit etre <= 50. Recommencez'
         goto 1
      end if
      print*,"Valeur absolue de la perturbation (delta Peierls)"
      read(*,*)dpeier
         
      frac = frac * 0.01
      
      cdoms = trim(adjustl(buf))
      
      ndom = 0
      do         
         p = index(cdoms,' ')
         
         if (p /= 0) then
            ndom = ndom + 1
            cdoms = trim(adjustl(cdoms(p+1:)))      
         else if (len_trim(cdoms) /= 0) then
            ndom = ndom + 1
            exit
         else
            exit
         end if
      end do
      
      allocate(doms(ndom), nedom(ndom), source = 0)
      cdoms = trim(adjustl(buf))
      read(cdoms,*)doms(:)
      
      allocate(peier(nelem), source = RZERO)
      
      ! on compte le nombre d'elements presents dans chaque domaines selectionnes :
      
      do e = 1, nelem
         m = mymesh % numdom(e)
         do idom = 1, ndom
            if (m == doms(idom)) nedom(idom) = nedom(idom)+1
         end do
         peier(e) = mydata % vmatprop(4,m) ! initialisation
      end do

      ! tirage aleatoire des frac % d'elements a perturber dans chaque domaine:
      
      do idom = 1, ndom
         n = nedom(idom)
         ! extraction de la liste des # des elements de ce domaine :
         allocate(eldom(n))
         i = 0
         do e = 1, nelem
            if (mymesh % numdom(e) == doms(idom)) then
               i = i+1 ; eldom(i) = e
            end if
         end do 
         ! permutation aleatoire des # des elements du domaine idom :
         do i = n, 2, -1
            call random_number(r)
            j = int(r * i) + 1
            p        = eldom(j)
            eldom(j) = eldom(i)
            eldom(i) = p
         end do              
         ! les frac % premiers elements sont mous, les frac % suivants durs
         ! et les restants sont inchanges
         m = int(frac*n)
         do i = 1, m
            e = eldom(i)
            peier(e) = peier(e) - dpeier
            e = eldom(i+m+1)
            peier(e) = peier(e) + dpeier
         end do
         
         deallocate(eldom)
         
      end do
                     
      END SUBROUTINE setPeierls


   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      SUBROUTINE setPeierls2 
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

      character(len=200)              :: buf
      integer                         :: i, p, e, n, m, pnod, nv
      real     (Rkind  )              :: dpeier, x0, y0, dmin, d
      
      print*,"Perturbation Peierls sur un element et ses voisin"
      print*,"Donnez les coordonnées d'un point :"
      read(*,*)x0,y0
      print*,"Nbr d'elements attaches au noeud le plus proche a perturber"
      read(*,*) n 
      print*,"Valeur absolue de la perturbation (delta Peierls)"
      read(*,*)dpeier
         
      ! recherche du noeud le plus proche de (x0,y0) :
      dmin = (x0-mymesh%coord(1,1))**2 +  (y0-mymesh%coord(2,1))**2
      do p = 2, npoin      
         d = (x0-mymesh%coord(1,p))**2 +  (y0-mymesh%coord(2,p))**2
         if (d < dmin) then
            dmin = d
            pnod = p
         end if
      end do
      
      print*,'noeud le plus proche : ',pnod, (mymesh%coord(1:2,pnod))
            
      allocate(peier(nelem), source = RZERO)
      
      do e = 1, nelem
         m = mymesh % numdom(e)
         peier(e) = mydata % vmatprop(4,m) ! initialisation
      end do
      
      nv = 0
      do i = 1, mymesh % lvale(pnod)
         nv = nv + 1
         e = mymesh % lelem(i,pnod)
         peier(e) = peier(e) - dpeier
         if (nv == n) exit
      end do
      
      print*,'nbr de voisins perturbes : ',nv
         
      END SUBROUTINE setPeierls2


   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      SUBROUTINE setPeierls3
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

      character(len=200)              :: buf
      integer                         :: i, j, p, e, n, m, pnod, nv
      real     (Rkind  )              :: dpeier, r, frac, meanVois
      integer           , allocatable :: lpnod(:), ideja(:), numNode(:), nvois(:) 
      
      print*,"Perturbation Peierls sur les voisinages de noeuds tires aleatoirement"
      print*,"Nbr d'elements dans chaque voisinage"
      read(*,*) n
1     print*,"Pourcentage (f) d'elements a perturber (<=50) :"
      print*,"(il y aura f % d'elements durs, f % d'elements mous)"
      read(*,*) frac 
      if (frac > 50) then
         print*,'Erreur : f doit etre <= 50. Recommencez'
         goto 1
      end if
      
      frac = frac * 0.01      
      
      print*,"Valeur absolue de la perturbation (delta Peierls)"
      read(*,*)dpeier
                     
      allocate(peier(nelem), source = RZERO)
      allocate(ideja(nelem), source = 0)
      
      allocate(numNode(npoin), source = 0)
      
      do e = 1, nelem
         m = mymesh % numdom(e)
         peier(e) = mydata % vmatprop(4,m) ! initialisation
      end do
      
      do i = 1, npoin
         numNode(i) = i
      end do
      
      do i = npoin, 2, -1
         call random_number(r)
         j = int(r * i) + 1
         p          = numNode(j)
         numNode(j) = numNode(i)
         numNode(i) = p
      end do             
       
      m = int(frac*npoin)
  
      allocate(nvois(2*m), source = 0)

      meanVois = RZERO

      ! les m premiers sont mous
      
      do i = 1, m
         nv = 0 
         pnod = numNode(i)
         do j = 1,mymesh % lvale(pnod)
            e = mymesh % lelem(j,pnod)
            if (ideja(e) == 1) cycle
            ideja(e) = 1
            nv = nv + 1
            peier(e) = peier(e) - dpeier
            if  (nv == n) exit
         end do
         nvois(i) = nv
         meanVois = meanVois + nv
      end do
      
      ! les m suivants sont durs
      
      do i = m+1, 2*m
         nv = 0 
         pnod = numNode(i)
         do j = 1,mymesh % lvale(pnod)
            e = mymesh % lelem(j,pnod)
            if (ideja(e) == 1) cycle
            ideja(e) = 1
            nv = nv + 1
            peier(e) = peier(e) + dpeier
            if  (nv == n) exit
         end do
         nvois(i) = nv
         meanVois = meanVois + nv

      end do
      
      meanVois = meanVois / real(2*m,kind=8)
      
      print*,"minVois, maxVois, meanVois :",minval(nvois),maxval(nvois),meanVois
      n = sum(ideja)
      print*,"nbr d'elements perturbes :",n," soit ",(100.0*n)/nelem," %"
      print*,"Peirls moyen :",1e-9*sum(peier)/nelem," GPa"
      n = 0 ; m = 0 ; p = 0
      do e = 1, nelem
         if (peier(e) > 2.0e9) then
            p = p + 1
         else if (peier(e) < 2.0e9) then
            m = m + 1
         else
            n = n + 1
         end if
      end do
      print*,"nbr elements durs",p," soit ", (100.0*p)/nelem," %"
      print*,"nbr elements mous",m," soit ", (100.0*m)/nelem," %"
      print*,"le reste         ",n," soit ", (100.0*n)/nelem," %"
      
      END SUBROUTINE setPeierls3


   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      SUBROUTINE setPeierls4
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

      character(len=200)              :: buf
      integer                         :: i, j, k, in, p, e, e0, n, m, pnod, nv
      real     (Rkind  )              :: dpeier, r, frac, meanVois
      integer           , allocatable :: lpnod(:), ideja(:), numElem(:), nvois(:) 
      logical                         :: pas_touche
      
      print*,"Perturbation Peierls sur des voisinage d'elements"
      print*,"Nbr d'elements dans chaque voisinage"
      read(*,*) n
1     print*,"Pourcentage (f) d'elements a perturber (<=50) :"
      print*,"(il y aura f % d'elements durs, f % d'elements mous)"
      read(*,*) frac 
      if (frac > 50) then
         print*,'Erreur : f doit etre <= 50. Recommencez'
         goto 1
      end if
      
      frac = frac * 0.01      
      
      print*,"Valeur absolue de la perturbation (delta Peierls)"
      read(*,*)dpeier
                     
      allocate(peier(nelem), source = RZERO)
      allocate(ideja(nelem), source = 0)
      
      allocate(numElem(nelem), source = 0)
      
      do e = 1, nelem
         m = mymesh % numdom(e)
         peier(e) = mydata % vmatprop(4,m) ! initialisation
      end do
      
      do i = 1, nelem
         numElem(i) = i
      end do
      
      do i = nelem, 2, -1
         call random_number(r)
         j = int(r * i) + 1
         p          = numElem(j)
         numElem(j) = numElem(i)
         numElem(i) = p
      end do             
       
      m = int(frac*nelem)
  
      allocate(nvois(2*m), source = 0)

      meanVois = RZERO

      ! les m premiers sont mous
      
      do i = 1, m
         nv = 0 
         e0 = numElem(i)
         if (ideja(e0) == 1) cycle
         ideja(e0) = 1
         peier(e0) = peier(e0) - dpeier
         nv = 1
         if (n > 1) then
            do in = 1, nnode
               pnod = mymesh % lnods(in,e0)
               do j = 1, mymesh % lvale(pnod)
                  e = mymesh % lelem(j,pnod)
                  if (ideja(e) == 1) cycle
                  pas_touche = .false.
                  do k = m+1, 2*m
                     if (numElem(k) == e) then
                        pas_touche = .true.
                        exit
                     end if
                  end do
                  if (pas_touche) cycle
                  ideja(e) = 1
                  nv = nv + 1
                  peier(e) = peier(e) - dpeier
                  if  (nv == n) exit
               end do
               if (nv == n) exit
            end do
         end if
         nvois(i) = nv
         meanVois = meanVois + nv
      end do
      
      ! les m suivants sont durs
      
      do i = m+1, 2*m
         nv = 0 
         e0 = numElem(i)
         if (ideja(e0) == 1) cycle         
         ideja(e0) = 1
         peier(e0) = peier(e0) + dpeier
         nv = 1
         if (n > 1) then
            do in = 1, nnode
               pnod = mymesh % lnods(in,e0)
               do j = 1,mymesh % lvale(pnod)
                  e = mymesh % lelem(j,pnod)
                  if (ideja(e) == 1) cycle
                  ideja(e) = 1
                  nv = nv + 1
                  peier(e) = peier(e) + dpeier
                  if  (nv == n) exit
               end do
               if (nv == n) exit
            end do
         end if
         nvois(i) = nv
         meanVois = meanVois + nv
      end do
      
      meanVois = meanVois / real(2*m,kind=8)
      
      print*,"minVois, maxVois, meanVois :",minval(nvois),maxval(nvois),meanVois
      n = sum(ideja)
      print*,"nbr d'elements perturbes :",n," soit ",(100.0*n)/nelem," %"
      print*,"Peirls moyen :",1e-9*sum(peier)/nelem," GPa"
      n = 0 ; m = 0 ; p = 0
      do e = 1, nelem
         if (peier(e) > 2.0e9) then
            p = p + 1
         else if (peier(e) < 2.0e9) then
            m = m + 1
         else
            n = n + 1
         end if
      end do
      print*,"nbr elements durs",p," soit ", (100.0*p)/nelem," %"
      print*,"nbr elements mous",m," soit ", (100.0*m)/nelem," %"
      print*,"le reste         ",n," soit ", (100.0*n)/nelem," %"
      
      END SUBROUTINE setPeierls4


   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      SUBROUTINE setPeierls5
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

      character(len=200)              :: buf
      integer                         :: i, j, k, in, jn, kn, p, e, e0, n, m, pnod, qnod, nv
      real     (Rkind  )              :: dpeier, r, frac, meanVois
      integer           , allocatable :: lpnod(:), ideja(:), numElem(:), nvois(:), seed(:)
      logical                         :: pas_touche, is_ok
      integer                         :: connec, time(8), nsize
      real     (Rkind  ), allocatable :: rnum(:)
      
      print*,"Perturbation Peierls sur des voisinage d'elements"
      print*,"Nbr d'elements dans chaque voisinage"
      read(*,*) n
      print*,"Restreindre aux elements partageant une même face (0/1)"
      read(*,*) connec
1     print*,"Pourcentage (f) d'elements a perturber (<=50) :"
      print*,"(il y aura f % d'elements durs, f % d'elements mous)"
      read(*,*) frac 
      if (frac > 50) then
         print*,'Erreur : f doit etre <= 50. Recommencez'
         goto 1
      end if
      
      frac = frac * 0.01      
      
      print*,"Valeur absolue de la perturbation (delta Peierls)"
      read(*,*)dpeier
                     
      allocate(peier(nelem), source = RZERO)
      allocate(ideja(nelem), source = 0)
      
      allocate(numElem(nelem), source = 0)
      
      do e = 1, nelem
         m = mymesh % numdom(e)
         peier(e) = mydata % vmatprop(4,m) ! initialisation
      end do
      
      do i = 1, nelem
         numElem(i) = i
      end do

      call date_and_time(values=time)
      call random_seed(size=nsize) ; allocate(seed(1:nsize))
      seed(1) = time(8) ; seed(2:nsize) = time(1)*time(8)
      call random_seed(put=seed)
      allocate(rnum(nelem))
      call random_number(harvest=rnum)
      
      do i = nelem, 2, -1
         !call random_number(r)
         !j = int(r * i) + 1
         j = int(rnum(i) * i) + 1
         p          = numElem(j)
         numElem(j) = numElem(i)
         numElem(i) = p
      end do             
       
      m = int(frac*nelem)
  
      allocate(nvois(2*m), source = 0)

      meanVois = RZERO

      ! les m premiers sont mous
      
      do i = 1, m
         nv = 0 
         e0 = numElem(i)
         if (ideja(e0) == 1) cycle
         ideja(e0) = 1
         peier(e0) = peier(e0) - dpeier
         nv = 1
         if (n > 1) then
            do in = 1, nnode
               pnod = mymesh % lnods(in,e0)
               do j = 1, mymesh % lvale(pnod)
                  e = mymesh % lelem(j,pnod)
                  if (ideja(e) == 1) cycle
                  pas_touche = .false.
                  do k = m+1, 2*m
                     if (numElem(k) == e) then
                        pas_touche = .true.
                        exit
                     end if
                  end do
                  if (pas_touche) cycle
                  
                  if (connec == 1) then
                     do jn = 1, nnode
                        qnod = mymesh % lnods(jn,e)
                        if (qnod == pnod) cycle
                        is_ok = .false.
                        do kn = 1, nnode
                           if (mymesh % lnods(kn,e0) == pnod) cycle
                           if (mymesh % lnods(kn,e0) == qnod) then
                              is_ok = .true.
                              exit
                           end if
                        end do
                        if (is_ok) exit
                     end do
                     if (.not. is_ok) cycle 
                  end if
                  
                  ideja(e) = 1
                  nv = nv + 1
                  peier(e) = peier(e) - dpeier
                  if  (nv == n) exit
               end do
               if (nv == n) exit
            end do
         end if
         nvois(i) = nv
         meanVois = meanVois + nv
      end do
      
      ! les m suivants sont durs
      
      do i = m+1, 2*m
         nv = 0 
         e0 = numElem(i)
         if (ideja(e0) == 1) cycle         
         ideja(e0) = 1
         peier(e0) = peier(e0) + dpeier
         nv = 1
         if (n > 1) then
            do in = 1, nnode
               pnod = mymesh % lnods(in,e0)
               do j = 1,mymesh % lvale(pnod)
                  e = mymesh % lelem(j,pnod)
                  if (ideja(e) == 1) cycle
                  
                  if (connec == 1) then
                     do jn = 1, nnode
                        qnod = mymesh % lnods(jn,e)
                        if (qnod == pnod) cycle
                        is_ok = .false.
                        do kn = 1, nnode
                           if (mymesh % lnods(kn,e0) == pnod) cycle
                           if (mymesh % lnods(kn,e0) == qnod) then
                              is_ok = .true.
                              exit
                           end if
                        end do
                        if (is_ok) exit
                     end do
                     if (.not. is_ok) cycle   
                  end if
                  
                  ideja(e) = 1
                  nv = nv + 1
                  peier(e) = peier(e) + dpeier
                  if  (nv == n) exit
               end do
               if (nv == n) exit
            end do
         end if
         nvois(i) = nv
         meanVois = meanVois + nv
      end do
      
      meanVois = meanVois / real(2*m,kind=8)
      
      print*,"minVois, maxVois, meanVois :",minval(nvois),maxval(nvois),meanVois
      n = sum(ideja)
      print*,"nbr d'elements perturbes :",n," soit ",(100.0*n)/nelem," %"
      print*,"Peirls moyen :",1e-9*sum(peier)/nelem," GPa"
      n = 0 ; m = 0 ; p = 0
      do e = 1, nelem
         if (peier(e) > 2.0e9) then
            p = p + 1
         else if (peier(e) < 2.0e9) then
            m = m + 1
         else
            n = n + 1
         end if
      end do
      print*,"nbr elements durs",p," soit ", (100.0*p)/nelem," %"
      print*,"nbr elements mous",m," soit ", (100.0*m)/nelem," %"
      print*,"le reste         ",n," soit ", (100.0*n)/nelem," %"
      
      END SUBROUTINE setPeierls5


   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      SUBROUTINE setPeierls6
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

      character(len=200)              :: buf
      integer                         :: i, j, p, e, n, m, pnod, nv
      real     (Rkind  )              :: dpeier, r, frac, xnode
      integer           , allocatable :: numNode(:)
      real     (Rkind  ), allocatable :: peier_nodes(:)
      
      print*,"Perturbation Peierls aux noeuds"
1     print*,"Pourcentage (f) de noeuds a perturber (<=50) :"
      print*,"(il y aura f % de noeuds durs, f % de noeuds mous)"
      read(*,*) frac 
      if (frac > 50) then
         print*,'Erreur : f doit etre <= 50. Recommencez'
         goto 1
      end if
      
      frac = frac * 0.01      
      
      print*,"Valeur absolue de la perturbation (delta Peierls)"
      read(*,*)dpeier
                     
      allocate(peier(nelem), source = RZERO)
      allocate(peier_nodes(npoin), source = RZERO)
      
      allocate(numNode(npoin), source = 0)
      
      do e = 1, nelem
         m = mymesh % numdom(e)
         do i = 1, nnode
            pnod = mymesh % lnods(i,e)
            peier_nodes(pnod) = mydata % vmatprop(4,m) ! initialisation
         end do
      end do
      
      do i = 1, npoin
         numNode(i) = i
      end do
      
      do i = npoin, 2, -1
         call random_number(r)
         j = int(r * i) + 1
         p          = numNode(j)
         numNode(j) = numNode(i)
         numNode(i) = p
      end do             
       
      m = int(frac*npoin)
  
      ! les m premiers sont mous
      
      do i = 1, m
         pnod = numNode(i)
         peier_nodes(pnod) = peier_nodes(pnod) - dpeier
      end do
      
      ! les m suivants sont durs
      
      do i = m+1, 2*m
         pnod = numNode(i)
         peier_nodes(pnod) = peier_nodes(pnod) + dpeier
      end do
          
      xnode = 1.0d0 / real(nnode,kind=8)
      
      p = 0 ; m = 0 ; n = 0
      
      do e = 1, nelem
         peier(e) = RZERO
         do i = 1, nnode
            pnod = mymesh % lnods(i,e)
            peier(e) = peier(e) + peier_nodes(pnod)
         end do
         peier(e) = xnode*peier(e) 
         if (peier(e) < 1e9) peier(e) = 1e9
         if (peier(e) > 3e9) peier(e) = 3e9
         
         if (peier(e) > 2.0e9) then
            p = p + 1
         else if (peier(e) < 2.0e9) then
            m = m + 1
         else
            n = n + 1
         end if
      end do
      
      print*,"nbr elements durs",p," soit ", (100.0*p)/nelem," %"
      print*,"nbr elements mous",m," soit ", (100.0*m)/nelem," %"
      print*,"le reste         ",n," soit ", (100.0*n)/nelem," %"
      
      END SUBROUTINE setPeierls6

      
   END SUBROUTINE modelsStokes_mainNL2
            
        
!=============================================================================================
   SUBROUTINE modelsStokes_KFelem ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F and/or 
!  stresses and strains eldata%S for a given element (# eldata%el)
!
!                     +-----------------------------------------------+  
!                     |           LINEAR 2D/3D STOKES CASE            |
!                     +-----------------------------------------------+
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!                    props(1:1) = viscosity, props(2:)=body forces
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!  Notice
!
!   * For this model, note that
!       - ndime: = 2 or 3     , space dimensions
!       - nprop: = 3 or 4     , nbr of properties 
!       - ndofn: = 3 or 4     , nbr of dof per node (ndime velocities, 1 pressure)
!       - nstri: = 3 or 6     , nbr independent deviatoric stresses
!       - nevab: = ndofn*nnode, nbr of dof per element
!       - nevav: = ndime*nnode, nbr of velocity dof per element
!       - nevap: = 1*nnode    , nbr of pressure dof per element
!
!   * The components of the stress tensor (sigma) and the strain-rate (epsil) are stored as:
!
!      . stress = [sigma_xx  sigma_yy  sqrt(2)*sigma_xy < sqrt(2)*sigma_yz sqrt(2)*sigma_zx >]
!      . strain = [epsil_xx  epsil_yy  sqrt(2)*epsil_xy < sqrt(2)*epsil_yz sqrt(2)*epsil_zx >]
!
!     and the element nodal variables (u,p) as 
!
!      . U = [u1_x u1_y <u1_z> p1 | u2_x u2_y <u2_z> p2 | ...  | un_x un_y <un_z> pn] 
!
!     (n = nnode = nbr of nodes). With this convention   
!
!          . sigma : epsil = stress' * strain
!     
!          . strain = Be * U
!
!          . stress = 2 * visco * strain 
!
!     where visco is the viscosity and Be contains the shape function derivatives at I.P.
!
!   * The body force components are given by 
!
!                    volsrc = [fx,fy <,fz>] = props(2:3 <4>)  
!
!     their contribution to the element load vector is thus F = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = 'modelsStokes_KFelem'
   integer  (Ikind), parameter :: perm(3) = [2,3,1]
   real     (Rkind), parameter :: Two  = 2.0_Rkind
   real     (Rkind), parameter :: sqrt2i = RONE / sqrt(two)

   real     (Rkind)            :: Be (nstri,ndime*nnode), Bdiv(1,ndime*nnode),    &
                                  Nev(ndime*nnode,ndime), dphi(ndime,nnode  ),    &
                                  Nep(nnode  ,1        )

   real     (Rkind)            :: strai(nstri),  volsrc(ndime)

   real     (Rkind)            :: dv, eljac, visco, twovisco, velem, q, stab

   integer  (Ikind)            :: dofp(nnode), dofv(ndime*nnode)

   integer  (Ikind)            :: i, j, k, ip, nevav, nevap, meth   
!---------------------------------------------------------------------------------------------          

   if ( rqst(1) .or. rqst(2) ) then
!
!-    selected method of stabilization 
!     (0: by L2 projection (default), 1: by pressure laplacian)
!
      meth = nint(optmod(1))
   
      if ( meth /= 0 .and. meth /= 1 ) then
         stat = err_t ( stat=UERROR, where=Here, msg='invalid stabilization option = ' // &
                        util_intToChar(meth) )
         return
      end if   

      if ( meth == 0 ) q = RONE / real(nnode,kind=Rkind)

      eldata%K(:,:) = RZERO ; eldata%F(:) = RZERO    

      if ( rqst(2) ) then
         Nev(:,:) = RZERO
!
!-       volscr vector (body forces, assumed constant):
!  
         volsrc = eldata%props(2:nprop)          
      end if    
   end if
   
   if ( rqst(1) .or. rqst(3) ) Be(:,:) = RZERO   
!
!- local velocity dof #: 
! 
   nevav = ndime*nnode
   dofv(1:nevav) = [  ((ndofn*(j-1)+i, i=1, ndime), j=1, nnode)  ]   
!
!- local pressure dof #: 
!   
   nevap = nnode   
   dofp(1:nevap) = [  (ndofn*j, j=1, nnode)  ]     
   
   visco = eldata%props(1) ; twovisco = Two * visco        
!
!- loop over the I.P. (integration phase):
!          
   velem = RZERO

   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives (dphi) at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), dphi, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if  
      
      dv = eljac * eldata%wIP(ip)
      
      if ( rqst(1) .or. rqst(3) ) then
!    
!-       B matrix (3d or plane-strain):
!
         do i = 1, ndime
            Be(i, i:nevav:ndime) = dphi(i,:)
         end do    
        
         j = 0
         do i = ndofn, nstri
            j = j + 1; k = perm(j)
            Be(i, j:nevav:ndime) = sqrt2i * dphi(k,:)
            Be(i, k:nevav:ndime) = sqrt2i * dphi(j,:)
         end do    

         if ( rqst(1) ) then

            Bdiv(1,1:nevav) = [ ((dphi(i,j), i=1,ndime), j = 1, nnode) ] 
         
            Nep(:,1) = eldata%shp(:,ip)            
!
!-          Stiffness matrix: velocity-velocity sub-matrix
!         
            eldata%K(dofv,dofv) = eldata%K(dofv,dofv) + dv * twovisco*matmul(transpose(Be),Be)  
!
!-          Stiffness matrix: pressure-velocity sub-matrix                               
!
            eldata%K(dofp,dofv) = eldata%K(dofp,dofv) + dv * matmul ( Nep, Bdiv )
!
!-          Stiffness matrix: pressure-pressure sub-matrix (stabilization term)                             
!           (modified mass matrix if meth=0 or laplacian matrix if meth=1)
!
            if ( meth == 0 ) then
               eldata%K(dofp,dofp) = eldata%K(dofp,dofp) - dv * matmul(Nep, transpose(Nep)-q)           
            else
               eldata%K(dofp,dofp) = eldata%K(dofp,dofp) - dv * matmul(transpose(dphi), dphi)
            end if                          
         else
!
!-          compute the derived quantities (stresses components)
!          
            strai = matmul ( Be, eldata%U(dofv) )
            
            eldata%S(      1:nstri  ,ip) = strai
            eldata%S(nstri+1:2*nstri,ip) = twovisco * strai 
            eldata%S(noutp          ,ip) = visco
         end if
             
      end if
             
      if ( rqst(2) ) then 
!
!-       N matrix:
!             
         do i = 1, ndime
            Nev(i:nevav:ndime,i) = eldata%shp(:,ip)
         end do   
!
!-       load vector (body forces):
!               
         eldata%F(dofv) = eldata%F(dofv) + dv * matmul ( Nev, volsrc )    
!
!-       stabilization term (for meth=1 only):
!         
         if ( meth == 1 ) eldata%F(dofp) = eldata%F(dofp) + dv*matmul(transpose(dphi),volsrc)
      end if
      
      velem = velem + dv  ! element volume or surface
       
   end do   
   
   if ( rqst(1) .or. rqst(2) ) then
   
      if ( meth == 0 ) then
         stab = RONE / visco
      else
         stab = velem / visco
         if ( name_geometry == 'triangle'  ) stab = stab * 4.0_Rkind / sqrt(3.0_Rkind)
         if ( name_geometry == 'tetraedre' ) stab = stab * 6.0_Rkind * sqrt(2.0_Rkind)
      end if   
     
      if ( rqst(1) ) then   
         eldata%K(dofp,dofp) = eldata%K(dofp,dofp) * stab
         eldata%K(dofv,dofp) = transpose( eldata%K(dofp,dofv) )
      end if    
   
      if ( rqst(2) .and. meth == 1 ) eldata%F(dofp) = eldata%F(dofp) * stab 
   end if
   
   END SUBROUTINE modelsStokes_KFelem


!=============================================================================================
   SUBROUTINE modelsStokes_KFelemNL ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F (residual) 
!  stresses and strains eldata%S for a given element (# eldata%el)
!
!                     +-----------------------------------------------+  
!                     |         NON-LINEAR 2D/3D STOKES CASE          |
!                     +-----------------------------------------------+
!
!  The rheological model is the power law:
!
!                  stress = sigma + p * I,  sigma = 2 * visco * epsil
!
!                  with  visco := beta * norm(epsil)^alpha
!
!  where sigma is the stress deviator, epsil is the strain-rate tensor (traceless), and alpha
!  and beta are two constants (alpha = 1/n - 1, n >= 1).
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!                    . props(1:1) = beta,
!                    . props(2:2) = exponent n,
!                    . props(3:3) = regularization parameter,
!                    . props(4:$) = body force components
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!  Notice
!
!   * For this model, note that
!       . ndime: = 2 or 3     , space dimensions
!       . nprop: = 5 or 6     , nbr of properties 
!       . ndofn: = 3 or 4     , nbr of dof per node (ndime velocities, 1 pressure)
!       . nstri: = 3 or 6     , nbr independent deviatoric stresses
!       . nevab: = ndofn*nnode, nbr of dof per element
!       . nevav: = ndime*nnode, nbr of velocity dof per element
!       . nevap: = 1*nnode    , nbr of pressure dof per element
!
!   * The components of the deviatoric stress tensor (sigma) and the strain-rate (epsil) are
!     stored as:
!
!      . stress = [sigma_xx  sigma_yy  sqrt(2)*sigma_xy < sqrt(2)*sigma_yz sqrt(2)*sigma_zx >]
!      . strain = [epsil_xx  epsil_yy  sqrt(2)*epsil_xy < sqrt(2)*epsil_yz sqrt(2)*epsil_zx >]
!
!     and the element nodal variables (u,p) as 
!
!      . U = [u1_x u1_y <u1_z> p1 | u2_x u2_y <u2_z> p2 | ...  | un_x un_y <un_z> pn] 
!
!     (n = nnode = nbr of nodes). With this convention   
!
!          . sigma : epsil = stress' * strain
!     
!          . strain = Be * U
!
!          . stress = 2 * visco * strain 
!
!     where visco is the effective viscosity and Be contains the shape function derivatives at
!     the integration points (I.P.)
!
!   * The body force components are given by 
!
!                    volsrc = [fx fy <fz>] = eldata%props(4:)  
!
!     their contribution to the element load vector is thus F = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: here = 'modelsStokes_KFelemNL'
   
   real     (Rkind), parameter :: Two  = 2.0_Rkind
                                  
   real     (Rkind), parameter :: sqrt2i = RONE / sqrt(Two)

   integer  (Ikind), parameter :: perm(3) = [2,3,1]

   real     (Rkind)            :: Be (nstri      ,ndime*nnode), Bdiv(1          ,ndime*nnode), &
                                  Nev(ndime*nnode,ndime      ), dphi(ndime      ,nnode      ), &
                                  Nep(nnode      ,1          )
                                  
   real     (Rkind)            :: K0 (ndime*nnode,ndime*nnode), K1  (ndime*nnode,ndime*nnode)
                                                       
   real     (Rkind)            :: strai(nstri),  volsrc(ndime)
   
   real     (Rkind)            :: dv, eljac, visco, meanVisco, norme2, alpha, regul, beta,  &
                                  velem, stab, qnode, coefa, coefb, Beki  
                                   
   integer  (Ikind)            :: i, j, k, l, ip, in, id, iv, meth, nevav, nevap
      
   integer  (Ikind)            :: dofp(nnode), dofv(ndime*nnode)
!---------------------------------------------------------------------------------------------          

   if ( rqst(1) .or. rqst(2) ) then
!
!-    selected method of stabilization 
!     (0: by L2 projection (default), 1: by pressure laplacian)
!
      meth = nint(optmod(1))
      
      if ( meth /= 0 .and. meth /= 1 ) then
         stat = err_t ( stat=UERROR, where=Here, msg='invalid stabilization option = ' // &
                        util_intToChar(meth) )
         return
      end if   
      
      if ( meth == 0 ) qnode = RONE / real(nnode,kind=Rkind)

      eldata%K(:,:) = RZERO ; eldata%F(:) = RZERO ; K0 (:,:) = RZERO ; K1 (:,:) = RZERO    

      if ( rqst(2) ) then
         Nev(:,:) = RZERO
!
!-       volscr vector (body forces, assumed constant):
!  
         volsrc = eldata%props(4:nprop)          
      end if    
   end if 
   
   Be(:,:) = RZERO
!
!- local velocity dof # (dofv) and local pressure dof # (dofp)
! 
   nevav = ndime*nnode
   dofv(1:nevav) = [  ((ndofn*(j-1)+i, i=1, ndime), j=1, nnode)  ]   
   
   nevap = nnode   
   dofp(1:nevap) = [  (ndofn*j, j=1, nnode)  ]    
         
   beta  = eldata%props(1)
   regul = eldata%props(3)
   alpha = RONE / eldata%props(2) - RONE 
!
!- loop over the I.P. (integration phase):
!          
   velem = RZERO ; meanVisco = RZERO
   
   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives (dphi) at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), dphi, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if  
      
      dv = eljac * eldata%wIP(ip)
!    
!-    B matrix (3d or plane-strain):
!
      do i = 1, ndime
         Be(i, i:nevav:ndime) = dphi(i,:)
      end do    
        
      j = 0
      do i = ndofn, nstri
         j = j + 1; k = perm(j)
         Be(i, j:nevav:ndime) = sqrt2i * dphi(k,:)
         Be(i, k:nevav:ndime) = sqrt2i * dphi(j,:)
      end do
!
!-    compute the strain rate from the previous step and the viscosity:
!     
      strai = matmul ( Be, eldata%U(dofv) )
                  
      norme2 = dot_product(strai,strai) + regul**2 ! square of the strain-rate + regul**2
      visco  = beta * (sqrt(norme2))**(alpha)

      if ( rqst(3) ) then
!
!-       compute the derived quantities: e = B*u, s = 2*visco*e (strain and stress components)
!          
         strai = matmul (Be, eldata%U(dofv) )
            
         eldata%S(1:nstri        , ip) = strai 
         eldata%S(nstri+1:2*nstri, ip) = Two * visco * strai 
         eldata%S(2*nstri+1      , ip) = visco 
                 
      end if
      
      if ( rqst(1) .or. rqst(2) ) then
!
!-       stiffness matrix: contribution to the velocity-velocity sub-matrix
!        (K0 = coefa*B'*B  and  K1 = coefb * B'*(e*e')*B):
!         
         coefa = dv * Two * visco
         coefb = coefa * (theta * alpha / norme2)  
         
         do j = 1, nevav
            do i = 1, nevav
               do k = 1, nstri
                  Beki = Be(k,i)
                  K0(i,j) = K0(i,j) + coefa*Beki*Be(k,j) 
                  if ( rqst(1) ) then
                     do l = 1, nstri
                        K1(i,j) = K1(i,j) + coefb*strai(k)*strai(l)*Beki*Be(l,j)
                     end do
                  end if
               end do
            end do
         end do
!
!-       Stiffness matrix: pressure-velocity sub-matrix                               
!                  
         Bdiv(1,1:nevav) = [ ((dphi(i,j), i=1,ndime), j = 1, nnode) ] 
         
         Nep(:,1) = eldata%shp(:,ip)
         
         eldata%K(dofp,dofv) = eldata%K(dofp,dofv) + dv * matmul ( Nep, Bdiv )
!
!-       Stiffness matrix: pressure-pressure sub-matrix (stabilization term)                             
!        (modified mass matrix if meth=0 or laplacian matrix if meth=1)
!
         if ( meth == 0 ) then
            eldata%K(dofp,dofp) = eldata%K(dofp,dofp) - dv * matmul(Nep, transpose(Nep)-qnode)
         else if ( meth == 1 ) then
            eldata%K(dofp,dofp) = eldata%K(dofp,dofp) - dv * matmul( transpose(dphi), dphi )
         endif
             
         if ( rqst(2) ) then 
!
!-          load vector (body forces):
!                        
            do i = 1, ndime
               Nev(i:nevav:ndime,i) = eldata%shp(:,ip)
            end do   

            eldata%F(dofv) = eldata%F(dofv) + dv * matmul ( Nev, volsrc )    
!
!-          stabilization term (for meth=1 only):
!         
            if ( meth == 1 ) eldata%F(dofp) = eldata%F(dofp) + &
                                              dv * matmul ( transpose(dphi), volsrc )
         end if
         
      end if
      
      velem = velem + dv  ! element volume or surface
      meanVisco = meanVisco + dv * visco                                
      
   end do   

   if ( rqst(1) .or. rqst(2) ) then

      meanVisco = meanVisco / velem
   
      if ( meth == 0 ) then
         stab = RONE / meanVisco
      else if ( meth == 1 ) then
         stab = 0.333333*velem / meanVisco
         if ( name_geometry == 'triangle'  ) stab = stab * 4.0_Rkind / sqrt(3.0_Rkind)
         if ( name_geometry == 'tetraedre' ) stab = stab * 6.0_Rkind * sqrt(2.0_Rkind)
      end if   
     
      eldata%K(dofp,dofp) = eldata%K(dofp,dofp) * stab
      eldata%K(dofv,dofp) = transpose( eldata%K(dofp,dofv) )
      eldata%K(dofv,dofv) = K0                
   
      if ( rqst(2) ) then
         if ( meth == 1 ) eldata%F(dofp) = eldata%F(dofp) * stab
!
!-       Elemental residual (without the Neumann bc, already computed and assembled):
!
         eldata%F = eldata%F - matmul ( eldata%K, eldata%U ) 
!         
!-       Set 0 Dirichlet bc since %U is an increment:    
!                                                
         do iv = 1, nevab
            if ( eldata%dof(iv) < 0 ) eldata%U(iv) = RZERO
         end do
         
      end if
      
      if ( rqst(1) ) eldata%K(dofv,dofv) = eldata%K(dofv,dofv) + K1
   end if
   
   END SUBROUTINE modelsStokes_KFelemNL


!=============================================================================================
   SUBROUTINE modelsStokes_KFelemNL2 ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F (residual) 
!  and/or stresses and strains eldata%S for a given element (# eldata%el)
!
!                     +-----------------------------------------------+  
!                     |         NON-LINEAR 2D/3D STOKES CASE          |
!                     +-----------------------------------------------+
!
!  The rheological model is of the form:
!
!                  stress = sigma + p * I,  sigma = 2 * visco * epsil
!
!                  with  visco := beta(sigma) * norm(epsil)^alpha
!
!  where sigma is the stress deviator, epsil is the strain-rate tensor (traceless), alpha is a
!  constant (alpha = 1/n - 1, n >= 1) and
!
!              beta(sigma) = 0.5*(1.5)^(alpha/2) * [g0*exp{-(Q/RT)*(1-(seq/s)^r)^q}]^(-1/n)
!
!  where g0, Q, R, T, r, q, s are material constants (see below) and seq is the Von Mises 
!  equivalent stress: seq = sqrt(1.5)*norm(sigma)
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!              . props(1:1) = pre-exponential term g0,
!              . props(2:2) = exponent n,
!              . props(3:3) = Energy activation (Q),
!              . props(4:4) = Peierls's stress (s)
!              . props(5:5) = exponent r
!              . props(6:6) = exponent q
!              . props(7:7) = temperature (T)
!              . props(8:8) = regularization parameter
!              . props(9:$) = body force components
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element residual vector (without the
!                                         contribution of Neumann bc )
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!  Notice
!
!   * For this model, note that
!       . ndime: = 2 or 3     , space dimensions
!       . nprop: = 10 or 11   , nbr of properties 
!       . ndofn: = 3 or 4     , nbr of dof per node (ndime velocities, 1 pressure)
!       . nstri: = 3 or 6     , nbr independent deviatoric stresses
!       . nevab: = ndofn*nnode, nbr of dof per element
!       . nevav: = ndime*nnode, nbr of velocity dof per element
!       . nevap: = 1*nnode    , nbr of pressure dof per element
!
!   * The components of the stress tensor (sigma) and the strain-rate (epsil) are stored as:
!
!      . stress = [sigma_xx  sigma_yy  sqrt(2)*sigma_xy < sqrt(2)*sigma_yz sqrt(2)*sigma_zx >]
!      . strain = [epsil_xx  epsil_yy  sqrt(2)*epsil_xy < sqrt(2)*epsil_yz sqrt(2)*epsil_zx >]
!
!     and the element nodal variables (u,p) as 
!
!      . U = [u1_x u1_y <u1_z> p1 | u2_x u2_y <u2_z> p2 | ...  | un_x un_y <un_z> pn] 
!
!     (n = nnode = nbr of nodes). With this convention   
!
!          . sigma : epsil = stress' * strain
!     
!          . strain = Be * U
!
!          . stress = 2 * visco * strain 
!
!     where visco is the effective viscosity and Be contains the shape function derivatives at
!     the integration points (I.P.).
!
!
!   * The body force components are given by 
!
!                    volsrc = [fx fy <fz>] = eldata%props(9:)  
!
!     their contribution to the element load vector is thus F = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: Here = 'modelsStokes_KFelemNL2'   
   
   real     (Rkind), parameter :: half      = 0.50_Rkind, &
                                  two       = 2.00_Rkind, &
                                  threehalf = 1.50_Rkind, &
                                  Rgas      = 8.31_Rkind, &
                                  tol       = 1e-4_Rkind
                                  
   real     (Rkind), parameter :: sqrt2i = RONE / sqrt(Two)

   integer  (Ikind), parameter :: perm(3) = [2,3,1], nitermax = 10

   real     (Rkind)            :: Be (nstri      ,ndime*nnode), Bdiv(1          ,ndime*nnode), &
                                  Nev(ndime*nnode,ndime      ), dphi(ndime      ,nnode      ), &
                                  Nep(nnode      ,1          )
                                  
   real     (Rkind)            :: K0 (ndime*nnode,ndime*nnode), K1  (ndime*nnode,ndime*nnode)
                                                       
   real     (Rkind)            :: strai(nstri),  volsrc(ndime)
   
   real     (Rkind)            :: dv, eljac, visco, norme2, alpha, regul, beta, gamma0, Eacti, &
                                  sbar, nPower, rPower, qPower, velem, meanVisco, qnode, stab, &
                                  coefa, coefb, Beki, Tkelv, nPowerInv, epsilEq, sigmaEq, QRT, &
                                  sbarInv, cst1, cst2, QRTn,n1, r1, q1, lambda, lambdap
                                  
   integer  (Ikind)            :: i, j, k, l, ip, in, id, iv, meth, nevav, nevap
      
   integer  (Ikind)            :: dofp(nnode), dofv(ndime*nnode)                        
!---------------------------------------------------------------------------------------------          

!
!- selected method of stabilization 
!  (0: by L2 projection (default), 1: by pressure laplacian)
!
   meth = nint(optmod(1))
      
   if ( meth /= 0 .and. meth /= 1 ) then
      stat = err_t ( stat=UERROR, where=Here, msg='invalid stabilization option = ' // &
                     util_intToChar(meth) )
      return
   end if   

   if ( rqst(1) .or. rqst(2) ) then
      eldata%K(:,:) = RZERO ; eldata%F(:) = RZERO
      K0 (:,:) = RZERO
      K1 (:,:) = RZERO
   end if
!
!- local velocity dof # (dofv) and local pressure dof # (dofp)
! 
   nevav = ndime*nnode
   dofv(1:nevav) = [  ((ndofn*(j-1)+i, i=1, ndime), j=1, nnode)  ]   
   nevap = nnode   
   dofp(1:nevap) = [  (ndofn*j, j=1, nnode)  ]    
         
   if ( rqst(1) .or. rqst(3)  ) then
      gamma0 = eldata%props(1)
      nPower = eldata%props(2)
      Eacti  = eldata%props(3)
      
      
      ! sbar   = props(4)
      sbar = peier(eldata%el) !! test pour Peier non homogene
         
         
      rPower = eldata%props(5)
      qPower = eldata%props(6)
      Tkelv  = eldata%props(7)
      regul  = eldata%props(8)

      sbarInv = RONE / sbar
      nPowerInv = RONE / nPower
      alpha = nPowerInv - RONE
      
      QRT  = Eacti/Rgas/Tkelv
      QRTn = QRT * nPowerInv
      
      n1 = nPower - RONE 
      r1 = rPower - RONE
      q1 = qPower - RONE
      
      cst1 = QRT * qPower * rPower * sbarInv      
      
      cst2 = half*((threehalf)**(half*alpha)) / (gamma0**nPowerInv)

      Be(:,:) = RZERO 
      qnode = RONE / real(nnode,kind=Rkind)

   end if

   if ( rqst(2) ) then
      Nev(:,:) = RZERO
!
!-    volscr vector (body forces, assumed constant):
!  
      volsrc = eldata%props(9:nprop)          
   end if    

!
!- loop over the I.P. (integration phase):
!          
   velem = RZERO
   meanVisco = RZERO
   
   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives (dphi) at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), dphi, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if  
      
      dv = eljac * eldata%wIP(ip)
!    
!-    B matrix (3d or plane-strain):
!
      do i = 1, ndime
         Be(i, i:nevav:ndime) = dphi(i,:)
      end do    
        
      j = 0
      do i = ndofn, nstri
         j = j + 1; k = perm(j)
         Be(i, j:nevav:ndime) = sqrt2i * dphi(k,:)
         Be(i, k:nevav:ndime) = sqrt2i * dphi(j,:)
      end do
!
!-    compute the strain rate from the previous step:
!     
      strai = matmul ( Be, eldata%U(dofv) )
      
      norme2 = dot_product ( strai, strai )
      
      epsilEq = sqrt ( threehalf*norme2 )                  
!
!-    compute the corresponding Von Mises equivalent stress "sigmaEq":
!
      call modelsStokes_eqStress ( epsilEq, sigmaEq, lambda, lambdap  )
!
!-    and the corresponding viscosity:
!
      beta = cst2*exp(lambda*nPowerInv)

      norme2 = norme2 + regul**2 ! square of the strain-rate + regul**2      

      visco  = beta * ( sqrt(norme2) )**(alpha)
            
      if ( rqst(3) ) then
!
!-       compute the derived quantities: e = B*u, s = 2*visco*e (strain and stress components)
!          
         strai = matmul ( Be, eldata%U(dofv) )
            
         eldata%S(1:nstri        , ip) = strai 
         eldata%S(nstri+1:2*nstri, ip) = Two * visco * strai 
         eldata%S(2*nstri+1      , ip) = visco 
                 
      end if
      
      if ( rqst(1) .or. rqst(2) ) then
!
!-       stiffness matrix: contribution to the velocity-velocity sub-matrix
!        (K0 = coefa*B'*B  and  K1 = coefb * B'*(e*e')*B):
!   
         coefa = dv * Two * visco
         coefb = theta * (coefa / norme2) * (RONE/(nPower - lambdap*sigmaEq)-RONE)
         
         do j = 1, nevav
            do i = 1, nevav
               do k = 1, nstri
                  Beki = Be(k,i)
                  K0(i,j) = K0(i,j) + coefa*Beki*Be(k,j) 
                  if ( rqst(1) ) then
                     do l = 1, nstri
                        K1(i,j) = K1(i,j) + coefb*strai(k)*strai(l)*Beki*Be(l,j)
                     end do
                  end if
               end do
            end do
         end do
!
!-       Stiffness matrix: pressure-velocity sub-matrix                               
!                  
         Bdiv(1,1:nevav) = [ ((dphi(i,j), i=1,ndime), j = 1, nnode) ] 
         
         Nep(:,1) = eldata%shp(:,ip)
         eldata%K(dofp,dofv) = eldata%K(dofp,dofv) + dv * matmul ( Nep, Bdiv )
!
!-       Stiffness matrix: pressure-pressure sub-matrix (stabilization term)                             
!        (modified mass matrix if meth=0 or laplacian matrix if meth=1)
!
         if ( meth == 0 ) then
            eldata%K(dofp,dofp) = eldata%K(dofp,dofp) - dv * matmul(Nep, transpose(Nep)-qnode)
         else if ( meth == 1 ) then
            eldata%K(dofp,dofp) = eldata%K(dofp,dofp) - dv * matmul(transpose(dphi), dphi)
         endif
             
         if ( rqst(2) ) then 
!
!-          load vector (body forces):
!                        
            do i = 1, ndime
               Nev(i:nevav:ndime,i) = eldata%shp(:,ip)
            end do   

            eldata%F(dofv) = eldata%F(dofv) + dv * matmul ( Nev, volsrc )    
!
!-          stabilization term (for meth=1 only):
!         
            if ( meth == 1 ) eldata%F(dofp) = eldata%F(dofp) + &
                                              dv * matmul ( transpose(dphi), volsrc )
         end if
         
      end if
      
      velem = velem + dv  ! element volume or surface
      meanVisco = meanVisco + dv * visco                                
      
   end do   

   meanVisco = meanVisco / velem

   if ( rqst(1) .or. rqst(2) ) then
   
      if ( meth == 0 ) then
         stab = RONE / meanVisco
      else if ( meth == 1 ) then
         stab = 0.333333*velem / meanVisco
         if ( name_geometry == 'triangle'  ) stab = stab * 4.0_Rkind / sqrt(3.0_Rkind)
         if ( name_geometry == 'tetraedre' ) stab = stab * 6.0_Rkind * sqrt(2.0_Rkind)
      end if   
     
      eldata%K(dofp,dofp) = eldata%K(dofp,dofp) * stab
      eldata%K(dofv,dofp) = transpose( eldata%K(dofp,dofv) )
      eldata%K(dofv,dofv) = K0                
   
      if ( rqst(2) ) then
         if ( meth == 1 ) eldata%F(dofp) = eldata%F(dofp) * stab
!
!-       Elemental residual (without the Neumann bc, already computed and assembled):
!
         eldata%F = eldata%F - matmul ( eldata%K, eldata%U ) 
!         
!-       Set 0 Dirichlet bc since %U is an increment:    
!                                                
         do iv = 1, nevab
            if ( eldata%dof(iv) < 0 ) eldata%U(iv) = RZERO
         end do
         
      end if
      
      if ( rqst(1) ) eldata%K(dofv,dofv) = eldata%K(dofv,dofv) + K1
      
   end if
   
   CONTAINS
   
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      SUBROUTINE modelsStokes_eqStress ( epsilEq, sigmaEq, lx, lpx )
   !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      real(Rkind), intent(in    ) :: epsilEq
      real(Rkind), intent(   out) :: sigmaEq, lx, lpx
   !------------------------------------------------------------------------------------------
   !  Finds by the Newton's method the solution "sigmaEq" of the non-linear equation:
   !
   !                          x^n * exp(-l(x)) - y = 0
   !
   !  where   l(x) = Q/RT * (1 - (x/sbar)^r)^q
   !   
   !  and     y = epsilEq / gamma0
   !
   !  Returns also l(x) and its derivative l'(x) at x = sigmaEq
   !
   !------------------------------------------------------------------------------------------      

   !- local variables: -----------------------------------------------------------------------
      integer(Ikind), parameter :: nitermax = 100
      real   (Rkind), parameter :: tol = 1e-8_Rkind, threshold = 1e-4_Rkind
      real   (Rkind)            :: x, xr, expl, le, res, resp, dsigma, epsilEqTilde
      integer(Ikind)            :: iter
      logical                   :: conv
   !------------------------------------------------------------------------------------------

      if ( epsilEq == RZERO ) then
         sigmaEq = RZERO
         lx = QRT
         lpx = RZERO
         return
      end if

      epsilEqTilde = epsilEq / gamma0
!
!-    Initialization: set sigmaEq to the previous Von Mises stress
!
      sigmaEq = sqrt( threehalf * &
              dot_product ( eldata%S(nstri+1:2*nstri,ip), eldata%S(nstri+1:2*nstri,ip) ) )
           
      if ( sigmaEq > sbar ) then
!
!-       This value must be lesser than the Peierls stress  
!
         sigmaEq = sbar
         
      else if ( sigmaEq < threshold*sbar ) then
!
!-       For small sigmaEq/sbar, initialize sigmaEq by neglecting this ratio in the exponential:
!
         sigmaEq = exp(QRTn) * (epsilEqTilde ** nPowerInv)
         
      end if
      
      le = log(epsilEqTilde)      
            
      do iter = 1, nitermax
         if ( sigmaEq > sbar ) then
            x = sbar/sigmaEq
            sigmaEq = x*sbar
         end if
   
         x = sigmaEq*sbarInv
         if ( x < 0 ) then
            print*,'Error in modelsStokes_eqStress: x < 0'
            stop
         end if   
         
         xr = x**rPower
         
         lx  = QRT  * (RONE - xr)**qPower
         lpx =-cst1 * (x**r1) * (RONE -xr)**q1

         res = nPower*log(sigmaEq) - lx - le
         dsigma =-res*sigmaEq / (nPower - sigmaEq*lpx) 
   
         if ( sigmaEq+dsigma < 0 ) then
            expl = exp(-lx)
            res = (sigmaEq**nPower)*expl - epsilEqTilde
            resp = (nPower - lpx*sigmaEq)*expl*(sigmaEq**n1)
            dsigma =-res / resp
         end if
   
         sigmaEq = sigmaEq + dsigma

         conv = ( abs(dsigma) <= tol * abs(sigmaEq) ) 
         if ( conv ) exit
      end do
      
      xr = (sigmaEq*sbarInv)**rPower
      lx  = QRT  * (RONE - xr)**qPower
      lpx =-cst1 * (x**r1) * (RONE -xr)**q1
      maxiter = max(maxiter,iter)
            
      if ( .not. conv ) then 
         print*,'Warning: in modelsStokes_eqStress not converged'
         print*,'epsilEq/gamma0, sigmaEq, dsigma: ',epsilEqTilde, sigmaEq, dsigma
         stop
      endif
      
      END SUBROUTINE modelsStokes_eqStress  
   
   END SUBROUTINE modelsStokes_KFelemNL2

       
END MODULE modelsStokes_m
