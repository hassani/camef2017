#include "error.fpp"

MODULE modelsElasticity_m
!--------------------------------------------------------------------------------------------- 
!-----------------------------------------------------------------------------------R.H. 12/16  
   use kindParameters_m

   use defTypes_m
   use err_m
   use param_m        

   use constants_m      , only: IZERO, RZERO, UERROR, NEGJAC    
   use element_m        , only: element_jacob
   use femsystem_m      , only: femsystem_statio, femsystem_storeNodalValues
   use solverdriver_m   , only: solverdriver_resol   
   use util_m           , only: util_intToChar
   use output_m         , only: output_driver
   use boundarycond_m   , only: boundarycond_Neumann2d, boundarycond_Neumann3d, &
                                boundarycond_Dirichlet1

   implicit none
      
   private
   public :: modelsElasticity_main
    
CONTAINS

    
!=============================================================================================
   SUBROUTINE modelsElasticity_main ( mymesh, mydata, femsys, stat, info )
!=============================================================================================
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (udata_t ), intent(in    )           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in out), optional :: info(*)
!---------------------------------------------------------------------------------------------            
!  Procedure that solves a linear elastic problem
!--------------------------------------------------------------------------------------------- 
!
!  Inputs
!
!      name_model_pb: string giving the name of the model (in lowercase and without spaces)
!
!      name_class_pb: string giving the class of the problem (steady or transient)
!                    
!      mymesh       : mesh structure
!
!      mydata       : data (material properties & boundary conditions) structure
!
!      femsys       : matrix structures
!
!      info         : array of strings, optional
!                     Through this option the model can be interogated to give some informations
!                     to the calling procedure about the required properties, number of d.o.f,
!                     space dimensions,... (useful for the input data phase)
!
!                     If present     --> informations are required
!
!                     If not present --> solution phase          
!
!  Outputs
!
!      information phase: 
!                      ndime: space dimensions
!                      ndofn: number of dof (= ndime)
!                      ncmpN: number of components of the traction vector ( = ndofn)
!                      nprop: number of material properties
!                      nstri: number of independent stress components
!                      info : messages that can be displayed to the user
!
!      solution phase: femsys%Sol
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'modelsElasticity_main'
   character(len=:), allocatable :: geom
   integer  (Ikind)              :: lp, rp
   logical                       :: request(4)
   external                      :: KFelem, Neumann
!---------------------------------------------------------------------------------------------    

   if ( stat > IZERO ) return 
    
   lp = index(name_model_pb,'(') ; rp = index(name_model_pb,')')
    
   geom = trim(adjustl(name_model_pb(lp+1:rp-1)))
    
   if ( name_class_pb == 'transient' ) then
      stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'sorry, transient problems not yet implemented' ) 
      return  
   end if    
   
   if (present(info)) then
!
!-    Information/initialization phase. According to the desired model define:
!
!      - ndime: space dimensions
!      - ndofn: nbr of d.o.f.
!      - nprop: nbr of material properties
!      - nstri: nbr of independent stress components 
!  
      if ( geom == 'plane_strain' .or. geom == 'plane_stress' ) then
         ndime = 2          ! 2D
         ndofn = ndime      ! 2 dof (ux, uy)
         ncmpN = ndime      ! 2 components of traction vector (for Neumann bc)
         nstri = 3          ! 3 indep. stresses : sxx, syy, sxy         
         nprop = 2 + ndime  ! 4 prop.: Young, Poisson, rho*gx, rho*gy
         
         noutp = 2*nstri+1  ! 7 post-proceded variables: 
                            ! . 3 strains and 4 stresses in plane-strain
                            ! . 4 strains and 3 stresses in plane-stress

      else if ( geom == 'axisymmetry' ) then
         ndime = 2          ! 2D
         ndofn = ndime      ! 2 dof (ur, uz)
         ncmpN = ndime      ! 2 components of stress vector (for Neumann's bc)    
         nstri = 4          ! 4 indep. stresses : srr, szz, stt, srz                       
         nprop = 2 + ndime  ! 4 prop.: Young, Poisson, rho*gr, rho*gz
         
         noutp = 2*nstri    ! post-proceded variables: 4 stresses and 4 strains
       
      else if ( geom == '3d' ) then
         ndime = 3          ! 3D
         ndofn = ndime      ! 3 dof (ux, uy, uz)
         ncmpN = ndime      ! 3 components of stress vector (for Neumann's bc)     
         nstri = 6          ! 6 indep. stresses : sxx, syy, szz, sxy, syz, szx                      
         nprop = 2 + ndime  ! 5 prop.: Young, Poisson, rho*gx, rho*gy, rho*gz

         noutp = 2*nstri    ! post-proceded variables: 6 stresses and 6 strains
          
      else     
         stat = err_t ( stat = UERROR, where = Here, &
                         msg = trim(name_model_pb)//': not a valid model' ) 
         return
      end if
!
!-    messages output
!       
      info( 1) = "Informations from subroutine models_elastic:"
      info( 2) = '   list of parameters for the model "'//trim(name_model_pb)//'":'
      write(info(3),'("  * Space dimension                       : ",i0)')ndime
      write(info(4),'("  * Number of degrees of freedom          : ",i0)')ndofn
      write(info(5),'("  * Number of components for stress vector: ",i0)')ncmpN
      write(info(6),'("  * Number of material properties         : ",i0)')nprop
      write(info(7),'("   and these ",i0," material properties are:")')nprop
      info( 8) = "       (1:1) Young's modulus"
      info( 9) = "       (2:2) Poisson's ratio"
      if ( geom == 'plane_stress' .or. geom == 'plane_strain') &
      info(10) = "       (3:4) body forces: rho*gx, rho*gy"    
      if ( geom == 'axisymmetry' ) & 
      info(10) = "       (3:4) body forces: rho*gr, rho*gz"   
      if ( geom == '3d' ) &         
      info(10) = "       (3:5) body forces: rho*gx, rho*gy, rho*gz"    
      info(11) = "   and must be given in this order"
       
      return
      
   end if   

!
!- Resolution phase:
!
   femsys%X(:) = 0.0e0_Rkind
    
   select case( name_class_pb )

      case ('steady')
!
!-       Form the matrix and the rhs:
!
         if ( verbosity(2) >= 1 ) print*,'Computing the rigidity matrix and the load vector'

!                     Mat      Rhs      Stress    Prod.
         request = [ .true. , .true. , .false. , .false. ]
       
         if ( geom == '3d' ) then
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsElasticity_KFelem3d, &
                                    Neumann   = boundarycond_Neumann3d   , &
                                    Dirichlet = boundarycond_Dirichlet1    )
       
         else if ( geom == 'plane_strain' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsElasticity_KFelemDp, &
                                    Neumann   = boundarycond_Neumann2d   , &
                                    Dirichlet = boundarycond_Dirichlet1    )

         else if ( geom == 'plane_stress' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsElasticity_KFelemCp, &
                                    Neumann   = boundarycond_Neumann2d   , &
                                    Dirichlet = boundarycond_Dirichlet1    )
                                  
         else if ( geom == 'axisymmetry' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsElasticity_KFelemAx, &
                                    Neumann   = boundarycond_Neumann2d   , &
                                    Dirichlet = boundarycond_Dirichlet1    )
                                  
         end if       
         
         error_TraceNreturn(stat>IZERO, Here, stat)        
!
!-       Solve the linear system:
!       
         if ( verbosity(2) >= 1 ) print*,'Solving the linear system'

         call solverdriver_resol ( femsys, stat )
         error_TraceNreturn(stat>IZERO, Here, stat)  

         call femsystem_storeNodalValues ( femsys )
      
         !femsys%Sol = femsys%X   
!      
!-       Compute the stresses (in femsys%Str):
!
         if ( verbosity(2) >=1 ) print*,'Computing the derived quantities (stress)'
          
!                     Mat       Rhs       Stress    Prod.
         request = [ .false. , .false. , .true.  , .false. ]
              
         if ( geom == '3d' ) then
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem  = modelsElasticity_KFelem3d )       
         else if ( geom == 'plane_strain' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem  = modelsElasticity_KFelemDp )
         else if ( geom == 'plane_stress' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem  = modelsElasticity_KFelemCp )                                  
         else if ( geom == 'axisymmetry' ) then 
         
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem  = modelsElasticity_KFelemAx )                                
         end if      
         
         error_TraceNreturn(stat>IZERO, Here, stat) 
!
!-       write the solution on the output file:
!
         if ( verbosity(2) >= 1 ) print*,'Printing the results'

         call output_driver ( mydata, mymesh, femsys, stat )
   

      case ('transient')
!
!-       cas dynamique : pas encore remis (reprendre schema de Newmark)
!    
!        conditions initiales, ...
!
!        do itime = 1, ntime
!           ...
!           ... matrice rigidite + masse + rhs(time)
!           ... resolution 
!           ... impression:
!           call output_driver (mydata, mymesh, femsys)

!        end do

      case default
         stat = err_t ( stat = UERROR, where = Here, msg = trim(name_class_pb) // &
                       ': not a valid class problem' )   
         return        
    
   end select                     

   END SUBROUTINE modelsElasticity_main
    

!=============================================================================================
   SUBROUTINE modelsElasticity_KFelem3d ( rqst, eldata, stat )
!=============================================================================================      
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F and/or 
!  stresses and strains eldata%S for a given element (# eldata%el)
!
!                     +-----------------------------------------------+  
!                     |           3D LINEAR ELASTICITY CASE           |
!                     +-----------------------------------------------+
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!                    props(1)=Young's mod., props(2)=Poisson's ratio, props(3:5)=body forces
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!
!  Notice
!
!   * For this model, note that
!       - ndime: = 3          , space dimensions
!       - nprop: = 5          , nbr of properties 
!       - ndofn: = 3          , nbr of dof per node
!       - nstri: = 6          , nbr independent stress components
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!   * The stress and strain components are arranged in the following conventional order
!
!       stress = [sigma_xx, sigma_yy, sigma_zz,   sigma_xy,   sigma_yz,   sigma_zx]
!       strain = [epsil_xx, epsil_yy, epsil_zz, 2*epsil_xy, 2*epsil_yz, 2*epsil_zx]
!
!     and the nodal displacements (n = nnode = nbr of nodes)
!
!       U = [ u1_x u1_y u1_z  | u2_x u2_y u2_z  | ...  | un_x un_y un_z ] 
!
!     With this convention   
!     
!       strain = Be * U,   stress = De * strain  (thus  \int (K = Be' * De * Be))
!
!     where De contains the elastic moduli and Be the shape function derivatives at I.P.
!
!   * The body force components are given by 
!
!                    volsrc = [fx,fy,fz] = eldata%props(3:5) 
!
!     their contribution to the element load vector is thus F = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!
!   * The element mass matrix is given by \int (Ne'*Ne).      
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = "modelsElasticity_KFelem3d"
   real     (Rkind), parameter :: z = 0.0_Rkind, u = 1.0_Rkind, h = 0.5_Rkind, d = 2.0_Rkind
   real     (Rkind)            :: Ne   (nevab,ndofn), Be   (nstri,nevab), De    (nstri,nstri), &
                                  elcar(ndime,nnode), strai(nstri      ), volsrc(ndofn      )
   real     (Rkind)            :: dv, eljac, a, b, mu, lamb, Poiss, Young
   integer  (Ikind)            :: ip
!---------------------------------------------------------------------------------------------          

   eldata%K(:,:) = z ; eldata%F(:) = z ; eldata%S(:,:) = z
    
   Be (:,:) = z;  Ne (:,:) = z

   if ( rqst(1) .or. rqst(3) ) then
!
!-    D matrix: if props are not constant on the element shift this part into the ip loop
!  
      Young = eldata%props(1); Poiss = eldata%props(2) ! Young's and Poisson's modulus

      a = Young / (u + Poiss) ; mu = a * h ; lamb = Poiss * a / (u - d*Poiss); b = a + lamb       
         
      De(1,1:6) = [ b   , lamb, lamb, z , z , z  ]
      De(2,1:6) = [ lamb, b   , lamb, z , z , z  ]
      De(3,1:6) = [ lamb, lamb, b   , z , z , z  ]
      De(4,1:6) = [ z   , z   , z   , mu, z , z  ]
      De(5,1:6) = [ z   , z   , z   , z , mu, z  ]
      De(6,1:6) = [ z   , z   , z   , z , z , mu ]

   end if   
    
   if ( rqst(2) ) then
!
!-    volscr vector (body forces): same remark as for D
!  
      volsrc = eldata%props(3:5)          
   end if    

!
!- loop over the I.P. (integration phase):
!          
   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if                        

      dv = eljac * eldata%wIP(ip)

      if ( rqst(1) .or. rqst(3) ) then
!    
!-       B matrix:
!
         Be(1, 1:nevab:3) = elcar(1,:)
         Be(2, 2:nevab:3) = elcar(2,:)
         Be(3, 3:nevab:3) = elcar(3,:) 
         Be(4, 1:nevab:3) = elcar(2,:) ;  Be(4, 2:nevab:3) = elcar(1,:)
         Be(5, 2:nevab:3) = elcar(3,:) ;  Be(5, 3:nevab:3) = elcar(2,:)
         Be(6, 1:nevab:3) = elcar(3,:) ;  Be(6, 3:nevab:3) = elcar(1,:)
           
         if ( rqst(1) ) then
!
!-          Stiffness matrix:
!         
            eldata%K = eldata%K + dv * matmul ( matmul ( transpose(Be), De ), Be )         
         else
!
!-          compute the derived quantities (stress and strain components):
!          
            strai = matmul ( Be,  eldata%U )
            
            eldata%S(      1:nstri  ,ip) = strai
            eldata%S(nstri+1:2*nstri,ip) = matmul ( De, strai )
         end if
             
      end if
             
      if (rqst(2)) then 
!
!-       N matrix:
!
         Ne(1:nevab:3, 1) = eldata%shp(:,ip) 
         Ne(2:nevab:3, 2) = eldata%shp(:,ip)   
         Ne(3:nevab:3, 3) = eldata%shp(:,ip)  

         eldata%F = eldata%F + dv * matmul ( Ne, volsrc )    
      end if
       
   end do   
       
   END SUBROUTINE modelsElasticity_KFelem3d


!=============================================================================================
   SUBROUTINE modelsElasticity_KFelemDp ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F and/or 
!  stresses and strains eldata%S for a given element (# eldata%el)
!
!                     +-----------------------------------------------+  
!                     |      PLANE STRAIN LINEAR ELASTICITY CASE      |
!                     +-----------------------------------------------+
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!                    props(1)=Young's mod., props(2)=Poisson's ratio, props(3:4)=body forces
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!  Notice
!
!   * For this model, note that
!       - ndime: = 2          , space dimensions
!       - nprop: = 4          , nbr of properties 
!       - ndofn: = 2          , nbr of dof per node
!       - nstri: = 3          , nbr independent stresses
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!   * The stress and strain components are arranged in the following conventional order
!
!           stress = [ sigma_xx, sigma_yy,   sigma_xy ]
!           strain = [ epsil_xx, epsil_yy, 2*epsil_xy ]
!
!     and the element nodal displacements (n = nnode = nbr of nodes)
!
!           U = [ u1_x u1_y | u2_x u2_y | ...  | un_x un_y ] 
!
!     With this convention   
!     
!       strain = Be * U,   stress = De * strain  (thus  \int (K = Be' * De * Be))
!
!     where De contains the elastic moduli and Be the shape function derivatives at I.P.
!
!     The sigma_zz stress (not computed) is simply given by Poisson * (sigma_xx + sigma_yy).
!
!   * The body force components are given by 
!
!                    volsrc = [fx,fy] = eldata%props(3:4) 
!
!     their contribution to the element load vector is thus F = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!
!   * The element mass matrix is given by \int (Ne'*Ne).    
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = "modelsElasticity_KFelemDp"
   real     (Rkind), parameter :: z = 0.0_Rkind, u = 1.0_Rkind, h = 0.5_Rkind, d = 2.0_Rkind 
   real     (Rkind)            :: Ne   (nevab,ndofn), Be   (nstri,nevab), De    (nstri,nstri), &
                                  elcar(ndime,nnode), strai(nstri      ), volsrc(ndofn      )
   real     (Rkind) :: dv, eljac, a, b, mu, lamb, Poiss, Young
   integer  (Ikind) :: ip
!---------------------------------------------------------------------------------------------          

   eldata%K(:,:) = z ; eldata%F(:) = z ; eldata%S(:,:) = z
    
   Be (:,:) = z;  Ne (:,:) = z

   if ( rqst(1) .or. rqst(3) ) then
    
!
!-    D matrix: if props are not constant on the element shift this part into the ip loop
!  
      Young = eldata%props(1); Poiss = eldata%props(2) ! Young's and Poisson's modulus
  
      a = Young / (u + Poiss) ; mu = a * h ; lamb = Poiss * a / (u - d*Poiss); b = a + lamb       
       
      De(1,1:3) = [ b    ,lamb, z  ]
      De(2,1:3) = [ lamb ,b   , z  ]
      De(3,1:3) = [ z    ,z   , mu ]

   end if   

   if ( rqst(2) ) then
!
!-    volscr vector (body forces): same remark  
!  
      volsrc = eldata%props(3:4)          
   end if    

!
!- loop over the I.P. (integration phase):
!          
   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if
      
      dv = eljac * eldata%wIP(ip)

      if ( rqst(1) .or. rqst(3) ) then
!    
!-       B matrix:
!
         Be(1, 1:nevab:2) = elcar(1,:)
         Be(2, 2:nevab:2) = elcar(2,:)
         Be(3, 1:nevab:2) = elcar(2,:) ;  Be(3, 2:nevab:2) = elcar(1,:)
           
         if ( rqst(1) ) then
!
!-          Stiffness matrix:
!         
            eldata%K = eldata%K + dv * matmul ( matmul ( transpose(Be), De ), Be )          
         else
!
!-          compute the derived quantities (stress and strain components)
!          
            strai = matmul ( Be, eldata%U )
            eldata%S(      1:nstri  ,ip) = strai
            eldata%S(nstri+1:2*nstri,ip) = matmul ( De, strai )
            
            ! sigma_zz = poiss * (sigma_xx + sigma_yy):            
            eldata%S(noutp,ip) = Poiss*(eldata%S(nstri+1,ip)+eldata%S(nstri+2,ip))
         end if
             
      end if
             
      if (rqst(2)) then 
!
!-       N matrix:
!             
         Ne(1:nevab:2, 1) = eldata%shp(:,ip)
         Ne(2:nevab:2, 2) = eldata%shp(:,ip)   
          
         eldata%F = eldata%F + dv * matmul ( Ne, volsrc )    
      end if
       
   end do   
       
   END SUBROUTINE modelsElasticity_KFelemDp
    
    
!=============================================================================================
   SUBROUTINE modelsElasticity_KFelemCp ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F and/or 
!  stresses and strains eldata%S for a given element (# eldata%el)
!
!                     +-----------------------------------------------+  
!                     |      PLANE STRESS LINEAR ELASTICITY CASE      |
!                     +-----------------------------------------------+
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!                    props(1)=Young's mod., props(2)=Poisson's ratio, props(3:4)=body forces
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!
!  Notice
!
!   * For this model, note that
!       - ndime: = 2          , space dimensions
!       - nprop: = 4          , nbr of properties 
!       - ndofn: = 2          , nbr of dof per node
!       - nstri: = 3          , nbr independent stresses
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!   * The stress and strain components are arranged in the following conventional order
!
!           stress = [ sigma_xx, sigma_yy,   sigma_xy ]
!           strain = [ epsil_xx, epsil_yy, 2*epsil_xy ]
!
!     and the element nodal displacements (n = nnode = nbr of nodes)
!
!           U = [ u1_x u1_y | u2_x u2_y | ...  | un_x un_y ] 
!
!     With this convention   
!     
!       strain = Be * elUdep,   stress = De * strain  (thus  \int (K = Be' * De * Be))
!
!     where De contains the elastic moduli and Be the shape function derivatives at I.P.
!
!     The strain epsil_zz (not computed) is simply given by A * (epsil_xx + epsil_yy)
!     with A = - Poisson / (1 - Poisson).
!
!   * The body force components are given by 
!
!                    volsrc = [fx,fy] = eldata%props(3:4) 
!
!     their contribution to the element load vector is thus F = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!
!   * The element mass matrix is given by \int (Ne'*Ne). 
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = "modelsElasticity_KFelemCp"
   real     (Rkind), parameter :: z = 0.0_Rkind, u = 1.0_Rkind, h = 0.5_Rkind    
   real     (Rkind)            :: Ne   (nevab,ndofn), Be   (nstri,nevab), De    (nstri,nstri), &
                                  elcar(ndime,nnode), strai(nstri      ), volsrc(ndofn      )
   real     (Rkind) :: dv, eljac, a, b, c, mu, Poiss, Young
   integer  (Ikind) :: ip
!---------------------------------------------------------------------------------------------          

   eldata%K(:,:) = z ; eldata%F(:) = z ; eldata%S(:,:) = z
    
   Be (:,:) = z;  Ne (:,:) = z

   if ( rqst(1) .or. rqst(3) ) then
!
!-    D matrix: if props are not constant on the element shift this part into the ip loop
!  
      Young = eldata%props(1); Poiss = eldata%props(2) ! Young's and Poisson's modulus
    
      a = Young / (u - Poiss * Poiss) ; b = Poiss*a ; c = u / (u + Poiss)
      
      mu = h * Young * c ; c =-Poiss * c
      
      De(1,1:3) = [ a, b, z ]
      De(2,1:3) = [ b, a, z ]
      De(3,1:3) = [ z ,z, mu]
       
   end if   
    
   if ( rqst(2) ) then
!
!-    volscr vector (body forces): same remark  
!  
      volsrc = eldata%props(3:4)          
   end if    

!
!- loop over the I.P. (integration phase):
!          
   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
      call element_jacob (eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode)
      
      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if
      
      dv = eljac * eldata%wIP(ip)

      if ( rqst(1) .or. rqst(3) ) then
!    
!-       B matrix:
!
         Be(1, 1:nevab:2) = elcar(1,:)
         Be(2, 2:nevab:2) = elcar(2,:)
         Be(3, 1:nevab:2) = elcar(2,:) ;  Be(3, 2:nevab:2) = elcar(1,:)
           
         if ( rqst(1) ) then
!
!-          Stiffness matrix:
!      
            eldata%K = eldata%K + dv * matmul ( matmul ( transpose(Be), De ), Be )      
         else
!
!-          compute the derived quantities (strains and stresses components):
!          
            strai = matmul ( Be, eldata%U )
            ! store the strains in %S:
            eldata%S(1:nstri,ip) = strai
            ! strain_zz =-poiss * (strain_xx + strain_yy)/(1+poiss):                        
            eldata%S(nstri+1,ip) = c * ( strai(1) + strai(2) )
            ! compute and store the stresses in %S:       
            eldata%S(nstri+2:noutp,ip) = matmul ( De, strai )      
            
         end if
             
      end if
             
      if ( rqst(2) ) then 
!
!-       N matrix:
!             
         Ne(1:nevab:2, 1) = eldata%shp(:,ip)
         Ne(2:nevab:2, 2) = eldata%shp(:,ip)   
          
         eldata%F = eldata%F + dv * matmul ( Ne, volsrc )    
      end if
       
   end do   
       
   END SUBROUTINE modelsElasticity_KFelemCp
    
    
!=============================================================================================
   SUBROUTINE modelsElasticity_KFelemAx ( rqst, eldata, stat )
!=============================================================================================  
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix eldata%K, load vector eldata%F and/or 
!  stresses and strains eldata%S for a given element (# eldata%el)
!
!                     +-----------------------------------------------+  
!                     |      AXISYMMETRIC LINEAR ELASTICITY CASE      |
!                     +-----------------------------------------------+
!
!  Inputs
!    - eldata%props: contains the material properties of the element: 
!                    props(1)=Young's mod., props(2)=Poisson's ratio, props(3:4)=body forces
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: strains and stresses calculation)
!    - rqst        : logical array
!                    if rqst(1) = .true.: compute the element stiffness matrix
!                    if rqst(2) = .true.: compute the element load vector
!                    if rqst(3) = .true.: compute the element stresses (at I.P.)
!
!  Outputs
!    - eldata%K  : the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F  : the body sources (if rqst(2)=.true., else 0)
!    - eldata%S  : the stresses at I.P. (if rqst(3)=.true., else 0)
!
!  Notice
!
!   * For this model, note that
!       - ndime: = 2          , space dimensions
!       - nprop: = 4          , nbr of properties 
!       - ndofn: = 2          , nbr of dof per node
!       - nstri: = 4          , nbr independent stresses
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!   * The stress and strain components are arranged in the following conventional order
!
!           stress = [ sigma_rr, sigma_zz, sigma_tt,   sigma_rz ]
!           strain = [ epsil_rr, epsil_zz, epsil_tt, 2*epsil_rz ]
!
!     and the element nodal displacements (n = nnode = nbr of nodes)
!
!           U = [ u1_r u1_z | u2_r u2_z | ...  | un_r un_z ] 
!
!     With this convention   
!     
!       strain = Be * eldep,   stress = De * strain  (thus  \int (K = Be' * De * Be))
!
!     where De contains the elastic moduli and Be the shape function derivatives at I.P.
!!
!   * The body force components are given by 
!
!                    volsrc = [fr,fz] = eldata%props(3:4) 
!
!     their contribution to the element load vector is thus F = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!
!   * The element mass matrix is given by \int (Ne'*Ne). 
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: Here = "modelsElasticity_KFelemAx"
   real     (Rkind), parameter :: z = 0.0_Rkind, u = 1.0_Rkind, h = 0.5_Rkind, d = 2.0_Rkind, &
                                  deuxpi = 6.283185307179586_Rkind
   real     (Rkind)            :: Ne   (nevab,ndofn), Be   (nstri,nevab), De    (nstri,nstri), &
                                  elcar(ndime,nnode), strai(nstri      ), volsrc(ndofn      ) 
   real     (Rkind)            :: dv, eljac, a, b, c, s, r, Poiss, Young
   integer  (Ikind)            :: ip
!---------------------------------------------------------------------------------------------          

   eldata%K(:,:) = z ; eldata%F(:) = z ; eldata%S(:,:) = z
    
   Be (:,:) = z;  Ne (:,:) = z

   if ( rqst(1) .or. rqst(3) ) then
!
!-    D matrix: if props are not constant on the element shift this part into the ip loop
!  
      Young = eldata%props(1); Poiss = eldata%props(2) ! Young's and Poisson's modulus
  
      s = h * Young / (u + Poiss) ! shear modulus

      c = d / (u - d*poiss) ; a = s * c * (u - poiss) ; b = s * c * Poiss
         
      De(1,1:4) = [ a, b, b, z ]
      De(2,1:4) = [ b, a, b, z ]
      De(3,1:4) = [ b, b, a, z ]     
      De(4,1:4) = [ z, z, z, s ]
       
   end if   
    
   if ( rqst(2) ) then
!
!-    volscr vector (body forces): same remark  
!  
      volsrc = eldata%props(3:4)          
   end if    

!
!- loop over the I.P. (integration phase):
!          
   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode )
      
      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if
               
      r = dot_product ( eldata%x(1,:), eldata%shp(:,ip) ) ! interpolation of r at the I.P.  

      dv = eljac * eldata%wIP(ip) * deuxpi * r
       
      if ( rqst(1) .or. rqst(3) ) then
!    
!-       B matrix:
!    
         Be(1,1:nevab:2) = elcar(1,:)
         Be(2,2:nevab:2) = elcar(2,:)
         Be(3,1:nevab:2) = eldata%shp(:,ip) / r
         Be(4,1:nevab:2) = elcar(2,:) ;  Be(4,2:nevab:2) = elcar(1,:)

         if ( rqst(1) ) then
!
!-          Stiffness matrix:
!         
            eldata%K = eldata%K + dv * matmul ( matmul ( transpose(Be), De ), Be )          
         else
!
!-          compute the derived quantities (stresses components)
!          
            strai = matmul ( Be, eldata%U )
            
            eldata%S(      1:nstri  ,ip) = strai
            eldata%S(nstri+1:2*nstri,ip) = matmul ( De, strai )
         end if
             
      end if
             
      if ( rqst(2) ) then 
!
!-       N matrix:
!             
         Ne(1:nevab:2, 1) = eldata%shp(:,ip)
         Ne(2:nevab:2, 2) = eldata%shp(:,ip)   
          
         eldata%F = eldata%F + dv * matmul ( Ne, volsrc )    
      end if
       
   end do   
       
   END SUBROUTINE modelsElasticity_KFelemAx
       

!=============================================================================================
!    SUBROUTINE modelsElasticity_KFelemMidlin &
!               (el, rqst, props, elshp, elcoo, weiIP, eldep, elder, Kel, Fel, Sel, eldof, vdiri)
!!! ... a continuer
!=============================================================================================  
!    integer(Ikind), intent(in ) :: el
    
!    logical       , intent(in ) :: Mat, Rhs, Str
    
!    real   (Rkind), intent(in ) :: props(      nprop), elshp(      nnode,nipts),             &
!                                   elcoo(ndime,nnode), weiIP(            nipts),             &
!                                   eldep(      nevab), elder(ndime,nnode,nipts)
                                   
!    real   (Rkind), intent(out) :: Kel(nevab,nevab), Fel(nevab), Sel(noutp,nipts)

!--------------------------------------------------------------------------------------------- 
!   This routine computes the element stiffness matrix Ke and load vector Fe for a given element 
!
!                     +-----------------------------------------------+  
!                     |         MINDLIN'S ELASTIC PLATE CASE          |
!                     +-----------------------------------------------+
!
!   Inputs
!       - props: contains material properties: props(1)=Young's mod., props(2)=Poisson's ratio
!                                              props(3)=thickness, props(4)=density constrast
!       - elcoo: nodal coordinates
!       - elshp: shape functions at the I.P.
!       - elder: local derivatives of shape functions at the I.P.
!       - weiIP: weight of I.P.
!       - eldep: nodal solution (for postprocessing: strains and stresses calculation)
!       - Mat  : if Mat =.true.: the element stifness matrix is requested
!       - Rhs  : if Rhs =.true.: the element load vector is requested
!       - Str  : if Str =.true.: the element "stresses" are requested (at I.P.)
!
!
!   Outputs
!       - Kel  : the stiffness integrand computed at the I.P. (if Mat=.true., else 0)
!       - Fel  : the body sources integrand computed at the I.P. (if Rhs=.true., else 0)
!       - Sel  : the fluxes at I.P. (if Str=.true., else 0)
!
!   Notice
!
!   * For this model, note that
!       - ndime: = 2          , space dimensions
!       - nprop: = 4          , nbr of properties 
!       - ndofn: = 3          , nbr of dof per node (vertical deflexion and 2 rotations)
!       - nstri: = 5          , nbr independent "stresses" (moment and shear)
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!   * The stress and strain components are arranged in the following conventional order
!
!           stress = [ sigma_rr, sigma_zz, sigma_tt,   sigma_rz ]
!           strain = [ epsil_rr, epsil_zz, epsil_tt, 2*epsil_rz ]
!
!     and the element nodal displacements (n = nnode = nbr of nodes)
!
!           eldep = [ u1_r u1_z | u2_r u2_z | ...  | un_r un_z ] 
!
!     With this convention   
!     
!       strain = Be * eldep,   stress = De * strain  (thus  \int (Kel = Be' * De * Be))
!
!     where De contains the elastic moduli and Be the shape function derivatives at I.P.
!!
!   * The body force components are given by 
!
!                    volsrc = [fr,fz] = props(3:4) 
!
!     their contribution to the element load vector is thus Fel = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!
!   * The element mass matrix is given by \int (Ne'*Ne). 
!---------------------------------------------------------------------------------------------     

!-- local variables: ------------------------------------------------------------------------- 
!    real   (Rkind) :: Be(nstri,nevab), De   (nstri,nstri), volsrc(ndofn),      &
!                      Ne(nevab,ndofn), elcar(ndime,nnode), strai (nstri)
!    real   (Rkind) :: dv, eljac, Poiss, Young, Thick
!    real   (Rkind) :: z = 0.0_Rkind, u = 1.0_Rkind, h = 0.5_Rkind, tw = 12.0_Rkind, f=2.4_Rkind
!    real   (Rkind) :: A, B, C, D, t3, p2
!    integer(Ikind) :: ip
!---------------------------------------------------------------------------------------------          

!    Kel(:,:) = z ; Fel(:) = z ; Sel(:,:) = z
    
!    Be (:,:) = z;  Ne (:,:) = z

!    if (Mat .or. Str) then
!
!--    D matrix: if props are not constant on the element shift this part into the ip loop
!  
!       Young = props(1); Poiss = props(2) ! Young's and Poisson's modulus
!       Thick = props(3)                   ! Plate thickness
       
!       t3 = Thick**3; p2 = Poiss**2
       
!       A = Young*t3 / (tw * (u - p2))
!       B = Poiss * A;  C = h*(u - Poiss)*A; D = Young*Thick / (f*(u + Poiss))
         
!       De(1,1:5) = [ A, B, z, z, z ]           
!       De(2,1:5) = [ B, A, z, z, z ]
!       De(3,1:5) = [ z, z, C, z, z ]
!       De(4,1:5) = [ z, z, z, D, z ]     
!       De(5,1:5) = [ z, z, z, z, D ]
       
!    end if   
    
!    if (Rhs) then
!
!--    volscr vector (body forces): same remark  
!  
!       volsrc = props(3:4)          
!    end if    

!
!-- loop over the I.P. (integration phase):
!          
!    do ip = 1, nipts      
!
!--    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
!       call element_jacob (eljac, elcoo, elder(:,:,ip), elcar, ndime, nnode)

!       if (eljac <= 0) call modelsDiffusion_error (err=2, where='KFmidl', matx=elcoo, elm=el)
         
!       dv = eljac * weiIP(ip)
              
!       if (Mat .or. Str) then
!    
!--       B matrix:
!    
!          Be(1,2:nevab:3) =-elcar(1,:)
!          Be(2,3:nevab:3) =-elcar(2,:)
!          Be(3,2:nevab:3) =-elcar(2,:) ; Be(3,3:nevab:3) =-elcar(1,:)
!          Be(4,1:nevab:3) = elcar(1,:) ; Be(4,2:nevab:3) =-elshp(:,ip)
!          Be(5,1:nevab:3) = elcar(2,:) ; Be(5,3:nevab:3) =-elshp(:,ip)

!!!!  rajouter les restoring terms (matrice de masse*props(5) sur les dof en deflexion)

!          if (Mat) Kel = Kel + dv * matmul ( matmul ( transpose(Be), De ), Be )
          
!          if (Str) then
!
!--          compute the derived quantities (stresses components)
!          
!             strai = matmul(Be,eldep) 
!             Sel(1:nstri        , ip) = strai 
!             Sel(nstri+1:2*nstri, ip) = matmul(De,strai) 
!          end if
             
!       end if
             
!       if (Rhs) then 
!
!--       N matrix:
!             
!!!     a completer
!          Fel = Fel + dv * matmul ( Ne, volsrc )    
!       end if
       
!    end do   
       
!    END SUBROUTINE modelsElasticity_KFelemMidlin

   
END MODULE modelsElasticity_m
