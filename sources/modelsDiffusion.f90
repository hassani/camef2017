#include "error.fpp"

MODULE modelsDiffusion_m
!--------------------------------------------------------------------------------------------- 
!-----------------------------------------------------------------------------------R.H. 12/16  
   use kindParameters_m

   use defTypes_m
   use err_m
   use param_m       
   
   use constants_m      , only: IZERO, RZERO, UERROR, NEGJAC
   use element_m        , only: element_jacob
   use femsystem_m      , only: femsystem_statio, femsystem_storeNodalValues
   use solverdriver_m   , only: solverdriver_resol   
   use util_m           , only: util_intToChar
   use output_m         , only: output_driver
   use boundarycond_m   , only: boundarycond_Neumann2d, boundarycond_Neumann3d, &
                                boundarycond_Dirichlet1

   implicit none
       
   private
   public :: modelsDiffusion_main, modelsDiffusion_error
    
CONTAINS

!=============================================================================================
   SUBROUTINE modelsDiffusion_main ( mymesh, mydata, femsys, stat, info )
!=============================================================================================    
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (udata_t ), intent(in    )           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in out), optional :: info(*)
!---------------------------------------------------------------------------------------------            
!  Procedure that solves a diffusion problem
!--------------------------------------------------------------------------------------------- 
!
!  Inputs
!
!       mymesh: mesh structure
!
!       mydata: data (material properties & boundary conditions) structure
!
!       femsys: matrix structures
!
!       init  : logical. 
!               If init==.true.  --> initialization phase (see below)
!               If init==.false. --> solution phase       
!
!  Outputs
!
!       ndime, ndofn, nprop, nstri (in the initialization phase)
!
!       femsys%Sol (in the solution phase)
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter   :: Here = 'modelsDiffusion_main' 
   character(len=:), allocatable :: geom
   integer  (Ikind)              :: lp, rp
   logical                       :: request(4)
!---------------------------------------------------------------------------------------------    

   if ( stat > IZERO ) return 
            
   lp = index(name_model_pb,'(') ; rp = index(name_model_pb,')')
    
   geom = trim(adjustl(name_model_pb(lp+1:rp-1)))
        
   if ( name_class_pb /= 'steady' ) then
      stat = err_t ( stat = UERROR, where = Here, &
                      msg = 'sorry, transient problems not yet implemented' ) 
      return  
   end if
     
   if ( present(info) ) then
!
!-    Initialisation phase. According to the desired model define:
!
!      - ndime: space dimensions
!      - ndofn: nbr of d.o.f.
!      - nprop: nbr of material properties to be read and saved in mydata%vmatprop
!      - nstri: nbr of independent stress components)  
!  
      if ( geom == '1d' ) then
         ndime = 1
      else if ( geom == '2d' ) then
         ndime = 2
      else if ( geom == '3d' ) then
         ndime = 3
      else
         stat = err_t ( stat = UERROR, where = Here, msg = geom // &
                       ': not a valid dimension (must be either 1d or 2d or 3d)')   
         return      
      end if   
                 
      ndofn = 1      ! nbr of dof 
      ncmpN = 1      ! 1 component (qn) of normal flux (for Neumann's bc)      
      nstri = ndime  ! 2 or 3 components of flux vector
      nprop = 2      ! 2 material properties
      
      noutp = nstri  ! post-proceded variables: flux vector components
      
!
!-    output messages
!       
      info(1) = "Informations from subroutine modelsDiffusion_main:"
      info(2) = '   list of parameters for the model "'//trim(name_model_pb)//'":'
      write(info(3),'("  * Space dimension                     : ",i0)')ndime
      write(info(4),'("  * Number of degrees of freedom        : ",i0)')ndofn
      write(info(5),'("  * Number of components for normal flux: ",i0)')ncmpN
      write(info(6),'("  * Number of material properties       : ",i0)')nprop
      write(info(7),'("   and these ",i0," material properties are:")')nprop
      info(8) = "       (1:1) conductivity (e.g. thermal conductivity)"
      info(9) = "       (2:2) volumetric source (e.g. heat production)"
       
      return 
    
   end if   

!
!- Resolution phase:
!
   femsys%X(:) = RZERO
    
   select case (name_class_pb)
    
      case ('steady')
!
!-       Form the matrix and the rhs:
!
         if ( verbosity(2) >=1 ) print*,'Computing the rigidity matrix and the load vector'
       
!                     Mat      Rhs      Stress    Prod.
         request = [ .true. , .true. , .false. , .false. ]

         if ( geom == '2d' ) then
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsDiffusion_KFelem,    &
                                    Neumann   = boundarycond_Neumann2d,    &
                                    Dirichlet = boundarycond_Dirichlet1    )
         else if ( geom == '3d' ) then
            call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                    KFelem    = modelsDiffusion_KFelem,    &
                                    Neumann   = boundarycond_Neumann3d,    &
                                    Dirichlet = boundarycond_Dirichlet1    )
         end if 
         
         error_TraceNreturn(stat>IZERO, Here, stat)        
!
!-       Solve the linear system:
!       
         if ( verbosity(2) >=1 ) print*,'Solving the linear system'

         call solverdriver_resol ( femsys, stat )
         error_TraceNreturn(stat>IZERO, Here, stat)
!
!-       Store the nodal solution:
!
         call femsystem_storeNodalValues ( femsys )
         !femsys%Sol = femsys%X   
!
!-       Compute the flux vector at each I.P.
!
         if ( verbosity(2) >=1 ) print*,'Computing the derived quantities (flux)'

!                     Mat       Rhs       Stress    Prod.
         request = [ .false. , .false. , .true.  , .false. ]
              
         call femsystem_statio ( request, mymesh, mydata, femsys, stat, &
                                 KFelem  = modelsDiffusion_KFelem       )
         error_TraceNreturn(stat>IZERO, Here, stat)                        
!
!-       write the solution on the output file:
!
         if ( verbosity(2) >=1 ) print*,'Printing the results'
    
         call output_driver ( mydata, mymesh, femsys, stat )


      case ('evolution')
!
!-       cas transitoire : pas encore remis... reprendre schema C.-N. ou Euler Impl. :
!    
!        (M/dt + K)* T^{n+1} = (M/dt)*T^n + F^{n+1}    -->    A*T = b + F
!
!        b : formee par assemblage des produits elementaires Me*Te^n / dt
!        A : formee par assemblage des matrices elementaires Me/dt + Ke
!
!        conditions initiales, ...
!
!        do itime = 1, ntime
!           ...
!           ... matrice rigidite + masse + rhs(time)
!           ... resolution 
!           ... impression:
!           call output_driver (mydata, mymesh, femsys)
!        end do

      case default
         stat = err_t ( stat = UERROR, where = Here, msg = trim(name_class_pb) // &
                       ': not a valid class problem' )   
         return       

   end select                          

   END SUBROUTINE modelsDiffusion_main


!=============================================================================================    
   SUBROUTINE modelsDiffusion_KFelem ( rqst, eldata, stat )
!=============================================================================================      
   logical          , intent(in    ) :: rqst(*)
   type   (eldata_t), intent(in out) :: eldata
   type   (err_t   ), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  This routine computes the element stiffness matrix Kel, load vector Fel or fluxes Sel for a
!  given element (# el)
!
!                         +--------------------------+ 
!                         | 2D or 3D DIFFUSION CASES |
!                         +--------------------------+ 
!
!  Inputs
!    - eldata%props: contains material properties: 
!                         %props(1) = conductivity, %props(2) = heat production
!    - eldata%x    : nodal coordinates
!    - eldata%shp  : shape functions computed at the I.P.
!    - eldata%der  : local derivatives of shape functions at the I.P.
!    - eldata%wIP  : weight of I.P.
!    - eldata%U    : nodal solution (for postprocessing: flux calculation)
!    - rqst        : logical array
!                      if rqst(1) = .true.: compute the element stiffness matrix
!                      if rqst(2) = .true.: compute the element load vector
!                      if rqst(3) = .true.: compute the element flux vector (at I.P.)
!
!   note that for this model:
!       - nprop: = 2          , nbr of properties 
!       - ndofn: = 1          , nbr of dof per node
!       - nstri: = ndime      , nbr flux components
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!  Outputs
!    - eldata%K: the stiffness matrix (if rqst(1)=.true., else 0)
!    - eldata%F: the body sources (if rqst(2)=.true., else 0)
!    - eldata%S: the fluxes at I.P. (if rqst(3)=.true., else 0)
!
!
!  Notice
!
!   * For this model, note that
!       - nprop: = 3          , nbr of properties 
!       - ndofn: = 1          , nbr of dof per node
!       - nstri: = ndime      , nbr independent stress components
!       - nevab: = ndofn*nnode, nbr of dof per element
!
!   * The gradient and the flux on each I.P. are
!
!       flux = [ q_x, q_y, q_z] (in 3D)
!       grad = [ dxu, dyu, dzu]
!
!     and the nodal dof (n = nnode = nbr of nodes)
!
!       U = [ u1, u2, ..., u_n ] 
!
!     With this convention   
!     
!       grad = Be * U,   flux = De * grad  (thus  \int (Kel = Be' * De * Be))
!
!     where De is the conductivity matrix and Be contains the shape function derivatives at I.P.
!
!   * The body source is  
!
!                    volsrc = props(2) 
!
!     its contribution to the element load vector is thus Fel = \int (Ne * volsrc) where
!     the matrix Ne contains the values of the shape functions at the I.P.
!
!   * The element mass matrix is given by \int (Ne'*Ne). 
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter :: HERE = "modelsDiffusion_KFelem"
   integer  (Ikind)            :: ip
   real     (Rkind)            :: eljac, dv, elcar(ndime,nnode)
!---------------------------------------------------------------------------------------------          

   eldata%K(:,:) = RZERO ; eldata%F(:) = RZERO ; eldata%S(:,:) = RZERO

   do ip = 1, nipts      
!
!-    compute the jacobian and the cartesian derivatives at the ip th I.P.:
!    
      call element_jacob ( eljac, eldata%x, eldata%der(:,:,ip), elcar, ndime, nnode )

      if ( eljac <= 0 ) then
         stat = err_t ( stat = NEGJAC, where = Here, &
                        msg = 'Negative jacobian (element #'//util_intToChar(eldata%el)//')' )
         return
      end if
!
!-    integration phase (for Kel and/or Fel):
!
      dv = eljac * eldata%wIP(ip)
       
      if ( rqst(1) ) eldata%K = eldata%K + &
                                eldata%props(1) * matmul ( transpose(elcar), elcar ) * dv
       
      if ( rqst(2) ) eldata%F = eldata%F + &
                                eldata%props(2) * eldata%shp(:,ip) * dv       
!
!-    compute the flux vector:
!       
      if ( rqst(3) ) eldata%S(:,ip) = eldata%props(1) * matmul ( elcar, eldata%U )
       
   end do
    
   END SUBROUTINE modelsDiffusion_KFelem
        

!=============================================================================================
   SUBROUTINE modelsDiffusion_error ( uerr, err, matx, elm, where, msg )
!=============================================================================================
   integer         , intent(in), optional :: err
   integer  (Ikind), intent(in), optional :: elm, uerr
   real     (Rkind), intent(in), optional :: matx(:,:)    
   character(len=*), intent(in), optional :: where, msg
!---------------------------------------------------------------------------------------------    
!  Affichage d'un message d'erreur et arret du programme
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   integer(Ikind) :: i, ue
!---------------------------------------------------------------------------------------------            

   if ( present(uerr) ) then 
     ue = uerr
   else 
     ue = 6
   end if    
    
   write(ue,'(//)')  
        
   write(ue,'(a,i0,a)')'Error (models_error #',err,')'

   if ( present(where) ) write(ue,'(a)')'--> in '//trim(where)//':'

   if ( present(err) ) then
      if ( err == 2 ) then
         if ( present(elm) ) then
            write(ue,'(a,i0)')'--> Negative jacobian. Element #',elm
         end if   
         if ( present(matx) ) then
            write(ue,'(a)')'--> Its nodal coordinates:'
            do i = 1, size(matx,2)
               write(ue,'(" --> ",3e13.5)')matx(:,i)
            end do
         end if
      end if      
   end if      
    
   if ( present(msg) ) write(ue,'(a)')'--> '//trim(msg)
       
                    
   write(ue,'(a,/)')'--> STOP'
        
   stop

   END SUBROUTINE modelsDiffusion_error
   
   
END MODULE modelsDiffusion_m
