#include "error.fpp"

MODULE femsystem_m
!
!- Ce module regroupe :
!
!      - les routines qui pilotent la construction du systeme global (matrice et vecteur) selon
!        les elements choisis (ces routines appellent les routines de calcul des matrices et
!        vecteurs elementaires --> module models) ainsi que les grandeurs derivees (flux, 
!        contraintes, etc).
!
!      - les routines d'assemblage de matrices et vecteurs elementaires selon le format de
!        stockage selectionne
!
!  Actuellement : femsystem_statio pour un probleme stationnaire
!  A faire      : femsystem_evolut pour un probleme d'evolution
!-----------------------------------------------------------------------------------R.H. 11/16  
   use kindParameters_m

   use defTypes_m
   use err_m
   use param_m
   
   use constants_m, only: IZERO, RZERO, RONE, UERROR
   use util_m     , only: util_barre_defilement
   
   implicit none
        
CONTAINS

!=============================================================================================    
   SUBROUTINE femsystem_statio ( rqst, mymesh, mydata, femsys, stat, KFelem, Neumann, Dirichlet )
!=============================================================================================    
   use quadrature_m  , only : quadrature_driver
   use element_m     , only : element_driver
   use boundarycond_m, only : boundarycond_dirichlet1
   logical          , intent(in    ) :: rqst(4)   
   type   (mesh_t  ), intent(in    ) :: mymesh
   type   (udata_t ), intent(in    ) :: mydata 
   type   (linsys_t), intent(in out) :: femsys
   type   (err_t   ), intent(in out) :: stat   
   external                          :: KFelem, Neumann, Dirichlet
   optional                          :: Neumann, Dirichlet
!---------------------------------------------------------------------------------------------            
!  Computes the stiffness matrix and the load vector for a standard stationary problem
!---------------------------------------------------------------------------------------------            
!  Inputs
!
!      mymesh : mesh structure (%lnods: connectivities, %coord: nodal coordinates)
!
!      mydata : data structure (%vmatprop: mat. properties, ...)
!
!      femsys : system system structure (%Dof: the d.o.f numbers)
!
!      rqst   : logical tab defining what is requested:
!
!                - rqst(1) = .true. : compute the stiffness matrix %Mat
!
!                - rqst(2) = .true. : compute the load vector %Rhs
!
!                - rqst(3) = .true. : compute the derived quantitites %Str (stresses, etc)
!
!                - rqst(4) = .true. : compute the residual %Rhs - %Mat * %Sol performed
!                                     at element level and put the result into %Rhs 
!
!      KFelem : external routine. Computes the element stiffness and load
!
!      Neumann: external routine. Computes the load corresponding to Neumann bc
!
!  Outputs
!
!     femsys : linear system structure (%Mat: the stiffness matrix, %Rhs: the load vector)   
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=* ), parameter :: HERE = 'femsystem_statio'
   integer(Ikind   )            :: ip, verb, el    
   type   (eldata_t), save      :: eldata 
   logical                      :: request(4)    
   character(len=20)            :: namelemt, namegeom  ! element type and geometry    
!---------------------------------------------------------------------------------------------      

   if ( stat > IZERO ) return 

   verb = verbosity(2)
!
!- Initialize the matrix, the rhs:
!        
   request = rqst
   if ( request(4) ) then
      request(1) = .true.
      request(2) = .true.
   end if
       
   if ( request(1) ) femsys%Mat = RZERO
   if ( request(2) ) femsys%Rhs = RZERO
   !if ( request(3) ) femsys%Str = RZERO

   if ( .not. eldata%allocated ) call eldata%alloc ( ndime, nnode, ndofn, noutp, &
                                                     nipts, nevab, nprop )
!
!- Part of the load coming from Neumann bc:
!
   if ( present(Neumann) .and. request(2) ) then
      call Neumann ( mymesh, mydata, femsys, stat )  
      error_TraceNreturn(stat>IZERO, HERE, stat)
   end if
      
!
!- Get the element type and geometry:
!
   namelemt = trim(adjustl(name_element )) ! name of the finite element
   namegeom = trim(adjustl(name_geometry)) ! name of its simplexe 
!
!- Get the local I.P. coordinates and associates weights:
! 
   call quadrature_driver ( eldata%xIP, eldata%wIP, nipts, ndime, namegeom, stat )
   error_TraceNreturn(stat>IZERO, Here, stat)  
!
!- Compute interpolation functions (%shp) and their local derivatives (%der) at I.P.:  
!     
   do ip = 1, nipts
      call element_driver ( namelemt, ndime, nnode, stat, pnt=eldata%xIP(:,ip), &
                            shp=eldata%shp(:,ip), drv=eldata%der(:,:,ip)        )
      error_TraceNreturn(stat>IZERO, Here, stat)
   end do       
!
!- Loop over the mesh elements
!       
   do el = 1, nelem
    
      eldata%el = el
      
      if ( verb >= 1 .and. request(1) ) &
         call util_barre_defilement ( i=el, n=nelem, di=2_Ikind, cDone='=' )
!
!-    get the material properties (props), the element nodal coordinates (elcoo),  
!     the corresponding d.o.f. # (eldof) and the previous nodal solution (eldep),
!     (useful for non-linear or time-dependent problem for ex.):
!
      call femsystem_locelem ( mymesh, mydata, femsys, eldata )
!
!-    compute the element stiffness matrix and (body) load vector:
!                                         
      call KFelem ( request, eldata, stat ) 
      
      error_TraceNreturn(stat>IZERO, HERE, stat)                     
!
!-    modify the rhs in case of prescribed d.o.f. (Dirichlet bc):
!
      if ( present(Dirichlet) .and. request(2) ) call Dirichlet ( eldata )
!
!-    if requested, replace %F by the residual %F - %K * %U:
!
      if ( request(4) ) eldata%F = eldata%F - matmul(eldata%K, eldata%U)     
!
!-    assemble element entities (%K and %F) into the global ones (%Mat and %Rhs):        
!
      call femsystem_assemb ( request, name_storage, eldata, femsys, stat )

      error_TraceNreturn(stat>IZERO, HERE, stat)                           
!
!-    if requested, store the I.P. coordinates and the derived quantities computed
!     at I.P. locations (postprocessing purpose):
!
      if ( request(3) ) call femsystem_storeElementalValues ( eldata, femsys )

   end do 
    
   END SUBROUTINE femsystem_statio


!=============================================================================================    
   SUBROUTINE femsystem_storeElementalValues ( eldata, femsys )
!=============================================================================================      
   type(eldata_t), intent(in    ) :: eldata
   type(linsys_t), intent(in out) :: femsys
!---------------------------------------------------------------------------------------------         
!  Saves the I.P. coordinates and the derived quantities computed at I.P. into femsys%Str
!-----------------------------------------------------------------------------------R.H. 12/16  
    
!- local variables: -------------------------------------------------------------------------- 
   integer(Ikind) :: ip1, ip2
   real   (Rkind) :: coorIP(ndime,nipts)
!---------------------------------------------------------------------------------------------     

!
!-  Current coordinates of the nipts integration points of the element # el:
!
    coorIP = matmul ( eldata%x, eldata%shp ) 
!
!- Store in femsys%Str the I.P. coordinates and the corresponding derived quantities:
!    
   ip1 = (eldata%el - 1) * nipts + 1 ; ip2 = ip1 + nipts - 1 ! first and last I.P. of el
   
   femsys%Str(      1:ndime      ,ip1:ip2) = coorIP
   femsys%Str(ndime+1:ndime+noutp,ip1:ip2) = eldata%S
   
   END SUBROUTINE femsystem_storeElementalValues


!=============================================================================================    
   SUBROUTINE femsystem_storeNodalValues ( femsys, is_incr, step )
!=============================================================================================      
   type(linsys_t), intent(in out)           :: femsys
   logical       , intent(in    ), optional :: is_incr
   real(Rkind)   , intent(in    ), optional :: step
!---------------------------------------------------------------------------------------------         
!  Saves into femsys%Sol the computed nodal solution completed by the boundary conditions
!-----------------------------------------------------------------------------------R.H. 12/16  
    
!- local variables: -------------------------------------------------------------------------- 
   integer(Ikind) :: id, ip, ieq
   logical        :: is_incr_
   real   (Rkind) :: step_
!---------------------------------------------------------------------------------------------     

   if ( present(step)    ) then ; step_    = step    ; else ; step_    = RONE    ; end if
   if ( present(is_incr) ) then ; is_incr_ = is_incr ; else ; is_incr_ = .false. ; end if
   
   if ( is_incr_ ) then
      do ip = 1, npoin
         do id = 1, ndofn
            ieq = femsys%Dof(id,ip)
            if ( ieq > 0 ) then
               femsys%Sol(id,ip) = femsys%Sol(id,ip) + step_ * femsys%X(ieq)
            else
               femsys%Sol(id,ip) = femsys%bCond(id,-ieq)
            end if
         end do
      end do
   else
      do ip = 1, npoin
         do id = 1, ndofn
            ieq = femsys%Dof(id,ip)
            if ( ieq > 0 ) then
               femsys%Sol(id,ip) = femsys%X(ieq)
            else
               femsys%Sol(id,ip) = femsys%bCond(id,-ieq)
            end if
         end do
      end do
   end if   
   
   END SUBROUTINE femsystem_storeNodalValues
   
    
!=============================================================================================    
   SUBROUTINE femsystem_locelem ( mymesh, mydata, femsys, eldata )
!=============================================================================================      
   type(mesh_t  ), intent(in    ) :: mymesh
   type(udata_t ), intent(in    ) :: mydata
   type(linsys_t), intent(in    ) :: femsys
   type(eldata_t), intent(in out) :: eldata
!---------------------------------------------------------------------------------------------
!  Extracts from the mesh structure, from the data structure and from the fem structure the
!
!     - nnode nodal current (eldata%x) and initial (eldata%x0) coordinates 
!     - ndofn*nnode nodal dof numbers (array eldata%dof)
!     - ndofn*nnode nodal values (array eldata%U) of the previous solution
!     - nprop material properties (array eldata%props)
!     - noutp*nipts derived quantities (e.g. stresses) at the IP (array eldata%S)
!     - set eldata%has_dirichlet = .true. if any of dof of the element is prescribed
!
!  of the element # eldata%el
!-----------------------------------------------------------------------------------R.H. 12/16  
    
!- local variables: -------------------------------------------------------------------------- 
   integer(Ikind) :: in, id, ip, globalDof, localDof, ip1, ip2
!---------------------------------------------------------------------------------------------     

!
!- Get the element material properties:
!       
   eldata%numDom = mymesh % numdom(eldata%el)
   eldata%numMat = mydata % lmatprop (eldata%numDom) 
   
   eldata%props  = mydata % vmatprop(1:nprop,eldata%numMat); 
!
!- Get the element nodes, the initial and current nodal coordinates:
!
   eldata%nod = mymesh % lnods(:, eldata%el )
   eldata%x   = mymesh % coord(:, eldata%nod)
   eldata%x0  = mymesh % coor0(:, eldata%nod)
!
!- Get the element the d.o.f and the nodal previous solution (used for non-linear or 
!  time-dependent problems):        
!
   localDof = 0 ; eldata%has_dirichlet = .false.
   do in = 1, nnode
      ip = eldata%nod(in)
      do id = 1, ndofn
         globalDof = femsys % Dof(id,ip) 
         localDof = localDof + 1
         eldata%dof(localDof) = globalDof
         if ( globalDof < 0 ) then
            eldata%has_dirichlet = .true.
            eldata%U(localDof) = femsys % bCond(id,-globalDof)
         else
            eldata%U(localDof) = femsys % Sol(id,ip)
         endif
!          if ( globalDof > 0 ) then
!             eldata%U(localDof) = femsys % Sol(globalDof)
!          else
!             eldata%U(localDof) = mydata % vdirichl(id,-globalDof)  
!             eldata%has_dirichlet = .true.
!          end if
      end do   
   end do    
   
!
!- Get the element previous derived variables at the nipts I.P.
!
   ip1 = (eldata%el - 1) * nipts + 1
   ip2 = ip1 + nipts - 1
   eldata%S = femsys % Str(ndime+1:ndime+noutp,ip1:ip2)

   END SUBROUTINE femsystem_locelem
               

!=============================================================================================
   SUBROUTINE femsystem_assemb ( request, name_storage, eldata, femsys, stat )
!=============================================================================================    
   logical            , intent(in    ) :: request(*)
   character(len=*   ), intent(in    ) :: name_storage
   type     (eldata_t), intent(in    ) :: eldata
   type     (linsys_t), intent(in out) :: femsys    
   type     (err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------------------    
!  Assembles an elemental matrix Ke into the global one (femsys % Mat) if request(1)=.true.
!  Assembles an elemental vector Fe into the global one (femsys % Rhs) if request(2)=.true.
!
!  Inputs
!
!       eldata%dof  : lists the nodal d.o.f. of the current element
!       eldata%K    : the elemental stiffness matrix 
!       eldata%F    : the elemental load vector
!       name_storage: storage format
!
!       request: logical tab giving what is requested
!                - request(1) = .true.: assemble K
!                - request(2) = .true.: assemble F
!                - request(4) = .true.: assemble F
!       
!  Outputs
!
!      femsys%Mat: the updating global stiffness matrix
!      femsys%Rhs: the updating global right hand side vector
!
!  Version : R.H., nov. 2016
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: Here = 'femsystem_assemb'
   integer  (Ikind)            :: i, j 
!---------------------------------------------------------------------------------------------
    
   if ( request(1) ) then
!
!-    matrix assembling (storage-dependent)
!    
      select case ( trim(name_storage) )
    
         case ('csr')
            call femsystem_assembMcsr     ( eldata%K  , eldata%dof, femsys%mat, &
                                            femsys%row, femsys%col              )
          
         case ('csrsym')
            call femsystem_assembMcsrsym  ( eldata%K  , eldata%dof, femsys%mat, &
                                            femsys%row, femsys%col              )
       
         case ('skyline')    
            call femsystem_assembMskyline ( eldata%K  , eldata%dof, femsys%mat, &
                                            femsys%row                          )
          
         case default
            stat = err_t( stat=UERROR, where=Here, msg="unknown storage format: "// &
                          trim(name_storage) )
            return
          
      end select  
         
   end if
    
   if ( request(2) ) then
!
!-    load assembling
!       
      do i = 1, size(eldata%dof)
         j = eldata%dof(i)
         if ( j > 0 ) femsys%Rhs(j) = femsys%Rhs(j) + eldata%F(i)
      end do   
       
   end if      
    
   END SUBROUTINE femsystem_assemb

    
!=============================================================================================
   SUBROUTINE femsystem_assembMCSR ( Ke, ldof, Mat, Row, Col )
!=============================================================================================    
   integer(Ikind), intent(in    ) :: Row(:), Col(:), ldof(:)
   real   (Rkind), intent(in    ) :: Ke(:,:)
   real   (Rkind), intent(in out) :: Mat(:)
!---------------------------------------------------------------------------------------------    
!  Assembles an element matrix Ke into the global one (Mat) stored in CSR format
!   
!  Version initiale : N. Cerpa 2013. Modifiee : R.H., nov. 2016
!--------------------------------------------------------------------------------------------- 

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i, j, idof, jdof, m, ma, first, last, nevab
!---------------------------------------------------------------------------------------------            
            
   nevab = size(ldof)
             
   do i = 1, nevab
      idof = ldof(i)
      if (idof > 0) then
         last = Row(idof+1) - 1; first = Row(idof)
         do j = 1, nevab
            jdof = ldof(j)
            if (jdof > 0) then
!
!-             Find the position of jdof in Col(first:last), i.e. find ma st Col(ma) = jdof
!
 
             ! Version 1 - without a do loop (using minloc), elegant but not efficient (!):
             !
             !  ma = minloc ( iabs ( Col(first:last) - jdof ), 1 ) + first - 1 !
             !  
             !  if (Col(ma) /= jdof) call femsystem_error &
             !  (msg='in femsystem_assembMCSR: Col(ma) /= jdof')


             ! Version 2 - unpack the row into a work array (as Y. Saad did) of max size n+1.
             !             Faster than the version 1 but slower than the version 3:
             !
             !  do m = first, last
             !     Colinv(Col(m)) = m    ! don't forget to allocate colinv
             !  end do
             !
             !  ma = Colinv(jdof)
             !
             !  if (Col(ma) /= jdof) call femsystem_error &
             !  (msg='in femsystem_assembMCSR: Col(ma) /= jdof')  

                           
             ! Version 3 - simply with a do loop (less elegant but faster than 1 and 2!)
             !             (alternatively: call findpos_in_sortedlist or findpos_in_unsortedlist)
               ma = 0                        
               do m = first, last           
                  if (Col(m)== jdof) then
                     ma = m
                     exit
                  end if
               end do   
                                         
             ! ma = util_findpos_in_sortedlist (col,first,last,jdof) ! par dicothomie. 
                                                                     ! Je ne gagne rien (!?)
      
               if (ma == 0) then
                  print*,'ldof:',ldof(1:nevab)
                  print*,'idof,jdof:',idof,jdof
                  print*,'first,last:',first,last
                  print*,'Col:',Col(first:last)
                  call femsystem_error (msg='ma == 0', where='assembMCSR')
               end if
                      
               Mat(ma) = Mat(ma) + Ke(i,j)
                 
            end if
         end do
      end if
   end do

   END SUBROUTINE femsystem_assembMCSR           


!=============================================================================================
   SUBROUTINE femsystem_assembMCSRsym ( Ke, ldof, Mat, Row, Col )
!=============================================================================================    
   integer(Ikind), intent(in    ) :: Row(:), Col(:), ldof(:)
   real   (Rkind), intent(in    ) :: Ke(:,:)
   real   (Rkind), intent(in out) :: Mat(:)
!---------------------------------------------------------------------------------------------    
!  Assembles an element matrix Ke into the global one (Mat) stored in CSR format
!   
!  Version initiale : N.Cerpa, 2013. Modifiee : R.H., nov. 2016
!--------------------------------------------------------------------------------------------- 

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: nevab, i, j, idof, jdof, m, ma, first, last, nnz
!---------------------------------------------------------------------------------------------            
    
   nevab = size(ldof)    ! total number of d.o.f in the element
   nnz = size(Mat)
        
   do i = 1, nevab
      idof = ldof(i)
      if (idof > 0) then
         first = Row(idof) ; last = Row(idof+1) - 1 
         do j = 1, nevab
            jdof = ldof(j)
            if (jdof > 0 .and. jdof >= idof) then
!
!-             Find the position of jdof in Col(first:last), i.e. find ma st Col(ma) = jdof
!
 
             ! Version 1 - without a do loop (using minloc), elegant but not efficient (!):
             !
             !  ma = minloc ( iabs ( Col(first:last) - jdof ), 1 ) + first - 1 !
             !  
             !  if (Col(ma) /= jdof) call femsystem_error &
             !  (msg='in femsystem_assembMCSR: Col(ma) /= jdof')


             ! Version 2 - unpack the row into a work array (as Y. Saad did) of max size n+1.
             !             Faster than the version 1 but slower than the version 3:
             !
             !  do m = first, last
             !     Colinv(Col(m)) = m    
             !  end do
             !
             !  ma = Colinv(jdof)
             !
             !  if (Col(ma) /= jdof) call femsystem_error &
             !  (msg='in femsystem_assembMCSR: Col(ma) /= jdof')  

                           
             ! Version 3 - simply with a do loop (less elegant but faster than 1 and 2!)
             !             (alternatively: use findpos_in_sortedlist or findpos_in_unsortedlist)
               ma = 0                        
               do m = first, last           
                  if (Col(m)== jdof) then
                     ma = m
                     exit
                  end if
               end do   
                                
              ! ma= util_findpos_in_sortedlist(col,first,last,jdof) ! par dicothomie.
                                                                    ! Je ne gagne rien (!?)
                           
                
               if (ma == 0) call femsystem_error (msg='ima == 0', where='assembMCSRsym')
                
               if (ma > nnz) call femsystem_error (msg='ma > nnz', where='assembMCSRsym')
                     
               Mat(ma) = Mat(ma) + Ke(i,j)
                 
            end if
         end do
      end if
   end do
        
   END SUBROUTINE femsystem_assembMCSRsym     
   
   
!=============================================================================================
   SUBROUTINE femsystem_assembMskyline ( Ke, ldof, Mat, Idiag )
!=============================================================================================    
   integer(Ikind), intent(in    ) :: Idiag(:), ldof(:)
   real   (Rkind), intent(in    ) :: Ke(:,:)
   real   (Rkind), intent(in out) :: Mat(:)
!---------------------------------------------------------------------------------------------    
!  Assembles an elemental matrix Ke into the global one (Mat) stored in SKYLINE SYMMETRIC
!  format
!
!  Version initiale : code Geodyn (J.P. Vilotte). 
!--------------------------------------------------------------------------------------------- 

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: nevab, i, j, idof, jdof, m, ma
!---------------------------------------------------------------------------------------------            
        
   nevab = size(ldof)    ! total number of d.o.f in the element

   do j = 1, nevab
      jdof = ldof(j)
      if (jdof > 0) then
         m = Idiag(jdof) - jdof
         do i = 1, nevab
            idof = ldof(i)
            if (idof > 0 .and. idof <= jdof) then
               ma = m + idof
               Mat(ma) = Mat(ma) + Ke(i,j)
            end if
         end do
      end if
   end do

   END SUBROUTINE femsystem_assembMskyline
    
        
!=============================================================================================
   SUBROUTINE femsystem_error ( uerr, err, where, msg )
!=============================================================================================
   integer  (Ikind), intent(in), optional :: err, uerr
   character(len=*), intent(in), optional :: where, msg
!---------------------------------------------------------------------------------------------    
!  Affichage d'un message d'erreur et arret du programme
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: -------------------------------------------------------------------------- 
   integer(Ikind) :: ue
!---------------------------------------------------------------------------------------------            
    
   if (present(uerr)) then 
     ue = uerr
   else 
     ue = 6
   end if    
       
   write(ue,'(/,a)')'Error (femsystem_error):'
        
   if ( present(err)  ) write(ue,'(a,i0)')'--> error  #',err

   if ( present(where)) write(ue,'(a)')   '--> in femsystem_'//trim(where)
     
   if ( present(msg)  ) write(ue,'(a)')   '--> '//trim(msg)
                   
   write(ue,'(a,/)')'--> STOP'
        
   stop

   END SUBROUTINE femsystem_error
    
   
END MODULE femsystem_m
