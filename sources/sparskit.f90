MODULE sparskit_m

    use kindParameters_m
    use err_m
    
    use constants_m, only: UERROR, IZERO, NLT
    use util_m     , only: util_barre_defilement, i2a => util_intToChar

    implicit none

CONTAINS
  
!------------------------------------------------------------------------------
    SUBROUTINE sparskit_runrc ( n,rhs,sol,ipar,fpar,wk,a,ja,ia,au,jau,ju,solver,verb,stat )
!------------------------------------------------------------------------------
    integer (Ikind) :: n
    integer (Ikind) :: ipar(16), ia(n+1), ja(*), ju(*), jau(*), verb
    real    (Rkind) :: fpar(16), rhs(n), sol(n), wk(*), a(*), au(*)
    type    (err_t), intent(in out) :: stat
   ! external solver
!-----------------------------------------------------------------------------
!   The structure {au, jau, ju} is assumed to have the output from
!   the ILU* routines in ilut.f.
!-----------------------------------------------------------------------------
!   local variables
!
    character(len=*), parameter :: Here = 'sparskit_runrc'
    integer(Ikind) :: its
    real   (Rkind) :: res
    save its,res
    character(len=80) :: buf,buf1,buf2
!-----------------------------------------------------------------------------

    if ( stat > IZERO ) return 

!
!     ipar(2) can be 0, 1, 2, please don't use 3
!
      if ( ipar(2) > 2 ) then
         stat = err_t ( stat = UERROR, where = Here, msg = &
                       'cannot do both left and right preconditioning' )
         return
      endif
!
!     normal execution
!
      its = 0
      res = 0.0_Rkind
 10   call solver ( n, rhs, sol, ipar, fpar, wk )
!
!     output the residuals
!
      if ( ipar(7)/=its .and. verb > 0 ) then
         write(buf1,'(i6)')its
         write(buf2,'(e13.5)')real(res)
         buf = ' iteration # : '//trim(adjustl(buf1))//', residual : '//trim(adjustl(buf2))
         write (6,'(a1,a)',advance='no')char(13),trim(buf)
         flush(6)
         its = ipar(7)
      endif
      res = fpar(5)

      if ( ipar(1)==1 ) then
         call amux(n, wk(ipar(8)), wk(ipar(9)), a, ja, ia)
         goto 10
      else if ( ipar(1)==2 ) then
         call atmux(n, wk(ipar(8)), wk(ipar(9)), a, ja, ia)
         goto 10
      else if ( ipar(1)==3 .or. ipar(1)==5 ) then
         call lusol(n,wk(ipar(8)),wk(ipar(9)),au,jau,ju)
         goto 10
      else if ( ipar(1)==4 .or. ipar(1)==6 ) then
         call lutsol(n,wk(ipar(8)),wk(ipar(9)),au,jau,ju)
         goto 10
      else if ( ipar(1)<=0 ) then
         if ( ipar(1)==0 ) then
            if ( verb > 0 ) then
               print*
               print*
               print*, 'Iterative solver has satisfied convergence test.'
            end if   
         else if ( ipar(1)==-1 ) then
            stat = err_t ( stat = UERROR, where = Here, msg = &
                           'Iterative solver has iterated too many times.' )
            return
         else if ( ipar(1)==-2 ) then
            stat = err_t ( stat = UERROR, where = Here, msg = &
                         'Iterative solver was not given enough work space.' // NLT // &
                         'The work space should at least have '//i2a(ipar(4))//' elements.' )
            return
         else if ( ipar(1)==-3 ) then
            stat = err_t ( stat = UERROR, where = Here, msg = &
                           'Iterative solver is facing a break-down.' )
            return
         else
            if ( verb > 0 ) then
               print*
               print*
               print*, 'Iterative solver terminated. code =', ipar(1)
            end if
         endif
      endif
      if ( verb > 0 ) write (6,*) '# return code =', ipar(1),'	convergence rate =', fpar(7)

      return
    
    END SUBROUTINE sparskit_runrc

!-----------------------------------------------------------------------
    FUNCTION distdot (n, x, ix, y, iy)
!-----------------------------------------------------------------------
    integer(Ikind) :: n, ix, iy
    real   (Rkind) :: distdot, x(*), y(*)
   ! real(Rkind) :: ddot
   ! external ddot
   
    distdot = ddot(n,x,ix,y,iy)
    return
    END FUNCTION distdot

!-----------------------------------------------------------------------
!
!----------------------------------------------------------------------c
!                          S P A R S K I T                             c
!----------------------------------------------------------------------c
!                   ITERATIVE SOLVERS MODULE                           c
!----------------------------------------------------------------------c
! This Version Dated: August 13, 1996. Warning: meaning of some        c
! ============ arguments have changed w.r.t. earlier versions. Some    c
!              Calling sequences may also have changed                 c
!----------------------------------------------------------------------c 
! Contents:                                                            c
!-------------------------preconditioners------------------------------c 
!                                                                      c
! ILUT    : Incomplete LU factorization with dual truncation strategy  c
! ILUTP   : ILUT with column  pivoting                                 c
! ILUD    : ILU with single dropping + diagonal compensation (~MILUT)  c
! ILUDP   : ILUD with column pivoting                                  c
! ILUK    : level-k ILU                                                c
! ILU0    : simple ILU(0) preconditioning                              c
! MILU0   : MILU(0) preconditioning                                    c
!                                                                      c
!----------sample-accelerator-and-LU-solvers---------------------------c 
!                                                                      c
! PGMRES  : preconditioned GMRES solver                                c
! LUSOL   : forward followed by backward triangular solve (Precond.)   c
! LUTSOL  : solving v = (LU)^{-T} u (used for preconditioning)         c
!                                                                      c
!-------------------------utility-routine------------------------------c
!                                                                      c 
! QSPLIT  : quick split routine used by ilut to sort out the k largest c
!           elements in absolute value                                 c
!                                                                      c
!----------------------------------------------------------------------c
!                                                                      c 
! Note: all preconditioners are preprocessors to pgmres.               c
! usage: call preconditioner then call pgmres                          c
!                                                                      c
!----------------------------------------------------------------------c

    SUBROUTINE sparskit_ilut (n  , a, ja, ia, lfil, droptol, alu, jlu, ju, &
                              iwk, w, jw, ierr,verb)
!-----------------------------------------------------------------------
    integer(Ikind) :: n, lfil, iwk, ierr
    integer(Ikind) :: ja(*),ia(n+1),jlu(iwk),ju(n),jw(2*n)    
    Real   (Rkind) :: a(*), alu(iwk), w(*), droptol
!----------------------------------------------------------------------*
!                      *** ILUT preconditioner ***                     *
!      incomplete LU factorization with dual truncation mechanism      *
!----------------------------------------------------------------------*
!     Author: Yousef Saad *May, 5, 1990, Latest revision, August 1996  *
!----------------------------------------------------------------------*
! PARAMETERS                                                           
!-----------                                                           
!
! on entry:
!========== 
! n       = integer. The row dimension of the matrix A. The matrix 
!
! a,ja,ia = matrix stored in Compressed Sparse Row format.              
!
! lfil    = integer. The fill-in parameter. Each row of L and each row
!           of U will have a maximum of lfil elements (excluding the 
!           diagonal element). lfil must be >= 0.
!           ** WARNING: THE MEANING OF LFIL HAS CHANGED WITH RESPECT TO
!           EARLIER VERSIONS. 
!
! droptol = double precision. Sets the threshold for dropping small terms in the
!           factorization. See below for details on dropping strategy.
!
!  
! iwk     = integer. The lengths of arrays alu and jlu. If the arrays
!           are not big enough to store the ILU factorizations, ilut
!           will stop with an error message. 
!
! On return:
!===========
!
! alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
!           the L and U factors together. The diagonal (stored in
!           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
!           contains the i-th row of L (excluding the diagonal entry=1)
!           followed by the i-th row of U.
!
! ju      = integer array of length n containing the pointers to
!           the beginning of each row of U in the matrix alu,jlu.
!
! ierr    = integer. Error message with the following meaning.
!           ierr  = 0    --> successful return.
!           ierr > 0  --> zero pivot encountered at step number ierr.
!           ierr  = -1   --> Error. input matrix may be wrong.
!                            (The elimination process has generated a
!                            row in L or U whose length is >  n.)
!           ierr  = -2   --> The matrix L overflows the array al.
!           ierr  = -3   --> The matrix U overflows the array alu.
!           ierr  = -4   --> Illegal value for lfil.
!           ierr  = -5   --> zero row encountered.
!
! work arrays:
!=============
! jw      = integer work array of length 2*n.
! w       = real work array of length n+1      <=== 
!  
!----------------------------------------------------------------------
! w, ju (1:n) store the working array [1:ii-1 = L-part, ii:n = u] 
! jw(n+1:2n)  stores nonzero indicators
! 
! Notes:
! ------
! The diagonal elements of the input matrix must be  nonzero (at least
! 'structurally'). 
!
!----------------------------------------------------------------------* 
!---- Dual drop strategy works as follows.                             *
!                                                                      *
!     1) Theresholding in L and U as set by droptol. Any element whose *
!        magnitude is less than some tolerance (relative to the abs    *
!        value of diagonal element in u) is dropped.                   *
!                                                                      *
!     2) Keeping only the largest lfil elements in the i-th row of L   * 
!        and the largest lfil elements in the i-th row of U (excluding *
!        diagonal elements).                                           *
!                                                                      *
! Flexibility: one  can use  droptol=0  to get  a strategy  based on   *
! keeping  the largest  elements in  each row  of L  and U.   Taking   *
! droptol /=  0 but lfil=n will give  the usual threshold strategy   *
! (however, fill-in is then mpredictible).                             *
!----------------------------------------------------------------------*
!     locals
      integer(Ikind) :: ju0,k,j1,j2,j,ii,i,lenl,lenu,jj,jrow,jpos,len,verb
      Real   (Rkind) :: tnorm, t, abs, s, fact 
      real           :: t1, t2
      
      if (lfil < 0) goto 998
!-----------------------------------------------------------------------
!     initialize ju0 (points to next element to be added to alu,jlu)
!     and pointer array.
!-----------------------------------------------------------------------
!cc   write(6,*) 'in ilut:',n
      ju0 = n+2
      jlu(1) = ju0
!
!     initialize nonzero indicator array. 
!
!cc   write(6,*) 'in ilut: loop 1',n
      do 1 j=1,n
         jw(n+j)  = 0
 1    continue
!-----------------------------------------------------------------------
!     beginning of main loop.
!-----------------------------------------------------------------------
!cc   write(6,*) 'loop 500, n=',n
      call cpu_time(t1)
      do 500 ii = 1, n
      
         if (verb > 0) call util_barre_defilement (i=ii, n=n, di=2_Ikind, cDone='.')

         j1 = ia(ii)
         j2 = ia(ii+1) - 1
         tnorm = 0.0d0
         do 501 k=j1,j2
            tnorm = tnorm+abs(a(k))
 501     continue
         if (tnorm == 0.0) goto 999
         tnorm = tnorm/real(j2-j1+1)
!     
!     unpack L-part and U-part of row of A in arrays w 
!     
         lenu = 1
         lenl = 0
         jw(ii) = ii
         w(ii) = 0.0
         jw(n+ii) = ii

         do 170  j = j1, j2
            k = ja(j)
            t = a(j)
            if (k < ii) then
               lenl = lenl+1
               jw(lenl) = k
               w(lenl) = t
               jw(n+k) = lenl
            else if (k == ii) then
               w(ii) = t
            else
               lenu = lenu+1
               jpos = ii+lenu-1 
               jw(jpos) = k
               w(jpos) = t
               jw(n+k) = jpos
            endif
 170     continue
         jj = 0
         len = 0 
!     
!     eliminate previous rows
!     
 150     jj = jj+1
         if (jj > lenl) goto 160
!-----------------------------------------------------------------------
!     in order to do the elimination in the correct order we must select
!     the smallest column index among jw(k), k=jj+1, ..., lenl.
!-----------------------------------------------------------------------
         jrow = jw(jj)
         k = jj
!     
!     determine smallest column index
!     
         do 151 j=jj+1,lenl
            if (jw(j) < jrow) then
               jrow = jw(j)
               k = j
            endif
 151     continue
!
         if (k /= jj) then
!     exchange in jw
            j = jw(jj)
            jw(jj) = jw(k)
            jw(k) = j
!     exchange in jr
            jw(n+jrow) = jj
            jw(n+j) = k
!     exchange in w
            s = w(jj)
            w(jj) = w(k)
            w(k) = s
         endif
!
!     zero out element in row by setting jw(n+jrow) to zero.
!     
         jw(n+jrow) = 0
!
!     get the multiplier for row to be eliminated (jrow).
!     
         fact = w(jj)*alu(jrow)
         if (abs(fact) <= droptol) goto 150
!     
!     combine current row and row jrow
!
         do 203 k = ju(jrow), jlu(jrow+1)-1
            s = fact*alu(k)
            j = jlu(k)
            jpos = jw(n+j)
            if (j >= ii) then
!     
!     dealing with upper part.
!     
               if (jpos == 0) then
!
!     this is a fill-in element
!     
                  lenu = lenu+1
                  if (lenu > n) goto 995
                  i = ii+lenu-1
                  jw(i) = j
                  jw(n+j) = i
                  w(i) = - s
               else
!
!     this is not a fill-in element 
!
                  w(jpos) = w(jpos) - s
               endif
            else
!     
!     dealing  with lower part.
!     
               if (jpos == 0) then
!
!     this is a fill-in element
!     
                  lenl = lenl+1
                  if (lenl > n) goto 995
                  jw(lenl) = j
                  jw(n+j) = lenl
                  w(lenl) = - s
               else
!     
!     this is not a fill-in element 
!     
                  w(jpos) = w(jpos) - s
               endif
            endif
 203     continue
!     
!     store this pivot element -- (from left to right -- no danger of
!     overlap with the working elements in L (pivots). 
!     
         len = len+1 
         w(len) = fact
         jw(len)  = jrow
         goto 150
 160     continue
!     
!     reset double-pointer to zero (U-part)
!     
         do 308 k=1, lenu
            jw(n+jw(ii+k-1)) = 0
 308     continue
!     
!     update L-matrix
!     
         lenl = len 
!riad         len = min0(lenl,lfil)
         len = min(lenl,lfil)
!     
!     sort by quick-split
!
         call qsplit (w,jw,lenl,len)
!
!     store L-part
! 
         do 204 k=1, len 
            if (ju0 > iwk) goto 996
            alu(ju0) =  w(k)
            jlu(ju0) =  jw(k)
            ju0 = ju0+1
 204     continue
!     
!     save pointer to beginning of row ii of U
!     
         ju(ii) = ju0
!
!     update U-matrix -- first apply dropping strategy 
!
         len = 0
         do k=1, lenu-1
            if (abs(w(ii+k)) > droptol*tnorm) then 
!           if (abs(w(ii+k)) > droptol*abs(w(ii))) then    ! new version
               len = len+1
               w(ii+len) = w(ii+k) 
               jw(ii+len) = jw(ii+k) 
            endif
         enddo
         lenu = len+1
!riad         len = min0(lenu,lfil)
         len = min(lenu,lfil)

         call qsplit (w(ii+1), jw(ii+1), lenu-1,len)
!
!     copy
! 
         t = abs(w(ii))
         if (len + ju0 > iwk) goto 997
!cc      write(6,*) 'loop 302, ii,ii+len-1',ii,ii+len-1,ju0
         do 302 k=ii+1,ii+len-1 
            jlu(ju0) = jw(k)
            alu(ju0) = w(k)
            t = t + abs(w(k) )
            ju0 = ju0+1
 302     continue
!     
!     store inverse of diagonal element of u
!     
         if (w(ii) == 0.0) w(ii) = (0.0001 + droptol)*tnorm
     
         alu(ii) = 1.0d0/ w(ii) 
!     
!     update pointer to beginning of next row of U.
!     
         jlu(ii+1) = ju0
!-----------------------------------------------------------------------
!     end main loop
!-----------------------------------------------------------------------
 500  continue
      call cpu_time(t2)
      if (verb > 0) print*,'temps total factorisation ILUT:',t2-t1
      ierr = 0
      return
!
!     incomprehensible error. Matrix must be wrong.
!     
 995  ierr = -1
      return
!     
!     insufficient storage in L.
!     
 996  ierr = -2
      return
!     
!     insufficient storage in U.
!     
 997  ierr = -3
      return
!     
!     illegal lfil entered.
!     
 998  ierr = -4
      return
!     
!     zero row encountered
!     
 999  ierr = -5
      return
!----------------end-of-ilut--------------------------------------------
!-----------------------------------------------------------------------
    END SUBROUTINE sparskit_ilut

!----------------------------------------------------------------------
    SUBROUTINE sparskit_ilutp (n,a,ja,ia,lfil,droptol,permtol,mbloc,alu, &
                      jlu,ju,iwk,w,jw,iperm,ierr)
!-----------------------------------------------------------------------
    integer(Ikind) :: n,ja(*),ia(n+1),lfil,jlu(*),ju(n),jw(2*n),iwk,     &
                      iperm(2*n),ierr
    real   (Rkind) :: a(*), alu(*), w(n), droptol
!----------------------------------------------------------------------*
!       *** ILUTP preconditioner -- ILUT with pivoting  ***            *
!      incomplete LU factorization with dual truncation mechanism      *
!----------------------------------------------------------------------*
! author Yousef Saad *Sep 8, 1993 -- Latest revision, August 1996.     *
!----------------------------------------------------------------------*
! on entry:
!==========
! n       = integer. The dimension of the matrix A.
!
! a,ja,ia = matrix stored in Compressed Sparse Row format.
!           ON RETURN THE COLUMNS OF A ARE PERMUTED. SEE BELOW FOR 
!           DETAILS. 
!
! lfil    = integer. The fill-in parameter. Each row of L and each row
!           of U will have a maximum of lfil elements (excluding the 
!           diagonal element). lfil must be >= 0.
!           ** WARNING: THE MEANING OF LFIL HAS CHANGED WITH RESPECT TO
!           EARLIER VERSIONS. 
!
! droptol = double precision. Sets the threshold for dropping small terms in the
!           factorization. See below for details on dropping strategy.
!
! lfil    = integer. The fill-in parameter. Each row of L and
!           each row of U will have a maximum of lfil elements.
!           WARNING: THE MEANING OF LFIL HAS CHANGED WITH RESPECT TO
!           EARLIER VERSIONS. 
!           lfil must be >= 0.
!
! permtol = tolerance ratio used to  determne whether or not to permute
!           two columns.  At step i columns i and j are permuted when 
!
!                     abs(a(i,j))*permtol > abs(a(i,i))
!
!           [0 --> never permute; good values 0.1 to 0.01]
!
! mbloc   = if desired, permuting can be done only within the diagonal
!           blocks of size mbloc. Useful for PDE problems with several
!           degrees of freedom.. If feature not wanted take mbloc=n.
!
!  
! iwk     = integer. The lengths of arrays alu and jlu. If the arrays
!           are not big enough to store the ILU factorizations, ilut
!           will stop with an error message. 
!
! On return:
!===========
!
! alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
!           the L and U factors together. The diagonal (stored in
!           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
!           contains the i-th row of L (excluding the diagonal entry=1)
!           followed by the i-th row of U.
!
! ju      = integer array of length n containing the pointers to
!           the beginning of each row of U in the matrix alu,jlu.
!
! iperm   = contains the permutation arrays. 
!           iperm(1:n) = old numbers of unknowns
!           iperm(n+1:2*n) = reverse permutation = new unknowns.
!
! ierr    = integer. Error message with the following meaning.
!           ierr  = 0    --> successful return.
!           ierr > 0  --> zero pivot encountered at step number ierr.
!           ierr  = -1   --> Error. input matrix may be wrong.
!                            (The elimination process has generated a
!                            row in L or U whose length is >  n.)
!           ierr  = -2   --> The matrix L overflows the array al.
!           ierr  = -3   --> The matrix U overflows the array alu.
!           ierr  = -4   --> Illegal value for lfil.
!           ierr  = -5   --> zero row encountered.
!
! work arrays:
!=============
! jw      = integer work array of length 2*n.
! w       = real work array of length n 
!
! IMPORTANR NOTE:
! --------------
! TO AVOID PERMUTING THE SOLUTION VECTORS ARRAYS FOR EACH LU-SOLVE, 
! THE MATRIX A IS PERMUTED ON RETURN. [all column indices are
! changed]. SIMILARLY FOR THE U MATRIX. 
! To permute the matrix back to its original state use the loop:
!
!      do k=ia(1), ia(n+1)-1
!         ja(k) = iperm(ja(k)) 
!      enddo
! 
!-----------------------------------------------------------------------
!     local variables
!
      integer(Ikind) :: k,i,j,jrow,ju0,ii,j1,j2,jpos,len,imax,lenu,lenl, &
                        jj,mbloc,icut
      real   (Rkind) :: s, tmp, tnorm,xmax,xmax0, fact, abs, t, permtol
!     
      if (lfil < 0) goto 998
!----------------------------------------------------------------------- 
!     initialize ju0 (points to next element to be added to alu,jlu)
!     and pointer array.
!-----------------------------------------------------------------------
      ju0 = n+2
      jlu(1) = ju0
!
!  integer double pointer array.
!
      do 1 j=1, n
         jw(n+j)  = 0
         iperm(j) = j
         iperm(n+j) = j
 1    continue
!-----------------------------------------------------------------------
!     beginning of main loop.
!-----------------------------------------------------------------------
      do 500 ii = 1, n
         j1 = ia(ii)
         j2 = ia(ii+1) - 1
         tnorm = 0.0d0
         do 501 k=j1,j2
            tnorm = tnorm+abs(a(k))
 501     continue
         if (tnorm == 0.0) goto 999
         tnorm = tnorm/(j2-j1+1)
!
!     unpack L-part and U-part of row of A in arrays  w  --
!
         lenu = 1
         lenl = 0
         jw(ii) = ii
         w(ii) = 0.0
         jw(n+ii) = ii
!
         do 170  j = j1, j2
            k = iperm(n+ja(j))
            t = a(j)
            if (k < ii) then
               lenl = lenl+1
               jw(lenl) = k
               w(lenl) = t
               jw(n+k) = lenl
            else if (k == ii) then
               w(ii) = t
            else
               lenu = lenu+1
               jpos = ii+lenu-1 
               jw(jpos) = k
               w(jpos) = t
               jw(n+k) = jpos
            endif
 170     continue
         jj = 0
         len = 0 
!
!     eliminate previous rows
!
 150     jj = jj+1
         if (jj > lenl) goto 160
!-----------------------------------------------------------------------
!     in order to do the elimination in the correct order we must select
!     the smallest column index among jw(k), k=jj+1, ..., lenl.
!-----------------------------------------------------------------------
         jrow = jw(jj)
         k = jj
!
!     determine smallest column index
!
         do 151 j=jj+1,lenl
            if (jw(j) < jrow) then
               jrow = jw(j)
               k = j
            endif
 151     continue
!
         if (k /= jj) then
!     exchange in jw
            j = jw(jj)
            jw(jj) = jw(k)
            jw(k) = j
!     exchange in jr
            jw(n+jrow) = jj
            jw(n+j) = k
!     exchange in w
            s = w(jj)
            w(jj) = w(k)
            w(k) = s
         endif
!
!     zero out element in row by resetting jw(n+jrow) to zero.
!     
         jw(n+jrow) = 0
!
!     get the multiplier for row to be eliminated: jrow
!
         fact = w(jj)*alu(jrow)
!
!     drop term if small
!     
         if (abs(fact) <= droptol) goto 150
!
!     combine current row and row jrow
!
         do 203 k = ju(jrow), jlu(jrow+1)-1
            s = fact*alu(k)
!     new column number
            j = iperm(n+jlu(k))
            jpos = jw(n+j)
            if (j >= ii) then
!
!     dealing with upper part.
!
               if (jpos == 0) then
!
!     this is a fill-in element
!
                  lenu = lenu+1
                  i = ii+lenu-1 
                  if (lenu > n) goto 995
                  jw(i) = j
                  jw(n+j) = i 
                  w(i) = - s
               else
!     no fill-in element --
                  w(jpos) = w(jpos) - s
               endif
            else
!
!     dealing with lower part.
!
               if (jpos == 0) then
!
!     this is a fill-in element
!
                 lenl = lenl+1
                 if (lenl > n) goto 995
                 jw(lenl) = j
                 jw(n+j) = lenl
                 w(lenl) = - s
              else
!
!     this is not a fill-in element
!
                 w(jpos) = w(jpos) - s
              endif
           endif
 203	continue
!     
!     store this pivot element -- (from left to right -- no danger of
!     overlap with the working elements in L (pivots). 
!     
        len = len+1 
        w(len) = fact
        jw(len)  = jrow
	goto 150
 160    continue
!
!     reset double-pointer to zero (U-part)
!     
        do 308 k=1, lenu
           jw(n+jw(ii+k-1)) = 0
 308	continue
!
!     update L-matrix
!
        lenl = len 
!riad        len = min0(lenl,lfil)
        len = min(lenl,lfil)
!     
!     sort by quick-split
!
        call qsplit (w,jw,lenl,len)
!
!     store L-part -- in original coordinates ..
!
        do 204 k=1, len
           if (ju0 > iwk) goto 996
           alu(ju0) =  w(k)  
           jlu(ju0) = iperm(jw(k))
           ju0 = ju0+1
 204    continue
!
!     save pointer to beginning of row ii of U
!
        ju(ii) = ju0
!
!     update U-matrix -- first apply dropping strategy 
!
         len = 0
         do k=1, lenu-1
            if (abs(w(ii+k)) > droptol*tnorm) then 
               len = len+1
               w(ii+len) = w(ii+k) 
               jw(ii+len) = jw(ii+k) 
            endif
         enddo
         lenu = len+1
!riad         len = min0(lenu,lfil)
         len = min(lenu,lfil)
         call qsplit (w(ii+1), jw(ii+1), lenu-1,len)
!
!     determine next pivot -- 
!
        imax = ii
        xmax = abs(w(imax))
        xmax0 = xmax
        icut = ii - 1 + mbloc - mod(ii-1,mbloc)
        do k=ii+1,ii+len-1
           t = abs(w(k))
           if (t > xmax .and. t*permtol > xmax0 .and. jw(k) <= icut) then
              imax = k
              xmax = t
           endif
        enddo
!
!     exchange w's
!
        tmp = w(ii)
        w(ii) = w(imax)
        w(imax) = tmp
!
!     update iperm and reverse iperm
!
        j = jw(imax)
        i = iperm(ii)
        iperm(ii) = iperm(j)
        iperm(j) = i
!
!     reverse iperm
!
        iperm(n+iperm(ii)) = ii
        iperm(n+iperm(j)) = j
!----------------------------------------------------------------------- 
!
        if (len + ju0 > iwk) goto 997
!
!     copy U-part in original coordinates
!     
        do 302 k=ii+1,ii+len-1 
           jlu(ju0) = iperm(jw(k))
           alu(ju0) = w(k)
           ju0 = ju0+1
 302	continue
!
!     store inverse of diagonal element of u
!
        if (w(ii) == 0.0) w(ii) = (1.0D-4 + droptol)*tnorm
        alu(ii) = 1.0d0/ w(ii) 
!
!     update pointer to beginning of next row of U.
!
	jlu(ii+1) = ju0
!-----------------------------------------------------------------------
!     end main loop
!-----------------------------------------------------------------------
 500  continue
!
!     permute all column indices of LU ...
!
      do k = jlu(1),jlu(n+1)-1
         jlu(k) = iperm(n+jlu(k))
      enddo
!
!     ...and of A
!
      do k=ia(1), ia(n+1)-1
         ja(k) = iperm(n+ja(k))
      enddo
!
      ierr = 0
      return
!
!     incomprehensible error. Matrix must be wrong.
!
 995  ierr = -1
      return
!
!     insufficient storage in L.
!
 996  ierr = -2
      return
!
!     insufficient storage in U.
!
 997  ierr = -3
      return
!
!     illegal lfil entered.
!
 998  ierr = -4
      return
!
!     zero row encountered
!
 999  ierr = -5
      return
!----------------end-of-ilutp-------------------------------------------
!-----------------------------------------------------------------------
    END SUBROUTINE sparskit_ilutp

!-----------------------------------------------------------------------
    SUBROUTINE sparskit_ilud(n,a,ja,ia,alph,tol,alu,jlu,ju,iwk,w,jw,ierr)
!-----------------------------------------------------------------------
    integer(Ikind) :: n
    integer(Ikind) :: ja(*), ia(n+1), jlu(*), ju(n), jw(2*n), iwk, ierr    
    real   (Rkind) :: a(*), alu(*), w(2*n), tol, alph 
!----------------------------------------------------------------------*
!                     *** ILUD preconditioner ***                      *
!    incomplete LU factorization with standard droppoing strategy      *
!----------------------------------------------------------------------*
! Author: Yousef Saad * Aug. 1995 --                                   * 
!----------------------------------------------------------------------*
! This routine computes the ILU factorization with standard threshold  *
! dropping: at i-th step of elimination, an element a(i,j) in row i is *
! dropped  if it satisfies the criterion:                              *
!                                                                      *
!  abs(a(i,j)) < tol * [average magnitude of elements in row i of A]   *
!                                                                      *
! There is no control on memory size required for the factors as is    *
! done in ILUT. This routines computes also various diagonal compensa- * 
! tion ILU's such MILU. These are defined through the parameter alph   *
!----------------------------------------------------------------------* 
! on entry:
!========== 
! n       = integer. The row dimension of the matrix A. The matrix 
!
! a,ja,ia = matrix stored in Compressed Sparse Row format              
!
! alph    = diagonal compensation parameter -- the term: 
!
!           alph*(sum of all dropped out elements in a given row) 
!
!           is added to the diagonal element of U of the factorization 
!           Thus: alph = 0 ---> ~ ILU with threshold,
!                 alph = 1 ---> ~ MILU with threshold. 
! 
! tol     = Threshold parameter for dropping small terms in the
!           factorization. During the elimination, a term a(i,j) is 
!           dropped whenever abs(a(i,j)) < tol * [weighted norm of
!           row i]. Here weighted norm = 1-norm / number of nnz 
!           elements in the row. 
!  
! iwk     = The length of arrays alu and jlu -- this routine will stop
!           if storage for the factors L and U is not sufficient 
!
! On return:
!=========== 
!
! alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
!           the L and U factors together. The diagonal (stored in
!           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
!           contains the i-th row of L (excluding the diagonal entry=1)
!           followed by the i-th row of U.
!
! ju      = integer array of length n containing the pointers to
!           the beginning of each row of U in the matrix alu,jlu.
!
! ierr    = integer. Error message with the following meaning.
!           ierr  = 0    --> successful return.
!           ierr > 0  --> zero pivot encountered at step number ierr.
!           ierr  = -1   --> Error. input matrix may be wrong.
!                            (The elimination process has generated a
!                            row in L or U whose length is >  n.)
!           ierr  = -2   --> Insufficient storage for the LU factors --
!                            arrays alu/ jalu are  overflowed. 
!           ierr  = -3   --> Zero row encountered.
!
! Work Arrays:
!=============
! jw      = integer work array of length 2*n.
! w       = real work array of length n 
!  
!----------------------------------------------------------------------
!
! w, ju (1:n) store the working array [1:ii-1 = L-part, ii:n = u] 
! jw(n+1:2n)  stores the nonzero indicator. 
! 
! Notes:
! ------
! All diagonal elements of the input matrix must be  nonzero.
!
!----------------------------------------------------------------------- 
!     locals
      integer(Ikind) :: ju0,k,j1,j2,j,ii,i,lenl,lenu,jj,jrow,jpos,len 
      real   (Rkind) :: tnorm, t, abs, s, fact, dropsum  
!-----------------------------------------------------------------------
!     initialize ju0 (points to next element to be added to alu,jlu)
!     and pointer array.
!-----------------------------------------------------------------------
      ju0 = n+2
      jlu(1) = ju0
!
!     initialize nonzero indicator array. 
!
      do 1 j=1,n
         jw(n+j)  = 0
 1    continue
!-----------------------------------------------------------------------
!     beginning of main loop.
!-----------------------------------------------------------------------
      do 500 ii = 1, n
         j1 = ia(ii)
         j2 = ia(ii+1) - 1
         dropsum = 0.0d0 
         tnorm = 0.0d0
         do 501 k=j1,j2
            tnorm = tnorm + abs(a(k)) 
 501     continue
         if (tnorm == 0.0) goto 997
         tnorm = tnorm / real(j2-j1+1) 
!     
!     unpack L-part and U-part of row of A in arrays w 
!     
         lenu = 1
         lenl = 0
         jw(ii) = ii
         w(ii) = 0.0
         jw(n+ii) = ii
!
         do 170  j = j1, j2
            k = ja(j)
            t = a(j)
            if (k < ii) then
               lenl = lenl+1
               jw(lenl) = k
               w(lenl) = t
               jw(n+k) = lenl
            else if (k == ii) then
               w(ii) = t
            else
               lenu = lenu+1
               jpos = ii+lenu-1 
               jw(jpos) = k
               w(jpos) = t
               jw(n+k) = jpos
            endif
 170     continue
         jj = 0
         len = 0 
!     
!     eliminate previous rows
!     
 150     jj = jj+1
         if (jj > lenl) goto 160
!-----------------------------------------------------------------------
!     in order to do the elimination in the correct order we must select
!     the smallest column index among jw(k), k=jj+1, ..., lenl.
!-----------------------------------------------------------------------
         jrow = jw(jj)
         k = jj
!     
!     determine smallest column index
!     
         do 151 j=jj+1,lenl
            if (jw(j) < jrow) then
               jrow = jw(j)
               k = j
            endif
 151     continue
!
         if (k /= jj) then
!     exchange in jw
            j = jw(jj)
            jw(jj) = jw(k)
            jw(k) = j
!     exchange in jr
            jw(n+jrow) = jj
            jw(n+j) = k
!     exchange in w
            s = w(jj)
            w(jj) = w(k)
            w(k) = s
         endif
!
!     zero out element in row by setting resetting jw(n+jrow) to zero.
!     
         jw(n+jrow) = 0
!
!     drop term if small
!     
!         if (abs(w(jj)) <= tol*tnorm) then
!            dropsum = dropsum + w(jj) 
!            goto 150
!         endif
!     
!     get the multiplier for row to be eliminated (jrow).
!     
         fact = w(jj)*alu(jrow)
!
!     drop term if small
!     
         if (abs(fact) <= tol) then
            dropsum = dropsum + w(jj) 
            goto 150
         endif
!     
!     combine current row and row jrow
!
         do 203 k = ju(jrow), jlu(jrow+1)-1
            s = fact*alu(k)
            j = jlu(k)
            jpos = jw(n+j)
            if (j >= ii) then
!     
!     dealing with upper part.
!     
               if (jpos == 0) then
!
!     this is a fill-in element
!     
                  lenu = lenu+1
                  if (lenu > n) goto 995
                  i = ii+lenu-1
                  jw(i) = j
                  jw(n+j) = i
                  w(i) = - s
               else
!
!     this is not a fill-in element 
!
                  w(jpos) = w(jpos) - s
               endif
            else
!     
!     dealing with lower part.
!     
               if (jpos == 0) then
!
!     this is a fill-in element
!
                  lenl = lenl+1
                  if (lenl > n) goto 995
                  jw(lenl) = j
                  jw(n+j) = lenl
                  w(lenl) = - s
               else
!
!     this is not a fill-in element 
!
                  w(jpos) = w(jpos) - s
               endif
            endif
 203     continue
         len = len+1 
         w(len) = fact
         jw(len)  = jrow
         goto 150
 160     continue
!     
!     reset double-pointer to zero (For U-part only)
!     
         do 308 k=1, lenu
            jw(n+jw(ii+k-1)) = 0
 308     continue
!
!     update l-matrix
!
         do 204 k=1, len
            if (ju0 > iwk) goto 996
            alu(ju0) =  w(k) 
            jlu(ju0) =  jw(k)
            ju0 = ju0+1
 204     continue
!     
!     save pointer to beginning of row ii of U
!     
         ju(ii) = ju0
!
!     go through elements in U-part of w to determine elements to keep
!
         len = 0
         do k=1, lenu-1
!            if (abs(w(ii+k)) > tnorm*tol) then 
            if (abs(w(ii+k)) > abs(w(ii))*tol) then 
               len = len+1
               w(ii+len) = w(ii+k) 
               jw(ii+len) = jw(ii+k)
            else
               dropsum = dropsum + w(ii+k) 
            endif
         enddo
!
!     now update u-matrix
!
         if (ju0 + len-1 > iwk) goto 996
         do 302 k=ii+1,ii+len
            jlu(ju0) = jw(k)
            alu(ju0) = w(k)
            ju0 = ju0+1
 302     continue
!
!     define diagonal element 
! 
         w(ii) = w(ii) + alph*dropsum 
!
!     store inverse of diagonal element of u
!              
         if (w(ii) == 0.0) w(ii) = (0.0001 + tol)*tnorm
!     
         alu(ii) = 1.0d0/ w(ii) 
!     
!     update pointer to beginning of next row of U.
!     
         jlu(ii+1) = ju0
!-----------------------------------------------------------------------
!     end main loop
!-----------------------------------------------------------------------
 500  continue
      ierr = 0
      return
!
!     incomprehensible error. Matrix must be wrong.
!     
 995  ierr = -1
      return
!     
!     insufficient storage in alu/ jlu arrays for  L / U factors 
!     
 996  ierr = -2
      return
!     
!     zero row encountered
!     
 997  ierr = -3 
      return
!----------------end-of-ilud  ------------------------------------------
!-----------------------------------------------------------------------
    END SUBROUTINE sparskit_ilud
!----------------------------------------------------------------------
    SUBROUTINE sparskit_iludp (n,a,ja,ia,alph,droptol,permtol,mbloc,alu,  &
                               jlu,ju,iwk,w,jw,iperm,ierr)
!-----------------------------------------------------------------------
    integer(Ikind) :: n,ja(*),ia(n+1),mbloc,jlu(*),ju(n),jw(2*n),iwk,     &
                      iperm(2*n),ierr
    real   (Rkind) :: a(*), alu(*), w(2*n), alph, droptol, permtol 
!----------------------------------------------------------------------*
!                     *** ILUDP preconditioner ***                     *
!    incomplete LU factorization with standard droppoing strategy      *
!    and column pivoting                                               * 
!----------------------------------------------------------------------*
! author Yousef Saad -- Aug 1995.                                      *
!----------------------------------------------------------------------*
! on entry:
!==========
! n       = integer. The dimension of the matrix A.
!
! a,ja,ia = matrix stored in Compressed Sparse Row format.
!           ON RETURN THE COLUMNS OF A ARE PERMUTED.
!
! alph    = diagonal compensation parameter -- the term: 
!
!           alph*(sum of all dropped out elements in a given row) 
!
!           is added to the diagonal element of U of the factorization 
!           Thus: alph = 0 ---> ~ ILU with threshold,
!                 alph = 1 ---> ~ MILU with threshold. 
! 
! droptol = tolerance used for dropping elements in L and U.
!           elements are dropped if they are < norm(row) x droptol
!           row = row being eliminated
!
! permtol = tolerance ratio used for determning whether to permute
!           two columns.  Two columns are permuted only when 
!           abs(a(i,j))*permtol > abs(a(i,i))
!           [0 --> never permute; good values 0.1 to 0.01]
!
! mbloc   = if desired, permuting can be done only within the diagonal
!           blocks of size mbloc. Useful for PDE problems with several
!           degrees of freedom.. If feature not wanted take mbloc=n.
!
! iwk     = integer. The declared lengths of arrays alu and jlu
!           if iwk is not large enough the code will stop prematurely
!           with ierr = -2 or ierr = -3 (see below).
!
! On return:
!===========
!
! alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
!           the L and U factors together. The diagonal (stored in
!           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
!           contains the i-th row of L (excluding the diagonal entry=1)
!           followed by the i-th row of U.
!
! ju      = integer array of length n containing the pointers to
!           the beginning of each row of U in the matrix alu,jlu.
! iperm   = contains the permutation arrays ..
!           iperm(1:n) = old numbers of unknowns
!           iperm(n+1:2*n) = reverse permutation = new unknowns.
!
! ierr    = integer. Error message with the following meaning.
!           ierr  = 0    --> successful return.
!           ierr > 0  --> zero pivot encountered at step number ierr.
!           ierr  = -1   --> Error. input matrix may be wrong.
!                            (The elimination process has generated a
!                            row in L or U whose length is >  n.)
!           ierr  = -2   --> The L/U matrix overflows the arrays alu,jlu
!           ierr  = -3   --> zero row encountered.
!
! work arrays:
!=============
! jw      = integer work array of length 2*n.
! w       = real work array of length 2*n 
!
! Notes:
! ------
! IMPORTANT: TO AVOID PERMUTING THE SOLUTION VECTORS ARRAYS FOR EACH 
! LU-SOLVE, THE MATRIX A IS PERMUTED ON RETURN. [all column indices are
! changed]. SIMILARLY FOR THE U MATRIX. 
! To permute the matrix back to its original state use the loop:
!
!      do k=ia(1), ia(n+1)-1
!         ja(k) = perm(ja(k)) 
!      enddo
! 
!-----------------------------------------------------------------------
!     local variables
!
      integer(Ikind) :: k,i,j,jrow,ju0,ii,j1,j2,jpos,len,imax,lenu,lenl,jj,icut
      real   (Rkind) ::  s,tmp,tnorm,xmax,xmax0,fact,abs,t,dropsum 
!----------------------------------------------------------------------- 
!     initialize ju0 (points to next element to be added to alu,jlu)
!     and pointer array.
!-----------------------------------------------------------------------
      ju0 = n+2
      jlu(1) = ju0
!
!  integer double pointer array.
!
      do 1 j=1,n
         jw(n+j)  = 0
         iperm(j) = j
         iperm(n+j) = j
 1    continue
!-----------------------------------------------------------------------
!     beginning of main loop.
!-----------------------------------------------------------------------
      do 500 ii = 1, n
         j1 = ia(ii)
         j2 = ia(ii+1) - 1
         dropsum = 0.0d0 
         tnorm = 0.0d0
         do 501 k=j1,j2
            tnorm = tnorm+abs(a(k))
 501     continue
         if (tnorm == 0.0) goto 997
         tnorm = tnorm/(j2-j1+1)
!
!     unpack L-part and U-part of row of A in arrays  w  --
!
         lenu = 1
         lenl = 0
         jw(ii) = ii
         w(ii) = 0.0
         jw(n+ii) = ii

         do 170  j = j1, j2
            k = iperm(n+ja(j))
            t = a(j)
            if (k < ii) then
               lenl = lenl+1
               jw(lenl) = k
               w(lenl) = t
               jw(n+k) = lenl
            else if (k == ii) then
               w(ii) = t
            else
               lenu = lenu+1
               jpos = ii+lenu-1 
               jw(jpos) = k
               w(jpos) = t
               jw(n+k) = jpos
            endif
 170     continue
         jj = 0
         len = 0 
!
!     eliminate previous rows
!
 150     jj = jj+1
         if (jj > lenl) goto 160
!-----------------------------------------------------------------------
!     in order to do the elimination in the correct order we must select
!     the smallest column index among jw(k), k=jj+1, ..., lenl.
!-----------------------------------------------------------------------
         jrow = jw(jj)
         k = jj
!
!     determine smallest column index
!
         do 151 j=jj+1,lenl
            if (jw(j) < jrow) then
               jrow = jw(j)
               k = j
            endif
 151     continue
!
         if (k /= jj) then
!     exchange in jw
            j = jw(jj)
            jw(jj) = jw(k)
            jw(k) = j
!     exchange in jr
            jw(n+jrow) = jj
            jw(n+j) = k
!     exchange in w
            s = w(jj)
            w(jj) = w(k)
            w(k) = s
         endif
!
!     zero out element in row by resetting jw(n+jrow) to zero.
!     
         jw(n+jrow) = 0
!
!     drop term if small
!     
         if (abs(w(jj)) <= droptol*tnorm) then
            dropsum = dropsum + w(jj) 
            goto 150
         endif      
!
!     get the multiplier for row to be eliminated: jrow
!
         fact = w(jj)*alu(jrow)
!
!     combine current row and row jrow
!
         do 203 k = ju(jrow), jlu(jrow+1)-1
            s = fact*alu(k)
!     new column number
            j = iperm(n+jlu(k))
            jpos = jw(n+j)
!
!     if fill-in element is small then disregard:
!     
            if (j >= ii) then
!
!     dealing with upper part.
!
               if (jpos == 0) then
!     this is a fill-in element
                  lenu = lenu+1
                  i = ii+lenu-1 
                  if (lenu > n) goto 995
                  jw(i) = j
                  jw(n+j) = i 
                  w(i) = - s
               else
!     no fill-in element --
                  w(jpos) = w(jpos) - s
               endif
            else
!
!     dealing with lower part.
!
               if (jpos == 0) then
!     this is a fill-in element
                 lenl = lenl+1
                 if (lenl > n) goto 995
                 jw(lenl) = j
                 jw(n+j) = lenl
                 w(lenl) = - s
              else
!     no fill-in element --
                 w(jpos) = w(jpos) - s
              endif
           endif
 203	continue
        len = len+1 
        w(len) = fact
        jw(len)  = jrow
	goto 150
 160    continue
!
!     reset double-pointer to zero (U-part)
!     
        do 308 k=1, lenu
           jw(n+jw(ii+k-1)) = 0
 308	continue
!
!     update L-matrix
!
        do 204 k=1, len
           if (ju0 > iwk) goto 996
           alu(ju0) =  w(k)
           jlu(ju0) = iperm(jw(k))
           ju0 = ju0+1
 204    continue
!
!     save pointer to beginning of row ii of U
!
        ju(ii) = ju0
!
!     update u-matrix -- first apply dropping strategy 
!
         len = 0
         do k=1, lenu-1
            if (abs(w(ii+k)) > tnorm*droptol) then 
               len = len+1
               w(ii+len) = w(ii+k) 
               jw(ii+len) = jw(ii+k) 
            else
               dropsum = dropsum + w(ii+k) 
            endif
         enddo
!
        imax = ii
        xmax = abs(w(imax))
        xmax0 = xmax
        icut = ii - 1 + mbloc - mod(ii-1,mbloc)
!
!     determine next pivot -- 
! 
        do k=ii+1,ii+len 
           t = abs(w(k))
           if (t > xmax .and. t*permtol > xmax0 .and. jw(k) <= icut) then
              imax = k
              xmax = t
           endif
        enddo
!
!     exchange w's
!
        tmp = w(ii)
        w(ii) = w(imax)
        w(imax) = tmp
!
!     update iperm and reverse iperm
!
        j = jw(imax)
        i = iperm(ii)
        iperm(ii) = iperm(j)
        iperm(j) = i
!     reverse iperm
        iperm(n+iperm(ii)) = ii
        iperm(n+iperm(j)) = j
!----------------------------------------------------------------------- 
        if (len + ju0-1 > iwk) goto 996
!
!     copy U-part in original coordinates
!     
        do 302 k=ii+1,ii+len
           jlu(ju0) = iperm(jw(k))
           alu(ju0) = w(k)
           ju0 = ju0+1
 302	continue
!
!     define diagonal element 
! 
         w(ii) = w(ii) + alph*dropsum 
!
!     store inverse of diagonal element of u
!
        if (w(ii) == 0.0) w(ii) = (1.0D-4 + droptol)*tnorm
!
        alu(ii) = 1.0d0/ w(ii) 
!
!     update pointer to beginning of next row of U.
!
	jlu(ii+1) = ju0
!-----------------------------------------------------------------------
!     end main loop
!-----------------------------------------------------------------------
 500  continue
!
!     permute all column indices of LU ...
!
      do k = jlu(1),jlu(n+1)-1
         jlu(k) = iperm(n+jlu(k))
      enddo
!
!     ...and of A
!
      do k=ia(1), ia(n+1)-1
         ja(k) = iperm(n+ja(k))
      enddo
!
      ierr = 0
      return
!
!     incomprehensible error. Matrix must be wrong.
!
 995  ierr = -1
      return
!
!     insufficient storage in arrays alu, jlu to store factors
!
 996  ierr = -2
      return
!
!     zero row encountered
!
 997  ierr = -3 
      return
!----------------end-of-iludp---------------------------!----------------
!-----------------------------------------------------------------------
    END SUBROUTINE sparskit_iludp
!-----------------------------------------------------------------------
    SUBROUTINE sparskit_iluk (n,a,ja,ia,lfil,alu,jlu,ju,levs,iwk,w,jw,ierr)
    integer(Ikind) :: n
    integer(Ikind) :: ja(*),ia(n+1),jlu(*),ju(n),levs(*),jw(3*n),lfil,iwk,ierr    
    real   (Rkind) :: a(*),alu(*),w(n)
!----------------------------------------------------------------------* 
!     SPARSKIT ROUTINE ILUK -- ILU WITH LEVEL OF FILL-IN OF K (ILU(k)) *
!----------------------------------------------------------------------*
!
! on entry:
!========== 
! n       = integer. The row dimension of the matrix A. The matrix 
!
! a,ja,ia = matrix stored in Compressed Sparse Row format.              
!
! lfil    = integer. The fill-in parameter. Each element whose
!           leve-of-fill exceeds lfil during the ILU process is dropped.
!           lfil must be >= 0 
!
! tol     = double precision. Sets the threshold for dropping small terms in the
!           factorization. See below for details on dropping strategy.
!  
! iwk     = integer. The minimum length of arrays alu, jlu, and levs.
!
! On return:
!===========
!
! alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
!           the L and U factors together. The diagonal (stored in
!           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
!           contains the i-th row of L (excluding the diagonal entry=1)
!           followed by the i-th row of U.
!
! ju      = integer array of length n containing the pointers to
!           the beginning of each row of U in the matrix alu,jlu.
!
! levs    = integer (work) array of size iwk -- which contains the 
!           levels of each element in alu, jlu.
!
! ierr    = integer. Error message with the following meaning.
!           ierr  = 0    --> successful return.
!           ierr > 0  --> zero pivot encountered at step number ierr.
!           ierr  = -1   --> Error. input matrix may be wrong.
!                            (The elimination process has generated a
!                            row in L or U whose length is >  n.)
!           ierr  = -2   --> The matrix L overflows the array al.
!           ierr  = -3   --> The matrix U overflows the array alu.
!           ierr  = -4   --> Illegal value for lfil.
!           ierr  = -5   --> zero row encountered in A or U.
!
! work arrays:
!=============
! jw      = integer work array of length 3*n.
! w       = real work array of length n 
!
! Notes/known bugs: This is not implemented efficiently storage-wise.
!       For example: Only the part of the array levs(*) associated with
!       the U-matrix is needed in the routine.. So some storage can 
!       be saved if needed. The levels of fills in the LU matrix are
!       output for information only -- they are not needed by LU-solve. 
!        
!----------------------------------------------------------------------
! w, ju (1:n) store the working array [1:ii-1 = L-part, ii:n = u] 
! jw(n+1:2n)  stores the nonzero indicator. 
! 
! Notes:
! ------
! All the diagonal elements of the input matrix must be  nonzero.
!
!----------------------------------------------------------------------* 
!     locals
      integer(Ikind) :: ju0,k,j1,j2,j,ii,i,lenl,lenu,jj,jrow,jpos,n2,   &
                        jlev, min 
      real   (Rkind) :: t, s, fact 
      if (lfil < 0) goto 998
!-----------------------------------------------------------------------
!     initialize ju0 (points to next element to be added to alu,jlu)
!     and pointer array.
!-----------------------------------------------------------------------
      n2 = n+n 
      ju0 = n+2
      jlu(1) = ju0
!
!     initialize nonzero indicator array + levs array -- 
!
      do 1 j=1,2*n 
         jw(j)  = 0
 1    continue
!-----------------------------------------------------------------------
!     beginning of main loop.
!-----------------------------------------------------------------------
      do 500 ii = 1, n
         j1 = ia(ii)
         j2 = ia(ii+1) - 1
!     
!     unpack L-part and U-part of row of A in arrays w 
!     
         lenu = 1
         lenl = 0
         jw(ii) = ii
         w(ii) = 0.0
         jw(n+ii) = ii
!
         do 170  j = j1, j2
            k = ja(j)
            t = a(j)
            if (t == 0.0) goto 170 
            if (k < ii) then
               lenl = lenl+1
               jw(lenl) = k
               w(lenl) = t
               jw(n2+lenl) = 0 
               jw(n+k) = lenl
            else if (k == ii) then
               w(ii) = t
               jw(n2+ii) = 0 
            else
               lenu = lenu+1
               jpos = ii+lenu-1 
               jw(jpos) = k
               w(jpos) = t
               jw(n2+jpos) = 0 
               jw(n+k) = jpos
            endif
 170     continue
!
         jj = 0
!
!     eliminate previous rows
!     
 150     jj = jj+1
         if (jj > lenl) goto 160
!-----------------------------------------------------------------------
!     in order to do the elimination in the correct order we must select
!     the smallest column index among jw(k), k=jj+1, ..., lenl.
!-----------------------------------------------------------------------
         jrow = jw(jj)
         k = jj
!     
!     determine smallest column index
!     
         do 151 j=jj+1,lenl
            if (jw(j) < jrow) then
               jrow = jw(j)
               k = j
            endif
 151     continue
!
         if (k /= jj) then
!     exchange in jw
            j = jw(jj)
            jw(jj) = jw(k)
            jw(k) = j
!     exchange in jw(n+  (pointers/ nonzero indicator).
            jw(n+jrow) = jj
            jw(n+j) = k
!     exchange in jw(n2+  (levels) 
            j = jw(n2+jj) 
            jw(n2+jj)  = jw(n2+k) 
            jw(n2+k) = j
!     exchange in w
            s = w(jj)
            w(jj) = w(k)
            w(k) = s
         endif
!
!     zero out element in row by resetting jw(n+jrow) to zero.
!     
         jw(n+jrow) = 0
!     
!     get the multiplier for row to be eliminated (jrow) + its level
!     
         fact = w(jj)*alu(jrow)
         jlev = jw(n2+jj) 
         if (jlev > lfil) goto 150
!
!     combine current row and row jrow
!
         do 203 k = ju(jrow), jlu(jrow+1)-1
            s = fact*alu(k)
            j = jlu(k)
            jpos = jw(n+j)
            if (j >= ii) then
!     
!     dealing with upper part.
!     
               if (jpos == 0) then
!
!     this is a fill-in element
!     
                  lenu = lenu+1
                  if (lenu > n) goto 995
                  i = ii+lenu-1
                  jw(i) = j
                  jw(n+j) = i
                  w(i) = - s
                  jw(n2+i) = jlev+levs(k)+1 
               else
!
!     this is not a fill-in element 
!
                  w(jpos) = w(jpos) - s
                  jw(n2+jpos) = min(jw(n2+jpos),jlev+levs(k)+1)
               endif
            else
!     
!     dealing with lower part.
!     
               if (jpos == 0) then
!
!     this is a fill-in element
!
                  lenl = lenl+1
                  if (lenl > n) goto 995
                  jw(lenl) = j
                  jw(n+j) = lenl
                  w(lenl) = - s
                  jw(n2+lenl) = jlev+levs(k)+1 
               else
!
!     this is not a fill-in element 
!
                  w(jpos) = w(jpos) - s
                  jw(n2+jpos) = min(jw(n2+jpos),jlev+levs(k)+1)
               endif
            endif
 203     continue
         w(jj) = fact
         jw(jj)  = jrow
         goto 150 
 160     continue 
!     
!     reset double-pointer to zero (U-part) 
!     
         do 308 k=1, lenu
            jw(n+jw(ii+k-1)) = 0
 308     continue
!
!     update l-matrix
!         
         do 204 k=1, lenl 
            if (ju0 > iwk) goto 996
            if (jw(n2+k) <= lfil) then
               alu(ju0) =  w(k)
               jlu(ju0) =  jw(k)
               ju0 = ju0+1
            endif
 204     continue
!     
!     save pointer to beginning of row ii of U
!     
         ju(ii) = ju0
!
!     update u-matrix
!
         do 302 k=ii+1,ii+lenu-1 
            if (jw(n2+k) <= lfil) then
               jlu(ju0) = jw(k)
               alu(ju0) = w(k)
               levs(ju0) = jw(n2+k) 
               ju0 = ju0+1
            endif
 302     continue

         if (w(ii) == 0.0) goto 999 
!     
         alu(ii) = 1.0d0/ w(ii) 
!     
!     update pointer to beginning of next row of U.
!     
         jlu(ii+1) = ju0
!-----------------------------------------------------------------------
!     end main loop
!-----------------------------------------------------------------------
 500  continue
      ierr = 0
      return
!
!     incomprehensible error. Matrix must be wrong.
!     
 995  ierr = -1
      return
!     
!     insufficient storage in L.
!     
 996  ierr = -2
      return
!     
!     insufficient storage in U.
!     
 997  ierr = -3       ! no path to here....
      return
!     
!     illegal lfil entered.
!     
 998  ierr = -4
      return
!     
!     zero row encountered in A or U. 
!     
 999  ierr = -5
      return
!----------------end-of-iluk--------------------------------------------
!-----------------------------------------------------------------------
    END SUBROUTINE sparskit_iluk
!----------------------------------------------------------------------
	SUBROUTINE sparskit_ilu0 (n, a, ja, ia, alu, jlu, ju, iw, ierr)	
   integer(Ikind) :: ja(*), ia(*), ju(*), jlu(*), iw(*), n, ierr	
	real   (Rkind) :: a(*), alu(*)
!------------------ right preconditioner ------------------------------*
!                    ***   ilu(0) preconditioner.   ***                *
!----------------------------------------------------------------------*
! Note that this has been coded in such a way that it can be used
! with pgmres. Normally, since the data structure of the L+U matrix is
! the same as that the A matrix, savings can be made. In fact with
! some definitions (not correct for general sparse matrices) all we
! need in addition to a, ja, ia is an additional diagonal.
! ILU0 is not recommended for serious problems. It is only provided
! here for comparison purposes.
!-----------------------------------------------------------------------
!
! on entry:
!---------
! n       = dimension of matrix
! a, ja,
! ia      = original matrix in compressed sparse row storage.
!
! on return:
!-----------
! alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
!           the L and U factors together. The diagonal (stored in
!           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
!           contains the i-th row of L (excluding the diagonal entry=1)
!           followed by the i-th row of U.
!
! ju	  = pointer to the diagonal elements in alu, jlu.
!
! ierr	  = integer indicating error code on return
!	     ierr = 0 --> normal return
!	     ierr = k --> code encountered a zero pivot at step k.!
! work arrays:
!-------------
! iw	    = integer work array of length n.
!------------
! IMPORTANT
!-----------
! it is assumed that the the elements in the input matrix are stored
!    in such a way that in each row the lower part comes first and
!    then the upper part. To get the correct ILU factorization, it is
!    also necessary to have the elements of L sorted by increasing
!    column number. It may therefore be necessary to sort the
!    elements of a, ja, ia prior to calling ilu0. This can be
!    achieved by transposing the matrix twice using csrcsc.
!
!-----------------------------------------------------------------------
      integer(Ikind) :: ju0, i, ii, js, j, jj, jcol, jm, jf, jw, jrow
      real   (Rkind) :: tl
        
      ju0 = n+2
      jlu(1) = ju0
!
! initialize work vector to zero's
!
	   do 31 i=1, n
         iw(i) = 0
31    continue
!
! main loop
!
	   do 500 ii = 1, n
         js = ju0
!
! generating row number ii of L and U.
!
         do 100 j=ia(ii),ia(ii+1)-1
!
!     copy row ii of a, ja, ia into row ii of alu, jlu (L/U) matrix.
!
            jcol = ja(j)
            if (jcol == ii) then
               alu(ii) = a(j)
               iw(jcol) = ii
               ju(ii)  = ju0
            else
               alu(ju0) = a(j)
               jlu(ju0) = ja(j)
               iw(jcol) = ju0
               ju0 = ju0+1
            endif
100      continue
         jlu(ii+1) = ju0
         jf = ju0-1
         jm = ju(ii)-1
!
!     exit if diagonal element is reached.
!
         do 150 j=js, jm
            jrow = jlu(j)
            tl = alu(j)*alu(jrow)
            alu(j) = tl
!
!     perform  linear combination
!
            do 140 jj = ju(jrow), jlu(jrow+1)-1
               jw = iw(jlu(jj))
               if (jw /= 0) alu(jw) = alu(jw) - tl*alu(jj)
140         continue
150      continue
!
!     invert  and store diagonal element.
!
         if (alu(ii) == 0.0e0_Rkind) goto 600
         alu(ii) = 1.0e0_Rkind/alu(ii)
!
!     reset pointer iw to zero
!
         iw(ii) = 0
!riad           do 201 i = js, jf
!riad 201          iw(jlu(i)) = 0
         do 201 i = js, jf
            iw(jlu(i)) = 0
201      continue

500   continue
      ierr = 0
      return
!
!     zero pivot :
!
600   ierr = ii
!
      return
!------- end-of-ilu0 ---------------------------------------------------
!-----------------------------------------------------------------------
    END SUBROUTINE sparskit_ilu0
!----------------------------------------------------------------------
	SUBROUTINE sparskit_milu0(n, a, ja, ia, alu, jlu, ju, iw, ierr)
	integer(Ikind) :: ja(*), ia(*), ju(*), jlu(*), iw(*), n, ierr	
	real   (Rkind) :: a(*), alu(*)
!----------------------------------------------------------------------*
!                *** simple milu(0) preconditioner. ***                *
!----------------------------------------------------------------------*
! Note that this has been coded in such a way that it can be used
! with pgmres. Normally, since the data structure of a, ja, ia is
! the same as that of a, ja, ia, savings can be made. In fact with
! some definitions (not correct for general sparse matrices) all we
! need in addition to a, ja, ia is an additional diagonal.
! Ilu0 is not recommended for serious problems. It is only provided
! here for comparison purposes.
!-----------------------------------------------------------------------
!
! on entry:
!----------
! n       = dimension of matrix
! a, ja,
! ia      = original matrix in compressed sparse row storage.
!
! on return:
!----------
! alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
!           the L and U factors together. The diagonal (stored in
!           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
!           contains the i-th row of L (excluding the diagonal entry=1)
!           followed by the i-th row of U.
!
! ju	  = pointer to the diagonal elements in alu, jlu.
!
! ierr	  = integer indicating error code on return
!	     ierr = 0 --> normal return
!	     ierr = k --> code encountered a zero pivot at step k.
! work arrays:
!-------------
! iw	    = integer work array of length n.
!------------
! Note (IMPORTANT):
!-----------
! it is assumed that the the elements in the input matrix are ordered
!    in such a way that in each row the lower part comes first and
!    then the upper part. To get the correct ILU factorization, it is
!    also necessary to have the elements of L ordered by increasing
!    column number. It may therefore be necessary to sort the!
!    elements of a, ja, ia prior to calling milu0. This can be
!    achieved by transposing the matrix twice using csrcsc.
!-----------------------------------------------------------
    integer(Ikind) :: ju0, i, js, j, jj, jw, ii, jcol, jrow, jf, jm
    real   (Rkind) :: s, tl
    
    ju0 = n+2
    jlu(1) = ju0
    
! initialize work vector to zero's
	 do 31 i=1, n
       iw(i) = 0
31  continue
!
!-------------- MAIN LOOP ----------------------------------
!
	 do 500 ii = 1, n
       js = ju0
!
! generating row number ii or L and U.
!
       do 100 j=ia(ii),ia(ii+1)-1
!
! copy row ii of a, ja, ia into row ii of alu, jlu (L/U) matrix.
!
          jcol = ja(j)
          if (jcol == ii) then
             alu(ii) = a(j)
             iw(jcol) = ii
             ju(ii)  = ju0
          else
             alu(ju0) = a(j)
             jlu(ju0) = ja(j)
             iw(jcol) = ju0
             ju0 = ju0+1
          endif
100    continue
       jlu(ii+1) = ju0
       jf = ju0-1
       jm = ju(ii)-1
! s accumulates fill-in values
       s = 0.0e0_Rkind
       do 150 j=js, jm
          jrow = jlu(j)
          tl = alu(j)*alu(jrow)
          alu(j) = tl
!-----------------------perform linear combination --------
          do 140 jj = ju(jrow), jlu(jrow+1)-1
             jw = iw(jlu(jj))
             if (jw /= 0) then
                alu(jw) = alu(jw) - tl*alu(jj)
             else
                s = s + tl*alu(jj)
              endif
140       continue
150    continue
!----------------------- invert and store diagonal element.
       alu(ii) = alu(ii)-s
       if (alu(ii) == 0.0e0_Rkind) goto 600
       alu(ii) = 1.0d0/alu(ii)
!----------------------- reset pointer iw to zero
       iw(ii) = 0
       do 201 i = js, jf
          iw(jlu(i)) = 0
201    continue 
500 continue

    ierr = 0
    return
!    
!     zero pivot :
!
600 ierr = ii

    return
!------- end-of-milu0 --------------------------------------------------
!-----------------------------------------------------------------------
    END SUBROUTINE sparskit_milu0
!-----------------------------------------------------------------------
    SUBROUTINE pgmres(n, im, rhs, sol, vv, eps, maxits, iout,           &
                      aa, ja, ia, alu, jlu, ju, ierr)
!-----------------------------------------------------------------------
    integer(Ikind) :: n, im, maxits, iout, ierr, ja(*), ia(n+1),        &
                      jlu(*), ju(n)
    real   (Rkind) :: vv(n,*), rhs(n), sol(n), aa(*), alu(*), eps
!----------------------------------------------------------------------*
!                                                                      *
!                 *** ILUT - Preconditioned GMRES ***                  *
!                                                                      *
!----------------------------------------------------------------------*
! This is a simple version of the ILUT preconditioned GMRES algorithm. *
! The ILUT preconditioner uses a dual strategy for dropping elements   *
! instead  of the usual level of-fill-in approach. See details in ILUT *
! subroutine documentation. PGMRES uses the L and U matrices generated *
! from the subroutine ILUT to precondition the GMRES algorithm.        *
! The preconditioning is applied to the right. The stopping criterion  *
! utilized is based simply on reducing the residual norm by epsilon.   *
! This preconditioning is more reliable than ilu0 but requires more    *
! storage. It seems to be much less prone to difficulties related to   *
! strong nonsymmetries in the matrix. We recommend using a nonzero tol *
! (tol=.005 or .001 usually give good results) in ILUT. Use a large    *
! lfil whenever possible (e.g. lfil = 5 to 10). The higher lfil the    *
! more reliable the code is. Efficiency may also be much improved.     *
! Note that lfil=n and tol=0.0 in ILUT  will yield the same factors as *
! Gaussian elimination without pivoting.                               *
!                                                                      *
! ILU(0) and MILU(0) are also provided for comparison purposes         *
! USAGE: first call ILUT or ILU0 or MILU0 to set up preconditioner and *
! then call pgmres.                                                    *
!----------------------------------------------------------------------*
! Coded by Y. Saad - This version dated May, 7, 1990.                  *
!----------------------------------------------------------------------*
! parameters                                                           *
!-----------                                                           *
! on entry:                                                            *
!==========                                                            *
!                                                                      *
! n     == integer. The dimension of the matrix.                       *
! im    == size of krylov subspace:  should not exceed 50 in this      *
!          version (can be reset by changing parameter command for     *
!          kmax below)                                                 *
! rhs   == real vector of length n containing the right hand side.     *
!          Destroyed on return.                                        *
! sol   == real vector of length n containing an initial guess to the  *
!          solution on input. approximate solution on output           *
! eps   == tolerance for stopping criterion. process is stopped        *
!          as soon as ( ||.|| is the euclidean norm):                  *
!          || current residual||/||initial residual|| <= eps           *
! maxits== maximum number of iterations allowed                        *
! iout  == output unit number number for printing intermediate results *
!          if (iout <= 0) nothing is printed out.                    *
!                                                                      *
! aa, ja,                                                              *
! ia    == the input matrix in compressed sparse row format:           *
!          aa(1:nnz)  = nonzero elements of A stored row-wise in order *
!          ja(1:nnz) = corresponding column indices.                   *
!          ia(1:n+1) = pointer to beginning of each row in aa and ja.  *
!          here nnz = number of nonzero elements in A = ia(n+1)-ia(1)  *
!                                                                      *
! alu,jlu== A matrix stored in Modified Sparse Row format containing   *
!           the L and U factors, as computed by subroutine ilut.       *
!                                                                      *
! ju     == integer array of length n containing the pointers to       *
!           the beginning of each row of U in alu, jlu as computed     *
!           by subroutine ILUT.                                        *
!                                                                      *
! on return:                                                           *
!==========                                                            *
! sol   == contains an approximate solution (upon successful return).  *
! ierr  == integer. Error message with the following meaning.          *
!          ierr = 0 --> successful return.                             *
!          ierr = 1 --> convergence not achieved in itmax iterations.  *
!          ierr =-1 --> the initial guess seems to be the exact        *
!                       solution (initial residual computed was zero)  *
!                                                                      *
!----------------------------------------------------------------------*
!                                                                      *
! work arrays:                                                         *
!=============                                                         *
! vv    == work array of length  n x (im+1) (used to store the Arnoli  *
!          basis)                                                      *
!----------------------------------------------------------------------*
! subroutines called :                                                 *
! amux   : SPARSKIT routine to do the matrix by vector multiplication  *
!          delivers y=Ax, given x  -- see SPARSKIT/BLASSM/amux         *
! lusol  : combined forward and backward solves (Preconditioning ope.) *
! BLAS1  routines.                                                     *
!----------------------------------------------------------------------*
       integer(Ikind) :: kmax, n1, its, j, i, i1, ii, k, k1, jj
       parameter (kmax=50)
       real   (Rkind) :: hh(kmax+1,kmax), c(kmax), s(kmax), rs(kmax+1),t
       real   (Rkind) :: ro, eps1, gam, epsmac
!-------------------------------------------------------------
! arnoldi size should not exceed kmax=50 in this version..
! to reset modify paramter kmax accordingly.
!-------------------------------------------------------------
       data epsmac/1.0e-16_Rkind/
       n1 = n + 1
       its = 0
!-------------------------------------------------------------
! outer loop starts here..
!-------------- compute initial residual vector --------------
       call amux (n, sol, vv, aa, ja, ia)
       do 21 j=1,n
          vv(j,1) = rhs(j) - vv(j,1)
 21    continue
!-------------------------------------------------------------
 20    ro = dnrm2(n, vv, 1_Ikind)
       if (iout > 0 .and. its == 0) write(iout, 199) its, ro
       if (ro == 0.0e0_Rkind) goto 999
       t = 1.0d0/ ro
       do 210 j=1, n
          vv(j,1) = vv(j,1)*t
 210   continue
       if (its == 0) eps1=eps*ro
!     ** initialize 1-st term  of rhs of hessenberg system..
       rs(1) = ro
       i = 0
 4     i=i+1
       its = its + 1
       i1 = i + 1
       call lusol (n, vv(1,i), rhs, alu, jlu, ju)
       call amux (n, rhs, vv(1,i1), aa, ja, ia)
!-----------------------------------------
!     modified gram - schmidt...
!-----------------------------------------
       do 55 j=1, i
          t = ddot(n, vv(1,j),1_Ikind,vv(1,i1),1_Ikind)
          hh(j,i) = t
          call daxpy(n, -t, vv(1,j), 1_Ikind, vv(1,i1), 1_Ikind)
 55    continue
       t = dnrm2(n, vv(1,i1), 1_Ikind)
       hh(i1,i) = t
       if ( t == 0.0e0_Rkind) goto 58
       t = 1.0e0_Rkind/t
       do 57  k=1,n
          vv(k,i1) = vv(k,i1)*t
 57    continue
!
!     done with modified gram schimd and arnoldi step..
!     now  update factorization of hh
!
 58    if (i == 1) goto 121
!--------perfrom previous transformations  on i-th column of h
       do 66 k=2,i
          k1 = k-1
          t = hh(k1,i)
          hh(k1,i) = c(k1)*t + s(k1)*hh(k,i)
          hh(k,i) = -s(k1)*t + c(k1)*hh(k,i)
 66    continue
 121   gam = sqrt(hh(i,i)**2 + hh(i1,i)**2)
!
!     if gamma is zero then any small value will do...
!     will affect only residual estimate
!
       if (gam == 0.0e0_Rkind) gam = epsmac
!
!     get  next plane rotation
!
       c(i) = hh(i,i)/gam
       s(i) = hh(i1,i)/gam
       rs(i1) = -s(i)*rs(i)
       rs(i) =  c(i)*rs(i)
!
!     detrermine residual norm and test for convergence-
!
       hh(i,i) = c(i)*hh(i,i) + s(i)*hh(i1,i)
       ro = abs(rs(i1))
 !131   format(1h ,2e14.4)
 131   format(1x,2e14.4)
       if (iout > 0) write(iout, 199) its, ro
       if (i < im .and. (ro > eps1))  goto 4
!
!     now compute solution. first solve upper triangular system.
!
       rs(i) = rs(i)/hh(i,i)
       do 30 ii=2,i
          k=i-ii+1
          k1 = k+1
          t=rs(k)
          do 40 j=k1,i
             t = t-hh(k,j)*rs(j)
 40       continue
          rs(k) = t/hh(k,k)
 30    continue
!
!     form linear combination of v(*,i)'s to get solution
!
       t = rs(1)
       do 15 k=1, n
          rhs(k) = vv(k,1)*t
 15    continue
       do 16 j=2, i
          t = rs(j)
          do 161 k=1, n
             rhs(k) = rhs(k)+t*vv(k,j)
 161      continue
 16    continue
!
!     call preconditioner.
!
       call lusol (n, rhs, rhs, alu, jlu, ju)
       do 17 k=1, n
          sol(k) = sol(k) + rhs(k)
 17    continue
!
!     restart outer loop  when necessary
!
       if (ro <= eps1) goto 990
       if (its >= maxits) goto 991
!
!     else compute residual vector and continue..
!
       do 24 j=1,i
          jj = i1-j+1
          rs(jj-1) = -s(jj-1)*rs(jj)
          rs(jj) = c(jj-1)*rs(jj)
 24    continue
       do 25  j=1,i1
          t = rs(j)
          if (j == 1)  t = t-1.0d0
          call daxpy (n, t, vv(1,j), 1_Ikind,  vv, 1_Ikind)
 25    continue
 199   format('   its =', i4, ' res. norm =', d20.6)
!     restart outer loop.
       goto 20
 990   ierr = 0
       return
 991   ierr = 1
       return
 999   continue
       ierr = -1
       return
!-----------------end of pgmres ---------------------------------------
!-----------------------------------------------------------------------
       END SUBROUTINE pgmres
!-----------------------------------------------------------------------
	SUBROUTINE lusol(n, y, x, alu, jlu, ju)
	integer(Ikind) :: n, jlu(*), ju(*)	
    real   (Rkind) :: x(n), y(n), alu(*)
!-----------------------------------------------------------------------
!
! This routine solves the system (LU) x = y, 
! given an LU decomposition of a matrix stored in (alu, jlu, ju) 
! modified sparse row format 
!
!-----------------------------------------------------------------------
! on entry:
! n   = dimension of system 
! y   = the right-hand-side vector
! alu, jlu, ju 
!     = the LU matrix as provided from the ILU routines. 
!
! on return
! x   = solution of LU x = y.     
!-----------------------------------------------------------------------
! 
! Note: routine is in place: call lusol (n, x, x, alu, jlu, ju) 
!       will solve the system with rhs x and overwrite the result on x . 
!
!-----------------------------------------------------------------------
! local variables
!
    integer(Ikind) :: i,k
!
! forward solve
!
        do 40 i = 1, n
           x(i) = y(i)
           do 41 k=jlu(i),ju(i)-1
              x(i) = x(i) - alu(k)* x(jlu(k))
 41        continue
 40     continue
!
!     backward solve.
!
	do 90 i = n, 1, -1
	   do 91 k=ju(i),jlu(i+1)-1
              x(i) = x(i) - alu(k)*x(jlu(k))
 91	   continue
           x(i) = alu(i)*x(i)
 90     continue
!
  	return
!----------------end of lusol ------------------------------------------
!-----------------------------------------------------------------------
	END SUBROUTINE lusol
!-----------------------------------------------------------------------
	SUBROUTINE lutsol(n, y, x, alu, jlu, ju) 
	integer(Ikind) :: n, jlu(*), ju(*)	
    real   (Rkind) :: x(n), y(n), alu(*)
!-----------------------------------------------------------------------
!
! This routine solves the system  Transp(LU) x = y,
! given an LU decomposition of a matrix stored in (alu, jlu, ju) 
! modified sparse row format. Transp(M) is the transpose of M. 
!----------------------------------------------------------------------- 
! on entry:
! n   = dimension of system 
! y   = the right-hand-side vector
! alu, jlu, ju 
!     = the LU matrix as provided from the ILU routines. 
!
! on return
! x   = solution of transp(LU) x = y.   
!-----------------------------------------------------------------------
!
! Note: routine is in place: call lutsol (n, x, x, alu, jlu, ju) 
!       will solve the system with rhs x and overwrite the result on x . 
! 
!-----------------------------------------------------------------------
! local variables
!
        integer(Ikind) :: i,k
!
        do 10 i = 1, n
           x(i) = y(i)
 10     continue
!
! forward solve (with U^T)
!
        do 20 i = 1, n
           x(i) = x(i) * alu(i)
           do 30 k=ju(i),jlu(i+1)-1
              x(jlu(k)) = x(jlu(k)) - alu(k)* x(i)
 30        continue
 20     continue
!     
!     backward solve (with L^T)
!     
	do 40 i = n, 1, -1 
	   do 50 k=jlu(i),ju(i)-1
              x(jlu(k)) = x(jlu(k)) - alu(k)*x(i)
 50        continue
 40     continue
!
  	return
!----------------end of lutsol -----------------------------------------
!-----------------------------------------------------------------------
	END SUBROUTINE lutsol
!----------------------------------------------------------------------- 
        SUBROUTINE qsplit (a,ind,n,ncut)
        integer(Ikind) :: n
        integer(Ikind) :: ind(n), ncut        
        real   (Rkind) :: a(n)
!-----------------------------------------------------------------------
!     does a quick-sort split of a real array.
!     on input a(1:n). is a real array
!     on output a(1:n) is permuted such that its elements satisfy:
!
!     abs(a(i)) >= abs(a(ncut)) for i < ncut and
!     abs(a(i)) <= abs(a(ncut)) for i > ncut
!
!     ind(1:n) is an integer array which permuted in the same way as a(*).
!-----------------------------------------------------------------------
        integer(Ikind) :: itmp, first, last, mid, j
        real   (Rkind) :: tmp, abskey
!-----
        first = 1
        last = n
        if (ncut < first .or. ncut > last) return
!
!     outer loop -- while mid /= ncut do
!
 1      mid = first
        abskey = abs(a(mid))
        do 2 j=first+1, last
           if (abs(a(j)) > abskey) then
              mid = mid+1
!     interchange
              tmp = a(mid)
              itmp = ind(mid)
              a(mid) = a(j)
              ind(mid) = ind(j)
              a(j)  = tmp
              ind(j) = itmp
           endif
 2      continue
!
!     interchange
!
        tmp = a(mid)
        a(mid) = a(first)
        a(first)  = tmp
!
        itmp = ind(mid)
        ind(mid) = ind(first)
        ind(first) = itmp
!
!     test for while loop
!
        if (mid == ncut) return
        if (mid > ncut) then
           last = mid-1
        else
           first = mid+1
        endif
        goto 1
!----------------end-of-qsplit------------------------------------------
!-----------------------------------------------------------------------
        END SUBROUTINE qsplit
!----------------------------------------------------------------------c
!                          S P A R S K I T                             c
!----------------------------------------------------------------------c
!         Basic Iterative Solvers with Reverse Communication           c
!----------------------------------------------------------------------c
!     This file currently has several basic iterative linear system    c
!     solvers. They are:                                               c
!     CG       -- Conjugate Gradient Method                            c
!     CGNR     -- Conjugate Gradient Method (Normal Residual equation) c
!     BCG      -- Bi-Conjugate Gradient Method                         c
!     DBCG     -- BCG with partial pivoting                            c
!     BCGSTAB  -- BCG stabilized                                       c
!     TFQMR    -- Transpose-Free Quasi-Minimum Residual method         c
!     FOM      -- Full Orthogonalization Method                        c
!     GMRES    -- Generalized Minimum RESidual method                  c
!     FGMRES   -- Flexible version of Generalized Minimum              c
!                 RESidual method                                      c
!     DQGMRES  -- Direct versions of Quasi Generalize Minimum          c
!                 Residual method                                      c
!----------------------------------------------------------------------c
!     They all have the following calling sequence:
!      subroutine solver(n, rhs, sol, ipar, fpar, w)
!      integer n, ipar(16)
!      double precision rhs(n), sol(n), fpar(16), w(*)
!     Where
!     (1) 'n' is the size of the linear system,
!     (2) 'rhs' is the right-hand side of the linear system,
!     (3) 'sol' is the solution to the linear system,
!     (4) 'ipar' is an integer parameter array for the reverse
!     communication protocol,
!     (5) 'fpar' is an floating-point parameter array storing
!     information to and from the iterative solvers.
!     (6) 'w' is the work space (size is specified in ipar)
!
!     They are preconditioned iterative solvers with reverse
!     communication. The preconditioners can be applied from either
!     from left or right or both (specified by ipar(2), see below).
!
!     Author: Kesheng John Wu (kewu@mail.cs.umn.edu) 1993
!
!     NOTES:
!
!     (1) Work space required by each of the iterative solver
!     routines is as follows:
!       CG      == 5 * n
!       CGNR    == 5 * n
!       BCG     == 7 * n
!       DBCG    == 11 * n
!       BCGSTAB == 8 * n
!       TFQMR   == 11 * n
!       FOM     == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
!       GMRES   == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
!       FGMRES  == 2*n*(m+1) + (m+1)*m/2 + 3*m + 2 (m = ipar(5),
!                  default m=15)
!       DQGMRES == n + lb * (2*n+4) (lb=ipar(5)+1, default lb = 16)
!
!     (2) ALL iterative solvers require a user-supplied DOT-product
!     routine named DISTDOT. The prototype of DISTDOT is
!
!     double precision function distdot(n,x,ix,y,iy)
!     integer n, ix, iy
!     double precision x(1+(n-1)*ix), y(1+(n-1)*iy)
!
!     This interface of DISTDOT is exactly the same as that of
!     DDOT (or SDOT if real == double precision) from BLAS-1. It should have
!     same functionality as DDOT on a single processor machine. On a
!     parallel/distributed environment, each processor can perform
!     DDOT on the data it has, then perform a summation on all the
!     partial results.
!
!     (3) To use this set of routines under SPMD/MIMD program paradigm,
!     several things are to be noted: (a) 'n' should be the number of
!     vector elements of 'rhs' that is present on the local processor.
!     (b) if RHS(i) is on processor j, it is expected that SOL(i)
!     will be on the same processor, i.e. the vectors are distributed
!     to each processor in the same way. (c) the preconditioning and
!     stopping criteria specifications have to be the same on all
!     processor involved, ipar and fpar have to be the same on each
!     processor. (d) DISTDOT should be replaced by a distributed
!     dot-product function.
!
!     ..................................................................
!     Reverse Communication Protocols
!
!     When a reverse-communication routine returns, it could be either
!     that the routine has terminated or it simply requires the caller
!     to perform one matrix-vector multiplication. The possible matrices
!     that involve in the matrix-vector multiplications are:
!     A       (the matrix of the linear system),
!     A^T     (A transposed),
!     Ml^{-1} (inverse of the left preconditioner),
!     Ml^{-T} (inverse of the left preconditioner transposed),
!     Mr^{-1} (inverse of the right preconditioner),
!     Mr^{-T} (inverse of the right preconditioner transposed).
!     For all the matrix vector multiplication, v = A u. The input and
!     output vectors are supposed to be part of the work space 'w', and
!     the starting positions of them are stored in ipar(8:9), see below.
!
!     The array 'ipar' is used to store the information about the solver.
!     Here is the list of what each element represents:
!
!     ipar(1) -- status of the call/return.
!     A call to the solver with ipar(1) == 0 will initialize the
!     iterative solver. On return from the iterative solver, ipar(1)
!     carries the status flag which indicates the condition of the
!     return. The status information is divided into two categories,
!     (1) a positive value indicates the solver requires a matrix-vector
!     multiplication,
!     (2) a non-positive value indicates termination of the solver.
!     Here is the current definition:
!       1 == request a matvec with A,
!       2 == request a matvec with A^T,
!       3 == request a left preconditioner solve (Ml^{-1}),
!       4 == request a left preconditioner transposed solve (Ml^{-T}),
!       5 == request a right preconditioner solve (Mr^{-1}),
!       6 == request a right preconditioner transposed solve (Mr^{-T}),
!      10 == request the caller to perform stopping test,
!       0 == normal termination of the solver, satisfied the stopping
!            criteria,
!      -1 == termination because iteration number is greater than the
!            preset limit,
!      -2 == return due to insufficient work space,
!      -3 == return due to anticipated break-down / divide by zero,
!            in the case where Arnoldi procedure is used, additional
!            error code can be found in ipar(12), where ipar(12) is
!            the error code of orthogonalization procedure MGSRO:
!               -1: zero input vector
!               -2: input vector contains abnormal numbers
!               -3: input vector is a linear combination of others
!               -4: trianguler system in GMRES/FOM/etc. has rank 0 (zero)
!      -4 == the values of fpar(1) and fpar(2) are both <= 0, the valid
!            ranges are 0 <= fpar(1) < 1, 0 <= fpar(2), and they can
!            not be zero at the same time
!      -9 == while trying to detect a break-down, an abnormal number is
!            detected.
!     -10 == return due to some non-numerical reasons, e.g. invalid
!            floating-point numbers etc.
!
!     ipar(2) -- status of the preconditioning:
!       0 == no preconditioning
!       1 == left preconditioning only
!       2 == right preconditioning only
!       3 == both left and right preconditioning
!
!     ipar(3) -- stopping criteria (details of this will be
!     discussed later).
!
!     ipar(4) -- number of elements in the array 'w'. if this is less
!     than the desired size, it will be over-written with the minimum
!     requirement. In which case the status flag ipar(1) = -2.
!
!     ipar(5) -- size of the Krylov subspace (used by GMRES and its
!     variants), e.g. GMRES(ipar(5)), FGMRES(ipar(5)),
!     DQGMRES(ipar(5)).
!
!     ipar(6) -- maximum number of matrix-vector multiplies, if not a
!     positive number the iterative solver will run till convergence
!     test is satisfied.
!
!     ipar(7) -- current number of matrix-vector multiplies. It is
!     incremented after each matrix-vector multiplication. If there
!     is preconditioning, the counter is incremented after the
!     preconditioning associated with each matrix-vector multiplication.
!
!     ipar(8) -- pointer to the input vector to the requested matrix-
!     vector multiplication.
!
!     ipar(9) -- pointer to the output vector of the requested matrix-
!     vector multiplication.
!
!     To perform v = A * u, it is assumed that u is w(ipar(8):ipar(8)+n-1)
!     and v is stored as w(ipar(9):ipar(9)+n-1).
!
!     ipar(10) -- the return address (used to determine where to go to
!     inside the iterative solvers after the caller has performed the
!     requested services).
!
!     ipar(11) -- the result of the external convergence test
!     On final return from the iterative solvers, this value
!     will be reflected by ipar(1) = 0 (details discussed later)
!
!     ipar(12) -- error code of MGSRO, it is
!                  1 if the input vector to MGSRO is linear combination
!                    of others,
!                  0 if MGSRO was successful,
!                 -1 if the input vector to MGSRO is zero,
!                 -2 if the input vector contains invalid number.
!
!     ipar(13) -- number of initializations. During each initilization
!                 residual norm is computed directly from M_l(b - A x).
!
!     ipar(14) to ipar(16) are NOT defined, they are NOT USED by
!     any iterative solver at this time.
!
!     Information about the error and tolerance are stored in the array
!     FPAR. So are some internal variables that need to be saved from
!     one iteration to the next one. Since the internal variables are
!     not the same for each routine, we only define the common ones.
!
!     The first two are input parameters:
!     fpar(1) -- the relative tolerance,
!     fpar(2) -- the absolute tolerance (details discussed later),
!
!     When the iterative solver terminates,
!     fpar(3) -- initial residual/error norm,
!     fpar(4) -- target residual/error norm,
!     fpar(5) -- current residual norm (if available),
!     fpar(6) -- current residual/error norm,
!     fpar(7) -- convergence rate,
!
!     fpar(8:10) are used by some of the iterative solvers to save some
!     internal information.
!
!     fpar(11) -- number of floating-point operations. The iterative
!     solvers will add the number of FLOPS they used to this variable,
!     but they do NOT initialize it, nor add the number of FLOPS due to
!     matrix-vector multiplications (since matvec is outside of the
!     iterative solvers). To insure the correct FLOPS count, the
!     caller should set fpar(11) = 0 before invoking the iterative
!     solvers and account for the number of FLOPS from matrix-vector
!     multiplications and preconditioners.
!
!     fpar(12:16) are not used in current implementation.
!
!     Whether the content of fpar(3), fpar(4) and fpar(6) are residual
!     norms or error norms depends on ipar(3). If the requested
!     convergence test is based on the residual norm, they will be
!     residual norms. If the caller want to test convergence based the
!     error norms (estimated by the norm of the modifications applied
!     to the approximate solution), they will be error norms.
!     Convergence rate is defined by (Fortran 77 statement)
!     fpar(7) = log10(fpar(3) / fpar(6)) / (ipar(7)-ipar(13))
!     If fpar(7) = 0.5, it means that approximately every 2 (= 1/0.5)
!     steps the residual/error norm decrease by a factor of 10.
!
!     ..................................................................
!     Stopping criteria,
!
!     An iterative solver may be terminated due to (1) satisfying
!     convergence test; (2) exceeding iteration limit; (3) insufficient
!     work space; (4) break-down. Checking of the work space is
!     only done in the initialization stage, i.e. when it is called with
!     ipar(1) == 0. A complete convergence test is done after each
!     update of the solutions. Other conditions are monitored
!     continuously.
!
!     With regard to the number of iteration, when ipar(6) is positive,
!     the current iteration number will be checked against it. If
!     current iteration number is greater the ipar(6) than the solver
!     will return with status -1. If ipar(6) is not positive, the
!     iteration will continue until convergence test is satisfied.
!
!     Two things may be used in the convergence tests, one is the
!     residual 2-norm, the other one is 2-norm of the change in the
!     approximate solution. The residual and the change in approximate
!     solution are from the preconditioned system (if preconditioning
!     is applied). The DQGMRES and TFQMR use two estimates for the
!     residual norms. The estimates are not accurate, but they are
!     acceptable in most of the cases. Generally speaking, the error
!     of the TFQMR's estimate is less accurate.
!
!     The convergence test type is indicated by ipar(3). There are four
!     type convergence tests: (1) tests based on the residual norm;
!     (2) tests based on change in approximate solution; (3) caller
!     does not care, the solver choose one from above two on its own;
!     (4) caller will perform the test, the solver should simply continue.
!     Here is the complete definition:
!      -2 == || dx(i) || <= rtol * || rhs || + atol
!      -1 == || dx(i) || <= rtol * || dx(1) || + atol
!       0 == solver will choose test 1 (next)
!       1 == || residual || <= rtol * || initial residual || + atol
!       2 == || residual || <= rtol * || rhs || + atol
!     999 == caller will perform the test
!     where dx(i) denote the change in the solution at the ith update.
!     ||.|| denotes 2-norm. rtol = fpar(1) and atol = fpar(2).
!
!     If the caller is to perform the convergence test, the outcome
!     should be stored in ipar(11).
!     ipar(11) = 0 -- failed the convergence test, iterative solver
!     should continue
!     ipar(11) = 1 -- satisfied convergence test, iterative solver
!     should perform the clean up job and stop.
!
!     Upon return with ipar(1) = 10,
!     ipar(8)  points to the starting position of the change in
!              solution Sx, where the actual solution of the step is
!              x_j = x_0 + M_r^{-1} Sx.
!              Exception: ipar(8) < 0, Sx = 0. It is mostly used by
!              GMRES and variants to indicate (1) Sx was not necessary,
!              (2) intermediate result of Sx is not computed.
!     ipar(9)  points to the starting position of a work vector that
!              can be used by the caller.
!
!     NOTE: the caller should allow the iterative solver to perform
!     clean up job after the external convergence test is satisfied,
!     since some of the iterative solvers do not directly
!     update the 'sol' array. A typical clean-up stage includes
!     performing the final update of the approximate solution and
!     computing the convergence information (e.g. values of fpar(3:7)).
!
!     NOTE: fpar(4) and fpar(6) are not set by the accelerators (the
!     routines implemented here) if ipar(3) = 999.
!
!     ..................................................................
!     Usage:
!
!     To start solving a linear system, the user needs to specify
!     first 6 elements of the ipar, and first 2 elements of fpar.
!     The user may optionally set fpar(11) = 0 if one wants to count
!     the number of floating-point operations. (Note: the iterative
!     solvers will only add the floating-point operations inside
!     themselves, the caller will have to add the FLOPS from the
!     matrix-vector multiplication routines and the preconditioning
!     routines in order to account for all the arithmetic operations.)
!
!     Here is an example:
!     ipar(1) = 0	! always 0 to start an iterative solver
!     ipar(2) = 2	! right preconditioning
!     ipar(3) = 1	! use convergence test scheme 1
!     ipar(4) = 10000	! the 'w' has 10,000 elements
!     ipar(5) = 10	! use *GMRES(10) (e.g. FGMRES(10))
!     ipar(6) = 100	! use at most 100 matvec's
!     fpar(1) = 1.0E-6	! relative tolerance 1.0E-6
!     fpar(2) = 1.0E-10 ! absolute tolerance 1.0E-10
!     fpar(11) = 0.0	! clearing the FLOPS counter
!
!     After the above specifications, one can start to call an iterative
!     solver, say BCG. Here is a piece of pseudo-code showing how it can
!     be done,
!
! 10   call bcg(n,rhs,sol,ipar,fpar,w)
!      if (ipar(1)==1) then
!         call amux(n,w(ipar(8)),w(ipar(9)),a,ja,ia)
!         goto 10
!      else if (ipar(1)==2) then
!         call atmux(n,w(ipar(8)),w(ipar(9)),a,ja,ia)
!         goto 10
!      else if (ipar(1)==3) then
!         left preconditioner solver
!         goto 10
!      else if (ipar(1)==4) then
!         left preconditioner transposed solve
!         goto 10
!      else if (ipar(1)==5) then
!         right preconditioner solve
!         goto 10
!      else if (ipar(1)==6) then
!         right preconditioner transposed solve
!         goto 10
!      else if (ipar(1)==10) then
!         call my own stopping test routine
!         goto 10
!      else if (ipar(1)>0) then
!         ipar(1) is an unspecified code
!      else
!         the iterative solver terminated with code = ipar(1)
!      endif
!
!     This segment of pseudo-code assumes the matrix is in CSR format,
!     AMUX and ATMUX are two routines from the SPARSKIT MATVEC module.
!     They perform matrix-vector multiplications for CSR matrices,
!     where w(ipar(8)) is the first element of the input vectors to the
!     two routines, and w(ipar(9)) is the first element of the output
!     vectors from them. For simplicity, we did not show the name of
!     the routine that performs the preconditioning operations or the
!     convergence tests.
!-----------------------------------------------------------------------
      SUBROUTINE cg(n, rhs, sol, ipar, fpar, w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(n,*)
!-----------------------------------------------------------------------
!     This is a implementation of the Conjugate Gradient (CG) method
!     for solving linear system.
!
!     NOTE: This is not the PCG algorithm. It is a regular CG algorithm.
!     To be consistent with the other solvers, the preconditioners are
!     applied by performing Ml^{-1} A Mr^{-1} P in place of A P in the
!     CG algorithm. The PCG uses its preconditioners very differently.
!
!     fpar(7) is used here internally to store <r, r>.
!     w(:,1) -- residual vector
!     w(:,2) -- P, the conjugate direction
!     w(:,3) -- A P, matrix multiply the conjugate direction
!     w(:,4) -- temporary storage for results of preconditioning
!     w(:,5) -- change in the solution (sol) is stored here until
!               termination of this solver
!-----------------------------------------------------------------------
!     external functions used
!
     ! real(Rkind) :: distdot
     ! logical     :: stopbis, brkdn
     ! external distdot, stopbis, brkdn, bisinit
!
!     local variables
!
      integer(Ikind) :: i
      real   (Rkind) :: alpha
      logical        :: lp,rp
      save
!
!     check the status of the call
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10, 20, 40, 50, 60, 70, 80), ipar(10)
!
!     initialization
!
      call bisinit(ipar,fpar,5*n,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
!
!     request for matrix vector multiplication A*x in the initialization
!
      ipar(1) = 1
      ipar(8) = n+1
      ipar(9) = ipar(8) + n
      ipar(10) = 1
      do i = 1, n
         w(i,2) = sol(i)
      enddo
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = 1
      do i = 1, n
         w(i,2) = rhs(i) - w(i,3)
      enddo
      fpar(11) = fpar(11) + n
!
!     if left preconditioned
!
      if (lp) then
         ipar(1) = 3
         ipar(9) = 1
         ipar(10) = 2
         return
      endif
!
 20   if (lp) then
         do i = 1, n
            w(i,2) = w(i,1)
         enddo
      else
         do i = 1, n
            w(i,1) = w(i,2)
         enddo
      endif
!
      fpar(7) = distdot(n,w,1_Ikind,w,1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      fpar(3) = sqrt(fpar(7))
      fpar(5) = fpar(3)
      if (abs(ipar(3))==2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
         fpar(11) = fpar(11) + 2 * n
      else if (ipar(3)/=999) then
         fpar(4) = fpar(1) * fpar(3) + fpar(2)
      endif
!
!     before iteration can continue, we need to compute A * p, which
!     includes the preconditioning operations
!
 30   if (rp) then
         ipar(1) = 5
         ipar(8) = n + 1
         if (lp) then
            ipar(9) = ipar(8) + n
         else
            ipar(9) = 3*n + 1
         endif
         ipar(10) = 3
         return
      endif
!
 40   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = n + 1
      endif
      if (lp) then
         ipar(9) = 3*n+1
      else
         ipar(9) = n+n+1
      endif
      ipar(10) = 4
      return
!
 50   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = n+n+1
         ipar(10) = 5
         return
      endif
!
!     continuing with the iterations
!
 60   ipar(7) = ipar(7) + 1
      alpha = distdot(n,w(1,2),1_Ikind,w(1,3),1_Ikind)
      fpar(11) = fpar(11) + 2*n
      if (brkdn(alpha,ipar)) goto 900
      alpha = fpar(7) / alpha
      do i = 1, n
         w(i,5) = w(i,5) + alpha * w(i,2)
         w(i,1) = w(i,1) - alpha * w(i,3)
      enddo
      fpar(11) = fpar(11) + 4*n
!
!     are we ready to terminate ?
!
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = 4*n + 1
         ipar(9) = 3*n + 1
         ipar(10) = 6
         return
      endif
 70   if (ipar(3)==999) then
         if (ipar(11)==1) goto 900
      else if (stopbis(n,ipar,1_Ikind,fpar,w,w(1,2),alpha)) then
         goto 900
      endif
!
!     continue the iterations
!
      alpha = fpar(5)*fpar(5) / fpar(7)
      fpar(7) = fpar(5)*fpar(5)
      do i = 1, n
         w(i,2) = w(i,1) + alpha * w(i,2)
      enddo
      fpar(11) = fpar(11) + 2*n
      goto 30
!
!     clean up -- necessary to accommodate the right-preconditioning
!
 900  if (rp) then
         if (ipar(1)<0) ipar(12) = ipar(1)
         ipar(1) = 5
         ipar(8) = 4*n + 1
         ipar(9) = ipar(8) - n
         ipar(10) = 7
         return
      endif
 80   if (rp) then
         call tidycg(n,ipar,fpar,sol,w(1,4))
      else
         call tidycg(n,ipar,fpar,sol,w(1,5))
      endif
!
      return
      END SUBROUTINE cg
!-----end-of-cg
!-----------------------------------------------------------------------
      SUBROUTINE cgnr(n,rhs,sol,ipar,fpar,wk)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n),sol(n),fpar(16),wk(n,*)
!-----------------------------------------------------------------------
!     CGNR -- Using CG algorithm solving A x = b by solving
!     Normal Residual equation: A^T A x = A^T b
!     As long as the matrix is not singular, A^T A is symmetric
!     positive definite, therefore CG (CGNR) will converge.
!
!     Usage of the work space:
!     wk(:,1) == residual vector R
!     wk(:,2) == the conjugate direction vector P
!     wk(:,3) == a scratch vector holds A P, or A^T R
!     wk(:,4) == a scratch vector holds intermediate results of the
!                preconditioning
!     wk(:,5) == a place to hold the modification to SOL
!
!     size of the work space WK is required = 5*n
!-----------------------------------------------------------------------
!      functions used
!
     ! real(Rkind) :: distdot
     ! logical :: stopbis, brkdn
     ! external distdot, stopbis, brkdn, bisinit
!
!     local variables
!
      integer(Ikind) :: i
      real   (Rkind) :: alpha, zz, zzm1
      logical        :: lp, rp
      save
!
!     check the status of the call
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10, 20, 40, 50, 60, 70, 80, 90, 100, 110), ipar(10)
!
!     initialization
!
      call bisinit(ipar,fpar,5*n,1_Ikind,lp,rp,wk)
      if (ipar(1)<0) return
!
!     request for matrix vector multiplication A*x in the initialization
!
      ipar(1) = 1
      ipar(8) = 1
      ipar(9) = 1 + n
      ipar(10) = 1
      do i = 1, n
         wk(i,1) = sol(i)
      enddo
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      do i = 1, n
         wk(i,1) = rhs(i) - wk(i,2)
      enddo
      fpar(11) = fpar(11) + n
!
!     if left preconditioned, precondition the initial residual
!
      if (lp) then
         ipar(1) = 3
         ipar(10) = 2
         return
      endif
!
 20   if (lp) then
         do i = 1, n
            wk(i,1) = wk(i,2)
         enddo
      endif
!
      zz = distdot(n,wk,1_Ikind,wk,1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      fpar(3) = sqrt(zz)
      fpar(5) = fpar(3)
      if (abs(ipar(3))==2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
         fpar(11) = fpar(11) + 2 * n
      else if (ipar(3)/=999) then
         fpar(4) = fpar(1) * fpar(3) + fpar(2)
      endif
!
!     normal iteration begins here, first half of the iteration
!     computes the conjugate direction
!
 30   continue
!
!     request the caller to perform a A^T r --> wk(:,3)
!
      if (lp) then
         ipar(1) = 4
         ipar(8) = 1
         if (rp) then
            ipar(9) = n + n + 1
         else
            ipar(9) = 3*n + 1
         endif
         ipar(10) = 3
         return
      endif
!
 40   ipar(1) = 2
      if (lp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = 1
      endif
      if (rp) then
         ipar(9) = 3*n + 1
      else
         ipar(9) = n + n + 1
      endif
      ipar(10) = 4
      return
!
 50   if (rp) then
         ipar(1) = 6
         ipar(8) = ipar(9)
         ipar(9) = n + n + 1
         ipar(10) = 5
         return
      endif
!
 60   ipar(7) = ipar(7) + 1
      zzm1 = zz
      zz = distdot(n,wk(1,3),1_Ikind,wk(1,3),1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      if (brkdn(zz,ipar)) goto 900
      if (ipar(7)>3) then
         alpha = zz / zzm1
         do i = 1, n
            wk(i,2) = wk(i,3) + alpha * wk(i,2)
         enddo
         fpar(11) = fpar(11) + 2 * n
      else
         do i = 1, n
            wk(i,2) = wk(i,3)
         enddo
      endif
!
!     before iteration can continue, we need to compute A * p
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = n + 1
         if (lp) then
            ipar(9) = ipar(8) + n
         else
            ipar(9) = 3*n + 1
         endif
         ipar(10) = 6
         return
      endif
!
 70   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = n + 1
      endif
      if (lp) then
        ipar(9) = 3*n+1
      else
         ipar(9) = n+n+1
      endif
      ipar(10) = 7
      return
!
 80   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = n+n+1
         ipar(10) = 8
         return
      endif
!
!     update the solution -- accumulate the changes in w(:,5)
!
 90   ipar(7) = ipar(7) + 1
      alpha = distdot(n,wk(1,3),1_Ikind,wk(1,3),1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      if (brkdn(alpha,ipar)) goto 900
      alpha = zz / alpha
      do i = 1, n
         wk(i,5) = wk(i,5) + alpha * wk(i,2)
         wk(i,1) = wk(i,1) - alpha * wk(i,3)
      enddo
      fpar(11) = fpar(11) + 4 * n
!
!     are we ready to terminate ?
!
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = 4*n + 1
         ipar(9) = 3*n + 1
         ipar(10) = 9
         return
      endif
 100  if (ipar(3)==999) then
         if (ipar(11)==1) goto 900
      else if (stopbis(n,ipar,1_Ikind,fpar,wk,wk(1,2),alpha)) then
         goto 900
      endif
!
!     continue the iterations
!
      goto 30
!
!     clean up -- necessary to accommodate the right-preconditioning
!
 900  if (rp) then
         if (ipar(1)<0) ipar(12) = ipar(1)
         ipar(1) = 5
         ipar(8) = 4*n + 1
         ipar(9) = ipar(8) - n
         ipar(10) = 10
         return
      endif
 110  if (rp) then
         call tidycg(n,ipar,fpar,sol,wk(1,4))
      else
         call tidycg(n,ipar,fpar,sol,wk(1,5))
      endif
      return
      END SUBROUTINE cgnr
!-----end-of-cgnr
!-----------------------------------------------------------------------
      SUBROUTINE bcg(n,rhs,sol,ipar,fpar,w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) ::  fpar(16), rhs(n), sol(n), w(n,*)
!-----------------------------------------------------------------------
!     BCG: Bi Conjugate Gradient method. Programmed with reverse
!     communication, see the header for detailed specifications
!     of the protocol.
!
!     in this routine, before successful return, the fpar's are
!     fpar(3) == initial residual norm
!     fpar(4) == target residual norm
!     fpar(5) == current residual norm
!     fpar(7) == current rho (rhok = <r, s>)
!     fpar(8) == previous rho (rhokm1)
!
!     w(:,1) -- r, the residual
!     w(:,2) -- s, the dual of the 'r'
!     w(:,3) -- p, the projection direction
!     w(:,4) -- q, the dual of the 'p'
!     w(:,5) -- v, a scratch vector to store A*p, or A*q.
!     w(:,6) -- a scratch vector to store intermediate results
!     w(:,7) -- changes in the solution
!-----------------------------------------------------------------------
!     external routines used
!
      ! real(Rkind) :: distdot
      ! logical :: stopbis,brkdn
      ! external distdot, stopbis, brkdn
!
      real(Rkind) :: one
      parameter(one=1.0e0_Rkind)
!
!     local variables
!
      integer(Ikind) :: i
      real   (Rkind) :: alpha
      logical        :: rp, lp
      save
!
!     status of the program
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10, 20, 40, 50, 60, 70, 80, 90, 100, 110), ipar(10)
!
!     initialization, initial residual
!
      call bisinit(ipar,fpar,7*n,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
!
!     compute initial residual, request a matvecc
!
      ipar(1) = 1
      ipar(8) = 3*n+1
      ipar(9) = ipar(8) + n
      do i = 1, n
         w(i,4) = sol(i)
      enddo
      ipar(10) = 1
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      do i = 1, n
         w(i,1) = rhs(i) - w(i,5)
      enddo
      fpar(11) = fpar(11) + n
      if (lp) then
         ipar(1) = 3
         ipar(8) = 1
         ipar(9) = n+1
         ipar(10) = 2
         return
      endif
!
 20   if (lp) then
         do i = 1, n
            w(i,1) = w(i,2)
            w(i,3) = w(i,2)
            w(i,4) = w(i,2)
         enddo
      else
         do i = 1, n
            w(i,2) = w(i,1)
            w(i,3) = w(i,1)
            w(i,4) = w(i,1)
         enddo
      endif
!
      fpar(7) = distdot(n,w,1_Ikind,w,1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      fpar(3) = sqrt(fpar(7))
      fpar(5) = fpar(3)
      fpar(8) = one
      if (abs(ipar(3))==2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
         fpar(11) = fpar(11) + 2 * n
      else if (ipar(3)/=999) then
         fpar(4) = fpar(1) * fpar(3) + fpar(2)
      endif
      if (ipar(3)>=0.and.fpar(5)<=fpar(4)) then
         fpar(6) = fpar(5)
         goto 900
      endif
!
!     end of initialization, begin iteration, v = A p
!
 30   if (rp) then
         ipar(1) = 5
         ipar(8) = n + n + 1
         if (lp) then
            ipar(9) = 4*n + 1
         else
            ipar(9) = 5*n + 1
         endif
         ipar(10) = 3
         return
      endif
!
 40   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = n + n + 1
      endif
      if (lp) then
         ipar(9) = 5*n + 1
      else
         ipar(9) = 4*n + 1
      endif
      ipar(10) = 4
      return
!
 50   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = 4*n + 1
         ipar(10) = 5
         return
      endif
!
 60   ipar(7) = ipar(7) + 1
      alpha = distdot(n,w(1,4),1_Ikind,w(1,5),1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      if (brkdn(alpha,ipar)) goto 900
      alpha = fpar(7) / alpha
      do i = 1, n
         w(i,7) = w(i,7) + alpha * w(i,3)
         w(i,1) = w(i,1) - alpha * w(i,5)
      enddo
      fpar(11) = fpar(11) + 4 * n
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = 6*n + 1
         ipar(9) = 5*n + 1
         ipar(10) = 6
         return
      endif
 70   if (ipar(3)==999) then
         if (ipar(11)==1) goto 900
      else if (stopbis(n,ipar,1_Ikind,fpar,w,w(1,3),alpha)) then
         goto 900
      endif
!
!     A^t * x
!
      if (lp) then
         ipar(1) = 4
         ipar(8) = 3*n + 1
         if (rp) then
            ipar(9) = 4*n + 1
         else
            ipar(9) = 5*n + 1
         endif
         ipar(10) = 7
         return
      endif
!
 80   ipar(1) = 2
      if (lp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = 3*n + 1
      endif
      if (rp) then
         ipar(9) = 5*n + 1
      else
         ipar(9) = 4*n + 1
      endif
      ipar(10) = 8
      return
!
 90   if (rp) then
         ipar(1) = 6
         ipar(8) = ipar(9)
         ipar(9) = 4*n + 1
         ipar(10) = 9
         return
      endif
!
 100  ipar(7) = ipar(7) + 1
      do i = 1, n
         w(i,2) = w(i,2) - alpha * w(i,5)
      enddo
      fpar(8) = fpar(7)
      fpar(7) = distdot(n,w,1_Ikind,w(1,2),1_Ikind)
      fpar(11) = fpar(11) + 4 * n
      if (brkdn(fpar(7), ipar)) return
      alpha = fpar(7) / fpar(8)
      do i = 1, n
         w(i,3) = w(i,1) + alpha * w(i,3)
         w(i,4) = w(i,2) + alpha * w(i,4)
      enddo
      fpar(11) = fpar(11) + 4 * n
!
!     end of the iterations
!
      goto 30
!
!     some clean up job to do
!
 900  if (rp) then
         if (ipar(1)<0) ipar(12) = ipar(1)
         ipar(1) = 5
         ipar(8) = 6*n + 1
         ipar(9) = ipar(8) - n
         ipar(10) = 10
         return
      endif
 110  if (rp) then
         call tidycg(n,ipar,fpar,sol,w(1,6))
      else
         call tidycg(n,ipar,fpar,sol,w(1,7))
      endif
      return
!-----end-of-bcg
      END SUBROUTINE bcg
!-----------------------------------------------------------------------
      SUBROUTINE bcgstab(n, rhs, sol, ipar, fpar, w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(n,8)
!-----------------------------------------------------------------------
!     BCGSTAB --- Bi Conjugate Gradient stabilized (BCGSTAB)
!     This is an improved BCG routine. (1) no matrix transpose is
!     involved. (2) the convergence is smoother.
!
!
!     Algorithm:
!     Initialization - r = b - A x, r0 = r, p = r, rho = (r0, r),
!     Iterate -
!     (1) v = A p
!     (2) alpha = rho / (r0, v)
!     (3) s = r - alpha v
!     (4) t = A s
!     (5) omega = (t, s) / (t, t)
!     (6) x = x + alpha * p + omega * s
!     (7) r = s - omega * t
!     convergence test goes here
!     (8) beta = rho, rho = (r0, r), beta = rho * alpha / (beta * omega)
!         p = r + beta * (p - omega * v)
!
!     in this routine, before successful return, the fpar's are
!     fpar(3) == initial (preconditionied-)residual norm
!     fpar(4) == target (preconditionied-)residual norm
!     fpar(5) == current (preconditionied-)residual norm
!     fpar(6) == current residual norm or error
!     fpar(7) == current rho (rhok = <r, r0>)
!     fpar(8) == alpha
!     fpar(9) == omega
!
!     Usage of the work space W
!     w(:, 1) = r0, the initial residual vector
!     w(:, 2) = r, current residual vector
!     w(:, 3) = s
!     w(:, 4) = t
!     w(:, 5) = v
!     w(:, 6) = p
!     w(:, 7) = tmp, used in preconditioning, etc.
!     w(:, 8) = delta x, the correction to the answer is accumulated
!               here, so that the right-preconditioning may be applied
!               at the end
!-----------------------------------------------------------------------
!     external routines used
!
     ! real(Rkind) :: distdot
     ! logical     :: stopbis, brkdn
     ! external distdot, stopbis, brkdn
!
      real(Rkind) :: one
      parameter(one=1.0e0_Rkind)
!
!     local variables
!
      integer(Ikind) :: i
      real   (Rkind) :: alpha,beta,rho,omega
      logical        :: lp, rp
      save lp, rp
!
!     where to go
!
      if (ipar(1)>0) then
         goto (10, 20, 40, 50, 60, 70, 80, 90, 100, 110) ipar(10)
      else if (ipar(1)<0) then
         goto 900
      endif
!
!     call the initialization routine
!
      call bisinit(ipar,fpar,8*n,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
!
!     perform a matvec to compute the initial residual
!
      ipar(1) = 1
      ipar(8) = 1
      ipar(9) = 1 + n
      do i = 1, n
         w(i,1) = sol(i)
      enddo
      ipar(10) = 1
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      do i = 1, n
         w(i,1) = rhs(i) - w(i,2)
      enddo
      fpar(11) = fpar(11) + n
      if (lp) then
         ipar(1) = 3
         ipar(10) = 2
         return
      endif
!
 20   if (lp) then
         do i = 1, n
            w(i,1) = w(i,2)
            w(i,6) = w(i,2)
         enddo
      else
         do i = 1, n
            w(i,2) = w(i,1)
            w(i,6) = w(i,1)
         enddo
      endif
!
      fpar(7) = distdot(n,w,1_Ikind,w,1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      fpar(5) = sqrt(fpar(7))
      fpar(3) = fpar(5)
      if (abs(ipar(3))==2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
         fpar(11) = fpar(11) + 2 * n
      else if (ipar(3)/=999) then
         fpar(4) = fpar(1) * fpar(3) + fpar(2)
      endif
      if (ipar(3)>=0) fpar(6) = fpar(5)
      if (ipar(3)>=0 .and. fpar(5)<=fpar(4) .and. ipar(3)/=999) then
         goto 900
      endif
!
!     beginning of the iterations
!
!     Step (1), v = A p
 30   if (rp) then
         ipar(1) = 5
         ipar(8) = 5*n+1
         if (lp) then
            ipar(9) = 4*n + 1
         else
            ipar(9) = 6*n + 1
         endif
         ipar(10) = 3
         return
      endif
!
 40   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = 5*n+1
      endif
      if (lp) then
         ipar(9) = 6*n + 1
      else
         ipar(9) = 4*n + 1
      endif
      ipar(10) = 4
      return
 50   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = 4*n + 1
         ipar(10) = 5
         return
      endif
!
 60   ipar(7) = ipar(7) + 1
!
!     step (2)
      alpha = distdot(n,w(1,1),1_Ikind,w(1,5),1_Ikind)
      fpar(11) = fpar(11) + 2 * n
      if (brkdn(alpha, ipar)) goto 900
      alpha = fpar(7) / alpha
      fpar(8) = alpha
!
!     step (3)
      do i = 1, n
         w(i,3) = w(i,2) - alpha * w(i,5)
      enddo
      fpar(11) = fpar(11) + 2 * n
!
!     Step (4): the second matvec -- t = A s
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = n+n+1
         if (lp) then
            ipar(9) = ipar(8)+n
         else
            ipar(9) = 6*n + 1
         endif
         ipar(10) = 6
         return
      endif
!
 70   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = n+n+1
      endif
      if (lp) then
         ipar(9) = 6*n + 1
      else
         ipar(9) = 3*n + 1
      endif
      ipar(10) = 7
      return
 80   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = 3*n + 1
         ipar(10) = 8
         return
      endif
 90   ipar(7) = ipar(7) + 1
!
!     step (5)
      omega = distdot(n,w(1,4),1_Ikind,w(1,4),1_Ikind)
      fpar(11) = fpar(11) + n + n
      if (brkdn(omega,ipar)) goto 900
      omega = distdot(n,w(1,4),1_Ikind,w(1,3),1_Ikind) / omega
      fpar(11) = fpar(11) + n + n
      if (brkdn(omega,ipar)) goto 900
      fpar(9) = omega
      alpha = fpar(8)
!
!     step (6) and (7)
      do i = 1, n
         w(i,7) = alpha * w(i,6) + omega * w(i,3)
         w(i,8) = w(i,8) + w(i,7)
         w(i,2) = w(i,3) - omega * w(i,4)
      enddo
      fpar(11) = fpar(11) + 6 * n + 1
!
!     convergence test
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = 7*n + 1
         ipar(9) = 6*n + 1
         ipar(10) = 9
         return
      endif
      if (stopbis(n,ipar,2_Ikind,fpar,w(1,2),w(1,7),one))  goto 900
 100  if (ipar(3)==999.and.ipar(11)==1) goto 900
!
!     step (8): computing new p and rho
      rho = fpar(7)
      fpar(7) = distdot(n,w(1,2),1_Ikind,w(1,1),1_Ikind)
      omega = fpar(9)
      beta = fpar(7) * fpar(8) / (fpar(9) * rho)
      do i = 1, n
         w(i,6) = w(i,2) + beta * (w(i,6) - omega * w(i,5))
      enddo
      fpar(11) = fpar(11) + 6 * n + 3
      if (brkdn(fpar(7),ipar)) goto 900
!
!     end of an iteration
!
      goto 30
!
!     some clean up job to do
!
 900  if (rp) then
         if (ipar(1)<0) ipar(12) = ipar(1)
         ipar(1) = 5
         ipar(8) = 7*n + 1
         ipar(9) = ipar(8) - n
         ipar(10) = 10
         return
      endif
 110  if (rp) then
         call tidycg(n,ipar,fpar,sol,w(1,7))
      else
         call tidycg(n,ipar,fpar,sol,w(1,8))
      endif
!
      return
!-----end-of-bcgstab
      END SUBROUTINE bcgstab
!-----------------------------------------------------------------------
      SUBROUTINE tfqmr(n, rhs, sol, ipar, fpar, w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(n,*)
!-----------------------------------------------------------------------
!     TFQMR --- transpose-free Quasi-Minimum Residual method
!     This is developed from BCG based on the principle of Quasi-Minimum
!     Residual, and it is transpose-free.
!
!     It uses approximate residual norm.
!
!     Internally, the fpar's are used as following:
!     fpar(3) --- initial residual norm squared
!     fpar(4) --- target residual norm squared
!     fpar(5) --- current residual norm squared
!
!     w(:,1) -- R, residual
!     w(:,2) -- R0, the initial residual
!     w(:,3) -- W
!     w(:,4) -- Y
!     w(:,5) -- Z
!     w(:,6) -- A * Y
!     w(:,7) -- A * Z
!     w(:,8) -- V
!     w(:,9) -- D
!     w(:,10) -- intermediate results of preconditioning
!     w(:,11) -- changes in the solution
!-----------------------------------------------------------------------
!     external functions
!
     ! real(Rkind) :: distdot
     ! logical     :: brkdn
     ! external brkdn, distdot
!
      real(Rkind) :: one,zero
      parameter(one=1.0e0_Rkind,zero=0.0e0_Rkind)
!
!     local variables
!
      integer(Ikind) :: i
      logical        :: lp, rp
      real   (Rkind) :: eta, sigma, theta, te, alpha, rho, tao
      save
!
!     status of the call (where to go)
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10,20,40,50,60,70,80,90,100,110), ipar(10)
!
!     initializations
!
      call bisinit(ipar,fpar,11*n,2_Ikind,lp,rp,w)
      if (ipar(1)<0) return
      ipar(1) = 1
      ipar(8) = 1
      ipar(9) = 1 + 6*n
      do i = 1, n
         w(i,1) = sol(i)
      enddo
      ipar(10) = 1
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      do i = 1, n
         w(i,1) = rhs(i) - w(i,7)
         w(i,9) = zero
      enddo
      fpar(11) = fpar(11) + n

      if (lp) then
         ipar(1) = 3
         ipar(9) = n+1
         ipar(10) = 2
         return
      endif
 20   continue
      if (lp) then
         do i = 1, n
            w(i,1) = w(i,2)
            w(i,3) = w(i,2)
         enddo
      else
         do i = 1, n
            w(i,2) = w(i,1)
            w(i,3) = w(i,1)
         enddo
      endif

      fpar(5) = sqrt(distdot(n,w,1_Ikind,w,1_Ikind))
      fpar(3) = fpar(5)
      tao = fpar(5)
      fpar(11) = fpar(11) + n + n
      if (abs(ipar(3))==2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
         fpar(11) = fpar(11) + n + n
      else if (ipar(3)/=999) then
         fpar(4) = fpar(1) * tao + fpar(2)
      endif
      te = zero
      rho = zero
!
!     begin iteration
!
 30   sigma = rho
      rho = distdot(n,w(1,2),1_Ikind,w(1,3),1_Ikind)
      fpar(11) = fpar(11) + n + n
      if (brkdn(rho,ipar)) goto 900
      if (ipar(7)==1) then
         alpha = zero
      else
         alpha = rho / sigma
      endif
      do i = 1, n
         w(i,4) = w(i,3) + alpha * w(i,5)
      enddo
      fpar(11) = fpar(11) + n + n
!
!     A * x -- with preconditioning
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = 3*n + 1
         if (lp) then
            ipar(9) = 5*n + 1
         else
            ipar(9) = 9*n + 1
         endif
         ipar(10) = 3
         return
      endif

 40   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = 3*n + 1
      endif
      if (lp) then
         ipar(9) = 9*n + 1
      else
         ipar(9) = 5*n + 1
      endif
      ipar(10) = 4
      return

 50   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = 5*n + 1
         ipar(10) = 5
         return
      endif
 60   ipar(7) = ipar(7) + 1
      do i = 1, n
         w(i,8) = w(i,6) + alpha * (w(i,7) + alpha * w(i,8))
      enddo
      sigma = distdot(n,w(1,2),1_Ikind,w(1,8),1_Ikind)
      fpar(11) = fpar(11) + 6 * n
      if (brkdn(sigma,ipar)) goto 900
      alpha = rho / sigma
      do i = 1, n
         w(i,5) = w(i,4) - alpha * w(i,8)
      enddo
      fpar(11) = fpar(11) + 2*n
!
!     the second A * x
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = 4*n + 1
         if (lp) then
            ipar(9) = 6*n + 1
         else
            ipar(9) = 9*n + 1
         endif
         ipar(10) = 6
         return
      endif
!
 70   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = 4*n + 1
      endif
      if (lp) then
         ipar(9) = 9*n + 1
      else
         ipar(9) = 6*n + 1
      endif
      ipar(10) = 7
      return
!
 80   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = 6*n + 1
         ipar(10) = 8
         return
      endif
 90   ipar(7) = ipar(7) + 1
      do i = 1, n
         w(i,3) = w(i,3) - alpha * w(i,6)
      enddo
!
!     update I
!
      theta = distdot(n,w(1,3),1_Ikind,w(1,3),1_Ikind) / (tao*tao)
      sigma = one / (one + theta)
      tao = tao * sqrt(sigma * theta)
      fpar(11) = fpar(11) + 4*n + 6
      if (brkdn(tao,ipar)) goto 900
      eta = sigma * alpha
      sigma = te / alpha
      te = theta * eta
      do i = 1, n
         w(i,9) = w(i,4) + sigma * w(i,9)
         w(i,11) = w(i,11) + eta * w(i,9)
         w(i,3) = w(i,3) - alpha * w(i,7)
      enddo
      fpar(11) = fpar(11) + 6 * n + 6
      if (ipar(7)==1) then
         if (ipar(3)==-1) then
            fpar(3) = eta * sqrt(distdot(n,w(1,9),1_Ikind,w(1,9),1_Ikind))
            fpar(4) = fpar(1)*fpar(3) + fpar(2)
            fpar(11) = fpar(11) + n + n + 4
         endif
      endif
!
!     update II
!
      theta = distdot(n,w(1,3),1_Ikind,w(1,3),1_Ikind) / (tao*tao)
      sigma = one / (one + theta)
      tao = tao * sqrt(sigma * theta)
      fpar(11) = fpar(11) + 8 + 2*n
      if (brkdn(tao,ipar)) goto 900
      eta = sigma * alpha
      sigma = te / alpha
      te = theta * eta
      do i = 1, n
         w(i,9) = w(i,5) + sigma * w(i,9)
         w(i,11) = w(i,11) + eta * w(i,9)
      enddo
      fpar(11) = fpar(11) + 4*n + 3
!
!     this is the correct over-estimate
!      fpar(5) = sqrt(real(ipar(7)+1)) * tao
!     this is an approximation
      fpar(5) = tao
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = 10*n + 1
         ipar(9) = 9*n + 1
         ipar(10) = 9
         return
      else if (ipar(3)<0) then
         fpar(6) = eta * sqrt(distdot(n,w(1,9),1_Ikind,w(1,9),1_Ikind))
         fpar(11) = fpar(11) + n + n + 2
      else
         fpar(6) = fpar(5)
      endif
      if (fpar(6)>fpar(4) .and. (ipar(7)<ipar(6) .or. ipar(6)<=0)) goto 30

 100  if (ipar(3)==999.and.ipar(11)==0) goto 30
!
!     clean up
!
 900  if (rp) then
         if (ipar(1)<0) ipar(12) = ipar(1)
         ipar(1) = 5
         ipar(8) = 10*n + 1
         ipar(9) = ipar(8) - n
         ipar(10) = 10
         return
      endif
 110  if (rp) then
         call tidycg(n,ipar,fpar,sol,w(1,10))
      else
         call tidycg(n,ipar,fpar,sol,w(1,11))
      endif
!
      return
      END SUBROUTINE tfqmr
!-----end-of-tfqmr
!-----------------------------------------------------------------------
      SUBROUTINE fom(n, rhs, sol, ipar, fpar, w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(*)
!-----------------------------------------------------------------------
!     This a version of The Full Orthogonalization Method (FOM) 
!     implemented with reverse communication. It is a simple restart 
!     version of the FOM algorithm and is implemented with plane 
!     rotations similarly to GMRES.
!
!  parameters:
!  ----------- 
!     ipar(5) == the dimension of the Krylov subspace
!     after every ipar(5) iterations, the FOM will restart with
!     the updated solution and recomputed residual vector.
!
!     the work space in `w' is used as follows:
!     (1) the basis for the Krylov subspace, size n*(m+1);
!     (2) the Hessenberg matrix, only the upper triangular
!     portion of the matrix is stored, size (m+1)*m/2 + 1
!     (3) three vectors, all are of size m, they are
!     the cosine and sine of the Givens rotations, the third one holds
!     the residuals, it is of size m+1.
!
!     TOTAL SIZE REQUIRED == (n+3)*(m+2) + (m+1)*m/2
!     Note: m == ipar(5). The default value for this is 15 if
!     ipar(5) <= 1.
!-----------------------------------------------------------------------
!     external functions used
!
     ! real(Rkind) :: distdot
     ! external distdot
!
      real(Rkind)  one, zero
      parameter(one=1.0e0_Rkind, zero=0.0e0_Rkind)
!
!     local variables, ptr and p2 are temporary pointers,
!     hes points to the Hessenberg matrix,
!     vc, vs point to the cosines and sines of the Givens rotations
!     vrn points to the vectors of residual norms, more precisely
!     the right hand side of the least square problem solved.
!
      integer(Ikind) :: i,ii,idx,k,m,ptr,p2,prs,hes,vc,vs,vrn
      real   (Rkind) :: alpha, c, s 
      logical        :: lp, rp
      save
!
!     check the status of the call
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10, 20, 30, 40, 50, 60, 70) ipar(10)
!
!     initialization
!
      if (ipar(5)<=1) then
         m = 15
      else
         m = ipar(5)
      endif
      idx = n * (m+1)
      hes = idx + n
      vc = hes + (m+1) * m / 2 + 1
      vs = vc + m
      vrn = vs + m
      i = vrn + m + 1
      call bisinit(ipar,fpar,i,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
!
!     request for matrix vector multiplication A*x in the initialization
!
 100  ipar(1) = 1
      ipar(8) = n+1
      ipar(9) = 1
      ipar(10) = 1
      k = 0
      do i = 1, n
         w(n+i) = sol(i)
      enddo
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      if (lp) then
         do i = 1, n
            w(n+i) = rhs(i) - w(i)
         enddo
         ipar(1) = 3
         ipar(10) = 2
         return
      else
         do i = 1, n
            w(i) = rhs(i) - w(i)
         enddo
      endif
      fpar(11) = fpar(11) + n
!
 20   alpha = sqrt(distdot(n,w,1_Ikind,w,1_Ikind))
      fpar(11) = fpar(11) + 2*n + 1
      if (ipar(7)==1 .and. ipar(3)/=999) then
         if (abs(ipar(3))==2) then
            fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
            fpar(11) = fpar(11) + 2*n
         else
            fpar(4) = fpar(1) * alpha + fpar(2)
         endif
         fpar(3) = alpha
      endif
      fpar(5) = alpha
      w(vrn+1) = alpha
      if (alpha<=fpar(4) .and. ipar(3)>=0 .and. ipar(3)/=999) then
         ipar(1) = 0
         fpar(6) = alpha
         goto 300
      endif
      alpha = one / alpha
      do ii = 1, n
         w(ii) = alpha * w(ii)
      enddo
      fpar(11) = fpar(11) + n
!
!     request for (1) right preconditioning
!     (2) matrix vector multiplication
!     (3) left preconditioning
!
 110  k = k + 1
      if (rp) then
         ipar(1) = 5
         ipar(8) = k*n - n + 1
         if (lp) then
            ipar(9) = k*n + 1
         else
            ipar(9) = idx + 1
         endif
         ipar(10) = 3
         return
      endif
!
 30   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = (k-1)*n + 1
      endif
      if (lp) then
         ipar(9) = idx + 1
      else
         ipar(9) = 1 + k*n
      endif
      ipar(10) = 4
      return
!
 40   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = k*n + 1
         ipar(10) = 5
         return
      endif
!
!     Modified Gram-Schmidt orthogonalization procedure
!     temporary pointer 'ptr' is pointing to the current column of the
!     Hessenberg matrix. 'p2' points to the new basis vector
!
 50   ipar(7) = ipar(7) + 1
      ptr = k * (k - 1) / 2 + hes
      p2 = ipar(9)
      call mgsro(.false.,n,n,k+1,k+1,fpar(11),w,w(ptr+1),ipar(12))
      if (ipar(12)<0) goto 200
!
!     apply previous Givens rotations to column.
!
      p2 = ptr + 1
      do i = 1, k-1
         ptr = p2
         p2 = p2 + 1
         alpha = w(ptr)
         c = w(vc+i)
         s = w(vs+i)
         w(ptr) = c * alpha + s * w(p2)
         w(p2) = c * w(p2) - s * alpha
      enddo
!
!     end of one Arnoldi iteration, alpha will store the estimated
!     residual norm at current stage
!
      fpar(11) = fpar(11) + 6*k

      prs = vrn+k
      alpha = fpar(5) 
      if (w(p2) /= zero) alpha = abs(w(p2+1)*w(prs)/w(p2)) 
      fpar(5) = alpha
!
      if (k>=m .or. (ipar(3)>=0 .and. alpha<=fpar(4)) &
          .or. (ipar(6)>0 .and. ipar(7)>=ipar(6))) goto 200
!
      call givens(w(p2), w(p2+1), c, s)
      w(vc+k) = c
      w(vs+k) = s
      alpha = - s * w(prs)
      w(prs) = c * w(prs)
      w(prs+1) = alpha
!
      if (w(p2)/=zero) goto 110
!
!     update the approximate solution, first solve the upper triangular
!     system, temporary pointer ptr points to the Hessenberg matrix,
!     prs points to the right-hand-side (also the solution) of the system.
!
 200  ptr = hes + k * (k + 1) / 2
      prs = vrn + k
      if (w(ptr)==zero) then
!
!     if the diagonal elements of the last column is zero, reduce k by 1
!     so that a smaller trianguler system is solved
!
         k = k - 1
         if (k>0) then
            goto 200
         else
            ipar(1) = -3
            ipar(12) = -4
            goto 300
         endif
      endif
      w(prs) = w(prs) / w(ptr)
      do i = k-1, 1, -1
         ptr = ptr - i - 1
         do ii = 1, i
            w(vrn+ii) = w(vrn+ii) - w(prs) * w(ptr+ii)
         enddo
         prs = prs - 1
         w(prs) = w(prs) / w(ptr)
      enddo
!
      do ii = 1, n
         w(ii) = w(ii) * w(prs)
      enddo
      do i = 1, k-1
         prs = prs + 1
         ptr = i*n
         do ii = 1, n
            w(ii) = w(ii) + w(prs) * w(ptr+ii)
         enddo
      enddo
      fpar(11) = fpar(11) + 2*(k-1)*n + n + k*(k+1)
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = 1
         ipar(9) = idx + 1
         ipar(10) = 6
         return
      endif
!
 60   if (rp) then
         do i = 1, n
            sol(i) = sol(i) + w(idx+i)
         enddo
      else
         do i = 1, n
            sol(i) = sol(i) + w(i)
         enddo
      endif
      fpar(11) = fpar(11) + n
!
!     process the complete stopping criteria
!
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = -1
         ipar(9) = idx + 1
         ipar(10) = 7
         return
      else if (ipar(3)<0) then
         if (ipar(7)<=m+1) then
            fpar(3) = abs(w(vrn+1))
            if (ipar(3)==-1) fpar(4) = fpar(1)*fpar(3)+fpar(2)
         endif
         alpha = abs(w(vrn+k))
      endif
      fpar(6) = alpha
!
!     do we need to restart ?
!
 70   if (ipar(12)/=0) then
         ipar(1) = -3
         goto 300
      endif
      if (ipar(7)<ipar(6) .or. ipar(6)<=0) then
         if (ipar(3)/=999) then
            if (fpar(6)>fpar(4)) goto 100
         else
            if (ipar(11)==0) goto 100
         endif
      endif
!
!     termination, set error code, compute convergence rate
!
      if (ipar(1)>0) then
         if (ipar(3)==999 .and. ipar(11)==1) then
            ipar(1) = 0
         else if (ipar(3)/=999 .and. fpar(6)<=fpar(4)) then
            ipar(1) = 0
         else if (ipar(7)>=ipar(6) .and. ipar(6)>0) then
            ipar(1) = -1
         else
            ipar(1) = -10
         endif
      endif
 300  if (fpar(3)/=zero .and. fpar(6)/=zero .and. ipar(7)>ipar(13)) then
         fpar(7) = log10(fpar(3) / fpar(6)) / dble(ipar(7)-ipar(13))
      else
         fpar(7) = zero
      endif
      return
      END SUBROUTINE fom
!-----end-of-fom-------------------------------------------------------- 
!-----------------------------------------------------------------------
      SUBROUTINE gmres(n, rhs, sol, ipar, fpar, w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(*)
!-----------------------------------------------------------------------
!     This a version of GMRES implemented with reverse communication.
!     It is a simple restart version of the GMRES algorithm.
!
!     ipar(5) == the dimension of the Krylov subspace
!     after every ipar(5) iterations, the GMRES will restart with
!     the updated solution and recomputed residual vector.
!
!     the space of the `w' is used as follows:
!     (1) the basis for the Krylov subspace, size n*(m+1);
!     (2) the Hessenberg matrix, only the upper triangular
!     portion of the matrix is stored, size (m+1)*m/2 + 1
!     (3) three vectors, all are of size m, they are
!     the cosine and sine of the Givens rotations, the third one holds
!     the residuals, it is of size m+1.
!
!     TOTAL SIZE REQUIRED == (n+3)*(m+2) + (m+1)*m/2
!     Note: m == ipar(5). The default value for this is 15 if
!     ipar(5) <= 1.
!-----------------------------------------------------------------------
!     external functions used
!
     ! real(Rkind) :: distdot
     ! external distdot
!
      real(Rkind) :: one, zero
      parameter(one=1.0e0_Rkind, zero=0.0e0_Rkind)
!
!     local variables, ptr and p2 are temporary pointers,
!     hess points to the Hessenberg matrix,
!     vc, vs point to the cosines and sines of the Givens rotations
!     vrn points to the vectors of residual norms, more precisely
!     the right hand side of the least square problem solved.
!
      integer(Ikind) :: i,ii,idx,k,m,ptr,p2,hess,vc,vs,vrn
      real(Rkind)    :: alpha, c, s
      logical        ::lp, rp
      save
!
!     check the status of the call
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10, 20, 30, 40, 50, 60, 70) ipar(10)
!
!     initialization
!
      if (ipar(5)<=1) then
         m = 15
      else
         m = ipar(5)
      endif
      idx = n * (m+1)
      hess = idx + n
      vc = hess + (m+1) * m / 2 + 1
      vs = vc + m
      vrn = vs + m
      i = vrn + m + 1
      call bisinit(ipar,fpar,i,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
!
!     request for matrix vector multiplication A*x in the initialization
!
 100  ipar(1) = 1
      ipar(8) = n+1
      ipar(9) = 1
      ipar(10) = 1
      k = 0
      do i = 1, n
         w(n+i) = sol(i)
      enddo
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      if (lp) then
         do i = 1, n
            w(n+i) = rhs(i) - w(i)
         enddo
         ipar(1) = 3
         ipar(10) = 2
         return
      else
         do i = 1, n
            w(i) = rhs(i) - w(i)
         enddo
      endif
      fpar(11) = fpar(11) + n
!
 20   alpha = sqrt(distdot(n,w,1_Ikind,w,1_Ikind))
      fpar(11) = fpar(11) + 2*n
      if (ipar(7)==1 .and. ipar(3)/=999) then
         if (abs(ipar(3))==2) then
            fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
            fpar(11) = fpar(11) + 2*n
         else
            fpar(4) = fpar(1) * alpha + fpar(2)
         endif
         fpar(3) = alpha
      endif
      fpar(5) = alpha
      w(vrn+1) = alpha
      if (alpha<=fpar(4) .and. ipar(3)>=0 .and. ipar(3)/=999) then
         ipar(1) = 0
         fpar(6) = alpha
         goto 300
      endif
      alpha = one / alpha
      do ii = 1, n
         w(ii) = alpha * w(ii)
      enddo
      fpar(11) = fpar(11) + n
!
!     request for (1) right preconditioning
!     (2) matrix vector multiplication
!     (3) left preconditioning
!
 110  k = k + 1
      if (rp) then
         ipar(1) = 5
         ipar(8) = k*n - n + 1
         if (lp) then
            ipar(9) = k*n + 1
         else
            ipar(9) = idx + 1
         endif
         ipar(10) = 3
         return
      endif
!
 30   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = (k-1)*n + 1
      endif
      if (lp) then
         ipar(9) = idx + 1
      else
         ipar(9) = 1 + k*n
      endif
      ipar(10) = 4
      return
!
 40   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = k*n + 1
         ipar(10) = 5
         return
      endif
!
!     Modified Gram-Schmidt orthogonalization procedure
!     temporary pointer 'ptr' is pointing to the current column of the
!     Hessenberg matrix. 'p2' points to the new basis vector
!
 50   ipar(7) = ipar(7) + 1
      ptr = k * (k - 1) / 2 + hess
      p2 = ipar(9)
      call mgsro(.false.,n,n,k+1,k+1,fpar(11),w,w(ptr+1),ipar(12))
      if (ipar(12)<0) goto 200
!
!     apply previous Givens rotations and generate a new one to eliminate
!     the subdiagonal element.
!
      p2 = ptr + 1
      do i = 1, k-1
         ptr = p2
         p2 = p2 + 1
         alpha = w(ptr)
         c = w(vc+i)
         s = w(vs+i)
         w(ptr) = c * alpha + s * w(p2)
         w(p2) = c * w(p2) - s * alpha
      enddo
      call givens(w(p2), w(p2+1), c, s)
      w(vc+k) = c
      w(vs+k) = s
      p2 = vrn + k
      alpha = - s * w(p2)
      w(p2) = c * w(p2)
      w(p2+1) = alpha
!
!     end of one Arnoldi iteration, alpha will store the estimated
!     residual norm at current stage
!
      fpar(11) = fpar(11) + 6*k + 2
      alpha = abs(alpha)
      fpar(5) = alpha
      if (k<m .and. .not.(ipar(3)>=0 .and. alpha<=fpar(4)) &
           .and. (ipar(6)<=0 .or. ipar(7)<ipar(6))) goto 110
!
!     update the approximate solution, first solve the upper triangular
!     system, temporary pointer ptr points to the Hessenberg matrix,
!     p2 points to the right-hand-side (also the solution) of the system.
!
 200  ptr = hess + k * (k + 1) / 2
      p2 = vrn + k
      if (w(ptr)==zero) then
!
!     if the diagonal elements of the last column is zero, reduce k by 1
!     so that a smaller trianguler system is solved [It should only
!     happen when the matrix is singular, and at most once!]
!
         k = k - 1
         if (k>0) then
            goto 200
         else
            ipar(1) = -3
            ipar(12) = -4
            goto 300
         endif
      endif
      w(p2) = w(p2) / w(ptr)
      do i = k-1, 1, -1
         ptr = ptr - i - 1
         do ii = 1, i
            w(vrn+ii) = w(vrn+ii) - w(p2) * w(ptr+ii)
         enddo
         p2 = p2 - 1
         w(p2) = w(p2) / w(ptr)
      enddo
!
      do ii = 1, n
         w(ii) = w(ii) * w(p2)
      enddo
      do i = 1, k-1
         ptr = i*n
         p2 = p2 + 1
         do ii = 1, n
            w(ii) = w(ii) + w(p2) * w(ptr+ii)
         enddo
      enddo
      fpar(11) = fpar(11) + 2*k*n - n + k*(k+1)
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = 1
         ipar(9) = idx + 1
         ipar(10) = 6
         return
      endif
!
 60   if (rp) then
         do i = 1, n
            sol(i) = sol(i) + w(idx+i)
         enddo
      else
         do i = 1, n
            sol(i) = sol(i) + w(i)
         enddo
      endif
      fpar(11) = fpar(11) + n
!
!     process the complete stopping criteria
!
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = -1
         ipar(9) = idx + 1
         ipar(10) = 7
         return
      else if (ipar(3)<0) then
         if (ipar(7)<=m+1) then
            fpar(3) = abs(w(vrn+1))
            if (ipar(3)==-1) fpar(4) = fpar(1)*fpar(3)+fpar(2)
         endif
         fpar(6) = abs(w(vrn+k))
      else
         fpar(6) = fpar(5)
      endif
!
!     do we need to restart ?
!
 70   if (ipar(12)/=0) then
         ipar(1) = -3
         goto 300
      endif
      if ((ipar(7)<ipar(6) .or. ipar(6)<=0) .and.       &
           ((ipar(3)==999.and.ipar(11)==0) .or.          &
           (ipar(3)/=999.and.fpar(6)>fpar(4)))) goto 100
!
!     termination, set error code, compute convergence rate
!
      if (ipar(1)>0) then
         if (ipar(3)==999 .and. ipar(11)==1) then
            ipar(1) = 0
         else if (ipar(3)/=999 .and. fpar(6)<=fpar(4)) then
            ipar(1) = 0
         else if (ipar(7)>=ipar(6) .and. ipar(6)>0) then
            ipar(1) = -1
         else
            ipar(1) = -10
         endif
      endif
 300  if (fpar(3)/=zero .and. fpar(6)/=zero .and. ipar(7)>ipar(13)) then
         fpar(7) = log10(fpar(3) / fpar(6)) / dble(ipar(7)-ipar(13))
      else
         fpar(7) = zero
      endif
      return
      END SUBROUTINE gmres
!-----end-of-gmres
!-----------------------------------------------------------------------
      SUBROUTINE dqgmres(n, rhs, sol, ipar, fpar, w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(*)
!-----------------------------------------------------------------------
!     DQGMRES -- Flexible Direct version of Quasi-General Minimum
!     Residual method. The right preconditioning can be varied from
!     step to step.
!
!     Work space used = n + lb * (2*n+4)
!     where lb = ipar(5) + 1 (default 16 if ipar(5) <= 1)
!-----------------------------------------------------------------------
!     local variables
!
      real   (Rkind) :: one,zero,deps
      parameter(one=1.0e0_Rkind,zero=0.0e0_Rkind)
      parameter(deps=1.0e-33_Rkind)
!
      integer(Ikind) :: i,ii,j,jp1,j0,k,ptrw,ptrv,iv,iw,ic,is,ihm,ihd,lb,ptr
      real   (Rkind) :: alpha,beta,psi,c,s
      logical        :: lp,rp,full
      
     ! real(Rkind) :: distdot
     ! external distdot,bisinit
      save
!
!     where to go
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10, 20, 40, 50, 60, 70) ipar(10)
!
!     locations of the work arrays. The arrangement is as follows:
!     w(1:n) -- temporary storage for the results of the preconditioning
!     w(iv+1:iw) -- the V's
!     w(iw+1:ic) -- the W's
!     w(ic+1:is) -- the COSINEs of the Givens rotations
!     w(is+1:ihm) -- the SINEs of the Givens rotations
!     w(ihm+1:ihd) -- the last column of the Hessenberg matrix
!     w(ihd+1:i) -- the inverse of the diagonals of the Hessenberg matrix
!
      if (ipar(5)<=1) then
         lb = 16
      else
         lb = ipar(5) + 1
      endif
      iv = n
      iw = iv + lb * n
      ic = iw + lb * n
      is = ic + lb
      ihm = is + lb
      ihd = ihm + lb
      i = ihd + lb
!
!     parameter check, initializations
!
      full = .false.
      call bisinit(ipar,fpar,i,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
      ipar(1) = 1
      if (lp) then
         do ii = 1, n
            w(iv+ii) = sol(ii)
         enddo
         ipar(8) = iv+1
         ipar(9) = 1
      else
         do ii = 1, n
            w(ii) = sol(ii)
         enddo
         ipar(8) = 1
         ipar(9) = iv+1
      endif
      ipar(10) = 1
      return
!
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      if (lp) then
         do i = 1, n
            w(i) = rhs(i) - w(i)
         enddo
         ipar(1) = 3
         ipar(8) = 1
         ipar(9) = iv+1
         ipar(10) = 2
         return
      else
         do i = 1, n
            w(iv+i) = rhs(i) - w(iv+i)
         enddo
      endif
      fpar(11) = fpar(11) + n
!
 20   alpha = sqrt(distdot(n, w(iv+1), 1_Ikind, w(iv+1), 1_Ikind))
      fpar(11) = fpar(11) + (n + n)
      if (abs(ipar(3))==2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
         fpar(11) = fpar(11) + 2*n
      else if (ipar(3)/=999) then
         fpar(4) = fpar(1) * alpha + fpar(2)
      endif
      fpar(3) = alpha
      fpar(5) = alpha
      psi = alpha
      if (alpha<=fpar(4)) then
         ipar(1) = 0
         fpar(6) = alpha
         goto 80
      endif
      alpha = one / alpha
      do i = 1, n
         w(iv+i) = w(iv+i) * alpha
      enddo
      fpar(11) = fpar(11) + n
      j = 0
!
!     iterations start here
!
 30   j = j + 1
      if (j>lb) j = j - lb
      jp1 = j + 1
      if (jp1>lb) jp1 = jp1 - lb
      ptrv = iv + (j-1)*n + 1
      ptrw = iv + (jp1-1)*n + 1
      if (.not.full) then
         if (j>jp1) full = .true.
      endif
      if (full) then
         j0 = jp1+1
         if (j0>lb) j0 = j0 - lb
      else
         j0 = 1
      endif
!
!     request the caller to perform matrix-vector multiplication and
!     preconditioning
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = ptrv
         ipar(9) = ptrv + iw - iv
         ipar(10) = 3
         return
      else
         do i = 0, n-1
            w(ptrv+iw-iv+i) = w(ptrv+i)
         enddo
      endif
!
 40   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = ptrv
      endif
      if (lp) then
         ipar(9) = 1
      else
         ipar(9) = ptrw
      endif
      ipar(10) = 4
      return
!
 50   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = ptrw
         ipar(10) = 5
         return
      endif
!
!     compute the last column of the Hessenberg matrix
!     modified Gram-schmidt procedure, orthogonalize against (lb-1)
!     previous vectors
!
 60   continue
      call mgsro(full,n,n,lb,jp1,fpar(11),w(iv+1),w(ihm+1),ipar(12))
      if (ipar(12)<0) then
         ipar(1) = -3
         goto 80
      endif
      beta = w(ihm+jp1)
!
!     incomplete factorization (QR factorization through Givens rotations)
!     (1) apply previous rotations [(lb-1) of them]
!     (2) generate a new rotation
!
      if (full) then
         w(ihm+jp1) = w(ihm+j0) * w(is+jp1)
         w(ihm+j0) = w(ihm+j0) * w(ic+jp1)
      endif
      i = j0
      do while (i/=j)
         k = i+1
         if (k>lb) k = k - lb
         c = w(ic+i)
         s = w(is+i)
         alpha = w(ihm+i)
         w(ihm+i) = c * alpha + s * w(ihm+k)
         w(ihm+k) = c * w(ihm+k) - s * alpha
         i = k
      enddo
      call givens(w(ihm+j), beta, c, s)
      if (full) then
         fpar(11) = fpar(11) + 6 * lb
      else
         fpar(11) = fpar(11) + 6 * j
      endif
!
!     detect whether diagonal element of this column is zero
!
      if (abs(w(ihm+j))<deps) then
         ipar(1) = -3
         goto 80
      endif
      w(ihd+j) = one / w(ihm+j)
      w(ic+j) = c
      w(is+j) = s
!
!     update the W's (the conjugate directions) -- essentially this is one
!     step of triangular solve.
!
      ptrw = iw+(j-1)*n + 1
      if (full) then
         do i = j+1, lb
            alpha = -w(ihm+i)*w(ihd+i)
            ptr = iw+(i-1)*n+1
            do ii = 0, n-1
               w(ptrw+ii) = w(ptrw+ii) + alpha * w(ptr+ii)
            enddo
         enddo
      endif
      do i = 1, j-1
         alpha = -w(ihm+i)*w(ihd+i)
         ptr = iw+(i-1)*n+1
         do ii = 0, n-1
            w(ptrw+ii) = w(ptrw+ii) + alpha * w(ptr+ii)
         enddo
      enddo
!
!     update the solution to the linear system
!
      alpha = psi * c * w(ihd+j)
      psi = - s * psi
      do i = 1, n
         sol(i) = sol(i) + alpha * w(ptrw-1+i)
      enddo
      if (full) then
         fpar(11) = fpar(11) + lb * (n+n)
      else
         fpar(11) = fpar(11) + j * (n+n)
      endif
!
!     determine whether to continue,
!     compute the desired error/residual norm
!
      ipar(7) = ipar(7) + 1
      fpar(5) = abs(psi)
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = -1
         ipar(9) = 1
         ipar(10) = 6
         return
      endif
      if (ipar(3)<0) then
         alpha = abs(alpha)
         if (ipar(7)==2 .and. ipar(3)==-1) then
            fpar(3) = alpha*sqrt(distdot(n, w(ptrw), 1_Ikind, w(ptrw), 1_Ikind))
            fpar(4) = fpar(1) * fpar(3) + fpar(2)
            fpar(6) = fpar(3)
         else
            fpar(6) = alpha*sqrt(distdot(n, w(ptrw), 1_Ikind, w(ptrw), 1_Ikind))
         endif
         fpar(11) = fpar(11) + 2 * n
      else
         fpar(6) = fpar(5)
      endif
      if (ipar(1)>=0 .and. fpar(6)>fpar(4) .and. (ipar(6)<=0    &
          .or. ipar(7)<ipar(6))) goto 30
 70   if (ipar(3)==999 .and. ipar(11)==0) goto 30
!
!     clean up the iterative solver
!
 80   fpar(7) = zero
      if (fpar(3)/=zero .and. fpar(6)/=zero .and.                  &
          ipar(7)>ipar(13))                                        &
          fpar(7) = log10(fpar(3) / fpar(6)) / dble(ipar(7)-ipar(13))  
      if (ipar(1)>0) then
         if (ipar(3)==999 .and. ipar(11)/=0) then
            ipar(1) = 0
         else if (fpar(6)<=fpar(4)) then
            ipar(1) = 0
         else if (ipar(6)>0 .and. ipar(7)>=ipar(6)) then
            ipar(1) = -1
         else
            ipar(1) = -10
         endif
      endif
      return
      END SUBROUTINE dqgmres
!-----end-of-dqgmres
!-----------------------------------------------------------------------
      SUBROUTINE fgmres(n, rhs, sol, ipar, fpar, w)
      integer(Ikind) :: n, ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(*)
!-----------------------------------------------------------------------
!     This a version of FGMRES implemented with reverse communication.
!
!     ipar(5) == the dimension of the Krylov subspace
!
!     the space of the `w' is used as follows:
!     >> V: the bases for the Krylov subspace, size n*(m+1);
!     >> W: the above bases after (left-)multiplying with the
!     right-preconditioner inverse, size m*n;
!     >> a temporary vector of size n;
!     >> the Hessenberg matrix, only the upper triangular portion
!     of the matrix is stored, size (m+1)*m/2 + 1
!     >> three vectors, first two are of size m, they are the cosine
!     and sine of the Givens rotations, the third one holds the
!     residuals, it is of size m+1.
!
!     TOTAL SIZE REQUIRED == n*(2m+1) + (m+1)*m/2 + 3*m + 2
!     Note: m == ipar(5). The default value for this is 15 if
!     ipar(5) <= 1.
!-----------------------------------------------------------------------
!     external functions used
!
     ! real   (Rkind) :: distdot
     ! external distdot
!
      real   (Rkind) ::  one, zero
      parameter(one=1.0e0_Rkind, zero=0.0e0_Rkind)
!
!     local variables, ptr and p2 are temporary pointers,
!     hess points to the Hessenberg matrix,
!     vc, vs point to the cosines and sines of the Givens rotations
!     vrn points to the vectors of residual norms, more precisely
!     the right hand side of the least square problem solved.
!
      integer(Ikind) :: i,ii,idx,iz,k,m,ptr,p2,hess,vc,vs,vrn
      real   (Rkind) :: alpha, c, s
      logical        :: lp, rp
      save
!
!     check the status of the call
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (10, 20, 30, 40, 50, 60) ipar(10)
!
!     initialization
!
      if (ipar(5)<=1) then
         m = 15
      else
         m = ipar(5)
      endif
      idx = n * (m+1)
      iz = idx + n
      hess = iz + n*m
      vc = hess + (m+1) * m / 2 + 1
      vs = vc + m
      vrn = vs + m
      i = vrn + m + 1
      call bisinit(ipar,fpar,i,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
!
!     request for matrix vector multiplication A*x in the initialization
!
 100  ipar(1) = 1
      ipar(8) = n+1
      ipar(9) = 1
      ipar(10) = 1
      k = 0
      do ii = 1, n
         w(ii+n) = sol(ii)
      enddo
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      fpar(11) = fpar(11) + n
      if (lp) then
         do i = 1, n
            w(n+i) = rhs(i) - w(i)
         enddo
         ipar(1) = 3
         ipar(10) = 2
         return
      else
         do i = 1, n
            w(i) = rhs(i) - w(i)
         enddo
      endif
!
 20   alpha = sqrt(distdot(n,w,1_Ikind,w,1_Ikind))
      fpar(11) = fpar(11) + n + n
      if (ipar(7)==1 .and. ipar(3)/=999) then
         if (abs(ipar(3))==2) then
            fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
            fpar(11) = fpar(11) + 2*n
         else
            fpar(4) = fpar(1) * alpha + fpar(2)
         endif
         fpar(3) = alpha
      endif
      fpar(5) = alpha
      w(vrn+1) = alpha
      if (alpha<=fpar(4) .and. ipar(3)>=0 .and. ipar(3)/=999) then
         ipar(1) = 0
         fpar(6) = alpha
         goto 300
      endif
      alpha = one / alpha
      do ii = 1, n
         w(ii) = w(ii) * alpha
      enddo
      fpar(11) = fpar(11) + n
!
!     request for (1) right preconditioning
!     (2) matrix vector multiplication
!     (3) left preconditioning
!
 110  k = k + 1
      if (rp) then
         ipar(1) = 5
         ipar(8) = k*n - n + 1
         ipar(9) = iz + ipar(8)
         ipar(10) = 3
         return
      else
         do ii = 0, n-1
            w(iz+k*n-ii) = w(k*n-ii)
         enddo
      endif
!
 30   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = (k-1)*n + 1
      endif
      if (lp) then
         ipar(9) = idx + 1
      else
         ipar(9) = 1 + k*n
      endif
      ipar(10) = 4
      return
!
 40   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = k*n + 1
         ipar(10) = 5
         return
      endif
!
!     Modified Gram-Schmidt orthogonalization procedure
!     temporary pointer 'ptr' is pointing to the current column of the
!     Hessenberg matrix. 'p2' points to the new basis vector
!
 50   ptr = k * (k - 1) / 2 + hess
      p2 = ipar(9)
      ipar(7) = ipar(7) + 1
      call mgsro(.false.,n,n,k+1,k+1,fpar(11),w,w(ptr+1),ipar(12))
      if (ipar(12)<0) goto 200
!
!     apply previous Givens rotations and generate a new one to eliminate
!     the subdiagonal element.
!
      p2 = ptr + 1
      do i = 1, k-1
         ptr = p2
         p2 = p2 + 1
         alpha = w(ptr)
         c = w(vc+i)
         s = w(vs+i)
         w(ptr) = c * alpha + s * w(p2)
         w(p2) = c * w(p2) - s * alpha
      enddo
      call givens(w(p2), w(p2+1), c, s)
      w(vc+k) = c
      w(vs+k) = s
      p2 = vrn + k
      alpha = - s * w(p2)
      w(p2) = c * w(p2)
      w(p2+1) = alpha
      fpar(11) = fpar(11) + 6 * k
!
!     end of one Arnoldi iteration, alpha will store the estimated
!     residual norm at current stage
!
      alpha = abs(alpha)
      fpar(5) = alpha
      if (k<m .and. .not.(ipar(3)>=0 .and. alpha<=fpar(4))    &
            .and. (ipar(6)<=0 .or. ipar(7)<ipar(6))) goto 110
!
!     update the approximate solution, first solve the upper triangular
!     system, temporary pointer ptr points to the Hessenberg matrix,
!     p2 points to the right-hand-side (also the solution) of the system.
!
 200  ptr = hess + k * (k + 1 ) / 2
      p2 = vrn + k
      if (w(ptr)==zero) then
!
!     if the diagonal elements of the last column is zero, reduce k by 1
!     so that a smaller trianguler system is solved [It should only
!     happen when the matrix is singular!]
!
         k = k - 1
         if (k>0) then
            goto 200
         else
            ipar(1) = -3
            ipar(12) = -4
            goto 300
         endif
      endif
      w(p2) = w(p2) / w(ptr)
      do i = k-1, 1, -1
         ptr = ptr - i - 1
         do ii = 1, i
            w(vrn+ii) = w(vrn+ii) - w(p2) * w(ptr+ii)
         enddo
         p2 = p2 - 1
         w(p2) = w(p2) / w(ptr)
      enddo
!
      do i = 0, k-1
         ptr = iz+i*n
         do ii = 1, n
            sol(ii) = sol(ii) + w(p2)*w(ptr+ii)
         enddo
         p2 = p2 + 1
      enddo
      fpar(11) = fpar(11) + 2*k*n + k*(k+1)
!
!     process the complete stopping criteria
!
      if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = -1
         ipar(9) = idx + 1
         ipar(10) = 6
         return
      else if (ipar(3)<0) then
         if (ipar(7)<=m+1) then
            fpar(3) = abs(w(vrn+1))
            if (ipar(3)==-1) fpar(4) = fpar(1)*fpar(3)+fpar(2)
         endif
         fpar(6) = abs(w(vrn+k))
      else if (ipar(3)/=999) then
         fpar(6) = fpar(5)
      endif
!
!     do we need to restart ?
!
 60   if (ipar(12)/=0) then
         ipar(1) = -3
         goto 300
      endif
      if ((ipar(7)<ipar(6) .or. ipar(6)<=0).and.         & 
           ((ipar(3)==999.and.ipar(11)==0) .or.           &
           (ipar(3)/=999.and.fpar(6)>fpar(4)))) goto 100
!
!     termination, set error code, compute convergence rate
!
      if (ipar(1)>0) then
         if (ipar(3)==999 .and. ipar(11)==1) then
            ipar(1) = 0
         else if (ipar(3)/=999 .and. fpar(6)<=fpar(4)) then
            ipar(1) = 0
         else if (ipar(7)>=ipar(6) .and. ipar(6)>0) then
            ipar(1) = -1
         else
            ipar(1) = -10
         endif
      endif
 300  if (fpar(3)/=zero .and. fpar(6)/=zero .and. ipar(7)>ipar(13)) then
         fpar(7) = log10(fpar(3) / fpar(6)) / dble(ipar(7)-ipar(13))
      else
         fpar(7) = zero
      endif
      return
      END SUBROUTINE fgmres
!-----end-of-fgmres
!-----------------------------------------------------------------------
      SUBROUTINE dbcg (n,rhs,sol,ipar,fpar,w)
      integer(Ikind) :: n,ipar(16)
      real   (Rkind) :: rhs(n), sol(n), fpar(16), w(n,*)
!-----------------------------------------------------------------------
! Quasi GMRES method for solving a linear
! system of equations a * sol = y.  double precision version.
! this version is without restarting and without preconditioning.
! parameters :
! -----------
! n     = dimension of the problem
!
! y     = w(:,1) a temporary storage used for various operations
! z     = w(:,2) a work vector of length n.
! v     = w(:,3:4) size n x 2
! w     = w(:,5:6) size n x 2
! p     = w(:,7:9) work array of dimension n x 3
! del x = w(:,10)  accumulation of the changes in solution
! tmp   = w(:,11)  a temporary vector used to hold intermediate result of
!                  preconditioning, etc.
!
! sol   = the solution of the problem . at input sol must contain an
!         initial guess to the solution.
!    ***  note:   y is destroyed on return.
!
!-----------------------------------------------------------------------
! subroutines and functions called:
! 1) matrix vector multiplication and preconditioning through reverse
!     communication
!
! 2) implu, uppdir, distdot (blas)
!-----------------------------------------------------------------------
! aug. 1983  version.    author youcef saad. yale university computer
! science dept. some  changes made july 3, 1986.
! references: siam j. sci. stat. comp., vol. 5, pp. 203-228 (1984)
!-----------------------------------------------------------------------
!     local variables
!
      real   (Rkind) :: one,zero
      parameter(one=1.0e0_Rkind,zero=0.0e0_Rkind)
!
      integer(Ikind) ::  k,j,i,i2,ip2,ju,lb,lbm1,np,indp
      real   (Rkind) :: t,sqrt,ss,res,beta,ss1,delta,x,zeta,umm
      real   (Rkind) :: ypiv(3),u(3),usav(3)
      logical        ::lp,rp,full, perm(3)
      
     ! real(Rkind) :: distdot
     ! external tidycg
      save
!
!     where to go
!
      if (ipar(1)<=0) ipar(10) = 0
      goto (110, 120, 130, 140, 150, 160, 170, 180, 190, 200) ipar(10)
!
!     initialization, parameter checking, clear the work arrays
!
      call bisinit(ipar,fpar,11*n,1_Ikind,lp,rp,w)
      if (ipar(1)<0) return
      perm(1) = .false.
      perm(2) = .false.
      perm(3) = .false.
      usav(1) = zero
      usav(2) = zero
      usav(3) = zero
      ypiv(1) = zero
      ypiv(2) = zero
      ypiv(3) = zero
!-----------------------------------------------------------------------
!     initialize constants for outer loop :
!-----------------------------------------------------------------------
      lb = 3
      lbm1 = 2
!
!     get initial residual vector and norm
!
      ipar(1) = 1
      ipar(8) = 1
      ipar(9) = 1 + n
      do i = 1, n
         w(i,1) = sol(i)
      enddo
      ipar(10) = 1
      return
 110  ipar(7) = ipar(7) + 1
      ipar(13) = ipar(13) + 1
      if (lp) then
         do i = 1, n
            w(i,1) = rhs(i) - w(i,2)
         enddo
         ipar(1) = 3
         ipar(8) = 1
         ipar(9) = n+n+1
         ipar(10) = 2
         return
      else
         do i = 1, n
            w(i,3) = rhs(i) - w(i,2)
         enddo
      endif
      fpar(11) = fpar(11) + n
!
 120  fpar(3) = sqrt(distdot(n,w(1,3),1_Ikind,w(1,3),1_Ikind))
      fpar(11) = fpar(11) + n + n
      fpar(5) = fpar(3)
      fpar(7) = fpar(3)
      zeta = fpar(3)
      if (abs(ipar(3))==2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1_Ikind,rhs,1_Ikind)) + fpar(2)
         fpar(11) = fpar(11) + 2*n
      else if (ipar(3)/=999) then
         fpar(4) = fpar(1) * zeta + fpar(2)
      endif
      if (ipar(3)>=0.and.fpar(5)<=fpar(4)) then
         fpar(6) = fpar(5)
         goto 900
      endif
!
!     normalize first arnoldi vector
!
      t = one/zeta
      do 22 k=1,n
         w(k,3) = w(k,3)*t
         w(k,5) = w(k,3)
 22   continue
      fpar(11) = fpar(11) + n
!
!     initialize constants for main loop
!
      beta = zero
      delta = zero
      i2 = 1
      indp = 0
      i = 0
!
!     main loop: i = index of the loop.
!
!-----------------------------------------------------------------------
 30   i = i + 1
!
      if (rp) then
         ipar(1) = 5
         ipar(8) = (1+i2)*n+1
         if (lp) then
            ipar(9) = 1
         else
            ipar(9) = 10*n + 1
         endif
         ipar(10) = 3
         return
      endif
!
 130  ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = (1+i2)*n + 1
      endif
      if (lp) then
         ipar(9) = 10*n + 1
      else
         ipar(9) = 1
      endif
      ipar(10) = 4
      return
!
 140  if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = 1
         ipar(10) = 5
         return
      endif
!
!     A^t * x
!
 150  ipar(7) = ipar(7) + 1
      if (lp) then
         ipar(1) = 4
         ipar(8) = (3+i2)*n + 1
         if (rp) then
            ipar(9) = n + 1
         else
            ipar(9) = 10*n + 1
         endif
         ipar(10) = 6
         return
      endif
!
 160  ipar(1) = 2
      if (lp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = (3+i2)*n + 1
      endif
      if (rp) then
         ipar(9) = 10*n + 1
      else
         ipar(9) = n + 1
      endif
      ipar(10) = 7
      return
!
 170  if (rp) then
         ipar(1) = 6
         ipar(8) = ipar(9)
         ipar(9) = n + 1
         ipar(10) = 8
         return
      endif
!-----------------------------------------------------------------------
!     orthogonalize current v against previous v's and
!     determine relevant part of i-th column of u(.,.) the
!     upper triangular matrix --
!-----------------------------------------------------------------------
 180  ipar(7) = ipar(7) + 1
      u(1) = zero
      ju = 1
      k = i2
      if (i <= lbm1) ju = 0
      if (i < lb) k = 0
 31   if (k == lbm1) k=0
      k=k+1
!
      if (k /= i2) then
         ss  = delta
         ss1 = beta
         ju = ju + 1
         u(ju) = ss
      else
         ss = distdot(n,w(1,1),1_Ikind,w(1,4+k),1_Ikind)
         fpar(11) = fpar(11) + 2*n
         ss1= ss
         ju = ju + 1
         u(ju) = ss
      endif
!
      do 32  j=1,n
         w(j,1) = w(j,1) - ss*w(j,k+2)
         w(j,2) = w(j,2) - ss1*w(j,k+4)
 32   continue
      fpar(11) = fpar(11) + 4*n
!
      if (k /= i2) goto 31
!
!     end of Mod. Gram. Schmidt loop
!
      t = distdot(n,w(1,2),1_Ikind,w(1,1),1_Ikind)
!
      beta   = sqrt(abs(t))
      delta  = t/beta
!
      ss = one/beta
      ss1 = one/ delta
!
!     normalize and insert new vectors
!
      ip2 = i2
      if (i2 == lbm1) i2=0
      i2=i2+1
!
      do 315 j=1,n
         w(j,i2+2)=w(j,1)*ss
         w(j,i2+4)=w(j,2)*ss1
 315  continue
      fpar(11) = fpar(11) + 4*n
!-----------------------------------------------------------------------
!     end of orthogonalization.
!     now compute the coefficients u(k) of the last
!     column of the  l . u  factorization of h .
!-----------------------------------------------------------------------
!riad      np = min0(i,lb)
      np = min(i,lb)
      full = (i >= lb)
      call implu(np, umm, beta, ypiv, u, perm, full)
!-----------------------------------------------------------------------
!     update conjugate directions and solution
!-----------------------------------------------------------------------
      do 33 k=1,n
         w(k,1) = w(k,ip2+2)
 33   continue
      call uppdir(n, w(1,7), np, lb, indp, w, u, usav, fpar(11))
!-----------------------------------------------------------------------
      if (i == 1) goto 34
      j = np - 1
      if (full) j = j-1
      if (.not.perm(j)) zeta = -zeta*ypiv(j)
 34   x = zeta/u(np)
      if (perm(np))goto 36
      do 35 k=1,n
         w(k,10) = w(k,10) + x*w(k,1)
 35   continue
      fpar(11) = fpar(11) + 2 * n
!-----------------------------------------------------------------------
 36   if (ipar(3)==999) then
         ipar(1) = 10
         ipar(8) = 9*n + 1
         ipar(9) = 10*n + 1
         ipar(10) = 9
         return
      endif
      res = abs(beta*zeta/umm)
      fpar(5) = res * sqrt(distdot(n, w(1,i2+2), 1_Ikind, w(1,i2+2), 1_Ikind))
      fpar(11) = fpar(11) + 2 * n
      if (ipar(3)<0) then
         fpar(6) = x * sqrt(distdot(n,w,1_Ikind,w,1_Ikind))
         fpar(11) = fpar(11) + 2 * n
         if (ipar(7)<=3) then
            fpar(3) = fpar(6)
            if (ipar(3)==-1) then
               fpar(4) = fpar(1) * sqrt(fpar(3)) + fpar(2)
            endif
         endif
      else
         fpar(6) = fpar(5)
      endif
!---- convergence test -----------------------------------------------
 190  if (ipar(3)==999.and.ipar(11)==0) then
         goto 30
      else if (fpar(6)>fpar(4) .and. (ipar(6)>ipar(7) .or.ipar(6)<=0)) then
         goto 30
      endif
!-----------------------------------------------------------------------
!     here the fact that the last step is different is accounted for.
!-----------------------------------------------------------------------
      if (.not. perm(np)) goto 900
      x = zeta/umm
      do 40 k = 1,n
         w(k,10) = w(k,10) + x*w(k,1)
 40   continue
      fpar(11) = fpar(11) + 2 * n
!
!     right preconditioning and clean-up jobs
!
 900  if (rp) then
         if (ipar(1)<0) ipar(12) = ipar(1)
         ipar(1) = 5
         ipar(8) = 9*n + 1
         ipar(9) = ipar(8) + n
         ipar(10) = 10
         return
      endif
 200  if (rp) then
         call tidycg(n,ipar,fpar,sol,w(1,11))
      else
         call tidycg(n,ipar,fpar,sol,w(1,10))
      endif
      return
      END SUBROUTINE dbcg
!-----end-of-dbcg-------------------------------------------------------
!-----------------------------------------------------------------------
      SUBROUTINE implu(np,umm,beta,ypiv,u,permut,full)
      integer(Ikind) :: np,k,npm1      
      real   (Rkind) :: umm,beta,ypiv(*),u(*),x, xpiv
      logical        :: full, perm, permut(*)
!-----------------------------------------------------------------------
!     performs implicitly one step of the lu factorization of a
!     banded hessenberg matrix.
!-----------------------------------------------------------------------
      if (np <= 1) goto 12
      npm1 = np - 1
!
!     -- perform  previous step of the factorization-
!
      do 6 k=1,npm1
         if (.not. permut(k)) goto 5
         x=u(k)
         u(k) = u(k+1)
         u(k+1) = x
 5       u(k+1) = u(k+1) - ypiv(k)*u(k)
 6    continue
!-----------------------------------------------------------------------
!     now determine pivotal information to be used in the next call
!-----------------------------------------------------------------------
 12   umm = u(np)
      perm = (beta > abs(umm))
      if (.not. perm) goto 4
      xpiv = umm / beta
      u(np) = beta
      goto 8
 4    xpiv = beta/umm
 8    permut(np) = perm
      ypiv(np) = xpiv
      if (.not. full) return
!     shift everything up if full...
      do 7 k=1,npm1
         ypiv(k) = ypiv(k+1)
         permut(k) = permut(k+1)
 7    continue
      return
!-----end-of-implu
      END SUBROUTINE implu
!-----------------------------------------------------------------------
      SUBROUTINE uppdir(n,p,np,lbp,indp,y,u,usav,flops)
      integer(Ikind) :: k,np,n,npm1,j,ju,indp,lbp      
      real   (Rkind) :: p(n,lbp), y(*), u(*), usav(*), x, flops
!-----------------------------------------------------------------------
!     updates the conjugate directions p given the upper part of the
!     banded upper triangular matrix u.  u contains the non zero
!     elements of the column of the triangular matrix..
!-----------------------------------------------------------------------
      real(Rkind)    :: zero
      parameter(zero=0.0e0_Rkind)
!
      npm1=np-1
      if (np <= 1) goto 12
      j=indp
      ju = npm1
 10   if (j <= 0) j=lbp
      x = u(ju) /usav(j)
      if (x == zero) goto 115
      do 11 k=1,n
         y(k) = y(k) - x*p(k,j)
 11   continue
      flops = flops + 2*n
 115  j = j-1
      ju = ju -1
      if (ju >= 1) goto 10
 12   indp = indp + 1
      if (indp > lbp) indp = 1
      usav(indp) = u(np)
      do 13 k=1,n
         p(k,indp) = y(k)
 13   continue
 208  return
!-----------------------------------------------------------------------
!-------end-of-uppdir---------------------------------------------------
      END SUBROUTINE uppdir
      SUBROUTINE givens(x,y,c,s)
      real(Rkind)    :: x,y,c,s
!-----------------------------------------------------------------------
!     Given x and y, this subroutine generates a Givens' rotation c, s.
!     And apply the rotation on (x,y) ==> (sqrt(x**2 + y**2), 0).
!     (See P 202 of "matrix computation" by Golub and van Loan.)
!-----------------------------------------------------------------------
      real(Rkind)    :: t,one,zero
      parameter (zero=0.0e0_Rkind,one=1.0e0_Rkind)
!
      if (x==zero .and. y==zero) then
         c = one
         s = zero
      else if (abs(y)>abs(x)) then
         t = x / y
         x = sqrt(one+t*t)
         s = sign(one / x, y)
         c = t*s
      else if (abs(y)<=abs(x)) then
         t = y / x
         y = sqrt(one+t*t)
         c = sign(one / y, x)
         s = t*c
      else
!
!     X or Y must be an invalid floating-point number, set both to zero
!
         x = zero
         y = zero
         c = one
         s = zero
      endif
      x = abs(x*y)
!
!     end of givens
!
      return
      END SUBROUTINE givens
!-----end-of-givens
!-----------------------------------------------------------------------
      logical FUNCTION stopbis(n,ipar,mvpi,fpar,r,delx,sx)
      integer(Ikind) :: n,mvpi,ipar(16)
      real   (Rkind) :: fpar(16), r(n), delx(n), sx
      
     ! real(Rkind) :: distdot
     ! external distdot
!-----------------------------------------------------------------------
!     function for determining the stopping criteria. return value of
!     true if the stopbis criteria is satisfied.
!-----------------------------------------------------------------------
      if (ipar(11) == 1) then
         stopbis = .true.
      else
         stopbis = .false.
      endif
      if (ipar(6)>0 .and. ipar(7)>=ipar(6)) then
         ipar(1) = -1
         stopbis = .true.
      endif
      if (stopbis) return
!
!     computes errors
!
      fpar(5) = sqrt(distdot(n,r,1_Ikind,r,1_Ikind))
      fpar(11) = fpar(11) + 2 * n
      if (ipar(3)<0) then
!
!     compute the change in the solution vector
!
         fpar(6) = sx * sqrt(distdot(n,delx,1_Ikind,delx,1_Ikind))
         fpar(11) = fpar(11) + 2 * n
         if (ipar(7)<mvpi+mvpi+1) then
!
!     if this is the end of the first iteration, set fpar(3:4)
!
            fpar(3) = fpar(6)
            if (ipar(3)==-1) then
               fpar(4) = fpar(1) * fpar(3) + fpar(2)
            endif
         endif
      else
         fpar(6) = fpar(5)
      endif
!
!     .. the test is struct this way so that when the value in fpar(6)
!       is not a valid number, STOPBIS is set to .true.
!
      if (fpar(6)>fpar(4)) then
         stopbis = .false.
         ipar(11) = 0
      else
         stopbis = .true.
         ipar(11) = 1
      endif
!
      return
      END FUNCTION stopbis
!-----end-of-stopbis
!-----------------------------------------------------------------------
      SUBROUTINE tidycg(n,ipar,fpar,sol,delx)
      integer(Ikind) :: i,n,ipar(16)
      real   (Rkind) :: fpar(16),sol(n),delx(n)
!-----------------------------------------------------------------------
!     Some common operations required before terminating the CG routines
!-----------------------------------------------------------------------
      real   (Rkind) :: zero
      parameter(zero=0.0e0_Rkind)
!
      if (ipar(12)/=0) then
         ipar(1) = ipar(12) 
      else if (ipar(1)>0) then
         if ((ipar(3)==999 .and. ipar(11)==1) .or.     &
             fpar(6)<=fpar(4)) then
            ipar(1) = 0
         else if (ipar(7)>=ipar(6) .and. ipar(6)>0) then
            ipar(1) = -1
         else
            ipar(1) = -10
         endif
      endif
      if (fpar(3)>zero .and. fpar(6)>zero .and.     &
          ipar(7)>ipar(13)) then
         fpar(7) = log10(fpar(3) / fpar(6)) / dble(ipar(7)-ipar(13))
      else
         fpar(7) = zero
      endif
      do i = 1, n
         sol(i) = sol(i) + delx(i)
      enddo
      return
      END SUBROUTINE tidycg
!-----end-of-tidycg
!-----------------------------------------------------------------------
      logical FUNCTION brkdn(alpha, ipar)
      integer(Ikind) :: ipar(16)
      real   (Rkind) :: alpha, beta, zero, one
      parameter (zero=0.0e0_Rkind, one=1.0e0_Rkind)
!-----------------------------------------------------------------------
!     test whether alpha is zero or an abnormal number, if yes,
!     this routine will return .true.
!
!     If alpha == 0, ipar(1) = -3,
!     if alpha is an abnormal number, ipar(1) = -9.
!-----------------------------------------------------------------------
      brkdn = .false.
      if (alpha>zero) then
         beta = one / alpha
         if (.not. beta>zero) then
            brkdn = .true.
            ipar(1) = -9
         endif
      else if (alpha<zero) then
         beta = one / alpha
         if (.not. beta<zero) then
            brkdn = .true.
            ipar(1) = -9
         endif
      else if (alpha==zero) then
         brkdn = .true.
         ipar(1) = -3
      else
         brkdn = .true.
         ipar(1) = -9
      endif
      return
      END FUNCTION brkdn
!-----end-of-brkdn
!-----------------------------------------------------------------------
      SUBROUTINE bisinit(ipar,fpar,wksize,dsc,lp,rp,wk)
      integer(Ikind) :: i,ipar(16),wksize,dsc
      logical        :: lp,rp
      real   (Rkind) :: fpar(16),wk(*)
!-----------------------------------------------------------------------
!     some common initializations for the iterative solvers
!-----------------------------------------------------------------------
      real   (Rkind) :: zero, one
      parameter(zero=0.0e0_Rkind, one=1.0e0_Rkind)
!
!     ipar(1) = -2 inidcate that there are not enough space in the work
!     array
!
      if (ipar(4)<wksize) then
         ipar(1) = -2
         ipar(4) = wksize
         return
      endif
!
      if (ipar(2)>2) then
         lp = .true.
         rp = .true.
      else if (ipar(2)==2) then
         lp = .false.
         rp = .true.
      else if (ipar(2)==1) then
         lp = .true.
         rp = .false.
      else
         lp = .false.
         rp = .false.
      endif
      if (ipar(3)==0) ipar(3) = dsc
!     .. clear the ipar elements used
      ipar(7) = 0
      ipar(8) = 0
      ipar(9) = 0
      ipar(10) = 0
      ipar(11) = 0
      ipar(12) = 0
      ipar(13) = 0
!
!     fpar(1) must be between (0, 1), fpar(2) must be positive,
!     fpar(1) and fpar(2) can NOT both be zero
!     Normally return ipar(1) = -4 to indicate any of above error
!
      if (fpar(1)<zero .or. fpar(1)>=one .or. fpar(2)<zero .or.  &
           (fpar(1)==zero .and. fpar(2)==zero)) then
         if (ipar(1)==0) then
            ipar(1) = -4
            return
         else
            fpar(1) = 1.0D-6
            fpar(2) = 1.0D-16
         endif
      endif
!     .. clear the fpar elements
      do i = 3, 10
         fpar(i) = zero
      enddo
      if (fpar(11)<zero) fpar(11) = zero
!     .. clear the used portion of the work array to zero
      do i = 1, wksize
         wk(i) = zero
      enddo
!
      return
!-----end-of-bisinit
      END SUBROUTINE bisinit
!-----------------------------------------------------------------------
      SUBROUTINE mgsro(full,lda,n,m,ind,ops,vec,hh,ierr)
      logical        :: full
      integer(Ikind) :: lda,m,n,ind,ierr
      real   (Rkind) :: ops,hh(m),vec(lda,m)
!-----------------------------------------------------------------------
!     MGSRO  -- Modified Gram-Schmidt procedure with Selective Re-
!               Orthogonalization
!     The ind'th vector of VEC is orthogonalized against the rest of
!     the vectors.
!
!     The test for performing re-orthogonalization is performed for
!     each indivadual vectors. If the cosine between the two vectors
!     is greater than 0.99 (REORTH = 0.99**2), re-orthogonalization is
!     performed. The norm of the 'new' vector is kept in variable NRM0,
!     and updated after operating with each vector.
!
!     full   -- .ture. if it is necessary to orthogonalize the ind'th
!               against all the vectors vec(:,1:ind-1), vec(:,ind+2:m)
!               .false. only orthogonalize againt vec(:,1:ind-1)
!     lda    -- the leading dimension of VEC
!     n      -- length of the vector in VEC
!     m      -- number of vectors can be stored in VEC
!     ind    -- index to the vector to be changed
!     ops    -- operation counts
!     vec    -- vector of LDA X M storing the vectors
!     hh     -- coefficient of the orthogonalization
!     ierr   -- error code
!               0 : successful return
!               -1: zero input vector
!               -2: input vector contains abnormal numbers
!               -3: input vector is a linear combination of others
!
!     External routines used: double precision distdot
!-----------------------------------------------------------------------
      integer(Ikind) :: i,k
      real   (Rkind) :: nrm0, nrm1, fct, thr, zero, one, reorth
      parameter (zero=0.0e0_Rkind, one=1.0e0_Rkind, reorth=0.98e0_Rkind)
      
     ! real(Rkind) :: distdot 
     ! external distdot
!
!     compute the norm of the input vector
!
      nrm0 = distdot(n,vec(1,ind),1_Ikind,vec(1,ind),1_Ikind)
      ops = ops + n + n
      thr = nrm0 * reorth
      if (nrm0<=zero) then
         ierr = - 1
         return
      else if (nrm0>zero .and. one/nrm0>zero) then
         ierr = 0
      else
         ierr = -2
         return
      endif
!
!     Modified Gram-Schmidt loop
!
      if (full) then
         do 40 i = ind+1, m
            fct = distdot(n,vec(1,ind),1_Ikind,vec(1,i),1_Ikind)
            hh(i) = fct
            do 20 k = 1, n
               vec(k,ind) = vec(k,ind) - fct * vec(k,i)
 20         continue
            ops = ops + 4 * n + 2
            if (fct*fct>thr) then
               fct = distdot(n,vec(1,ind),1_Ikind,vec(1,i),1_Ikind)
               hh(i) = hh(i) + fct
               do 30 k = 1, n
                  vec(k,ind) = vec(k,ind) - fct * vec(k,i)
 30            continue
               ops = ops + 4*n + 1
            endif
            nrm0 = nrm0 - hh(i) * hh(i)
            if (nrm0<zero) nrm0 = zero
            thr = nrm0 * reorth
 40      continue
      endif
!
      do 70 i = 1, ind-1
         fct = distdot(n,vec(1,ind),1_Ikind,vec(1,i),1_Ikind)
         hh(i) = fct
         do 50 k = 1, n
            vec(k,ind) = vec(k,ind) - fct * vec(k,i)
 50      continue
         ops = ops + 4 * n + 2
         if (fct*fct>thr) then
            fct = distdot(n,vec(1,ind),1_Ikind,vec(1,i),1_Ikind)
            hh(i) = hh(i) + fct
            do 60 k = 1, n
               vec(k,ind) = vec(k,ind) - fct * vec(k,i)
 60         continue
            ops = ops + 4*n + 1
         endif
         nrm0 = nrm0 - hh(i) * hh(i)
         if (nrm0<zero) nrm0 = zero
         thr = nrm0 * reorth
 70   continue
!
!     test the resulting vector
!
      nrm1 = sqrt(distdot(n,vec(1,ind),1_Ikind,vec(1,ind),1_Ikind))
      ops = ops + n + n
 75   hh(ind) = nrm1
      if (nrm1<=zero) then
         ierr = -3
         return
      endif
!
!     scale the resulting vector
!
      fct = one / nrm1
      do 80 k = 1, n
         vec(k,ind) = vec(k,ind) * fct
 80   continue
      ops = ops + n + 1
!
!     normal return
!
      ierr = 0
      return
      END SUBROUTINE mgsro
!----------------------------------------------------------------------c
!                          S P A R S K I T                             c
!----------------------------------------------------------------------c
!          BASIC MATRIX-VECTOR OPERATIONS - MATVEC MODULE              c
!         Matrix-vector Mulitiplications and Triang. Solves            c
!----------------------------------------------------------------------c
! contents: (as of Nov 18, 1991)                                       c
!----------                                                            c
! 1) Matrix-vector products:                                           c
!---------------------------                                           c
! amux  : A times a vector. Compressed Sparse Row (CSR) format.        c
! atmux : Transp(A) times a vector. CSR format.                        c
!----------------------------------------------------------------------c
      SUBROUTINE amux (n, x, y, a,ja,ia) 
      integer(Ikind) :: n, ja(*), ia(*)
      real   (Rkind) :: x(*), y(*), a(*)       
!-----------------------------------------------------------------------
!         A times a vector
!----------------------------------------------------------------------- 
! multiplies a matrix by a vector using the dot product form
! Matrix A is stored in compressed sparse row storage.
!
! on entry:
!----------
! n     = row dimension of A
! x     = real array of length equal to the column dimension of
!         the A matrix.
! a, ja,
!    ia = input matrix in compressed sparse row format.
!
! on return:
!-----------
! y     = real array of length n, containing the product y=Ax
!
!-----------------------------------------------------------------------
! local variables
!
      integer(Ikind) :: i, k
      real   (Rkind) :: t
!-----------------------------------------------------------------------
      do 100 i = 1,n
!
!     compute the inner product of row i with vector x
! 
         t = 0.0d0
         do 99 k=ia(i), ia(i+1)-1 
            t = t + a(k)*x(ja(k))
 99      continue
!
!     store result in y(i) 
!
         y(i) = t
 100  continue
!
      return
!---------end-of-amux---------------------------------------------------
!-----------------------------------------------------------------------
      END SUBROUTINE amux
!-----------------------------------------------------------------------
      SUBROUTINE atmux (n, x, y, a, ja, ia)
      integer(Ikind) :: n, ia(*), ja(*)
      real   (Rkind) :: x(*), y(*), a(*)       
!-----------------------------------------------------------------------
!         transp( A ) times a vector
!----------------------------------------------------------------------- 
! multiplies the transpose of a matrix by a vector when the original
! matrix is stored in compressed sparse row storage. Can also be
! viewed as the product of a matrix by a vector when the original
! matrix is stored in the compressed sparse column format.
!-----------------------------------------------------------------------
!
! on entry:
!----------
! n     = row dimension of A
! x     = real array of length equal to the column dimension of
!         the A matrix.
! a, ja,
!    ia = input matrix in compressed sparse row format.
!
! on return:
!-----------
! y     = real array of length n, containing the product y=transp(A)*x
!
!-----------------------------------------------------------------------
!     local variables 
!
      integer(Ikind) :: i, k 
!-----------------------------------------------------------------------
!
!     zero out output vector
! 
      do 1 i=1,n
         y(i) = 0.0
 1    continue
!
! loop over the rows
!
      do 100 i = 1,n
         do 99 k=ia(i), ia(i+1)-1 
            y(ja(k)) = y(ja(k)) + x(i)*a(k)
 99      continue
 100  continue
!
      return
!-------------end-of-atmux---------------------------------------------- 
!-----------------------------------------------------------------------
      END SUBROUTINE atmux
!----------------------------------------------------------------------- 
      SUBROUTINE rnrms   (nrow, nrm, a, ja, ia, diag)
      integer(Ikind) :: nrow, nrm
      integer(Ikind) :: ja(*), ia(nrow+1)
      real   (Rkind) :: a(*), diag(nrow)      
!-----------------------------------------------------------------------
! gets the norms of each row of A. (choice of three norms)
!-----------------------------------------------------------------------
! on entry:
! ---------
! nrow  = integer. The row dimension of A
!
! nrm   = integer. norm indicator. nrm = 1, means 1-norm, nrm =2
!                  means the 2-nrm, nrm = 0 means max norm
!
! a,
! ja,
! ia   = Matrix A in compressed sparse row format.
!
! on return:
!----------
!
! diag = real vector of length nrow containing the norms
!
!-----------------------------------------------------------------
      integer(Ikind) :: ii, k1, k2, k
      real   (Rkind) :: scal
      
      do 1 ii=1,nrow
!
!     compute the norm if each element.
!
         scal = 0.0_Rkind
         k1 = ia(ii)
         k2 = ia(ii+1)-1
         if (nrm == 0) then
            do 2 k=k1, k2
               scal = max(scal,abs(a(k) ) )
 2          continue
         elseif (nrm == 1) then
            do 3 k=k1, k2
               scal = scal + abs(a(k) )
 3          continue
         else
            do 4 k=k1, k2
               scal = scal + a(k)**2
 4          continue
         endif
         if (nrm == 2) scal = sqrt(scal)
         diag(ii) = scal
 1    continue
      return
!-----------------------------------------------------------------------
!-------------end-of-rnrms----------------------------------------------
      END SUBROUTINE rnrms
!-----------------------------------------------------------------------
      SUBROUTINE cnrms   (nrow, nrm, a, ja, ia, diag)
      integer(Ikind) :: nrow, nrm
      integer(Ikind) :: ja(*), ia(nrow+1)
      real   (Rkind) :: a(*), diag(nrow)      
!-----------------------------------------------------------------------
! gets the norms of each column of A. (choice of three norms)
!-----------------------------------------------------------------------
! on entry:
! ---------
! nrow  = integer. The row dimension of A
!
! nrm   = integer. norm indicator. nrm = 1, means 1-norm, nrm =2
!                  means the 2-nrm, nrm = 0 means max norm
!
! a,
! ja,
! ia   = Matrix A in compressed sparse row format.
!
! on return:
!----------
!
! diag = real vector of length nrow containing the norms
!
!-----------------------------------------------------------------
      integer(Ikind) :: k, ii, k1, k2, j
      
      do 10 k=1, nrow
         diag(k) = 0.0e0_Rkind
 10   continue
      do 1 ii=1,nrow
         k1 = ia(ii)
         k2 = ia(ii+1)-1
         do 2 k=k1, k2
            j = ja(k)
!     update the norm of each column
            if (nrm == 0) then
               diag(j) = max(diag(j),abs(a(k) ) )
            elseif (nrm == 1) then
               diag(j) = diag(j) + abs(a(k) )
            else
               diag(j) = diag(j)+a(k)**2
            endif
 2       continue
 1    continue
      if (nrm /= 2) return
      do 3 k=1, nrow
         diag(k) = sqrt(abs(diag(k)))
 3    continue
      return
!-----------------------------------------------------------------------
!------------end-of-cnrms-----------------------------------------------
      END SUBROUTINE cnrms
!-----------------------------------------------------------------------
      SUBROUTINE roscal(nrow,job,nrm,a,ja,ia,diag,b,jb,ib,ierr)
      integer(Ikind) :: nrow,job,nrm,ja(*),jb(*),ia(nrow+1),ib(nrow+1),ierr
      real   (Rkind) :: a(*), b(*), diag(nrow)      
!-----------------------------------------------------------------------
! scales the rows of A such that their norms are one on return
! 3 choices of norms: 1-norm, 2-norm, max-norm.
!-----------------------------------------------------------------------
! on entry:
! ---------
! nrow  = integer. The row dimension of A
!
! job   = integer. job indicator. Job=0 means get array b only
!         job = 1 means get b, and the integer arrays ib, jb.
!
! nrm   = integer. norm indicator. nrm = 1, means 1-norm, nrm =2
!                  means the 2-nrm, nrm = 0 means max norm
!
! a,
! ja,
! ia   = Matrix A in compressed sparse row format.
!
! on return:
!----------
!
! diag = diagonal matrix stored as a vector containing the matrix
!        by which the rows have been scaled, i.e., on return
!        we have B = Diag*A.
!
! b,
! jb,
! ib    = resulting matrix B in compressed sparse row format.
!
! ierr  = error message. ierr=0     : Normal return
!                        ierr=i > 0 : Row number i is a zero row.
! Notes:
!-------
! 1)        The column dimension of A is not needed.
! 2)        algorithm in place (B can take the place of A).
!-----------------------------------------------------------------
      integer(Ikind) :: j
      
      call rnrms (nrow,nrm,a,ja,ia,diag)
      ierr = 0
      do 1 j=1, nrow
         if (diag(j) == 0.0_Rkind) then
            ierr = j
            return
         else
            diag(j) = 1.0e0_Rkind/diag(j)
         endif
 1    continue
      call diamua(nrow,job,a,ja,ia,diag,b,jb,ib)
      return
!-------end-of-roscal---------------------------------------------------
!-----------------------------------------------------------------------
      END SUBROUTINE roscal
!-----------------------------------------------------------------------
      SUBROUTINE coscal(nrow,job,nrm,a,ja,ia,diag,b,jb,ib,ierr)
!-----------------------------------------------------------------------
      integer(Ikind) :: nrow,nrm,job,ja(*),jb(*),ia(nrow+1),ib(nrow+1),ierr
      real   (Rkind) :: a(*),b(*),diag(nrow)
!-----------------------------------------------------------------------
! scales the columns of A such that their norms are one on return
! result matrix written on b, or overwritten on A.
! 3 choices of norms: 1-norm, 2-norm, max-norm. in place.
!-----------------------------------------------------------------------
! on entry:
! ---------
! nrow  = integer. The row dimension of A
!
! job   = integer. job indicator. Job=0 means get array b only
!         job = 1 means get b, and the integer arrays ib, jb.
!
! nrm   = integer. norm indicator. nrm = 1, means 1-norm, nrm =2
!                  means the 2-nrm, nrm = 0 means max norm
!
! a,
! ja,
! ia   = Matrix A in compressed sparse row format.
!
! on return:
!----------
!
! diag = diagonal matrix stored as a vector containing the matrix
!        by which the columns have been scaled, i.e., on return
!        we have B = A * Diag
!
! b,
! jb,
! ib    = resulting matrix B in compressed sparse row sparse format.
!
! ierr  = error message. ierr=0     : Normal return
!                        ierr=i > 0 : Column number i is a zero row.
! Notes:
!-------
! 1)     The column dimension of A is not needed.
! 2)     algorithm in place (B can take the place of A).
!-----------------------------------------------------------------
      integer(Ikind) :: j
      
      call cnrms (nrow,nrm,a,ja,ia,diag)
      ierr = 0
      do 1 j=1, nrow
         if (diag(j) == 0.0e0_Rkind) then
            ierr = j
            return
         else
            diag(j) = 1.0e0_Rkind/diag(j)
         endif
 1    continue
      call amudia (nrow,job,a,ja,ia,diag,b,jb,ib)
      return
!--------end-of-coscal--------------------------------------------------
!-----------------------------------------------------------------------
      END SUBROUTINE coscal
!-----------------------------------------------------------------------
      SUBROUTINE diamua (nrow,job, a, ja, ia, diag, b, jb, ib)
      integer(Ikind) :: nrow, job
      integer(Ikind) :: ja(*), jb(*), ia(nrow+1),ib(nrow+1)
      real   (Rkind) :: a(*), b(*), diag(nrow)      
!-----------------------------------------------------------------------
! performs the matrix by matrix product B = Diag * A  (in place)
!-----------------------------------------------------------------------
! on entry:
! ---------
! nrow  = integer. The row dimension of A
!
! job   = integer. job indicator. Job=0 means get array b only
!         job = 1 means get b, and the integer arrays ib, jb.
!
! a,
! ja,
! ia   = Matrix A in compressed sparse row format.
!
! diag = diagonal matrix stored as a vector dig(1:n)
!
! on return:
!----------
!
! b,
! jb,
! ib    = resulting matrix B in compressed sparse row sparse format.
!
! Notes:
!-------
! 1)        The column dimension of A is not needed.
! 2)        algorithm in place (B can take the place of A).
!           in this case use job=0.
!-----------------------------------------------------------------
      integer(Ikind) :: ii, k1, k2, k
      real   (Rkind) :: scal
      
      do 1 ii=1,nrow
!
!     normalize each row
!
         k1 = ia(ii)
         k2 = ia(ii+1)-1
         scal = diag(ii)
         do 2 k=k1, k2
            b(k) = a(k)*scal
 2       continue
 1    continue
!
      if (job == 0) return
!
      do 3 ii=1, nrow+1
         ib(ii) = ia(ii)
 3    continue
      do 31 k=ia(1), ia(nrow+1) -1
         jb(k) = ja(k)
 31   continue
      return
!----------end-of-diamua------------------------------------------------
!-----------------------------------------------------------------------
      END SUBROUTINE diamua
!-----------------------------------------------------------------------
      SUBROUTINE amudia (nrow,job, a, ja, ia, diag, b, jb, ib)
      integer(Ikind) :: nrow, job
      integer(Ikind) :: ja(*),jb(*), ia(nrow+1),ib(nrow+1)
      real   (Rkind) :: a(*), b(*), diag(nrow)      
!-----------------------------------------------------------------------
! performs the matrix by matrix product B = A * Diag  (in place)
!-----------------------------------------------------------------------
! on entry:
! ---------
! nrow  = integer. The row dimension of A
!
! job   = integer. job indicator. Job=0 means get array b only
!         job = 1 means get b, and the integer arrays ib, jb.
!
! a,
! ja,
! ia   = Matrix A in compressed sparse row format.
!
! diag = diagonal matrix stored as a vector dig(1:n)
!
! on return:
!----------
!
! b,
! jb,
! ib    = resulting matrix B in compressed sparse row sparse format.
!
! Notes:
!-------
! 1)        The column dimension of A is not needed.
! 2)        algorithm in place (B can take the place of A).
!-----------------------------------------------------------------
      integer(Ikind) :: ii, k1, k2, k
      
      do 1 ii=1,nrow
!
!     scale each element
!
         k1 = ia(ii)
         k2 = ia(ii+1)-1
         do 2 k=k1, k2
            b(k) = a(k)*diag(ja(k))
 2       continue
 1    continue
!
      if (job == 0) return
!
      do 3 ii=1, nrow+1
         ib(ii) = ia(ii)
 3    continue
      do 31 k=ia(1), ia(nrow+1) -1
         jb(k) = ja(k)
 31   continue
      return
!-----------------------------------------------------------------------
!-----------end-of-amudiag----------------------------------------------
      END SUBROUTINE amudia
!-----------------------------------------------------------------------

      SUBROUTINE  dcopy(n,dx,incx,dy,incy)
!
!     copies a vector, x, to a vector, y.
!     uses unrolled loops for increments equal to one.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,m,mp1,n
      !real   (Rkind) :: dx(1),dy(1)
      real   (Rkind) :: dx(*),dy(*)
!
      if(n<=0)return
      if (incx==1 .and. incy==1) go to 20
!
!        code for unequal increments or equal increments
!          not equal to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dy(iy) = dx(ix)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
!        code for both increments equal to 1
!
!
!        clean-up loop
!
   20 m = mod(n,7_Ikind)
      if( m == 0 ) go to 40
      do 30 i = 1,m
        dy(i) = dx(i)
   30 continue
      if( n < 7 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,7
        dy(i) = dx(i)
        dy(i + 1) = dx(i + 1)
        dy(i + 2) = dx(i + 2)
        dy(i + 3) = dx(i + 3)
        dy(i + 4) = dx(i + 4)
        dy(i + 5) = dx(i + 5)
        dy(i + 6) = dx(i + 6)
   50 continue
      return
      END SUBROUTINE dcopy

      real(Rkind) FUNCTION ddot(n,dx,incx,dy,incy)
!
!     forms the dot product of two vectors.
!     uses unrolled loops for increments equal to one.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,m,mp1,n
     ! real   (Rkind) :: dx(1),dy(1),dtemp      
      real   (Rkind) :: dx(*),dy(*),dtemp      
!
      ddot = 0.0e0_Rkind
      dtemp = 0.0e0_Rkind
      if(n<=0)return
      if(incx==1.and.incy==1)go to 20
!
!        code for unequal increments or equal increments
!          not equal to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dtemp = dtemp + dx(ix)*dy(iy)
        ix = ix + incx
        iy = iy + incy
   10 continue
      ddot = dtemp
      return
!
!        code for both increments equal to 1
!
!
!        clean-up loop
!
   20 m = mod(n,5_Ikind)
      if( m == 0 ) go to 40
      do 30 i = 1,m
        dtemp = dtemp + dx(i)*dy(i)
   30 continue
      if( n < 5 ) go to 60
   40 mp1 = m + 1
      do 50 i = mp1,n,5
        dtemp = dtemp + dx(i)*dy(i) + dx(i + 1)*dy(i + 1) +              &
         dx(i + 2)*dy(i + 2) + dx(i + 3)*dy(i + 3) + dx(i + 4)*dy(i + 4)
   50 continue
   60 ddot = dtemp
      return
      END FUNCTION ddot
!
      real(Rkind)  FUNCTION dasum(n,dx,incx)
!
!     takes the sum of the absolute values.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,m,mp1,n,nincx
     ! real   (Rkind) :: dx(1),dtemp
      real   (Rkind) :: dx(*),dtemp
!
      dasum = 0.0e0_Rkind
      dtemp = 0.0e0_Rkind
      if(n<=0)return
      if(incx==1)go to 20
!
!        code for increment not equal to 1
!
      nincx = n*incx
      do 10 i = 1,nincx,incx
        dtemp = dtemp + abs(dx(i))
   10 continue
      dasum = dtemp
      return
!
!        code for increment equal to 1
!
!
!        clean-up loop
!
   20 m = mod(n,6_Ikind)
      if( m == 0 ) go to 40
      do 30 i = 1,m
        dtemp = dtemp + abs(dx(i))
   30 continue
      if( n < 6 ) go to 60
   40 mp1 = m + 1
      do 50 i = mp1,n,6
        dtemp = dtemp + abs(dx(i)) + abs(dx(i + 1)) + abs(dx(i + 2))   &
        + abs(dx(i + 3)) + abs(dx(i + 4)) + abs(dx(i + 5))
   50 continue
   60 dasum = dtemp
      return
      END FUNCTION dasum

      SUBROUTINE daxpy(n,da,dx,incx,dy,incy)
!
!     constant times a vector plus a vector.
!     uses unrolled loops for increments equal to one.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,m,mp1,n
     ! real   (Rkind) :: dx(1),dy(1),da
      real   (Rkind) :: dx(*),dy(*),da
!
      if(n<=0)return
      if (da == 0.0e0_Rkind) return
      if(incx==1.and.incy==1)go to 20
!
!        code for unequal increments or equal increments
!          not equal to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dy(iy) = dy(iy) + da*dx(ix)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
!        code for both increments equal to 1
!
!
!        clean-up loop
!
   20 m = mod(n,4_Ikind)
      if( m == 0 ) go to 40
      do 30 i = 1,m
        dy(i) = dy(i) + da*dx(i)
   30 continue
      if( n < 4 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,4
        dy(i) = dy(i) + da*dx(i)
        dy(i + 1) = dy(i + 1) + da*dx(i + 1)
        dy(i + 2) = dy(i + 2) + da*dx(i + 2)
        dy(i + 3) = dy(i + 3) + da*dx(i + 3)
   50 continue
      return
      END SUBROUTINE daxpy
      
      real (Rkind) FUNCTION dnrm2 ( n, dx, incx)
      integer(Ikind) :: n, incx
      integer        :: next, nn, i, j
     ! real   (Rkind) :: dx(1), cutlo, cuthi, hitest, sum, xmax,zero,one
      real   (Rkind) :: dx(*), cutlo, cuthi, hitest, sum, xmax,zero,one
      data   zero, one /0.0e0_Rkind, 1.0e0_Rkind/
!
!     euclidean norm of the n-vector stored in dx() with storage
!     increment incx .
!     if    n <= 0 return with result = 0.
!     if n >= 1 then incx must be >= 1
!
!           c.l.lawson, 1978 jan 08
!
!     four phase method     using two built-in constants that are
!     hopefully applicable to all machines.
!         cutlo = maximum of  dsqrt(u/eps)  over all known machines.
!         cuthi = minimum of  dsqrt(v)      over all known machines.
!     where
!         eps = smallest no. such that eps + 1. > 1.
!         u   = smallest positive no.   (underflow limit)
!         v   = largest  no.            (overflow  limit)
!
!     brief outline of algorithm..
!
!     phase 1    scans zero components.
!     move to phase 2 when a component is nonzero and <= cutlo
!     move to phase 3 when a component is > cutlo
!     move to phase 4 when a component is >= cuthi/m
!     where m = n for x() real and m = 2*n for complex.
!
!     values for cutlo and cuthi..
!     from the environmental parameters listed in the imsl converter
!     document the limiting values are as follows..
!     cutlo, s.p.   u/eps = 2**(-102) for  honeywell.  close seconds are
!                   univac and dec at 2**(-103)
!                   thus cutlo = 2**(-51) = 4.44089e-16
!     cuthi, s.p.   v = 2**127 for univac, honeywell, and dec.
!                   thus cuthi = 2**(63.5) = 1.30438e19
!     cutlo, d.p.   u/eps = 2**(-67) for honeywell and dec.
!                   thus cutlo = 2**(-33.5) = 8.23181d-11
!     cuthi, d.p.   same as s.p.  cuthi = 1.30438d19
!     data cutlo, cuthi / 8.232d-11,  1.304d19 /
!     data cutlo, cuthi / 4.441e-16,  1.304e19 /
      data cutlo, cuthi / 8.232e-11_Rkind,  1.304e19_Rkind /
!
      if(n > 0) go to 10
         dnrm2  = zero
         go to 300
!
   10 assign 30 to next
      sum = zero
      nn = n * incx
!                                                 begin main loop
      i = 1
   20    go to next,(30, 50, 70, 110)
   30 if( abs(dx(i)) > cutlo) go to 85
      assign 50 to next
      xmax = zero
!
!                        phase 1.  sum is zero
!
   50 if( dx(i) == zero) go to 200
      if( abs(dx(i)) > cutlo) go to 85
!
!                                prepare for phase 2.
      assign 70 to next
      go to 105
!
!                                prepare for phase 4.
!
  100 i = j
      assign 110 to next
      sum = (sum / dx(i)) / dx(i)
  105 xmax = abs(dx(i))
      go to 115
!
!                   phase 2.  sum is small.
!                             scale to avoid destructive underflow.
!
   70 if( abs(dx(i)) > cutlo ) go to 75
!
!                     common code for phases 2 and 4.
!                     in phase 4 sum is large.  scale to avoid overflow.
!
  110 if( abs(dx(i)) <= xmax ) go to 115
         sum = one + sum * (xmax / dx(i))**2
         xmax = abs(dx(i))
         go to 200
!
  115 sum = sum + (dx(i)/xmax)**2
      go to 200
!
!
!                  prepare for phase 3.
!
   75 sum = (sum * xmax) * xmax
!
!
!     for real or d.p. set hitest = cuthi/n
!     for complex      set hitest = cuthi/(2*n)
!
!   85 hitest = cuthi/float( n )
   85 hitest = cuthi/real( n,kind=Rkind )
!
!                   phase 3.  sum is mid-range.  no scaling.
!
      do 95 j =i,nn,incx
         if(abs(dx(j)) >= hitest) go to 100
         sum = sum + dx(j)**2
   95 continue
      dnrm2 = sqrt( sum )
      go to 300
!
  200 continue
      i = i + incx
      if ( i <= nn ) go to 20
!
!              end of main loop.
!
!              compute square root and adjust for scaling.
!
      dnrm2 = xmax * sqrt(sum)
  300 continue
      return
      END FUNCTION dnrm2

      SUBROUTINE  dscal(n,da,dx,incx)
!     scales a vector by a constant.
!     uses unrolled loops for increment equal to one.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,m,mp1,n,nincx
    !  real   (Rkind) :: da,dx(1)
      real   (Rkind) :: da,dx(*)
!
      if(n<=0)return
      if(incx==1)go to 20
!
!        code for increment not equal to 1
!
      nincx = n*incx
      do 10 i = 1,nincx,incx
        dx(i) = da*dx(i)
   10 continue
      return
!
!        code for increment equal to 1
!
!
!        clean-up loop
!
   20 m = mod(n,5_Ikind)
      if( m == 0 ) go to 40
      do 30 i = 1,m
        dx(i) = da*dx(i)
   30 continue
      if( n < 5 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,5
        dx(i) = da*dx(i)
        dx(i + 1) = da*dx(i + 1)
        dx(i + 2) = da*dx(i + 2)
        dx(i + 3) = da*dx(i + 3)
        dx(i + 4) = da*dx(i + 4)
   50 continue
      return
      END SUBROUTINE dscal

      SUBROUTINE  dswap (n,dx,incx,dy,incy)
!
!     interchanges two vectors.
!     uses unrolled loops for increments equal one.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,m,mp1,n
     ! real   (Rkind) :: dx(1),dy(1),dtemp
      real   (Rkind) :: dx(*),dy(*),dtemp
!
      if(n<=0)return
      if(incx==1.and.incy==1)go to 20
!
!       code for unequal increments or equal increments not equal
!         to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dtemp = dx(ix)
        dx(ix) = dy(iy)
        dy(iy) = dtemp
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
!       code for both increments equal to 1
!
!
!       clean-up loop
!
   20 m = mod(n,3_Ikind)
      if( m == 0 ) go to 40
      do 30 i = 1,m
        dtemp = dx(i)
        dx(i) = dy(i)
        dy(i) = dtemp
   30 continue
      if( n < 3 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,3
        dtemp = dx(i)
        dx(i) = dy(i)
        dy(i) = dtemp
        dtemp = dx(i + 1)
        dx(i + 1) = dy(i + 1)
        dy(i + 1) = dtemp
        dtemp = dx(i + 2)
        dx(i + 2) = dy(i + 2)
        dy(i + 2) = dtemp
   50 continue
      return
      END SUBROUTINE dswap

      integer(Ikind) FUNCTION idamax(n,dx,incx)
!
!     finds the index of element having max. absolute value.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,ix,n
     ! real   (Rkind) :: dx(1),dmax
      real   (Rkind) :: dx(*),dmax
!
      idamax = 0
      if( n < 1 ) return
      idamax = 1
      if(n==1)return
      if(incx==1)go to 20
!
!        code for increment not equal to 1
!
      ix = 1
      dmax = abs(dx(1))
      ix = ix + incx
      do 10 i = 2,n
         if(abs(dx(ix))<=dmax) go to 5
         idamax = i
         dmax = abs(dx(ix))
    5    ix = ix + incx
   10 continue
      return
!
!        code for increment equal to 1
!
   20 dmax = abs(dx(1))
      do 30 i = 2,n
         if(abs(dx(i))<=dmax) go to 30
         idamax = i
         dmax = abs(dx(i))
   30 continue
      return
      END FUNCTION idamax
!
      SUBROUTINE  drot (n,dx,incx,dy,incy,c,s)
!
!     applies a plane rotation.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,n
     ! real   (Rkind) :: dx(1),dy(1),dtemp,c,s
      real   (Rkind) :: dx(*),dy(*),dtemp,c,s
!
      if(n<=0)return
      if(incx==1.and.incy==1)go to 20
!
!       code for unequal increments or equal increments not equal
!         to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dtemp = c*dx(ix) + s*dy(iy)
        dy(iy) = c*dy(iy) - s*dx(ix)
        dx(ix) = dtemp
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
!       code for both increments equal to 1
!
   20 do 30 i = 1,n
        dtemp = c*dx(i) + s*dy(i)
        dy(i) = c*dy(i) - s*dx(i)
        dx(i) = dtemp
   30 continue
      return
      END SUBROUTINE drot
!
      SUBROUTINE drotg(da,db,c,s)
!
!     construct givens plane rotation.
!     jack dongarra, linpack, 3/11/78.
!
      real   (Rkind) :: da,db,c,s,roe,scale,r,z
!
      roe = db
      if( abs(da) > abs(db) ) roe = da
      scale = abs(da) + abs(db)
      if( scale /= 0.0d0 ) go to 10
         c = 1.0e0_Rkind
         s = 0.0e0_Rkind
         r = 0.0e0_Rkind
         go to 20
   10 r = scale*sqrt((da/scale)**2 + (db/scale)**2)
      r = sign(1.0e0_Rkind,roe)*r
      c = da/r
      s = db/r
   20 z = 1.0d0
      if( abs(da) > abs(db) ) z = s
      if( abs(db) >= abs(da) .and. c /= 0.0d0 ) z = 1.0d0/c
      da = r
      db = z
      return
      END SUBROUTINE drotg
!
      SUBROUTINE  ccopy(n,cx,incx,cy,incy)
!
!     copies a vector, x, to a vector, y.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,n
     ! complex :: cx(1),cy(1)
      complex :: cx(*),cy(*)
!
      if(n<=0)return
      if(incx==1.and.incy==1)go to 20
!
!        code for unequal increments or equal increments
!          not equal to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        cy(iy) = cx(ix)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
!        code for both increments equal to 1
!
   20 do 30 i = 1,n
        cy(i) = cx(i)
   30 continue
      return
      END SUBROUTINE ccopy
      
      SUBROUTINE  cscal(n,ca,cx,incx)
!
!     scales a vector by a constant.
!     jack dongarra, linpack,  3/11/78.
!
      integer(Ikind) :: i,incx,n,nincx
     ! complex        :: ca,cx(1)
      complex        :: ca,cx(*)
!
      if(n<=0)return
      if(incx==1)go to 20
!
!        code for increment not equal to 1
!
      nincx = n*incx
      do 10 i = 1,nincx,incx
        cx(i) = ca*cx(i)
   10 continue
      return
!
!        code for increment equal to 1
!
   20 do 30 i = 1,n
        cx(i) = ca*cx(i)
   30 continue
      return
      END SUBROUTINE cscal
!
      SUBROUTINE  csrot (n,cx,incx,cy,incy,c,s)
!
!     applies a plane rotation, where the cos and sin (c and s) are real
!     and the vectors cx and cy are complex.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,n
     ! complex        :: cx(1),cy(1),ctemp
      complex        :: cx(*),cy(*),ctemp
      real   (Rkind) :: c,s
!
      if(n<=0)return
      if(incx==1.and.incy==1)go to 20
!
!       code for unequal increments or equal increments not equal
!         to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        ctemp = c*cx(ix) + s*cy(iy)
        cy(iy) = c*cy(iy) - s*cx(ix)
        cx(ix) = ctemp
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
!       code for both increments equal to 1
!
   20 do 30 i = 1,n
        ctemp = c*cx(i) + s*cy(i)
        cy(i) = c*cy(i) - s*cx(i)
        cx(i) = ctemp
   30 continue
      return
      END SUBROUTINE csrot
      
      SUBROUTINE  cswap (n,cx,incx,cy,incy)
!
!     interchanges two vectors.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,incy,ix,iy,n
     ! complex       :: cx(1),cy(1),ctemp
      complex       :: cx(*),cy(*),ctemp
!
      if(n<=0)return
      if(incx==1.and.incy==1)go to 20
!
!       code for unequal increments or equal increments not equal
!         to 1
!
      ix = 1
      iy = 1
      if(incx<0)ix = (-n+1)*incx + 1
      if(incy<0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        ctemp = cx(ix)
        cx(ix) = cy(iy)
        cy(iy) = ctemp
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
!       code for both increments equal to 1
   20 do 30 i = 1,n
        ctemp = cx(i)


        cx(i) = cy(i)
        cy(i) = ctemp
   30 continue
      return
      END SUBROUTINE cswap
      
      SUBROUTINE  csscal(n,sa,cx,incx)
!
!     scales a complex vector by a real constant.
!     jack dongarra, linpack, 3/11/78.
!
      integer(Ikind) :: i,incx,n,nincx
     ! complex        :: cx(1)
      complex        :: cx(*)
      real   (Rkind) :: sa
!
      if(n<=0)return
      if(incx==1)go to 20
!
!        code for increment not equal to 1
!
      nincx = n*incx
      do 10 i = 1,nincx,incx
        cx(i) = cmplx(sa*real(cx(i)),sa*aimag(cx(i)))
   10 continue
      return
!
!        code for increment equal to 1
!
   20 do 30 i = 1,n
        cx(i) = cmplx(sa*real(cx(i)),sa*aimag(cx(i)))
   30 continue
      return
      END SUBROUTINE csscal
      
!------------------------------------------------------------------------------
    SUBROUTINE sparskit_csort ( n, a, ja, ia, iwork, values )
!------------------------------------------------------------------------------
    integer(Ikind) :: n
    integer(Ikind) :: ia(n+1), iwork(*), ja(*)
    real   (Rkind) :: a(*)
    logical        :: values
!
!-- CSORT sorts the elements of a CSR matrix.
!
!   Discussion:
!
!    This routine sorts the elements of a CSR matrix (stored in Compressed
!    Sparse Row Format) in increasing order of their column indices within
!    each row. It uses a form of bucket sort with a cost of O(nnz) where
!    nnz = number of nonzero elements.
!
!    Requires an integer work array of size length 2*nnz.
!
!   Modified:
!
!    07 January 2004
!
!   Author:
!
!    Youcef Saad
!
!   Parameters:
!
!    Input, integer N, the row dimension of the matrix.
!
!    Input, real A(*), integer JA(*), IA(N+1), the matrix in CSR
!    Compressed Sparse Row format.
!
!  iwork = integer work array of length max ( n+1, 2*nnz )
!         where nnz = 2* (ia(n+1)-ia(1))  ) .
!
!  values= logical indicating whether or not the real values a(*) must
!          also be permuted. if (.not. values) then the array a is not
!          touched by csort and can be a dummy array.
!
!  on return:
!
!  the matrix stored in the structure a, ja, ia is permuted in such a
!  way that the column indices are in increasing order within each row.
!  iwork(1:nnz) contains the permutation used  to rearrange the elements.
!

  integer(Ikind) :: i, j, k, ko, next, nnz, irow, ifirst
!
!  Count the number of elements in each column.
!
  iwork(1:n+1) = 0

  do i = 1, n
     do k = ia(i), ia(i+1)-1
        j = ja(k) + 1
        iwork(j) = iwork(j) + 1
     end do
  end do
!
!  Compute pointers from lengths.
!
  iwork(1) = 1

  do i = 1, n
     iwork(i+1) = iwork(i) + iwork(i+1)
  end do
!
!  Get the positions of the nonzero elements in order of columns.
!
  ifirst = ia(1)
  nnz = ia(n+1)-ifirst

  do i = 1, n
    do k = ia(i), ia(i+1)-1
      j = ja(k)
      next = iwork(j)
      iwork(nnz+next) = k
      iwork(j) = next + 1
    end do
  end do
!
!  Convert to coordinate format.
!
  do i = 1, n
    do k = ia(i), ia(i+1)-1
      iwork(k) = i
    end do
  end do
!
!  Loop to find permutation: for each element find the correct
!  position in (sorted) arrays A, JA.  Record this in IWORK.
!
  do k = 1, nnz
     ko = iwork(nnz+k)
     irow = iwork(ko)
     next = ia(irow)
!
!  The current element should go in next position in row. IWORK
!  records this position.
!
     iwork(ko) = next
     ia(irow) = next + 1
  end do
!
!  Perform an in-place permutation of the arrays.
!
     call ivperm ( nnz, ja(ifirst), iwork )

     if ( values ) call dvperm ( nnz, a(ifirst), iwork )
!
!  Reshift the pointers of the original matrix back.
!
  do i = n, 1, -1
    ia(i+1) = ia(i)
  end do

  ia(1) = ifirst

  END SUBROUTINE sparskit_csort
  
!------------------------------------------------------------------------------
   SUBROUTINE ivperm ( n, ix, perm )
!------------------------------------------------------------------------------
   integer(Ikind) :: n
   integer(Ikind) :: ix(n), perm(n)
!
!! IVPERM performs an in-place permutation of an integer vector.
!
!  Discussion:
!
!    The integer vector ix is permuted according to the permutation 
!    array perm(*), i.e., on return, the vector x satisfies,
!
!      ix(perm(j)) :== ix(j), j = 1,2,.., n
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer N, the length of the vector.
!
!    Input/output, integer IX(N), the vector to be permuted.
!
!    Input, integer PERM(N), the permutation.
!

  integer(Ikind) :: ii, init, k, next, tmp, tmp1

  init = 1
  tmp = ix(init)
  ii = perm(init)
  perm(init)= -perm(init)
  k = 0
!
!  Loop.
!
 6    continue

  k = k + 1
!
!  Save the chased element.
!
  tmp1 = ix(ii)
  ix(ii) = tmp
  next = perm(ii)

  if ( next < 0 ) then
    go to 65
  end if
!
!  Test for end.
!
  if ( n < k ) then
    perm(1:n) = -perm(1:n)
    return
  end if

  tmp = tmp1
  perm(ii) = -perm(ii)
  ii = next
!
!  End of loop.
!
  go to 6
!
!  Reinitilaize cycle.
!
 65   continue

  init = init + 1

  if ( n < init ) then
    perm(1:n) = -perm(1:n)
    return
  end if

  if ( perm(init) < 0 ) then
    go to 65
  end if

  tmp = ix(init)
  ii = perm(init)
  perm(init)=-perm(init)
  go to 6

  END SUBROUTINE ivperm  

!------------------------------------------------------------------------------
  SUBROUTINE dvperm ( n, x, perm )
!------------------------------------------------------------------------------
  integer(Ikind) :: n
  integer(Ikind) :: perm(n)
  real   (Rkind) :: x(n)
!
!! DVPERM performs an in-place permutation of a real vector.
!
!  Discussion:
!
!    This routine permutes a real vector X using a permutation PERM.
!
!    On return, the vector X satisfies,
!
!      x(perm(j)) :== x(j), j = 1,2,.., n
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer N, the length of X.
!
!    Input/output, real X(N), the vector to be permuted.
!
!    Input, integer PERM(N), the permutation.
!
 

  integer(Ikind) :: ii, init, k, next
  real   (Rkind) :: tmp, tmp1

  init = 1
  tmp = x(init)
  ii = perm(init)
  perm(init)= -perm(init)
  k = 0
!
!  The main loop.
!
 6  continue

   k = k + 1
!
!  Save the chased element.
!
  tmp1 = x(ii)
  x(ii) = tmp
  next = perm(ii)

  if ( next < 0 ) then
    go to 65
  end if
!
!  Test for end.
!
  if ( n < k ) then
    perm(1:n) = -perm(1:n)
    return
  end if

  tmp = tmp1
  perm(ii) = -perm(ii)
  ii = next
!
!  End of the loop.
!
  go to 6
!
!  Reinitialize cycle.
!
 65   continue

  init = init + 1

  if ( n < init ) then 
    perm(1:n) = -perm(1:n)
    return
  end if

  if ( perm(init) < 0 ) then
    go to 65
  end if

  tmp = x(init)
  ii = perm(init)
  perm(init) = -perm(init)
  go to 6

  END SUBROUTINE dvperm  
  
END MODULE sparskit_m

