#include "error.fpp"

MODULE models_m
!--------------------------------------------------------------------------------------------- 
!  This module includes the set of routines that solve some physical models
!
!  If you want to implement a new model you are in the right place! In this purpose the
!  approach is as follows:
!
! 1- choose a name for your model (say "mymodel") and add it in the "select case" block in
!    the driver (subroutine models_driver below).
!
! 2- implement a subroutine that computes the element stiffness matrix, the element load
!    vector and the secondary unknowns for your problem. Name it as something like
!    "models_KFmymodel" (see some models below).
!
! 3- implement a subroutine "models_mymodel" taking example of existing ones. In this
!    routine, define the parameters
!       "ndime" (space dimension), 
!       "ndofn" (number of d.o.f),
!       "nprop" (number of material properties),
!       "nstri" (number of independent components of secondary unknowns).
!    The values of these parameters will be first used during the input phase (a call to the
!    driver is done during the read of data to fetch these parameters). Add also in the array
!    of strings "info" some useful messages that will be displayed to the user if something
!    goes wrong with the data. In the convention I used, these parameters are initialized
!    when "info" (optional argument) is present.
!
!    If your problem is a standard time-independent model, a simple call to "femsystem_statio" 
!    could be sufficient to form your f.e. system (matrix and rhs). The calling instance is:
!
!            !            Mat      Rhs      Stress    Prod.
!            request = [ .true. , .true. , .false. , .false. ]
!      
!            call femsystem_statio ( mymesh, mydata, femsys, request,  &
!                                    KFelem  = models_KFmymodel     ,  &
!                                    Neumann = boundarycond_Neumann?)
!
!    In case of Neumann bc, check if one of the routines in "boundarycond" module fits 
!    with your model (Neumann is an optional argument of femsystem_statio), if no, implement
!    a new one.
!
!    Then call the linear solver.
!
!    If needed, a new call to "femsystem_statio" with 
!
!            request = [ .false. , .fase. , .true. , .false. ]
!
!    allows you to compute the secondary unknowns (stresses for example):
!
!            call femsystem_statio ( mymesh, mydata, femsys, request,  &
!                                    KFelem  = models_KFmymodel     )
!
!    Finally a call to "output_driver" will prints the results in one of the implemented
!    format and choosen by the user.
!
!  For non-linear problem, take a look to the simple example "models_torsion" (torsion of an
!  elasto-plastic beam), model that corresponds to a minimization problem with inequality 
!  constraints).
!-----------------------------------------------------------------------------------R.H. 12/16  
   
   use defTypes_m
   use err_m
   
   use util_m     , only: util_wtime
   use param_m    , only: timer
   use constants_m, only: IZERO, UERROR 
   
   use modelsDiffusion_m
   use modelsElasticity_m
   use modelsTorsion_m
   use modelsStokes_m
   use modelsNeoHookean_m  

   implicit none
       
   private
   public :: models_driver
    
CONTAINS

!=============================================================================================    
   SUBROUTINE models_driver ( mymesh, mydata, femsys, stat, info )
!============================================================================================= 
   use param_m, only : name_model_pb
   type     (mesh_t  ), intent(in out)           :: mymesh
   type     (udata_t ), intent(in out)           :: mydata 
   type     (linsys_t), intent(in out)           :: femsys 
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in out), optional :: info(*)
!--------------------------------------------------------------------------------------------- 
!  Calls the routine that solves the chosen problem (if info not present) 
!
!  Inputs
!
!      name_model_pb: string giving the name of the model (in lowercase and without spaces)
!                    
!      mymesh       : mesh structure
!
!      mydata       : data (material properties & boundary conditions) structure
!
!      femsys       : matrix structures
!
!      info         : array of strings, optional
!                     Through this option the model can be interogated to give some informations
!                     to the calling procedure about the required properties, number of d.o.f,
!                     space dimensions,... (useful for the input data phase)
!
!                     If present     --> informations are required
!
!                     If not present --> solution phase       
!-----------------------------------------------------------------------------------R.H. 11/16  

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: Here = 'models_driver'
   real     (Rkind)            :: t0
   integer                     :: lp
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 
   
   t0 = util_wtime()
       
   lp = index(name_model_pb,'(')
    
   select case ( name_model_pb(1:lp-1) )

      case ( 'diffusion' )
         call modelsDiffusion_main ( mymesh, mydata, femsys, stat, info )
    	
      case ( 'elasticity' )
         call modelsElasticity_main ( mymesh, mydata, femsys, stat, info )
             
      case ( 'stokes' )
        call modelsStokes_main ( mymesh, mydata, femsys, stat, info )
    	
      case ( 'demo1' )
           call modelsTorsion_main ( mymesh, mydata, femsys, stat, info )

      case ( 'stokesnl' )
           call modelsStokes_mainNL ( mymesh, mydata, femsys, stat, info )

      case ( 'stokesnl2' )
           call modelsStokes_mainNL2 ( mymesh, mydata, femsys, stat, info )

      case ( 'neohookean' )
           call modelsNeoHookean_main ( mymesh, mydata, femsys, stat, info )
                          	    
      case default
         stat = err_t ( stat = UERROR, where = Here, msg = 'model "' // &
                         trim(name_model_pb)//'" not yet implemented' )
         return
  
   end select
   
   error_TraceNreturn(stat>IZERO, Here, stat)

   timer%times(3) = util_wtime(t0) ; timer%names(3) = 'Solving the problem'
    		
   END SUBROUTINE models_driver
    
       
END MODULE models_m
