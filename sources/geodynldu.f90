MODULE geodynldu_m

    use kindParameters_m

    use util_m, only : util_dot, util_barre_defilement
    
    implicit none
    
    real(Rkind), parameter :: zero = 0.0e0_Rkind
    
CONTAINS

!=============================================================================================
    SUBROUTINE geodynldu_facLDU ( A, idiag, nesky, nequa, verb )
!=============================================================================================
    integer(Ikind), intent(in    ) :: nequa, nesky, verb
    integer(Ikind), intent(in    ) :: idiag(*)
    real   (Rkind), intent(in out) :: a(nesky)
!---------------------------------------------------------------------------------------------                
!                                                       
!   Factorisation L D U d'un matrice symetrique (L = U') stockee par ligne de ciel 
!     (source : extraite du code GEODYN, J.P. Vilotte)
!
!
!     - en entree : 
!
!          A   : elements de la matrice symetrique stockes par ligne de ciel
!        idiag : adresses des elements diagonaux
!
!        nesky : occupation memoire de A
!        nequa : nombre d'equations (= nbr d'inconnues)
!
!     - en sortie :
!
!          A   : table contenant les elements des matrices D et U
!
!
!     Notice :
!     ------
!
!    * Le stockage par ligne de ciel (pour matrice symetrique)
!
!     exemple :
!
!                       I  A11   .   A13   .    .   A16  I
!                       I       A22   0   A24   .    0   I
!            si    A =  I            A33  A34   .    0   I
!                       I                 A44   .    0   I
!                       I                      A55  A56  I
!                       I                           A66  I
!
!     (les points representent les termes nuls) est presentee sous forme d'un
!     vecteur de 15 mots :
!
!      A11 / A22 / A13, 0, A33 / A24, A34, A44 / A55 / A16, 0, 0, 0, A56, A66
!
!      dans ce vecteur, les positions des termes diagonaux sont contenues dans
!      le tableau d'entiers idiag :
!
!                  idiag =   1, 2, 5, 8, 9, 15
!
!    * La factorisation A = L D U (ou U est la transposee de L) :
!
!      le triangle superieur stricte U est range dans la partie superieur de A, 
!      les elements de D sont places sur la diagonale de A. Pour l'exemple 
!      precedent :
!
!                        I  D1        U13               U16  I
!                        I       D2   U23   U24         U26  I
!                        I            D3    U34         U36  I
!                        I                  D4          U46  I
!                        I                        D5    U56  I
!                        I                              D6   I
!
!      
!      en sortie, le tableau A sera donc :
!
!      D1 / D2 / U13, U23, D3 / U24, U34, D4 / D5 / U16, U26, U36, U46, U56, D6
!
!
!    * routine externe : 
!
!                      util_dot  (produit scalaire de 2 vecteurs)
!---------------------------------------------------------------------------------------------            
    
!--locals:------------------------------------------------------------------------------------  
    real   (Rkind) :: d
    integer(Ikind) :: jr, j, jd, jh, i, is, ie, k, id, ir, ih
!---------------------------------------------------------------------------------------------            
            
!
!-- f a c t o r i s a t i o n   s e c t i o n
!
    jr = 0
    do j = 1, nequa

       if ( verb > 0 ) call util_barre_defilement (i=j, n=nequa, di=1_Ikind, cDone='o')
 
       jd = idiag(j) ; jh = jd - jr ;  is = j - jh + 2 
         
       if ( jh-2 >= 0 ) then
       
          if ( jh-2 > 0 ) then
             ie = j - 1 ;  k = jr + 2  ; id = idiag(is-1)             
!
!--          reduce all equations except diagonal
!
             do i = is, ie
                ir = id ; id = idiag(i) ; ih = min(id-ir-1, i-is+1)
                if ( ih > 0 ) a(k) = a(k) - util_dot(a(k-ih),a(id-ih),ih)
                k = k + 1
             end do
          end if   
!
!--       reduce diagonal term
!
          ir = jr + 1 ; ie = jd - 1 ;  k  = j - jd
                    
          do i = ir, ie
             id = idiag(k + i)
             if ( abs(a(id)) > zero ) then
                d    = a(i)
                a(i) = d / a(id)
                a(jd)= a(jd) - d*a(i)
             end if
          end do
          
       end if
       
       jr = jd
       
    end do       

    END SUBROUTINE geodynldu_facLDU


!=============================================================================================
    SUBROUTINE geodynldu_solLDU ( A, b, idiag, x, nequa, nesky )
!=============================================================================================
    integer(Ikind), intent(in    ) :: nequa, nesky
    integer(Ikind), intent(in    ) :: idiag(*)
    real   (Rkind), intent(in    ) :: A(nesky), b(nequa)
    real   (Rkind), intent(in out) :: x(nequa)
!---------------------------------------------------------------------------------------------            
!
!   Resolution du systeme lineaire A x = b apres avoir factorise la matrice A par facLDU
!   (A est stockee par ligne de ciel)
!     (source : extraite du code GEODYN, J.P. Vilotte)
!
!
!     input arguments :
!     ---------------   .a     = output matrix factors produced by decldu
!                       .idiag = diagonal location pointers
!                       .b     = rhs vector
!                       .x     = solution b is transferred in it at the
!                                beginning
!                       .nequa = number of equations
!
!     output :
!     -----      .x computed solution vector for neq equations
!
!
!    external subroutines :
!    --------------------
!                             .util_dot
!---------------------------------------------------------------------------------------------            
      
!--locals:------------------------------------------------------------------------------------  
    real   (Rkind) :: d
    integer(Ikind) :: jr, j, jd, jh, is, i, id, k 
!---------------------------------------------------------------------------------------------            

!
!-- i n i t i a l i s a t i o n
!
    x = b
!
!-- f o r w a r d    s u b s t i t u t i o n   p a s s
!
    jr = 0
    do j = 1, nequa
       jd   = idiag(j) ;  jh   = jd - jr ;  is = j - jh + 2
       if ( jh - 2 >= 0 ) x(j) = x(j) - util_dot(a(jr+1),x(is-1),jh-1)
       jr = jd
    end do   
!
!-- s c a l i n g     p a s s
!  
    do i = 1, nequa
       id = idiag(i)
       if ( abs(a(id)) > zero ) x(i) = x(i) / a(id)
    end do
    
!
!-- b a c k s u b s t i t u t i o n    p a s s
!
    jd = idiag(nequa); d = x(nequa)
    do j = nequa-1, 1, -1
       jr = idiag(j)
       if ( jd - jr > 1 ) then
          is = j - jd + jr + 2; k = jr - is + 1
          do i = is, j
             x(i) = x(i) - a(i+k)*d
          end do
       end if
       jd = jr ;  d = x(j)
    end do

    END SUBROUTINE geodynldu_solLDU


!=============================================================================================
    SUBROUTINE geodynldu_matvec (a, idiag, av, v, nequa, nesky)
!=============================================================================================
    integer(Ikind), intent(in ) :: nequa, nesky
    integer(Ikind), intent(in ) :: idiag(*)
    real   (Rkind), intent(in ) :: a(nesky), v(nequa)
    real   (Rkind), intent(out) :: av(nequa)
!---------------------------------------------------------------------------------------------            
!   Produit d'une matrice a, stockee par ligne de ciel, par un vecteur v
!     (source : extraite du code GEODYN, J.P. Vilotte)
!---------------------------------------------------------------------------------------------            
      
!--locals:------------------------------------------------------------------------------------  
    real   (Rkind) :: vj, ab
    integer(Ikind) :: js, j, jd, jb, je, jj
!---------------------------------------------------------------------------------------------       

    av(:) = zero
    
    js = 1
    do j = 1, nequa
       jd = idiag(j)
       if ( js <= jd ) then
          vj = v(j)
          ab = a(jd)*vj
          if ( js /= jd ) then
             jb = j - jd
             je = jd - 1
             do jj = js, je
                ab = ab + a(jj)*v(jj+jb)
                av(jj+jb) = av(jj+jb) + a(jj)*vj
             end do
          end if
          av(j) = av(j) + ab
       end if
       js = jd + 1
    end do

!    av(:) = zero
!    js = 1
!    do 20 j = 1, nequa
!       jd = idiag(j)
!       if (js > jd) go to 20
!       vj = v(j)
!       ab = a(jd)*vj
!       if (js == jd) go to 15
!       jb = j - jd
!       je = jd - 1
!       do 10 jj = js, je
!          ab = ab + a(jj) * v(jj+jb)
!10     av(jj+jb) = av(jj+jb) + a(jj)*vj
!15     av(j) = av(j) + ab
!20  js = jd + 1

    
    END SUBROUTINE geodynldu_matvec
    
END MODULE geodynldu_m
