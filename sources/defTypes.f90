MODULE defTypes_m

   use kindParameters_m
   
   implicit none
		
!--------------------------------------------------------------------------------------------- 
   type mesh_t
!
!     all arrays related to the mesh (corresponding sizes are in param)
!	                                                   
      integer(Ikind), allocatable :: lnods(:,:) ! element / nodes (element connectvities)
      integer(Ikind), allocatable :: lface(:,:) ! facet / nodes (facet connectvities) and their ref.#
      integer(Ikind), allocatable :: ledge(:,:) ! edge / nodes (edge connectvities) and their ref. #
      integer(Ikind), allocatable :: lelem(:,:) ! node / elements (neighbourhood) 
      integer(Ikind), allocatable :: lvois(:,:) ! node / nodes (neighbourhood) 
      integer(Ikind), allocatable :: lpoin(:,:) ! point / node and their ref. #
        
      integer(Ikind), allocatable :: iffix(:,:) ! node / dof 
        
      integer(Ikind), allocatable :: lvale(:)   ! number of element connexion
      integer(Ikind), allocatable :: numdom(:)  ! element / domains ref. #
        		
      real   (Rkind), allocatable :: coor0(:,:), & ! initial and 
                                     coord(:,:)    ! current coordinates
						
   end type mesh_t
!--------------------------------------------------------------------------------------------- 
   type udata_t
!
!     all arrays related to material properties and boundary conditions given by the user
!     (corresponding sizes are in param)
!		   
      integer(Ikind), allocatable :: lmatprop(:), &  ! domain / material ref. #   
                                     ldirichl(:), &  ! list of Dirichlet ref. #
                                     lneumann(:)     ! list of Neumann ref. #                                     
	                                  
      integer(Ikind), allocatable :: codediri(:,:)   ! code of imposition (0, 1 or 2) for each dof
                                                     ! and for each Dirichlet condition:
                                                     ! code = codediri(j,i), j=1, ndofn

      integer(Ikind), allocatable :: codeneum(:,:)   ! code of imposition (0, 1 or 2) for each dof
                                                     ! and for each Neumann condition:
                                                     ! code = codeneum(j,i), j=1, ndofn
                                                      
      real   (Rkind), allocatable :: vmatprop(:,:), & ! material properties
                                     vdirichl(:,:), & ! prescribed Dirichlet values
                                     vneumann(:,:)    ! prescribed Neumann values
                                      
      character(len=1), allocatable :: odirichl(:), & ! list of Dirichlet object type 
                                       oneumann(:)    ! list of Neumann object type                      

!     Notice :
!
!     - lmatprop(i), (for i = 1, nmats) gives the # of the corresponding sub-domain in the mesh
!       where the nprop material propreties are given in vmatprop(:,i). 	

!     - lneumann(i), (for i = 1, neuma) gives the # of the corresponding face in the mesh
!       where the ndofn values of the Neumann conditions are given in vneumann(:,i).
!       oneumman(i), gives the type of corresponding objects ('F' for face, 'L' for line,
!       and 'P' for point)
!
!     - ldirichl(i), (for i = 1, ndiri) gives the # of the corresponding face in the mesh
!       where the ndofn values of the Dirichlet conditions are given in vdirichl(j,i) and
!       apply only if codediri(j,i) = 1 or 2 ("2" means: in the local frame)
!       odirichl(i), gives the type of corresponding objects ('F' for face, 'L' for line,
!       and 'P' for point)
!
   end type udata_t
!--------------------------------------------------------------------------------------------- 
   type linsys_t
       
      ! informations about the fem n x n linear (or linearised) system A x = b
      ! (corresponding sizes are also in param)
     
      ! n     : number of unknowns (=nequa in MODULE def_param)
      !
      ! nzero : length of the array Mat needed to store the matrix A. 
      !
      !        - For csr format, nzero is the number of non-zero coefficients. 
      !        - For skyline format, nzero is the length of the skyline profil
      !
      ! Mat   : array of length nzero containing the coefficients of the matrix A.
      !
      ! Row   :- For csr format     : array of length n+1. The first n elements of 
      !                               Row store the index into Mat of the first  
      !                               nonzero element in each row of the matrix.  
      !                               The last element of Row stores nzero+1.
      !
      !        - For skyline matrix : array of length n. It contains the index into 
      !                               Mat of the diagonal element of the matrix.
      !                              
      !
      ! Col   :- For csr format     : contains the column index in the matrix of 
      !                               each element of Mat.
      !         
      !        - For skyline matrix : not used.
      !
      ! Dof   : 2d array of size (ndofn, npoin) corresponding to the dof numbering.
      !          
      !         Each nodal unknowns (j, I), where I is the node number and j the local
      !         dof number, is numbered from 1 to nequa. Constrained (Dirichlet
      !         boundary condition) dofs are not numbered (a negative value is used):
      !
      !             iequa = Dof(j,I) 
      !             if (iequa > 0) then
      !                iequa is then the dof number
      !             else 
      !                the value of the unknown associated to (j,I) is prescribed 
      !                and -iequa gives information about the prescribed value
      !                (e.g. the face number)
      !
      ! Rhs   : array of length n corresponding to the right hand side b of the system.
      !
      ! X     : array of length n corresponding to the solution x of the system A*x=b.
      !
      ! Sol   : array of length n corresponding to the current solution of the fem problem
      !         In case of a time-independent linear problem, Sol = X, while for a non-linear
      !         problem X is the incremental solution. 
      !
      ! Str   : 2d array of size (nstri+ndime) x (nipts*nelem) containing the derived quantities 
      !         at I.P. (stresses, fluxes, etc).
      !
      ! verb    : level of verbosity (for the solvers)
      !       
       
      integer(Ikind)              :: n, nzero, verb
      integer(Ikind), allocatable :: Row(:), Col(:), Dof(:,:), Dof2node(:,:)
        
      real   (Rkind), allocatable :: Mat(:)   
      real   (Rkind), allocatable :: Rhs(:), X(:), Sol(:,:), Str(:,:), bCond(:,:)
                        
   end type linsys_t
!--------------------------------------------------------------------------------------------- 
   type eldata_t
      ! A DT for embedding data from a given element
      logical                     :: allocated = .false., has_dirichlet = .false.
      
      integer(Ikind)              :: el = 0,     & ! Element ID
                                     numDom = 0, & ! ID of its mesh subdomain 
                                     numMat = 0    ! ID of its material
                                     
      integer(Ikind), allocatable :: nod(:), dof(:) ! Node #s and d.o.f
      
      real   (Rkind), allocatable :: xIP(:,:), wIP(:), & ! Local coord. of I.P. and weights
                                     x0 (:,:), x(:,:), & ! Initial and current nodal coord. 
                                     shp(:,:),         & ! Shape functions at the I.P.
                                     der(:,:,:),       & ! their local derivatives
                                     props(:),         & ! Material properties
                                     U(:),             & ! Nodal solution
                                     S(:,:),           & ! Derived quantities
                                     K(:,:),           & ! Elemental stiffness matrix
                                     F(:)                ! Elemental load vector
   contains
      procedure, pass :: alloc => defTypes_eldataAlloc
   end type eldata_t
!--------------------------------------------------------------------------------------------- 
   type cpu_t
      real     (Rkind ) :: times(10), tstart
      character(len=80) :: names(10) 
   end type cpu_t
!--------------------------------------------------------------------------------------------- 

CONTAINS

!=============================================================================================    
   SUBROUTINE defTypes_eldataAlloc ( self, ndime, nnode, ndofn, noutp, nipts, nevab, nprop )
!============================================================================================= 
   class  (eldata_t), intent(in out) :: self   
   integer(Ikind   ), intent(in    ) :: ndime, nnode, ndofn, noutp, nipts, nevab, nprop
!--------------------------------------------------------------------------------------------- 

   if ( self%allocated ) then
      deallocate(self%dof,self%nod)
      deallocate(self%xIP, self%wIP, self%x, self%x0   , self%u  , self%S, &
                 self%shp, self%K  , self%F, self%props, self%der          )
   end if

   allocate( self%dof(nevab), self%nod(nnode) )
   
   allocate( self%xIP(ndime,nipts)      , self%wIP(nipts)      , &
             self%x(ndime,nnode)        , self%x0(ndime,nnode) , &
             self%U(nevab)              , self%S(noutp,nipts)  , &
             self%shp(nnode,nipts)      , self%K(nevab,nevab)  , &
             self%F(nevab)              , self%props(nprop)    , &
             self%der(ndime,nnode,nipts)                         )
             
   self%allocated = .true.

   END SUBROUTINE defTypes_eldataAlloc
      
END MODULE defTypes_m