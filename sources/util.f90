#include "error.fpp"

MODULE util_m

   use kindParameters_m
   use err_m   
   use constants_m, only: IERROR, UERROR, STDOUT, LGSTR, IZERO  

   implicit none
!
!- generic routine for allocating or re-allocating dynamic arrays (resizing):
!
   interface util_alloc
     module procedure util_Ialloc1 ; module procedure util_Ialloc2
     module procedure util_Ralloc1 ; module procedure util_Ralloc2  
     module procedure util_Calloc1 ; module procedure util_Calloc2  
   end interface util_alloc
!
!- generic routine for sorting (adapted from LAPACK routine DLASRT):

   interface util_sort
      module procedure util_SortIk1, util_SortRk1, util_SortCk1   
   end interface

   interface util_write
      module procedure util_writeI0, util_writeI1, util_writeI2, &
                       util_writeR0, util_writeR1, util_writeR2, &
                       util_writeS0
   end interface util_write
   
   interface util_intToChar
      module procedure util_int32ToChar, util_int64ToChar
   end interface util_intToChar
   
CONTAINS
   
   
!=============================================================================================
   SUBROUTINE util_printTimer ()
!=============================================================================================
   use param_m, only: timer
   
!- local variables: --------------------------------------------------------------------------
   integer           :: i, maxl
   real     (Rkind ) :: prct, tot
   character(len=99) :: fmt = '', ctot = 'Total wall clock time'
!---------------------------------------------------------------------------------------------
   
   tot = util_wtime(timer%tstart)
   
   print '(/,a)',' Partial and total elapsed times '
   print '(a  )',' =============================== '
   maxl = 0
   do i = 1, 3
      maxl = max(maxl, len_trim(timer%names(i)))
   end do
   maxl = max(maxl,len_trim(ctot))
   write(fmt,'(i0)')maxl
   fmt = '(" - ",a'//trim(fmt)//',a2,f12.5,a,f5.2,a)'
      
   do i = 1, 3
      prct = 100.0*timer%times(i) / tot
      write(*,fmt) &
             timer%names(i),': ', timer%times(i), ' s (',prct,' %)'
   end do
   write(*,fmt)ctot,': ',tot,' s'
   
   END SUBROUTINE util_printTimer
     

!=============================================================================================
   FUNCTION util_wtime ( t0 ) result ( t )
!=============================================================================================
   use, intrinsic :: iso_fortran_env, only: int64
   real(Rkind), intent(in), optional :: t0
   real(Rkind)                       :: t
!---------------------------------------------------------------------------------------------
!  Computes the elapsed wall clock time
!  Usage:
!       real(Rkind) :: t0
!       t0 = util_wtime()
!       .... (part of the code to be timed) ....
!       print*,'Elapsed time: ',util_time(t0),' seconds'
!---------------------------------------------------------------------------------- R.H. 12/21

!- local variables: --------------------------------------------------------------------------
   integer(int64) :: nPeriode, nPeriode_sec, nPeriode_max
!---------------------------------------------------------------------------------------------
   
   if ( present(t0) ) then
      call system_clock ( count=nPeriode, count_rate=nperiode_sec, count_max=nPeriode_max )
      t = real(nPeriode,Rkind) / real(nperiode_sec,Rkind) - t0
      if ( t < 0.0_Rkind ) t = t  + real(nPeriode_max,Rkind) / real(nperiode_sec,Rkind)
   else
      call system_clock ( count=nPeriode, count_rate=nperiode_sec)
      t = real(nPeriode,Rkind) / real(nperiode_sec,Rkind)
   end if

   END FUNCTION util_wtime


!=============================================================================================   
   FUNCTION util_int32ToChar ( i ) result( res )
!=============================================================================================
   integer  ( i32 ),             intent(in) :: i
   character(len=:), allocatable            :: res
!---------------------------------------------------------------------------------------------
!  Converts integer to string (from Vladimir F., https://stackoverflow.com)
!----------------------------------------------------------------------------------- R.H.04/22

!- local variables: --------------------------------------------------------------------------
   character(len=range(i)+2) :: tmp
!---------------------------------------------------------------------------------------------

   write(tmp,'(i0)') i ;  res = trim(tmp)
  
  END FUNCTION util_int32ToChar       


!=============================================================================================   
   FUNCTION util_int64ToChar ( i ) result( res )
!=============================================================================================
   integer  ( i64 ),             intent(in) :: i
   character(len=:), allocatable            :: res
!---------------------------------------------------------------------------------------------
!  Converts integer to string (from Vladimir F., https://stackoverflow.com)
!----------------------------------------------------------------------------------- R.H.04/22

!- local variables: --------------------------------------------------------------------------
   character(len=range(i)+2) :: tmp
!---------------------------------------------------------------------------------------------

   write(tmp,'(i0)') i ;  res = trim(tmp)
  
  END FUNCTION util_int64ToChar     


!=============================================================================================   
   SUBROUTINE util_writeS0 ( u, form, x, fmt )
!=============================================================================================
   integer  (Ikind),           intent(in) :: u
   character(len=*),           intent(in) :: form
   character(len=*),           intent(in) :: x
   character(len=*), optional, intent(in) :: fmt   
!---------------------------------------------------------------------------------------------
!  Writes a string on the file of logical unit u
!----------------------------------------------------------------------------------------- R.H.

  if ( form == 'bin' ) then
     write(u) x
  else 
     if ( present(fmt) ) then
        write(u,fmt) x
     else
        write(u,'(a)') x
     end if
  end if  
  
  END SUBROUTINE util_writeS0  
       

!=============================================================================================   
   SUBROUTINE util_writeI0 ( u, form, x, fmt )
!=============================================================================================
   integer  (Ikind),           intent(in) :: u
   character(len=*),           intent(in) :: form
   integer  (Ikind),           intent(in) :: x
   character(len=*), optional, intent(in) :: fmt      
!---------------------------------------------------------------------------------------------
!  Writes an integer on the file of logical unit u
!----------------------------------------------------------------------------------------- R.H.

  if ( form == 'bin' ) then
     write(u) x
  else 
     if ( present(fmt) ) then
        write(u,fmt) x
     else  
        write(u,'(g0)') x
     end if
  end if  
  
  END SUBROUTINE util_writeI0  

!=============================================================================================   
   SUBROUTINE util_writeR0 ( u, form, x, fmt )
!=============================================================================================
   integer  (Ikind),           intent(in) :: u
   character(len=*),           intent(in) :: form
   real     (Rkind),           intent(in) :: x
   character(len=*), optional, intent(in) :: fmt         
!---------------------------------------------------------------------------------------------
!  Writes a real on the file of logical unit u
!----------------------------------------------------------------------------------------- R.H.

  if ( form == 'bin' ) then
     write(u) x
  else 
     if ( present(fmt) ) then
        write(u,fmt) x
     else  
        write(u,'(g0)') x
     end if        
  end if  
  
  END SUBROUTINE util_writeR0  
  

!=============================================================================================   
   SUBROUTINE util_writeI1 ( u, form, x, fmt, lb, ub )
!=============================================================================================
   integer  (Ikind),           intent(in) :: u
   character(len=*),           intent(in) :: form
   integer  (Ikind),           intent(in) :: x(:)
   character(len=*), optional, intent(in) :: fmt            
   integer  (Ikind), optional, intent(in) :: lb, ub
!---------------------------------------------------------------------------------------------
!  Writes a rank-1 array of integers on the file of logical unit u
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: lb_, ub_
!---------------------------------------------------------------------------------------------

  if ( present(lb) ) then ; lb_ = lb ; else ; lb_ = lbound(x,1) ; end if
  if ( present(ub) ) then ; ub_ = ub ; else ; ub_ = ubound(x,1) ; end if
           
  if ( form == 'bin' ) then
     write(u) x(lb_:ub_)
  else 
     if ( present(fmt) ) then
        write(u,fmt) x(lb_:ub_)
     else  
        write(u,'(*(g0,1x))') x(lb_:ub_)
     end if   
  end if  
  
  END SUBROUTINE util_writeI1


!=============================================================================================   
   SUBROUTINE util_writeR1 ( u, form, x, fmt, lb, ub )
!=============================================================================================
   integer  (Ikind),           intent(in) :: u
   character(len=*),           intent(in) :: form
   real     (Rkind),           intent(in) :: x(:)
   character(len=*), optional, intent(in) :: fmt            
   integer  (Ikind), optional, intent(in) :: lb, ub   
!---------------------------------------------------------------------------------------------
!  Writes a rank-1 array of reals on the file of logical unit u
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: lb_, ub_
!---------------------------------------------------------------------------------------------

  if ( present(lb) ) then ; lb_ = lb ; else ; lb_ = lbound(x,1) ; end if
  if ( present(ub) ) then ; ub_ = ub ; else ; ub_ = ubound(x,1) ; end if
           
  if ( form == 'bin' ) then
     write(u) x(lb_:ub_)
  else 
     if ( present(fmt) ) then
        write(u,fmt) x(lb_:ub_)
     else  
        write(u,'(*(g0,1x))') x(lb_:ub_)
     end if    
  end if  
  
  END SUBROUTINE util_writeR1      

   
!=============================================================================================   
   SUBROUTINE util_writeI2 ( u, form, x, fmt, lb1, ub1, lb2, ub2 )
!=============================================================================================
   integer  (Ikind),           intent(in) :: u
   character(len=*),           intent(in) :: form
   integer  (Ikind),           intent(in) :: x(:,:)
   character(len=*), optional, intent(in) :: fmt            
   integer  (Ikind), optional, intent(in) :: lb1, ub1, lb2, ub2      
!---------------------------------------------------------------------------------------------
!  Writes a rank-2 array of integers on the file of logical unit u
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: lb1_, ub1_, lb2_, ub2_, i, j
!---------------------------------------------------------------------------------------------

  if ( present(lb1) ) then ; lb1_ = lb1 ; else ; lb1_ = lbound(x,1) ; end if
  if ( present(ub1) ) then ; ub1_ = ub1 ; else ; ub1_ = ubound(x,1) ; end if
  if ( present(lb2) ) then ; lb2_ = lb2 ; else ; lb2_ = lbound(x,2) ; end if
  if ( present(ub2) ) then ; ub2_ = ub2 ; else ; ub2_ = ubound(x,2) ; end if 
   
  if ( form == 'bin' ) then
    ! write(u) x(lb1_:ub1_,lb2_:ub2_) !<-- a temporary array may be created
     write(u) ((x(i,j),i=lb1_,ub1_),j=lb2_,ub2_)
  else 
     if ( present(fmt) ) then
        write(u,fmt) ((x(i,j),i=lb1_,ub1_),j=lb2_,ub2_)
     else  
        write(u,'(*(g0,1x))') ((x(i,j),i=lb1_,ub1_),j=lb2_,ub2_)
     end if    
  end if  
  
  END SUBROUTINE util_writeI2   


!=============================================================================================   
   SUBROUTINE util_writeR2 ( u, form, x, fmt, lb1, ub1, lb2, ub2 )
!=============================================================================================
   integer  (Ikind),           intent(in) :: u
   character(len=*),           intent(in) :: form
   real     (Rkind),           intent(in) :: x(:,:)
   character(len=*), optional, intent(in) :: fmt            
   integer  (Ikind), optional, intent(in) :: lb1, ub1, lb2, ub2
!---------------------------------------------------------------------------------------------
!  Writes a rank-2 array of reals on the file of logical unit u
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: lb1_, ub1_, lb2_, ub2_,i, j
!---------------------------------------------------------------------------------------------

  if ( present(lb1) ) then ; lb1_ = lb1 ; else ; lb1_ = lbound(x,1) ; end if
  if ( present(ub1) ) then ; ub1_ = ub1 ; else ; ub1_ = ubound(x,1) ; end if
  if ( present(lb2) ) then ; lb2_ = lb2 ; else ; lb2_ = lbound(x,2) ; end if
  if ( present(ub2) ) then ; ub2_ = ub2 ; else ; ub2_ = ubound(x,2) ; end if 
   
  if ( form == 'bin' ) then
    ! write(u) x(lb1_:ub1_,lb2_:ub2_) !<-- a temporary array may be created
     write(u) ((x(i,j),i=lb1_,ub1_),j=lb2_,ub2_)
  else 
     if ( present(fmt) ) then
        write(u,fmt) ((x(i,j),i=lb1_,ub1_),j=lb2_,ub2_)
     else  
        write(u,'(*(g0,1x))') ((x(i,j),i=lb1_,ub1_),j=lb2_,ub2_)
     end if     
  end if  
  
  END SUBROUTINE util_writeR2
           
           
!=============================================================================================
   FUNCTION util_findpos_in_unsortedlist (list, ideb, ifin, k) result(posk)
!=============================================================================================
   integer(Ikind) :: ideb, ifin, k, list(*)
   integer(Ikind) :: posk
!---------------------------------------------------------------------------------------------
!  Finds the absolute position of the integer k in the unordered array list: list(posk) = k.
!
!  Search is done for ideb <= posk <= ifin.
!
!  Returns 0 if k is not present in the sub-array list(ideb:ifin)
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   integer :: i
!---------------------------------------------------------------------------------------------

!
!- brute force
!
   posk = 0
   do i = ideb, ifin
      if (list(i) == k) then
         posk = i
         exit
      end if
   end do

   END FUNCTION util_findpos_in_unsortedlist    

!=============================================================================================
   RECURSIVE FUNCTION util_findpos_in_sortedlist (list, ideb, ifin, k) result(posk)
!=============================================================================================
   integer(Ikind) :: ideb, ifin, k, list(*)
   integer(Ikind) :: posk
!---------------------------------------------------------------------------------------------
!  Finds the absolute position of the integer k in the array list, assumed sorted in ascending 
!  order: find posk st list(posk) = k.
!
!  Search is done for ideb <= posk <= ifin.
!
!  Returns 0 if k is not present in the sub-array list(ideb:ifin)
!
!  (j'avais mis les deux cas, sorted et unsorted, avec un select case. Plus judicieux de faire
!   test avant l'appel, ca prend moins de temps)
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i1, i2, mil
!---------------------------------------------------------------------------------------------

!
!- sorted list: proceed by dichotomy
!
   posk = 0   ; if (k  > list(ifin) .or. k < list(ideb)) return  
   posk = ideb; if (k == list(ideb)                    ) return  
   posk = ifin; if (k == list(ifin)                    ) return
   posk = 0   ; if (ifin - ideb <= 1                   ) return
        
   mil = (ideb + ifin)/2; 
 
   posk = mil; if (k == list(mil)                     ) return

   if (k < list(mil)) then
      i1 = ideb; i2 = mil
   else
      i1 = mil; i2 = ifin
   end if      
        
   posk = util_findpos_in_sortedlist (list, i1, i2, k)
    
   END FUNCTION util_findpos_in_sortedlist    

                  
!=============================================================================================
   SUBROUTINE util_trisurf (xyz, stri, wtri)
!=============================================================================================
   real(Rkind), intent(in ) :: xyz(3,3)
   real(Rkind), intent(out) :: wtri(3), stri
!---------------------------------------------------------------------------------------------
!  Computes the area of a triangle in 3D space
!
!  Input
!      xyz(:,i) : the 3 coordinates of the i th vertice (i = 1, 2, 3)
!
!  Outputs
!      stri : the surface measure
!      wtri : the unit normal vector (if stri > 0)
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   real(Rkind) :: u(3), v(3)
!---------------------------------------------------------------------------------------------
     
   u = xyz(:,2) - xyz(:,1)  
   v = xyz(:,3) - xyz(:,1)
    
   wtri = 0.5_Rkind * [ u(2)*v(3)-u(3)*v(2) , u(3)*v(1)-u(1)*v(3) , u(1)*v(2)-u(2)*v(1) ]
    
   stri = sqrt ( dot_product(wtri,wtri) )
     
   if (stri > 0.0_Rkind) wtri = wtri / stri

   END SUBROUTINE util_trisurf


!=============================================================================================
   SUBROUTINE util_barre_defilement ( i, n, di, sLead, sTrail, cDone, cRemain, nsymb )
!=============================================================================================
   integer  (Ikind),           intent(in) :: di, i, n
   character(len=*), optional, intent(in) :: sLead, sTrail
   character(len=1), optional, intent(in) :: cDone, cRemain      
   integer  (Ikind), optional, intent(in) :: nsymb
!---------------------------------------------------------------------------------------------
!  Affiche (sur stdout) une barre de defilement au cours d'un calcul dont le volume est connu 
!  a l'avance (une boucle par exemple). 
!
!  Inputs:
!
!  . i       : Numero de l'element en cours dans la boucle (1 <= i <= n).
!
!  . n       : Numero du dernier element de la boucle (n >= i).
!
!  . di      : Frequence de l'affichage (en % : 1 <= di <= 100).
!
!  . sLead   : (option). Commentaire (fixe) a placer en debut de barre (sera suivi de ": ").
!
!  . sTrail  : (option). Commentaire (eventuellement changeant) a placer en fin de barre.
!
!  . cDone   : (option) Symbole (character*1) a utiliser pour la partie deja traitee (1 a i).
!                       S'il est absent, le caracter "=" sera utilise par defaut.
!
!  . cRemain : (option) Symbole (character*1) a utiliser pour la partie restante (i+1 a n).
!                       S'il est absent, le caracter blanc (" ") sera utilise sauf si "cDone" 
!                       est lui meme un blanc, auquel cas le character "o" sera utilise.
!
!  . nsymb   : (option) Nombre total de symboles a utiliser dans la barre. Si absent ou nul,
!                       le nombre par defaut (DEFNSYMB) sera utilise.
!                       Attention: la taille de la console doit etre >= nsymb+15
!                       (voire superieure si presence de sTrail).
!  Exemples :
!
!        n = 200
!        do i = 1, n
!           call sleep(1)
!           call BarreDefilement ( i, n, 10, &
!                                  cDone = "=", cRemain = "#" ) ! options
!        end do
!
!        produira un affichage tous les 10% sous la forme :
!
!                40%  (/)  [ ====================############################# ]
!
!        alors que 
!
!           call BarreDefilement ( i, n, 10, &
!                                  cDone = "=" ) ! options
!        produira :
!
!                40%  (/)  [ ====================                               ]
!
!        et que 
!
!           call BarreDefilement ( i, n, 10,  &
!                                cDone = "=", cRemain = "#", sLead = "In progress" ) ! options
!
!        produira :
!
!                In progress: 40%  (/)  [ ====================############################# ]
!
! 
!  La barre contiendra un total de ntsymb symboles "cDone" quand 100 % du calcul sera atteint
!  (ntsymb = nsymb si present ou DEFNSYMB sinon).
!
!  source : inspire d'une reponse de Steve Lionel trouvee sur un forum intel : 
!           https://software.intel.com/en-us/forums/intel-fortran-compiler-for-linux-and-mac-
!           os-x/topic/270155
!----------------------------------------------------------------------------------------- R.H.

!- local variables: --------------------------------------------------------------------------
   integer         , parameter         :: DEFNSYMB = 50 ! defaut pour le nombre de symboles 
   character(len=3), parameter         :: wheel(4) = ['(|)','(/)','(-)','(\)']

   integer                             :: isymb, m
   real                                :: tprint
   character(len=3)                    :: percents
   character(len=:), allocatable       :: bar, sT

   integer         ,              SAVE :: nprint, ntsymb, sTlen
   character(len=1),              SAVE :: symbDone, symbRemain      
   character(len=:), allocatable, SAVE :: sL
!---------------------------------------------------------------------------------------------
                     
   if (i == 1) then
!
!-    Si premiere fois : definir le commentaire a placer en debut de barre, les symboles a 
!     a utiliser et leur nombre total
!      
      nprint = 0
!
!-    Le commentaire (ou chaine vide) en debut de barre (fixe) :
!      
      if (present(sLead)) then
         sL = char(13) // sLead // ': '
      else
         sL = char(13)
      end if   
      
      if (present(sTrail)) then
         sTlen = len_trim(sTrail)
      else
         sTlen = 1
      end if      
!
!-    Le symbole de progression :    
!  
      if (present(cDone)) then
         symbDone = cDone
      else
         symbDone = '='
      end if    
!
!-    Le symbole pour la partie restante :
!    
      if (present(cRemain)) then
         symbRemain = cRemain 
      else
         symbRemain = ' '
      end if      
!
!-    Si identiques, modifier symbRemain :
!   
      if (symbRemain == symbDone) then
         if (symbRemain /= ' ') then
            symbRemain = ' '
         else
            symbRemain = 'o'
         end if
      end if      
!
!-    Le nombre total de symboles dans la barre :
!            
      if (present(nsymb)) then
         ntsymb = nsymb
         if (ntsymb == 0) ntsymb = DEFNSYMB
      else
         ntsymb = DEFNSYMB
      end if    
            
   end if   
!
!- Commentaire a placer en fin de barre (ou chaine vide) :
!
   if (present(sTrail)) then
      m = sTlen - len_trim(sTrail)
      if (m > 0) then
         sT = sTrail // repeat(" ",m)
      else
         sTlen = len_trim(sTrail)      
         sT = sTrail   
      end if   
   else
      sT = repeat(" ",sTlen)
   end if         
         
   tprint = real(n*nprint*di) / 100.0e0
   
   if (i >= tprint .or. i == n) then
      nprint = nprint + 1

      isymb = nint( real(i*ntsymb)/real(n) )
      
      write(percents,'(i3)') floor( ( 100.0*real(i) ) / real(n) )

      bar = repeat(symbRemain, 15+ntsymb)
      
!     1  4 6  9 13 13+1                                     13+ntsymb 15+ntsymb
!     |  | |  |  | |                                                | |
!     v  v v  v  v v                                                v v
!     100%  (/)  [ ====================############################## ]
!                  ^                  ^                             ^
!                  |                  |                             |
!                  1    ...         isymb           ...        ntsymb
 
      bar(1         : 6        ) = trim(percents) // '%  '
      bar(7         : 9        ) = wheel(mod(nprint,4)+1)
      bar(10        : 13       ) = '  [ '
      bar(14        : 13+isymb ) = repeat(symbDone, isymb)
      bar(14+ntsymb : 15+ntsymb) = ' ]'
      
      write(6,'(a)',advance = 'no') sL // bar // ' ' // sT ; flush(6)

      if (i == n) then
         write(6,*) ; flush(6)
      end if   
   end if

   END SUBROUTINE util_barre_defilement


!=============================================================================================
   recursive SUBROUTINE util_ScrollingMessage ( message, delaytime )
!=============================================================================================   
   use iso_c_binding
   character(len=*), intent(in) :: message
   real            , intent(in) :: delaytime
!---------------------------------------------------------------------------------------------
!  Prints (on stdout) a scrolling message
!
!  Inputs:
!  . message  : the message to display
!  . delaytime: delay time (in seconds) between two character prints
!-----------------------------------------------------------------------------------R.H. 01/18 

!- local variables: --------------------------------------------------------------------------
   integer(c_int32_t), parameter :: ONESEC = 100000_c_int32_t
   integer(c_int32_t)            :: delay
   integer(Ikind    )            :: nlen, i
!---------------------------------------------------------------------------------------------
   
   interface ! interface found in http://computer-programming-forum.com (by Tobias Burnu)
      subroutine usleep (useconds) bind(C)
         use iso_c_binding
         implicit none
         integer(c_int32_t), value :: useconds
      end subroutine
   end interface   
   
   delay = int(ONESEC * delaytime, kind=c_int32_t)
   nlen = len(message)
   do i = 1, nlen
      call usleep ( delay )
      if ( i == nlen ) then
         write(STDOUT,'(a1)') message(i:i)
         flush(STDOUT)         
      else
         write(STDOUT,'(a1)',advance='no') message(i:i)
         flush(STDOUT)
      end if
   end do
      
   END SUBROUTINE util_ScrollingMessage
   
                   
!=============================================================================================
   SUBROUTINE util_prodmt (c,a,b,n1,m,n2)
!=============================================================================================
   integer(Ikind), intent(in ) :: n1, m, n2
   real   (Rkind), intent(in ) :: a(n1,m), b(m,n2)
   real   (Rkind), intent(out) :: c(n1,n2)
!---------------------------------------------------------------------------------------------        
!  Produit de deux matrices : c = a*b
!---------------------------------------------------------------------------------------------        

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i, j, k
!--------------------------------------------------------------------------------------------- 

   do i = 1, n1
      do j = 1, n2
         c(i,j) = 0.0_Rkind
         do k = 1, m
            c(i,j) = c(i,j) + a(i,k) * b(k,j)
         end do
      end do
   end do

   END SUBROUTINE util_prodmt


!=============================================================================================
   SUBROUTINE util_prodmv (v,A,u,n1,n2)
!=============================================================================================
   integer(Ikind), intent(in ) :: n1, n2
   real   (Rkind), intent(in ) :: a(n1,n2), u(n2)
   real   (Rkind), intent(out) :: v(n1)
!---------------------------------------------------------------------------------------------    
!  Produit de la matrice A par le vecteur u : v = A*u
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i, j
!---------------------------------------------------------------------------------------------  
      
   do i = 1, n1
      v(i) = 0.0_Rkind
      do j = 1, n2
         v(i) = v(i) + a(i,j) * u(j)
      enddo
   enddo
    
   END SUBROUTINE util_prodmv
   

!=============================================================================================
   SUBROUTINE util_prodCsrSymMv (n, a, ia, ja, u, v)
!=============================================================================================
   integer(Ikind), intent(in   ) :: n, ia(n+1), ja(*)
   real   (Rkind), intent(in   ) :: a(*), u(n)
   real   (Rkind), intent(  out) :: v(n)
!---------------------------------------------------------------------------------------------    
!  Produit de la matrice CSR symetrique par le vecteur u : v = a*u
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i, j, k
   real   (Rkind) :: ui, aj, uk
!---------------------------------------------------------------------------------------------  
   
   v = 0.0e0_Rkind
      
   do i = 1, n
      ui = u(i)
      v(i) = v(i) + a(ia(i)) * ui
      do j = ia(i)+1, ia(i+1)-1
         k = ja(j) ; aj = a(j) ; uk = u(k)
         v(i) = v(i) + aj * uk
         v(k) = v(k) + aj * ui
      enddo
   enddo
       
   END SUBROUTINE util_prodCsrSymMv
   


!=============================================================================================
   FUNCTION util_dot (a,b,n) result(dot)
!=============================================================================================
   integer(Ikind), intent(in ) :: n
   real   (Rkind), intent(in ) :: a (*), b(*)
   real   (Rkind)              :: dot
!---------------------------------------------------------------------------------------------
!  function to perform dot product of two real vectors
!---------------------------------------------------------------------------------------------                   
 
!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i
!---------------------------------------------------------------------------------------------                   

   dot = 0.0e0_Rkind
   do i = 1, n
      dot = dot + a(i)*b(i)
   end do

   END FUNCTION util_dot


!=============================================================================================
   FUNCTION util_det ( a, n, stat ) result( det )
!=============================================================================================
   integer(Ikind), intent(in    ) :: n
   real   (Rkind), intent(in    ) :: a(n,n)
   real   (Rkind)                 :: det
   type   (err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  computes the determinant of nxn matrix with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------
      
   det = 0.0_Rkind
    
   select case (n)
      case (1)
         det = a(1,1)
    
      case (2)
         det = a(1,1)*a(2,2) - a(1,2)*a(2,1)

      case (3)
         det = a(1,1)*(a(2,2)*a(3,3) - a(3,2)*a(2,3)) +  &
               a(1,2)*(a(3,1)*a(2,3) - a(2,1)*a(3,3)) +  &
               a(1,3)*(a(2,1)*a(3,2) - a(3,1)*a(2,2))   
                
      case default
         stat = err_t( stat = UERROR, where = 'util_det', &
                       msg = 'The matrix must be 1x1, 2x2 or 3x3 only' )
          
   end select
    
   END FUNCTION util_det

    
!=============================================================================================
   FUNCTION util_inverse ( A, n, stat ) result( Ainv )
!=============================================================================================
   integer(Ikind), intent(in    ) :: n
   real   (Rkind), intent(in    ) :: A(n,n)
   real   (Rkind)                 :: Ainv(n,n)
   type   (err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Computes the inverse of n x n matrix with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------                   

   call util_detAndinverse ( A, n, Ainv, stat )
    
   END FUNCTION util_inverse
   
!=============================================================================================
   SUBROUTINE util_detAndInverse ( A, n, Ainv, stat, detA ) 
!=============================================================================================
   integer(Ikind),           intent(in    ) :: n
   real   (Rkind),           intent(in    ) :: A(n,n)
   real   (Rkind),           intent(in out) :: Ainv(n,n)
   type   (err_t),           intent(in out) :: stat
   real   (Rkind), optional, intent(in out) :: detA
!---------------------------------------------------------------------------------------------
!  Computes the determinant and the inverse of n x n matrix with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
   real      (Rkind), parameter :: zero = 0.0_Rkind, one = 1.0_Rkind
   real      (Rkind)            :: detA_
!---------------------------------------------------------------------------------------------                   
   
   detA_ = util_det ( A, n, stat )
   
   if ( present(detA) ) detA  = detA_ 
   
   if ( detA_ == zero ) then
      stat = err_t( stat = UERROR, where = 'util_detAndInverse', msg = 'Singular matrix' )
      return
   end if
       
   detA_ = one / detA_
   
   select case (n)
       case (1)
          Ainv(1,1) = detA_
          
       case (2)
          Ainv(1:2,1) = [ A(2,2) , -A(2,1)] * detA_
          Ainv(1:2,2) = [-A(1,2) ,  A(1,1)] * detA_
          
       case (3)
          Ainv(1:3,1) = [ A(2,2)*A(3,3) - A(2,3)*A(3,2)  ,          &
                          A(3,1)*A(2,3) - A(2,1)*A(3,3)  ,          &
                          A(2,1)*A(3,2) - A(3,1)*A(2,2)  ] * detA_

          Ainv(1:3,2) = [ A(1,3)*A(3,2) - A(1,2)*A(3,3)  ,           &
                          A(1,1)*A(3,3) - A(1,3)*A(3,1)  ,           &
                          A(1,2)*A(3,1) - A(3,2)*A(1,1)  ] * detA_

          Ainv(1:3,3) = [ A(1,2)*A(2,3) - A(1,3)*A(2,2)  ,           &
                          A(2,1)*A(1,3) - A(2,3)*A(1,1)  ,           &
                          A(1,1)*A(2,2) - A(1,2)*A(2,1)  ] * detA_
        
       case default
         stat = err_t( stat = UERROR, where = 'util_det', &
                       msg = 'The matrix must be 1x1, 2x2 or 3x3 only' )
                   
   end select
    
   END SUBROUTINE util_detAndInverse   


!=============================================================================================
   SUBROUTINE util_SortIk1 ( d, ord, stat, indx )
!=============================================================================================
   integer  (Ikind),                        intent(in out) :: d(:)
   character(len=1),                        intent(in    ) :: ord
   type     (err_t),                        intent(in out) :: stat
   integer  (Ikind), allocatable, optional, intent(   out) :: indx(:)   
!--------------------------------------------------------------------------------------------- 
!  Sorts the integer rank-1 array "v" in increasing (ord='i') or in decreasing (ord='d') order
!
!  Inputs:
!  . v  : (modified) The array to sort.
!  . ord: 'i' (or 'I') for increasing order or 'd' (or 'D') for decreasing order.
!
!  Outputs:
!  . v   : The sorted array.
!  . indx: (optional) The original indices.
!
!  Source: this is a modified copy of LAPACK routine DLASRT adapted here for integer array
!          and where "indx" was added.
!-----------------------------------------------------------------------------------R.H. 10/18

!- local variables ---------------------------------------------------------------------------  
   character(len=*), parameter   :: HERE = 'util_SortIk1'
   integer  (Ikind), parameter   :: SELECT = 20
   integer  (Ikind)              :: dir, endd, i, j, n, start, stkpnt, stack(2,32), estat, &
                                    i1, i2, i3, imnmx, itmp
   integer  (Ikind)              :: d1, d2, d3, dmnmx, tmp
   integer  (Ikind), allocatable :: indxd(:)
!--------------------------------------------------------------------------------------------- 

   n = size(d)
   
#include "include/util_sort.inc" 

   if (present(indx)) call move_alloc (from = indxd, to = indx)
      
   END SUBROUTINE util_SortIk1


!=============================================================================================
   SUBROUTINE util_SortRk1 ( d, ord, stat, indx )
!=============================================================================================
   real     (Rkind),                        intent(in out) :: d(:)
   character(len=1),                        intent(in    ) :: ord
   type     (err_t),                        intent(in out) :: stat   
   integer  (Ikind), allocatable, optional, intent(   out) :: indx(:)
!--------------------------------------------------------------------------------------------- 
!  Sorts the real rank-1 array "v" in increasing (ord='i') or in decreasing (ord='d') order
!
!  Inputs:
!  . v  : (modified) The array to sort.
!  . ord: 'i' (or 'I') for increasing order or 'd' (or 'D') for decreasing order.
!
!  Outputs:
!  . v   : The sorted array.
!  . indx: (optional) The original indices.
!
!  Source: this is a modified copy of LAPACK routine DLASRT where "indx" was added.
!-----------------------------------------------------------------------------------R.H. 10/18

!- local variables ---------------------------------------------------------------------------  
   character(len=*), parameter   :: HERE = 'util_SortRk1'
   integer  (Ikind), parameter   :: SELECT = 20
   integer  (Ikind)              :: dir, endd, i, j, n, start, stkpnt, stack(2,32), estat, &
                                    i1, i2, i3, imnmx, itmp
   real     (Rkind)              :: d1, d2, d3, dmnmx, tmp
   integer  (Ikind), allocatable :: indxd(:)
!--------------------------------------------------------------------------------------------- 

   n = size(d)
   
#include "include/util_sort.inc" 

   if (present(indx)) call move_alloc (from = indxd, to = indx)
      
   END SUBROUTINE util_SortRk1


!=============================================================================================
   SUBROUTINE util_SortCk1 ( z, ord, stat, indx )
!=============================================================================================
   complex  (Rkind),                        intent(in out) :: z(:)
   character(len=1),                        intent(in    ) :: ord
   type     (err_t),                        intent(in out) :: stat
   integer  (Ikind), allocatable, optional, intent(   out) :: indx(:)
!--------------------------------------------------------------------------------------------- 
!  Sorts the complex rank-1 array "v" in increasing (ord='i') or in decreasing (ord='d') 
!  magnitude order
!
!  Inputs:
!  . v  : (modified) The array to sort.
!  . ord: 'i' (or 'I') for increasing order or 'd' (or 'D') for decreasing order.
!
!  Outputs:
!  . v   : The sorted array.
!  . indx: (optional) The original indices.
!
!  Source: this is a modified copy of LAPACK routine DLASRT where "indx" was added.
!-----------------------------------------------------------------------------------R.H. 10/18

!- local variables ---------------------------------------------------------------------------  
   character(len=*), parameter   :: HERE = 'util_SortCk1'
   integer  (Ikind), parameter   :: SELECT = 20
   integer  (Ikind)              :: dir, endd, i, j, n, start, stkpnt, stack(2,32), estat, &
                                    i1, i2, i3, imnmx, itmp
   real     (Rkind)              :: d1, d2, d3, dmnmx, tmp
   real     (Rkind), allocatable :: d(:)
   integer  (Ikind), allocatable :: indxd(:)
!--------------------------------------------------------------------------------------------- 

   n = size(z)
   
   allocate(d(n)) ; d = abs(z)
   
#include "include/util_sort.inc" 

   z = z(indxd)
   
   if (present(indx)) call move_alloc (from = indxd, to = indx)
      
   END SUBROUTINE util_SortCk1
                        
                                        
!=============================================================================================
   SUBROUTINE util_get_unit ( iunit )
!=============================================================================================
   integer(Ikind), intent(out) :: iunit  
!---------------------------------------------------------------------------------------------                   
!   GET_UNIT returns a free FORTRAN unit number.
!
!   Discussion:
!
!    A "free" FORTRAN unit number is an integer between 1 and 99 which
!    is not currently associated with an I/O device.  A free FORTRAN unit
!    number is needed in order to open a file with the OPEN command.
!
!    If IUNIT = 0, then no free FORTRAN unit could be found, although
!    all 99 units were checked (except for units 5, 6 and 9, which
!    are commonly reserved for console I/O).
!
!    Otherwise, IUNIT is an integer between 1 and 99, representing a
!    free FORTRAN unit.  Note that GET_UNIT assumes that units 5 and 6
!    are special, and will never return those values.
!
!   Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!   Modified:
!
!    18 September 2005
!
!   Author:
!
!    John Burkardt
!
!   Parameters:
!
!    Output, integer, the free unit number.
!---------------------------------------------------------------------------------------------                   


!- local variables: --------------------------------------------------------------------------
   integer(Ikind)       :: i,ios
   logical              :: lopen
!---------------------------------------------------------------------------------------------                   
 
   iunit = 0

   do i = 1, 99

      if ( i /= 5 .and. i /= 6 .and. i /= 9 ) then

         inquire ( unit = i, opened = lopen, iostat = ios )

         if ( ios == 0 ) then
            if ( .not. lopen ) then
               iunit = i
               return
            end if
         end if

      end if

   end do

   return
  
   END SUBROUTINE util_get_unit


!=============================================================================================
   SUBROUTINE util_timestamp ( funit )
!=============================================================================================
   integer(Ikind), intent(in) :: funit
!---------------------------------------------------------------------------------------------                   
!  TIMESTAMP prints the current YMDHMS date as a time stamp.
!
!   Example:
!
!     31 May 2001   9:45:54.872 AM
!
!    Licensing:
!
!     This code is distributed under the GNU LGPL license.
!
!    Modified:
!
!     18 May 2013
!
!    Author:
!
!     John Burkardt
!
!    Parameters:
!
!     None
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   character(len=8 ) :: ampm
   integer  (Ikind ) :: d, h, m, mm, n, s, y
   integer           :: values(8)
  
   character(len=9), parameter, dimension(12) :: month =   (/     &
        'January  ', 'February ', 'March    ', 'April    ',       &
        'May      ', 'June     ', 'July     ', 'August   ',       &
        'September', 'October  ', 'November ', 'December ' /)  
!---------------------------------------------------------------------------------------------                   

   call date_and_time ( values = values )

   y  = values(1); m  = values(2); d  = values(3); h  = values(5)
   n  = values(6); s  = values(7); mm = values(8)

   if ( h < 12 ) then
      ampm = 'AM'
   else if ( h == 12 ) then
      if ( n == 0 .and. s == 0 ) then
         ampm = 'Noon'
      else
         ampm = 'PM'
      end if
   else
      h = h - 12
      if ( h < 12 ) then
         ampm = 'PM'
      else if ( h == 12 ) then
         if ( n == 0 .and. s == 0 ) then
            ampm = 'Midnight'
         else
            ampm = 'AM'
         end if
      end if
   end if

   write ( funit, '(3x,i2.2,1x,a,1x,i4,2x,i2,a1,i2.2,a1,i2.2,a1,i3.3,1x,a)' ) &
          d, trim ( month(m) ), y, h, ':', n, ':', s, '.', mm, trim ( ampm )

   return
  
   END SUBROUTINE util_timestamp


!=============================================================================================
   FUNCTION util_s_begin ( s1, s2 ) result(s_begin)
!=============================================================================================
   character (len=* ), intent(in) :: s1, s2
   logical   (kind=4)             :: s_begin
!---------------------------------------------------------------------------------------------                   
!  S_BEGIN is TRUE if one string matches the beginning of the other.
!
!  Discussion:
!
!     The strings are compared, ignoring blanks, spaces and capitalization.
!
!  Example:
!
!       S1              S2      S_BEGIN
!
!      'Bob'          'BOB'     TRUE
!      '  B  o b '    ' bo b'   TRUE
!      'Bob'          'Bobby'   TRUE
!      'Bobo'         'Bobb'    FALSE
!      ' '            'Bob'     FALSE    (Do not allow a blank to match
!                                         anything but another blank string.)
!  Licensing:
!
!     This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!     20 January 2000
!
!  Author:
!
!     John Burkardt
! 
!  Parameters:
!
!     Input, character ( len = * ) S1, S2, the strings to be compared.
!
!     Output, logical ( kind = 4 ) S_BEGIN, is TRUE if the strings match up to
!     the end of the shorter string, ignoring case.
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   integer (kind=4) :: i1, i2, s1_length, s2_length
!---------------------------------------------------------------------------------------------                   

   s1_length = len_trim ( s1 );  s2_length = len_trim ( s2 )
!
!  If either string is blank, then both must be blank to match.
!  Otherwise, a blank string matches anything, which is not
!  what most people want.
!
   if ( s1_length == 0 .or. s2_length == 0 ) then

      if ( s1_length == 0 .and. s2_length == 0 ) then
         s_begin = .true.
      else
         s_begin = .false.
      end if

      return

   end if

   i1 = 0; i2 = 0
!
!  Find the next nonblank in S1.
!
   do

     do

       i1 = i1 + 1

       if ( s1_length < i1 ) then
          s_begin = .true.
          return
       end if

       if ( s1(i1:i1) /= ' ' ) exit

     end do
!
!    Find the next nonblank in S2.
!
     do

       i2 = i2 + 1

       if ( s2_length < i2 ) then
           s_begin = .true.
           return
       end if

       if ( s2(i2:i2) /= ' ' ) exit

     end do
!
!    If the characters match, get the next pair.
!
     if ( .not. util_ch_eqi ( s1(i1:i1), s2(i2:i2) ) ) exit

   end do

   s_begin = .false.

   return

   END FUNCTION util_s_begin


!=============================================================================================
   FUNCTION util_ch_eqi ( c1, c2 ) result(ch_eqi)
!=============================================================================================
   character, intent(in) :: c1, c2
   logical (kind=4)      :: ch_eqi
!---------------------------------------------------------------------------------------------                   
!  CH_EQI is a case insensitive comparison of two characters for equality.
!
!  Example:
!
!     CH_EQI ( 'A', 'a' ) is .TRUE.
!
!  Licensing:
!
!     This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!     28 July 2000
!
!  Author:
!
!     John Burkardt
!
!  Parameters:
!
!     Input, character C1, C2, the characters to compare.
!
!     Output, logical ( kind = 4 ) CH_EQI, the result of the comparison.
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   character :: c1_cap, c2_cap
!---------------------------------------------------------------------------------------------                   

   c1_cap = c1; c2_cap = c2
   
   call util_ch_cap ( c1_cap ); call util_ch_cap ( c2_cap )

   if ( c1_cap == c2_cap ) then
      ch_eqi = .true.
   else
      ch_eqi = .false.
   end if

   return

   END FUNCTION util_ch_eqi


!=============================================================================================
   SUBROUTINE util_ch_cap ( ch )
!=============================================================================================
   character, intent(in out) :: ch
!---------------------------------------------------------------------------------------------                     
!  CH_CAP capitalizes a single character.
!
!  Discussion:
!
!     Instead of CHAR and ICHAR, we now use the ACHAR and IACHAR functions,
!     which guarantee the ASCII collating sequence.
!
!  Licensing:
!
!     This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!     19 July 1998
!
!  Author:
!
!     John Burkardt
!
!  Parameters:
!
!     Input/output, character CH, the character to capitalize.
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   integer (kind=4) :: itemp
!---------------------------------------------------------------------------------------------                   

   itemp = iachar ( ch )

   if ( 97 <= itemp .and. itemp <= 122 ) ch = achar ( itemp - 32 )

   return

   END SUBROUTINE util_ch_cap


!=============================================================================================
   SUBROUTINE util_ch_low ( ch )
!=============================================================================================
   character, intent(in out) :: ch
!---------------------------------------------------------------------------------------------                     
!  ch_low lowercase a single character
!
!  Parameters:
!
!     Input/output, character ch, the character to lowercase.
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   integer (kind=4) :: itemp
!---------------------------------------------------------------------------------------------                   

   itemp = iachar ( ch )

   if ( 65 <= itemp .and. itemp <= 90 ) ch = achar ( itemp + 32 )

   return

   END SUBROUTINE util_ch_low


!=============================================================================================
   FUNCTION util_string_cap ( str ) result(string_cap)
!=============================================================================================
   character(len=*), intent(in ) :: str
   character(len=len(str))       :: string_cap  
!---------------------------------------------------------------------------------------------                   
!  string_cap capitalizes a string.
!  external routine used: ch_cap
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   character         :: c
   integer  (kind=4) :: i
!---------------------------------------------------------------------------------------------                   
  
   string_cap = ''
  
   do i = 1, len(trim(str))
      c = str(i:i)
      call util_ch_cap(c)
      string_cap(i:i) = c
   end do
  
   END FUNCTION util_string_cap  


!=============================================================================================
   FUNCTION util_string_low ( str ) result(string_low)
!=============================================================================================
   character(len=*), intent(in ) :: str
   character(len=len(str))       :: string_low
!---------------------------------------------------------------------------------------------  
!---------------------------------------------------------------------------------------------                   
!  string_low lowercases a string.
!  external routine used: ch_low
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   character         :: c
   integer  (kind=4) :: i
!---------------------------------------------------------------------------------------------                   
  
   string_low = ''
 
   do i = 1, len(trim(str))
      c = str(i:i)
      call util_ch_low(c)
      string_low(i:i) = c
   end do
  
   END FUNCTION util_string_low  


!=============================================================================================
   FUNCTION util_removesp ( str ) result(removesp)
!=============================================================================================
   character(len=*), intent(in) :: str
   character(len=len(str))      :: removesp
!---------------------------------------------------------------------------------------------                   
!   Removes spaces, tabs, and control characters in string str
!   Source: George Benthien (http://www.gbenthien.net)
!---------------------------------------------------------------------------------------------                   

!- local variables: --------------------------------------------------------------------------
   character(len=1)             :: ch
   character(len=len_trim(str)) :: outstr
   integer                      :: i, k, lenstr, ich
!---------------------------------------------------------------------------------------------

   removesp = adjustl(str) ; lenstr = len_trim(removesp)
   outstr=' '; k=0

   do i = 1, lenstr
      ch = removesp(i:i)
      ich = iachar(ch)
      select case(ich)    
         case(0:32)  ! space, tab, or control character
            cycle       
         case(33:)  
            k = k + 1
            outstr(k:k)=ch
      end select
   end do

   removesp = adjustl(outstr)

   END FUNCTION util_removesp    


!=============================================================================================
   SUBROUTINE util_Ialloc1 ( mode, t, n, stat )
!=============================================================================================
   character(len=1), intent(in    )              :: mode
   integer  (Ikind), intent(in    )              :: n
   integer  (Ikind), intent(in out), allocatable :: t(:)
   type     (err_t), intent(in out)              :: stat 
!--------------------------------------------------------------------------------------------- 
!  This procedure allocate or re-allocate the 1d integer array t. It is specially useful when  
!  t is already allocated but needs to be extended.
! 
! * If mode = 's' (s for 'save'), t is saved in a temporary array, de-allocated and re-allocated 
!   with the new size (n). The saved entries are then copied back to t.
!
!   Warning: if the new size n is less than the old one, the part of t that is located beyond n
!            will be definitively lost. 
!
! * If mode = 'e' (e for 'erase'), t is simply de-allocated and re-allocated without a copy of 
!   its possible previous values.
! 
!
!  Inputs
!
!   mode: 's' or 'e'
!      n: the (new) size of t
!      t: array of integers to be allocated or re-allocated with the new size n
!-----------------------------------------------------------------------------------R.H. 12/16  
 
!- locals variables --------------------------------------------------------------------------  
   integer  (Ikind)              :: nold, ierr
   integer  (Ikind), allocatable :: tmp(:)
!--------------------------------------------------------------------------------------------- 
   
   ierr = 0
   
   if (.not. allocated(t)) then
      allocate(t(n),stat=ierr)
      if (ierr == 0) t = 0_Ikind

   else if (mode == 's') then
      nold = size(t)
      allocate(tmp(nold),stat=ierr)
      if (ierr == 0) then
         tmp = t ; deallocate(t) 
         allocate(t(n),stat=ierr)     
         if (ierr == 0) then    
            t = 0_Ikind
            t(1:min(nold,n)) = tmp(1:min(nold,n)); deallocate(tmp)
         end if
      end if

   else if (mode == 'e') then
      deallocate(t)
      allocate(t(n),stat=ierr)  
      if (ierr == 0) t = 0_Ikind
      
   else
      stat = err_t(stat=UERROR,where='util_Ialloc1',msg='Invalid mode '//trim(mode))
      return
   end if  
   
   if (ierr /= 0) then
      stat = err_t(stat=IERROR,where='util_Ialloc1',msg='Allocation failure')
      return
   end if
           
      
   END SUBROUTINE util_Ialloc1


!=============================================================================================
   SUBROUTINE util_Ialloc2 (mode, t, n, m, stat)
!=============================================================================================
   character(len=1), intent(in    )              :: mode
   integer  (Ikind), intent(in    )              :: n, m
   integer  (Ikind), intent(in out), allocatable :: t(:,:)
   type     (err_t), intent(in out)              :: stat 
!--------------------------------------------------------------------------------------------- 
!  This procedure allocate or re-allocate the 2d integer array t. It is specially useful when  
!  t is already allocated but needs to be extended. 
!
! * If mode = 's', t is saved in a temporary array, de-allocated and re-allocated with the new
!   size (n,m). The saved entries are then copied back to t.
!
!  Warning: if one of the new sizes (n or m) is less than its old value, the part of t that
!            is located beyond this size will be definitively lost. 
!
! * If mode = 'e' (e for 'erase'), t is simply de-allocated and re-allocated without a copy of 
!   its possible previous values.
!
!
!  Inputs
!
!     mode: 's' or 'e'
!      n,m: the (new) sizes of t
!        t: array of integers to be allocated or re-allocated with the new sizes (n,m)
!-----------------------------------------------------------------------------------R.H. 12/16 
   
!- locals variables --------------------------------------------------------------------------  
   integer  (Ikind)              :: nold, mold, ierr
   integer  (Ikind), allocatable :: tmp(:,:)
!--------------------------------------------------------------------------------------------- 
   
   ierr = 0
   
   if (.not. allocated(t)) then
      allocate(t(n,m),stat=ierr)
      if (ierr == 0) t = 0_Ikind
  
   else if (mode == 's') then
      nold = size(t,1) ; mold = size(t,2)            
      allocate(tmp(n,mold),stat=ierr)
      if (ierr == 0) then
         tmp = t ; deallocate(t) 
         allocate(t(n,m),stat=ierr)     
         if (ierr == 0) then 
            t = 0_Ikind
            t(1:min(nold,n),1:min(mold,m)) = tmp(1:min(nold,n),1:min(mold,m))
            deallocate(tmp)
         end if
      end if
  
   else if (mode == 'e') then
      deallocate(t)
      allocate(t(n,m),stat=ierr)  
      if (ierr == 0) t = 0_Ikind
      
   else
      stat = err_t(stat=UERROR,where='util_Ialloc2',msg='Invalid mode '//trim(mode))
      return
   end if  
   
   if (ierr /= 0) then
      stat = err_t(stat=IERROR,where='util_Ialloc2',msg='Allocation failure')
      return
   end if      
      
   END SUBROUTINE util_Ialloc2


!=============================================================================================
   SUBROUTINE util_Ralloc1 (mode, t, n, stat)
!=============================================================================================
   character(len=1), intent(in    )              :: mode   
   integer  (Ikind), intent(in    )              :: n
   real     (Rkind), intent(in out), allocatable :: t(:)
   type     (err_t), intent(in out)              :: stat 
!--------------------------------------------------------------------------------------------- 
! (see util_Ialloc1)
!-----------------------------------------------------------------------------------R.H. 12/16
   
!- locals variables --------------------------------------------------------------------------  
   integer  (Ikind)              :: nold,  ierr
   real     (Rkind), allocatable :: tmp(:)
!--------------------------------------------------------------------------------------------- 

   ierr = 0
      
   if (.not. allocated(t)) then
      allocate(t(n),stat=ierr)
      if (ierr == 0) t = 0.0e0_Rkind      
  
   else if (mode == 's') then
      nold = size(t)
      allocate(tmp(nold),stat=ierr)
      if (ierr == 0) then
         tmp = t ; deallocate(t) 
         allocate(t(n),stat=ierr)
         if (ierr == 0) then   
            t = 0.0e0_Rkind
            t(1:min(nold,n)) = tmp(1:min(nold,n)); deallocate(tmp)
         end if
      end if
  
   else if (mode == 'e') then
      deallocate(t)
      allocate(t(n),stat=ierr)  
      if (ierr == 0) t = 0.0e0_Rkind
  
   else
      stat = err_t(stat=UERROR,where='util_Ralloc1',msg='Invalid mode '//trim(mode))
      return
   end if  
   
   if (ierr /= 0) then
      stat = err_t(stat=IERROR,where='util_Ralloc1',msg='Allocation failure')
      return
   end if     
         
   END SUBROUTINE util_Ralloc1


!=============================================================================================
   SUBROUTINE util_Ralloc2 (mode, t, n, m, stat)
!=============================================================================================
   character(len=1), intent(in    )              :: mode      
   integer  (Ikind), intent(in    )              :: n, m
   real     (Rkind), intent(in out), allocatable :: t(:,:)
   type     (err_t), intent(in out)              :: stat 
!--------------------------------------------------------------------------------------------- 
! (see util_Ialloc2)
!-----------------------------------------------------------------------------------R.H. 12/16 
   
!- locals variables --------------------------------------------------------------------------  
   integer  (Ikind)              :: nold, mold, ierr
   real     (Rkind), allocatable :: tmp(:,:)
!--------------------------------------------------------------------------------------------- 
   
   ierr = 0
      
   if (.not. allocated(t)) then
      allocate(t(n,m),stat=ierr)
      if (ierr == 0) t = 0.0e0_Rkind        
          
   else if (mode == 's') then   
      nold = size(t,1) ; mold = size(t,2)        
      allocate(tmp(n,mold),stat=ierr)
      if (ierr == 0) then
         tmp = t ; deallocate(t) 
         allocate(t(n,m),stat=ierr)
         if (ierr == 0) then   
            t = 0.0e0_Rkind      
            t(1:min(nold,n),1:min(mold,m)) = tmp(1:min(nold,n),1:min(mold,m))
            deallocate(tmp)
         end if
      end if
   
   else if (mode == 'e') then
      deallocate(t)
      allocate(t(n,m),stat=ierr)      
      if (ierr == 0) t = 0.0e0_Rkind
   
   else
      stat = err_t(stat=UERROR,where='util_Ralloc2',msg='Invalid mode '//trim(mode))
      return
   end if  
   
   if (ierr /= 0) then
      stat = err_t(stat=IERROR,where='util_Ralloc2',msg='Allocation failure')
      return
   end if           
      
   END SUBROUTINE util_Ralloc2

!=============================================================================================
   SUBROUTINE util_Calloc1 (mode, t, n, lstr, stat)
!=============================================================================================
   character(len=1   ), intent(in    )              :: mode         
   integer  (Ikind   ), intent(in    )              :: n, lstr
   character(len=lstr), intent(in out), allocatable :: t(:)
   type     (err_t   ), intent(in out)              :: stat 
!--------------------------------------------------------------------------------------------- 
! (see util_Ialloc1)
!-----------------------------------------------------------------------------------R.H. 12/16
   
!- locals variables --------------------------------------------------------------------------  
   integer  (Ikind   )              :: nold, ierr
   character(len=lstr), allocatable :: tmp(:)
!--------------------------------------------------------------------------------------------- 

   ierr = 0
         
   if (.not. allocated(t)) then
      allocate(character(len=lstr)::t(n),stat=ierr)
      if (ierr == 0) t = ''

   else if (mode == 's') then
      nold = size(t)
      allocate(character(len=lstr)::tmp(nold),stat=ierr)
      if (ierr == 0) then  
         tmp = t ; deallocate(t) 
         allocate(character(len=lstr)::t(n),stat=ierr)
         if (ierr == 0) then 
            t = ''
            t(1:min(nold,n)) = tmp(1:min(nold,n)); deallocate(tmp)
         end if
      end if

   else if (mode == 'e') then
      deallocate(t)
      allocate(character(len=lstr)::t(n),stat=ierr)
      if (ierr == 0) t = ''
       
   else
      stat = err_t(stat=UERROR,where='util_Calloc1',msg='Invalid mode '//trim(mode))
      return
   end if  
   
   if (ierr /= 0) then
      stat = err_t(stat=IERROR,where='util_Calloc1',msg='Allocation failure')
      return
   end if          
      
   END SUBROUTINE util_Calloc1


!=============================================================================================
   SUBROUTINE util_Calloc2 (mode, t, n, m, lstr, stat)
!=============================================================================================
   character(len=1   ), intent(in    )              :: mode         
   integer  (Ikind   ), intent(in    )              :: n, m, lstr
   character(len=lstr), intent(in out), allocatable :: t(:,:)
   type     (err_t   ), intent(in out)              :: stat 
!--------------------------------------------------------------------------------------------- 
! (see util_Ialloc2)
!---------------------------------------------------------------------------------- R.H. 12/16
   
!- locals variables --------------------------------------------------------------------------  
   integer  (Ikind   )              :: nold, mold, ierr
   character(len=lstr), allocatable :: tmp(:,:)
!--------------------------------------------------------------------------------------------- 

   ierr = 0
   
   if (.not. allocated(t)) then
      allocate(character(len=lstr)::t(n,m),stat=ierr)
      if (ierr == 0) t = ''      

   else if (mode == 's') then
      nold = size(t,1) ; mold = size(t,2)
      allocate(character(len=lstr)::tmp(nold,mold),stat=ierr)
      if (ierr == 0) then  
         tmp = t ; deallocate(t) 
         allocate(character(len=lstr)::t(n,m),stat=ierr)
         if (ierr == 0) then 
            t = ''
            t(1:min(nold,n),1:min(mold,m)) = tmp(1:min(nold,n),1:min(mold,m))
            deallocate(tmp)
         end if
      end if

   else if (mode == 'e') then
      deallocate(t)
      allocate(character(len=lstr)::t(n,m),stat=ierr)
      if (ierr == 0) t = ''
      
   else
      stat = err_t(stat=UERROR,where='util_Calloc2',msg='Invalid mode '//trim(mode))
      return
   end if  
   
   if (ierr /= 0) then
      stat = err_t(stat=IERROR,where='util_Calloc2',msg='Allocation failure')
      return
   end if          
      
   END SUBROUTINE util_Calloc2
   

!=============================================================================================
   SUBROUTINE util_FindSubstring1 (loc, n, str, lind, rind, substr, stat)
!=============================================================================================  
   integer  (Ikind),              intent(in    ) :: lind, rind
   character(len=*),              intent(in    ) :: str, substr
   integer  (Ikind),              intent(   out) :: n   
   integer  (Ikind), allocatable, intent(   out) :: loc(:)
   type     (err_t),              intent(   out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Finds in the string "str" and between indices "lind" and "rind", the occurences (n) of the 
!  substring "substr". Stores its different locations in the array "loc". 
!-----------------------------------------------------------------------------------R.H. 01/18 

!- local variables ---------------------------------------------------------------------------
   character(len=* ), parameter   :: HERE = 'util_FindSubstring1'
   integer  (Ikind )              :: err, len_substr, len_str, p, lind1
   integer  (Ikind ), allocatable :: tmp(:)
   character(len=20)              :: lcnum, rcnum
!--------------------------------------------------------------------------------------------- 
   
   n = 0 ; err = 0

   len_str = len(str) ; len_substr = len(substr)   
   
   if ( len_str == 0 .or. len_substr == 0 ) return

   if ( lind > rind ) then
      write(lcnum,'(i0)')lind ; write(rcnum,'(i0)')rind
      stat = err_t ( stat = UERROR, where = HERE,                &
                     msg = 'Left index (= '           //         &
                     trim(lcnum) // ') > Right index (= ' //     &
                     trim(rcnum) // ') for str = ' // trim(str)  )
      return
   end if
   
   if ( rind < len_str ) len_str = rind
   
   allocate(loc(len_str - lind + 1), source=0_Ikind, stat=err)
      
   if ( err /= 0 ) then
      stat = err_t (stat = IERROR, where = HERE, msg = 'Allocation failure for array "loc"')
      return  
   end if   

   lind1 = lind
   do while ( lind1 <= len_str )
      p = index(str(lind1:len_str),substr)
      if ( p == 0 ) exit
      n = n + 1
      loc(n) = lind1 + p - 1
      lind1 = loc(n) + len_substr
   end do
   
   allocate(tmp(n), stat=err)
   
   if ( err /= 0 ) then
      stat = err_t (stat = IERROR, where = HERE, msg = 'Allocation failure for array "tmp"')
      return   
   end if   
   
   tmp = loc(1:n) ; call move_alloc(from=tmp, to=loc)  
            
   END SUBROUTINE util_FindSubstring1
   

!=============================================================================================
   SUBROUTINE util_FindSubstring2 (loc, n, str, lind, rind, substr, opcl, stat)
!=============================================================================================  
   integer  (Ikind),              intent(in    ) :: lind, rind
   character(len=*),              intent(in    ) :: str, substr, opcl
   integer  (Ikind),              intent(   out) :: n   
   integer  (Ikind), allocatable, intent(   out) :: loc(:)
   type     (err_t),              intent(   out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Same as util_FindSubstring1 except that the locations of "substr" surrounded by a pair of 
!  symbols given in "opcl" are not taken into account.
!-----------------------------------------------------------------------------------R.H. 03/19 

!- local variables ---------------------------------------------------------------------------
   character(len=* ), parameter   :: HERE = 'util_FindSubstring2'
   integer  (Ikind )              :: err, len_substr, len_str, p, lind1, k, k1, k2, pos, lopcl
   integer  (Ikind ), allocatable :: tmp(:)
   character(len=20)              :: lcnum, rcnum
   logical                        :: is_enclosed
!--------------------------------------------------------------------------------------------- 

   lopcl = len_trim(opcl)
   
   if ( modulo(lopcl,2_Ikind) /= 0 ) then
      stat = err_t ( stat = IERROR, where = HERE, msg =                                  &
                     'Number of characters in opcl must be even (opcl='//trim(opcl)//')' )
      return
   end if      
      
   lopcl = lopcl / 2      
   
   n = 0 ; err = 0

   len_str = len(str) ; len_substr = len(substr)   
   
   if ( len_str == 0 .or. len_substr == 0 ) return

   if ( lind > rind ) then
      write(lcnum,'(i0)')lind ; write(rcnum,'(i0)')rind
      stat = err_t ( stat = UERROR, where = HERE,                &
                     msg = ' Left index (= ' //                  &
                     trim(lcnum) // ') > Right index (= ' //     &
                     trim(rcnum) // ') for str = ' // trim(str) )
      return
   end if
   
   if ( rind < len_str ) len_str = rind
   
   allocate(loc(len_str - lind + 1), source=0_Ikind, stat=err)
      
   if ( err /= 0 ) then
      stat = err_t (stat = IERROR, where = HERE, msg = 'Allocation failure for array "loc"')
      return  
   end if      

   lind1 = lind
   do while ( lind1 <= len_str )
   
      p = index(str(lind1:len_str),substr) ! position in the substring 
      
      if ( p == 0 ) exit

      pos = lind1 + p - 1 ! position in the entire string "str"

      is_enclosed = .false.
      do k = 1, lopcl
         k2 = 2*k ; k1 = k2 - 1
         is_enclosed = util_IsEnclosed (str, pos, opcl(k1:k1), opcl(k2:k2), stat)
         error_TraceNreturn(stat>IZERO, HERE, stat) 
         if ( is_enclosed ) exit
      end do        
      
      if ( .not. is_enclosed ) then      
         n = n + 1
         loc(n) = pos
      end if
         
      lind1 = pos + len_substr ! the next substring to examine start at lind1
   end do
   
   allocate(tmp(n), stat=err)
   
   if ( err /= 0 ) then
      stat = err_t (stat = IERROR, where = HERE, msg = 'Allocation failure for array "tmp"')
      return   
   end if   
   
   tmp = loc(1:n) ; call move_alloc(from=tmp, to=loc)  
            
   END SUBROUTINE util_FindSubstring2      
   

!=============================================================================================
   FUNCTION util_IsEnclosed ( str, p, substr1, substr2, stat ) result ( is_enclosed )
!=============================================================================================
   character(len=*),           intent(in    ) :: str, substr1, substr2
   integer  (Ikind),           intent(in    ) :: p
   type     (err_t),           intent(   out) :: stat   
   logical                                    :: is_enclosed
!--------------------------------------------------------------------------------------------- 
!  See if the "p" th (p > 1) character of the string "str" is enclosed between substrings 
!  "substr1" and "substr2"
!
!   Inputs:
!    . str             : the string
!    . p               : the position in "str" of the character to be analysed
!    . substr1, substr2: the two substrings 
!
!   Outputs:
!    . is_enclosed: is .true. if the p-th character is enclosed between "substr1" and "substr2"
!    . stat       : status error object
!
!   External procedures:
!    . util_FindSubstring1
!
!   Examples:
!
!        1)   str = ((a(b)c)d(e)f)                 2)  str = --abcdef***
!                           |                                     |
!                          p=9                                   p=6
!
!             substr1 = (                             substr1 = --
!             substr2 = )                             substr2 = ***
!
!             is_enclosed = .true.                    is_enclosed = .true.
!
!        3)   str = "abc" + "ef"                   4)  str = "abc + ef"
!                         |                                       |
!                        p=7                                     p=6
!
!             substr1 = substr2 = "                    substr1 = substr2 = " 
!
!             is_enclosed = .false.                    is_enclosed = .true.
!-----------------------------------------------------------------------------------R.H. 01/18 

!- local variables ---------------------------------------------------------------------------  
   character(len=*), parameter   :: HERE = 'util_IsEnclosed'
   character(len=:), allocatable :: left, right, s1, s2
   integer  (Ikind), allocatable :: loc(:)
   integer  (Ikind)              :: lleft , nleft1 , nleft2 , nleft
   integer  (Ikind)              :: lright, nright1, nright2, nright
!--------------------------------------------------------------------------------------------- 
      
   is_enclosed = .false.
   
   if ( p <= 1 ) return
   
   s1 = trim(adjustl(substr1)) ; s2 = trim(adjustl(substr2))
!
!- occurence of "substr1" in the left part of the "str":
!   
   left = trim(adjustl(str(:p-1))) ; lleft = len_trim(left)  

   call util_FindSubstring1 (loc, nleft1, left, 1_Ikind, lleft, s1, stat)   

   error_TraceNreturn(stat>IZERO, HERE, stat) 
!
!- occurence of "substr1" in the right part of the "str":
!  
   right = trim(adjustl(str(p+1:))) ; lright = len_trim(right)  

   call util_FindSubstring1 (loc, nright1, right, 1_Ikind, lright, s1, stat)  
   
   error_TraceNreturn(stat>IZERO, HERE, stat) 
!
!- case substr1 = substr2:
!
   if ( s1 == s2 ) then
     
      if ( modulo(nleft1,2_Ikind) /= 0 .and. modulo(nright1,2_Ikind) /= 0 ) &
         is_enclosed = .true.
!
!- case string1 <> string2:
!  
   else      
!
!-    occurence of "substr2" in the left part of the "str":
!            
      call util_FindSubstring1 (loc, nleft2, left, 1_Ikind, lleft, s2, stat)
      
      error_TraceNreturn(stat>IZERO, HERE, stat) 
      
      nleft = nleft1 - nleft2
!
!-    occurence of "substr2" in the right part of the "str":
!      
      call util_FindSubstring1 (loc, nright2, right, 1_Ikind, lright, s2, stat)
      
      error_TraceNreturn(stat>IZERO, HERE, stat) 

      nright = nright1 - nright2      
   
      if (nleft == -nright .and. nleft > 0) is_enclosed = .true.
   end if
   
   if ( allocated(loc) ) deallocate(loc)
         
   END FUNCTION util_IsEnclosed       
   

!=============================================================================================
   FUNCTION util_IsBalanced1 ( str, op, cl ) result ( is_balanced )
!=============================================================================================
   character(len=*), intent(in) :: str
   character(len=1), intent(in) :: op, cl
   logical                      :: is_balanced
!--------------------------------------------------------------------------------------------- 
!  Determines whether the opening/closing symbols "op" and "cl" (in this order) are balanced
!  in the string "str".
!
!   Inputs:
!    . str: the string to analyze
!    . op : opening symbol (1-char)
!    . cl : closing symbol (1-char)
!
!   Outputs:
!    . is_balanced: is .true. if the symbols are balanced
!
!   External procedures: none
!
!   Notes:
!    . Source: adapted (slightly modified) from rosettacode.org/wiki 
!    . If op and cl are identical (e.g. op=cl=" or op=cl='), is_balanced = .true. if they are
!      in even numbers 
!-----------------------------------------------------------------------------------R.H. 02/18 

!- local variables ---------------------------------------------------------------------------    
   integer(Ikind) :: i, a
!--------------------------------------------------------------------------------------------- 
    
   is_balanced = .true.
   
   a = 0   
   if ( op == cl)  then
      do i = 1, len_trim(str)
         if (str(i:i) == op) a = a + 1
      end do
      if ( modulo(a,2_Ikind) /= 0 ) is_balanced = .false.
   else
      do i = 1, len_trim(str)
         if ( str(i:i) == op ) a = a + 1
         if ( str(i:i) == cl ) a = a - 1
         is_balanced = is_balanced .and. (a >= 0)
      end do
      is_balanced = is_balanced .and. (a == 0)
   end if

   END FUNCTION util_IsBalanced1


!=============================================================================================
   FUNCTION util_IsBalanced2 ( str, opcl, stat ) result ( is_balanced )
!=============================================================================================
   character(len=*), intent(in    ) :: str, opcl
   type     (err_t), intent(in out) :: stat      
   logical                          :: is_balanced
!--------------------------------------------------------------------------------------------- 
!  Same as util_IsBalanced1 but for a set of pairs of opening/closing symbols given in the
!  string "opcl" (the pairs are [opcl(1:1), opcl(2:2)], [opcl(3:3), opcl(4:4)], and so on).
!
!   Inputs:
!    . str : the string to analyze
!    . opcl: a 0-length or an even-length string containing the opening/closing symbols
!
!   Outputs:
!    . is_balanced: is .true. if the symbols are balanced
!    . stat       : status error object
!
!   External procedures:
!    . util_IsBalanced1
!-----------------------------------------------------------------------------------R.H. 02/18 

!- local variables ---------------------------------------------------------------------------  
   character(len=*), parameter   :: HERE = 'util_IsBalanced2' 
   integer  (Ikind)              :: j, p1, p2, lopcl
!--------------------------------------------------------------------------------------------- 

   is_balanced = .true.
   
   lopcl = len_trim(opcl)
   
   if ( lopcl == 0 ) return
      
   if ( modulo(lopcl,2_Ikind) /= 0 ) then
      stat = err_t ( stat = IERROR, where = HERE, msg =                                   &
                     ' Number of characters in opcl must be even (opcl='//trim(opcl)//')' )
      return
   end if   
      
   lopcl = lopcl / 2

   do j = 1, lopcl
      p2 = 2*j ; p1 = p2 - 1
      is_balanced = is_balanced .and. util_IsBalanced1 ( str, opcl(p1:p1), opcl(p2:p2) )
   end do   
      
   END FUNCTION util_IsBalanced2
   
      
!=============================================================================================
   FUNCTION util_fromVtkCodeToElementName ( vtkCode, stat ) result ( name )
!=============================================================================================
   integer  (Ikind), intent(in    ) :: vtkCode
   character(len=:), allocatable    :: name
   type     (err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Mapping between certain VTK codes and the corresponding element names in this library
!---------------------------------------------------------------------------------- RH 04/23 -   

!- local variables ---------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------   

   select case (vtkCode)
      case ( 3 ) ! VTK_LINE
         name = 'segm02' 
      case ( 21 ) ! VTK_QUADRATIC_EDGE
         name = 'segm03'
      case ( 5 ) ! VTK_TRIANGLE
         name = 'tria03'
      case ( 22 ) ! VTK_QUADRATIC_TRIANGLE
         name = 'tria06'
      case ( 9 ) ! VTK_QUAD 
         name = 'quad04'
      case ( 23 ) ! VTK_QUADRATIC_QUAD
         name = 'quad08'
      case ( 10 ) ! VTK_TETRA
         name = 'tetr04'
      case ( 24 ) ! VTK_QUADRATIC_TETRA
         name = 'tetr10'
      case ( 12 ) ! VTK_HEXAHEDRON
         name = 'hexa08'
      case ( 25 ) ! VTK_QUADRATIC_HEXAHEDRON
         name = 'hexa20'
      case default
         stat = err_t(stat=UERROR,where='util_fromVtkCodeToElementName', &
         msg='Sorry, element corresponding to VTK Code '//util_intToChar(vtkCode) // &
             ' not present in this library')
   end select

   END FUNCTION util_fromVtkCodeToElementName
      

!=============================================================================================
   FUNCTION util_fromElementNameToVtkCode ( name, stat ) result ( vtkCode )
!=============================================================================================
   character(len=*), intent(in    ) :: name
   integer  (Ikind)                 :: vtkCode   
   type     (err_t), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Mapping between certain element names in this library to their VTK code
!---------------------------------------------------------------------------------- RH 04/23 -   

!- local variables ---------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------   
   
   select case ( name )
      case ( 'segm02' ) 
         vtkCode = 3  ! VTK_LINE
      case ( 'segm03' ) 
         vtkCode = 21 ! VTK_QUADRATIC_EDGE
      case ( 'tria03' ) 
         vtkCode = 5  ! VTK_TRIANGLE
      case ( 'tria06' ) 
         vtkCode = 22 ! VTK_QUADRATIC_TRIANGLE
      case ( 'quad04' )  
         vtkCode = 9  ! VTK_QUAD 
      case ( 'quad08' ) 
         vtkCode = 23 ! VTK_QUADRATIC_QUAD
      case ( 'tetr04' ) 
         vtkCode = 10 ! VTK_TETRA
      case ( 'tetr10' ) 
         vtkCode = 24 ! VTK_QUADRATIC_TETRA
      case ( 'hexa08' ) 
         vtkCode = 12 ! VTK_HEXAHEDRON
      case ( 'hexa20' ) 
         vtkCode = 25 ! VTK_QUADRATIC_HEXAHEDRON
      case default
         stat = err_t(stat=UERROR,where='util_fromElementNameToVtkCode', &
         msg='Sorry, no known VTK Code corresponding to element '//trim(adjustl(name)) )
   end select

   END FUNCTION util_fromElementNameToVtkCode
   

!=============================================================================================
   SUBROUTINE util_executeCommandLine ( expr, stat, msg )
!=============================================================================================
   character(len=*),           intent(in    ) :: expr
   type     (err_t),           intent(in out) :: stat
   character(len=*), optional, intent(in    ) :: msg
!---------------------------------------------------------------------------------------------
!  Calls execute_command_line
!---------------------------------------------------------------------------------------------

!- local variables ---------------------------------------------------------------------------
   character(len=*    ), parameter :: HERE = 'util_executeCommandLine'
   character(len=LGSTR)            :: cmdmsg
   integer                         :: exitstat, cmdstat
!---------------------------------------------------------------------------------------------

   if ( present(msg) ) write(*,'(a)') trim(msg)

   cmdmsg = ''  ; exitstat = 0
   call execute_command_line ( expr, exitstat=exitstat, cmdstat=cmdstat, cmdmsg=cmdmsg )

   if ( cmdstat > 0 ) then
      if ( len_trim(cmdmsg ) == 0) then
         cmdmsg = 'Command execution failed '
      else
         cmdmsg = 'Command execution failed with the error "'//trim(cmdmsg)//'"'
      end if
      stat = err_t ( stat=UERROR, where=HERE, msg=cmdmsg )

   else if ( cmdstat < 0 ) then
      stat = err_t ( stat=UERROR, where=HERE, msg='Command line execution not supported' )

   else if (exitstat /= 0) then
      if ( len_trim(cmdmsg) == 0 ) then
         cmdmsg = 'Command execution failed '
      else
         cmdmsg = 'Command execution failed with the error "'//trim(msg)//'"'
      end if
      stat = err_t ( stat=UERROR, where=HERE, msg=cmdmsg )

   end if

   END SUBROUTINE util_executeCommandLine
   
   
!=============================================================================================   
   SUBROUTINE util_end ( stat )
!============================================================================================= 
   use constants_m
   type(err_t), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  Displays an end message, print error/warning if any
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   integer :: n 
!---------------------------------------------------------------------------------------------
 
   call util_printTimer  
!
!- Print error messages if any and the number of warnings 
!
   if ( stat <= IZERO ) then
   
      call stat%addMsg ( msg = 'Normal termination ', before =.true. ) 
      n = err_ncall(2)
      if ( n == 1 ) then
         call stat%addMsg ( newline = .true., &
                                msg = 'with 1 warning (see message above)' )
      else if ( n > 1 ) then
         call stat%addMsg ( newline = .true., msg = 'with ' // util_intToChar(n) // &
                            ' warnings (see messages above)' )
      end if
      if ( stat == EOF ) stat%has_loc = .false.
      call stat%display ( title = '--> Camef2017 info:', trace = OFF )      
   else
      call stat%display ( title = '--> Camef2017 info:', trace = ON  )
   end if
   
   END SUBROUTINE util_end   
   
       
!=============================================================================================
   SUBROUTINE util_error ( uerr, msg, nmsg, where, warning )
!=============================================================================================
   integer  (Ikind), intent(in), optional :: warning, nmsg, uerr
   character(len=*), intent(in), optional :: msg(*)
   character(len=*), intent(in), optional :: where
!---------------------------------------------------------------------------------------------    
!  Affichage d'un message d'erreur ou d'avertissement. Arret du programme dans le 1er cas
!
!  Si uerr n'est pas present, l'affichage se fait sur la sortie standard, sinon uerr doit
!  correspondre a l'unite logique d'un fichier prealablement ouvert.
!----------------------------------------------------------------------------------- R.H 11/16  

!- local variables: --------------------------------------------------------------------------    
   integer  (Ikind ) :: i, ue
!---------------------------------------------------------------------------------------------            

   if (present(uerr)) then 
     ue = uerr
   else 
     ue = 6
   end if    
   
   write(ue,*)
   if (present(warning)) then 
      if (warning > 0) write(ue,'(a)')'Warning:'   
   else   
      write(ue,'(a)')'Error:'
   end if   
    
   if (present(where)) then
      write(ue,'(a)')'--> in util_'//trim(adjustl(where))
   end if
           
   if (present(msg)) then
      if (present(nmsg)) then
         do i = 1, nmsg
            if (len_trim(msg(i)) > 0) write(ue,'(a)')'--> '//trim(msg(i))
         end do
      else   
         write(ue,'(a)')'--> '//trim(msg(1))
      end if   
   end if      
    
   if (.not. present(warning)) then
      write(ue,'(a,/)')'--> STOP'
      stop
   end if       
      
   write(ue,*)

   END SUBROUTINE util_error
   
END MODULE util_m

