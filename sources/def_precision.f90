MODULE def_precision
	
   private
   public :: Iprec, Rprec, i32, i64, rSP, rDP, rQP
   
        
   integer, parameter :: i32 = SELECTED_INT_KIND(8)
   integer, parameter :: i64 = SELECTED_INT_KIND(15)
   
   integer, parameter :: rSP = SELECTED_REAL_KIND(6,37)
   integer, parameter :: rDP = SELECTED_REAL_KIND(15,307)
   integer, parameter :: rQP = selected_real_kind(33,4931)
    
   integer, parameter :: Iprec = i32, Rprec = rDP

END MODULE def_precision
