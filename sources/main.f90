! Copyright 2017-2025  Riad Hassani, Universite Cote d'Azur
!
! This file is part of the camef code.
!
! The  camef code  is free software:  you can redistribute it and/or modify it under the terms
! of  the  GNU  General  Public  License  as published by the Free Software Foundation, either 
! version 3 of the License, or (at your option) any later version.
!
! The camef code is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;
! without  even the implied warranty of  MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE. 
! See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with the pk2 library.
! If not, see <https://www.gnu.org/licenses/>. 

PROGRAM camef2017

!=============================================================================================
!  code CAMEF2017
!
!  Translation from f77 to f90 and from french to bad english of
!
!  code Camef (Code d'Apprentissage de la Methode des Elements Finis
!               Master 2 Ingenierie Mathematique   
!               "Initiation a la methode des elements finis"
!               R. Hassani, Universite de Savoie, 09/2000)
!
!  The code is organized in several modules:
!
!  def_precision  : to set the precision of real and integer types
!  def_types      : some derived types definition
!  param          : global parameters (of mesh, physical model, ...)
!
!  input          : a set of routines for reading data
!  output         : routines for printing the results in different formats
!
!  element        : a small finite elements library
!  quadrature     : a small quadrature schemes library
!  femsystem      : routines that form and assemble the f.e. system
!
!  models         : a set of routines that solve some physical problems
!                   (you can add your model in this module by providing a routine that
!                   computes the element stiffness matrix and load vector. For more
!                   details, see the introductive part of model.f90)
! 
!  boundarycond   : routines for Dirichlet and Neumann bc
!
!  storagetools   : a set of routines to set up the matrix
!  solverdriver   : contains drivers for the small set of linear methods used
!  sparskit       : a version of SPARSKIT (including iterative solvers)
!  geodynldu      : a LDU direct solver
!  c_fortran_dgssv: a fortran driver for the superlu direct solver  
! 
!  To locate more easily a procedure in the set of these modules, all of them are named in 
!  the following way: "namemodule_nameprocedure". Please, adopt this rule if you implement
!  new subroutines or functions.
!=============================================================================================
   use kindParameters_m

   use param_m
   use input_m
   use storagetools_m, only : storagetools_driver
   use models_m      , only : models_driver
   use util_m        , only : util_end
   use constants_m   , only : ON, OFF, STDOUT
!---------------------------------------------------------------------------------------------   
   implicit none
   
   type(udata_t ) :: mydata 
   type(mesh_t  ) :: mymesh
   type(linsys_t) :: femsys
   type(err_t   ) :: flagerr ! error/warning handler
!---------------------------------------------------------------------------------------------

!
!- Disable automatic halting when an error occur (decision is left to the caller):
!
   call err_SetHaltingMode ( halting = OFF, unit = STDOUT, DisplayWarning = ON )
!
!- Read the data:
!   
   call input_param ( mydata, mymesh, flagerr )
!
!- Set up the matrix according to the storage format:
!   
   call storagetools_driver ( mydata, mymesh, femsys, flagerr )
!
!- Solve the problem and print the solution:
!   
   call models_driver ( mymesh, mydata, femsys, flagerr )
!
!- Report any error:
!
   call util_end ( flagerr )
   
END PROGRAM camef2017

