module output_m

   use kindParameters_m

   use err_m
   use defTypes_m
   use util_m     , only : util_get_unit, i2a => util_intToChar
   use constants_m, only : IZERO, IERROR, UERROR, WARNING, LGSTR, NLT
   
   use param_m
   
   implicit none
   
   !private
   public :: output_driver
   
CONTAINS

!=============================================================================================
   SUBROUTINE output_driver ( mydata, mymesh, femsys, stat, optvar )
!=============================================================================================
   type(udata_t ), intent(in    )           :: mydata
   type(mesh_t  ), intent(in    )           :: mymesh
   type(linsys_t), intent(in    )           :: femsys
   type(err_t   ), intent(in out)           :: stat
   real(Rkind   ), intent(in    ), optional :: optvar(:)
!--------------------------------------------------------------------------------------------- 
!  Driver to call output routines    
!-----------------------------------------------------------------------------------R.H. 12/16  

!- locals variables --------------------------------------------------------------------------      
   integer  (Ikind) :: i, freq
   character(len=9) :: form
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return
   
   if (name_outputtype(1) == 'none') return
   
   do i = 1, size(name_outputtype)

      form = name_outputfmt(i)
      freq = freq_output   (i)
   
      select case ( name_outputtype(i) )

         case ( 'velm' )
!
!-          for printing values (secondary unknowns) at I.P. locations:
!         
            call output_velm ( form, freq, femsys, stat )
                  
         case ( 'velml' )
!
!-          for printing values averaged on element level and smoothed at nodes
!           
            call output_velml ( form, freq, femsys, mymesh, stat )
   
         case ( 'vnod' )
!
!-          for printing nodal values (primary unknowns):
!         
            call output_vnod ( form, freq, femsys, mymesh, stat )

         case ( 'vtkl' )
!
!-          for printing results in vtk format (secondary unknowns smoothed at nodes)
!         
            call output_vtkl ( form, femsys, mymesh, mydata, stat )

         case ( 'vtk' )
!
!-          for printing results in vtk format (secondary unknowns averaged on elements)
!         
            call output_vtk ( form, femsys, mymesh, mydata, stat, optvar) 
        
         case ( 'sparse' )
!
!-          for printing sparsity patern
!
            call output_sparse ( form, femsys, stat )
         
         case ( 'vnodSubSet' )
!
!-          for printing nodal values of a given node(s):
!         
            call output_vnodSubSet ( form, freq, femsys, mymesh, stat )
            
         case ( 'vpntSubSet' )
!
!-          for printing nodal values of a given node(s):
!         
            call output_vpntSubSet ( form, freq, femsys, mymesh, stat )   
            
         case ( 'velmSubSet' )
!
!-          for printing elemental values of a given element(s):
!         
            call output_velmSubSet ( form, freq, femsys, stat )   
                                          
         case ( '' )
                  
         case default
            stat = err_t ( stat = WARNING, where = 'output_driver', msg = &
                           'warning: unknown output format "'//form//'"')
            
      end select
   end do
         
   END SUBROUTINE output_driver

!=============================================================================================
   SUBROUTINE output_sparse ( fmt, femsys, stat )
!=============================================================================================
   type     (linsys_t), intent(in    ) :: femsys
   character(len=*   ), intent(in    ) :: fmt
   type     (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Prints sparsity patern (file sparsity_patern)
!
!  Use for example gnuplot to visualize the patern. Launch gnuplot and then type:
!       set yrange[] reverse
!       plot 'sparsity_patern'
!-----------------------------------------------------------------------------------R.H. 01/17  

!- locals variables --------------------------------------------------------------------------      
   integer  (Ikind)              :: i, j, k, usp, ios
   character(len=:), allocatable :: fn
   integer  (Ikind), save        :: n = 0
   character(len=LGSTR)          :: msg
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return

   n = n + 1
   if (n > 1) return

   fn = trim(name_outputDir) // trim(name_input_file) // '.sparsity_patern'
      
   if (fmt == 'bin') then
      open (newunit = usp, file = fn, form='unformatted', iostat = ios, iomsg = msg)
   else
      open (newunit = usp, file = fn, iostat = ios, iomsg = msg)
   end if      

   if ( ios /= 0 ) then
      stat = err_t ( stat=UERROR, where='output_sparse', msg='Unable to open the file '// &
                     fn // NLT // 'IO-MSG: '//trim(msg) )
      return
   end if
         
   if (verbosity(1) >= 1) write(*,'(1x,a)') &
   'Writing the output file "sparsity_patern in" '//trim(fmt)//' format'   
   
   if (trim(name_storage) == "csr" .or. trim(name_storage) == "csrsym") then
      if (trim(fmt) == "ascii") then
         write(usp,'(a,a )')'# file.par : ',trim(name_input_file)//'.par'
         write(usp,'(a,i0)')'# n, nzero : ',femsys%n
         write(usp,'(a,a )')'# storage  : ',trim(name_storage)
         do i = 1, femsys%n
            do j = femsys%row(i), femsys%row(i+1)-1
               write(usp,*)i,femsys%col(j)
            end do
         end do
      else
         do i = 1, femsys%n
            do j = femsys%row(i), femsys%row(i+1)-1
               write(usp)i,femsys%col(j)
            end do
         end do
      end if
   end if   

   if (trim(name_storage) == "skyline") then
      if (trim(fmt) == "ascii") then 
         write(usp,'(a,a )')'# file.par : ',trim(name_input_file)//'.par'
         write(usp,'(a,i0)')'# n, nzero : ',femsys%n
         write(usp,'(a,a )')'# storage  : ',trim(name_storage)
      end if
      do j = 1, femsys%n
         if (j == 1) then
            if (trim(fmt) == "ascii") write(usp,*)j,j
            if (trim(fmt) == "bin"  ) write(usp)  j,j
         else   
            k = j
            do i = femsys%row(j-1)+1, femsys%row(j)
               if (trim(fmt) == "ascii") write(usp,*)k,j
               if (trim(fmt) == "bin"  ) write(usp)  k,j
               k = k - 1
            end do
         end if
      end do          
   end if    
   
   close(usp)
   
   END SUBROUTINE output_sparse
     
         
!=============================================================================================
   SUBROUTINE output_vnod ( fmt, freq, femsys, mymesh, stat, title )
!=============================================================================================
   character(len=*   ), intent(in    )           :: fmt
   integer  (Ikind   ), intent(in    )           :: freq
   type     (linsys_t), intent(in    )           :: femsys
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in    ), optional :: title
!--------------------------------------------------------------------------------------------- 
!  Prints nodal quantities (file xxxx.n.vnod)
!-----------------------------------------------------------------------------------R.H. 12/16  

!- locals variables --------------------------------------------------------------------------      
   integer  (Ikind )              :: ip, ios
   character(len=: ), allocatable :: ofvnod
   integer  (Ikind ), save        :: ufvnod=0, n=0, num=0
   character(len=LGSTR)           :: msg
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return
   
   n = n + 1
   
   if ( n /= freq ) return
   
   n = 0  ; num = num + 1
!
!- open the file xxxxx.n.vnod, whit xxxx the input file name and n the # of the output
!
   ofvnod = trim(name_outputDir) // trim(name_input_file) // '.' // i2a(num)  //'.vnod'
      
   if (fmt == 'bin') then
      open (newunit = ufvnod, file = ofvnod, form='unformatted', iostat = ios, iomsg = msg)
   else
      open (newunit = ufvnod, file = ofvnod, iostat = ios, iomsg = msg)
   end if      

   if ( ios /= 0 ) then
      stat = err_t ( stat=UERROR, where='output_vnod', msg='Unable to open the file '// &
                     ofvnod // NLT // 'IO-MSG: '//trim(msg) )
      return
   end if
   
   if (verbosity(1) >= 1) write(*,'(1x,a)') &
   'Writing into the output file "'//trim(ofvnod)//'" in '//trim(fmt)//' format'   
   
   if ( fmt == 'bin' ) then
      if ( present(title) ) write(ufvnod)'# '//trim(title)
      write(ufvnod)'# number of lines: npoin = '//i2a(npoin)
      write(ufvnod)'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(ndofn)//')'
   else
      if ( present(title) ) write(ufvnod,'(a)')'# '//trim(title)
      write(ufvnod,'(a)')'# number of lines: npoin = '//i2a(npoin)
      write(ufvnod,'(a)')'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(ndofn)//')'
   end if
   
   
   do ip = 1, npoin
      if (mymesh%lvale(ip) == 0) cycle ! "orphan" nodes
      if (fmt == 'ascii') then
         write(ufvnod,'(*(1x,g0))') mymesh%coord(:,ip), femsys%Sol(:,ip)
      else   
         write(ufvnod)              mymesh%coord(:,ip), femsys%Sol(:,ip)
      end if   
   end do
   
   close(ufvnod)
         
   END SUBROUTINE output_vnod   
   

!=============================================================================================
   SUBROUTINE output_vnodSubSet ( fmt, freq, femsys, mymesh, stat, title )
!=============================================================================================
   character(len=*   ), intent(in    )           :: fmt
   integer  (Ikind   ), intent(in    )           :: freq
   type     (linsys_t), intent(in    )           :: femsys
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in    ), optional :: title
!--------------------------------------------------------------------------------------------- 
!  Prints nodal quantities (file xxxx.n.vnod)
!-----------------------------------------------------------------------------------R.H. 12/16  

!- locals variables --------------------------------------------------------------------------  
   character(len=*), parameter   :: Here = 'output_vnodSubSet'
   integer  (Ikind)              :: i, ip, ios, l, l0
   character(len=:), allocatable :: f
   integer  (Ikind), save        :: n=0, u(size(nod_output))
   character(len=LGSTR)          :: msg
   logical         , save        :: first = .true.
   character(len=80)             :: title_
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return
   
   n = n + 1
   
   if ( first ) then
      
      do i = 1, nNod_output
         ip = nod_output(i)
         
         if ( ip == 0 ) cycle ! ignore invalid node #
         
         f = trim(name_outputDir) // trim(name_input_file) // '.vnod_'//i2a(ip)
         
         if (fmt == 'bin') then
            open (newunit = u(i), file = f, status = 'replace', action = 'write', &
                  iostat = ios, iomsg = msg, form='unformatted')
         else
            open (newunit = u(i), file = f, status = 'replace', action = 'write', &
                  iostat = ios, iomsg = msg)
         end if  
         if ( ios /= 0 ) then
            stat = err_t ( stat=UERROR, where=Here, msg='Unable to open the file '//f//NLT// &
                           'IO-MSG: '//trim(msg) )
            return
         end if         
         
         if ( fmt == 'bin' ) then
            l0 = len(title_)
            if ( present(title) ) then
               l = len(title)
               if ( l > l0-1 ) then
                  title_ = '#'//title(1:l0-1)
               else
                  title_ = '#'//title//repeat(' ',l0-l-1)
               end if
            else
               title_ = '#'//repeat(' ',l0-1)
            end if
            write(u(i))title_ 
            write(u(i))'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(ndofn)//')'
         else
            if ( present(title) ) write(u(i),'(a)')trim(title)
            write(u(i),'(a)')'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(ndofn)//')'
         end if  
         close(u(i))  
      end do
   end if
   
   if ( n /= freq .and. .not. first ) return
   
   if ( first ) first = .false.
   
   if ( n == freq ) n = 0 
   
   do i = 1, nNod_output
      ip = nod_output(i)
      
      if ( ip == 0 ) cycle ! ignore invalid node #

      f = trim(name_outputDir) // trim(name_input_file) // '.vnod_'//i2a(ip)

      if (verbosity(1) >= 1) write(*,'(1x,a)') &
      'Writing into the output file "'//trim(f)//'" in '//trim(fmt)//' format'   
   
      if (fmt == 'bin') then
         open (newunit = u(i), file = f, status='unknown', &
               position='append', iostat = ios, iomsg = msg, form='unformatted')
         if ( ios == 0 ) write(u(i))              mymesh%coord(:,ip), femsys%Sol(:,ip)
      else   
         open (newunit = u(i), file = f, status='unknown', &
               position='append', iostat = ios, iomsg = msg)
         if ( ios == 0 ) write(u(i),'(*(1x,g0))') mymesh%coord(:,ip), femsys%Sol(:,ip)
      end if   
      
      if ( ios /= 0 ) then
         stat = err_t ( stat=UERROR, where=Here, msg='Unable to open the file '//f//NLT// &
                        'IO-MSG: '//trim(msg) )
         return
      end if  
      close(u(i))    
   end do
            
   END SUBROUTINE output_vnodSubSet   


!=============================================================================================
   SUBROUTINE output_vElmSubSet ( fmt, freq, femsys, stat, title )
!=============================================================================================
   character(len=*   ), intent(in    )           :: fmt
   integer  (Ikind   ), intent(in    )           :: freq
   type     (linsys_t), intent(in    )           :: femsys
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in    ), optional :: title
!--------------------------------------------------------------------------------------------- 
!  Prints nodal quantities (file xxxx.n.vnod)
!-----------------------------------------------------------------------------------R.H. 12/16  

!- locals variables --------------------------------------------------------------------------  
   character(len=*), parameter   :: Here = 'output_vElmSubSet'
   integer  (Ikind)              :: i, e, g1, g2, g, ios
   character(len=:), allocatable :: f
   integer  (Ikind), save        :: n=0, u(size(elm_output))
   character(len=LGSTR)          :: msg
   logical         , save        :: first = .true.
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return
   
   n = n + 1
   
   if ( first ) then
      do i = 1, nElm_output
         e = elm_output(i)
         
         if ( e == 0 ) cycle ! ignore invalid element #
         
         f = trim(name_outputDir) // trim(name_input_file) // '.velm_'//i2a(e)
         
         if (fmt == 'bin') then
            open (newunit = u(i), file = f, status = 'replace', action = 'write', &
                  iostat = ios, iomsg = msg, form='unformatted')
         else
            open (newunit = u(i), file = f, status = 'replace', action = 'write', &
                  iostat = ios, iomsg = msg)
         end if  
         if ( ios /= 0 ) then
            stat = err_t ( stat=UERROR, where=Here, msg='Unable to open the file '//f//NLT// &
                           'IO-MSG: '//trim(msg) )
            return
         end if         
         
         if ( fmt == 'bin' ) then
            if ( present(title) ) write(u(i))'# '//trim(title)
            write(u(i))'# ( X(1:'//i2a(ndime)//')_g, Val(1:'//i2a(noutp)//')_g,  for g=1:'//&
                          i2a(nipts)//' )'
         else
            if ( present(title) ) write(u(i),'(a)')'# '//trim(title)
            write(u(i),'(a)')'# ( X(1:'//i2a(ndime)//')_g, Val(1:'//i2a(noutp)// &
                         ')_g,  for g=1:'//i2a(nipts)//' )'
         end if  
         close(u(i))  
      end do
   end if
   
   if ( n /= freq .and. .not. first ) return
   
   if ( first ) first = .false.
   
   if ( n == freq ) n = 0 
   
   do i = 1, nElm_output
      e = elm_output(i)
      
      if ( e == 0 ) cycle ! ignore invalid element #

      f = trim(name_outputDir) // trim(name_input_file) // '.velm_'//i2a(e)

      if (verbosity(1) >= 1) write(*,'(1x,a)') &
      'Writing into the output file "'//trim(f)//'" in '//trim(fmt)//' format'   

      g1 = (e - 1) * nipts + 1 ; g2 = g1 + nipts - 1   
      
      if (fmt == 'bin') then
         open (newunit = u(i), file = f, status='unknown', &
               position='append', iostat = ios, iomsg = msg, form='unformatted')
         if ( ios == 0 ) then
            do g = g1, g2
               write(u(i)) femsys%Str(:,g)
            end do
         end if
      else   
         open (newunit = u(i), file = f, status='unknown', &
               position='append', iostat = ios, iomsg = msg)
         if ( ios == 0 ) then
            do g = g1, g2
               write(u(i),'(*(1x,g0))') femsys%Str(:,g)
            end do
         end if
      end if   
      
      if ( ios /= 0 ) then
         stat = err_t ( stat=UERROR, where=Here, msg='Unable to open the file '//f//NLT// &
                        'IO-MSG: '//trim(msg) )
         return
      end if  
      close(u(i))    
   end do
            
   END SUBROUTINE output_vElmSubSet   


!=============================================================================================
   SUBROUTINE output_vpntSubSet ( fmt, freq, femsys, mymesh, stat, title )
!=============================================================================================
   character(len=*   ), intent(in    )           :: fmt
   integer  (Ikind   ), intent(in    )           :: freq
   type     (linsys_t), intent(in    )           :: femsys
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in    ), optional :: title
!--------------------------------------------------------------------------------------------- 
!  Prints nodal quantities (file xxxx.n.vnod)
!-----------------------------------------------------------------------------------R.H. 12/16  

!- locals variables --------------------------------------------------------------------------  
   character(len=*), parameter   :: Here = 'output_vpntSubSet'
   integer  (Ikind)              :: i, ip, ios, iref
   character(len=:), allocatable :: f
   integer  (Ikind), save        :: n=0, u(size(pnt_output))
   character(len=LGSTR)          :: msg
   logical         , save        :: first = .true.
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return
   
   n = n + 1
   
   if ( first ) then
      do i = 1, nPnt_output
         iref = pnt_output(i,1) ; ip = pnt_output(i,2)
         
         if ( ip == 0 ) cycle ! ignore invalid ref.
         
         f = trim(name_outputDir) // trim(name_input_file) // '.vpnt_'//i2a(iref)
         
         if (fmt == 'bin') then
            open (newunit = u(i), file = f, status = 'replace', action = 'write', &
                  iostat = ios, iomsg = msg, form='unformatted')
         else
            open (newunit = u(i), file = f, status = 'replace', action = 'write', &
                  iostat = ios, iomsg = msg)
         end if  
         if ( ios /= 0 ) then
            stat = err_t ( stat=UERROR, where=Here, msg='Unable to open the file '//f//NLT// &
                           'IO-MSG: '//trim(msg) )
            return
         end if         
         
         if ( fmt == 'bin' ) then
            if ( present(title) ) write(u(i))'# '//trim(title)
            write(u(i))'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(ndofn)//')'
         else
            if ( present(title) ) write(u(i),'(a)')'# '//trim(title)
            write(u(i),'(a)')'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(ndofn)//')'
         end if      
         close(u(i))  
      end do
   end if
   
   if ( n /= freq .and. .not. first ) return
   
   if ( first ) first = .false.
   
   if ( n == freq ) n = 0 
   
   do i = 1, nPnt_output
      iref = pnt_output(i,1) ; ip = pnt_output(i,2)
      
      if ( ip == 0 ) cycle ! ignore invalid ref.
      
      f = trim(name_outputDir) // trim(name_input_file) // '.vpnt_'//i2a(iref)

      if (verbosity(1) >= 1) write(*,'(1x,a)') &
      'Writing into the output file "'//trim(f)//'" in '//trim(fmt)//' format'   
   
      if (fmt == 'bin') then
         open (newunit = u(i), file = f, status='unknown', &
               position='append', iostat = ios, iomsg = msg, form='unformatted')
         if ( ios == 0 ) write(u(i))              mymesh%coord(:,ip), femsys%Sol(:,ip)
      else   
         open (newunit = u(i), file = f, status='unknown', &
               position='append', iostat = ios, iomsg = msg)
         if ( ios == 0 ) write(u(i),'(*(1x,g0))') mymesh%coord(:,ip), femsys%Sol(:,ip)
      end if   
      
      if ( ios /= 0 ) then
         stat = err_t ( stat=UERROR, where=Here, msg='Unable to open the file '//f//NLT// &
                        'IO-MSG: '//trim(msg) )
         return
      end if  
      close(u(i))    
   end do
            
   END SUBROUTINE output_vpntSubSet 
     

!=============================================================================================
   SUBROUTINE output_velm ( fmt, freq, femsys, stat, title )
!=============================================================================================
   character(len=*   ), intent(in    )           :: fmt
   integer  (Ikind   ), intent(in    )           :: freq
   type     (linsys_t), intent(in    )           :: femsys
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in    ), optional :: title
!--------------------------------------------------------------------------------------------- 
!  Prints derived quantities computed at I.P. (file xxxx.n.velm)
!-----------------------------------------------------------------------------------R.H. 12/16 
 
!- locals variables --------------------------------------------------------------------------      
   integer  (Ikind )              :: ip, ios
   character(len=: ), allocatable :: ofvelm
   integer  (Ikind ), save        :: ufvelm=0, n=0, num=0
   character(len=LGSTR)           :: msg
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return

   n = n + 1
   
   if ( n /= freq ) return
   
   n = 0  ; num = num + 1   
!
!- open the file xxxxx.n.velm, whit xxxx the input file name and n the # of the output
!
   ofvelm = trim(name_outputDir) // trim(name_input_file) // '.' // i2a(num)  //'.velm'
      
   if (fmt == 'bin') then
      open (newunit = ufvelm, file = ofvelm, form='unformatted', iostat = ios, iomsg = msg)
   else
      open (newunit = ufvelm, file = ofvelm, iostat = ios, iomsg = msg)
   end if      

   if ( ios /= 0 ) then
      stat = err_t ( stat=UERROR, where='output_velm', msg='Unable to open the file '// &
                     ofvelm // NLT // 'IO-MSG: '//trim(msg) )
      return
   end if      
      
   if (verbosity(1) >= 1) write(*,'(1x,a)') &
   'Writing into the output file "'//trim(ofvelm)//'" in '//trim(fmt)//' format'   

   if ( fmt == 'bin' ) then
      if ( present(title) ) write(ufvelm)'# '//trim(title)
      write(ufvelm)'# number of lines: nelem * nipts = '//i2a(ntotg)
      write(ufvelm)'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(noutp)//')'
   else
      if ( present(title) ) write(ufvelm,'(a)')'# '//trim(title)
      write(ufvelm,'(a)')'# number of lines: nelem * nipts = '//i2a(ntotg)
      write(ufvelm,'(a)')'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(noutp)//')'
   end if
        
   do ip = 1, ntotg
      if (fmt == 'ascii') then
         write(ufvelm,'(*(e13.5,1x))') femsys%Str(:,ip)
      else
         write(ufvelm)                 femsys%Str(:,ip)
      end if   
   end do
   
   close(ufvelm)
         
   END SUBROUTINE output_velm 
   

!=============================================================================================
   SUBROUTINE output_velml ( fmt, freq, femsys, mymesh, stat, title )
!=============================================================================================
   character(len=*   ), intent(in    )           :: fmt
   integer  (Ikind   ), intent(in    )           :: freq
   type     (linsys_t), intent(in    )           :: femsys
   type     (mesh_t  ), intent(in    )           :: mymesh
   type     (err_t   ), intent(in out)           :: stat
   character(len=*   ), intent(in    ), optional :: title
!--------------------------------------------------------------------------------------------- 
!  Prints derived quantities averaged on elements and smoothed at nodes (file xxxx.n.velml)
!-----------------------------------------------------------------------------------R.H. 12/16  

!- locals variables --------------------------------------------------------------------------      
   integer  (Ikind )              :: i, ip, ie, ig, itg, nv, ios 
   real     (Rkind )              :: u(noutp), xipts, xval
   character(len=: ), allocatable :: ofvelm
   integer  (Ikind ), save        :: ufvelm=0, n=0, num=0
   character(len=LGSTR)           :: msg
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return

   n = n + 1
   
   if ( n /= freq ) return
   
   n = 0  ; num = num + 1   
!
!- open the file xxxxx.n.velm, whit xxxx the input file name and n the # of the output
!
   ofvelm = trim(name_outputDir) // trim(name_input_file) // '.' // i2a(num)  //'.velml'
      
   if (fmt == 'bin') then
      open (newunit = ufvelm, file = ofvelm, form='unformatted', iostat = ios, iomsg = msg)
   else
      open (newunit = ufvelm, file = ofvelm, iostat = ios, iomsg = msg)
   end if      

   if ( ios /= 0 ) then
      stat = err_t ( stat=UERROR, where='output_velml', msg='Unable to open the file '// &
                     ofvelm // NLT // 'IO-MSG: '//trim(msg) )
      return
   end if      
!
!- open the file xxxxx.n.velml, whit xxxx the input file name and n the # of the output
!
   if (verbosity(1) >= 1) write(*,'(1x,a)') &
   'Writing into the output file "'//trim(ofvelm)//'" in '//trim(fmt)//' format'   

   if ( fmt == 'bin' ) then
      if ( present(title) ) write(ufvelm)'# '//trim(title)
      write(ufvelm)'# number of lines: npoin = '//i2a(npoin)
      write(ufvelm)'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(noutp)//')'
   else
      if ( present(title) )write(ufvelm,'(a)')'# '//trim(title)
      write(ufvelm,'(a)')'# number of lines: npoin = '//i2a(npoin)
      write(ufvelm,'(a)')'# X(1:'//i2a(ndime)//'), Val(1:'//i2a(noutp)//')'
   end if
        
   xipts = 1.0_Rkind / real(nipts,kind=Rkind)
   
   do ip = 1, npoin
      nv = mymesh%lvale(ip) 
      if (nv == 0) cycle ! "orphan" nodes
      xval = 1.0_Rkind / real(nv,kind=Rkind)
      u = 0.0_Rkind
      do i = 1, nv
         ie = mymesh%lelem(i,ip)
         do ig = 1, nipts
            itg = (ie-1)*nipts + ig
            u = u + femsys%Str(ndime+1:ndime+noutp,itg)*xipts
         end do
      end do
      u = u * xval
      if (fmt == 'ascii') then
         write(ufvelm,'(20e13.5)') mymesh%coord(:,ip), u
      else
         write(ufvelm) mymesh%coord(:,ip), u
      end if   
   end do
   
   close(ufvelm)
         
   END SUBROUTINE output_velml 


!=============================================================================================
   SUBROUTINE output_vtkl ( fmt, femsys, mymesh, mydata, stat )
!=============================================================================================
   character(len=*   ), intent(in    ) :: fmt   
   type     (linsys_t), intent(in    ) :: femsys
   type     (mesh_t  ), intent(in    ) :: mymesh
   type     (udata_t ), intent(in    ) :: mydata
   type     (err_t   ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Prints results in vtk format (version with averaged element values smoothed at nodes)
!-----------------------------------------------------------------------------------R.H. 01/17  

!- locals variables --------------------------------------------------------------------------   
   real     (Rkind ), parameter   :: sqrt2i = 1.0_Rkind / sqrt(2.0_Rkind)   
   integer  (Ikind )              :: i, ip, ie, in, type_cell, lp, elast, stokes, & 
                                     nstre, nval, nv, err
   real     (Rkind )              :: u(ndofn), z(2), s(noutp+10), xval, &
                                     tre, trs, dequiv, sequiv, x
   character(len=80)              :: ofvtk, fm
   integer  (Ikind ), save        :: ufvtk=0, n=0
   real     (Rkind ), allocatable :: str(:,:) 
   character(len=13)              :: var(28)
!--------------------------------------------------------------------------------------------- 

   n = n + 1
!
!- open the file xxxxx.n.vtk, whit xxxx the input file name and n the # of the output
!
   write(ofvtk,'(a,".liss.",i0,".vtk")')trim(adjustl(name_input_file)),n

   call util_get_unit (ufvtk)

   if (fmt == 'bin') then
      open (ufvtk, file = ofvtk, form='unformatted')
   else
      open (ufvtk, file = ofvtk)
   end if      

   if (verbosity(1) >= 1) write(*,'(1x,a)') &
   'Writing the output file "'//trim(ofvtk)//'" in '//trim(fmt)//' format'   

   lp = index(name_model_pb,'(')

   elast = 0; nstre = nstri; nval = nstre; var = ''
   if (name_model_pb(1:lp-1) == 'elasticity') then
      if (index(name_model_pb,'3d') /= 0          ) then 
         elast = 3; 
         var(1:16) = ['epsil_xx     ','epsil_yy     ','epsil_zz     ', &
                      'epsil_xy     ','epsil_yz     ','epsil_zx     ', &
                      'sigma_xx     ','sigma_yy     ','sigma_zz     ', &
                      'sigma_xy     ','sigma_yz     ','sigma_zx     ', &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3', &
                      'sigma_equiv  ']               
                       
         nval = 16                                                                                           
      end if   
       
      if (index(name_model_pb,'plane_strain') /= 0) then
         elast = 2 ; nstre = 4
         var(1:11) = ['epsil_xx     ','epsil_yy     ','epsil_xy     ',                 &
                      'sigma_xx     ','sigma_yy     ','sigma_zz     ','sigma_xy     ', &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3','sigma_equiv  ']
         nval = 11
      end if   
      
      if (index(name_model_pb,'plane_stress') /= 0) then
         elast =-2; nstre = 4 
         var(1:11) = ['epsil_xx     ','epsil_yy     ','epsil_zz     ','epsil_xy     ', &
                      'sigma_xx     ','sigma_yy     ','sigma_xy     ',                 &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3','sigma_equiv  ']
         nval = 11                                   
      end if   
         
      if (index(name_model_pb,'axisymmetry') /= 0 ) then
         elast = 1
         var(1:12) = ['epsil_rr     ','epsil_zz     ','epsil_tt     ','epsil_rz     ', &
                      'sigma_rr     ','sigma_zz     ','sigma_tt     ','sigma_rz     ', &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3','sigma_equiv  ']
         nval = 12                                                 
      end if
   end if

   stokes = 0
   if (name_model_pb(1:lp-1) == 'stokes'  .or. name_model_pb(1:lp-1) == 'stokesnl' .or. &
       name_model_pb(1:lp-1) == 'stokesnl2' ) then
      if (index(name_model_pb,'3d') /= 0) then 
         stokes = 3
         var(1:15) = ['deps_xx     ','deps_yy     ','deps_zz     ', &
                      'deps_xy     ','deps_yz     ','deps_zx     ', &
                      'tau_xx      ','tau_yy      ','tau_zz      ', &
                      'tau_xy      ','tau_yz      ','tau_zx      ', &
                      'log10(visco)','deps_equiv  ','tau_equiv   ']                             
         nval = 15                                                 
      else if (index(name_model_pb,'2d') /= 0) then 
         stokes = 2 ; nstre = 4
         var(1:9) = ['deps_xx     ','deps_yy     ','deps_xy     ', &
                     'tau_xx      ','tau_yy      ','tau_xy      ', &
                     'log10(visco)','deps_equiv  ','tau_equiv   ']                             
         nval = 9                                  
      end if   
   end if   
!
!- compute the averaged values at element level and smooth them at nodes:
!   
   allocate(str(nval,npoin), source = 0.0_Rkind, stat = err)
   
   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where='output_vtl', msg='Allocation failure for "str"')
      return
   end if
   
   do ie = 1, nelem
      call output_elemtavg (s, ie, femsys) ! element average
      
      if (elast /= 0) then
         if (elast == 2) then
            ! plane-strain
            !numat = mymesh % numdom(ie)
            !poiss = mydata % vmatprop(2,numat)
            
            ! strain components: exx=s(1), eyy=s(2), 2*exy=s(3)
            
            s(3) = 0.5_Rkind * s(3) !< exy
            
            ! stress components: sxx=s(4), syy=s(5), sxy=s(6), szz=s(7)
            
            ! Put the tangential stresse at the end:
            x = s(6) ; s(6) = s(7) ; s(7) = x
            
            !s(6) = poiss*(s(4) + s(5)) ! for plane_strain compute szz
            trs = s(4)+s(5)+s(6)
            tre = s(1)+s(2)
            s(8) = tre
            s(9) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + 2.0_Rkind*s(3)**2 - (tre**2)/3.0_Rkind))
            s(10) = trs / 3.0_Rkind
            s(11) = sqrt(1.5_Rkind*(s(4)**2 + s(5)**2 + s(6)**2 + 2.0_Rkind*s(7)**2 - &
                                    (trs**2)/3.0_Rkind) )
         else if (elast ==-2) then
            ! plane-stress
            !numat = mymesh % numdom(ie)
            !poiss = mydata % vmatprop(2,numat)
            
            ! strain components: exx=s(1), eyy=s(2), 2*exy=s(3), ezz=s(4)
            
            ! Put exy at the end:
            x = 0.5_Rkind * s(3) ; s(3) = s(4) ; s(4) = x
            
            ! stress components: sxx=s(5), syy=s(6), sxy=s(7)

            !s(4:7) = s(3:6)
            !s(3) =-poiss*(s(1)+s(2))/(1.0_Rkind + poiss) ! for plane_stress compute ezz    
            trs = s(5)+s(6)
            tre = s(1)+s(2)+s(3)
            s(8) = tre
            s(9) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + s(3)**2 + 2.0_Rkind*s(4)**2 - &
                                   (tre**2)/3.0_Rkind))
            s(10) = trs / 3.0_Rkind
            s(11) = sqrt(1.5_Rkind*(s(5)**2 + s(6)**2 + 2.0_Rkind*s(7)**2 - (trs**2)/3.0_Rkind))
         else if (elast == 1) then
            s(4) = 0.5_Rkind * s(4)
            tre = s(1)+s(2)+s(3)
            trs = s(5)+s(6)+s(7)
            s(9) = tre
            s(10) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + s(3)**2 + 2.0_Rkind*s(4)**2 - &
                                    (tre**2)/3.0_Rkind))
            s(11) = trs / 3.0_Rkind
            s(12) = sqrt(1.5_Rkind*(s(5)**2 + s(6)**2 + s(7)**2 + 2.0_Rkind*s(8)**2 - &
                                    (trs**2)/3.0_Rkind))
         else if (elast == 3) then
            s(4:6) = 0.5_Rkind * s(4:6)
            tre = s(1)+s(2)+s(3)
            trs = s(7)+s(8)+s(9)
            s(13) = tre
            s(14) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + s(3)**2 + &
                                    2.0_Rkind*(s(4)**2 + s(5)**2 + s(6)**2) - &
                                    (tre**2)/3.0_Rkind))
            s(15) = trs / 3.0_Rkind
            s(16) = sqrt(1.5_Rkind*(s(7)**2 + s(8)**2 + s(9)**2 + &
                                    2.0_Rkind*(s(10)**2 + s(11)**2 + s(12)**2) - &
                                    (trs**2)/3.0_Rkind))
         end if
         
      else if (stokes /= 0) then
         dequiv = sqrt( 1.50_Rkind * dot_product ( s(1:nstri)        , s(1:nstri)         ) )
         sequiv = sqrt( 1.50_Rkind * dot_product ( s(nstri+1:2*nstri), s(nstri+1:2*nstri) ) )
         s(2*nstri+1) = log10(s(2*nstri+1))
         
         s(nval-1) = dequiv
         s(nval  ) = sequiv
         
         if (stokes == 2) then
            s(3) = s(3) * sqrt2i
            s(6) = s(6) * sqrt2i
         else
            s( 4: 6) = s( 4: 6) * sqrt2i
            s(10:12) = s(10:12) * sqrt2i
         end if                  
      end if    
             
      do in = 1, nnode
         ip = mymesh%lnods(in,ie) ; str(1:nval,ip) = str(1:nval,ip) + s(1:nval)
      end do   
      
   end do 

   do ip = 1, npoin
      nv = mymesh%lvale(ip)
      if (nv /= 0) then
         xval = 1.0_Rkind / real(nv,kind=Rkind)
      else
         xval = 0.0_Rkind
      end if      
      str(1:nval,ip) = str(1:nval,ip) * xval ! node average
   end do   
      
      
   write(ufvtk,'(a)')'# vtk DataFile Version 3.0'
   write(ufvtk,'(a)')'produit par camef2017'       ! titre
   write(ufvtk,'(a)')'ASCII'                       ! Format ecriture
   write(ufvtk,'(a)')'DATASET UNSTRUCTURED_GRID'   ! Type de maillage
   
   fm = '(30e13.5)'
   fm = '(*(g0,1x))'
!
!- node coordinates:
!
   z(:) = 0.0_Rkind
   
   write(ufvtk,'(a,i0,a)')'POINTS ',npoin,' double'
   do ip = 1, npoin
      write(ufvtk,fm)mymesh%coord(:,ip),z(1:3-ndime)
   end do 
!
!- cell topology:
!
!     type_cell: vtk code defining the topological object type
!     For low order, e.g.:      
!           3 = 2-noded line           5 = 3-noded triangle     9 = 4-noded quadrangle
!          10 = 4-noded tetraedra     11 = 8-noded hexaedra     
!
   select case(name_element)
      case ('segm02')
         type_cell = 3
      case ('segm03')
         type_cell = 21
         
      case ('tria03')   
         type_cell = 5
      case ('tria06')   
         type_cell = 22
         
      case ('quad04')   
         type_cell = 9
      case ('quad08')   
         type_cell = 23
      case ('quad09')   
         type_cell = 28

      case ('tetr04')   
         type_cell = 10
      case ('tetr10')   
         type_cell = 24 
         
      case ('hexa08')   
         type_cell = 11
         
      case default
         stat = err_t ( stat = WARNING, where = 'output_vtk', msg = &
                       'output in vtk format for this cell type not yet implemented' )
   end select         
   
!
!- (attention: 0-based array (hence the "-1" below))
!   
   write(ufvtk,'(a,i0,1x,i0)') 'CELLS ',nelem,(nnode+1)*nelem
   
   do ie = 1, nelem
      write(ufvtk,'(30(1x,i0))')nnode, mymesh%lnods(:,ie) - 1
   enddo
   write(ufvtk,'(a, i0)') 'cell_types ',nelem
   do ie = 1, nelem
      write(ufvtk,'(i0)')type_cell
   enddo
!
!- nodal values and averaged element values:
!
   write(ufvtk,'(a,i0)')'POINT_DATA ',npoin
        
   select case(name_model_pb(1:lp-1))

      case('diffusion')
      
         write(ufvtk,'(a)')'SCALARS "potential" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm)femsys%Sol(:,ip)
         end do   
       
         write(ufvtk,'(a)') 'SCALARS "||grad(u)||" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'   
         do ip = 1, npoin
            write(ufvtk,fm)sqrt(str(1,ip)**2 + str(2,ip)**2)
         end do
           
         write(ufvtk,'(a)') 'VECTORS "grad(u)" double'     
         do ip = 1, npoin
            write(ufvtk,fm)str(1:ndime,ip), z(ndime:2)
         end do

      case('demo1')
      
         write(ufvtk,'(a)')'SCALARS "potential" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm)femsys%Sol(:,ip)
         end do   
       
         write(ufvtk,'(a)') 'SCALARS "||grad(u)||" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'   
         do ip = 1, npoin
            write(ufvtk,fm)sqrt(str(1,ip)**2 + str(2,ip)**2)
         end do
           
         write(ufvtk,'(a)') 'VECTORS "grad(u)" double'     
         do ip = 1, npoin
            write(ufvtk,fm)str(1,ip),str(2,ip),0.0
         end do

       
      case ('elasticity')
      
         write(ufvtk,'(a)')'VECTORS "displacement" double'
         do ip = 1, npoin
            call output_nodalval (u, ip, femsys)
            write(ufvtk,fm)femsys%Sol(:,ip), z(ndime:2)
            !write(ufvtk,fm) u(1:ndofn), z(ndime:2)
         end do   

         
         do i = 1, nval
            write(ufvtk,'(a)') 'SCALARS "'//trim(var(i))//'" double 1'
            write(ufvtk,'(a)')'LOOKUP_TABLE default'   
            do ip = 1, npoin
               write(ufvtk,fm)str(i,ip)
            end do
         end do   
                     
      case ('stokes','stokesnl','stokesnl2')
         write(ufvtk,'(a)')'VECTORS "velocity" double'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm) femsys%Sol(1:ndofn-1,ip), z(ndofn-1:2)
         end do   
         
         do i = 1, nval
            write(ufvtk,'(a)') 'SCALARS "'//trim(var(i))//'" double 1'
            write(ufvtk,'(a)')'LOOKUP_TABLE default'   
            do ip = 1, npoin
               write(ufvtk,fm)str(i,ip)
            end do
         end do   

         write(ufvtk,'(a)')'SCALARS "pressure" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm) femsys%Sol(:,ip)
         end do   
                        
   end select    
                     
   close(ufvtk)
   
   END SUBROUTINE output_vtkl                  


!=============================================================================================
   SUBROUTINE output_vtk ( fmt, femsys, mymesh, mydata, stat, optvar )
!=============================================================================================
   character(len=*   ), intent(in    )           :: fmt
   type     (linsys_t), intent(in    )           :: femsys
   type     (mesh_t  ), intent(in    )           :: mymesh   
   type     (udata_t ), intent(in    )           :: mydata
   type     (err_t   ), intent(in out)           :: stat
   real     (Rkind   ), intent(in    ), optional :: optvar(:)
!--------------------------------------------------------------------------------------------- 
!  Prints results in vtk format
!-----------------------------------------------------------------------------------R.H. 01/17  
!- locals variables --------------------------------------------------------------------------
   real     (Rkind ), parameter   :: sqrt2i = 1.0_Rkind / sqrt(2.0_Rkind)   
   integer  (Ikind )              :: i, ip, ie, type_cell, lp, nstre, nval, elast, stokes, &
                                     numat, err
   real     (Rkind )              :: z(2), s(noutp+10), poiss, tre, trs, dequiv, &
                                     sequiv
   real     (Rkind ), allocatable :: str(:,:) !str(nstri+10,nelem)
   character(len=80)              :: ofvtk, fm
   integer  (Ikind ), save        :: ufvtk=0, n=0
   character(len=13)              :: var(28)   
!--------------------------------------------------------------------------------------------- 

   n = n + 1
!
!- open the file xxxxx.n.vtk, whit xxxx the input file name and n the # of the output
!
   write(ofvtk,'(a,".",i0,".vtk")')trim(adjustl(name_input_file)),n

   call util_get_unit (ufvtk)

   if (fmt == 'bin') then
      open (ufvtk, file = ofvtk, form='unformatted')
   else
      open (ufvtk, file = ofvtk)
   end if      

   if (verbosity(1) >= 1) write(*,'(1x,a)') &
   'Writing the output file "'//trim(ofvtk)//'" in '//trim(fmt)//' format'   

   write(ufvtk,'(a)')'# vtk DataFile Version 3.0'
   write(ufvtk,'(a)')'produit par camef2017'       ! titre
   write(ufvtk,'(a)')'ASCII'                       ! Format ecriture
   write(ufvtk,'(a)')'DATASET UNSTRUCTURED_GRID'   ! Type de maillage
   
   fm = '(30e13.5)'
!
!- node coordinates:
!
   z(:) = 0_Rkind
   
   write(ufvtk,'(a,i0,a)')'POINTS ',npoin,' double'
   do ip = 1, npoin
      write(ufvtk,fm)mymesh%coord(:,ip),z(1:3-ndime)
   end do 
!
!- cell topology:
!
!     type_cell: vtk code defining the topological object type
!     For low order, e.g.:      
!           3 = 2-noded line           5 = 3-noded triangle     9 = 4-noded quadrangle
!          10 = 4-noded tetraedra     11 = 8-noded hexaedra     
!
   select case(name_element)
      case ('segm02')
         type_cell = 3
      case ('segm03')
         type_cell = 21
         
      case ('tria03')   
         type_cell = 5
      case ('tria06')   
         type_cell = 22
         
      case ('quad04')   
         type_cell = 9
      case ('quad08')   
         type_cell = 23
      case ('quad09')   
         type_cell = 28
         
      case ('tetr04')   
         type_cell = 10
      case ('tetr10')   
         type_cell = 24 
         
      case ('hexa08')   
         type_cell = 11
      case default
         stat = err_t ( stat = WARNING, where = 'output_vtk', msg = &
                       'output in vtk format for this cell type not yet implemented' )
   end select
!
!- (attention: 0-based array (hence the "-1" below))
!   
   write(ufvtk,'(a,i0,1x,i0)') 'CELLS ',nelem,(nnode+1)*nelem
   
   do ie = 1, nelem
      write(ufvtk,'(30(1x,i0))')nnode, mymesh%lnods(:,ie) - 1
   enddo
   write(ufvtk,'(a, i0)') 'cell_types ',nelem
   do ie = 1, nelem
      write(ufvtk,'(i0)')type_cell
   enddo

   lp = index(name_model_pb,'(')
   
   elast = 0; nstre = nstri; nval = nstre; var = ''
   if (name_model_pb(1:lp-1) == 'elasticity') then
      if (index(name_model_pb,'3d') /= 0          ) then 
         elast = 3; 
         var(1:16) = ['epsil_xx     ','epsil_yy     ','epsil_zz     ', &
                      'epsil_xy     ','epsil_yz     ','epsil_zx     ', &
                      'sigma_xx     ','sigma_yy     ','sigma_zz     ', &
                      'sigma_xy     ','sigma_yz     ','sigma_zx     ', &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3', &
                      'sigma_equiv  ']               
                       
         nval = 16                                                                                           
      end if   
       
      if (index(name_model_pb,'plane_strain') /= 0) then
         elast = 2 ; nstre = 4
         var(1:11) = ['epsil_xx     ','epsil_yy     ','epsil_xy     ',                 &
                      'sigma_xx     ','sigma_yy     ','sigma_zz     ','sigma_xy     ', &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3','sigma_equiv  ']
         nval = 11
      end if   
      
      if (index(name_model_pb,'plane_stress') /= 0) then
         elast =-2; nstre = 4 
         var(1:11) = ['epsil_xx     ','epsil_yy     ','epsil_zz     ','epsil_xy     ', &
                      'sigma_xx     ','sigma_yy     ','sigma_xy     ',                 &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3','sigma_equiv  ']
         nval = 11                                   
      end if   
         
      if (index(name_model_pb,'axisymmetry') /= 0 ) then
         elast = 1
         var(1:12) = ['epsil_rr     ','epsil_zz     ','epsil_tt     ','epsil_rz     ', &
                      'sigma_rr     ','sigma_zz     ','sigma_tt     ','sigma_rz     ', &
                      'epsil_trace  ','epsil_equiv  ','sigma_trace/3','sigma_equiv  ']
         nval = 12                                                 
      end if
   end if

   stokes = 0
   if (name_model_pb(1:lp-1) == 'stokes'  .or. name_model_pb(1:lp-1) == 'stokesnl' .or. &
       name_model_pb(1:lp-1) == 'stokesnl2' ) then
      if (index(name_model_pb,'3d') /= 0) then 
         stokes = 3
         var(1:15) = ['deps_xx     ','deps_yy     ','deps_zz     ', &
                      'deps_xy     ','deps_yz     ','deps_zx     ', &
                      'tau_xx      ','tau_yy      ','tau_zz      ', &
                      'tau_xy      ','tau_yz      ','tau_zx      ', &
                      'log10(visco)','deps_equiv  ','tau_equiv   ']                      
         nval = 15                                                 
         if (name_model_pb(1:lp-1) == 'stokesnl2' ) then
            stokes =-3
            var(16) = 'Peierls     '
            nval = 16
         end if
      else if (index(name_model_pb,'2d') /= 0) then 
         stokes = 2 ; nstre = 4
         var(1:9) = ['deps_xx     ','deps_yy     ','deps_xy     ', &
                     'tau_xx      ','tau_yy      ','tau_xy      ', &
                     'log10(visco)','deps_equiv  ','tau_equiv   ']                             
         nval = 9                                  
         if (name_model_pb(1:lp-1) == 'stokesnl2' ) then
            stokes =-2
            var(10) = 'Peierls     '
            nval = 10
         end if
      end if   
   end if   
!
!- average I.P. values on each element
!
   allocate(str(nval,nelem), source = 0.0_Rkind, stat = err)

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where='output_vtk', msg='Allocation failure for "str"')
      return
   end if

!deqmean = 0.0_Rkind
   
   do ie = 1, nelem  
      call output_elemtavg (s, ie, femsys)

      if (elast /= 0) then
         if (elast == 2) then
            numat = mymesh % numdom(ie)
            poiss = mydata % vmatprop(2,numat)
            s(3) = 0.5_Rkind * s(3)
            s(7) = s(6); s(6) = poiss*(s(4) + s(5)) ! for plane_strain compute szz
            trs = s(4)+s(5)+s(6)
            tre = s(1)+s(2)
            s(8) = tre
            s(9) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + 2.0_Rkind*s(3)**2 - (tre**2)/3.0_Rkind))
            s(10) = trs / 3.0_Rkind
            s(11) = sqrt(1.5_Rkind*(s(4)**2 + s(5)**2 + s(6)**2 + 2.0_Rkind*s(7)**2 - &
                                    (trs**2)/3.0_Rkind) )
         else if (elast ==-2) then
            numat = mymesh % numdom(ie)
            poiss = mydata % vmatprop(2,numat)
            s(3) = 0.5_Rkind * s(3)
            s(4:7) = s(3:6)
            s(3) =-poiss*(s(1)+s(2))/(1.0_Rkind + poiss) ! for plane_stress compute ezz    
            trs = s(5)+s(6)
            tre = s(1)+s(2)+s(3)
            s(8) = tre
            s(9) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + s(3)**2 + 2.0_Rkind*s(4)**2 - &
                                   (tre**2)/3.0_Rkind))
            s(10) = trs / 3.0_Rkind
            s(11) = sqrt(1.5_Rkind*(s(5)**2 + s(6)**2 + 2.0_Rkind*s(7)**2 - (trs**2)/3.0_Rkind))
         else if (elast == 1) then
            s(4) = 0.5_Rkind * s(4)
            tre = s(1)+s(2)+s(3)
            trs = s(5)+s(6)+s(7)
            s(9) = tre
            s(10) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + s(3)**2 + 2.0_Rkind*s(4)**2 - &
                                    (tre**2)/3.0_Rkind))
            s(11) = trs / 3.0_Rkind
            s(12) = sqrt(1.5_Rkind*(s(5)**2 + s(6)**2 + s(7)**2 + 2.0_Rkind*s(8)**2 - &
                                    (trs**2)/3.0_Rkind))
         else if (elast == 3) then
            s(4:6) = 0.5_Rkind * s(4:6)
            tre = s(1)+s(2)+s(3)
            trs = s(7)+s(8)+s(9)
            s(13) = tre
            s(14) = sqrt(1.5_Rkind*(s(1)**2 + s(2)**2 + s(3)**2 + &
                                    2.0_Rkind*(s(4)**2 + s(5)**2 + s(6)**2) - &
                                    (tre**2)/3.0_Rkind))
            s(15) = trs / 3.0_Rkind
            s(16) = sqrt(1.5_Rkind*(s(7)**2 + s(8)**2 + s(9)**2 + &
                                    2.0_Rkind*(s(10)**2 + s(11)**2 + s(12)**2) - &
                                    (trs**2)/3.0_Rkind))
         end if
         
      else if (stokes /= 0) then
         dequiv = sqrt( 1.50_Rkind * dot_product ( s(1:nstri)        , s(1:nstri)         ) )
!dequiv = dequiv*1e14
         sequiv = sqrt( 1.50_Rkind * dot_product ( s(nstri+1:2*nstri), s(nstri+1:2*nstri) ) )

!deqmean = deqmean+dequiv         
         s(2*nstri+1) = log10(s(2*nstri+1))
                  
         if (stokes == 2 .or. stokes ==-2) then
            s(3) = s(3) * sqrt2i
            s(6) = s(6) * sqrt2i
            if (stokes == 2) then
               s(nval-1) = dequiv
               s(nval  ) = sequiv    
            else        
               s(nval-2) = dequiv
               s(nval-1) = sequiv    
               s(nval  ) = optvar(ie)
            end if
         else if (stokes == 3 .or. stokes ==-3) then
            s( 4: 6) = s( 4: 6) * sqrt2i
            s(10:12) = s(10:12) * sqrt2i
            if (stokes == 3) then
               s(nval-1) = dequiv
               s(nval  ) = sequiv
            else
               s(nval-2) = dequiv
               s(nval-1) = sequiv    
               s(nval  ) = optvar(ie)
            end if
         end if                  
      end if    
                                    
      str(1:nval,ie) = s(1:nval)
   end do       
!
!- nodal values and averaged element values:
!
   write(ufvtk,'(a,i0)')'POINT_DATA ',npoin
        
   select case(name_model_pb(1:lp-1))

      case('diffusion')
      
         write(ufvtk,'(a)')'SCALARS "potential" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm)femsys%Sol(:,ip)
         end do   
   
         write(ufvtk,'(a,i0)')'CELL_DATA ',nelem  
    
         write(ufvtk,'(a)') 'SCALARS "||grad(u)||" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'   
         do ie = 1, nelem
            write(ufvtk,fm)sqrt( sum(str(1:ndime,ie)**2) )
         end do  
         
         write(ufvtk,'(a)') 'VECTORS "grad(u)" double'     
         do ie = 1, nelem
            write(ufvtk,fm)str(1:ndime,ie), z(ndime:2)
         end do           
         
      case('demo1')
      
         write(ufvtk,'(a)')'SCALARS "potential" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm)femsys%Sol(:,ip)
         end do   
   
         write(ufvtk,'(a,i0)')'CELL_DATA ',nelem  
    
         write(ufvtk,'(a)') 'SCALARS "||grad(u)||" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'   
         do ie = 1, nelem
            write(ufvtk,fm)sqrt(str(1,ie)**2 + str(2,ie)**2)
         end do  
         
         write(ufvtk,'(a)') 'VECTORS "grad(u)" double'     
         do ie = 1, nelem
            write(ufvtk,fm)str(1,ie),str(2,ie),0.0
         end do           
       
      case ('elasticity')
      
         write(ufvtk,'(a)')'VECTORS "displacement" double'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm)femsys%Sol(:,ip), z(ndime:2)
         end do   

         write(ufvtk,'(a,i0)')'CELL_DATA ',nelem  

         do i = 1, nval
            write(ufvtk,'(a)')'SCALARS "'//trim(var(i))//'" double 1'
            write(ufvtk,'(a)')'LOOKUP_TABLE default'   
            do ie = 1, nelem
               write(ufvtk,fm)str(i,ie)
            end do
         end do   

      case ('stokes','stokesnl','stokesnl2')
         write(ufvtk,'(a)')'VECTORS "velocity" double'         
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm) femsys%Sol(1:ndofn-1,ip), z(ndofn-1:2)
         end do   

         write(ufvtk,'(a)')'SCALARS "pressure" double 1'
         write(ufvtk,'(a)')'LOOKUP_TABLE default'
         do ip = 1, npoin
            !call output_nodalval (u, ip, femsys)
            write(ufvtk,fm) femsys%Sol(ndofn,ip)
         end do            

         write(ufvtk,'(a,i0)')'CELL_DATA ',nelem           
         do i = 1, nval
            write(ufvtk,'(a)')'SCALARS "'//trim(var(i))//'" double 1'            
            write(ufvtk,'(a)')'LOOKUP_TABLE default'   
            do ie = 1, nelem
               write(ufvtk,fm)str(i,ie)
            end do
         end do   
      
   end select    
                  
         
   close(ufvtk)
   
   END SUBROUTINE output_vtk   
   
   
!=============================================================================================
   SUBROUTINE output_nodalval ( u, ip, femsys )
!=============================================================================================
   type     (linsys_t), intent(in ) :: femsys
   integer  (Ikind   ), intent(in ) :: ip   
   real     (Rkind   ), intent(out) :: u(ndofn)
!--------------------------------------------------------------------------------------------- 
!  Extracts nodal values of node #ip
!-----------------------------------------------------------------------------------R.H. 01/17  

!- locals variables --------------------------------------------------------------------------      
!   integer  (Ikind ) :: id, idof
!--------------------------------------------------------------------------------------------- 

   u = femsys%Sol(:,ip)
!    do id = 1, ndofn
!       idof = femsys%Dof(id,ip)
!       if (idof > 0) then
!          u(id) = femsys%sol(idof)
!       else
!          u(id) = femsys%bCond(id,-idof)
!       end if        
!    end do
   
   END SUBROUTINE output_nodalval
   
!=============================================================================================
   SUBROUTINE output_elemtavg (s, ie, femsys)
!=============================================================================================
   type     (linsys_t), intent(in ) :: femsys
   integer  (Ikind   ), intent(in ) :: ie   
   real     (Rkind   ), intent(out) :: s(*)
!--------------------------------------------------------------------------------------------- 
!  Computes averaged element values of element #ie 
!
!  Inputs
!       ie                                 : the # of the element
!       femsys%Str(1:ndime            , ip): ndime coordinates of the ip th I.P.
!       femsys%Str(ndime+1:ndime+nstri, ip): nstri values at the ip th I.P. 
!
!  Output
!       s                                  : averaged values on the element ie 
!-----------------------------------------------------------------------------------R.H. 01/17 

!- locals variables --------------------------------------------------------------------------      
   integer  (Ikind ) :: i1, i2
!--------------------------------------------------------------------------------------------- 
  
   i1 = (ie-1)*nipts + 1 ; i2 = i1 + nipts - 1
        
   s(1:noutp) = sum( femsys%Str(ndime+1:ndime+noutp,i1:i2) , 2 ) / real(nipts,kind=Rkind)
   
   END SUBROUTINE output_elemtavg


!=============================================================================================
   SUBROUTINE output_error (uerr, msg, nmsg, where, warning)
!=============================================================================================
   integer  (Ikind), intent(in), optional :: warning, nmsg, uerr
   character(len=*), intent(in), optional :: msg(*)
   character(len=*), intent(in), optional :: where
!---------------------------------------------------------------------------------------------    
!  Affichage d'un message d'erreur ou d'avertissement. Arret du programme dans le 1er cas
!
!  Si uerr n'est pas present, l'affichage se fait sur la sortie standard, sinon uerr doit
!  correspondre a l'unite logique d'un fichier prealablement ouvert.
!----------------------------------------------------------------------------------- R.H 11/16  

!- local variables: --------------------------------------------------------------------------    
   integer  (Ikind ) :: i, ue
!---------------------------------------------------------------------------------------------            

   if (present(uerr)) then 
     ue = uerr
   else 
     ue = 6
   end if    
   
   write(ue,*)
   if (present(warning)) then 
      if (warning > 0) write(ue,'(a)')'Warning:'   
   else   
      write(ue,'(a)')'Error:'
   end if   
    
   if (present(where)) then
      write(ue,'(a)')'--> in output_'//trim(adjustl(where))
   end if
           
   if (present(msg)) then
      if (present(nmsg)) then
         do i = 1, nmsg
            if (len_trim(msg(i)) > 0) write(ue,'(a)')'--> '//trim(msg(i))
         end do
      else   
         write(ue,'(a)')'--> '//trim(msg(1))
      end if   
   end if      
    
   if (.not. present(warning)) then
      write(ue,'(a,/)')'--> STOP'
      stop
   end if       
      
   write(ue,*)

   END SUBROUTINE output_error   
   
end module output_m   
   