#include "error.fpp"

MODULE storagetools_m
!
!- Ce module regroupe l'ensemble des routines liees au stockage du systeme lineaire
!
   use kindParameters_m

   use defTypes_m
   use param_m
   use reorderings_m

   use sparskit_m , only : sparskit_csort       
   use util_m     , only : util_string_low, util_wtime, i2a=>util_intToChar
   use constants_m, only : IZERO, IERROR, UERROR
    
   implicit none    
            
CONTAINS
    
!=============================================================================================
   SUBROUTINE storagetools_driver ( mydata, mymesh, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(udata_t ), intent(in    ) :: mydata 
   type(linsys_t), intent(in out) :: femsys
   type(err_t   ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!
!  Determines the matrix profil according to the selected storage format given by the variable
!  storage (= "csr", "csrsym" or "skyline")
!
!  The routine first initializes the array
!
!          femsys % Dof (1:ndofn,1:npoin) 
!
!  which gives the # associated to each dof: Dof(id,ip) is the global dof # of the id th local
!  dof of the node # ip. A negative value indicates that the dof is constrained.
!   
!  The routine then computes, for csr formats, the two arrays
!
!         femsys % Row (1:n+1) and femsys % Col (1:nzero)
!
!         (where nzero is first computed according to the mesh connectivities)
! 
!  and for skyline format, the array
!
!         femsys % Row (1:n)
!
!  which contains the diagonal adresses.
!
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=* ), parameter :: Here = 'storagetools_driver'
   integer  (Ikind )            :: ip, id, idir, iref, jref, ifac, in, ieq, err
   character(len=80)            :: storage
   character(len=1 )            :: obj
   real     (Rkind )            :: t0
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 

   t0 = util_wtime()   
!
!- 1) Numbering of each dof (allocate and compute the array Dof)
!		
   allocate (femsys%Dof(ndofn,npoin), stat=err)
    
   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Dof'" )
      return
   end if
               
   femsys%Dof(:,:) = 0
!
!  first, scroll through the list of Dirichlet bc and mark the corresponding constrained dof
!  by putting a positive value in Dof. 
!  After this step, Dof(id,ip) = 0 if (id,ip) is a free dof
!
   do idir = 1, ndiri
      iref = mydata%ldirichl(idir); obj = mydata%odirichl(idir)
       
      if ( obj == 'f' ) then ! bc on a face
         err = 1
         do ifac = 1, nface
            jref = mymesh%lface(1,ifac)
         
            if ( iref == jref ) then
               err = 0
               do in = 2, ndfac+1
                  ip = mymesh%lface(in,ifac)
                  do id = 1, ndofn
                     if ( mydata%codediri(id,idir) >= 1 .and. & 
                          femsys%Dof(id,ip) == 0              ) femsys%Dof(id,ip) = idir
                  end do
               end do
            end if
         end do
         
         if ( err /= 0 ) then
            stat = err_t ( stat = UERROR, where = Here, msg = 'The face of reference #' // &
                           i2a(iref)//' given for a Dirichlet bc is not a face of the mesh')
            return
         end if   
          
      else if ( obj == 'l' ) then ! bc on a line
         err = 1
         do ifac = 1, nedge
            jref = mymesh%ledge(1,ifac)
         
            if ( iref == jref ) then
               err = 0
               do in = 2, ndedg+1
                  ip = mymesh%ledge(in,ifac)
                  do id = 1, ndofn
                     if ( mydata%codediri(id,idir) >= 1 .and. & 
                          femsys%Dof(id,ip) == 0              ) femsys%Dof(id,ip) = idir
                  end do
               end do
            end if
         end do
         
         if ( err /= 0 ) then
            stat = err_t ( stat = UERROR, where = Here, msg = 'The line of reference #' // &
                           i2a(iref)//' given for a Dirichlet bc is not a line of the mesh')
            return
         end if   

      else if ( obj == 'p' ) then  ! bc at a point
         err = 1
         do ifac = 1, npoif
            jref = mymesh%lpoin(1,ifac)
         
            if ( iref == jref ) then
               err = 0
               ip = mymesh%lpoin(2,ifac)
               do id = 1, ndofn
                  if ( mydata%codediri(id,idir) >= 1 .and. & 
                       femsys%Dof(id,ip) == 0              ) femsys%Dof(id,ip) = idir
               end do
            end if
         end do

         if ( err /= 0 ) then
            stat = err_t ( stat = UERROR, where = Here, msg = 'The point of reference #' // &
                           i2a(iref)//' given for a Dirichlet bc is not a point of the mesh')
            return
         end if
                  
      else   
         stat = err_t ( stat = UERROR, where = Here, msg = 'Indetermined object "' // &
                        trim(obj)//'" found for the Dirichlet #'//i2a(iref)           )
         return
      end if
           
   end do                  
                  
!          
!  Compute the total number of unknowns (nequa) and attribute a number to each free dof. 
!  After this step, Dof(id,ip)<0 if (id,ip) is a constrained dof while for a free dof
!  Dof(id,ip) gives its #.

   nequa = 0
   do ip = 1, npoin

      if ( mymesh%lvale(ip) == 0 ) then ! do not count orphan nodes
         do id = 1, ndofn               ! in the list of unknowns
            femsys%Dof(id,ip) = 1      
         end do
      end if      
    
      do id = 1, ndofn
         if ( femsys%Dof(id,ip) == 0 ) then
            nequa = nequa + 1
            femsys%Dof(id,ip) = nequa              ! # of the dof (id,ip)
         else
            femsys%Dof(id,ip) = -femsys%Dof(id,ip) ! mark constrained dof by a < 0 number
         end if
      end do
    
   end do                
                
   if ( verbosity(3) >= 1 ) then
      print*
      print*,'total number of unknowns =',nequa  ! size of the linear system
      print*
   end if   
    
   femsys%n = nequa
!
!- Optimizes the dof numbering for bandwidth and profile reduction:
!    
   call reorderings_SetEquationNumbers ( mymesh = mymesh,       method = name_reorderMeth, &
                                         verbos = verbosity(3), ldof   = femsys%Dof      , &
                                         stat   = stat                                     )
   error_TraceNreturn(stat>IZERO, Here, stat)
!
!- Construct the dof to node array (%Dof2node):
!
   allocate ( femsys%Dof2node(2,nequa), stat=err ) 

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Dof2node'" )
      return
   end if
                  
   femsys%Dof2node(:,:) = 0
    
   do ip = 1, npoin
      do id = 1, ndofn
         ieq = femsys%dof(id,ip)
         if ( ieq > 0 ) then
            femsys%Dof2node(1,ieq) = id 
            femsys%Dof2node(2,ieq) = ip 
         end if
      end do
   end do
!
!- 2) determine the matrix profil according to the desired storage format
!     (allocate and compute Row and Col):
!    
   storage = util_string_low ( adjustl( name_storage )  )
        
   select case( trim(storage) )

      case ( "csrsym" )
         call storagetools_csrsym  ( mymesh, femsys, stat )
    
      case ( "csr" )
         call storagetools_csr     ( mymesh, femsys, stat )
    
      case ( "skyline" )
         call storagetools_skyline ( mymesh, femsys, stat )
    
      case default
         stat = err_t( stat = UERROR, where = Here, msg = 'Invalid storage scheme "'// &
                       trim(storage)//'"' )
         return       
    
   end select
   
   error_TraceNreturn(stat>IZERO, Here, stat)
!
!- 3) allocate the matrix, the rhs and the solution vector
! 

   allocate ( femsys%Mat(nzero), stat=err )  
            
   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Mat'" )
      return
   end if      
     
   allocate ( femsys%Rhs(nequa), stat=err )  
    
   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Rhs'" )
      return
   end if            
          
   allocate ( femsys%Sol(ndofn,npoin), stat=err )

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Sol'" )
      return
   end if
      
   allocate ( femsys%X  (nequa), stat=err )    

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%X'" )
      return
   end if
                  
   allocate (femsys%Str(noutp+ndime,ntotg), stat=err )               

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Str'" )
      return
   end if
   
   femsys%bCond = mydata%vdirichl

   timer%times(2) = util_wtime(t0)
   timer%names(2) = 'Allocation and preparation of the linear system'
   
   END SUBROUTINE storagetools_driver
    
    
!=============================================================================================
   SUBROUTINE storagetools_CSR ( mymesh, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(linsys_t), intent(in out) :: femsys 
   type(err_t   ), intent(in out) :: stat
!---------------------------------------------------------------------------------------------	
!  Determine the matrix profil for CSR storage format  
!
!  Initial Version     : N.C., 2012.
!  Modified and adapted: R.H., 2015
!  New Version         : R.H., 2020. (uses femsys % dof2node array)
!  New New Version     : R.H., 2020. (without dof2node)
!
!  Notice:
!
!   considere the n x n matrix
!
!                       |  A11   .   A13   .    .   A16  |
!                       |  A21  A22   0   A24   .    .   |
!                 A =   |   .   A32  A33  A34   .    .   |      n = 6
!                       |   .    .   A43  A44   .    .   |
!                       |   .    .   A53   .   A55  A56  |
!                       |   .   A62   .    .    .   A66  |
!
!   where dots stand for zero elements.
!
!   In csr format, the non-zero elements of A are stored in an array M of length nzero 
!   as follows (in this version, column indices in each row are sorted in increasing order
!   by calling the sparskit routine csort (at the end of this routine)):
!
!   M  = [ A11, A13, A16 | A21 A22, A24 | A32, A33, A34 | A43, A44 | A53, A55, A56 | A62, A66 ]
!            1    2    3     4   5    6     7    8    9    10   11    12   13   14    15   16 ] 
!            
!   The array Col, also of length nzero, contains the column index in A of each element of M:
!
!   Col= [   1,   3,   6 |  1,  2,   4  |   2,   3,   4 |   3,   4 |   3,   5,  6  |   2,  6  ]
!
!   and the array Row of length n+1 stores the index into M of the first nonzero element in
!   each row of A:
!
!   Row= [   1,             4,             7,               10,       12,             15,      17]
!             
!   We have then, nzero = Row(n+1) - 1 and the elements of the ith row of A are located in M 
!   between the indexes Row(i) and Row(i+1)-1.
!
!   Note that the diagonal entries A(i,i) of A are located in M at M(k) where 
!                        Row(i) <= k <= Row(i+1)-1   st   Col(k) = i 
!
!   M, Col and Row are saved in the fields %Mat, %Col and %Row of the structure femsys, 
!   respectively.  
!---------------------------------------------------------------------------------------------
	
!- local variables: --------------------------------------------------------------------------
   character(len=* ), parameter   :: Here = 'storagetools_CSR'
   integer  (Ikind )              :: ip, jp, id, jd, iv, jv, npv, nzlin, ieq, jeq, err
   real     (Rkind )              :: dum(1)
   character(len=80)              :: fmt    
   integer  (Ikind ), allocatable :: iwork(:)    
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
! 
!- 1) determine the maximum number of non-zero terms in the matrix and temporarily store in 
!     %Row the # of nodes associated to each dof
!
   allocate ( femsys%Row(nequa+1), stat=err )

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Row'" )
      return
   end if

   nzero = 0
   do ip = 1 , npoin
      do id = 1, ndofn
         ieq = femsys%Dof(id,ip)
         if ( ieq > 0 ) then
            nzlin = 0
            npv = mymesh%lvois(maxnn,ip)
            do jv = 1, npv
               jp = mymesh%lvois(jv,ip)
               do jd = 1, ndofn
                  jeq = femsys%Dof(jd,jp)
                  if ( jeq > 0 ) nzero = nzero + 1
               end do 
            end do
            femsys%row(ieq) = ip 
         end if 
     end do  
   end do
    
   femsys%nzero = nzero ! memorize nzero
!
!- 2) compute arrays Col and Row
!
   allocate ( femsys%Col(nzero), stat=err )

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Col'" )
      return
   end if
                  
   nzero = 0
   ip = femsys%row(1) ; femsys%row(1) = 1
   do ieq = 1, nequa
      npv = mymesh%lvois(maxnn,ip)
      nzlin = 0
      do iv = 1, npv
         jp = mymesh%lvois(iv,ip)
         do id = 1, ndofn
            jeq = femsys%dof(id,jp)
            if ( jeq > 0 ) then
               nzero = nzero + 1
               nzlin = nzlin + 1
               femsys%Col(nzero) = jeq
            end if
         end do
      end do
      ip = femsys%row(ieq+1) ; femsys%row(ieq+1) = femsys%row(ieq) + nzlin
   end do
    
!
!- sorts column indices in each row
!	  	  
   allocate ( iwork(max(nequa,2*nzero)), stat=err )

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'iwork'" )
      return
   end if   
    
   call sparskit_csort ( nequa, dum, femsys%col, femsys%row, iwork, .false. )
   deallocate(iwork)

   if (verbosity(3) >= 1) then
      iv = nzero*Rkind; ip = nzero*Ikind; id = (nequa+1)*Ikind
      fmt='(/," Summary of memory usage required for the matrix (CSR storage):",/)'
      write(*,fmt)
      fmt = '(3x,"- array Mat: ",i10," reals    x ",i0," = ",i10, " bytes")'
      write(*,fmt)nzero,Rkind,iv
      fmt = '(3x,"- array Col: ",i10," integers x ",i0," = ",i10, " bytes")'
      write(*,fmt)nzero,Ikind,ip
      fmt = '(3x,"- array Row: ",i10," integers x ",i0," = ",i10, " bytes")'
      write(*,fmt)nequa+1,Ikind,id       
      fmt = '(/,3x,"  Total  : ",i10," bytes"," (",f0.2," Mb)"/)'
      write(*,fmt) iv+ip+id,(iv+ip+id)/1e6
   end if   
	  
   END SUBROUTINE storagetools_CSR


!=============================================================================================
   SUBROUTINE storagetools_CSRsym ( mymesh, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(linsys_t), intent(in out) :: femsys 
   type(err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------------------
!  Determine the matrix profil for symetric csr storage format  
!
!  Initial Version     : N.C., 2012.
!  Modified and adapted: R.H., 2015
!  New Version         : R.H., 2020. (uses femsys % dof2node array)
!
!  Notice:
!
!   considere the n x n symmetric matrix
!
!                       |  A11   .   A13   .    .   A16  |
!                       |       A22   0   A24   .    .   |
!                 A =   |            A33  A34   .    .   |      n = 6
!                       |                 A44   .    .   |
!                       |                      A55  A56  |
!                       |                           A66  |
!
!   where dots stand for zero elements.
!
!   In symmetric csr format, the upper triangle of A is stored in an array M of length nzero 
!   as follows (in this version, column indices in each row are sorted in increasing order
!   by calling the sparskit routine csort (at the end of this routine)):
!
!   M   = [ A11, A13, A16 | A22, A24 | A33, A34 | A44 | A55, A56 |   A66 ]
!             1    2    3     4    5     6    7     8     9   10      11 
!            
!   The array Col of length nzero contains the column index in M of each element of A:
!
!   Col = [   1,   3,   6 |   2,   4 |   3,   4 |   4 |   5,   6 |     6 ]
!
!   and the array Row of length n+1 stores the index into M of the first nonzero element 
!   in each row of A:
!
!   Row = [   1,              4,         6,         8,    9,          11,  12]
!             
!   We have then, nzero = Row(n+1) - 1 and the elements of the ith row of A are located in M 
!   between the indexes Row(i) and Row(i+1)-1.
!
!   Note that since each row are sorted by column, the diagonal entries A(i,i) of A are 
!   located in M at Row(i):  M(Row(i)) = A(i,i),  i = 1, ..., n
! 
!   M, Col and Row are saved in the fields %Mat, %Col and %Row of the structure femsys, 
!   respectively.   
!---------------------------------------------------------------------------------------------
	
!- local variables: --------------------------------------------------------------------------
   character(len=* ), parameter   :: Here = 'storagetools_CSRsym'
   integer  (Ikind )              :: ip, jp, id, jd, iv, jv, nzlin, npv, ieq, jeq, err
   real     (Rkind )              :: dum(1)
   character(len=90)              :: fmt
   integer  (Ikind ), allocatable :: iwork(:)
!---------------------------------------------------------------------------------------------
   
   if ( stat > IZERO ) return 
! 
!- 1) determine the maximum number of non-zero terms in the matrix and temporarily store in 
!     %Row the # of nodes associated to each dof
!
   allocate ( femsys%Row(nequa+1), stat=err )  

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Row'" )
      return
   end if

   nzero = 0
   do ip = 1 , npoin
      do id = 1, ndofn
         ieq = femsys%Dof(id,ip)
         if ( ieq > 0 ) then
            nzlin = 0
            npv = mymesh%lvois(maxnn,ip)
            do jv = 1, npv
               jp = mymesh%lvois(jv,ip)
               do jd = 1, ndofn
                  jeq = femsys%Dof(jd,jp)
                  if ( jeq > 0 ) nzero = nzero + 1
               end do 
            end do
            femsys%row(ieq) = ip 
         end if 
     end do  
   end do
    
   femsys%nzero = nzero ! memorize nzero
!
!- 2) compute arrays column and row
!
   allocate ( femsys%Col(nzero), stat=err )  
   
   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Col'" )
      return
   end if

   nzero = 0
   ip = femsys%row(1) ; femsys%row(1) = 1
   do ieq = 1, nequa
      npv = mymesh%lvois(maxnn,ip)
      nzlin = 0
      do iv = 1, npv
         jp = mymesh%lvois(iv,ip)
         do id = 1, ndofn
            jeq = femsys%dof(id,jp)
            if ( jeq > 0 .and. jeq >= ieq ) then
               nzero = nzero + 1
               nzlin = nzlin + 1
               femsys%Col(nzero) = jeq
            end if
         end do
      end do
      ip = femsys%row(ieq+1) ; femsys%row(ieq+1) = femsys%row(ieq) + nzlin
   end do
!
!- sorts column indices in each row
!	  	  
   allocate ( iwork(max(nequa,2*nzero)), stat=err )
 
   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'iwork'" )
      return
   end if 
    
   call sparskit_csort ( nequa, dum, femsys%col, femsys%row, iwork, .false. )
   deallocate(iwork)

   if (verbosity(3) >= 1) then
      iv = nzero*Rkind; ip = nzero*Ikind; id = (nequa+1)*Ikind
      fmt='(/," Resume de l''occupation memoire necessaire a la matrice (stockage CSRsym):",/)'
      write(*,fmt)
      fmt = '(3x,"- tableau Mat",2x,": ",i10," reels   x ",i0," = ",i10, " octets")'
      write(*,fmt)nzero,Rkind,iv
      fmt = '(3x,"- tableau Col",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
      write(*,fmt)nzero,Ikind,ip
      fmt = '(3x,"- tableau Row",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
      write(*,fmt)nequa+1,Ikind,id       
      fmt = '(/,3x,"  Total      ",2x,": ",i10," octets"," (",f0.2," Mo)"/)'
      write(*,fmt) iv+ip+id,(iv+ip+id)/1e6
   end if   
	      
   END SUBROUTINE storagetools_CSRsym
	

!=============================================================================================
   SUBROUTINE storagetools_skyline ( mymesh, femsys, stat )
!=============================================================================================
   type(mesh_t  ), intent(in    ) :: mymesh
   type(linsys_t), intent(in out) :: femsys 
   type(err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------------------
!  Determine the matrix profil for symmetric skyline storage format  
!
!  Version initiale : code Geodyn (J.P. Vilotte). Version modifiee : R.H., 1999, 2016
!
!  Notice:
!
!   considere the n x n symmetric matrix
!
!                       |  A11   .   A13   .    .   A16  |
!                       |       A22   0   A24   .    0   |
!                 A =   |            A33  A34   .    0   |      n = 6
!                       |                 A44   .    0   |
!                       |                      A55  A56  |
!                       |                           A66  |
!
!   where dots stand for zero elements.
!
!   In symmetric skyline format, the upper triangle of A is stored in an array M of length nzero 
!   as follows (descending column):
!
!   M   = [ A11 | A22 | A13, 0, A33 | A24, A34, A44 | A55 | A16,  0,  0,  0, A56, A66 ]
!             1    2     3   4   5     6    7    8     9    10   11  12  13   14   15
!            
!   The adresses in M of the diagonal entries of A are saved in an array Diag of length n
!
!   Diag = [1, 2, 5, 8, 9, 15]
!
!   We have: nzero = Diag(n) and the elements of the ith (i>1) column of A are located in M at
!   the indexes : Diag(i-1)+1, Diag(i-1)+2,..., Diag(i)
!
!   M and Diag are saved in the fields %Mat and %Row of the structure femsys, respectively.   
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=* ), parameter :: Here = 'storagetools_skyline'
   integer  (Ikind )            :: i, ie, in, id, ip, iequa, min_e, diff, ihmax, err
   character(len=90)            :: fmt
!---------------------------------------------------------------------------------------------

   if ( stat > IZERO ) return 
    
   allocate ( femsys%Row(nequa), stat=err ) 

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg="Allocation failure for 'femsys%Row'" )
      return
   end if
   
   femsys%Row(:) = 0
!
!- 1) compute the column heights
!    
   do ie = 1, nelem
    
      min_e = nequa
      do in = 1, nnode
         ip = mymesh%lnods(in,ie)
         do id = 1, ndofn
            iequa = femsys%Dof(id,ip)
            if ( iequa > 0 ) min_e = min (min_e, iequa)
         end do
      end do   

      do in = 1, nnode
         ip = mymesh%lnods(in,ie)
         do id = 1, ndofn
            iequa = femsys%Dof(id,ip)
            if ( iequa > 0 ) then
               diff = iequa - min_e
               femsys%Row(iequa) = max(femsys%Row(iequa), diff)
            end if   
         end do
      end do   
       
   end do
!
!- 2) compute and memorize the diagonal adresses
!
   ihmax = 0      
   femsys%Row(1) = 1
   do i = 2, nequa
      ihmax = max ( ihmax, femsys%Row(i) )
      femsys%Row(i) = femsys%Row(i) + femsys%Row(i-1) + 1
      if ( femsys%Row(i) < 0 ) then
         stat = err_t ( stat=IERROR, where=Here, msg="Unable to proceed with integer(" // &
                        i2a(Ikind)//") (overflow)" )
         return
      end if
   enddo
    
   nzero = femsys%Row(nequa); femsys%nzero = nzero

   if ( verbosity(3) >= 1 ) then
      ip = nzero*Rkind; id = nequa*Ikind
      fmt='(/," Resume de l''occupation memoire necessaire a la matrice (stockage skyline):",/)'
      write(*,fmt)
      fmt = '(3x,"- tableau Mat",2x,": ",i12," reels   x ",i0," = ",i12, " octets")'
      write(*,fmt)nzero,Rkind,ip
      fmt = '(3x,"- tableau Row",2x,": ",i12," entiers x ",i0," = ",i12, " octets")'
      write(*,fmt)nequa+1,Ikind,id       
      fmt = '(/,3x,"  Total      ",2x,": ",i12," octets"," (",f0.2," Mo)"/)'
      write(*,fmt) ip+id,(ip+id)/1e6
   end if   
        
   END SUBROUTINE storagetools_skyline
   
   
! !=============================================================================================
!    SUBROUTINE storagetools_CSR1 (mymesh, femsys)
! !=============================================================================================
!    type(mesh_t  ), intent(in    )  :: mymesh
!    type(linsys_t), intent(in out)  :: femsys 
! !---------------------------------------------------------------------------------------------	
! !  Determine the matrix profil for CSR storage format  
! !
! !  Initial Version     : N.C., 2012.
! !  Modified and adapted: R.H., 2015
! !  New Version         : R.H., 2020. (uses femsys % dof2node array)
! !
! !  Notice:
! !
! !   considere the n x n matrix
! !
! !                       |  A11   .   A13   .    .   A16  |
! !                       |  A21  A22   0   A24   .    .   |
! !                 A =   |   .   A32  A33  A34   .    .   |      n = 6
! !                       |   .    .   A43  A44   .    .   |
! !                       |   .    .   A53   .   A55  A56  |
! !                       |   .   A62   .    .    .   A66  |
! !
! !   where dots stand for zero elements.
! !
! !   In csr format, the non-zero elements of A are stored in an array M of length nzero 
! !   as follows (in this version, column indices in each row are sorted in increasing order
! !   by calling the sparskit routine csort (at the end of this routine)):
! !
! !   M  = [ A11, A13, A16 | A21 A22, A24 | A32, A33, A34 | A43, A44 | A53, A55, A56 | A62, A66 ]
! !            1    2    3     4   5    6     7    8    9    10   11    12   13   14    15   16 ] 
! !            
! !   The array Col, also of length nzero, contains the column index in A of each element of M:
! !
! !   Col= [   1,   3,   6 |  1,  2,   4  |   2,   3,   4 |   3,   4 |   3,   5,  6  |   2,  6  ]
! !
! !   and the array Row of length n+1 stores the index into M of the first nonzero element in
! !   each row of A:
! !
! !   Row= [   1,             4,             7,               10,       12,             15,      17]
! !             
! !   We have then, nzero = Row(n+1) - 1 and the elements of the ith row of A are located in M 
! !   between the indexes Row(i) and Row(i+1)-1.
! !
! !   Note that the diagonal entries A(i,i) of A are located in M at M(k) where 
! !                        Row(i) <= k <= Row(i+1)-1   st   Col(k) = i 
! !
! !   M, Col and Row are saved in the fields %Mat, %Col and %Row of the structure femsys, 
! !   respectively.  
! !---------------------------------------------------------------------------------------------
! 	
! !- local variables: --------------------------------------------------------------------------
!    integer  (Ikind  )  :: ip, jp, id, iv, npv, nzlin, ieq, jeq, err
!    real     (Rkind  )  :: dum(1)
!    character(len=80 )  :: fmt    
!    integer  (Ikind  ), allocatable :: iwork(:)    
! !---------------------------------------------------------------------------------------------
! 
! ! 
! !- 1) determine the maximum number of non-zero terms in the matrix and compute %Row
! !     (adresses of the begining of each row)
! !
!   !-------------------------------------------------------------------!
!   !                    A L L O C A T I O N S                          !
!   !-------------------------------------------------------------------!   
!   ! allocate Row:                                                     !
!   !                                                                   ! 
!    allocate (femsys%Row(nequa+1), stat=err)                           !
!   !-------------------------------------------------------------------!  
! 
!    if (err /= 0) &
!       call storagetools_error (msg= "Memory allocation error for femsys%Row", where="csr")
! 
!    nzero = 0
!    femsys%Row(1) = 1
!    do ieq = 1, nequa
!       ip = femsys%dof2node(2,ieq)    ! the node corresponding to this dof
!       npv = mymesh%lvois(maxnn,ip)   ! number of nodes attached to this node
!       nzlin = 0
!       do iv = 1, npv
!          jp = mymesh%lvois(iv,ip)
!          do id = 1, ndofn
!             if (femsys%dof(id,jp) > 0) nzlin = nzlin + 1
!          end do
!       end do
!       femsys%Row(ieq+1) = femsys%Row(ieq) + nzlin
!       nzero = nzero + nzlin
!    end do
!     
!    femsys%nzero = nzero ! memorize nzero
! !
! !- 2) compute arrays Col and Row
! !
!   !-------------------------------------------------------------------!
!   !                    A L L O C A T I O N S                          !
!   !-------------------------------------------------------------------!   
!   ! allocate Col:                                                     !
!   !                                                                   ! 
!    allocate (femsys%Col(nzero), stat=err)                             !
!   !-------------------------------------------------------------------!  
! 
!    if (err /= 0) &
!       call storagetools_error (msg= "Memory allocation error for femsys%Col", where="csr")
!                   
!    nzero = 0
!    do ieq = 1, nequa
!       ip = femsys%dof2node(2,ieq)
!       npv = mymesh%lvois(maxnn,ip)
!       do iv = 1, npv
!          jp = mymesh%lvois(iv,ip)
!          do id = 1, ndofn
!             jeq = femsys%dof(id,jp)
!             if (jeq > 0) then
!                nzero = nzero + 1
!                femsys%Col(nzero) = jeq
!             end if
!          end do
!       end do
!    end do 
! !
! !- sorts column indices in each row
! !	  	  
!    allocate(iwork(max(nequa,2*nzero)),stat=err)
!     
!    if (err /= 0) &
!       call storagetools_error (msg= "Memory allocation error for femsys%iwork", where="csr")
!     
!    call sparskit_csort ( nequa, dum, femsys%col, femsys%row, iwork, .false. )
!    deallocate(iwork)
! 
!    if (verbosity(3) >= 1) then
!       iv = nzero*Rkind; ip = nzero*Ikind; id = (nequa+1)*Ikind
!       fmt='(/," Resume de l''occupation memoire necessaire a la matrice (stockage CSR):",/)'
!       write(*,fmt)
!       fmt = '(3x,"- tableau Mat",2x,": ",i10," reels   x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Rkind,iv
!       fmt = '(3x,"- tableau Col",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Ikind,ip
!       fmt = '(3x,"- tableau Row",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nequa+1,Ikind,id       
!       fmt = '(/,3x,"  Total      ",2x,": ",i10," octets"," (",f0.2," Mo)"/)'
!       write(*,fmt) iv+ip+id,(iv+ip+id)/1e6
!    end if   
! 	  
!    END SUBROUTINE storagetools_CSR1
! 
! 
! !=============================================================================================
!    SUBROUTINE storagetools_CSR0 (mymesh, femsys)
! !=============================================================================================
!    type(mesh_t  ), intent(in    )  :: mymesh
!    type(linsys_t), intent(in out)  :: femsys 
! !---------------------------------------------------------------------------------------------	
! !  Determine the matrix profil for CSR storage format  
! !
! !  Version initiale : N.C., 2012.
! !
! !  Notice:
! !
! !   considere the n x n matrix
! !
! !                       |  A11   .   A13   .    .   A16  |
! !                       |  A21  A22   0   A24   .    .   |
! !                 A =   |   .   A32  A33  A34   .    .   |      n = 6
! !                       |   .    .   A43  A44   .    .   |
! !                       |   .    .   A53   .   A55  A56  |
! !                       |   .   A62   .    .    .   A66  |
! !
! !   where dots stand for zero elements.
! !
! !   In csr format, the non-zero elements of A are stored in an array M of length nzero 
! !   as follows (in this version, column indices in each row are sorted in increasing order
! !   by calling the sparskit routine csort (at the end of this routine)):
! !
! !   M  = [ A11, A13, A16 | A21 A22, A24 | A32, A33, A34 | A43, A44 | A53, A55, A56 | A62, A66 ]
! !            1    2    3     4   5    6     7    8    9    10   11    12   13   14    15   16 ] 
! !            
! !   The array Col, also of length nzero, contains the column index in A of each element of M:
! !
! !   Col= [   1,   3,   6 |  1,  2,   4  |   2,   3,   4 |   3,   4 |   3,   5,  6  |   2,  6  ]
! !
! !   and the array Row of length n+1 stores the index into M of the first nonzero element in
! !   each row of A:
! !
! !   Row= [   1,             4,             7,               10,       12,             15,      17]
! !             
! !   We have then, nzero = Row(n+1) - 1 and the elements of the ith row of A are located in M 
! !   between the indexes Row(i) and Row(i+1)-1.
! !
! !   Note that the diagonal entries A(i,i) of A are located in M at M(k) where 
! !                        Row(i) <= k <= Row(i+1)-1   st   Col(k) = i 
! !
! !   M, Col and Row are saved in the fields %Mat, %Col and %Row of the structure femsys, 
! !   respectively.  
! !---------------------------------------------------------------------------------------------
! 	
! !- local variables: --------------------------------------------------------------------------
!    integer  (Ikind  )  :: ip, id, iddl, nzlin, npv, jv, jp, jd, jddl, err, iequa, nnz
!    real     (Rkind  )  :: dum(1)
!    character(len=80 )  :: fmt    
!    integer  (Ikind  ), allocatable :: iwork(:)
! !---------------------------------------------------------------------------------------------
! 
! ! 
! !- 1) determine the maximum number of non-zero terms in the matrix
! !
!    nzero = 0
!    do ip = 1 , npoin
!       do id = 1, ndofn
!          iddl = femsys%Dof(id,ip)
!          if (iddl > 0) then
!             nzlin = 0
!             npv = mymesh%lvois(maxnn,ip)
!             do jv = 1, npv
!                jp = mymesh%lvois(jv,ip)
!                do jd = 1, ndofn
!                   jddl = femsys%Dof(jd,jp)
!                   if (jddl > 0) nzero = nzero + 1
!                end do 
!             end do 
!          end if 
!      end do  
!    end do
!     
!    femsys%nzero = nzero ! memorize nzero
! !
! !- 2) compute arrays Col and Row
! !
!   !-------------------------------------------------------------------!
!   !                    A L L O C A T I O N S                          !
!   !-------------------------------------------------------------------!   
!   ! allocate Col & Row:                                               !
!   !                                                                   ! 
!    allocate (femsys%Col(nzero), femsys%Row(nequa+1), stat=err)        !
!   !-------------------------------------------------------------------!  
! 
!    if (err /= 0) call storagetools_error  &
!                       (msg= "pb lors de l'allocation de Col ou de Row", where="csr")
!                   
!    nnz = 0
!    iequa = 0
!    femsys%Row(1) = 1   
!    do ip = 1 , npoin
!       do id = 1, ndofn
!          iddl = femsys%Dof(id,ip)
!          if (iddl > 0) then
!             nzlin = 0
!             iequa = iequa + 1
!             npv = mymesh%lvois(maxnn,ip)    
!             do jv = 1, npv
!                jp = mymesh%lvois(jv,ip)
!                do jd = 1, ndofn
!                   jddl = femsys%Dof(jd,jp)
!                   if (jddl > 0) then
!                      nzlin = nzlin + 1
!                      nnz   = nnz + 1
!                      femsys%Col(nnz) = jddl
!                   end if
!                end do 
!             end do
!             if (iequa /= iddl) then                
!              !!  print*,'ip,iddl,npv,lvois:',ip,iddl,npv,(mymesh%lvois(jv,ip),jv=1,npv)
!              !!  call storagetools_error (msg= "iequa /=  iddl", where="csr")
!             end if
!             femsys%Row(iddl+1) = femsys%Row(iddl) + nzlin
!          end if
!       end do
!    end do   
! 
!    if (iequa /= nequa) then  
!       print*, 'nnz, nequa, iequa =',nnz,nequa,iequa
!       call storagetools_error (msg= "iequa /= nequa", where="csr")
!    end if  	  
! !
! !- sorts column indices in each row
! !	  	  
!    allocate(iwork(max(nequa,2*nzero)),stat=err)
!     
!    if (err /= 0) call storagetools_error &
!                       (msg= "pb lors de l'allocation de iwork", where="csr")        
!     
!    call sparskit_csort ( nequa, dum, femsys%col, femsys%row, iwork, .false. )
!    deallocate(iwork)
! 
!    if (verbosity(3) >= 1) then
!       jv = nzero*Rkind; jp = nzero*Ikind; jd = (nequa+1)*Ikind
!       fmt='(/," Resume de l''occupation memoire necessaire a la matrice (stockage CSR):",/)'
!       write(*,fmt)
!       fmt = '(3x,"- tableau Mat",2x,": ",i10," reels   x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Rkind,jv
!       fmt = '(3x,"- tableau Col",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Ikind,jp
!       fmt = '(3x,"- tableau Row",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nequa+1,Ikind,jd       
!       fmt = '(/,3x,"  Total      ",2x,": ",i10," octets"," (",f0.2," Mo)"/)'
!       write(*,fmt) jv+jp+jd,(jv+jp+jd)/1e6
!    end if   
! 	  
!    END SUBROUTINE storagetools_csr0    
! 
! 
! !=============================================================================================
!    SUBROUTINE storagetools_CSRsym1 (mymesh, femsys)
! !=============================================================================================
!    type(mesh_t  ), intent(in    )  :: mymesh
!    type(linsys_t), intent(in out)  :: femsys 
! !---------------------------------------------------------------------------------------------
! !  Determine the matrix profil for symetric csr storage format  
! !
! !  Initial Version     : N.C., 2012.
! !  Modified and adapted: R.H., 2015
! !  New Version         : R.H., 2020. (uses femsys % dof2node array)
! !
! !  Notice:
! !
! !   considere the n x n symmetric matrix
! !
! !                       |  A11   .   A13   .    .   A16  |
! !                       |       A22   0   A24   .    .   |
! !                 A =   |            A33  A34   .    .   |      n = 6
! !                       |                 A44   .    .   |
! !                       |                      A55  A56  |
! !                       |                           A66  |
! !
! !   where dots stand for zero elements.
! !
! !   In symmetric csr format, the upper triangle of A is stored in an array M of length nzero 
! !   as follows (in this version, column indices in each row are sorted in increasing order
! !   by calling the sparskit routine csort (at the end of this routine)):
! !
! !   M   = [ A11, A13, A16 | A22, A24 | A33, A34 | A44 | A55, A56 |   A66 ]
! !             1    2    3     4    5     6    7     8     9   10      11 
! !            
! !   The array Col of length nzero contains the column index in M of each element of A:
! !
! !   Col = [   1,   3,   6 |   2,   4 |   3,   4 |   4 |   5,   6 |     6 ]
! !
! !   and the array Row of length n+1 stores the index into M of the first nonzero element 
! !   in each row of A:
! !
! !   Row = [   1,              4,         6,         8,    9,          11,  12]
! !             
! !   We have then, nzero = Row(n+1) - 1 and the elements of the ith row of A are located in M 
! !   between the indexes Row(i) and Row(i+1)-1.
! !
! !   Note that since each row are sorted by column, the diagonal entries A(i,i) of A are 
! !   located in M at Row(i):  M(Row(i)) = A(i,i),  i = 1, ..., n
! ! 
! !   M, Col and Row are saved in the fields %Mat, %Col and %Row of the structure femsys, 
! !   respectively.   
! !---------------------------------------------------------------------------------------------
! 	
! !- local variables: --------------------------------------------------------------------------
!    integer  (Ikind )  :: ip, jp, id, iv, nzlin, npv, ieq, jeq, err
!    real     (Rkind )  :: dum(1)
!    character(len=90)  :: fmt
!    integer  (Ikind ), allocatable :: iwork(:)
! !---------------------------------------------------------------------------------------------
! 
! ! 
! !- 1) determine the maximum number of non-zero terms in the matrix and compute %Row
! !      (adresses of the begining of each row)
! !
!   !-------------------------------------------------------------------!
!   !                    A L L O C A T I O N S                          !
!   !-------------------------------------------------------------------!   
!   ! allocate Row:                                                     !
!   !                                                                   ! 
!    allocate (femsys%Row(nequa+1), stat=err)                           !
!   !-------------------------------------------------------------------!  
! 
!    if (err /= 0) &
!       call storagetools_error (msg= "Memory allocation error for femsys%Row", where="csrsym")
! 
!    nzero = 0
!    femsys%Row(1) = 1
!    do ieq = 1, nequa
!       ip = femsys%dof2node(2,ieq)    ! the node corresponding to this dof
!       npv = mymesh%lvois(maxnn,ip)   ! number of nodes attached to this node
!       nzlin = 0
!       do iv = 1, npv
!          jp = mymesh%lvois(iv,ip)
!          do id = 1, ndofn
!             jeq = femsys%dof(id,jp)
!             if (jeq > 0 .and. jeq >= ieq) nzlin = nzlin + 1
!          end do
!       end do
!       femsys%Row(ieq+1) = femsys%Row(ieq) + nzlin
!       nzero = nzero + nzlin
!    end do
!     
!    femsys%nzero = nzero ! memorize nzero
! !
! !- 2) compute arrays column and row
! !
!   !-------------------------------------------------------------------!
!   !                    A L L O C A T I O N S                          !
!   !-------------------------------------------------------------------!   
!   ! allocate Col & Row:                                               !
!   !                                                                   ! 
!    allocate (femsys%Col(nzero),  stat=err)                            !
!   !-------------------------------------------------------------------!    
!    
!    if (err /= 0) &
!       call storagetools_error (msg= "Memory allocation error for femsys%Col", where="csrsym")
!                   
!    nzero = 0
!    do ieq = 1, nequa
!       ip = femsys%dof2node(2,ieq)
!       npv = mymesh%lvois(maxnn,ip)
!       do iv = 1, npv
!          jp = mymesh%lvois(iv,ip)
!          do id = 1, ndofn
!             jeq = femsys%dof(id,jp)
!             if (jeq > 0 .and. jeq >= ieq) then
!                nzero = nzero + 1
!                femsys%Col(nzero) = jeq
!             end if
!          end do
!       end do
!    end do
! !
! !- sorts column indices in each row
! !	  	  
!    allocate(iwork(max(nequa,2*nzero)),stat=err)
!     
!    if (err /= 0) &
!       call storagetools_error (msg= "Memory allocation error for femsys%iwork", where="csrsym")
!     
!    call sparskit_csort ( nequa, dum, femsys%col, femsys%row, iwork, .false. )
!    deallocate(iwork)
! 
!    if (verbosity(3) >= 1) then
!       iv = nzero*Rkind; ip = nzero*Ikind; id = (nequa+1)*Ikind
!       fmt='(/," Resume de l''occupation memoire necessaire a la matrice (stockage CSR):",/)'
!       write(*,fmt)
!       fmt = '(3x,"- tableau Mat",2x,": ",i10," reels   x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Rkind,iv
!       fmt = '(3x,"- tableau Col",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Ikind,ip
!       fmt = '(3x,"- tableau Row",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nequa+1,Ikind,id       
!       fmt = '(/,3x,"  Total      ",2x,": ",i10," octets"," (",f0.2," Mo)"/)'
!       write(*,fmt) iv+ip+id,(iv+ip+id)/1e6
!    end if   
! 	      
!    END SUBROUTINE storagetools_CSRsym1
! 
! 
! !=============================================================================================
!    SUBROUTINE storagetools_csrsym0 (mymesh, femsys)
! !=============================================================================================
!    type(mesh_t  ), intent(in    )  :: mymesh
!    type(linsys_t), intent(in out)  :: femsys 
! !---------------------------------------------------------------------------------------------
! !  Determine the matrix profil for symetric csr storage format  
! !
! !  Initial Version: N.C., 2012.
! !  Modified and adapted: R.H., 2015
! !
! !  Notice:
! !
! !   considere the n x n symmetric matrix
! !
! !                       |  A11   .   A13   .    .   A16  |
! !                       |       A22   0   A24   .    .   |
! !                 A =   |            A33  A34   .    .   |      n = 6
! !                       |                 A44   .    .   |
! !                       |                      A55  A56  |
! !                       |                           A66  |
! !
! !   where dots stand for zero elements.
! !
! !   In symmetric csr format, the upper triangle of A is stored in an array M of length nzero 
! !   as follows (in this version, column indices in each row are sorted in increasing order
! !   by calling the sparskit routine csort (at the end of this routine)):
! !
! !   M   = [ A11, A13, A16 | A22, A24 | A33, A34 | A44 | A55, A56 |   A66 ]
! !             1    2    3     4    5     6    7     8     9   10      11 
! !            
! !   The array Col of length nzero contains the column index in M of each element of A:
! !
! !   Col = [   1,   3,   6 |   2,   4 |   3,   4 |   4 |   5,   6 |     6 ]
! !
! !   and the array Row of length n+1 stores the index into M of the first nonzero element 
! !   in each row of A:
! !
! !   Row = [   1,              4,         6,         8,    9,          11,  12]
! !             
! !   We have then, nzero = Row(n+1) - 1 and the elements of the ith row of A are located in M 
! !   between the indexes Row(i) and Row(i+1)-1.
! !
! !   Note that since each row are sorted by column, the diagonal entries A(i,i) of A are 
! !   located in M at Row(i):  M(Row(i)) = A(i,i),  i = 1, ..., n
! ! 
! !   M, Col and Row are saved in the fields %Mat, %Col and %Row of the structure femsys, 
! !   respectively.   
! !---------------------------------------------------------------------------------------------
! 	
! !- local variables: --------------------------------------------------------------------------
!    integer  (Ikind )  :: ip, id, iddl, nzlin, npv, jv, jp, jd, jddl, nnz, err,iequa
!    real     (Rkind )  :: dum(1)
!    character(len=90)  :: fmt
!    integer  (Ikind ), allocatable :: iwork(:)
! !---------------------------------------------------------------------------------------------
! 
! ! 
! !- 1) determine the maximum number of non-zero terms in the matrix
! !
!    nzero = 0
!    do ip = 1 , npoin
!       do id = 1, ndofn
!          iddl = femsys%Dof(id,ip)
!          if (iddl > 0) then
!             nzlin = 0
!             npv = mymesh%lvois(maxnn,ip)
!             do jv = 1, npv
!                jp = mymesh%lvois(jv,ip)
!                do jd = 1, ndofn
!                   jddl = femsys%Dof(jd,jp)
!                   if (jddl > 0 .and. jddl >= iddl) nzero = nzero + 1
!                end do 
!             end do 
!          end if 
!      end do  
!    end do
!     
!    femsys%nzero = nzero ! memorize nnz	      
! !
! !- 2) compute arrays column and row
! !
!         
!   !-------------------------------------------------------------------!
!   !                    A L L O C A T I O N S                          !
!   !-------------------------------------------------------------------!   
!   ! allocate Col & Row:                                               !
!   !                                                                   ! 
!    allocate (femsys%Col(nzero), femsys%Row(nequa+1), stat=err)        !
!   !-------------------------------------------------------------------!    
!    
!    if (err /= 0) call storagetools_error   &
!                       (msg= "pb lors de l'allocation de Col ou de Row", where="csrsym")
!                   
!    nnz = 0
!    iequa = 0
!    femsys%Row(1) = 1   
!    do ip = 1 , npoin
!       do id = 1, ndofn
!          iddl = femsys%Dof(id,ip)
!          if (iddl > 0) then
!             nzlin = 0
!             iequa = iequa + 1
!             npv = mymesh%lvois(maxnn,ip)    
!             do jv = 1, npv
!                jp = mymesh%lvois(jv,ip)
!                do jd = 1, ndofn
!                   jddl = femsys%Dof(jd,jp)
!                   if (jddl > 0 .and. jddl >= iddl) then
!                      nzlin = nzlin + 1
!                      nnz   = nnz + 1
!                      femsys%Col(nnz) = jddl
!                   end if
!                end do 
!             end do
!             if (iequa /= iddl) then
!                print*,'ip,iddl,npv,lvois:',ip,iddl,npv,(mymesh%lvois(jv,ip),jv=1,npv)
!                call storagetools_error (msg= "iequa /=  iddl", where="csrsym")
!             end if
!             femsys%Row(iddl+1) = femsys%Row(iddl) + nzlin
!          end if
!       end do
!    end do   
!      
!    if (iequa /= nequa) then
!       print*, 'nnz, nequa, iequa =',nnz,nequa,iequa
!       call storagetools_error (msg= "iequa /= nequa", where="csrsym")
!    end if
! !
! !- sorts column indices in each row
! !	
!    allocate(iwork(max(nequa,2*nzero)),stat=err)
!     
!    if (err /= 0) call storagetools_error &
!                       (msg= "pb lors de l'allocation de iwork", where="csrsym")    
!     
!    call sparskit_csort ( nequa, dum, femsys%col, femsys%row, iwork, .false. )
!     
!    deallocate(iwork)
! 
!    if (verbosity(3) >= 1) then
!       jv = nzero*Rkind; jp = nzero*Ikind; jd = (nequa+1)*Ikind
!       fmt='(/," Resume de l''occupation memoire necessaire a la matrice (stockage CSRsym):",/)'
!       write(*,fmt)
!       fmt = '(3x,"- tableau Mat",2x,": ",i10," reels   x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Rkind,jv
!       fmt = '(3x,"- tableau Col",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nzero,Ikind,jp
!       fmt = '(3x,"- tableau Row",2x,": ",i10," entiers x ",i0," = ",i10, " octets")'
!       write(*,fmt)nequa+1,Ikind,jd       
!       fmt = '(/,3x,"  Total      ",2x,": ",i10," octets"," (",f0.2," Mo)"/)'
!       write(*,fmt) jv+jp+jd,(jv+jp+jd)/1e6
!    end if   
!     
!    END SUBROUTINE storagetools_csrsym0


! !=============================================================================================
!    SUBROUTINE storagetools_error (uerr, msg, where)
! !=============================================================================================
!    integer  (Ikind), intent(in), optional :: uerr
!    character(len=*), intent(in), optional :: msg, where
! !---------------------------------------------------------------------------------------------    
! !  Prints an error message and stops
! !---------------------------------------------------------------------------------------------    
!     
! !- local variables: --------------------------------------------------------------------------
!    integer  (Ikind ) :: ue
! !---------------------------------------------------------------------------------------------            
! 
!    if (present(uerr)) then 
!      ue = uerr
!    else 
!      ue = 6
!    end if    
!        
!    write(ue,'(/,a)')'Error (storagetools_error):'
!     
!    if (present(where)) write(ue,'(a)')'--> in storagetools_'//trim(adjustl(where))
!         
!    if (present(msg)) write(ue,'(a)')'--> '//trim(msg)
!     
!    write(ue,'(a,/)')'--> STOP'
!         
!    stop
! 
!    END SUBROUTINE storagetools_error

END MODULE storagetools_m