! ifort  -check all -traceback -gen-interfaces -warn interfaces -O0 -I ../../mod/ifort test_leftCauchyGreen.f90 ../../obj/ifort/def_precision.o  ../../obj/ifort/param.o ../../obj/ifort/util.o ../../obj/ifort/quadrature.o ../../obj/ifort/element.o  -o lcg


program test
   use def_precision
   use param
   use quadrature
   use element
   use util
   
   implicit none
   
   ndime  = 2; nnode = 3; nipts = 1
   
   call truc
   
      
   contains
   
   subroutine truc
   
   real   (Rprec), parameter :: o = 1.0_Rprec
   
   real(Rprec) :: x0(ndime,nnode), x(ndime,nnode), F(ndime,ndime), b(ndime,ndime)
   real(Rprec) :: xIP(ndime,nipts), wIP(nipts)
   real(Rprec) :: shp(nnode,nipts), der(ndime,nnode,nipts), elcar(ndime,nnode), eljac, detF
   
   
   integer(Iprec) :: ip, i
   
   
   x0(:,1) = [0,0]*o ; x0(:,2) = [ 4,0]*o ; x0(:,3) = [ 0,3]*o
   x (:,1) = [2,3]*o ; x (:,2) = [10,3]*o ; x (:,3) = [10,9]*o
   
   xIP(:,1) = [1,1]/3.0_Rprec

   call quadrature_driver ( xIP, wIP, nipts, ndime, "triangle" )  

   ip = 1
   
      call element_driver ( "tria03", ndime, nnode, pnt=xIP(:,ip), &
                            shp=shp(:,ip), drv=der(:,:,ip)  )
      call element_jacob ( eljac, x, der(:,:,ip), elcar, ndime, nnode )
      call modelsNeoHookean_LeftCauchyGreen ( elcar, x0, b, F, detF )
      
      print*,'xIP:',xIP(:,ip)
      print*
      do i = 1, nnode
         print '(a,i0,a,g0,",",1x,g0)', 'dN',i,'/dksi = ',der(:,i,ip)
      end do
      print*
      do i = 1, nnode
         print '(a,i0,a,g0,",",1x,g0)', 'dN',i,'/dx = ',elcar(:,i)
      end do      
      print*,'F='
      do i =  1, ndime
         print*,F(i,:)
      end do
      print*,'detF=',detF
      print*,'b='
      do i = 1, ndime
         print*,b(i,:)
      end do
        
   
   end subroutine truc
        
!=============================================================================================
   SUBROUTINE modelsNeoHookean_LeftCauchyGreen ( dNdx, x0, b, F, detF )
!=============================================================================================
   real(Rprec), intent(in    ) :: dNdx(:,:), x0(:,:)
   real(Rprec), intent(   out) :: b(:,:), F(:,:), detF
!--------------------------------------------------------------------------------------------- 
!  Computes the transformation gradient (F), the left Cauchy-Green tensor (b) and the volumic
!  ratio (detF) given:
!  . the cartesian derivatives (w.r.t current coordinates), dNdx, of the shape functions 
!  . the initial nodal coordinates x0
!
!  The gradient of the inverse transformation is first computed: Finv = sum {x0_i .o. dNdx_i}
!  (.o. the tensor product) then inverted.
!
!  Source: copied and modified from flagshyp08v30, Bonnet & Wood
!--------------------------------------------------------------------------------------------- 

!- local variables: -------------------------------------------------------------------------- 
   real   (Rprec), parameter :: zero = 0.0_Rprec, one = 1.0_Rprec
   integer(Iprec)            :: i, j, n
   real   (Rprec)            :: Finv(ndime,ndime), detFinv
!--------------------------------------------------------------------------------------------- 

!
!- determine F^(-1):
!
   Finv = zero
   
   do n = 1, nnode
      do j = 1, ndime
         do i = 1, ndime
            Finv(i,j) = Finv(i,j) + x0(i,n) * dNdx(j,n)
         end do
      end do
   end do   
   
   call util_detAndInverse ( Finv, ndime, F, detFinv ) 
   
   detF = one /detFinv
!                                     
!- Finally obtains the tensor b = F*F^T
!  
   b = matmul ( F, transpose(F) )
      
   END SUBROUTINE modelsNeoHookean_LeftCauchyGreen
   
!=============================================================================================
   SUBROUTINE modelsNeoHookean_LeftCauchyGreen0 ( dNdx, x0, b, F, detF )
!=============================================================================================
   real(Rprec), intent(in    ) :: dNdx(:,:), x0(:,:)
   real(Rprec), intent(   out) :: b(:,:), F(:,:), detF
!--------------------------------------------------------------------------------------------- 
!  Computes the transformation gradient (F), the left Cauchy-Green tensor (b) and the volumic
!  ratio (detF) given:
!  . the cartesian derivatives (w.r.t current coordinates), dNdx, of the shape functions 
!  . the initial nodal coordinates x0
!
!  The gradient of the inverse transformation is first computed: Finv = sum {x0_i .o. dNdx_i}
!  (.o. the tensor product) then inverted.
!
!  Source: copied from flagshyp08v30, Bonnet & Wood
!--------------------------------------------------------------------------------------------- 

!- local variables: -------------------------------------------------------------------------- 
   real   (Rprec), parameter :: zero = 0.0_Rprec, one = 1.0_Rprec
   integer(Iprec)            :: i, j, n
   real   (Rprec)            :: Finv(3,3)
!--------------------------------------------------------------------------------------------- 

   Finv = zero
!
!- determine F^(-1):
!
   if ( ndime == 2 ) Finv(3,3) = one

   do i = 1, ndime
      do j = 1, ndime
         do n = 1, nnode
            Finv(i,j) = Finv(i,j) + x0(i,n) * dNdx(j,n)
         end do
      end do
   end do
!
!- compute inverse F^(-1):
!
   detF = Finv(1,1) * Finv(2,2) * Finv(3,3) + Finv(1,2) * Finv(2,3) * Finv(3,1) + &
          Finv(1,3) * Finv(2,1) * Finv(3,2) - Finv(1,3) * Finv(2,2) * Finv(3,1) - &
          Finv(1,2) * Finv(2,1) * Finv(3,3) - Finv(1,1) * Finv(2,3) * Finv(3,2)
          
   if ( detF == zero ) then
      detF = one
   else
      detF = one / detF
   end if

   F(1,1) = ( Finv(2,2) * Finv(3,3) - Finv(2,3) * Finv(3,2) ) * detF
   F(1,2) = ( Finv(1,3) * Finv(3,2) - Finv(1,2) * Finv(3,3) ) * detF
   F(1,3) = ( Finv(1,2) * Finv(2,3) - Finv(1,3) * Finv(2,2) ) * detF
   F(2,1) = ( Finv(2,3) * Finv(3,1) - Finv(2,1) * Finv(3,3) ) * detF
   F(2,2) = ( Finv(1,1) * Finv(3,3) - Finv(1,3) * Finv(3,1) ) * detF
   F(2,3) = ( Finv(1,3) * Finv(2,1) - Finv(1,1) * Finv(2,3) ) * detF
   F(3,1) = ( Finv(2,1) * Finv(3,2) - Finv(2,2) * Finv(3,1) ) * detF
   F(3,2) = ( Finv(1,2) * Finv(3,1) - Finv(1,1) * Finv(3,2) ) * detF
   F(3,3) = ( Finv(1,1) * Finv(2,2) - Finv(1,2) * Finv(2,1) ) * detF
!                                     
!- Finally obtains the tensor b = F*F^T
!
   if ( ndime == 2 ) b(3,3) = one
   
   b = matmul ( F, transpose(F) )
      
   END SUBROUTINE modelsNeoHookean_LeftCauchyGreen0   
end