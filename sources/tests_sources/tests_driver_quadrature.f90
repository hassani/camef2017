program tests_driver_quadrature
!
!---- programme qui teste l'appel au pilote des routines de quadrature
!
!     Les tests sont realises sur l'interpolation d'un polynome
!     de degre croissant.
!
!
use def_precision
use quadrature, only : quadrature_driver

implicit none

integer(iprec), parameter :: ndeg = 7, niptri=5, niptetra=3, nipseg=4
integer(iprec)            :: i,j, ideg, ndime, nip, niptot, maxnpi=500
integer(iprec)            :: deg(ndeg), liptri(niptri), choix, liptetra(niptetra)

real(rprec)               :: I_num, I_exa
real(rprec), allocatable  :: p(:,:), w(:)
real(rprec)               :: Coef(1000)

character(len=3)         :: cnip

interface 
    subroutine ptrscreen (deg,ideg,Coef,p,w,ndime,nip,geom)
      use def_precision
      implicit none
      real   (rprec), intent(in) :: Coef(:), p(:,:), w(:)
      integer(iprec), intent(in) :: ndime,deg,ideg,geom,nip
    end subroutine ptrscreen
end interface


deg   = (/ 0, 1, 2, 3, 4, 5, 6 /) ! les differents degres

do i = 1,1000
   Coef(i) = cos(float(i)) ! on se donne un nbr quelconque de coef.
end do                     ! rand ne marche pas avec ifort (il faut mettre use IFPORT)


print*
print*
print*,'Tester les schemas d''integration numerique'
print*,'sur un polynome de degre croissant (jusqu''a 6)'
print*
print*,'Choisir :'
print*,'1 : integration sur le segment [-1,1]'
print*,'2 : integration sur le carre [-1,1]^2'
print*,'3 : integration sur le cube [-1,1]^3'
print*,'4 : integration sur le triangle 0 <= x <= 1, 0 <= y <= 1-x'
print*,'5 : integration sur le tetraedre 0 <= x <= 1, 0 <= y <= 0, 0 <= z <= 1 - x - y'

print"(/,a,$)",'votre choix : '

read*, choix

select case(choix)

  case(1)

  !---------------------------------------------------------------------------------------
  !                          Integration sur le segment [-1,1]
  !---------------------------------------------------------------------------------------
  ndime = 1
  allocate ( p(ndime,maxnpi), w(maxnpi) )

  print*
  print*
  
  do nip = 1, nipseg

     write(cnip,'(i2)')nip

     print *,"***************************************************"
     print *,"** Sur le segment [-1,1] : Formule a "//trim(adjustl(cnip))//" point(s)  **"
     print *,"***************************************************"

     call quadrature_driver (p, w, nip, ndime, "segment") ! coord. et poids des P.I.

     do ideg = 1, ndeg
        call ptrscreen (deg(ideg),ideg,Coef,p,w,ndime,nip,1) ! calcul et affichage
     end do
   
  end do

case(2)

  !---------------------------------------------------------------------------------------
  !                          Integration sur le carre [-1,1]^2
  !---------------------------------------------------------------------------------------
  ndime = 2
  allocate ( p(ndime,maxnpi), w(maxnpi) )

  print*
  print*

  do nip = 1, nipseg

     niptot = nip**2
     write(cnip,'(i2)')niptot

     print *,"***************************************************"
     print *,"** Sur le carre [-1,1]^2 : Formule a "//trim(adjustl(cnip))//" point(s) **"
     print *,"***************************************************"

     call quadrature_driver (p, w, niptot, ndime, "carre") ! coord. et poids des P.I.

     do ideg = 1, ndeg
        call ptrscreen (deg(ideg),ideg,Coef,p,w,ndime,niptot,2) ! calcul et affichage
     end do
      
  end do

case(3)

  !---------------------------------------------------------------------------------------
  !                          Integration sur le cube [-1,1]^3
  !---------------------------------------------------------------------------------------
  ndime = 3
  allocate ( p(ndime,maxnpi), w(maxnpi) )

  print*
  print*

  do nip = 1, nipseg

     niptot = nip**3
     write(cnip,'(i2)')niptot

     print *,"***************************************************"
     print *,"** Sur le cube [-1,1]^3 : Formule a "//trim(adjustl(cnip))//" point(s)  **"
     print *,"***************************************************"

     call quadrature_driver (p, w, niptot, ndime, "cube") ! coord. et poids des P.I.

     do ideg = 1, ndeg
        call ptrscreen (deg(ideg),ideg,Coef,p,w,ndime,niptot,2) ! calcul et affichage
     end do
      
  end do

case(4)

  !---------------------------------------------------------------------------------------
  !                          Integration sur le triangle [-1,1]^3
  !---------------------------------------------------------------------------------------
  ndime = 2
  allocate ( p(ndime,maxnpi), w(maxnpi) )

  print*
  print*

  liptri = [1, 3, 4, 7, 9] ! nbr de P.I. disponibles dans la librairie pour un triangle

  do i = 1, niptri

     nip = liptri(i)
     write(cnip,'(i2)')nip

     print *,"***************************************************"
     print *,"** Sur le triangle :     Formule a "//trim(adjustl(cnip))//" point(s)    **"
     print *,"***************************************************"

     call quadrature_driver (p, w, nip, ndime, "triangle") ! coord. et poids des P.I.

     do ideg = 1, ndeg
        call ptrscreen (deg(ideg),ideg,Coef,p,w,ndime,nip,3) ! calcul et affichage
     end do
      
  end do

case(5)

  !---------------------------------------------------------------------------------------
  !                          Integration sur le triangle [-1,1]^3
  !---------------------------------------------------------------------------------------
  ndime = 3
  allocate ( p(ndime,maxnpi), w(maxnpi) )

  print*
  print*

  liptetra = [1, 4, 5] ! nbr de P.I. disponibles dans la librairie pour un tetraedre

  do i = 1, niptetra

     nip = liptetra(i)
     write(cnip,'(i2)')nip

     print *,"***************************************************"
     print *,"** Sur le tetraedre :    Formule a "//trim(adjustl(cnip))//" point(s)    **"
     print *,"***************************************************"

     call quadrature_driver (p, w, nip, ndime, "   tetraedre") ! coord. et poids des P.I.

     do ideg = 1, ndeg
        call ptrscreen (deg(ideg),ideg,Coef,p,w,ndime,nip,3) ! calcul et affichage
     end do
      
  end do
         
case default

  print*,'choix non valide'

end select

end program tests_driver_quadrature

!=============================================================================================    
subroutine ptrscreen (deg,ideg,Coef,p,w,ndime,nip,geom)
!=============================================================================================    

   use def_precision
   implicit none
   real   (rprec), intent(in) :: Coef(:), p(:,:), w(:)
   integer(iprec), intent(in) :: ndime,deg,ideg,geom,nip

   integer(iprec) :: i, j
   real   (rprec) :: I_num, I_exa, err, polx(100), petit=1e-4
   
   character(len=2) :: ok

   interface 
       subroutine polyn (polx,Intg,x,Coef,deg,ndime,geom)
          use def_precision
          implicit none
          real   (rprec), intent(out) :: polx(:), Intg
          real   (rprec), intent(in ) :: x(:,:), Coef(:)
          integer(iprec), intent(in ) :: deg, ndime, geom
       end subroutine polyn
   end interface

!
!-- valeurs du polynome de degre deg aux differents P.I. + valeur exacte de l'integrale :
!
    call polyn (polx, I_exa, p(1:ndime,1:nip), Coef, deg, ndime, geom)
!
!-- calcul numerique de l'integrale + calcul de l'erreur relative :
!
    I_num = dot_product(polx(1:nip),w(1:nip)); err = 100.*abs(I_num-I_exa)/abs(I_exa)
!
!-- affichage :
!
    ok = 'KO'
    if (err < petit) ok = 'ok'
    write(*,1) ideg, deg
    write(*,2)I_num, I_exa, err, ok

    1 format(i2,") polynome de degre :",i2)
    2 format("I_num =",e13.5,"   I_exa =",e13.5, "   Erreur = ",f6.2,' %',' ---> ',a2)

end subroutine ptrscreen

!=============================================================================================    
subroutine  polyn (polx,Intg,x,Coef,deg,ndime,geom)
!=============================================================================================    
!
!-- on calcule la valeur du polynome aux differents points x
!   ainsi que son integrale sur le domaine de reference choisi 
!   (segment, carre, cube, triangle, tetraedre)
!
!   Entrees :
!
!   x(i,j) : coordonnee i (i <= ndime) du point numero j
!   Coef   : tableau qui contient un ensemble de valeurs. (coef. du polynome)
!   deg    : le degre du polynome
!   ndime  : nbr de variable (1, 2 ou 3)
!   geom   : geom = 1 pour le carre ou le cube, geom = 2 pour le triangle ou le tetraedre
!
!
!   Sorties :
!
!   polx   : les valeurs du polynomes aux points x
!   Intg   : la valeur de son integrale
!   
!   Note : le polynome est calcule de facon "brute" (le degre n'etant pas tres eleve)
!          
!
    use def_precision
    implicit none

    real   (rprec), intent(out) :: polx(:), Intg
    real   (rprec), intent(in ) :: x(:,:), Coef(:)
    integer(iprec), intent(in ) :: deg, ndime, geom

    integer(iprec) :: i, j, k, l, nx, p
    real(rprec) :: u = 1.0_rprec, Iij, Ijl

    nx = size(x,2)

    polx(1:nx) = 0.0_rprec;   Intg = 0.0_rprec

!---------------------------------------------------------------------------------------------  
    if (ndime == 1) then
!
!--    polynome a une seule variable
!   
       do p = 1, nx
          do i=0, deg
             polx(p) = polx(p) + Coef(i+1) * x(1,p)**i
          end do
       end do

!
!--    son integrale sur [-1,1]
!      
       do i=0, deg, 2
          Intg = Intg + Coef(i+1)/float(i+1)
       end do
       Intg = Intg*2.0_rprec
      
       return
    end if
   
!---------------------------------------------------------------------------------------------  
    if (ndime == 2) then
!
!--    polynome a deux variables
!   
       do p = 1, nx
          k=0
          do i=0, deg
             do j=0, deg
                k = k+1
                if (i+j <= deg) then
                   polx(p) = polx(p) + Coef(k) * (x(1,p)**i) * (x(2,p)**j)
                end if
             end do
          end do
       end do
!
!--    calcul de son integrale
!   
       if (geom == 2) then  ! sur le carre {(x,y); 0<=x<=1, 0<=y<=1}
      
          Intg = 0.0_rprec
          k = 0
          do i = 0, deg
             do j = 0, deg
                k = k + 1
                if (i+j <= deg) then
                   Intg = Intg + Coef(k) * ( u-(-u)**(j+1) ) *     &
                                           ( u-(-u)**(i+1) ) /     &
                                           float( (i+1)*(j+1) )
                end if
             end do
          end do
         
          return
         
       end if     
   
       if (geom == 3) then ! sur le triangle {(x,y); 0<=x<=1, 0<=y<=1-x}
      
          Intg = 0.0_rprec
          k = 0
          do i = 0, deg
             do j = 0, deg
                call truc(i,j+1,Iij) 
                k = k + 1
                if (i+j <= deg) then
                   Intg = Intg + Coef(k)*Iij/float(j+1)
                end if
             end do
          end do
       end if     
      
       return
            
    end if   
   
!---------------------------------------------------------------------------------------------  
    if (ndime == 3) then
!
!--    polynome a trois variables
!   
       do p = 1, nx
          k = 0
          do i = 0, deg
             do j = 0, deg
                do l = 0, deg
                   k = k + 1
                   if (i+j+l <= deg) then
                      polx(p) = polx(p) + Coef(k) * (x(1,p)**i) * (x(2,p)**j) * (x(3,p)**l)
                   end if
                end do
             end do
          end do
       end do
!
!--    calcul de son integrale 
!      
       if (geom == 2) then  ! sur le cube {(x,y,z); 0<=x<=1, 0<=y<=1}, 0<=z<=1}
      
          Intg = 0.0_rprec
          k = 0
          do i = 0, deg
             do j = 0, deg
                do l = 0, deg
                   k = k + 1
                   if (i+j+l <= deg) then
                      Intg = Intg + Coef(k) * ( u-(-u)**(j+1) ) *        &
                                              ( u-(-u)**(i+1) ) *        &
                                              ( u-(-u)**(l+1) ) /        &
                                              float( (i+1)*(j+1)*(l+1) )
                   end if
                end do   
             end do
          end do
          
          return
         
       end if  
   
       if (geom == 3) then ! sur le tetraedre {(x,y); 0<=x<=1, 0<=y<=1-x, 0<=z<=1-x-y}
      
          Intg = 0.0_rprec
          k = 0
          do i = 0, deg
             do j = 0, deg
                do l = 0, deg
                   call truc(j,l+1  ,Ijl) ; call truc(i,j+l+2,Iij)
                   k = k + 1
                   if (i+j+l <= deg) then
                      Intg = Intg + Coef(k)*Iij*Ijl/float(l+1)
                   end if
                end do
             end do
          end do
         
          return
         
       end if 
                   
    end if   

end subroutine polyn

!=============================================================================================    
subroutine truc (p,q,Ipq)
!=============================================================================================    
!
!-- integrale de  f(x) = x^p (1-x)^q   entre 0 et 1
!
!            ( = p! q! / (p + q + 1) ! )

    use def_precision
    integer(iprec) :: p, q, i, facq, facp
    real   (rprec) :: Ipq

    facq = 1
    do i = 1, q
       facq = facq*i
    end do

    Ipq = 1.0_rprec
    do i = p+1, p+q+1
       Ipq = Ipq*i
    end do
    Ipq = float(facq) / Ipq   
   
end subroutine truc

