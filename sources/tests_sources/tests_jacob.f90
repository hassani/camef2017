program tests_jacob
!
!-- programme qui teste le calcul des derivees cartesiennes des fonctions de forme
!
!   Le test consiste a deformer l'element de reference par une transformation
!   affine de matrice gradient F et de translation c que l'on se donne a l'avance :
!
!                 elcod = F * cdloc + c
!
!   cdloc : coordonnees des noeuds de l'element de reference
!   elcod : coordonnees de l'element deforme
!
!   et a comparer le calcul des derivees cartesiennes donne par la routine element_jacob
!    avec le calcul theorique donne par 
!
!					F^(-T) * drloc
!
!   ou drloc sont les derivees locales  
!
!   Attention : det(F) doit etre > 0 
!               (en cas de jacobien nulle ou negative, element_jacob revoie un message
!                d'erreur et ne calcule pas les derivees cartesiennes)
!
    use def_precision
    use element, only : element_driver, element_jacob
    
    implicit none
    
    real(rprec) :: F3d(3,3), c(3), ptest(3)
    
    real(rprec), allocatable :: F(:,:), Finv(:,:)
    real(rprec), allocatable :: cdloc(:,:), elcod(:,:), drloc(:,:), & 
                                elcar(:,:), dtheo(:,:), shape(:)
    
    real(rprec) :: djacb, detF, petit = 1.0d-15
    
    integer(iprec) :: err, info(2)
    
    integer(iprec) :: ndime, nnode, i, id, in
    
    character(len=6) :: listelt(12)=['Segm02','Segm03','Tria03','Tria04' , &
                                     'Tria06','Tria07','Quad04','Quad08' , &
                                     'Quad09','Tetr04','Tetr10','Hexa08'], &
                                      elt,ok
    
    interface
       subroutine inversemat (A,B,det,n,err)
          use def_precision
    
          integer(iprec), intent(in ) :: n
          integer(iprec), intent(out) :: err
          real   (rprec), intent(in ) :: A(:,:)
          real   (rprec), intent(out) :: B(:,:), det
          
       end subroutine inversemat
    end interface
    
    print*
    print*,'Test du calcul des derivees cartesiennes (appel a element_jacob)'
    print*,'pour tous les elements de la librairie'
    print*
!
!-- on se donne une transformation quelconque
!    
    F3d(1,:) = [ 1.00 + 0.78 ,  1.34        ,       -1.25 ]
    F3d(2,:) = [-0.84        ,  1.00 - 0.52 ,        0.25 ]
    F3d(3,:) = [ 0.09        ,  2.05        ,  1.00 + 0.76]
    
    c = [-0.09, 0.56, 0.34]

    ptest = [0.10, 0.70, 0.15] ! on choisit un point ou l'on fera les calculs des derivees

    do i = 1, size(listelt)
    
       elt = listelt(i) ! nom de l'element
!
!--    on interroge l'element concerne pour connaitre le nbr de noeuds et la dimension
!       
       call element_driver (namelem = elt, ndime = 1, nnode = 1, nnd=info)

       ndime = info(2); nnode = info(1)
    
       if (allocated(F)) deallocate (F,Finv,cdloc,elcod,drloc,elcar,dtheo,shape)
       
       allocate (F(ndime,ndime), Finv(ndime,ndime))
       allocate (cdloc(ndime,nnode), elcod(ndime,nnode))
       allocate (drloc(ndime,nnode), elcar(ndime,nnode), dtheo(ndime,nnode), shape(nnode))
    
       F = F3d(1:ndime,1:ndime)
!
!--    on recupere les coordonnees des noeuds de l'element de reference (cdloc)  
!  
       call element_driver (namelem = elt, ndime=ndime, nnode=nnode, cnd=cdloc)    
!
!--    on "fabrique" l'element deforme en deplacant les noeuds selon la transformation
!      F et la translation c
!
       elcod = matmul(F,cdloc); 
       do in = 1, nnode
          elcod(:,in) = elcod(:,in) + c(1:ndime)
       end do
!
!--    on calcule les derivees locales (drloc) des fonctions de forme au point ptest  
!
       call element_driver (namelem=elt,ndime=ndime,nnode=nnode,     &
                            pnt=ptest(1:ndime),shp=shape,drv=drloc)
!
!--    on calcule les derivees cartesiennes au meme point
!
       call element_jacob (djacb,elcod,drloc,elcar,ndime,nnode)
       
       if (djacb <= 0) then
          print*
          print*,'erreur renvoyee par element_jacob : jacobien nul ou negatif'
          print*
          stop
       end if
!
!--    on compare elcar avec dtheo = F^(-T) * drloc    
!
       call inversemat(F,Finv,detF,ndime,err)
       if (err /= 0) then
          print*,'transformation non inversible'
          print*,'err=',err,'det=',detF
          stop
       end if
       dtheo = matmul(transpose(Finv),drloc)
    
       write(*,1) elt
       write(*,2) ptest(1:ndime)
       do in = 1, nnode
          write(*,3) in
          do id = 1, ndime
             ok = 'KO'
             if (abs(elcar(id,in)-dtheo(id,in)) < petit) ok = 'ok'
             write(*,4) id,elcar(id,in),dtheo(id,in),ok
          end do   
       end do
       ok = 'KO'
       if (abs(djacb-detF) < petit) ok='ok'
       write(*,5)djacb,detF,ok
       
    end do
    
    1 format(/,44('*'),/,'*',12x,'Element : ',a6,14x,'*',/,44('*'))
    2 format('Comparaison derivees calculees (elcar) et',/,'theoriques (dtheo) ',&
      'au point :', 3f6.2)
    3 format('Noeud no. :',i3,/,'   comp.   elcar           dtheo')
    4 format(3x,i1,5x,f13.9,3x,f13.9,3x,' ---> ',a2)
    5 format(/,'Comparaison des jacobiens :',f13.9,1x,f13.9,1x,' ---> ',a2)
    
end program tests_jacob

!=============================================================================================    
subroutine inversemat (A,B,det,n,err)
!=============================================================================================          
!
!-- calcul de l'inverse B de la matrice A au plus 3 x 3
!
    use def_precision
    
    integer(iprec), intent(in ) :: n
    integer(iprec), intent(out) :: err
    real   (rprec), intent(in ) :: A(:,:)
    real   (rprec), intent(out) :: B(:,:), det
    
    real   (rprec)              :: petit = 1.0d-15
    integer(iprec) :: i
    
    if (n < 1 .or. n > 3) then
       write(*,*)'Erreur dans inversemat : n doit etre entre 1 et 3'
       stop
    end if

    err = 0
    
    if (n == 1) then
       det = a(1,1)
       if (abs(det) < petit) then
          err = 1
          return
       else
          b(1,1) = 1.0_rprec/ det
       end if   
       return         
    endif

    if (n == 2) then
       det = a(1,1)*a(2,2) - a(1,2)*a(2,1)
       if (abs(det) < petit) then
          err = 1
          return
       end if
       b(:,1) = [ a(2,2), -a(2,1)] / det
       b(:,2) = [-a(1,2),  a(1,1)] / det
       return
    end if

    if (n == 3) then
       det = a(1,1)*(a(2,2)*a(3,3) - a(3,2)*a(2,3)) +    &
             a(1,2)*(a(3,1)*a(2,3) - a(2,1)*a(3,3)) +    &
             a(1,3)*(a(2,1)*a(3,2) - a(3,1)*a(2,2))
       if (abs(det) < petit) then
          err = 1
          return
       end if
       b(:,1) = [ a(2,2)*a(3,3)-a(2,3)*a(3,2), a(3,1)*a(2,3)-a(2,1)*a(3,3), a(2,1)*a(3,2)-a(3,1)*a(2,2) ]/det
       b(:,2) = [ a(1,3)*a(3,2)-a(1,2)*a(3,3), a(1,1)*a(3,3)-a(1,3)*a(3,1), a(1,2)*a(3,1)-a(3,2)*a(1,1) ]/det
       b(:,3) = [ a(1,2)*a(2,3)-a(1,3)*a(2,2), a(2,1)*a(1,3)-a(2,3)*a(1,1), a(1,1)*a(2,2)-a(1,2)*a(2,1) ]/det

       return
    end if
    
end subroutine inversemat
    
