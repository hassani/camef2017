program tests_driver_interpolation
!
!-- programme qui teste l'appel au pilote des routines de calcul des
!   fonctions d'interpolation et de leurs derivees pour les
!   differents elements de reference de la librairie element.f90
!
!   Les tests sont realises sur l'interpolation d'un polynome
!   de degre croissant (coefficients aleatoires).
!
!   On affiche la valeur 
!         - de la fonction interpolee et la valeur exacte
!         - des derivees de l'interpolee et des valeurs exactes
!   en un point de coordonnees donnees
!
    use def_precision
    use element, only : element_driver

    implicit none

    integer(iprec) :: i,j,idime, ideg, nnode,ndime, nnodx=100
    integer(iprec) :: deg(5)
    integer(iprec) :: nnd(2)
   
    real   (rprec)           :: x,pi,fi,y,t=1.0_rprec/3.0_rprec,fex,p
    real   (rprec)           :: ptest(3), dfex(3), dp(3), Coef(100)
   
    real(rprec), allocatable :: shape(:), fnode(:)
    real(rprec), allocatable :: deriv(:,:) ,xi(:,:), dfnode(:,:)

    character(len=6)         :: namelem

    ptest = (/ 0.1_rprec, 0.7_rprec, 0.1_rprec /)
    deg   = (/ 0, 1, 2, 3, 4 /)

    do i = 1,100
       Coef(i) = cos(float(i)) ! rand() (avec ifort necessite use IFPORT)
    end do

    allocate ( shape(nnodx), fnode(nnodx) )

    !---------------------------------------------------------------------------------------
    !--                                      Elements 1D                                  --
    !---------------------------------------------------------------------------------------
    ndime = 1
    allocate ( deriv (ndime,nnodx), xi(ndime,nnodx), dfnode(ndime,nnodx) )

    print *,"*******************************************"
    print *,"************** Element Segm02 *************"
    print *,"*******************************************"
    nnode = 2
    call element_driver (namelem='segm02',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Segm03 *************"
    print *,"*******************************************"
    nnode = 3
    call element_driver (namelem='Segm03',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    !---------------------------------------------------------------------------------------
    !--                                    Elements 2D                                    --
    !---------------------------------------------------------------------------------------
    ndime = 2
    deallocate (deriv, xi, dfnode)
    allocate ( deriv (ndime,nnodx), xi(ndime,nnodx), dfnode(ndime,nnodx) )

    print *,"*******************************************"
    print *,"************** Element Tria03 *************"
    print *,"*******************************************"
    nnode = 3
    call element_driver (namelem='Tria03',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Tria04 *************"
    print *,"*******************************************"
    nnode = 4
    call element_driver (namelem='Tria04',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Tria06 *************"
    print *,"*******************************************"
    nnode = 6
    call element_driver (namelem='Tria06',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Tria07 *************"
    print *,"*******************************************"
    nnode = 7
    call element_driver (namelem='TRIA07',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Quad04 *************"
    print *,"*******************************************"
    nnode = 4
    call element_driver (namelem='quad04',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Quad08 *************"
    print *,"*******************************************"
    nnode = 8
    call element_driver (namelem='quad08',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Quad09 *************"
    print *,"*******************************************"
    nnode = 9
    call element_driver (namelem='QUAD09',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    !---------------------------------------------------------------------------------------
    !--                                   Elements 3D                                     --
    !---------------------------------------------------------------------------------------
    ndime=3
    deallocate ( deriv, xi, dfnode )
    allocate ( deriv (ndime,nnodx), xi(ndime,nnodx), dfnode(ndime,nnodx) )

    print *,"*******************************************"
    print *,"************** Element Tetr04 *************"
    print *,"*******************************************"
    nnode = 4
    call element_driver (namelem='Tetr04',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do
    print *,"*******************************************"
    print *,"************** Element Tetr10 *************"
    print *,"*******************************************"
    nnode = 10
    call element_driver (namelem='Tetr10',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

    print *,"*******************************************"
    print *,"************** Element Hexa08 *************"
    print *,"*******************************************"
    nnode = 8
    call element_driver (namelem='Hexa08',ndime=ndime,nnode=nnode, &
                         pnt=ptest,shp=shape,drv=deriv,cnd=xi)

    call ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
    print *,"---- Interpolation d''un polynome ----"
    do ideg = 1, 5
       do i=1, nnode
          call polyn (xi(:,i),fnode(i),dfnode(1,i),coef,deg(ideg),ndime)
       end do
       call polyn (ptest,fex,dfex,Coef,deg(ideg),ndime)
       call ptrscreen2 (deg(ideg),ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
    end do

end program tests_driver_interpolation

!=============================================================================================    
subroutine ptrscreen2 (deg,ideg,shape,fnode,deriv,fex,dfex,ndime,nnode)
!=============================================================================================    

    use def_precision
    implicit none

    integer(iprec) :: ndime, nnode, i, j, deg,ideg
    
    real   (rprec) :: shape(nnode), fnode(nnode)
    real   (rprec) :: dfex(ndime)
    real   (rprec) :: deriv(ndime,nnode), xi(ndime,nnode)
    real   (rprec) :: fex
    
    write(*,1) ideg, deg
    write(*,2)dot_product(shape,fnode),fex
    write(*,3)dot_product(deriv(1,:),fnode),dfex(1)
    if (ndime >=2) write(*,4)dot_product(deriv(2,:),fnode),dfex(2)
    if (ndime ==3 )write(*,5)dot_product(deriv(3,:),fnode),dfex(3)

    1 format(i2,") de degre :",i2)
    2 format("sum(N_i * f_i  ) =",f8.5,", fexact   =",f8.5)
    3 format("sum(dxN_i * f_i) =",f8.5,", dxfexact =",f8.5)
    4 format("sum(dyN_i * f_i) =",f8.5,", dyfexact =",f8.5)
    5 format("sum(dzN_i * f_i) =",f8.5,", dzfexact =",f8.5)

end subroutine ptrscreen2

!=============================================================================================    
subroutine ptrscreen1 (xi,ptest,shape,deriv,ndime,nnode)
!=============================================================================================    

    use def_precision
    implicit none

    integer(iprec) :: ndime, nnode, i, j
    
    real   (rprec) :: shape(nnode), ptest(ndime)
    real   (rprec) :: deriv(ndime,nnode), xi(ndime,nnode)

    write(*,1)
    do i = 1,nnode
       write(*,2)i,(xi(j,i),j=1,ndime)
    end do
    if (ndime == 1) write(*,31)(ptest(i),i=1,ndime)
    if (ndime == 2) write(*,32)(ptest(i),i=1,ndime)
    if (ndime == 3) write(*,33)(ptest(i),i=1,ndime)

    do i = 1, nnode
       write(*,2)i,shape(i),(deriv(j,i),j=1,ndime)
    end do
    write(*,4)sum(shape(1:nnode))
    write(*,5)dot_product(shape,xi(1,:)),ptest(1)
    if (ndime >= 2) write(*,6)dot_product(shape,xi(2,:)),ptest(2)
    if (ndime == 3) write(*,7)dot_product(shape,xi(3,:)),ptest(3)

    1  format("coordonnees locales des noeuds :")
    2  format(i2,1x,4(1x,f8.5))
    31 format("fct de forme et derivees au point :",f8.5,/,    &
  &           " i",6x,"N_i",3x,"gradN_i")
    32 format("fct de forme et derivees au point :",2f8.5,/,   &
  &           " i",6x,"N_i",8x,"gradN_i")
    33 format("fct de forme et derivees au point :",3f8.5,/,   &
  &           " i",6x,"N_i",10x,"gradN_i")
    4  format("sum(N_i) =",f8.5)
    5  format("sum(N_i * x_i) =",f8.5,", x =",f8.5)
    6  format("sum(N_i * y_i) =",f8.5,", y =",f8.5)
    7  format("sum(N_i * z_i) =",f8.5,", z =",f8.5)

end subroutine ptrscreen1

!=============================================================================================    
subroutine  polyn (x,p,dp,Coef,deg,ndime)
!=============================================================================================    

    use def_precision
    implicit none
    
    integer(iprec) :: i, j, k, l, deg, ndime

    real   (rprec) :: p
    real   (rprec) :: x(ndime),dp(ndime)
    real   (rprec) :: Coef(100)

    p = 0.0_rprec; dp=0.0_rprec

    if (ndime == 1) then
    
       do i=0, deg
          p = p + Coef(i+1)*x(1)**i
          dp(1) = dp(1) + float(i)*Coef(i+1)*x(1)**(i-1)
       end do
       
       return
       
    end if

    if (ndime == 2) then
    
       k=0
       do i=0, deg
          do j=0, deg
             k = k+1
             if (i+j <= deg) then
                p = p + Coef(k)*(x(1)**i)*(x(2)**j)
                dp(1) = dp(1) + float(i)*Coef(k)*(x(1)**(i-1))*(x(2)**j)
                dp(2) = dp(2) + float(j)*Coef(k)*(x(1)**i)*(x(2)**(j-1))
             end if
          end do
       end do
       
       return
       
    end if

    if (ndime == 3) then
    
       k=0
       do i=0, deg
          do j=0, deg
             do l=0, deg
                k = k+1
                if (i+j+l <= deg) then
                   p = p + Coef(k)*(x(1)**i)*(x(2)**j)*(x(3)**l)
                   dp(1) = dp(1) + float(i)*Coef(k)*(x(1)**(i-1))*(x(2)**j)*(x(3)**l)
                   dp(2) = dp(2) + float(j)*Coef(k)*(x(1)**i)*(x(2)**(j-1))*(x(3)**l)
                   dp(3) = dp(3) + float(l)*Coef(k)*(x(1)**i)*(x(2)**j)*(x(3)**(l-1))
                end if
             end do
          end do
       end do
       
       return
       
    end if

end subroutine polyn



