#include "error.fpp"

!---------------------------------------------------------------------------------------------
! CAMEF-2017, Code d'Apprentissage de la Methode des Elements Finis - OOP version
!---------------------------------------------------------------------------------------------
!
! MODULE: 
!        Reorderings
!
! AUTHOR: 
!        R.H., Universite de Nice - Sophia Antipolis
!
!        Collected algorithms and subroutine from 
!         1) Durocher and Gasper, 1979 : subroutine DG (extracted from code Geodyn - J.P. Vilotte)
!         2) ACM : subroutines GPSKA, B, C, ... (thanks to JH. Prevost, Princeton Univ.)
!
! VERSION:
!        August 2017
!
! DESCRIPTION: 
!        optimizes the dof numbering for bandwidth and profile reduction
!
! USAGE:
!        call reoderings_SetEquationNumbers ( mymesh, method, ndofn, verbos, ldof)
!
!        where . mymesh (in ): is the given mesh structure
!              . method (in ): is a string ('DG' or 'GPSKA') defining the algorithm to be used
!              . ndofn  (in ): is the number of d.o.f. / node
!              . verbos (in ): is integer (verbosity level)
!              . ldof   (out): is an array containing the dof numbers of each node 
!
!---------------------------------------------------------------------------------- R.H. 08/17

MODULE Reorderings_m

   use kindParameters_m
   use err_m
   
   use constants_m, only: IERROR, UERROR, IONE, IZERO
   use util_m     , only: util_string_low
   use defTypes_m , only: mesh_t
   
   implicit none
   
   integer(Ikind) :: verb
   
   private
   public :: reorderings_SetEquationNumbers

CONTAINS

!=============================================================================================
   SUBROUTINE reorderings_SetEquationNumbers ( mymesh, method, verbos, ldof, stat )
!=============================================================================================   
   type     (Mesh_t), intent(in    ) :: mymesh
   character(len=* ), intent(in    ) :: method    
   integer  (Ikind ), intent(in    ) :: verbos
   integer  (Ikind ), intent(in out) :: ldof(:,:)
   type     (err_t ), intent(in out) :: stat   
!---------------------------------------------------------------------------------------------
!  Optimizes the dof numbering for bandwidth and profile reduction
!
!  Inputs:
!   . mymesh: the mesh (structure). Only %lvois is used (node-node connectivities)
!   . method: the method to use. Must be 'dg' or 'gpskca'
!   . verbos: level of verbosity
!   . ldof  : on input an array of size ndofn x npoin where ndofn is the number of d.o.f/node
!             and npoin the number of nodes in the mesh.
!             Moreover, ldof(id,ip) must be < 0 if (id,ip) is a constrained d.o.f.
!
!  Ouput:
!   . ldof: ldof(id,ip) gives the global # of the d.o.f. (id,ip). For a constrained d.o.f., 
!           the original value is unchanged.
!
!---------------------------------------------------------------------------------- R.H. 08/17

!- local variables: -------------------------------------------------------------------------- 
   character(len=*), parameter   :: Here = 'SetEquationNumbers'
   integer  (Ikind), allocatable :: rstart(:), nconnec(:), lconnec(:), permut(:),  &
                                    invperm(:), work(:)
   integer  (Ikind)              :: nwork, maxcn, err, bandwd, space, profil, npoin, ndofn, &
                                    i, ip, ieq, id, irw, ipnew, npv, bandwd0, norph, maxnn
   integer  (i64  )              :: profil0                                 
   character(len=:), allocatable :: meth                                 
!--------------------------------------------------------------------------------------------- 

   if ( stat > IZERO ) return 
   
   ndofn = size(ldof,1)
   npoin = size(ldof,2)
   maxnn = size(mymesh%lvois,1)
   
   meth = trim(adjustl(util_string_low(method)))
   
   if ( meth == 'none' ) return
   
   if ( size(ldof,2) /= npoin ) then
      stat = err_t ( stat=UERROR, where=Here, msg='size(ldof,2) is different from npoin' )
      return
   end if
   
   maxcn = maxnn ; nwork = 6*npoin + 3
   
   verb = verbos
         
   allocate ( rstart(npoin), nconnec(npoin), lconnec(maxcn*npoin), work(nwork), &
              invperm(npoin), permut(npoin), stat = err )

   if ( err /= 0 ) then
      stat = err_t ( stat=IERROR, where=Here, msg='Allocation failure of local arrays' )
      return
   end if

   if ( verb >= 1 ) write(*,'(a)')' Optimizing the dof numbering '   
   
   do i = 1, npoin
      permut(i) = i
   end do
   
!
!- Call node numbering optimization algorithm for bandwidth and profile reduction
!  (note: mesh nodes are not actually renumbered, the computed permutation will serve to
!   number each dof):
! 
   select case ( meth )

      case ('gpsk')
         call GetConnectedNodes ( mymesh%lvois, IZERO, rstart, nconnec, &
                                 lconnec, bandwd0, profil0 )

         call GPSKCA ( npoin , nconnec, rstart, lconnec, .false., nwork, &
                       permut, work   , bandwd,  profil, err    , space )  
      case ('dg')
         call GetConnectedNodes ( mymesh%lvois, IONE, rstart, nconnec, &
                                  lconnec, bandwd0, profil0 )
         
         call DG ( permut, nconnec, lconnec, maxcn, npoin, &
                   min(npoin,max(10_Ikind,npoin/100)) )

      case default
         stat = err_t(stat=UERROR,where=Here,msg='Unexpected renumbering method "'//meth//'"')
         return

   end select

   call indinv ( invperm, permut, npoin ) ! inverse of permut
!
!- Number the d.o.f according to the computed optimized permutation:
!   
   profil = 0 ; bandwd = 0
   norph = 0
   
   ieq = 0
   do ipnew = 1, npoin
      ip = invperm(ipnew)
      
      npv = mymesh%lvois(maxcn,ip)

      if ( npv <= 1 ) then
         ldof(:,ip) =-1 ! for "orphan" nodes
         norph = norph + 1
         cycle
      end if
!
!-    number the ndofn d.o.f. of node ip:
!      
      do id = 1, ndofn
         if ( ldof(id,ip) >= 0 ) then
            ieq = ieq + 1
            ldof(id,ip) = ieq ! d.o.f numbers for the node ip
         end if
      end do     
!
!-    compute the new bandwith and profil:
!      
      irw = 0
      do i = 2, npv
         irw = max( irw,ipnew-permut(mymesh%lvois(i,ip)) )
      end do
      profil = profil + irw
      bandwd = max(bandwd,irw)    
   end do   

   if (verb >= 1) then
      write(*,'(/,a,/)'    ) " In reoderings_SetEquationNumbers:"
      write(*,'(3x,a,i0)'  ) "   . Number of orphan nodes: " , norph 
      write(*,'(3x,a,i0)'  ) "   . Total number of d.o.f.: " , ieq        
      write(*,'(3x,a,a)'   ) "   . Method used           : " , trim(method)
      write(*,'(3x,a,i0)'  ) "   . Original profil       : " , profil0        
      write(*,'(3x,a,i0)'  ) "   . New profil            : " , profil      
      write(*,'(3x,a,i0)'  ) "   . Original bandwidth    : " , bandwd0        
      write(*,'(3x,a,i0)'  ) "   . New bandwidth         : " , bandwd       
   end if   
          
   END SUBROUTINE reorderings_SetEquationNumbers


!=============================================================================================
   SUBROUTINE GetConnectedNodes ( lvois, version, rstart, nconnec, lconnec, bandwd, profil )
!=============================================================================================   
   integer(Ikind), intent(in    ) :: lvois(:,:), version
   integer(Ikind), intent(   out) :: rstart(*), nconnec(*), lconnec(*), bandwd
   integer(i64  ), intent(   out) :: profil
!---------------------------------------------------------------------------------------------    
!
!  Rearranges the node-node connectivities to adapt to renumbering routines GPSKCA or DG and
!  compute the original bandwidth and profil
!
!  Inputs:
!
!     - lvois: 2-rank array (maxcn,npoin) of integers containing the node-node connectivities  
!            
!              lvois(1:maxcn, ip) = [ip, ip_1, ip_2,..., ip_N, 0, 0, ..., 0, N+1]
!
!     - version:  0 or 1 
!
!  Outputs:
! 
!  - nconnec: 1-rank array (npoin) of integers containing the number of node-nodes connections
!
!        nconnec(ip) = the number of nodes attached to node #ip  (=lvois(maxcn,ip)-1)
!
!  - lconnec: node-node connectivities
!
!    if version == 0  
!
!         the nconnec(ip) nodes connected to the node #ip are stored in consecutive locations
!         starting at location k = rstart(ip):              
!            
!                 lconnec(k),  lconnec(k + 1), ..., lconnec(k + nconnec(ip) - 1)
!
!    if version == 1  
!
!         the nconnec(ip) nodes connected to the node #ip are stored in locations starting at
!         k = (ip-1)*maxcn 
!
!                 lconnec(k + 1),  lconnec(k + 2), ..., lconnec(k + nconnec(ip))
!
!---------------------------------------------------------------------------------- R.H. 08/17

!- local variables: --------------------------------------------------------------------------                        
   integer(Ikind) :: i, k, ip, jp, npv, irw, npoin, maxcn
!---------------------------------------------------------------------------------------------
   
   maxcn = size(lvois,dim=1) ; npoin = size(lvois,dim=2)

   nconnec(1:npoin) = 0 ; lconnec(1:maxcn*npoin) = 0 ; bandwd = 0 ; profil = 0
   
   if (version == 0) then
   
      k = 0
      do ip = 1, npoin
         npv = lvois(maxcn,ip)
         nconnec(ip) = npv - 1
         rstart(ip) = k + 1
         
         irw = 0
         do i = 2, npv
            k = k + 1
            jp = lvois(i,ip)
            lconnec(k) = jp
            irw = max(irw,ip-jp)
         end do       
                  
         profil = profil + irw
         bandwd = max(bandwd,irw)      
      end do   

   else
      
      do ip = 1, npoin
         npv = lvois(maxcn,ip)
         nconnec(ip) = npv - 1
         k = (ip-1)*maxcn
         
         irw = 0
         do i = 2, npv
            k = k + 1
            jp = lvois(i,ip)
            lconnec(k) = jp
            irw = max(irw,ip-jp)
         end do    

         profil = profil + irw
         bandwd = max(bandwd,irw)
      end do
      
   end if
    
   END SUBROUTINE GetConnectedNodes
   
   
!=============================================================================================
   SUBROUTINE DG ( joint, jmem, memjt, maxcn, npoin, ntrial )
!=============================================================================================                      
   integer(Ikind),           intent(in    ) :: maxcn, npoin 
   integer(Ikind),           intent(in    ) :: memjt(maxcn*npoin) 
   integer(Ikind),           intent(in out) :: jmem (npoin)   
   integer(Ikind),           intent(   out) :: joint(npoin)
   integer(Ikind), optional, intent(in    ) :: ntrial
!---------------------------------------------------------------------------------------------
!
!  Node numbering optimisation
!
!  note:
!  -----
!        This subroutine trys npoin renumbering schemes where npoin is the number of nodes in
!        the model.
!
!  Source: extracted and modified from Geodyn code (J.-P. Vilotte)
!  Ref.  : Durocher and Gasper, 1979, A versatile two-dimensional mesh generator with 
!          automatic bandwidth reduction, Computer & Structures, Vol. 10, pp 561-575.
!
!
!
!  Inputs:
!     . npoin: number of nodes
!     . maxcn: max. number of node-nodes connections
!     . jmem : jmem(ip) is the number of attached nodes to the node # ip
!     . memjt: memjt(k+i), with k=(ip-1)*maxcn and i=1,...,jmem(ip), are the node #s attached
!              to node # ip
!
!  Ouput:
!     . joint: gives the correspondance between the old and new numberings:
!
!              ip_new = joint(ip_old), ip_old = 1, ..., npoin
!
!---------------------------------------------------------------------------------------------
	
!- local variables: --------------------------------------------------------------------------                     
   integer(Ikind) :: newjt(npoin), lorph(npoin), jnt(npoin)
   integer(Ikind) :: idiff, ipoin, jpoin, npv, i, j, k, ni, minmax, npreel, mmax, k4, k5, jj, &
                     ndiff, jsub, norph, ntry
   character(len=*), parameter :: Here = 'reorderings_DG'                  
!---------------------------------------------------------------------------------------------

   if ( verb >= 2 ) write(*,90)
!      
!- reperer les points "orphelins" (c-a-d, n'intervenant pas dans la connectivite)
!
   norph = 0    
   do ipoin = 1, npoin
      if (jmem(ipoin) .eq. 0) then
         norph = norph + 1
         lorph(ipoin) = 1
      else
         lorph(ipoin) = 0
      end if
   end do    
   
   if ( verb >= 2 ) write(*,91) norph
! 
!- i n i t i a l i z a t i o n
!
   idiff = 0
   do ipoin = 1, npoin
      npv = jmem(ipoin)
      k = (ipoin - 1) * maxcn
      do j = 1, npv
         jpoin = memjt(k+j)
         idiff = max(idiff,jpoin - ipoin)
      end do
   end do

   if ( verb >= 2 ) write(*,92) idiff
        
   minmax = idiff
   npreel = npoin - norph  ! nbr de noeuds "effectifs" 
                           ! (c-a-d sans les "orphelins")
!
!- t r y   n p o i n    r e n u m b e r i n g    s c h e m e s
!
   if ( present(ntrial) ) then
      ntry = ntrial
   else
      ntry = npoin
   end if
   
   do 17 ipoin = 1,  ntry
               
      if (lorph(ipoin) == 1) goto 17
               
      mmax         = 0
      i            = 1
      k            = 1
!
!-    initialize arrays
!
      joint(:) = 0
      newjt(:) = 0
         
      newjt(    1) = ipoin
      joint(ipoin) = 1
      
10    k4 = jmem(newjt(i))            
         if ( k4 == 0 ) go to 12
         jsub = (newjt(i)-1) * maxcn
         do 11 jj = 1,k4
            k5 = memjt(jsub+jj)
            if ( joint(k5) > 0 ) go to 11
            k         = k+1
            newjt(k)  = k5
            joint(k5) = k
            ndiff     = abs(i-k)
            if ( ndiff >= minmax ) goto 17
            if ( ndiff > mmax ) mmax = ndiff
11       continue
         if (k == npreel) go to 13
12       i = i + 1
      go to 10

13    minmax = mmax
         
      ni = 0
      do 14 jpoin = 1,npoin
         if ( lorph(jpoin) == 0 ) then
            jnt(jpoin) = joint(jpoin)
         else
            ni = ni + 1
            jnt(jpoin) = npreel + ni  ! mettre les pts isoles a la fin
         end if
14    continue
17 continue

   if ( minmax .lt. idiff ) go to 19
!
!-    n e w   n u m b e r i n g   k e p t
!
      do 18 jpoin = 1,npoin
      !  jnt(jpoin) = jpoin
      !  jmem(jnt(jpoin)) = jpoin
         joint(jpoin) = jpoin
18    continue

      !joint = jnt
      
      if ( verb >= 2 ) write(*,93)
      
      return
      
19    idiff = minmax
!
!-    w r i t e   n e w   n u m b e r i n g
!
      do 20 ipoin = 1,npoin
         jmem(jnt(ipoin)) = ipoin
20    continue

!
!- i n v e r s e   r e n u m b e r i n g
!
   do 21 jpoin = 1,npoin
      joint(jpoin)     = npoin-jnt(jpoin) + 1
      jnt(jpoin)       = joint(jpoin)
      jmem(jnt(jpoin)) = jpoin
21 continue
   
   if ( verb >= 2 ) write(*,94) idiff

   return
!
!- f o r m a t s
!
90 format(////,8x,'O P T I M I S A T I O N  DE   LA  N U M E R O T A T I O N',//)
91 format(10x,'Nombre de noeuds "orphelins"               : ',i0)
92 format(10x,'Numerotation initiale (diff. nodale maxi.) : ',i0)
93 format(/10x,'N u m e r o t a t i o n  i n i t i a l e  r e t e n u e',//)
94 format(10x,'Nouvelle numerotation (diff. nodale maxi.) : ',i0,//)

   END SUBROUTINE DG
   
   
!=======================================================================
!     ==============================================================
!     ALGORITHM 582, COLLECTED ALGORITHMS FROM ACM.
!     ALGORITHM APPEARED IN ACM-TRANS. MATH. SOFTWARE, VOL.8, NO. 2,
!     JUN., 1982, P. 190.
!     ==============================================================
!
!     GIBBS-POOLE-STOCKMEYER AND GIBBS-KING ALGORITHMS ...
!
!     1.  SUBROUTINES  GPSKCA, GPSKCB, ..., GPSKCQ  WHICH IMPLEMENT
!         GIBBS-POOLE-STOCKMEYER AND GIBBS-KING ALGORITHMS ...
!
!     2.  SAMPLE DRIVER PROGRAM
!
!     3.  SAMPLE TEST PROBLEMS
!
!     4.  OUTPUT PRODUCED BY SAMPLE DRIVER ON SAMPLE TEST PROBLEMS
!
!     ALL OF THE ABOVE ARE IN 80 COLUMN FORMAT.  THE FIRST TWO
!     SECTIONS HAVE SEQUENCE NUMBERS IN COLUMNS 73 TO 80.  THE
!     THIRD SECTION HAS DATA IN ALL 80 COLUMNS.  THE LAST SECTION
!     HAS CARRIAGE CONTROL CHARACTERS IN COLUMN 1 AND DATA IN ALL
!     80 COLUMNS.
!
!     THESE FOUR SECTIONS OF THE FILE ARE SEPARATED BY SINGLE CARDS
!     OF THE FORM  'C === SEPARATOR ===' IN COLUMNS 1 TO 19
!
!     ==============================================================
!
! === SEPARATOR ===  BEGINNING OF GPS AND GK ALGORITHMS
!=============================================================================================
   SUBROUTINE GPSKCA  (N     , DEGREE, RSTART, CONNEC, OPTPRO, WRKLEN, &
                       PERMUT, WORK  , BANDWD, PROFIL, ERROR , SPACE   )
!=============================================================================================
                            
!
!     ==================================================================
!     ==================================================================
!     =                                                                =
!     = B A N D W I D T H    OR    P R O F I L E    R E D U C T I O N  =
!     =        FOR A SPARSE AND (STRUCTURALLY) SYMMETRIC MATRIX,       =
!     =                         USING EITHER                           =
!     =                                                                =
!     =   THE GIBBS-POOLE-STOCKMEYER ALGORITHM (BANDWIDTH REDUCTION)   =
!     =                               OR                               =
!     =          THE GIBBS-KING ALGORITHM (PROFILE REDUCTION)          =
!     =                                                                =
!     ==================================================================
!     ==================================================================
!     =     THIS CODE SUPERSEDES TOMS ALGORITHMS 508 AND 509 IN THE    =
!     =     COLLECTED ALGORITHMS OF THE ACM (CALGO).                   =
!     ==================================================================
!     ==================================================================
!
!     -------------------
!     P A R A M E T E R S
!     -------------------
!
      integer(Ikind), intent(in    ) :: N, RSTART(N), WRKLEN, CONNEC(*)
      logical       , intent(in    ) :: OPTPRO
      integer(Ikind), intent(in out) :: DEGREE(N), PERMUT(N) 
      integer(Ikind), intent(out   ) :: WORK(WRKLEN), BANDWD, PROFIL, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(1), PERMUT(N), WORK(WRKLEN)
!
!
!     ------------------------------------------------------------------
!
!     INPUT PARAMETERS:
!     ----- ----------
!
!         N      -- THE DIMENSION OF THE MATRIX
!
!         DEGREE,
!         RSTART,
!         CONNEC -- DESCRIBE THE STRUCTURE OF THE SPARSE MATRIX.
!                   DEGREE(I) SPECIFIES THE NUMBER OF NON-ZERO
!                   OFF-DIAGONAL ENTRIES IN THE  I-TH  ROW OF THE
!                   SPARSE MATRIX.  THE COLUMN INDICES OF THESE
!                   ENTRIES ARE GIVEN IN CONSECUTIVE LOCATIONS IN
!                   CONNEC, STARTING AT LOCATION  RSTART(I).
!                   IN OTHER WORDS, THE INDICES OF THE NON-ZERO
!                   OFF-DIAGONAL ELEMENTS OF THE  I-TH  ROW ARE FOUND
!                   IN:
!                       CONNEC (RSTART(I)),
!                       CONNEC (RSTART(I) + 1),
!                                . . .
!                       CONNEC (RSTART(I) + DEGREE(I) - 1)
!
!                   DIMENSIONS:
!                       RSTART IS DIMENSION  N  (OR LONGER).
!                       DEGREE IS DIMENSION  N  (OR LONGER).
!                       CONNEC IS DIMENSION ROUGHLY THE NUMBER OF NON-
!                              ZERO ENTRIES IN THE MATRIX.
!
!         OPTPRO -- .true. IF REDUCING THE PROFILE OF THE MATRIX
!                          IS MORE IMPORTANT THAN REDUCING THE
!                          BANDWIDTH
!                   .false. IF BANDWIDTH REDUCTION IS MOST IMPORTANT
!
!         WRKLEN -- THE  ACTUAL  LENGTH OF THE VECTOR  WORK  AS SUPPLIED
!                   BY THE USER.  SEE THE DISCUSSION OF THE WORKSPACE
!                   'WORK'  BELOW FOR TYPICAL STORAGE REQUIREMENTS.
!                   THE VALUE OF  WRKLEN  WILL BE USED TO ENSURE THAT
!                   THE ROUTINE WILL NOT USE MORE STORAGE THAN IS
!                   AVAILABLE.  IF NOT ENOUGH SPACE IS GIVEN IN  WORK
!                   TO PERMIT A SOLUTION TO BE FOUND, THE  ERROR  FLAG
!                   WILL BE SET AND FURTHER COMPUTATION STOPPED.
!
!
!     INPUT AND OUTPUT PARAMETER:
!     ----- --- ------ ---------
!
!         PERMUT -- ON INPUT, AN ALTERNATIVE REORDERING FOR THE
!                   ROWS AND COLUMNS OF THE MATRIX.  PERMUT(I) GIVES
!                   THE POSITION IN WHICH ROW AND COLUMN  I  SHOULD
!                   BE PLACED TO REDUCE THE BANDWIDTH OR THE PROFILE.
!                   IF THE USER HAS NO ALTERNATIVE TO THE NATURAL
!                   ORDERING IMPLICIT IN  DEGREE,  RSTART  AND  CONNEC,
!                   HE SHOULD INITIALIZE  PERMUT  TO BE THE IDENTITY
!                   PERMUTATION  PERMUT(I) = I .
!
!                   ON OUTPUT,  PERMUT  WILL CONTAIN THE PERMUTATION
!                   FOR REORDERING THE ROWS AND COLUMNS WHICH REDUCES
!                   THE BANDWIDTH AND/OR PROFILE.  THE RESULT WILL BE
!                   THE REORDERING FOUND BY 'GPSKCA' OR THE REORDERING
!                   GIVEN BY THE USER IN 'PERMUT', WHICHEVER DOES THE
!                   JOB BETTER.
!
!
!     OUTPUT PARAMETERS:
!     ------ ----------
!
!         WORK   -- A TEMPORARY STORAGE VECTOR, OF LENGTH SOMEWHAT
!                   GREATER THAN  3N.  THE SPACE BEYOND  3N  REQUIRED
!                   IS PROBLEM-DEPENDENT.  ANY PROBLEM CAN BE SOLVED
!                   IN  6N+3  LOCATIONS.
!                   MOST PROBLEMS CAN BE REORDERED WITH  4N  LOCATIONS
!                   IN 'WORK'.  IF SPACE IS NOT A CONSTRAINT, PROVIDE
!                   6N+3  LOCATIONS IN 'WORK'.  OTHERWISE, PROVIDE AS
!                   MUCH MORE THAN  3N  AS IS CONVENIENT AND CHECK THE
!                   ERROR FLAG AND SPACE REQUIRED PARAMETERS (SEE BELOW)
!
!                   ON OUTPUT, THE 1ST  N  LOCATIONS OF WORK WILL BE
!                   A LISTING OF THE ORIGINAL ROW AND COLUMN INDICES AS
!                   THEY APPEAR IN THE COMPUTED REORDERING.
!                   LOCATIONS  N+1, ... , 2N  OF  WORK  WILL CONTAIN
!                   THE NEW POSITIONS FOR THE EQUATIONS IN THE ORDER
!                   FOUND BY GPSKCA.  THUS, THE TWO VECTORS ARE INVERSE
!                   PERMUTATIONS OF EACH OTHER.  IF THE ORDERING
!                   FOUND BY THIS ALGORITHM IS BETTER THAN THE USER-
!                   SUPPLIED ORDER, THE SECOND PERMUTATION VECTOR IS
!                   IDENTICAL TO THE RESULT RETURNED IN  'PERMUT'.
!
!         BANDWD -- THE BANDWIDTH OF THE MATRIX WHEN ROWS AND COLUMNS
!                   ARE REORDERED IN THE ORDERING RETURNED IN  PERMUT.
!
!         PROFIL -- THE PROFILE OF THE MATRIX WHEN ROWS AND COLUMNS ARE
!                   REORDERED IN THE ORDERING RETURNED IN  PERMUT.
!
!         ERROR  -- WILL BE EQUAL TO ZERO IF A NEW NUMBERING COULD BE
!                   FOUND IN THE SPACE PROVIDED.  OTHERWISE,  ERROR
!                   WILL BE SET TO A POSITIVE ERROR CODE (SEE TABLE
!                   GIVEN BELOW).  IF THE REORDERING ALGORITHM HAS BEEN
!                   STOPPED BY LACK OF WORKSPACE, THE SPACE PARAMETER
!                   WILL BE SET TO THE NUMBER OF ADDITIONAL LOCATIONS
!                   REQUIRED TO COMPLETE AT LEAST THE NEXT PHASE OF
!                   THE ALGORITHM.
!
!                   WHENEVER A NON-ZERO VALUE FOR  ERROR  IS GIVEN
!                   PERMUT  WILL RETAIN THE VALUES PROVIDED BY THE USER
!                   AND THE SCALARS  BANDWD  AND  PROFIL  WILL BE SET TO
!                   OUTRAGEOUS VALUES.  IT IS THE USER'S RESPONSIBILITY
!                   TO CHECK THE STATUS OF  ERROR.
!
!         SPACE  -- WILL INDICATE EITHER HOW MUCH SPACE THE REORDERING
!                   ACTUALLY REQUIRED OR HOW MUCH SPACE WILL BE
!                   REQUIRED TO COMPLETE THE NEXT PHASE OF THE
!                   REORDERING ALGORITHM.  THE POSSIBLE OUTCOMES ARE ..
!
!                      ERROR = 0          SPACE IS THE MINIMAL VALUE FOR
!                                         WRKLEN  REQUIRED TO REORDER
!                                         THIS MATRIX AGAIN.
!
!                      ERROR <> 0         SPACE IS THE MINIMUM NUMBER
!                      DUE TO LACK OF     OF EXTRA WORKSPACE REQUIRED
!                      WORKSPACE          TO continue THE REORDERING
!                                         ALGORITHM ON THIS MATRIX.
!
!                      ERROR <> 0         SPACE = -1
!                      DUE TO ERROR
!                      IN DATA STRUCTURES
!
!
!     ==================================================================
!
!     ----------------------
!     E R R O R    C O D E S
!     ----------------------
!
!         ERROR CODES HAVE THE FORM  0XY  OR  1XY.
!
!         ERRORS OF THE FORM  1XY  RESULT FROM INADEQUATE WORKSPACE.
!
!         ERRORS OF THE FORM  0XY  ARE INTERNAL PROGRAM CHECKS, WHICH
!         MOST LIKELY OCCUR BECAUSE THE CONNECTIVITY STRUCTURE OF THE
!         MATRIX IS REPRESENTED INCORRECTLY (E.G., THE DEGREE OF
!         A NODE IS NOT CORRECT  OR  NODE I IS CONNECTED TO NODE J,
!         BUT NOT CONVERSELY).
!
!         THE LAST DIGIT (Y) IS MAINLY USEFUL FOR DEBUGGING THE
!         THE REORDERING ALGORITHM.  THE MIDDLE DIGIT  (X)  INDICATES
!         HOW MUCH OF THE ALGORITHM HAS BEEN PERFORMED.
!         THE TABLE BELOW GIVES THE CORRESPONDENCE BETWEEN THE
!         VALUES OF  X   AND THE STRUCTURE OF THE ALGORITHM.
!             X = 0     INITIAL PROCESSING
!             X = 1     COMPUTING PSEUDO-DIAMETER  (ALGORITHM I)
!             X = 2     TRANSITION BETWEEN ALGORITHM I AND II
!             X = 3     COMBINING LEVEL STRUCTURES (ALGORITHM II)
!             X = 4     TRANSITION BETWEEN ALGORITHM II AND III
!             X = 5     BANDWIDTH NUMBERING (ALGORITHM IIIA)
!             X = 6     PROFILE NUMBERING (ALGORITHM IIIB)
!             X = 7     FINAL BANDWIDTH/PROFILE COMPUTATION
!
!     ==================================================================
!
!     ---------------------    ---------------
!     A L T E R N A T I V E    V E R S I O N S
!     ---------------------    ---------------
!
!     SHORT integer VERSION
!
!         ON MACHINES WITH TWO OR MORE PRECISIONS FOR integerS,
!         ALL OF THE INPUT ARRAYS EXCEPT 'RSTART' CAN BE CONVERTED
!         TO THE SHORTER PRECISION AS LONG AS THAT SHORTER PRECISION
!         ALLOWS NUMBERS AS LARGE AS 'N'.  A VERSION OF THIS CODE
!         SUITABLE FOR USE ON IBM COMPUTERS (integer * 2) IS EMBEDDED
!         AS COMMENTS IN THIS CODE.  ALL SUCH COMMENTS HAVE THE
!         CHARACTERS 'CIBM' IN THE FIRST FOUR COLUMNS, AND PRECEDE THE
!         EQUIVALENT STANDARD CODE WHICH THEY WOULD REPLACE.
!
!     CONNECTIVITY COMPATIBILITY VERSION
!
!         THE ORIGINAL (1976) TOMS CODE  'REDUCE'  USED A LESS STORAGE
!         EFFICIENT FORMAT FOR THE CONNECTIVITY TABLE  'CONNEC'.
!         THE 1976 CODE USED A RECTANGULAR MATRIX OF DIMENSIONS
!         N  BY  MAXDGR,  Where  MAXDGR  IS AT LEAST AS LARGE AS
!         THE MAXIMUM DEGREE OF ANY NODE IN THE GRAPH OF THE MATRIX.
!         THE FORMAT USED IN THE CURRENT CODE IS OFTEN SUBSTANTIALLY
!         MORE EFFICIENT.  HOWEVER, FOR USERS FOR WHOM CONVERSION WILL
!         BE DIFFICULT OR IMPOSSIBLE, TWO ALTERNATIVES ARE ..
!             1.  SIMPLY NOTE THAT CHANGING THE ORDER OF SUBSCRIPTS
!                 IN A RECTANGULAR CONNECTION TABLE WILL ENABLE YOU
!                 TO USE THE NEW VERSION.  THIS SUBROUTINE WILL ACCEPT A
!                 RECTANGULAR CONNECTION TABLE OF DIMENSIONS
!                     MAXDGR BY N,
!                 PROVIDED THAT  RSTART(I)  IS SET TO  (I-1)*MAXDGR + 1.
!             2.  THE AUTHOR WILL MAKE AVAILABLE A VARIANT VERSION
!                 'GPSKRA', WHICH EXPECTS THE ADJACENCY MATRIX OR
!                 CONNECTIVITY TABLE IN THE SAME FORM AS DID  'REDUCE'.
!                 THIS VERSION CAN BE OBTAINED BY WRITING TO ..
!                     JOHN GREGG LEWIS
!                     BOEING COMPUTER SERVICES COMPANY
!                     MAIL STOP 9C-01
!                     P.O. BOX 24346
!                     SEATTLE, WA 98124
!                 PLEASE INCLUDE A DESCRIPTION OF THE COMPUTING
!                 ENVIRONMENT ON WHICH YOU WILL BE USING THE CODE.
!
!     ==================================================================
!
   
   integer(Ikind) :: I     , INC1  , INC2  , AVAIL , NXTNUM, LOWDG , STNODE, NLEFT,  &
                     TREE1 , TREE2 , DEPTH , EMPTY , STOTAL, REQD  , CSPACE,         &
                     LVLLST, LVLPTR, ACTIVE, RVNODE, WIDTH1, WIDTH2, MXDG
!
   logical        :: REVRS1, ONEIS1
!
!     ==================================================================
!
!     << NUMBER ANY DEGREE ZERO NODES >>
!
!     WHILE << SOME NODES YET UNNUMBERED >> DO
!         << FIND A PSEUDO-DIAMETER OF THE MATRIX GRAPH >>
!         << CONVERT FORM OF LEVEL TREES >>
!         << COMBINE LEVEL TREES INTO ONE LEVEL STRUCTURE >>
!         << CONVERT FORM OF LEVEL STRUCTURE >>
!         IF OPTPRO then
!             << RENUMBER BY KING ALGORITHM >>
!         else
!             << RENUMBER BY REVERSE CUTHILL-MCKEE ALGORITHM >>
!
!     ==================================================================
!
!     ... INITIALIZE COUNTERS, then NUMBER ANY NODES OF DEGREE  0.
!         THE LIST OF NODES, BY NEW NUMBER, WILL BE BUILT IN PLACE AT
!         THE FRONT OF THE WORK AREA.
!

!                       CONNEC (RSTART(I)),
!                       CONNEC (RSTART(I) + 1),
!                                . . .
!                       CONNEC (RSTART(I) + DEGREE(I) - 1)
!
            
      NXTNUM = 1
      ERROR = 0
      SPACE = 2*N

      MXDG = 0
      DO 300 I = 1, N
          IF  (DEGREE(I))  6000, 100, 200
  100         WORK(NXTNUM) = I
              NXTNUM = NXTNUM + 1
              GO TO 300
  200         IF  (DEGREE(I) .GT. MXDG)  MXDG = DEGREE(I)
  300 continue
  
!
!
!     ==============================
!     ... WHILE  NXTNUM <= N  DO ...
!     ==============================
!
 1000 IF  ( NXTNUM .GT. N )  GO TO 2000
!
!         ... FIND AN UNNUMBERED NODE OF MINIMAL DEGREE
!
          LOWDG = MXDG + 1
          STNODE = 0
          DO 400 I = 1, N
              IF ( (DEGREE(I) .LE. 0) .OR. (DEGREE(I) .GE. LOWDG) ) GO TO 400
                  LOWDG = DEGREE(I)
                  STNODE = I
  400     continue
!
          IF ( STNODE .EQ. 0 )  GO TO 6100
!
!         ... SET UP POINTERS FOR THREE LISTS IN WORK AREA, then LOOK
!             FOR PSEUDO-DIAMETER, BEGINNING WITH STNODE.
!
          AVAIL = (WRKLEN - NXTNUM + 1) / 3
          NLEFT = N - NXTNUM + 1
!riad          SPACE = MAX0 (SPACE, NXTNUM + 3*N - 1)
          SPACE = MAX (SPACE, NXTNUM + 3*N - 1)
          IF ( AVAIL .LT. N )  GO TO 5200
!
          call GPSKCB (N     , DEGREE, RSTART      , CONNEC, AVAIL, NLEFT, &
                       STNODE, RVNODE, WORK(NXTNUM), TREE1 , TREE2,        &
                       ACTIVE, DEPTH , WIDTH1      , WIDTH2,               &
                       ERROR , SPACE                                     )
          IF ( ERROR .NE. 0 )  GO TO 5000
!riad          SPACE = MAX0 (SPACE, NXTNUM + 3*(ACTIVE+DEPTH+1) - 1)
          SPACE = MAX (SPACE, NXTNUM + 3*(ACTIVE+DEPTH+1) - 1)

!
!         ... DYNAMIC SPACE CHECK FOR MOST OF REMAINDER OF ALGORITHM
!
!riad          REQD = MAX0 (NXTNUM + 2*N + 3*DEPTH - 1, 3*N + 2*DEPTH + 1)
!riad          SPACE = MAX0 (SPACE, REQD)
          REQD = MAX (NXTNUM + 2*N + 3*DEPTH - 1, 3*N + 2*DEPTH + 1)
          SPACE = MAX (SPACE, REQD)

          IF  ( WRKLEN .LT. REQD )  GO TO 5300
!
!
!         ... OUTPUT FROM GPSKCB IS A PAIR OF LEVEL TREES, IN THE FORM
!             OF LISTS OF NODES BY LEVEL.  CONVERT THIS TO TWO LISTS OF
!             OF LEVEL NUMBER BY NODE.  AT THE SAME TIME PACK
!             STORAGE SO THAT ONE OF THE LEVEL TREE VECTORS IS AT THE
!             BACK END OF THE WORK AREA.
!
          LVLPTR = NXTNUM + AVAIL - DEPTH 
          call GPSKCE (N           , AVAIL  , ACTIVE, DEPTH , WRKLEN, WORK(NXTNUM), &
                       WORK(LVLPTR), WORK(1), NXTNUM, TREE1 ,                       &
                       TREE2       , WIDTH1 , WIDTH2, ONEIS1, ERROR , SPACE       )
                       
          IF ( ERROR .NE. 0 ) GO TO 5000
          IF (( TREE1 .NE. WRKLEN - N + 1 ) .OR. (TREE2 .NE. NXTNUM)) GO TO 6200
!
!         ... COMBINE THE TWO LEVEL TREES INTO A MORE GENERAL
!             LEVEL STRUCTURE.
!
          AVAIL = WRKLEN - NXTNUM + 1 - 2*N - 3*DEPTH
          STOTAL = N + NXTNUM
          EMPTY = STOTAL + DEPTH
          INC1 = TREE1 - DEPTH
          INC2 = INC1 - DEPTH
!
          call GPSKCG (N           , DEGREE     , RSTART     , CONNEC     , ACTIVE, WIDTH1, &
                       WIDTH2      , WORK(TREE1), WORK(TREE2), WORK(EMPTY),                 &
                       AVAIL       , DEPTH      , WORK(INC1) , WORK(INC2) ,                 &
                       WORK(STOTAL), ONEIS1     , REVRS1     , ERROR      , CSPACE        )
!
          IF ( ERROR .NE. 0 )  GO TO 5000
!riad          SPACE = MAX0 (SPACE, NXTNUM + CSPACE - 1)
          SPACE = MAX (SPACE, NXTNUM + CSPACE - 1)
!
!         ... COMBINED LEVEL STRUCTURE IS REPRESENTED BY GPSKCG AS
!             A VECTOR OF LEVEL NUMBERS.  FOR RENUMBERING PHASE,
!             CONVERT THIS ALSO TO THE INVERSE PERMUTATION.
!
          LVLPTR = TREE1 - (DEPTH + 1)
          LVLLST = LVLPTR - ACTIVE
          IF ( STOTAL + DEPTH .GT. LVLPTR )  GO TO 6300
!
          call GPSKCI (N           , ACTIVE      , DEPTH, WORK(TREE1), WORK(LVLLST), &
                       WORK(LVLPTR), WORK(STOTAL), ERROR, SPACE                    )
          IF  (ERROR .NE. 0)  GO TO 5000
!
!         ... NOW RENUMBER ALL MEMBERS OF THIS COMPONENT USING
!             EITHER A REVERSE CUTHILL-MCKEE OR A KING STRATEGY,
!             AS PROFILE OR BANDWIDTH REDUCTION IS MORE IMPORTANT.
!
          IF ( OPTPRO )  GO TO 500
              call GPSKCJ (N           , DEGREE      , RSTART     , CONNEC, ACTIVE, &
                           WORK(NXTNUM), STNODE      , RVNODE     , REVRS1, DEPTH , &
                           WORK(LVLLST), WORK(LVLPTR), WORK(TREE1), ERROR , SPACE )
              IF ( ERROR .NE. 0 )  GO TO 5000
              NXTNUM = NXTNUM + ACTIVE
              GO TO 600
!
  500         call GPSKCK (N           , DEGREE     , RSTART, CONNEC      , LVLLST-1, NXTNUM, &
                           WORK        , ACTIVE     , DEPTH , WORK(LVLLST),                   &
                           WORK(LVLPTR), WORK(TREE1), ERROR , SPACE                         )
              IF ( ERROR .NE. 0 )  GO TO 5000
!
!         =========================================================
!         ... END OF WHILE LOOP ... REPEAT IF GRAPH IS DISCONNECTED
!         =========================================================
!
  600     GO TO 1000
!
!     ... CHECK WHETHER INITIAL NUMBERING OR FINAL NUMBERING
!         PROVIDES BETTER RESULTS
!
 2000 IF  (WRKLEN .LT. 2*N)  GO TO 5400
!
      IF  (OPTPRO)  GO TO 2100
          call GPSKCL (N     , DEGREE, RSTART, CONNEC, WORK(1), WORK(N+1), &
                       PERMUT, BANDWD, PROFIL, ERROR , SPACE             )
          GO TO 2200
!
 2100     call GPSKCM (N     , DEGREE, RSTART, CONNEC, WORK(1), WORK(N+1), &
                       PERMUT, BANDWD, PROFIL, ERROR , SPACE             )
!
 2200 RETURN
!
!
!     . . .  E R R O R   D I A G N O S T I C S
!            ---------------------------------
!
!     ... ERROR DETECTED BY LOWER LEVEL ROUTINE.  MAKE SURE THAT SIGNS
!         OF DEGREE ARE PROPERLY SET
!
 5000 DO 5100 I = 1, N
          IF  (DEGREE(I) .LT. 0)  DEGREE(I) = -DEGREE(I)
 5100 continue
!
      BANDWD = -1
      PROFIL = -1
      RETURN
!
!     ... STORAGE ALLOCATION ERRORS DETECTED IN THIS ROUTINE
!
 5200 ERROR = 101
      SPACE = -1
      GO TO 5000
!
 5300 ERROR = 102
      SPACE = -1
      GO TO 5000
!
 5400 ERROR =  10
      SPACE = 2*N - WRKLEN
      GO TO 5000
!
!     ... DATA STRUCTURE ERRORS DETECTED IN THIS ROUTINE
!
 6000 ERROR = 1
      GO TO 6900
!
 6100 ERROR = 2
      GO TO 6900
!
 6200 ERROR = 3
      GO TO 6900
!
 6300 ERROR = 4
!
 6900 SPACE = -1
      GO TO 5000
   
   END SUBROUTINE GPSKCA


!=============================================================================================   
   SUBROUTINE GPSKCB (N     , DEGREE, RSTART, CONNEC, AVAIL , NLEFT , &     
                      STNODE, RVNODE, WORK  , FORWD , BESTBK, NNODES, &
                      DEPTH , FWIDTH, BWIDTH, ERROR , SPACE         )
!=============================================================================================   
!
!     ==================================================================
!
!     FIND A PSEUDO-DIAMETER OF THE MATRIX GRAPH ...
!
!         << BUILD A LEVEL TREE FROM STNODE >>
!         REPEAT
!             << BUILD A LEVEL TREE FROM EACH NODE 'BKNODE' IN THE
!                DEEPEST LEVEL OF  STNODE'S TREE >>
!             << REPLACE 'STNODE' WITH 'BKNODE' IF A DEEPER AND
!                NARROWER TREE WAS FOUND. >>
!         UNTIL
!             << NO FURTHER IMPROVEMENT MADE >>
!
!     ... HEURISTIC ABOVE DIFFERS FROM THE ALGORITHM PUBLISHED IN
!         SIAM J. NUMERICAL ANALYSIS, BUT MATCHES THE CODE
!         DISTRIBUTED BY TOMS.
!
!
!     PARAMETERS :
!
!         N, DEGREE, RSTART & CONNEC  DESCRIBE THE MATRIX STRUCTURE
!
!         WORK   -- WORKING SPACE, OF LENGTH  3*AVAIL, USED TO STORE
!         THREE LEVEL TREES.
!
!         STNODE IS INITIALLY THE NUMBER OF A NODE TO BE USED TO
!             START THE PROCESS, TO BE THE ROOT OF THE FIRST TREE.
!             ON OUTPUT, STNODE IS THE END OF THE PSEUDO-DIAMETER WHOSE
!             LEVEL TREE IS NARROWEST.
!
!         RVNODE WILL BE THE OTHER END OF THE PSEUDO-DIAMETER.
!
!         NNODES WILL BE THE NUMBER OF NODES IN THIS CONNECTED
!             COMPONNENT OF THE MATRIX GRAPH, I.E., THE LENGTH OF
!             THE LEVEL TREES.
!
!         DEPTH  -- THE DEPTH OF THE LEVEL TREES BEING RETURNED,
!                   I.E., THE LENGTH OF THE PSEUDO-DIAMETER.
!
!     ==================================================================
!
!     STRUCTURE OF WORKSPACE ...
!
!     ---------------------------------------------------------------
!     : NUMBERED :  TLIST1  PTR1  :  TLIST2  PTR2  :  TLIST3  PTR3  :
!     ---------------------------------------------------------------
!
!     TLISTI IS A LIST OF NODES OF LENGTH  'ACTIVE'
!     PTRI   IS A LIST OF POINTERS INTO TLISTI, OF LENGTH  'DEPTH+1'
!
!     ==================================================================
!
      integer(Ikind) :: N, RSTART(N), AVAIL, NLEFT, STNODE, RVNODE, FORWD, & 
                        BESTBK, NNODES, DEPTH, FWIDTH, BWIDTH, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(1), WORK(AVAIL,3)
      integer(Ikind) :: DEGREE(N), CONNEC(*), WORK(AVAIL,3)
!
!     ----------------
!
      integer(Ikind) :: BACKWD, MXDPTH, WIDTH, FDEPTH, LSTLVL, &
                        NLAST, T, I, BKNODE, LSTLVI
      logical        :: IMPROV
!
!
!     ... BUILD INITIAL LEVEL TREE FROM 'STNODE'.  FIND OUT HOW MANY
!         NODES LIE IN THE CURRENT CONNECTED COMPONENT.
!
      FORWD = 1
      BACKWD = 2
      BESTBK = 3
!
      call GPSKCC (N            , DEGREE, RSTART, CONNEC, STNODE, AVAIL, NLEFT, &
                   WORK(1,FORWD), NNODES, DEPTH , WIDTH , ERROR , SPACE       )
      IF ( ERROR .NE. 0 )  GO TO 5000
!
      MXDPTH = AVAIL - NNODES - 1
!
!     ==========================================
!     REPEAT UNTIL NO DEEPER TREES ARE FOUND ...
!     ==========================================
!
 1000     FWIDTH = WIDTH
          FDEPTH = DEPTH
          LSTLVL = AVAIL - DEPTH + 1
          NLAST = WORK (LSTLVL-1, FORWD) - WORK (LSTLVL, FORWD)
          LSTLVL = WORK (LSTLVL, FORWD)
          BWIDTH = N+1
!
!         ... SORT THE DEEPEST LEVEL OF 'FORWD' TREE INTO INCREASING
!             ORDER OF NODE DEGREE.
!
          call GPSKCQ (NLAST, WORK(LSTLVL,FORWD), N, DEGREE, ERROR)
          IF  (ERROR .NE. 0)  GO TO 6000
!
!         ... BUILD LEVEL TREE FROM NODES IN 'LSTLVL' UNTIL A DEEPER
!             AND NARROWER TREE IS FOUND OR THE LIST IS EXHAUSTED.
!
          IMPROV = .false.
          DO 1200 I = 1, NLAST
              LSTLVI = LSTLVL + I - 1
              BKNODE = WORK (LSTLVI, FORWD)
              call GPSKCD (N     , DEGREE, RSTART        , CONNEC, BKNODE, AVAIL, &
                           NNODES, MXDPTH, WORK(1,BACKWD), DEPTH , WIDTH ,        &
                           BWIDTH, ERROR , SPACE                                )
              IF ( ERROR .NE. 0 )  GO TO 5000
!
              IF ( DEPTH .LE. FDEPTH )  GO TO 1100
!
!                 ... NEW DEEPER TREE ... MAKE IT NEW 'FORWD' TREE
!                     AND BREAK OUT OF 'DO' LOOP.
!
                  IMPROV = .true.
                  T = FORWD
                  FORWD = BACKWD
                  BACKWD = T
                  STNODE = BKNODE
                  GO TO 1300
!
!                 ... else CHECK FOR NARROWER TREE.
!
 1100             IF ( WIDTH .GE. BWIDTH )  GO TO 1200
                      T = BESTBK
                      BESTBK = BACKWD
                      BACKWD = T
                      BWIDTH = WIDTH
                      RVNODE = BKNODE
 1200     continue
!
!         ... END OF REPEAT LOOP
!         ----------------------
!
 1300     IF ( IMPROV )  GO TO 1000
!
      DEPTH = FDEPTH
      RETURN
!
!     ... IN CASE OF ERROR, SIMPLY RETURN ERROR FLAG TO USER.
!
 5000 RETURN
!
 6000 ERROR = 11
      SPACE = -1
      RETURN
!
   END SUBROUTINE GPSKCB
         

!=============================================================================================         
   SUBROUTINE GPSKCC (N     , DEGREE, RSTART, CONNEC, STNODE, AVAIL, NLEFT, LIST, ACTIVE,    &   
                      DEPTH , WIDTH , ERROR , SPACE                                     )
!=============================================================================================         
!
!     ==================================================================
!     BUILD THE LEVEL TREE ROOTED AT 'STNODE' IN THE SPACE PROVIDED IN
!     LIST.  CHECK FOR OVERRUN OF SPACE ALLOCATION.
!     ==================================================================
!
      integer(Ikind) :: N, RSTART(N), STNODE, AVAIL, NLEFT, ACTIVE, DEPTH, WIDTH, ERROR, SPACE

!IBM  integer *2  DEGREE(N), CONNEC(1), LIST(AVAIL)
      integer(Ikind) :: DEGREE(N), CONNEC(*), LIST(AVAIL)
!
!     ... PARAMETERS:
!
!         INPUT ...
!
!             N, DEGREE, RSTART, CONNEC -- DESCRIBE THE MATRIX STRUCTURE
!
!             STNODE -- THE ROOT OF THE LEVEL TREE.
!
!             AVAIL  -- THE LENGTH OF THE WORKING SPACE AVAILABLE
!
!             NLEFT  -- THE NUMBER OF NODES YET TO BE NUMBERED
!
!             LIST   -- THE WORKING SPACE.
!
!         OUTPUT ...
!
!             ACTIVE -- THE NUMBER OF NODES IN THE COMPONENT
!
!             DEPTH  -- THE DEPTH OF THE LEVEL TREE ROOTED AT  STNODE.
!
!             WIDTH  -- THE WIDTH OF THE LEVEL TREE ROOTED AT  STNODE.
!
!             ERROR  -- ZERO UNLESS STORAGE WAS INSUFFICIENT.
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: LSTART, NLEVEL, FRONT, J, NEWNOD, PTR, CDGREE, LFRONT, LISTJ
!
!     ... BUILD THE LEVEL TREE USING  LIST  AS A QUEUE AND LEAVING
!         THE NODES IN PLACE.  THIS GENERATES THE NODES ORDERED BY LEVEL
!         PUT POINTERS TO THE BEGINNING OF EACH LEVEL, BUILDING FROM
!         THE BACK OF THE WORK AREA.
!
      ACTIVE = 1
      DEPTH = 0
      WIDTH = 0
      ERROR = 0
      LSTART = 1
      FRONT = 1
      LIST (ACTIVE) = STNODE
      DEGREE (STNODE) = -DEGREE (STNODE)
      LIST (AVAIL)  = 1
      NLEVEL = AVAIL
!
!     ... REPEAT UNTIL QUEUE BECOMES EMPTY OR WE RUN OUT OF SPACE.
!     ------------------------------------------------------------
!
 1000     IF ( FRONT .LT. LSTART ) GO TO 1100
!
!         ... FIRST NODE OF LEVEL.  UPDATE POINTERS.
!
              LSTART = ACTIVE + 1
!riad              WIDTH = MAX0 (WIDTH, LSTART - LIST(NLEVEL))
              WIDTH = MAX (WIDTH, LSTART - LIST(NLEVEL))
              NLEVEL = NLEVEL - 1
              DEPTH = DEPTH + 1
              IF ( NLEVEL .LE. ACTIVE )  GO TO 5000
                  LIST (NLEVEL) = LSTART
!
!         ... FIND ALL NEIGHBORS OF CURRENT NODE, ADD THEM TO QUEUE.
!
 1100     LFRONT = LIST (FRONT)
          PTR = RSTART (LFRONT)
          CDGREE = -DEGREE (LFRONT)
          IF (CDGREE .LE. 0)  GO TO 6000
          DO 1200 J = 1, CDGREE
              NEWNOD = CONNEC (PTR)
              PTR = PTR + 1
!
!             ... ADD TO QUEUE ONLY NODES NOT ALREADY IN QUEUE
!
              IF ( DEGREE(NEWNOD) .LE. 0 )  GO TO 1200
                  DEGREE (NEWNOD) = -DEGREE (NEWNOD)
                  ACTIVE = ACTIVE + 1
                  IF ( NLEVEL .LE. ACTIVE )  GO TO 5000
                  IF ( ACTIVE .GT. NLEFT  )  GO TO 6000
                      LIST (ACTIVE) = NEWNOD
 1200     continue
          FRONT = FRONT + 1
!
!         ... IS QUEUE EMPTY?
!         -------------------
!
          IF ( FRONT .LE. ACTIVE )  GO TO 1000
!
!     ... YES, THE TREE IS BUILT.  UNDO OUR MARKINGS.
!
      DO 1300 J = 1, ACTIVE
          LISTJ = LIST(J)
          DEGREE (LISTJ) = -DEGREE (LISTJ)
 1300 continue
!
      RETURN
!
!     ... INSUFFICIENT STORAGE ...
!
 5000 SPACE = 3 * ( (NLEFT+1-ACTIVE)*DEPTH / NLEFT + (NLEFT+1-ACTIVE) )
      ERROR = 110
      RETURN
!
 6000 ERROR = 12
      SPACE = -1
      RETURN
!
   END SUBROUTINE GPSKCC
   

!=============================================================================================            
   SUBROUTINE GPSKCD (N    , DEGREE, RSTART, CONNEC, STNODE, AVAIL, ACTIVE, MXDPTH, LIST,    &
                      DEPTH, WIDTH , MAXWID, ERROR , SPACE                              )
!=============================================================================================            
!
!     ==================================================================
!     BUILD THE LEVEL TREE ROOTED AT 'STNODE' IN THE SPACE PROVIDED IN
!     LIST.  OVERFLOW CHECK NEEDED ONLY ON DEPTH OF TREE.
!
!     BUILD THE LEVEL TREE TO COMPLETION ONLY IF THE WIDTH OF ALL
!     LEVELS IS SMALLER THAN 'MAXWID'.  IF A WIDER LEVEL IS FOUND
!     TERMINATE THE CONSTRUCTION.
!     ==================================================================
!
      integer(Ikind) :: N    , RSTART(N), STNODE, AVAIL, ACTIVE, MXDPTH,  &
                        DEPTH, WIDTH    , MAXWID, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(1), LIST(AVAIL)
      integer(Ikind) :: DEGREE(N), CONNEC(*), LIST(AVAIL)
!
!     ... PARAMETERS:
!
!         INPUT ...
!
!             N, DEGREE, RSTART, CONNEC -- DESCRIBE THE MATRIX STRUCTURE
!
!             STNODE -- THE ROOT OF THE LEVEL TREE.
!
!             AVAIL  -- THE LENGTH OF THE WORKING SPACE AVAILABLE
!
!             NLEFT  -- THE NUMBER OF NODES YET TO BE NUMBERED
!
!             ACTIVE -- THE NUMBER OF NODES IN THE COMPONENT
!
!             MXDPTH -- MAXIMUM DEPTH OF LEVEL TREE POSSIBLE IN
!                       ALLOTTED WORKING SPACE
!
!             LIST   -- THE WORKING SPACE.
!
!         OUTPUT ...
!
!             DEPTH  -- THE DEPTH OF THE LEVEL TREE ROOTED AT  STNODE.
!
!             WIDTH  -- THE WIDTH OF THE LEVEL TREE ROOTED AT  STNODE.
!
!             MAXWID -- LIMIT ON WIDTH OF THE TREE.  TREE WILL NOT BE
!                       USED IF WIDTH OF ANY LEVEL IS AS GREAT AS
!                       MAXWID, SO CONSTRUCTION OF TREE NEED NOT
!                       continue IF ANY LEVEL THAT WIDE IS FOUND.
!             ERROR  -- ZERO UNLESS STORAGE WAS INSUFFICIENT.
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: LSTART, NLEVEL, FRONT, J, NEWNOD, PTR, BACK, SPTR, FPTR, LFRONT, LISTJ
!
!     ... BUILD THE LEVEL TREE USING  LIST  AS A QUEUE AND LEAVING
!         THE NODES IN PLACE.  THIS GENERATES THE NODES ORDERED BY LEVEL
!         PUT POINTERS TO THE BEGINNING OF EACH LEVEL, BUILDING FROM
!         THE BACK OF THE WORK AREA.
!
      BACK = 1
      DEPTH = 0
      WIDTH = 0
      ERROR = 0
      LSTART = 1
      FRONT = 1
      LIST (BACK) = STNODE
      DEGREE (STNODE) = -DEGREE (STNODE)
      LIST (AVAIL)  = 1
      NLEVEL = AVAIL
!
!     ... REPEAT UNTIL QUEUE BECOMES EMPTY OR WE RUN OUT OF SPACE.
!     ------------------------------------------------------------
!
 1000     IF ( FRONT .LT. LSTART ) GO TO 1100
!
!         ... FIRST NODE OF LEVEL.  UPDATE POINTERS.
!
              LSTART = BACK + 1
!riad              WIDTH = MAX0 (WIDTH, LSTART - LIST(NLEVEL))
              WIDTH = MAX (WIDTH, LSTART - LIST(NLEVEL))
              IF  ( WIDTH .GE. MAXWID )  GO TO 2000
              NLEVEL = NLEVEL - 1
              DEPTH = DEPTH + 1
              IF ( DEPTH .GT. MXDPTH )  GO TO 5000
                  LIST (NLEVEL) = LSTART
!
!         ... FIND ALL NEIGHBORS OF CURRENT NODE, ADD THEM TO QUEUE.
!
 1100     LFRONT = LIST (FRONT)
          SPTR = RSTART (LFRONT)
          FPTR = SPTR - DEGREE (LFRONT) - 1
          DO 1200 PTR = SPTR, FPTR
              NEWNOD = CONNEC (PTR)
!
!             ... ADD TO QUEUE ONLY NODES NOT ALREADY IN QUEUE

              IF ( DEGREE(NEWNOD) .LE. 0 )  GO TO 1200
                  DEGREE (NEWNOD) = -DEGREE (NEWNOD)
                  BACK = BACK + 1
                  LIST (BACK) = NEWNOD
 1200     continue
          FRONT = FRONT + 1
!
!         ... IS QUEUE EMPTY?
!         -------------------
!
          IF ( FRONT .LE. BACK )  GO TO 1000
!
!     ... YES, THE TREE IS BUILT.  UNDO OUR MARKINGS.
!
      IF (BACK .NE. ACTIVE)  GO TO 6000
!
 1300 DO 1400 J = 1, BACK
          LISTJ = LIST(J)
          DEGREE (LISTJ) = -DEGREE (LISTJ)
 1400 continue
!
      RETURN
!
!     ... ABORT GENERATION OF TREE BECAUSE IT IS ALREADY TOO WIDE
!
 2000 WIDTH = N + 1
      DEPTH = 0
      GO TO 1300
!
!     ... INSUFFICIENT STORAGE ...
!
 5000 SPACE = 3 * ( (ACTIVE+1-BACK)*DEPTH / ACTIVE + (ACTIVE+1-BACK) )
      ERROR = 111
      RETURN
!
 6000 ERROR = 13
      SPACE = -1
      RETURN
!
   END SUBROUTINE GPSKCD
   

!=============================================================================================            
   SUBROUTINE GPSKCE (N    , AVAIL, ACTIVE, DEPTH , WRKLEN, LVLLST, LVLPTR, WORK, NXTNUM,    &            
                      TREE1, TREE2, WIDTH1, WIDTH2, ONEIS1, ERROR , SPACE               )
!=============================================================================================                                        
!
!     ==================================================================
!
!     TRANSITION BETWEEN ALGORITHM I AND ALGORITHM II OF
!     THE GIBBS-POOLE-STOCKMEYER PAPER.
!
!     IN THIS IMPLEMENTATION ALGORITHM I REPRESENTS LEVEL TREES AS
!     LISTS OF NODES ORDERED BY LEVEL.  ALGORITHM II APPEARS TO REQUIRE
!     LEVEL NUMBERS INDEXED BY NODE -- VECTORS FOR EFFICIENCY.
!     THIS SUBROUTINE CHANGES THE LEVEL TREE REPRESENTATION TO THAT
!     REQUIRED BY ALGORITHM II.  NOTE THAT THE FIRST ALGORITHM CAN BE
!     CARRIED OUT WITH THE LEVEL NUMBER VECTOR FORMAT, PROBABLY REQURING
!     MORE COMPUTATION TIME, BUT PERHAPS LESS STORAGE.
!
!     INPUT:  TWO LEVEL TREES, AS LEVEL LISTS AND LEVEL POINTERS,
!             FOUND IN TWO OF THE THREE COLUMNS OF THE ARRAYS 'LVLLST'
!             AND 'LVLPTR'
!
!     OUTPUT: TWO LEVEL TREES, AS VECTORS OF LEVEL NUMBERS,
!             ONE PACKED TO THE FRONT, ONE TO THE REAR OF THE WORKING
!             AREA 'WORK'.  NOTE THAT 'WORK', 'LVLLST' AND 'LVLPTR'
!             SHARE COMMON LOCATIONS.
!
!     ================================================================
!
!     ... STRUCTURE OF WORKSPACE
!
!         INPUT .. (OUTPUT FROM GPSKCB)
!
!     --------------------------------------------------------------
!     : NUMBERED : TLIST1  PTR1  :  TLIST2  PTR2  :  TLIST3  PTR3  :
!     --------------------------------------------------------------
!
!         OUTPUT .. (GOES TO COMBIN)
!
!     --------------------------------------------------------------
!     : NUMBERED :  TREE2  :           ...               :  TREE1  :
!     --------------------------------------------------------------
!
!     ==================================================================
!
      integer(Ikind) :: N    , AVAIL, ACTIVE, DEPTH, WRKLEN, NXTNUM, WIDTH1, WIDTH2, &
                        TREE1, TREE2, ERROR , SPACE
!
!IBM  integer *2  LVLLST(AVAIL,3), LVLPTR(AVAIL,3), WORK(WRKLEN)
!riad integer(Ikind) :: LVLLST(AVAIL,3), LVLPTR(AVAIL,3), WORK(WRKLEN) !riad: nagfor complains that the
                                                                       ! size of the dummy arg. LVLPTR is 
                                                                       ! greater that the actual argument
                      
      integer(Ikind) :: LVLLST(AVAIL,*), LVLPTR(AVAIL,*), WORK(WRKLEN)
      logical        :: ONEIS1
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: I, BTREE, FTREE, FWIDTH, BWIDTH
!
!
!     ... CHECK THAT WE HAVE ENOUGH ROOM TO DO THE NECESSARY UNPACKING
!
      IF (3*AVAIL .GT. WRKLEN)  GO TO 6000
      IF (AVAIL .LT. N)  GO TO 5100
!
!     ... INPUT HAS THREE POSSIBLE CASES:
!             LVLLST(*,1) IS EMPTY
!             LVLLST(*,2) IS EMPTY
!             LVLLST(*,3) IS EMPTY
!
      FTREE = TREE1
      BTREE = TREE2
      FWIDTH = WIDTH1
      BWIDTH = WIDTH2
!
      TREE1 = WRKLEN - N + 1
      TREE2 = NXTNUM
!
      IF ( (FTREE .EQ. 1) .OR. (BTREE .EQ. 1) )  GO TO 300
!
!         ... CASE 1:  1ST SLOT IS EMPTY.  UNPACK 3 INTO 1, 2 INTO 3
!
          IF (FTREE .NE. 2)  GO TO 100
              ONEIS1 = .true.
              WIDTH2 = BWIDTH
              WIDTH1 = FWIDTH
              GO TO 200
!
  100         ONEIS1 = .false.
              WIDTH1 = BWIDTH
              WIDTH2 = FWIDTH
!
  200     call GPSKCF (N, ACTIVE, DEPTH, LVLLST(1,3), LVLPTR(1,3), WORK(TREE2), ONEIS1)
!
          call GPSKCF (N, ACTIVE, DEPTH, LVLLST(1,2), LVLPTR(1,2), WORK(TREE1), .NOT. ONEIS1)
!
          GO TO 1000
!
!
  300 IF ( (FTREE .EQ. 2) .OR. (BTREE .EQ. 2) )  GO TO 600
!
!         ... CASE 2:  2ND SLOT IS EMPTY.  TO ENABLE COMPLETE
!              REPACKING, MOVE 3 INTO 2, then FALL INTO NEXT CASE
!
          DO 400 I = 1, ACTIVE
              LVLLST(I,2) = LVLLST(I,3)
  400     continue
!
          DO 500 I = 1, DEPTH
              LVLPTR(I,2) = LVLPTR(I,3)
  500     continue
!
!         ... CASE 3:  SLOT 3 IS EMPTY.  MOVE 1 INTO 3, then 2 INTO 1.
!
  600     IF (FTREE .EQ. 1) GO TO 700
              ONEIS1 = .false.
              WIDTH1 = BWIDTH
              WIDTH2 = FWIDTH
              GO TO 800
!
  700         ONEIS1 = .true.
              WIDTH1 = FWIDTH
              WIDTH2 = BWIDTH
!
  800     call GPSKCF (N, ACTIVE, DEPTH, LVLLST(1,1), LVLPTR(1,1), WORK(TREE1), .NOT. ONEIS1)
!
          call GPSKCF (N, ACTIVE, DEPTH, LVLLST(1,2), LVLPTR(1,2), WORK(TREE2), ONEIS1)
 1000 RETURN
!
!     ------------------------------------------------------------------
!
 5100 SPACE = 3 * (N - AVAIL)
      ERROR = 120
      RETURN
!
 6000 ERROR = 20
      SPACE = -1
      RETURN
!
   END SUBROUTINE GPSKCE

   
!=============================================================================================            
   SUBROUTINE GPSKCF (N, ACTIVE, DEPTH, LVLLST, LVLPTR, LVLNUM, REVERS)
!=============================================================================================               
!
!     ==================================================================
!
!     CONVERT LEVEL STRUCTURE REPRESENTATION FROM A LIST OF NODES
!     GROUPED BY LEVEL TO A VECTOR GIVING LEVEL NUMBER FOR EACH NODE.
!
!     LVLLST, LVLPTR -- LIST OF LISTS
!
!     LVLNUM -- OUTPUT VECTOR OF LEVEL NUMBERS
!
!     REVERS -- IF .true., NUMBER LEVEL STRUCTURE FROM BACK END
!               INSTEAD OF FROM FRONT
!
!     ==================================================================
!
      integer(Ikind) :: N, ACTIVE, DEPTH
!
!IBM  integer *2  LVLLST(ACTIVE), LVLPTR(DEPTH), LVLNUM(N)
      integer(Ikind) :: LVLLST(ACTIVE), LVLPTR(DEPTH), LVLNUM(N)
      logical        :: REVERS
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: I, LEVEL, LSTART, LEND, XLEVEL, PLSTRT, LVLLSI
!
      IF  (ACTIVE .EQ. N)  GO TO 200
!
!         ... IF NOT ALL NODES OF GRAPH ARE ACTIVE, MASK OUT THE
!             NODES WHICH ARE NOT ACTIVE
!
          DO 100 I = 1, N
              LVLNUM(I) = 0
  100     continue
!
  200 DO 400 LEVEL = 1, DEPTH
          XLEVEL = LEVEL
          PLSTRT = DEPTH - LEVEL + 1
          IF (REVERS) XLEVEL = PLSTRT
          LSTART = LVLPTR (PLSTRT)
          
          if (PLSTRT .eq. 1) then                                                       ! added by RH
            if (verb >= 2) print*,'Warning in GPSKCF: PLSTRT=1. I set lend=active (RH)' ! added by RH
            lend = active                                                               ! added by RH
          else                                                                          ! added by RH          
            LEND = LVLPTR (PLSTRT - 1) - 1
          end if                                                                        ! added by RH
          
          DO 300 I = LSTART, LEND
              LVLLSI = LVLLST(I)
              LVLNUM (LVLLSI) = XLEVEL
  300     continue
  400 continue
!
      RETURN
   END SUBROUTINE GPSKCF


!=============================================================================================            
   SUBROUTINE GPSKCG (N   , DEGREE, RSTART, CONNEC, ACTIVE, WIDTH1, WIDTH2, TREE1 , TREE2,   &
                      WORK, WRKLEN, DEPTH , INC1  , INC2  , TOTAL , ONEIS1, REVRS1, ERROR,   &
                      SPACE                                                              )
!=============================================================================================            
!
!     ==================================================================
!
!     COMBINE THE TWO ROOTED LEVEL TREES INTO A SINGLE LEVEL STRUCTURE
!     WHICH MAY HAVE SMALLER WIDTH THAN EITHER OF THE TREES.  THE NEW
!     STRUCTURE IS NOT NECESSARILY A ROOTED STRUCTURE.
!
!     PARAMETERS:
!
!         N, DEGREE, RSTART, CONNEC -- GIVE THE DIMENSION AND STRUCTURE
!                                      OF THE SPARSE SYMMETRIC MATRIX
!
!         ACTIVE -- THE NUMBER OF NODES IN THIS CONNECTED COMPONENT OF
!                   THE MATRIX GRAPH
!
!         TREE1  -- ON INPUT, ONE OF THE INPUT LEVEL TREES.  ON
!                   OUTPUT, THE COMBINED LEVEL STRUCTURE
!
!         TREE2  -- THE SECOND INPUT LEVEL TREE
!
!         WIDTH1 -- THE MAXIMUM WIDTH OF A LEVEL IN TREE1
!
!         WIDTH2 -- THE MAXIMUM WIDTH OF A LEVEL IN TREE2
!
!         WORK   -- A WORKING AREA OF LENGTH 'WRKLEN'
!
!         INC1,  -- VECTORS OF LENGTH 'DEPTH'
!         INC2,
!         TOTAL
!
!         ONEIS1 -- INDICATES WHETHER TREE1 OR TREE2 REPRESENTS THE
!                   FORWARD TREE OR THE BACKWARDS TREE OF PHASE 1.
!                   USED TO MIMIC ARBITRARY TIE-BREAKING PROCEDURE OF
!                   ORIGINAL GIBBS-POOLE-STOCKMEYER CODE.
!
!         REVRS1 -- OUTPUT PARAMETER INDICATING WHETHER A BACKWARDS
!                   ORDERING WAS USED FOR THE LARGEST COMPONENT OF
!                   THE REDUCED GRAPH
!
!         ERROR  -- NON-ZERO ONLY IF FAILURE OF SPACE ALLOCATION OR
!                   DATA STRUCTURE ERROR FOUND
!
!         SPACE -- MINIMUM SPACE REQUIRED TO RERUN OR COMPLETE PHASE.
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: N, RSTART(N), ACTIVE, WIDTH1, WIDTH2, WRKLEN, DEPTH, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(1), TREE1(N), TREE2(N),
      integer(Ikind) :: DEGREE(N)   , CONNEC(*)  , TREE1(N)   , TREE2(N)   , &
                        WORK(WRKLEN), INC1(DEPTH), INC2(DEPTH), TOTAL(DEPTH)
      logical        :: ONEIS1, REVRS1
!
!     ==================================================================
!
!     << REMOVE ALL NODES OF PSEUDO-DIAMETERS >>
!     << FIND CONNECTED COMPONENTS OF REDUCED GRAPH >>
!     << COMBINE LEVEL TREES, COMPONENT BY COMPONENT >>
!
!     ==================================================================
!
!     STRUCTURE OF WORKSPACE ...
!
!     ------------------------------------------------------------------
!     : NUMBERED : TREE2 : TOTAL : NODES : START : SIZE : INC1 : INC2 :
!     ------------------------------------------------------------------
!
!     --------
!      TREE1 :
!     --------
!
!         NUMBERED  IS THE SET OF  NUMBERED NODES (PROBABLY EMPTY)
!
!         TREE1 AND TREE1 ARE LEVEL TREES (LENGTH N)
!         TOTAL, INC1 AND INC2  ARE VECTORS OF NODE COUNTS PER LEVEL
!             (LENGTH 'DEPTH')
!         NODES IS THE SET OF NODES IN THE REDUCED GRAPH (THE NODES
!             NOT ON ANY SHORTEST PATH FROM ONE END OF THE
!             PSEUDODIAMETER TO THE OTHER)
!         START, SIZE ARE POINTERS INTO 'NODES', ONE OF EACH FOR
!         EACH CONNECTED COMPONENT OF THE REDUCED GRAPH.
!         THE SIZES OF NODES, START AND SIZE ARE NOT KNOWN APRIORI.
!
!     ==================================================================
      integer(Ikind) :: I     , SIZE  , AVAIL , CSTOP , START , COMPON, TREE1I, PCSTRT, &
                        CSTART, MXINC1, MXINC2, COMPNS, MXCOMP, OFFDIA, CSIZE , PCSIZE, &
                        WORKI , TWORKI
!
!     ------------------------------------------------------------------
!
!     ... FIND ALL SHORTEST PATHS FROM START TO FINISH.  REMOVE NODES ON
!         THESE PATHS AND IN OTHER CONNECTED COMPONENTS OF FULL GRAPH
!         FROM FURTHER CONSIDERATION.  SIGN OF ENTRIES IN TREE1 IS USED
!         AS A MASK.
!
      OFFDIA = ACTIVE
!
      DO 100 I = 1, DEPTH
          TOTAL(I) = 0
  100 continue
!
      DO 200 I = 1, N
          TREE1I = TREE1 (I)
          IF ((TREE1(I) .NE. TREE2(I)) .OR. (TREE1(I) .EQ. 0)) GO TO 200
              TOTAL (TREE1I) = TOTAL (TREE1I) + 1
              TREE1(I) = - TREE1(I)
              OFFDIA = OFFDIA - 1
  200 continue
!
      IF ( OFFDIA .EQ. 0 )  GO TO 1100
      IF ( OFFDIA .LT. 0 )  GO TO 6000
!
!     ... FIND CONNECTED COMPONENTS OF GRAPH INDUCED BY THE NODES NOT
!         REMOVED.  'MXCOMP' IS THE LARGEST NUMBER OF COMPONENTS
!         REPRESENTABLE IN THE WORKING SPACE AVAILABLE.
!
      AVAIL = WRKLEN - OFFDIA
      MXCOMP = AVAIL/2
      START = OFFDIA + 1
      SIZE = START + MXCOMP
!
      IF  (MXCOMP .LE. 0)  GO TO 5100
!
      call GPSKCH (N     , DEGREE     , RSTART    , CONNEC, TREE1, OFFDIA, WORK, &
                   MXCOMP, WORK(START), WORK(SIZE), COMPNS, ERROR, SPACE       )
!
      IF ( ERROR .NE. 0 )  GO TO 5000
!
!     ... RECORD SPACE ACTUALLY USED  (NOT INCLUDING  NUMBERED )
!
      SPACE = 2*N + 3*(DEPTH) + 2*COMPNS + OFFDIA
!
!     ... SORT THE COMPONENT START POINTERS INTO INCREASING ORDER
!         OF SIZE OF COMPONENT
!
      IF (COMPNS .GT. 1) call GPSKCN (COMPNS, WORK(SIZE), WORK(START), ERROR)
          IF  (ERROR .NE. 0)  GO TO 6200
!
!     ... FOR EACH COMPONENT IN TURN, CHOOSE TO USE THE ORDERING OF THE
!         'FORWARD' TREE1 OR OF THE 'BACKWARD' TREE2 TO NUMBER THE NODES
!         IN THIS COMPONENT.  THE NUMBERING IS CHOSEN TO MINIMIZE THE
!         MAXIMUM INCREMENT TO ANY LEVEL.
!
      DO 1000 COMPON = 1, COMPNS
          PCSTRT = START + COMPON - 1
          CSTART = WORK (PCSTRT)
          PCSIZE = SIZE + COMPON - 1
          CSIZE = WORK (PCSIZE)
          CSTOP  = CSTART + CSIZE - 1
          IF ( ( CSIZE .LT. 0 ) .OR. ( CSIZE .GT. OFFDIA ) )  GO TO 6100
!
          DO 300 I = 1, DEPTH
              INC1(I) = 0
              INC2(I) = 0
  300     continue
!
          MXINC1 = 0
          MXINC2 = 0
!
          DO 400 I = CSTART, CSTOP
              WORKI = WORK(I)
              TWORKI = -TREE1 (WORKI)
              INC1 (TWORKI) = INC1 (TWORKI) + 1
              TWORKI =  TREE2 (WORKI)
              INC2 (TWORKI) = INC2 (TWORKI) + 1
  400     continue
!
!         ... BAROQUE TESTS BELOW DUPLICATE THE GIBBS-POOLE-STOCKMEYER-
!             CRANE PROGRAM, *** NOT *** THE PUBLISHED ALGORITHM.
!
          DO 500 I = 1, DEPTH
              IF ((INC1(I) .EQ. 0) .AND. (INC2(I) .EQ. 0))  GO TO 500
                  IF  (MXINC1  .LT.  TOTAL(I) + INC1(I)) MXINC1 = TOTAL(I) + INC1(I)
                  IF  (MXINC2  .LT.  TOTAL(I) + INC2(I)) MXINC2 = TOTAL(I) + INC2(I)
  500     continue
!
!         ... USE ORDERING OF NARROWER TREE UNLESS IT INCREASES
!             WIDTH MORE THAN WIDER TREE.  IN CASE OF TIE, USE TREE 2!
!
          IF ( (MXINC1 .GT. MXINC2)  .OR.                               &
              ( (MXINC1 .EQ. MXINC2) .AND. ( (WIDTH1 .GT. WIDTH2) .OR.  &
                                             ( (WIDTH1 .EQ. WIDTH2)     &
                                              .AND. ONEIS1) ) ) )       GO TO 700
!
              IF ( COMPON .EQ. 1 )  REVRS1 = .NOT. ONEIS1
!
              DO 600 I = 1, DEPTH
                  TOTAL(I) = TOTAL(I) + INC1(I)
  600         continue
              GO TO 1000
!
  700         IF ( COMPON .EQ. 1 )  REVRS1 = ONEIS1
              DO 800 I = CSTART, CSTOP
                  WORKI = WORK(I)
                  TREE1 (WORKI) = - TREE2 (WORKI)
  800         continue
!
              DO 900 I = 1, DEPTH
                  TOTAL(I) = TOTAL(I) + INC2(I)
  900         continue
!
 1000 continue
      GO TO 2000
!
!     ... DEFAULT WHEN THE REDUCED GRAPH IS EMPTY
!
 1100 REVRS1 = .true.
      SPACE = 2*N
!
 2000 RETURN
!
!     ------------------------------------------------------------------
!
!     ERROR FOUND ...
!
 5000 SPACE = -1
      GO TO 2000
!
 5100 SPACE = 2 - AVAIL
      ERROR = 131
      GO TO 2000
!
 6000 ERROR = 30
      GO TO 5000
!
 6100 ERROR = 31
      GO TO 5000
!
 6200 ERROR = 32
      GO TO 5000
!
   END SUBROUTINE GPSKCG
   

!=============================================================================================            
   SUBROUTINE GPSKCH (N   , DEGREE, RSTART, CONNEC, STATUS, NREDUC, WORK, MXCOMP, START, &
                      SIZE, COMPNS, ERROR , SPACE                                      )
!=============================================================================================            
!
!     ==================================================================
!
!     FIND THE CONNECTED COMPONENTS OF THE GRAPH INDUCED BY THE SET
!     OF NODES WITH POSITIVE 'STATUS'.  WE SHALL BUILD THE LIST OF
!     CONNECTED COMPONENTS IN 'WORK', WITH A LIST OF POINTERS
!     TO THE BEGINNING NODES OF COMPONENTS LOCATED IN 'START'
!
!
      integer(Ikind) :: N, RSTART(N), NREDUC, MXCOMP, COMPNS, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(1), STATUS(N), WORK(NREDUC),
      integer(Ikind) :: DEGREE(N), CONNEC(*), STATUS(N), WORK(NREDUC), START(MXCOMP), SIZE(MXCOMP)
!
!
!     PARAMETERS ...
!
!         N      -- DIMENSION OF THE ORIGINAL MATRIX
!         DEGREE, RSTART, CONNEC -- THE STRUCTURE OF THE ORIGINAL MATRIX
!
!         STATUS -- DERIVED FROM A LEVEL TREE. POSITIVE ENTRIES INDICATE
!                   ACTIVE NODES.  NODES WITH STATUS <= 0 ARE IGNORED.
!
!         NREDUC -- THE NUMBER OF ACTIVE NODES
!
!         WORK   -- WORK SPACE, USED AS A QUEUE TO BUILD CONNECTED
!                   COMPONENTS IN PLACE.
!
!         MXCOMP -- MAXIMUM NUMBER OF COMPONENTS ALLOWED BY CURRENT
!                   SPACE ALLOCATION.  MUST NOT BE VIOLATED.
!
!         START  -- POINTER TO BEGINNING OF  I-TH  CONNECTED COMPONENT
!
!         SIZE   -- SIZE OF EACH COMPONENT
!
!         COMPNS -- NUMBER OF COMPONENTS ACTUALLY FOUND
!
!         ERROR  -- SHOULD BE ZERO ON RETURN UNLESS WE HAVE TOO LITTLE
!                   SPACE OR WE ENCOUNTER AN ERROR IN THE DATA STRUCTURE
!
!         SPACE  -- MAXIMUM AMOUNT OF WORKSPACE USED / NEEDED
!
!     ==================================================================
!
      integer(Ikind) :: I, J, FREE, JPTR, NODE, JNODE, FRONT, CDGREE, ROOT
!
!     ------------------------------------------------------------------
!
!
!     REPEAT
!         << FIND AN UNASSIGNED NODE AND START A NEW COMPONENT >>
!         REPEAT
!             << ADD ALL NEW NEIGHBORS OF FRONT NODE TO QUEUE, >>
!             << REMOVE FRONT NODE.                            >>
!         UNTIL <<QUEUE EMPTY>>
!     UNTIL << ALL NODES ASSIGNED >>
!
      FREE   = 1
      COMPNS = 0
      ROOT   = 1
!
!     ... START OF OUTER REPEAT LOOP
!
!         ... FIND AN UNASSIGNED NODE
!
  100     DO 200 I = ROOT, N
              IF (STATUS(I) .LE. 0) GO TO 200
                  NODE = I
                  GO TO 300
  200     continue
          GO TO 6100
!
!         ... START NEW COMPONENT
!
  300     COMPNS = COMPNS + 1
          ROOT   = NODE + 1
          IF (COMPNS .GT. MXCOMP)  GO TO 5000
          START (COMPNS) = FREE
          WORK (FREE) = NODE
          STATUS (NODE) = -STATUS (NODE)
          FRONT = FREE
          FREE = FREE + 1
!
!             ... INNER REPEAT UNTIL QUEUE BECOMES EMPTY
!
  400         NODE = WORK (FRONT)
              FRONT = FRONT + 1
!
              JPTR = RSTART (NODE)
              CDGREE = DEGREE (NODE)
              DO 500 J = 1, CDGREE
                  JNODE = CONNEC (JPTR)
                  JPTR = JPTR + 1
                  IF (STATUS(JNODE) .LT. 0) GO TO 500
                  IF (STATUS(JNODE) .EQ. 0) GO TO 6000
                      STATUS (JNODE) = -STATUS (JNODE)
                      WORK (FREE) = JNODE
                      FREE = FREE + 1
  500         continue
!
              IF (FRONT .LT. FREE) GO TO 400
!
!         ... END OF INNER REPEAT.  COMPUTE SIZE OF COMPONENT AND
!             SEE IF There ARE MORE NODES TO BE ASSIGNED
!
          SIZE (COMPNS) = FREE - START (COMPNS)
          IF (FREE .LE. NREDUC)  GO TO 100
!
      IF (FREE .NE. NREDUC+1)  GO TO 6200
      RETURN
!
!     ------------------------------------------------------------------
!
 5000 SPACE = NREDUC - FREE + 1
      ERROR = 130
      RETURN
!
 6000 ERROR = 33
      SPACE = -1
      RETURN
!
 6100 ERROR = 34
      SPACE = -1
      RETURN
!
 6200 ERROR = 35
      SPACE = -1
      RETURN
      
   END SUBROUTINE GPSKCH


!=============================================================================================               
   SUBROUTINE GPSKCI (N, ACTIVE, DEPTH, LSTRUC, LVLLST, LVLPTR, LTOTAL, ERROR, SPACE)
!=============================================================================================   
!            
!     ==================================================================
!
!     TRANSITIONAL SUBROUTINE, ALGORITHM II TO IIIA OR IIIB.
!
!     CONVERT LEVEL STRUCTURE GIVEN AS VECTOR OF LEVEL NUMBERS FOR NODES
!     TO STRUCTURE AS LIST OF NODES BY LEVEL
!
!     N, ACTIVE, DEPTH -- PROBLEM SIZES
!     LSTRUC -- INPUT LEVEL STRUCTURE
!     LVLLST, LVLPTR -- OUTPUT LEVEL STRUCTURE
!     LTOTAL -- NUMBER OF NODES AT EACH LEVEL (PRECOMPUTED)
!
      integer(Ikind) :: N, ACTIVE, DEPTH, ERROR, SPACE
!
!IBM  integer *2  LSTRUC(N), LVLLST(ACTIVE), LVLPTR(1), LTOTAL(DEPTH)
      integer(Ikind) :: LSTRUC(N), LVLLST(ACTIVE), LVLPTR(*), LTOTAL(DEPTH)
!
!     ===============================================================
!
!     STRUCTURE OF WORKSPACE ..
!
!         INPUT (FROM COMBIN) ..
!
!     ------------------------------------------------------------------
!     :  NUMBERED  :  ..(N)..  :  TOTAL  :         ...        :  TREE  :
!     ------------------------------------------------------------------
!
!         OUTPUT (TO GPSKCJ OR GPSKCK) ..
!
!     ------------------------------------------------------------------
!     :  NUMBERED  :       ...             :  TLIST  :  TPTR  :  TREE  :
!     ------------------------------------------------------------------
!
!     HERE, NUMBERED IS THE SET OF NODES IN NUMBERED COMPONENTS
!         TOTAL IS A VECTOR OF LENGTH 'DEPTH' GIVING THE NUMBER
!         OF NODES IN EACH LEVEL OF THE 'TREE'.
!         TLIST, TPTR ARE LISTS OF NODES OF THE TREE, ARRANGED
!         BY LEVEL.  TLIST IS OF LENGTH 'ACTIVE', TPTR 'DEPTH+1'.
!
!     =================================================================
!
      integer(Ikind) :: I, ACOUNT, START, LEVEL, PLEVEL
!
!     ... ESTABLISH STARTING AND ENDING POINTERS FOR EACH LEVEL
!
      START = 1
      DO 100 I = 1, DEPTH
          LVLPTR(I) = START
          START = START + LTOTAL(I)
          LTOTAL(I) = START
  100 continue
      LVLPTR(DEPTH+1) = START
!
      ACOUNT = 0
      DO 300 I = 1, N
          IF (LSTRUC(I)) 200, 300, 6000
  200         LEVEL = -LSTRUC(I)
              LSTRUC(I) = LEVEL
              PLEVEL = LVLPTR (LEVEL)
              LVLLST (PLEVEL) = I
              LVLPTR (LEVEL) = LVLPTR (LEVEL) + 1
              ACOUNT = ACOUNT + 1
              IF (LVLPTR (LEVEL) .GT. LTOTAL (LEVEL))  GO TO 6100
  300 continue
!
!     ... RESET STARTING POINTERS
!
      LVLPTR(1) = 1
      DO 400 I = 1, DEPTH
          LVLPTR(I+1) = LTOTAL(I)
  400 continue
!
      RETURN
!
!     ------------------------------------------------------------------
!
 6000 ERROR = 40
      GO TO 6200
!
 6100 ERROR = 41
!
 6200 SPACE = -1
      RETURN
      
   END SUBROUTINE GPSKCI
      

!=============================================================================================                     
   SUBROUTINE GPSKCJ (N    , DEGREE, RSTART, CONNEC, NCOMPN, INVNUM, SNODE1, SNODE2, REVRS1, &        
                      DEPTH, LVLLST, LVLPTR, LVLNUM, ERROR , SPACE                         )
!=============================================================================================                     
!
!     ==================================================================
!
!     NUMBER THE NODES IN A GENERALIZED LEVEL STRUCTURE ACCORDING
!     TO A GENERALIZATION OF THE CUTHILL MCKEE STRATEGY.
!
!     N      -- DIMENSION OF ORIGINAL PROBLEM
!     DEGREE, RSTART, CONNEC -- GIVE STRUCTURE OF SPARSE AND
!                               SYMMETRIC MATRIX
!
!     NCOMPN -- NUMBER OF NODES IN THIS COMPONENT OF MATRIX GRAPH
!
!     INVNUM -- WILL BECOME A LIST OF THE ORIGINAL NODES IN THE ORDER
!               WHICH REDUCES THE BANDWIDTH OF THE MATRIX.
!
!     NXTNUM -- THE NEXT INDEX TO BE ASSIGNED (1 FOR FIRST COMPONENT)
!
!     REVRS1 -- IF .true., FIRST COMPONENT OF REDUCED GRAPH WAS NUMBERED
!               BACKWARDS.
!
!     LVLLST -- LIST OF NODES IN LEVEL TREE ORDERED BY LEVEL.
!
!     LVLPTR -- POSITION OF INITIAL NODE IN EACH LEVEL OF LVLLST.
!
!     LVLNUM -- LEVEL NUMBER OF EACH NODE IN COMPONENT
!
!
      integer(Ikind) :: N, RSTART(N), NCOMPN, SNODE1, SNODE2, DEPTH, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(1), INVNUM(NCOMPN),
      integer(Ikind) :: DEGREE(N), CONNEC(*), INVNUM(NCOMPN), LVLLST(NCOMPN), & 
!!                        LVLPTR(DEPTH), LVLNUM(N)
                        LVLPTR(*), LVLNUM(N)
      logical        :: REVRS1
!
!
!     ==================================================================
!
!     NUMBERING REQUIRES TWO QUEUES, WHICH CAN BE BUILD IN PLACE
!     IN INVNUM.
!
!
!     ==================================================================
!     A L G O R I T H M    S T R U C T U R E
!     ==================================================================
!
!     << SET QUEUE1 TO BE THE SET CONTAINING ONLY THE START NODE. >>
!
!     FOR LEVEL = 1 TO DEPTH DO
!
!         BEGIN
!         LOOP
!
!             REPEAT
!                 BEGIN
!                 << CNODE <- FRONT OF QUEUE1                        >>
!                 << ADD UNNUMBERED NEIGHBORS OF CNODE TO THE BACK   >>
!                 << OF QUEUE1 OR QUEUE2 (USE QUEUE1 IF NEIGHBOR     >>
!                 << AT SAME LEVEL, QUEUE2 IF AT NEXT LEVEL).  SORT  >>
!                 << THE NEWLY QUEUED NODES INTO INCREASING ORDER OF >>
!                 << DEGREE.  NUMBER CNODE, DELETE IT FROM QUEUE1.   >>
!                 END
!             UNTIL
!                 << QUEUE1 IS EMPTY >>
!
!         EXIT IF << ALL NODES AT THIS LEVEL NUMBERED >>
!
!             BEGIN
!             << FIND THE UNNUMBERED NODE OF MINIMAL DEGREE AT THIS >>
!             << LEVEL, RESTART QUEUE1 WITH THIS NODE.              >>
!             END
!
!         END << LOOP LOOP >>
!
!         << PROMOTE QUEUE2 TO BE INITIAL QUEUE1 FOR NEXT ITERATION >>
!         << OF  FOR  LOOP.                                         >>
!
!         END <<FOR LOOP>>
!
!     ==================================================================
!
!     STRUCTURE OF WORKSPACE ..
!
!     --------------------------------------------------------------
!     : NUMBERED :  QUEUE1  :  QUEUE2  : ... : TLIST : TPTR : TREE :
!     --------------------------------------------------------------
!
!     ON COMPLETION, WE HAVE ONLY A NEW, LONGER NUMBERED SET.
!
!     ==================================================================
      integer(Ikind) :: I     , BQ1   , BQ2   , FQ1   , INC   , CPTR  , CNODE, &
                        INODE , LEVEL , NLEFT , LSTART, LWIDTH, QUEUE1,        &
                        QUEUE2, CDGREE, XLEVEL, STNODE, ILEVEL, SQ1   , SQ2,   &
                        NSORT , LOWDG , BPTR  , LVLLSC, LVLLSB, INVNMI
      logical        :: FORWRD, RLEVEL
!
!     ------------------------------------------------------------------
!
!     ... GIBBS-POOLE-STOCKMEYER HEURISTIC CHOICE OF ORDER
!
      IF  (DEGREE(SNODE1) .GT. DEGREE(SNODE2))  GO TO 10
          FORWRD = REVRS1
          STNODE = SNODE1
          GO TO 20
!
   10     FORWRD = .NOT. REVRS1
          STNODE = SNODE2
!
!     ... SET UP INITIAL QUEUES AT FRONT OF 'INVNUM' FOR FORWRD ORDER,
!         AT BACK FOR REVERSED ORDER.
!
   20 IF (FORWRD) GO TO 100
          INC = -1
          QUEUE1 = NCOMPN
          GO TO 200
!
  100     INC = +1
          QUEUE1 = 1
!
  200 INVNUM (QUEUE1) = STNODE
      RLEVEL = (LVLNUM(STNODE) .EQ. DEPTH)
      LVLNUM (STNODE) = 0
      FQ1 = QUEUE1
      BQ1 = QUEUE1 + INC
!
!     -------------------------------
!     NUMBER NODES LEVEL BY LEVEL ...
!     -------------------------------
!
      DO 3000 XLEVEL = 1, DEPTH
          LEVEL = XLEVEL
          IF  (RLEVEL)  LEVEL = DEPTH - XLEVEL + 1
!
          LSTART = LVLPTR (LEVEL)
          LWIDTH = LVLPTR (LEVEL+1) - LSTART  ! pb si level=depth
          NLEFT = LWIDTH
          QUEUE2 = QUEUE1 + INC*LWIDTH
          BQ2 = QUEUE2
!
!         ==============================================================
!         ... 'LOOP' CONSTRUCT BEGINS AT STATEMENT 1000
!                 THE INNER 'REPEAT' WILL BE DONE AS MANY TIMES AS
!                 IS NECESSARY TO NUMBER ALL THE NODES AT THIS LEVEL.
!         ==============================================================
!
 1000     continue
!
!             ==========================================================
!             ... REPEAT ... UNTIL QUEUE1 BECOMES EMPTY
!                 TAKE NODE FROM FRONT OF QUEUE1, FIND EACH OF ITS
!                 NEIGHBORS WHICH HAVE NOT YET BEEN NUMBERED, AND
!                 ADD THE NEIGHBORS TO QUEUE1 OR QUEUE2 ACCORDING TO
!                 THEIR LEVELS.
!             ==========================================================
!
 1100             CNODE = INVNUM (FQ1)
                  FQ1 = FQ1 + INC
                  SQ1 = BQ1
                  SQ2 = BQ2
                  NLEFT = NLEFT - 1
!
                  CPTR = RSTART (CNODE)
                  CDGREE = DEGREE (CNODE)
                  DO 1300 I = 1, CDGREE
                      INODE = CONNEC (CPTR)
                      CPTR = CPTR + 1
                      ILEVEL = LVLNUM (INODE)
                      IF (ILEVEL .EQ. 0)  GO TO 1300
                          LVLNUM (INODE) = 0
                          IF ( ILEVEL .EQ. LEVEL ) GO TO 1200
!
!riad                              IF  (IABS(LEVEL-ILEVEL) .NE. 1) GO TO 6400
                              IF  (ABS(LEVEL-ILEVEL) .NE. 1) GO TO 6400
                                  INVNUM (BQ2) = INODE
                                  BQ2 = BQ2 + INC
                                  GO TO 1300
!
 1200                             INVNUM (BQ1) = INODE
                                  BQ1 = BQ1 + INC
 1300             continue
!
!                 ==================================================
!                 ... SORT THE NODES JUST ADDED TO QUEUE1 AND QUEUE2
!                     SEPARATELY INTO INCREASING ORDER OF DEGREE.
!                 ==================================================
!
!riad                  IF  (IABS (BQ1 - SQ1) .LE. 1)  GO TO 1500
                  IF  (ABS (BQ1 - SQ1) .LE. 1)  GO TO 1500
!riad                      NSORT = IABS (BQ1 - SQ1)
                      NSORT = ABS (BQ1 - SQ1)
                      IF  (FORWRD)  GO TO 1400
                          call GPSKCP (NSORT, INVNUM(BQ1+1), N, DEGREE, ERROR)
                          IF  (ERROR .NE. 0)  GO TO 6600
                          GO TO 1500

 1400                     call GPSKCQ (NSORT, INVNUM(SQ1), N, DEGREE, ERROR)
                          IF  (ERROR .NE. 0)  GO TO 6600

 !riad 1500             IF  (IABS (BQ2 - SQ2) .LE. 1)  GO TO 1700
 1500             IF  (ABS (BQ2 - SQ2) .LE. 1)  GO TO 1700
 !riad                     NSORT = IABS (BQ2 - SQ2)
                      NSORT = ABS (BQ2 - SQ2)
                      IF  (FORWRD)  GO TO 1600
                          call GPSKCP (NSORT, INVNUM(BQ2+1), N, DEGREE, ERROR)
                          IF  (ERROR .NE. 0)  GO TO 6600
                          GO TO 1700

 1600                     call GPSKCQ (NSORT, INVNUM(SQ2), N, DEGREE, ERROR)
                          IF  (ERROR .NE. 0)  GO TO 6600
!
!                     ... END OF REPEAT LOOP
!
 1700             IF  (FQ1 .NE. BQ1)  GO TO 1100
!
!         ==============================================================
!         ... QUEUE1 IS NOW EMPTY ...
!             IF THERE ARE ANY UNNUMBERED NODES LEFT AT THIS LEVEL,
!             FIND THE ONE OF MINIMAL DEGREE AND RETURN TO THE
!             REPEAT LOOP ABOVE.
!         ==============================================================
!
          IF  ((BQ1 .EQ. QUEUE2) .AND. (NLEFT .EQ. 0))  GO TO 2900

              IF ((NLEFT .LE. 0) .OR. (NLEFT .NE. INC * (QUEUE2 - BQ1))) GO TO 6200

              LOWDG = N + 1
              BPTR  = N + 1
              CPTR  = LSTART - 1
              DO 2800 I = 1, NLEFT
 2600             CPTR   = CPTR + 1
                  LVLLSC = LVLLST (CPTR)
                  IF (LVLNUM (LVLLSC) .EQ. LEVEL)  GO TO 2700
                      IF (LVLNUM (LVLLSC) .NE. 0)  GO TO 6300
                      GO TO 2600

 2700             IF  (DEGREE(LVLLSC) .GE. LOWDG)  GO TO 2800
                      LOWDG = DEGREE (LVLLSC)
                      BPTR  = CPTR

 2800         continue
!
!             ... MINIMAL DEGREE UNNUMBERED NODE FOUND ...
!
              IF  (BPTR .GT. N)  GO TO 6500
              LVLLSB = LVLLST (BPTR)
              INVNUM (BQ1) = LVLLSB
              LVLNUM (LVLLSB) = 0
              BQ1 = BQ1 + INC
              GO TO 1000
!
!             =============================================
!             ... ADVANCE QUEUE POINTERS TO MAKE QUEUE2 THE
!                 NEW QUEUE1 FOR THE NEXT ITERATION.
!             =============================================
!
 2900     QUEUE1 = QUEUE2
          FQ1 = QUEUE1
          BQ1 = BQ2
          IF  ((BQ1 .EQ. FQ1) .AND. (XLEVEL .LT. DEPTH))  GO TO 6100

 3000 continue
!
!     ... CHANGE SIGN OF DEGREE TO MARK THESE NODES AS 'NUMBERED'
!
      DO 3100 I = 1, NCOMPN
          INVNMI = INVNUM(I)
          DEGREE (INVNMI) = -DEGREE (INVNMI)
 3100 continue

      RETURN
!
!     ------------------------------------------------------------------
!
 6000 SPACE = -1
      RETURN

 6100 ERROR = 51
      GO TO 6000

 6200 ERROR = 52
      GO TO 6000

 6300 ERROR = 53
      GO TO 6000

 6400 ERROR = 54
      GO TO 6000

 6500 ERROR = 55
      GO TO 6000

 6600 ERROR = 56
      GO TO 6000

   END SUBROUTINE GPSKCJ
   
   
!=============================================================================================                     
   SUBROUTINE  GPSKCK  (N     , DEGREE, RSTART, CONNEC, WRKLEN, NXTNUM, WORK, NCOMPN, DEPTH, &   
                        LVLLST, LVLPTR, LVLNUM, ERROR , SPACE                              )
!=============================================================================================                     
      integer(Ikind) :: N, RSTART(N), WRKLEN, NXTNUM, NCOMPN, DEPTH, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(1), WORK(WRKLEN), LVLLST(N),
      integer(Ikind) :: DEGREE(N), CONNEC(*), WORK(WRKLEN), LVLLST(N), LVLPTR(DEPTH), LVLNUM(N)
!
!     ==================================================================
!
!     NUMBER NODES IN A GENERALIZED LEVEL STRUCTURE ACCORDING TO
!     A GENERALIZATION OF THE KING ALGORITHM, WHICH REDUCES
!     THE PROFILE OF THE SPARSE SYMMETRIC MATRIX.
!
!     ---------------------
!
!     CODE USES A PRIORITY QUEUE TO CHOOSE THE NEXT NODE TO BE NUMBERED
!     THE PRIORITY QUEUE IS REPRESENTED BY A SIMPLE LINEAR-LINKED LIST
!     TO SAVE SPACE.  THIS WILL REQUIRE MORE SEARCHING THAN A FULLY
!     LINKED REPRESENTATION, BUT THE DATA MANIPULATION IS SIMPLER.
!
!     -------------------
!
!     << ESTABLISH PRIORITY QUEUE 'ACTIVE' FOR LEVEL 1 NODES >>
!
!     FOR I = 1 TO DEPTH DO
!         << SET QUEUE 'QUEUED' TO BE EMPTY, LIST 'NEXT' TO BE >>
!         << SET OF NODES AT NEXT LEVEL.                       >>
!
!         FOR J = 1 TO 'NODES AT THIS LEVEL' DO
!             << FIND FIRST NODE IN ACTIVE WITH MINIMAL CONNECTIONS >>
!             << TO 'NEXT'.  NUMBER THIS NODE AND REMOVE HIM FROM   >>
!             << 'ACTIVE'.  FOR EACH NODE IN 'NEXT' WHICH CONNECTED >>
!             << TO THIS NODE, MOVE IT TO 'QUEUED' AND REMOVE IT    >>
!             << FROM 'NEXT'.                                       >>
!
!         << SET NEW QUEUE 'ACTIVE' TO BE 'QUEUED' FOLLOWED BY ANY >>
!         << NODES STILL IN 'NEXT'.                                >>
!
!     ==================================================================
!
!     DATA STRUCTURE ASSUMPTIONS:
!     THE FIRST 'NXTNUM-1' ELEMENTS OF  WORK  ARE ALREADY IN USE.
!     THE LEVEL STRUCTURE 'LVLLST' IS CONTIGUOUS WITH  WORK, THAT IS,
!     IT RESIDES IN ELEMENTS  WRKLEN+1, ...  OF  WORK.  'LVLPTR' AND
!     'LVLNUM' ARE ALSO EMBEDDED IN WORK, BEHIND 'LVLLST'.  THE
!     THREE VECTORS ARE PASSED SEPARATELY TO CLARIFY THE INDEXING,
!     BUT THE QUEUES DEVELOPED WILL BE ALLOWED TO OVERRUN 'LVLLST'
!     AS NEEDED.
!
!     ... BUILD THE FIRST 'ACTIVE' QUEUE STARTING W1 LOCATIONS FROM
!         THE FRONT OF THE CURRENT WORKING AREA  (W1 IS THE WIDTH OF THE
!         FIRST LEVEL).  BUILD THE FIRST 'QUEUED' QUEUE STARTING FROM
!         THE BACK OF WORK SPACE.  THE LIST 'NEXT' WILL BE realIZED
!         IMPLICITLY IN 'LVLNUM' AS:
!                  LVLNUM(I) > 0   <== LEVEL NUMBER OF NODE.  'NEXT' IS
!                                      SET WITH LVLNUM(I) = LEVEL+1
!                  LVLNUM(I) = 0   <== I-TH NODE IS IN 'QUEUED' OR IS
!                                      NOT IN THIS COMPONENT OF GRAPH,
!                                      OR HAS JUST BEEN NUMBERED.
!                  LVLNUM(I) < 0   <== I-TH NODE IS IN 'ACTIVE' AND IS
!                                      CONNECTED TO -LVLNUM(I) NODES IN
!                                      'NEXT'.
!
!     ==================================================================
!
!     STRUCTURE OF WORKSPACE ..
!
!     --------------------------------------------------------------
!     : NUMBERED : DONE : ACTIVE : ALEVEL : ... : QUEUED : LVLLST :
!     --------------------------------------------------------------
!
!     -------------------
!       LVLPTR : LVLNUM :
!     -------------------
!
!     IN THE ABOVE,
!         NUMBERED IS THE SET OF NODES ALREADY NUMBERED FROM
!         PREVIOUS COMPONENTS AND EARLIER LEVELS OF THIS COMPONENT.
!         DONE, ACTIVE, ALEVEL  ARE VECTORS OF LENGTH THE WIDTH OF
!         THE CURRENT LEVEL.  ACTIVE IS A SET OF INDICES INTO
!         ALEVEL.  AS THE NODES IN ALEVEL ARE NUMBERED, THEY
!         ARE PLACED INTO 'DONE'.
!         QUEUED IS A QUEUE OF NODES IN THE 'NEXT' LEVEL, WHICH
!         GROWS FROM THE START OF THE 'NEXT' LEVEL IN LVLLST
!         FORWARDS TOWARD 'ALEVEL'.  QUEUED IS OF LENGTH NO MORE
!         THAN THE WIDTH OF THE NEXT LEVEL.
!         LVLLST IS THE LIST OF UNNUMBERED NODES IN THE TREE,
!         ARRANGED BY LEVEL.
!
!     ==================================================================
      integer(Ikind) :: I, J, K, PTR, JPTR, KPTR, LPTR, MPTR, PPTR, RPTR,    &
                        MPPTR, JNODE, KNODE, CNODE, LEVEL, LOWDG, UNUSED,    &
                        MXQUE, NNEXT, ASTART, MINDG, LSTART, LWIDTH, ACTIVE, &
                        QUEUEB, QUEUED, QCOUNT, NCONNC, NACTIV, CDGREE,      &
                        LDGREE, NFINAL, JDGREE, STRTIC, ADDED, TWRKLN,       &
                        LVLLSL, CONNEJ, CONNER, ASTPTR, ACTPTR, ACTIVI,      &
                        ASTRTI, QUEUEI, ACPPTR
!
!     ------------------------------------------------------------------
!
      TWRKLN = WRKLEN + NCOMPN + N + DEPTH + 1
      UNUSED = TWRKLN

      ASTART = LVLPTR(1)
      LWIDTH = LVLPTR(2) - ASTART
      ASTART = WRKLEN  + 1
      ACTIVE = NXTNUM + LWIDTH + 1
      NACTIV = LWIDTH
      NFINAL = NXTNUM + NCOMPN

      NNEXT = LVLPTR(3) - LVLPTR(2)
      QUEUED = WRKLEN
      QUEUEB = QUEUED
      MXQUE = ACTIVE + LWIDTH
!
!     ... BUILD FIRST PRIORITY QUEUE 'ACTIVE'
!
      LOWDG = - (N + 1)
      LPTR = LVLPTR(1)
      DO 200 I = 1, LWIDTH
          NCONNC = 0
          LVLLSL= LVLLST (LPTR)
          JPTR = RSTART (LVLLSL)
          LDGREE = DEGREE(LVLLSL)
          DO 100 J = 1, LDGREE
              CONNEJ = CONNEC (JPTR)
              IF ( LVLNUM (CONNEJ) .EQ. 2 )  NCONNC = NCONNC - 1
              JPTR = JPTR + 1
  100     continue

          ACTIVI = ACTIVE + I - 1
          WORK (ACTIVI) = I
          LVLNUM (LVLLSL) = NCONNC
!riad          LOWDG = MAX0 (LOWDG, NCONNC)
          LOWDG = MAX (LOWDG, NCONNC)
          LPTR = LPTR + 1
  200 continue
      WORK (ACTIVE-1) = 0
!
!     -----------------------------------
!     NOW NUMBER NODES LEVEL BY LEVEL ...
!     -----------------------------------

      DO 2000 LEVEL = 1, DEPTH
!
!         ... NUMBER ALL NODES IN THIS LEVEL
!
          DO 1100 I = 1, LWIDTH
              PPTR = -1
              PTR = WORK (ACTIVE-1)
              IF (NNEXT .EQ. 0)  GO TO 1000
!
!                 ... IF NODES REMAIN IN NEXT, FIND THE EARLIEST NODE
!                     IN ACTIVE OF MINIMAL DEGREE.
!
                  MINDG = -(N+1)
                  DO 400 J = 1, NACTIV
                      ASTPTR = ASTART + PTR
                      CNODE = WORK (ASTPTR)
                      IF ( LVLNUM (CNODE) .EQ. LOWDG )  GO TO 500
                      IF ( LVLNUM (CNODE) .LE. MINDG )  GO TO 300
                          MPPTR = PPTR
                          MPTR = PTR
                          MINDG = LVLNUM (CNODE)
  300                 PPTR = PTR
                      ACTPTR = ACTIVE + PTR
                      PTR = WORK (ACTPTR)
  400             continue
!
!                     ... ESTABLISH  PTR  AS FIRST MIN DEGREE NODE
!                         PPTR AS PREDECESSOR IN LIST.

                  PTR = MPTR
                  PPTR = MPPTR

  500             ASTPTR = ASTART + PTR
                  CNODE = WORK (ASTPTR)
                  LOWDG = LVLNUM (CNODE)
                  LVLNUM (CNODE) = 0
                  JPTR = RSTART (CNODE)
!
!                 ... UPDATE CONNECTION COUNTS FOR ALL NODES WHICH
!                     CONNECT TO  CNODE'S  NEIGHBORS IN  NEXT.
!
                  CDGREE = DEGREE(CNODE)
                  STRTIC = QUEUEB

                  DO 700 J = 1, CDGREE
                      JNODE = CONNEC (JPTR)
                      JPTR = JPTR + 1
                      IF (LVLNUM (JNODE) .NE. LEVEL+1 )  GO TO 700
                          IF (QUEUEB .LT. MXQUE)  GO TO 5000
                          WORK (QUEUEB) = JNODE
                          QUEUEB = QUEUEB - 1
                          NNEXT = NNEXT - 1
                          LVLNUM (JNODE) = 0
                          IF  (NACTIV .EQ. 1)  GO TO 700
                            KPTR = RSTART (JNODE)
                            JDGREE = DEGREE (JNODE)
                            DO 600 K = 1, JDGREE
                                KNODE = CONNEC (KPTR)
                                KPTR = KPTR + 1
                                IF (LVLNUM (KNODE) .GE. 0)  GO TO 600
                                    LVLNUM (KNODE) = LVLNUM (KNODE) + 1
                                    IF  (LOWDG .LT. LVLNUM(KNODE)) LOWDG = LVLNUM(KNODE)
  600                       continue
  700             continue
!
!                 ... TO MIMIC THE ALGORITHM AS IMPLEMENTED BY GIBBS,
!                     SORT THE NODES JUST ADDED TO THE QUEUE INTO
!                     INCREASING ORDER OF ORIGINAL INDEX. (BUT, BECAUSE
!                     THE QUEUE IS STORED BACKWARDS IN MEMORY, THE SORT
!                     ROUTINE IS callED FOR DECREASING INDEX.)
!
!                     TREAT  0, 1 OR 2  NODES ADDED AS SPECIAL CASES
!
                  ADDED = STRTIC - QUEUEB
                  IF  (ADDED - 2)  1000, 800, 900

  800                 IF (WORK(STRTIC-1) .GT. WORK(STRTIC))  GO TO 1000
                          JNODE = WORK(STRTIC)
                          WORK(STRTIC) = WORK(STRTIC-1)
                          WORK(STRTIC-1) = JNODE
                          GO TO 1000

  900                 call GPSKCO (ADDED, WORK(QUEUEB+1), ERROR)
                      IF  (ERROR .NE. 0)  GO TO 5500
!
!
!                 ... NUMBER THIS NODE AND DELETE IT FROM 'ACTIVE'.
!                     MARK IT UNAVAILABLE BY CHANGING SIGN OF DEGREE
!
 1000         NACTIV = NACTIV - 1
              ASTPTR = ASTART + PTR
              CNODE = WORK (ASTPTR)
              WORK (NXTNUM) = CNODE
              DEGREE (CNODE) = -DEGREE (CNODE)
              NXTNUM = NXTNUM + 1
!
!             ... DELETE LINK TO THIS NODE FROM LIST
!
              ACPPTR = ACTIVE + PPTR
              ACTPTR = ACTIVE + PTR
              WORK (ACPPTR) = WORK (ACTPTR)
 1100     continue
!
!         ... NOW MOVE THE QUEUE 'QUEUED' FORWARD, AT THE SAME
!             TIME COMPUTING CONNECTION COUNTS FOR ITS ELEMENTS.
!             then DO THE SAME FOR THE REMAINING NODES IN 'NEXT'.
!
!riad          UNUSED = MIN0 (UNUSED, QUEUEB - MXQUE)
          UNUSED = MIN (UNUSED, QUEUEB - MXQUE)
          IF ( NXTNUM .NE. ACTIVE-1 )  GO TO 5100
          IF ( LEVEL .EQ. DEPTH ) GO TO 2000
              LSTART = LVLPTR (LEVEL+1)
              LWIDTH = LVLPTR (LEVEL+2) - LSTART
              ACTIVE = NXTNUM + LWIDTH + 1
              ASTART = ACTIVE + LWIDTH
              NACTIV = LWIDTH
              MXQUE = ASTART + LWIDTH
              IF ( MXQUE .GT. QUEUEB + 1 )  GO TO 5000
!riad              UNUSED = MIN0 (UNUSED, QUEUEB - MXQUE + 1)
              UNUSED = MIN (UNUSED, QUEUEB - MXQUE + 1)

              QCOUNT = QUEUED - QUEUEB
              LOWDG = -N-1
              WORK (ACTIVE-1) = 0

              PTR = LSTART
              DO 1600 I = 1, LWIDTH
!
!                 ... CHOOSE NEXT NODE FROM EITHER 'QUEUED' OR 'NEXT'
!
                  IF (I .GT. QCOUNT )  GO TO 1200
                      QUEUEI = QUEUED + 1 - I
                      CNODE = WORK (QUEUEI)
                      GO TO 1300

 1200                 CNODE = LVLLST (PTR)
                      PTR = PTR + 1
                      IF ( PTR .GT. LVLPTR(LEVEL+2) )  GO TO 5200
                          IF (LVLNUM (CNODE) .GT. 0)  GO TO 1300
                              GO TO 1200

 1300             IF ( LEVEL+1 .EQ. DEPTH ) GO TO 1500

                      RPTR = RSTART (CNODE)
                      NCONNC = 0
                      JDGREE = DEGREE (CNODE)
                      DO 1400 J = 1, JDGREE
                          CONNER = CONNEC (RPTR)
                          IF ( LVLNUM (CONNER) .EQ. LEVEL+2 ) NCONNC = NCONNC - 1
                          RPTR = RPTR + 1
 1400                 continue
                      LVLNUM (CNODE) = NCONNC
!riad                      LOWDG = MAX0 (LOWDG, NCONNC)
                      LOWDG = MAX (LOWDG, NCONNC)
!
!             ... ADD CNODE TO NEW 'ACTIVE' QUEUE
!
 1500             ACTIVI = ACTIVE + (I - 1)
                  ASTRTI = ASTART + (I - 1)
                  WORK (ACTIVI) = I
                  WORK (ASTRTI) = CNODE
 1600         continue

              IF (DEPTH .EQ. LEVEL+1 ) GO TO 1700
                  NNEXT = LVLPTR (LEVEL+3) - LVLPTR (LEVEL+2)
                  QUEUED = LSTART - 1 + LWIDTH + WRKLEN
                  QUEUEB = QUEUED
                  GO TO 2000

 1700             NNEXT = 0

 2000 continue

      IF  (NXTNUM .NE. NFINAL)  GO TO 5300
!riad      SPACE = MAX0 (SPACE, TWRKLN - UNUSED)
      SPACE = MAX (SPACE, TWRKLN - UNUSED)
      RETURN
!
!
!     ------------------------------------------------------------------
!
 5000 SPACE = NACTIV + NNEXT
      ERROR = 160
      RETURN

 5100 ERROR = 61
      GO TO 5400

 5200 ERROR = 62
      GO TO 5400

 5300 ERROR = 63

 5400 RETURN

 5500 ERROR = 64
      GO TO 5400

   END SUBROUTINE  GPSKCK
   

!=============================================================================================                     
   SUBROUTINE GPSKCL (N    , DEGREE, RSTART, CONNEC, INVNUM, NEWNUM, OLDNUM, BANDWD, PROFIL, &
                      ERROR, SPACE                                                         )
!=============================================================================================                     
      integer(Ikind) :: N, RSTART(N), BANDWD, PROFIL, ERROR, SPACE
!IBM  integer *2  DEGREE(N), CONNEC(1), INVNUM(N), NEWNUM(N), OLDNUM(N)
      integer(Ikind) :: DEGREE(N), CONNEC(*), INVNUM(N), NEWNUM(N), OLDNUM(N)
!
!     ==================================================================
!
!
!     COMPUTE THE BANDWIDTH AND PROFILE FOR THE RENUMBERING GIVEN
!     BY 'INVNUM' AND ALSO FOR THE RENUMBERING GIVEN BY 'OLDNUM'.
!     'NEWNUM' WILL BE A PERMUTATION VECTOR COPY OF THE NODE
!     LIST 'INVNUM'.
!
!     ==================================================================
!
      integer(Ikind) :: I     , J     , JPTR  , IDGREE, OLDBND, OLDPRO, NEWBND, &
                        NEWPRO, OLDRWD, NEWRWD, OLDORG, NEWORG, JNODE , INVNMI
!
!     ------------------------------------------------------------------
!
!     ... CREATE NEWNUM AS A PERMUTATION VECTOR
!
      DO 100 I = 1, N
          INVNMI = INVNUM (I)
          NEWNUM (INVNMI) = I
  100 continue
!
!     ... COMPUTE PROFILE AND BANDWIDTH FOR BOTH THE OLD AND THE NEW
!         ORDERINGS.
!
      OLDBND = 0
      OLDPRO = 0
      NEWBND = 0
      NEWPRO = 0

      DO 300 I = 1, N
          IF (DEGREE(I) .EQ. 0)  GO TO 300
          IF (DEGREE(I) .GT. 0)  GO TO 6000
              IDGREE = -DEGREE(I)
              DEGREE(I) = IDGREE
              NEWORG = NEWNUM(I)
              OLDORG = OLDNUM(I)
              NEWRWD = 0
              OLDRWD = 0
              JPTR = RSTART (I)
!
!             ... FIND NEIGHBOR WHICH IS NUMBERED FARTHEST AHEAD OF THE
!                 CURRENT NODE.
!
              DO 200 J = 1, IDGREE
                  JNODE = CONNEC(JPTR)
                  JPTR = JPTR + 1
!riad                  NEWRWD = MAX0 (NEWRWD, NEWORG - NEWNUM(JNODE))
!riad                  OLDRWD = MAX0 (OLDRWD, OLDORG - OLDNUM(JNODE))
                  NEWRWD = MAX (NEWRWD, NEWORG - NEWNUM(JNODE))
                  OLDRWD = MAX (OLDRWD, OLDORG - OLDNUM(JNODE))

  200         continue

              NEWPRO = NEWPRO + NEWRWD
!riad              NEWBND = MAX0 (NEWBND, NEWRWD)
              NEWBND = MAX (NEWBND, NEWRWD)
              OLDPRO = OLDPRO + OLDRWD
!riad              OLDBND = MAX0 (OLDBND, OLDRWD)
              OLDBND = MAX (OLDBND, OLDRWD)
  300 continue

!     ... IF NEW ORDERING HAS BETTER BANDWIDTH THAN OLD ORDERING,
!         REPLACE OLD ORDERING BY NEW ORDERING
!
      IF  (NEWBND .GT. OLDBND)  GO TO 500
          BANDWD = NEWBND
          PROFIL = NEWPRO
          DO 400 I = 1, N
              OLDNUM(I) = NEWNUM(I)
  400     continue
          GO TO 600
!
!     ... RETAIN OLD ORDERING
!
  500     BANDWD = OLDBND
          PROFIL = OLDPRO

  600 RETURN
!
!     ------------------------------------------------------------------
!
 6000 SPACE = -1
      ERROR = 70
      RETURN
!
   END SUBROUTINE GPSKCL


!=============================================================================================                     
   SUBROUTINE GPSKCM (N    , DEGREE, RSTART, CONNEC, INVNUM, NEWNUM,  OLDNUM, BANDWD, PROFIL, &
                      ERROR, SPACE                                                          )
!=============================================================================================                     
      integer(Ikind) :: N, RSTART(N), BANDWD, PROFIL, ERROR, SPACE
!
!IBM  integer *2  DEGREE(N), CONNEC(N), INVNUM(N), NEWNUM(N), OLDNUM(N)
      integer(Ikind) :: DEGREE(N), CONNEC(N), INVNUM(N), NEWNUM(N), OLDNUM(N)
!
!     ==================================================================
!
!
!     COMPUTE THE BANDWIDTH AND PROFILE FOR THE RENUMBERING GIVEN
!     BY 'INVNUM', BY THE REVERSE OF NUMBERING 'INVNUM', AND ALSO
!     BY THE RENUMBERING GIVEN IN 'OLDNUM'.
!     'NEWNUM' WILL BE A PERMUTATION VECTOR COPY OF THE NODE
!     LIST 'INVNUM'.
!
!     ==================================================================
!
      integer(Ikind) :: I     , J     , JPTR  , IDGREE, OLDBND, OLDPRO, NEWBND, NEWPRO,  &
                        OLDRWD, NEWRWD, OLDORG, NEWORG, JNODE , NRVBND, NRVPRO, NRVORG,  &
                        NRVRWD, INVNMI, NMIP1
!
!     ------------------------------------------------------------------
!
!     ... CREATE NEWNUM AS A PERMUTATION VECTOR
!
      DO 100 I = 1, N
          INVNMI = INVNUM (I)
          NEWNUM (INVNMI) = I
  100 continue
!
!     ... COMPUTE PROFILE AND BANDWIDTH FOR BOTH THE OLD AND THE NEW
!         ORDERINGS.
!
      OLDBND = 0
      OLDPRO = 0
      NEWBND = 0
      NEWPRO = 0
      NRVBND = 0
      NRVPRO = 0

      DO 300 I = 1, N
          IF (DEGREE(I) .EQ. 0)  GO TO 300
          IF (DEGREE(I) .GT. 0)  GO TO 6000
              IDGREE = -DEGREE(I)
              DEGREE(I) = IDGREE
              NEWRWD = 0
              OLDRWD = 0
              NRVRWD = 0
              NEWORG = NEWNUM(I)
              OLDORG = OLDNUM(I)
              NRVORG = N - NEWNUM(I) + 1
              JPTR = RSTART (I)
!
!             ... FIND NEIGHBOR WHICH IS NUMBERED FARTHEST AHEAD OF THE
!                 CURRENT NODE.
!
              DO 200 J = 1, IDGREE
                  JNODE = CONNEC(JPTR)
                  JPTR = JPTR + 1
!riad                  NEWRWD = MAX0 (NEWRWD, NEWORG - NEWNUM(JNODE))
!riad                  OLDRWD = MAX0 (OLDRWD, OLDORG - OLDNUM(JNODE))
!riad                  NRVRWD = MAX0 (NRVRWD, NRVORG - N + NEWNUM(JNODE) - 1)
                  NEWRWD = MAX (NEWRWD, NEWORG - NEWNUM(JNODE))
                  OLDRWD = MAX (OLDRWD, OLDORG - OLDNUM(JNODE))
                  NRVRWD = MAX (NRVRWD, NRVORG - N + NEWNUM(JNODE) - 1)

  200         continue

              NEWPRO = NEWPRO + NEWRWD
!riad              NEWBND = MAX0 (NEWBND, NEWRWD)
              NEWBND = MAX (NEWBND, NEWRWD)
              NRVPRO = NRVPRO + NRVRWD
!riad              NRVBND = MAX0 (NRVBND, NRVRWD)
              NRVBND = MAX (NRVBND, NRVRWD)
              OLDPRO = OLDPRO + OLDRWD
!riad              OLDBND = MAX0 (OLDBND, OLDRWD)
              OLDBND = MAX (OLDBND, OLDRWD)
  300 continue
!
!     ... IF NEW ORDERING HAS BETTER BANDWIDTH THAN OLD ORDERING,
!         REPLACE OLD ORDERING BY NEW ORDERING
!
      IF  ((NEWPRO .GT. OLDPRO)  .OR. (NEWPRO .GT. NRVPRO)) GO TO 500
          BANDWD = NEWBND
          PROFIL = NEWPRO
          DO 400 I = 1, N
              OLDNUM(I) = NEWNUM(I)
  400     continue
          GO TO 800
!
!     ... CHECK NEW REVERSED ORDERING FOR BEST PROFILE
!
  500 IF  (NRVPRO .GT. OLDPRO)  GO TO 700
          BANDWD = NRVBND
          PROFIL = NRVPRO
          DO 600 I = 1, N
              OLDNUM(I) = N - NEWNUM(I) + 1
              IF  (I .GT. N/2)  GO TO 600
                  J = INVNUM(I)
                  NMIP1 = (N + 1) - I
                  INVNUM(I) = INVNUM (NMIP1)
                  INVNUM (NMIP1) = J
  600     continue
          GO TO 800
!
!
!     ... RETAIN OLD ORDERING
!
  700     BANDWD = OLDBND
          PROFIL = OLDPRO

  800 RETURN
!
!     ------------------------------------------------------------------
!
 6000 ERROR = 71
      SPACE = -1
      RETURN

   END SUBROUTINE GPSKCM
   

!=============================================================================================                        
   SUBROUTINE GPSKCN (N, KEY, DATA, ERROR)                       
!=============================================================================================                     
!
!     ==================================================================
!
!     I N S E R T I O N    S O R T
!
!     INPUT:
!         N    -- NUMBER OF ELEMENTS TO BE SORTED
!         KEY  -- AN ARRAY OF LENGTH  N  CONTAINING THE VALUES
!                 WHICH ARE TO BE SORTED
!         DATA -- A SECOND ARRAY OF LENGTH  N  CONTAINING DATA
!                 ASSOCIATED WITH THE INDIVIDUAL KEYS.
!
!     OUTPUT:
!         KEY  -- WILL BE ARRANGED SO THAT VALUES ARE IN DECREASING
!                 ORDER
!         DATA -- REARRANGED TO CORRESPOND TO REARRANGED KEYS
!         ERROR -- WILL BE ZERO UNLESS THE PROGRAM IS MALFUNCTIONING,
!                  IN WHICH CASE IT WILL BE EQUAL TO 1.
!
!
!     ==================================================================
!
      integer(Ikind) :: N, ERROR
!
!IBM  integer *2  KEY(N), DATA(N)
      integer(Ikind) :: KEY(N), DATA(N)
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: I, J, D, K, IP1, JM1
!
!     ------------------------------------------------------------------
!
      IF (N .EQ. 1)  RETURN
      IF (N .LE. 0)  GO TO 6000

      ERROR = 0
!
!     ... INSERTION SORT ... FOR I := N-1 STEP -1 TO 1 DO ...
!
      I = N - 1
      IP1 = N

 2500     IF ( KEY (I) .GE. KEY (IP1) )  GO TO 2800
!
!             ... OUT OF ORDER ... MOVE UP TO CORRECT PLACE
!
              K = KEY (I)
              D = DATA (I)
              J = IP1
              JM1 = I
!
!             ... REPEAT ... UNTIL 'CORRECT PLACE FOR K FOUND'
!
 2600             KEY (JM1) = KEY (J)
                  DATA (JM1) = DATA (J)
                  JM1 = J
                  J = J + 1
                  IF  (J .GT. N)  GO TO 2700
                  IF (KEY (J) .GT. K)  GO TO 2600

 2700         KEY (JM1) = K
              DATA (JM1) = D

 2800     IP1 = I
          I = I - 1
          IF ( I .GT. 0 )  GO TO 2500

 3000 RETURN

 6000 ERROR = 1
      GO TO 3000

   END SUBROUTINE GPSKCN
   
   
!=============================================================================================                           
   SUBROUTINE GPSKCO (N, KEY, ERROR)                             
!=============================================================================================                        
!
!     ==================================================================
!
!     I N S E R T I O N    S O R T
!
!     INPUT:
!         N    -- NUMBER OF ELEMENTS TO BE SORTED
!         KEY  -- AN ARRAY OF LENGTH  N  CONTAINING THE VALUES
!                 WHICH ARE TO BE SORTED
!
!     OUTPUT:
!         KEY  -- WILL BE ARRANGED SO THAT VALUES ARE IN DECREASING
!                 ORDER
!
!     ==================================================================
!
      integer(Ikind) :: N, ERROR
!
!IBM  integer *2  KEY(N)
      integer(Ikind) :: KEY(N)
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: I, J, K, IP1, JM1
!
!     ------------------------------------------------------------------
!
      IF (N .EQ. 1)  RETURN
      IF (N .LE. 0)  GO TO 6000
!
      ERROR = 0
!
!     ... INSERTION SORT ... FOR I := N-1 STEP -1 TO 1 DO ...
!
      I = N - 1
      IP1 = N

 2500     IF ( KEY (I) .GE. KEY (IP1) )  GO TO 2800
!
!             ... OUT OF ORDER ... MOVE UP TO CORRECT PLACE
!
              K = KEY (I)
              J = IP1
              JM1 = I
!
!             ... REPEAT ... UNTIL 'CORRECT PLACE FOR K FOUND'
!
 2600             KEY (JM1) = KEY (J)
                  JM1 = J
                  J = J + 1
                  IF  (J .GT. N)  GO TO 2700
                  IF (KEY (J) .GT. K)  GO TO 2600

 2700         KEY (JM1) = K

 2800     IP1 = I
          I = I - 1
          IF ( I .GT. 0 )  GO TO 2500

 3000 RETURN

 6000 ERROR = 1
      GO TO 3000

   END SUBROUTINE GPSKCO
   

!=============================================================================================                              
   SUBROUTINE GPSKCP (N, INDEX, NVEC, DEGREE, ERROR)               
!=============================================================================================                              
!
!     ==================================================================
!
!     I N S E R T I O N      S O R T
!
!     INPUT:
!         N    -- NUMBER OF ELEMENTS TO BE SORTED
!         INDEX  -- AN ARRAY OF LENGTH  N  CONTAINING THE INDICES
!                 WHOSE DEGREES ARE TO BE SORTED
!         DEGREE -- AN  NVEC  VECTOR, GIVING THE DEGREES OF NODES
!                   WHICH ARE TO BE SORTED.
!
!     OUTPUT:
!         INDEX  -- WILL BE ARRANGED SO THAT VALUES ARE IN DECREASING
!                   ORDER
!         ERROR -- WILL BE ZERO UNLESS THE PROGRAM IS MALFUNCTIONING,
!                  IN WHICH CASE IT WILL BE EQUAL TO 1.
!
!     ==================================================================
!
      integer(Ikind) :: N, NVEC, ERROR
!
!IBM  integer *2  INDEX(N), DEGREE(NVEC)
      integer(Ikind) :: INDEX(N), DEGREE(NVEC)
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: I, J, V, IP1, JM1, INDEXI, INDXI1, INDEXJ
!
!     ------------------------------------------------------------------
!
      IF (N .EQ. 1)  RETURN
      IF (N .LE. 0)  GO TO 6000

      ERROR = 0
!
!     ------------------------------------------------------------------
!     INSERTION SORT THE ENTIRE FILE
!     ------------------------------------------------------------------
!
!
!     ... INSERTION SORT ... FOR I := N-1 STEP -1 TO 1 DO ...
!
      I = N - 1
      IP1 = N

 2500     INDEXI = INDEX (I)
          INDXI1 = INDEX (IP1)
          IF ( DEGREE(INDEXI) .GE. DEGREE(INDXI1) )  GO TO 2800
!
!             ... OUT OF ORDER ... MOVE UP TO CORRECT PLACE
!
              V = DEGREE (INDEXI)
              J = IP1
              JM1 = I
              INDEXJ = INDEX (J)
!
!             ... REPEAT ... UNTIL 'CORRECT PLACE FOR V FOUND'
!
 2600             INDEX (JM1) = INDEXJ
                  JM1 = J
                  J = J + 1
                  IF (J .GT. N)  GO TO 2700
                  INDEXJ = INDEX (J)
                  IF (DEGREE(INDEXJ) .GT. V)  GO TO 2600

 2700         INDEX (JM1) = INDEXI

 2800     IP1 = I
          I = I - 1
          IF ( I .GT. 0 )  GO TO 2500

 3000 RETURN

 6000 ERROR = 1
      GO TO 3000

   END SUBROUTINE GPSKCP
   
   
!=============================================================================================                                 
   SUBROUTINE GPSKCQ (N, INDEX, NVEC, DEGREE, ERROR)                
!=============================================================================================                              
!
!     ==================================================================
!
!     I N S E R T I O N      S O R T
!
!     INPUT:
!         N    -- NUMBER OF ELEMENTS TO BE SORTED
!         INDEX  -- AN ARRAY OF LENGTH  N  CONTAINING THE INDICES
!                 WHOSE DEGREES ARE TO BE SORTED
!         DEGREE -- AN  NVEC  VECTOR, GIVING THE DEGREES OF NODES
!                   WHICH ARE TO BE SORTED.
!
!     OUTPUT:
!         INDEX  -- WILL BE ARRANGED SO THAT VALUES ARE IN INCREASING
!                   ORDER
!         ERROR -- WILL BE ZERO UNLESS THE PROGRAM IS MALFUNCTIONING,
!                  IN WHICH CASE IT WILL BE EQUAL TO 1.
!
!     ==================================================================
      integer(Ikind) :: N, NVEC, ERROR
!
!IBM  integer *2  INDEX(N), DEGREE(NVEC)
      integer(Ikind) :: INDEX(N), DEGREE(NVEC)
!
!     ------------------------------------------------------------------
!
      integer(Ikind) :: I, J, V, INDEXI, INDXI1, INDEXJ, IP1, JM1
!
!     ------------------------------------------------------------------
!
      IF (N .EQ. 1)  RETURN
      IF (N .LE. 0)  GO TO 6000

      ERROR = 0
!
!     ------------------------------------------------------------------
!     INSERTION SORT THE ENTIRE FILE
!     ------------------------------------------------------------------
!
!
!     ... INSERTION SORT ... FOR I := N-1 STEP -1 TO 1 DO ...
!
      I = N - 1
      IP1 = N

 2500     INDEXI = INDEX (I)
          INDXI1 = INDEX (IP1)
          IF ( DEGREE(INDEXI) .LE. DEGREE(INDXI1) )  GO TO 2800
!
!             ... OUT OF ORDER ... MOVE UP TO CORRECT PLACE
!
              V = DEGREE (INDEXI)
              J = IP1
              JM1 = I
              INDEXJ = INDEX (J)
!
!             ... REPEAT ... UNTIL 'CORRECT PLACE FOR V FOUND'
!
 2600             INDEX (JM1) = INDEXJ
                  JM1 = J
                  J = J + 1
                  IF (J .GT. N)  GO TO 2700
                  INDEXJ = INDEX (J)
                  IF (DEGREE(INDEXJ) .LT. V)  GO TO 2600

 2700         INDEX (JM1) = INDEXI

 2800     IP1 = I
          I = I - 1
          IF ( I .GT. 0 )  GO TO 2500

 3000 RETURN

 6000 ERROR = 1
      GO TO 3000

   END SUBROUTINE GPSKCQ
  
   
!=============================================================================================                                 
   SUBROUTINE indinv ( mrenum, nrenum, np )
!=============================================================================================                                 
   integer(Ikind), intent(in    ) :: nrenum(*), np
   integer(Ikind), intent(   out) :: mrenum(*)
!---------------------------------------------------------------------------------------------    
!  This subroutine permutes nrenum
!---------------------------------------------------------------------------------------------  
  
!- locals: -----------------------------------------------------------------------------------
   integer(Ikind) :: nn
!---------------------------------------------------------------------------------------------    
   
   do nn = 1, np
      mrenum(nrenum(nn)) = nn
   end do

   END SUBROUTINE indinv   
   

!=============================================================================================
    SUBROUTINE reorderings_error (uerr, where, num, msg)
!=============================================================================================
    integer  (Ikind), intent(in), optional :: num, uerr
    character(len=*), intent(in), optional :: msg, where
!---------------------------------------------------------------------------------------------	
!   Prints an error message and stops 
!---------------------------------------------------------------------------------------------	
    
!--locals:------------------------------------------------------------------------------------ 
    integer  (Ikind ) :: ue   
!---------------------------------------------------------------------------------------------            

    if (present(uerr)) then 
      ue = uerr
    else 
      ue = 6
    end if    
       
    write(ue,'(/,a)')'Error (reorderings_error):'

    if (present(where)) write(ue,'(a)')'--> in reorderings_'//trim(adjustl(where))
        
    if (present(num)) write(ue,'(a,i0)')'--> error  #',num
        
    if (present(msg)) write(ue,'(a)')'--> '//trim(msg)
       
    write(ue,'(a,/)')'--> STOP'
        
    stop

    END SUBROUTINE reorderings_error   

END MODULE reorderings_m